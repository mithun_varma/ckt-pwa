export const overSeparatorMock = {
  "opta:44229:2:16": [
    {
      overNo: 15,
      overStr: "15.6",
      runs: 1,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 15,
      overStr: "15.5",
      runs: 0,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 15,
      overStr: "15.4",
      runs: 1,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 15,
      overStr: "15.3",
      runs: 6,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 15,
      overStr: "15.2",
      runs: 0,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 15,
      overStr: "15.1",
      runs: 1,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:17": [
    {
      overNo: 16,
      overStr: "16.6",
      runs: 0,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 16,
      overStr: "16.5",
      runs: 4,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 16,
      overStr: "16.4",
      runs: 0,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 16,
      overStr: "16.3",
      runs: 0,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 16,
      overStr: "16.2",
      runs: 1,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 16,
      overStr: "16.1",
      runs: 1,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:14": [
    {
      overNo: 13,
      overStr: "13.6",
      runs: 1,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 13,
      overStr: "13.5",
      runs: 1,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 13,
      overStr: "13.4",
      runs: 1,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 13,
      overStr: "13.3",
      runs: 1,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 13,
      overStr: "13.2",
      runs: 0,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 13,
      overStr: "13.1",
      runs: 1,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:15": [
    {
      overNo: 14,
      overStr: "14.6",
      runs: 1,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 14,
      overStr: "14.5",
      runs: 4,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 14,
      overStr: "14.4",
      runs: 0,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 14,
      overStr: "14.3",
      runs: 0,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 14,
      overStr: "14.2",
      runs: 2,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 14,
      overStr: "14.1",
      runs: 1,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:20": [
    {
      overNo: 19,
      overStr: "19.6",
      runs: 3,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 19,
      overStr: "19.5",
      runs: 0,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 19,
      overStr: "19.4",
      runs: 4,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 19,
      overStr: "19.3",
      runs: 1,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 19,
      overStr: "19.2",
      runs: 0,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 19,
      overStr: "19.1",
      runs: 1,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:18": [
    {
      overNo: 17,
      overStr: "17.6",
      runs: 4,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 17,
      overStr: "17.5",
      runs: 1,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 17,
      overStr: "17.4",
      runs: 1,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 17,
      overStr: "17.3",
      runs: 0,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 17,
      overStr: "17.2",
      runs: 1,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 17,
      overStr: "17.1",
      runs: 0,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:19": [
    {
      overNo: 18,
      overStr: "18.6",
      runs: 4,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 18,
      overStr: "18.5",
      runs: 1,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 18,
      overStr: "18.4",
      runs: 1,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 18,
      overStr: "18.3",
      runs: 1,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 18,
      overStr: "18.2",
      runs: 0,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 18,
      overStr: "18.1",
      runs: 2,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:2": [
    {
      overNo: 1,
      overStr: "1.6",
      runs: 1,
      wicketcount: 0,
      ballNo: 7
    },
    {
      overNo: 1,
      overStr: "1.5",
      runs: 4,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 1,
      overStr: "1.4",
      runs: 1,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 1,
      overStr: "1.3",
      runs: 0,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 1,
      overStr: "1.2",
      runs: 0,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 1,
      overStr: "1.1",
      runs: 0,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 1,
      overStr: "1.1",
      runs: 1,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:1": [
    {
      overNo: 0,
      overStr: "0.6",
      runs: 0,
      wicketcount: 0,
      ballNo: 8
    },
    {
      overNo: 0,
      overStr: "0.6",
      runs: 1,
      wicketcount: 0,
      ballNo: 7
    },
    {
      overNo: 0,
      overStr: "0.5",
      runs: 2,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 0,
      overStr: "0.4",
      runs: 0,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 0,
      overStr: "0.3",
      runs: 0,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 0,
      overStr: "0.3",
      runs: 1,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 0,
      overStr: "0.2",
      runs: 4,
      wicketcount: 0,
      ballNo: 2
    }
  ],
  "opta:44229:2:6": [
    {
      overNo: 5,
      overStr: "5.6",
      runs: 0,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 5,
      overStr: "5.5",
      runs: 1,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 5,
      overStr: "5.4",
      runs: 1,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 5,
      overStr: "5.3",
      runs: 1,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 5,
      overStr: "5.2",
      runs: 4,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 5,
      overStr: "5.1",
      runs: 0,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:12": [
    {
      overNo: 11,
      overStr: "11.6",
      runs: 4,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 11,
      overStr: "11.5",
      runs: 1,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 11,
      overStr: "11.4",
      runs: 0,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 11,
      overStr: "11.3",
      runs: 0,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 11,
      overStr: "11.2",
      runs: 1,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 11,
      overStr: "11.1",
      runs: 1,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:5": [
    {
      overNo: 4,
      overStr: "4.6",
      runs: 1,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 4,
      overStr: "4.5",
      runs: 4,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 4,
      overStr: "4.4",
      runs: 0,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 4,
      overStr: "4.3",
      runs: 0,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 4,
      overStr: "4.2",
      runs: 0,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 4,
      overStr: "4.1",
      runs: 4,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:13": [
    {
      overNo: 12,
      overStr: "12.6",
      runs: 1,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 12,
      overStr: "12.5",
      runs: 0,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 12,
      overStr: "12.4",
      runs: 2,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 12,
      overStr: "12.3",
      runs: 4,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 12,
      overStr: "12.2",
      runs: 1,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 12,
      overStr: "12.1",
      runs: 1,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:4": [
    {
      overNo: 3,
      overStr: "3.6",
      runs: 0,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 3,
      overStr: "3.5",
      runs: 1,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 3,
      overStr: "3.4",
      runs: 1,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 3,
      overStr: "3.3",
      runs: 1,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 3,
      overStr: "3.2",
      runs: 4,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 3,
      overStr: "3.1",
      runs: 1,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:3": [
    {
      overNo: 2,
      overStr: "2.6",
      runs: 0,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 2,
      overStr: "2.5",
      runs: 0,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 2,
      overStr: "2.4",
      runs: 0,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 2,
      overStr: "2.3",
      runs: 0,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 2,
      overStr: "2.2",
      runs: 0,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 2,
      overStr: "2.1",
      runs: 0,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:11": [
    {
      overNo: 10,
      overStr: "10.6",
      runs: 0,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 10,
      overStr: "10.5",
      runs: 1,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 10,
      overStr: "10.4",
      runs: 0,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 10,
      overStr: "10.3",
      runs: 0,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 10,
      overStr: "10.2",
      runs: 6,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 10,
      overStr: "10.1",
      runs: 0,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:10": [
    {
      overNo: 9,
      overStr: "9.6",
      runs: 0,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 9,
      overStr: "9.5",
      runs: 0,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 9,
      overStr: "9.4",
      runs: 1,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 9,
      overStr: "9.3",
      runs: 6,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 9,
      overStr: "9.2",
      runs: 0,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 9,
      overStr: "9.1",
      runs: 1,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:9": [
    {
      overNo: 8,
      overStr: "8.6",
      runs: 1,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 8,
      overStr: "8.5",
      runs: 4,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 8,
      overStr: "8.4",
      runs: 0,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 8,
      overStr: "8.3",
      runs: 1,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 8,
      overStr: "8.2",
      runs: 4,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 8,
      overStr: "8.1",
      runs: 0,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:8": [
    {
      overNo: 7,
      overStr: "7.6",
      runs: 0,
      wicketcount: 0,
      ballNo: 7
    },
    {
      overNo: 7,
      overStr: "7.5",
      runs: 1,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 7,
      overStr: "7.4",
      runs: 0,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 7,
      overStr: "7.3",
      runs: 0,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 7,
      overStr: "7.3",
      runs: 1,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 7,
      overStr: "7.2",
      runs: 1,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 7,
      overStr: "7.1",
      runs: 1,
      wicketcount: 0,
      ballNo: 1
    }
  ],
  "opta:44229:2:7": [
    {
      overNo: 6,
      overStr: "6.6",
      runs: 1,
      wicketcount: 0,
      ballNo: 6
    },
    {
      overNo: 6,
      overStr: "6.5",
      runs: 1,
      wicketcount: 0,
      ballNo: 5
    },
    {
      overNo: 6,
      overStr: "6.4",
      runs: 1,
      wicketcount: 0,
      ballNo: 4
    },
    {
      overNo: 6,
      overStr: "6.3",
      runs: 1,
      wicketcount: 0,
      ballNo: 3
    },
    {
      overNo: 6,
      overStr: "6.2",
      runs: 0,
      wicketcount: 0,
      ballNo: 2
    },
    {
      overNo: 6,
      overStr: "6.1",
      runs: 1,
      wicketcount: 0,
      ballNo: 1
    }
  ]
};

export const overSeparatorMockNew = [
  [
    [
      {
        "overNo": 20,
        "overStr": "19.6",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 7,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 20,
        "overStr": "19.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 20,
        "overStr": "19.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 20,
        "overStr": "19.3",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 20,
        "overStr": "19.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 20,
        "overStr": "19.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": "Wd1",
        "valid": false
      },
      {
        "overNo": 20,
        "overStr": "19.1",
        "runs": 0,
        "wicketcount": 1,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 19,
        "overStr": "18.6",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 19,
        "overStr": "18.5",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 19,
        "overStr": "18.4",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 19,
        "overStr": "18.3",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 19,
        "overStr": "18.2",
        "runs": 0,
        "wicketcount": 1,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 19,
        "overStr": "18.1",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 18,
        "overStr": "17.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 18,
        "overStr": "17.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 18,
        "overStr": "17.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 18,
        "overStr": "17.3",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 18,
        "overStr": "17.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 18,
        "overStr": "17.1",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 17,
        "overStr": "16.6",
        "runs": 0,
        "wicketcount": 1,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 17,
        "overStr": "16.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 17,
        "overStr": "16.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 17,
        "overStr": "16.3",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 17,
        "overStr": "16.2",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 17,
        "overStr": "16.1",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 16,
        "overStr": "15.6",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 16,
        "overStr": "15.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 16,
        "overStr": "15.4",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 16,
        "overStr": "15.3",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 16,
        "overStr": "15.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 16,
        "overStr": "15.1",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 15,
        "overStr": "14.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 15,
        "overStr": "14.5",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 15,
        "overStr": "14.4",
        "runs": 2,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 15,
        "overStr": "14.3",
        "runs": 0,
        "wicketcount": 1,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 15,
        "overStr": "14.2",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 15,
        "overStr": "14.1",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 14,
        "overStr": "13.6",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 14,
        "overStr": "13.5",
        "runs": 2,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 14,
        "overStr": "13.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 14,
        "overStr": "13.3",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 14,
        "overStr": "13.2",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 14,
        "overStr": "13.1",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 13,
        "overStr": "12.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 13,
        "overStr": "12.5",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 13,
        "overStr": "12.4",
        "runs": 0,
        "wicketcount": 1,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 13,
        "overStr": "12.3",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 13,
        "overStr": "12.2",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 13,
        "overStr": "12.1",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 12,
        "overStr": "11.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 12,
        "overStr": "11.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 12,
        "overStr": "11.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 12,
        "overStr": "11.3",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 12,
        "overStr": "11.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 12,
        "overStr": "11.1",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 11,
        "overStr": "10.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 11,
        "overStr": "10.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 11,
        "overStr": "10.4",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 11,
        "overStr": "10.3",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 11,
        "overStr": "10.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 11,
        "overStr": "10.1",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 10,
        "overStr": "9.6",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 10,
        "overStr": "9.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 10,
        "overStr": "9.4",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 10,
        "overStr": "9.3",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 10,
        "overStr": "9.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 10,
        "overStr": "9.1",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 9,
        "overStr": "8.6",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 9,
        "overStr": "8.5",
        "runs": 2,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 9,
        "overStr": "8.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 9,
        "overStr": "8.3",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 9,
        "overStr": "8.2",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 9,
        "overStr": "8.1",
        "runs": 2,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 8,
        "overStr": "7.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 7,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 8,
        "overStr": "7.6",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": "Wd1",
        "valid": false
      },
      {
        "overNo": 8,
        "overStr": "7.5",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 8,
        "overStr": "7.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 8,
        "overStr": "7.3",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 8,
        "overStr": "7.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 8,
        "overStr": "7.1",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 7,
        "overStr": "6.6",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 7,
        "overStr": "6.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 7,
        "overStr": "6.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 7,
        "overStr": "6.3",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 7,
        "overStr": "6.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 7,
        "overStr": "6.1",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 6,
        "overStr": "5.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 7,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 6,
        "overStr": "5.5",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 6,
        "overStr": "5.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": "Wd1",
        "valid": false
      },
      {
        "overNo": 6,
        "overStr": "5.4",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 6,
        "overStr": "5.3",
        "runs": 0,
        "wicketcount": 1,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 6,
        "overStr": "5.2",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 6,
        "overStr": "5.1",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 5,
        "overStr": "4.6",
        "runs": 0,
        "wicketcount": 1,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 5,
        "overStr": "4.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 5,
        "overStr": "4.4",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 5,
        "overStr": "4.3",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 5,
        "overStr": "4.2",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 5,
        "overStr": "4.1",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 4,
        "overStr": "3.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 4,
        "overStr": "3.5",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 4,
        "overStr": "3.4",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 4,
        "overStr": "3.3",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 4,
        "overStr": "3.2",
        "runs": 2,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 4,
        "overStr": "3.1",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 3,
        "overStr": "2.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 3,
        "overStr": "2.5",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 3,
        "overStr": "2.4",
        "runs": 2,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 3,
        "overStr": "2.3",
        "runs": 0,
        "wicketcount": 1,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 3,
        "overStr": "2.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 3,
        "overStr": "2.1",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 2,
        "overStr": "1.6",
        "runs": 6,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 2,
        "overStr": "1.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 2,
        "overStr": "1.4",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 2,
        "overStr": "1.3",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 2,
        "overStr": "1.2",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 2,
        "overStr": "1.1",
        "runs": 6,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 1,
        "overStr": "0.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 7,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 1,
        "overStr": "0.5",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 1,
        "overStr": "0.4",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 1,
        "overStr": "0.4",
        "runs": 5,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": "Wd5",
        "valid": false
      },
      {
        "overNo": 1,
        "overStr": "0.3",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 1,
        "overStr": "0.2",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 1,
        "overStr": "0.1",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ]
  ],
  [
    [
      {
        "overNo": 20,
        "overStr": "19.1",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 19,
        "overStr": "18.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 19,
        "overStr": "18.5",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 19,
        "overStr": "18.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 19,
        "overStr": "18.3",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 19,
        "overStr": "18.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 19,
        "overStr": "18.1",
        "runs": 2,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 18,
        "overStr": "17.6",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 18,
        "overStr": "17.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 18,
        "overStr": "17.4",
        "runs": 0,
        "wicketcount": 1,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 18,
        "overStr": "17.3",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 18,
        "overStr": "17.2",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 18,
        "overStr": "17.1",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 17,
        "overStr": "16.6",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 17,
        "overStr": "16.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 17,
        "overStr": "16.4",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 17,
        "overStr": "16.3",
        "runs": 2,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 17,
        "overStr": "16.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 17,
        "overStr": "16.1",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 16,
        "overStr": "15.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 16,
        "overStr": "15.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 16,
        "overStr": "15.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 16,
        "overStr": "15.3",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 16,
        "overStr": "15.2",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 16,
        "overStr": "15.1",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 15,
        "overStr": "14.6",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 15,
        "overStr": "14.5",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 15,
        "overStr": "14.4",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 15,
        "overStr": "14.3",
        "runs": 2,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 15,
        "overStr": "14.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 15,
        "overStr": "14.1",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 14,
        "overStr": "13.6",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 14,
        "overStr": "13.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 14,
        "overStr": "13.4",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 14,
        "overStr": "13.3",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 14,
        "overStr": "13.2",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 14,
        "overStr": "13.1",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 13,
        "overStr": "12.6",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 13,
        "overStr": "12.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 13,
        "overStr": "12.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 13,
        "overStr": "12.3",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 13,
        "overStr": "12.2",
        "runs": 2,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 13,
        "overStr": "12.1",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 12,
        "overStr": "11.6",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 12,
        "overStr": "11.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 12,
        "overStr": "11.4",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 12,
        "overStr": "11.3",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 12,
        "overStr": "11.2",
        "runs": 2,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 12,
        "overStr": "11.1",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 11,
        "overStr": "10.6",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 11,
        "overStr": "10.5",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 11,
        "overStr": "10.4",
        "runs": 0,
        "wicketcount": 1,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 11,
        "overStr": "10.3",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 11,
        "overStr": "10.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 11,
        "overStr": "10.1",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 10,
        "overStr": "9.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 10,
        "overStr": "9.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 10,
        "overStr": "9.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 10,
        "overStr": "9.3",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 10,
        "overStr": "9.2",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 10,
        "overStr": "9.1",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 9,
        "overStr": "8.6",
        "runs": 0,
        "wicketcount": 1,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 9,
        "overStr": "8.5",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 9,
        "overStr": "8.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 9,
        "overStr": "8.3",
        "runs": 2,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 9,
        "overStr": "8.2",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 9,
        "overStr": "8.1",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 8,
        "overStr": "7.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 8,
        "overStr": "7.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 8,
        "overStr": "7.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 8,
        "overStr": "7.3",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 8,
        "overStr": "7.2",
        "runs": 0,
        "wicketcount": 1,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 8,
        "overStr": "7.1",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 7,
        "overStr": "6.6",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 7,
        "overStr": "6.5",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 7,
        "overStr": "6.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 7,
        "overStr": "6.3",
        "runs": 2,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 7,
        "overStr": "6.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 7,
        "overStr": "6.1",
        "runs": 2,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 6,
        "overStr": "5.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 6,
        "overStr": "5.5",
        "runs": 2,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 6,
        "overStr": "5.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 6,
        "overStr": "5.3",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 6,
        "overStr": "5.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 6,
        "overStr": "5.1",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 5,
        "overStr": "4.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 5,
        "overStr": "4.5",
        "runs": 0,
        "wicketcount": 1,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 5,
        "overStr": "4.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 5,
        "overStr": "4.3",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 5,
        "overStr": "4.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 5,
        "overStr": "4.1",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 4,
        "overStr": "3.6",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 4,
        "overStr": "3.5",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 4,
        "overStr": "3.4",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 4,
        "overStr": "3.3",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 4,
        "overStr": "3.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 4,
        "overStr": "3.1",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 3,
        "overStr": "2.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 3,
        "overStr": "2.5",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 3,
        "overStr": "2.4",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 3,
        "overStr": "2.3",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 3,
        "overStr": "2.2",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 3,
        "overStr": "2.1",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 2,
        "overStr": "1.6",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 2,
        "overStr": "1.5",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 2,
        "overStr": "1.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 2,
        "overStr": "1.3",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 2,
        "overStr": "1.2",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 2,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 2,
        "overStr": "1.1",
        "runs": 4,
        "wicketcount": 0,
        "ballNo": 1,
        "ballStr": null,
        "valid": true
      }
    ],
    [
      {
        "overNo": 1,
        "overStr": "0.6",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 7,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 1,
        "overStr": "0.6",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 6,
        "ballStr": "Wd1",
        "valid": false
      },
      {
        "overNo": 1,
        "overStr": "0.5",
        "runs": 0,
        "wicketcount": 0,
        "ballNo": 5,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 1,
        "overStr": "0.4",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 4,
        "ballStr": null,
        "valid": true
      },
      {
        "overNo": 1,
        "overStr": "0.3",
        "runs": 1,
        "wicketcount": 0,
        "ballNo": 3,
        "ballStr": null,
        "valid": true
      }
    ]
  ]
]

export const testScoreSummary = {
    "teamScoreA": {
      "batsmanSummary1": {
        "topBatsman": {
          "dots": null,
          "balls": 65,
          "runs": 24,
          "fours": 2,
          "sixes": 0,
          "strikeRate": "36.92307692307693",
          "inningId": "1",
          "playerKey": "5165",
          "wicketType": "LBW",
          "bowler": "5363",
          "fielder": null,
          "dismissedBall": null,
          "dismissed": true,
          "dismissedAt": {
            "teamRuns": 106,
            "over": 55,
            "ball": 0,
            "wicketIndex": 10,
            "ballKey": null
          },
          "outStr": "L.B.W. Harry Conway",
          "displayName": "Nic Maddinson",
          "shortName": "Nic Maddinson"
        },
        "runnerBatsman": {
          "dots": null,
          "balls": 75,
          "runs": 19,
          "fours": 3,
          "sixes": 0,
          "strikeRate": "25.333333333333336",
          "inningId": "1",
          "playerKey": "7984",
          "wicketType": "CAUGHT",
          "bowler": "5363",
          "fielder": "5368",
          "dismissedBall": null,
          "dismissed": true,
          "dismissedAt": {
            "teamRuns": 106,
            "over": 55,
            "ball": 0,
            "wicketIndex": 10,
            "ballKey": null
          },
          "outStr": "Caught Kurtis Patterson Bowled Harry Conway",
          "displayName": null,
          "shortName": null
        }
      },
      "batsmanSummary2": {
        "topBatsman": {
          "dots": null,
          "balls": 111,
          "runs": 50,
          "fours": 6,
          "sixes": 0,
          "strikeRate": "45.04504504504504",
          "inningId": "2",
          "playerKey": "7034",
          "wicketType": "CAUGHT",
          "bowler": "5363",
          "fielder": "7419",
          "dismissedBall": null,
          "dismissed": true,
          "dismissedAt": {
            "teamRuns": 135,
            "over": 50,
            "ball": 0,
            "wicketIndex": 10,
            "ballKey": null
          },
          "outStr": "Caught Nick Larkin Bowled Harry Conway",
          "displayName": "Matt Short",
          "shortName": "Matt Short"
        },
        "runnerBatsman": {
          "dots": null,
          "balls": 58,
          "runs": 33,
          "fours": 3,
          "sixes": 0,
          "strikeRate": "56.896551724137936",
          "inningId": "2",
          "playerKey": "5165",
          "wicketType": "CAUGHT",
          "bowler": "7663",
          "fielder": "5316",
          "dismissedBall": null,
          "dismissed": true,
          "dismissedAt": {
            "teamRuns": 135,
            "over": 50,
            "ball": 0,
            "wicketIndex": 10,
            "ballKey": null
          },
          "outStr": "Caught Peter Nevill Bowled Greg West",
          "displayName": "Nic Maddinson",
          "shortName": "Nic Maddinson"
        }
      },
      "bowlerSummary1": {
        "topBowler": {
          "dots": 71,
          "runs": 31,
          "balls": 96,
          "maidenOvers": 2,
          "wickets": 5,
          "extras": null,
          "overs": "16",
          "economy": "1.9375",
          "inningId": "1",
          "overStr": null,
          "playerKey": "4557",
          "displayName": null,
          "shortName": null
        },
        "runnerBowler": {
          "dots": 74,
          "runs": 35,
          "balls": 90,
          "maidenOvers": 6,
          "wickets": 3,
          "extras": null,
          "overs": "15",
          "economy": "2.3333333333333335",
          "inningId": "1",
          "overStr": null,
          "playerKey": "4242",
          "displayName": "Peter Siddle",
          "shortName": "Peter Siddle"
        }
      },
      "bowlerSummary2": {
        "topBowler": {
          "dots": 60,
          "runs": 25,
          "balls": 72,
          "maidenOvers": 3,
          "wickets": 5,
          "extras": null,
          "overs": "12",
          "economy": "2.0833333333333335",
          "inningId": "2",
          "overStr": null,
          "playerKey": "4906",
          "displayName": null,
          "shortName": null
        },
        "runnerBowler": {
          "dots": 64,
          "runs": 34,
          "balls": 84,
          "maidenOvers": 4,
          "wickets": 3,
          "extras": null,
          "overs": "14",
          "economy": "2.4285714285714284",
          "inningId": "2",
          "overStr": null,
          "playerKey": "4242",
          "displayName": "Peter Siddle",
          "shortName": "Peter Siddle"
        }
      },
      "teamName": "VIC",
      "avatar": null,
      "runs": 194,
      "wickets": 10,
      "overs": "70.2"
    },
    "teamScoreB": {
      "batsmanSummary1": {
        "topBatsman": {
          "dots": null,
          "balls": 70,
          "runs": 45,
          "fours": 8,
          "sixes": 0,
          "strikeRate": "64.28571428571429",
          "inningId": "1",
          "playerKey": "5946",
          "wicketType": "CAUGHT",
          "bowler": "4557",
          "fielder": "8028",
          "dismissedBall": null,
          "dismissed": true,
          "dismissedAt": {
            "teamRuns": 106,
            "over": 55,
            "ball": 0,
            "wicketIndex": 10,
            "ballKey": null
          },
          "outStr": "Caught Travis Dean Bowled Jon Holland",
          "displayName": "Daniel Hughes",
          "shortName": "Daniel Hughes"
        },
        "runnerBatsman": {
          "dots": null,
          "balls": 61,
          "runs": 33,
          "fours": 3,
          "sixes": 0,
          "strikeRate": "54.09836065573771",
          "inningId": "1",
          "playerKey": "5316",
          "wicketType": "NO",
          "bowler": null,
          "fielder": null,
          "dismissedBall": null,
          "dismissed": true,
          "dismissedAt": {
            "teamRuns": 106,
            "over": 55,
            "ball": 0,
            "wicketIndex": 10,
            "ballKey": null
          },
          "outStr": "Not Out",
          "displayName": "Peter Nevill",
          "shortName": "Peter Nevill"
        }
      },
      "batsmanSummary2": {
        "topBatsman": {
          "dots": null,
          "balls": 72,
          "runs": 26,
          "fours": 1,
          "sixes": 0,
          "strikeRate": "36.11111111111111",
          "inningId": "2",
          "playerKey": "5316",
          "wicketType": "NO",
          "bowler": null,
          "fielder": null,
          "dismissedBall": null,
          "dismissed": true,
          "dismissedAt": {
            "teamRuns": 135,
            "over": 50,
            "ball": 0,
            "wicketIndex": 10,
            "ballKey": null
          },
          "outStr": "Not Out",
          "displayName": "Peter Nevill",
          "shortName": "Peter Nevill"
        },
        "runnerBatsman": {
          "dots": null,
          "balls": 48,
          "runs": 25,
          "fours": 2,
          "sixes": 0,
          "strikeRate": "52.083333333333336",
          "inningId": "2",
          "playerKey": "4827",
          "wicketType": "CAUGHT",
          "bowler": "5979",
          "fielder": "8028",
          "dismissedBall": null,
          "dismissed": true,
          "dismissedAt": {
            "teamRuns": 135,
            "over": 50,
            "ball": 0,
            "wicketIndex": 10,
            "ballKey": null
          },
          "outStr": "Caught Travis Dean Bowled Scott Boland",
          "displayName": "Steve O'Keefe",
          "shortName": "Steve O'Keefe"
        }
      },
      "bowlerSummary1": {
        "topBowler": {
          "dots": 69,
          "runs": 14,
          "balls": 78,
          "maidenOvers": 7,
          "wickets": 5,
          "extras": null,
          "overs": "13",
          "economy": "1.0769230769230769",
          "inningId": "1",
          "overStr": null,
          "playerKey": "5363",
          "displayName": "Harry Conway",
          "shortName": "Harry Conway"
        },
        "runnerBowler": {
          "dots": 111,
          "runs": 50,
          "balls": 138,
          "maidenOvers": 7,
          "wickets": 3,
          "extras": null,
          "overs": "23",
          "economy": "2.1739130434782608",
          "inningId": "1",
          "overStr": null,
          "playerKey": "5143",
          "displayName": "Trent Copeland",
          "shortName": "Trent Copeland"
        }
      },
      "bowlerSummary2": {
        "topBowler": {
          "dots": 99,
          "runs": 50,
          "balls": 120,
          "maidenOvers": 6,
          "wickets": 4,
          "extras": null,
          "overs": "20",
          "economy": "2.5",
          "inningId": "2",
          "overStr": null,
          "playerKey": "5363",
          "displayName": "Harry Conway",
          "shortName": "Harry Conway"
        },
        "runnerBowler": {
          "dots": 48,
          "runs": 30,
          "balls": 63,
          "maidenOvers": 2,
          "wickets": 3,
          "extras": null,
          "overs": "10",
          "economy": "3.0",
          "inningId": "2",
          "overStr": null,
          "playerKey": "7663",
          "displayName": "Greg West",
          "shortName": "Greg West"
        }
      },
      "teamName": "NSW",
      "avatar": null,
      "runs": 102,
      "wickets": 10,
      "overs": "38.5"
    },
    "bestBatsman": {
      "key": "opta:43782:5363",
      "crictecKey": null,
      "playerKey": "5363",
      "matchId": "opta:43782",
      "playerName": "Harry Conway",
      "role": null,
      "inningId": "4",
      "crictecMatchId": "opta:43782",
      "avatar": null,
      "teamAvatar": null,
      "shortName": "Harry Conway",
      "battingStatistics": [
        {
          "dots": null,
          "balls": 23,
          "runs": 5,
          "fours": 0,
          "sixes": 0,
          "strikeRate": "21.73913043478261",
          "inningId": "1",
          "wicketType": "CAUGHT",
          "bowler": "4242",
          "fielder": "7541",
          "dismissedBall": null,
          "dismissed": true,
          "dismissedAt": {
            "teamRuns": 106,
            "over": 55,
            "ball": 0,
            "wicketIndex": 10,
            "ballKey": null
          },
          "outStr": "Caught Seb Gotch Bowled Peter Siddle"
        },
        {
          "dots": null,
          "balls": 16,
          "runs": 2,
          "fours": 0,
          "sixes": 0,
          "strikeRate": "12.5",
          "inningId": "2",
          "wicketType": "CAUGHT",
          "bowler": "4242",
          "fielder": "7541",
          "dismissedBall": null,
          "dismissed": true,
          "dismissedAt": {
            "teamRuns": 135,
            "over": 50,
            "ball": 0,
            "wicketIndex": 10,
            "ballKey": null
          },
          "outStr": "Caught Seb Gotch Bowled Peter Siddle"
        }
      ],
      "bowlingStatistics": [
        {
          "dots": 69,
          "runs": 14,
          "balls": 78,
          "maidenOvers": 7,
          "wickets": 5,
          "extras": null,
          "overs": "13",
          "economy": "1.08",
          "inningId": "1",
          "overStr": null,
          "displayName": null,
          "shortName": null
        },
        {
          "dots": 99,
          "runs": 50,
          "balls": 120,
          "maidenOvers": 6,
          "wickets": 4,
          "extras": null,
          "overs": "20",
          "economy": "2.5",
          "inningId": "2",
          "overStr": null,
          "displayName": null,
          "shortName": null
        }
      ],
      "fieldingStatistics": [
        {
          "catches": 0,
          "runouts": 0,
          "stumbeds": 0,
          "inningId": "1"
        }
      ]
    },
    "bestBowler": {
      "key": "opta:43782:4557",
      "crictecKey": null,
      "playerKey": "4557",
      "matchId": "opta:43782",
      "playerName": null,
      "role": null,
      "inningId": "4",
      "crictecMatchId": "opta:43782",
      "avatar": null,
      "teamAvatar": null,
      "shortName": null,
      "battingStatistics": [
        {
          "dots": null,
          "balls": 15,
          "runs": 12,
          "fours": 2,
          "sixes": 0,
          "strikeRate": "80.0",
          "inningId": "1",
          "wicketType": "NO",
          "bowler": null,
          "fielder": null,
          "dismissedBall": null,
          "dismissed": true,
          "dismissedAt": {
            "teamRuns": 106,
            "over": 55,
            "ball": 0,
            "wicketIndex": 10,
            "ballKey": null
          },
          "outStr": "Not Out"
        },
        {
          "dots": null,
          "balls": 13,
          "runs": 18,
          "fours": 2,
          "sixes": 1,
          "strikeRate": "138.46153846153845",
          "inningId": "2",
          "wicketType": "NO",
          "bowler": null,
          "fielder": null,
          "dismissedBall": null,
          "dismissed": true,
          "dismissedAt": {
            "teamRuns": 135,
            "over": 50,
            "ball": 0,
            "wicketIndex": 10,
            "ballKey": null
          },
          "outStr": "Not Out"
        }
      ],
      "bowlingStatistics": [
        {
          "dots": 71,
          "runs": 31,
          "balls": 96,
          "maidenOvers": 2,
          "wickets": 5,
          "extras": null,
          "overs": "16",
          "economy": "1.94",
          "inningId": "1",
          "overStr": null,
          "displayName": null,
          "shortName": null
        },
        {
          "dots": 11,
          "runs": 10,
          "balls": 18,
          "maidenOvers": 1,
          "wickets": 0,
          "extras": null,
          "overs": "3",
          "economy": "3.33",
          "inningId": "2",
          "overStr": null,
          "displayName": null,
          "shortName": null
        }
      ],
      "fieldingStatistics": [
        {
          "catches": 0,
          "runouts": 1,
          "stumbeds": 0,
          "inningId": "1"
        },
        {
          "catches": 1,
          "runouts": 0,
          "stumbeds": 0,
          "inningId": "2"
        }
      ]
    },
    "teamA": "157",
    "teamB": "106",
    "abondoned": false,
    "inningOrder": [
      "teamScoreA",
      "teamScoreB",
      "teamScoreA",
      "teamScoreB"
    ],
    "format": "test"
  }