// import firebase from "firebase";
// export const initializeFirebase = () => {
//   firebase.initializeApp({
//     apiKey: "AIzaSyC4d7KSGD6u4Hx0YVKdFtpcuBOv1t9lvoU",
//     authDomain: "crictec-78852.firebaseapp.com",
//     databaseURL: "https://crictec-78852.firebaseio.com",
//     projectId: "crictec-78852",
//     storageBucket: "crictec-78852.appspot.com",
//     messagingSenderId: "550136502903"
//   });
// };

// navigator.serviceWorker
//   .register("./firebase-messaging-sw.js")
//   .then(registration => {
//     firebase.messaging().useServiceWorker(registration);
//   });

// export const askForPermissioToReceiveNotifications = async () => {
//   try {
//     const messaging = firebase.messaging();
//     await messaging.requestPermission();
//     const token = await messaging.getToken();
//     // console.log("user token", token);

//     // When user token is present subscribe to common topic
//     fetch(
//       "https://iid.googleapis.com/iid/v1/" + token + "/rel/topics/" + "common",
//       {
//         method: "POST",
//         headers: new Headers({
//           Authorization: "key=AIzaSyBnB85RcqWCdOb1s7EEcLP7nJrblbS8Eow"
//         })
//       }
//     )
//       .then(response => {
//         // console.log(response);
//         if (response.status < 200 || response.status >= 400) {
//           throw "Error subscribing to topic: " +
//             response.status +
//             " - " +
//             response.text();
//         }
//         // console.log("Subscribed to common topic");
//       })
//       .catch(error => {
//         // console.error(error);
//       });

//     return token;
//   } catch (error) {
//     // console.error(error);
//   }
// };

// export const subscribeToMatch = async topic => {
//   try {
//     const messaging = firebase.messaging();
//     await messaging.requestPermission();
//     const token = await messaging.getToken();
//     // console.log("user token", token);

//     // When user token is present subscribe to common topic
//     fetch(
//       "https://iid.googleapis.com/iid/v1/" + token + "/rel/topics/" + topic,
//       {
//         method: "POST",
//         headers: new Headers({
//           Authorization: "key=AIzaSyBnB85RcqWCdOb1s7EEcLP7nJrblbS8Eow"
//         })
//       }
//     )
//       .then(response => {
//         // console.log(response);
//         if (response.status < 200 || response.status >= 400) {
//           throw "Error subscribing to topic: " +
//             response.status +
//             " - " +
//             response.text();
//         }
//         // console.log("Subscribed to " + topic + " topic");
//       })
//       .catch(error => {
//         // console.error(error);
//       });

//     return token;
//   } catch (error) {
//     // console.error(error);
//   }
// };

// export const unSubscribeToMatch = async topic => {
//   try {
//     const messaging = firebase.messaging();
//     await messaging.requestPermission();
//     const token = await messaging.getToken();
//     // console.log("user token", token);

//     // When user token is present subscribe to common topic
//     fetch("https://iid.googleapis.com/iid/v1:batchRemove", {
//       method: "POST",
//       // data: {
//       //   to: `/topics/${topic}`,
//       //   registration_tokens: [token]
//       // },
//       body: JSON.stringify({
//         to: `/topics/${topic}`,
//         registration_tokens: [token]
//       }),
//       headers: new Headers({
//         Authorization: "key=AIzaSyBnB85RcqWCdOb1s7EEcLP7nJrblbS8Eow"
//       })
//     })
//       .then(response => {
//         // console.log(response);
//         if (response.status < 200 || response.status >= 400) {
//           throw "Error unSubscribing to topic: " +
//             response.status +
//             " - " +
//             response.text();
//         }
//         // console.log("unSubscribed from " + topic + " topic");
//       })
//       .catch(error => {
//         // console.error(error);
//       });

//     return token;
//   } catch (error) {
//     // console.error(error);
//   }
// };
