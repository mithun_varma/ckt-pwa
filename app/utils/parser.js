/* eslint-disable no-underscore-dangle */
/* eslint-disable func-names */
/* eslint-disable no-restricted-syntax */
const statusConstant = {
  UPCOMING: "notstarted",
  RUNNING: "started",
  COMPLETED: "completed",
  BETWEEN_INNINGS: "started"
};
const formatConstant = {
  T20: "t20",
  TEST: "test"
};
const playerParser = playerList => {
  const players = {};
  for (const [key, value] of Object.entries(playerList)) {
    players[key] = {
      name: value.playerName,
      fullname: value.playerName,
      role: value.role,
      key: value.key,
      innings: (function() {
        const innings = {};
        const KEYS = {
          battingStatistics: "batting",
          bowlingStatistics: "bowling",
          fieldingStatistics: "fielding"
        };
        ["battingStatistics", "bowlingStatistics", "fieldingStatistics"].map(
          stats => {
            Object.keys(value[stats]).map(_key => {
              innings[_key] = innings[_key] || {};
              innings[_key][KEYS[stats]] = value[stats][_key];
              return null;
            });
            return null;
          }
        );
        return innings;
      })(),
      teams: {}
    };
  }
  return players;
};
const teamParser = teamList => {
  const teams = {};
  ["teamA", "teamB"].map(team => {
    const data = teamList[team];
    teams[data.teamKey] = {
      key: data.name,
      name: data.fullName,
      shortName: data.shortName,
      avatar: data.avatar,
      keeper: data.keeper,
      captain: data.captain,
      players: data.squad,
      playingXi: data.playingx1,
      probablePlayingXi: data.playingEleven || [],
      playingElevenDeclared: data.playingElevenDeclared,
      teamKey: data.teamKey
    };
    return null;
  });
  return teams;
};
const inningsParser = inningsList => {
  const innings = {};
  if (inningsList)
    Object.keys(inningsList).map(_inning => {
      const data = inningsList[_inning] || {};
      innings[_inning] = {
        inningId: data.inningId,
        teamKey: data.teamKey,
        runs: data.runs,
        overs: data.overs,
        wickets: data.wickets,
        status: statusConstant[data.status],
        dotBalls: null,
        balls: null,
        extras: data.extras,
        wides: data.wide,
        noBalls: data.noballs,
        legByes: data.legByes,
        byes: data.byes,
        fours: data.fours,
        sixes: data.sixes,
        runRate: data.runRate,
        battingOrder: data.battingOrder,
        bowlingOrder: data.bowlingOrder,
        battedPlayers: data.battedPlayers,
        wicketOrder: data.wicketOrder,
        fallOfWickets: data.fallOfWickets,
        partnership: data.partnership,
        completed: data.completed,
        lastWicket: data.lastWicket,
        yetToBat: data.yetToBat
      };
      return null;
    });
  return innings;
};
const nowParser = (currentScore, fullScorecard) => {
  if (currentScore)
    return {
      inningId: currentScore.inningId,
      teamKey: currentScore.teamKey,
      battingTeam: currentScore.battingTeam,
      bowlingTeam: currentScore.bowlingTeam,
      striker: fullScorecard.striker,
      nonStriker: fullScorecard.nonStriker,
      bowler: fullScorecard.bowler,
      runs: currentScore.runs,
      wickets: currentScore.wickets,
      innings: currentScore.inningId,
      // innings: "1",
      runRate: currentScore.currentRunRate,
      status: statusConstant[currentScore.status],
      leadBy: currentScore.leadBy,
      trialBy: currentScore.trialBy,
      overs: currentScore.overs,
      thisOver: fullScorecard.thisOverRuns,
      totalOvers: null,
      balls: null,
      req: {
        reqRuns: currentScore.reqRuns,
        requiredRunRate: currentScore.reqRunRate
      },
      wicketOrder: currentScore.wicketOrder,
      fallOfWickets: currentScore.fallOfWickets,
      battingOrder: currentScore.battingOrder,
      bowlingOrder: currentScore.bowlingOrder
    };
  return null;
};
const inningOrderParser = inningOrder => {
  const order = [];
  inningOrder &&
    inningOrder.reverse().map(orderKey => {
      order.push(orderKey.split("_"));
      return null;
    });
  return order;
};
export default function parser(json) {
  const scorecard = json.fullScorecard.matchMiniScorecard;
  const { fullScorecard } = json;
  return {
    key: json.matchid || json.matchId,
    matchId: json.matchid || json.matchId,
    seriesId: scorecard.seriesId,
    criclyticsAvailable: scorecard.criclyticsAvailable,
    name: json.matchName,
    shortName: scorecard.matchShortName,
    relatedName: scorecard.relatedName,
    title: scorecard.title,
    venue: json.venue,
    venueId: json.venueId,
    venueCapacity: json.venueCapacity,
    format: formatConstant[scorecard.format] || "odi",
    matchOvers: null,
    status: statusConstant[scorecard.status],
    statusOverView: scorecard.statusOverView,
    firstBallStatus: scorecard.firstBallStatus,
    manOfTheMatch: scorecard.manOfTheMatch,
    prediction: scorecard.prediction,
    players: playerParser(json.playerStatsViews),
    teams: teamParser(scorecard),
    innings: inningsParser(scorecard.inningCollection),
    winnerTeam: scorecard.winningTeam,
    firstBatting: scorecard.firstBatting,
    startDate: {
      str: new Date(scorecard.startDate).toLocaleString(),
      iso: new Date(scorecard.startDate).toISOString(),
      timestamp: scorecard.startDate
    },
    endDate: {
      str: new Date(scorecard.endDate).toLocaleString(),
      iso: new Date(scorecard.endDate).toISOString(),
      timestamp: scorecard.endDate
    },
    day: "",
    battingOrder: inningOrderParser(scorecard.inningOrder),
    toss: scorecard.toss,
    now: nowParser(scorecard.currentScoreCard, fullScorecard),
    season: scorecard.seriesName,
    statusStr: json.statusStr,
    matchOfficials: json.matchOfficials || {},
    matchDescription: json.matchDescription,
    originalTossDecision: scorecard.originalTossDecision,
    totalMatches: json.totalMatches
  };
}
