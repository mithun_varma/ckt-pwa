/* eslint-disable no-underscore-dangle */
// const JSON = require("./parserJson");

const formatConstant = {
  T20: 't20',
  TEST: 'test',
};
const statusConstant = {
  UPCOMING: 'notstarted',
  RUNNING: 'started',
  COMPLETED: 'completed',
};

const teamParser = teamList => {
  const teams = {};
  ['teamA', 'teamB'].map(team => {
    const data = teamList[team] || {};
    teams[data.teamKey] = {
      key: data.name,
      name: data.fullName,
      shortName: data.shortName,
      avatar: data.avatar,
      keeper: data.keeper,
      captain: data.captain,
      players: data.squad,
      playingXi: data.playingx1,
    };
    return null;
  });
  return teams;
};
const inningsParser = inningsList => {
  const innings = {};
  if (inningsList)
    Object.keys(inningsList).map(_inning => {
      const data = inningsList[_inning] || {};
      innings[_inning] = {
        inningId: data.inningId,
        teamKey: data.teamKey,
        runs: data.runs,
        overs: data.overs,
        wickets: data.wickets,
        status: statusConstant[data.status],
        dotBalls: null,
        balls: null,
        extras: data.extras,
        wides: data.wide,
        noBalls: data.noballs,
        legByes: data.legByes,
        byes: data.byes,
        fours: null,
        sixers: null,
        runRate: null,
        battingOrder: data.battingOrder,
        bowlingOrder: data.bowlingOrder,
        wicketOrder: data.wicketOrder,
        fallOfWickets: data.fallOfWickets,
      };
      return null;
    });
  return innings;
};
const nowParser = (currentScore, fullScorecard = {}) => {
  if (currentScore)
    return {
      inningId: currentScore.inningId,
      teamKey: currentScore.teamKey,
      battingTeam: currentScore.battingTeam,
      bowlingTeam: currentScore.bowlingTeam,
      striker: fullScorecard.striker,
      nonStriker: fullScorecard.nonStriker,
      bowler: fullScorecard.bowler,
      runs: currentScore.runs,
      wickets: currentScore.wickets,
      innings: currentScore.inningId,
      runRate: currentScore.currentRunRate,
      status: statusConstant[currentScore.status],
      leadBy: currentScore.leadBy,
      trialBy: currentScore.trialBy,
      overs: currentScore.overs,
      totalOvers: fullScorecard.thisOverRuns,
      balls: null,
      req: {
        reqRuns: currentScore.reqRuns,
        requiredRunRate: currentScore.reqRunRate,
      },
      wicketOrder: currentScore.wicketOrder,
      fallOfWickets: currentScore.fallOfWickets,
      battingOrder: currentScore.battingOrder,
      bowlingOrder: currentScore.bowlingOrder,
    };
  return null;
};
const inningOrderParser = inningOrder => {
  const order = [];
  inningOrder.map(orderKey => {
    order.push(orderKey.split('_'));
    return null;
  });
  return order;
};
export default function(json) {
  return {
    key: json.matchId,
    name: json.matchName,
    shortName: json.matchShortName,
    relatedName: json.relatedName,
    title: json.title,
    venue: json.venue,
    format: formatConstant[json.format],
    matchOvers: null,
    status: statusConstant[json.matchStatus],
    statusOverview: 'result',
    dataReviewCheckPoint: 'post-match-validated',
    dlApplied: false,
    manOfTheMatch: json.manOfTheMatch,
    // resultBy: "wickets",
    prediction: json.prediction,
    // players: playerParser(json.playerStatsViews),
    teams: teamParser(json),
    innings: inningsParser(json.inningCollection),
    winnerTeam: json.winningTeam,
    firstBatting: json.firstBatting,
    startDate: {
      str: new Date(json.startDate).toLocaleString(),
      iso: new Date(json.startDate).toISOString(),
      timestamp: json.startDate,
    },
    day: '',
    battingOrder: inningOrderParser(json.inningOrder),
    toss: json.toss,
    now: nowParser(json.currentScoreCard, {}),
    season: json.seriesName,
    statusStr: json.statusStr,
  };
}
