export function toCapitalize(value) {
  return value.replace(/([a-z0-9])([A-Z])/g, '$1 $2').toLowerCase();
}

export function toUpperCase(value) {
  let str = value.replace(/([a-z0-9])([A-Z])/g, '$1 $2').toLowerCase();
  str = str.replace('odi', 'ODI');
  str = str.replace('Odi', 'ODI');
  return str;
}
