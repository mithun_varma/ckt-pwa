import { find } from "lodash";
import React from "react";
const cx150 = 98;
const cy150 = 102;
const cx30 = 35;
const cy30 = 104;
const cx90 = 92.5;
const cy90 = 62 ;
const svg = [
  {
    property: 30,
    svg: (
      <svg
        x={cx30}
        y={cy30}
        xmlns="http://www.w3.org/2000/svg"
        width="81"
        height="50"
        viewBox="0 0 81 50"
      >
        <g
          fill="none"
          fillRule="evenodd"
          transform="scale(-1 1) rotate(60 3.483 -5.967)"
        >
          <path fill="#FFF" d="M7.5.826L10 85H4L6.5.826a.5.5 0 0 1 1 0z" />
          <circle cx="7" cy="85" r="7" fill="#D44030" fillRule="nonzero" />
        </g>
      </svg>
    )
  },
  {
    property: 90,
    svg: (
      <svg
        x={cx90}
        y={cy90}
        xmlns="http://www.w3.org/2000/svg"
        width="15"
        height="92"
        viewBox="0 0 15 92"
      >
        <g fill="none" fillRule="evenodd" transform="translate(.763)">
          <path fill="#FFF" d="M7.5.826L10 85H4L6.5.826a.5.5 0 0 1 1 0z" />
          <circle cx="7" cy="85" r="7" fill="#D44030" fillRule="nonzero" />
        </g>
      </svg>
    )
  },
  {
    property: 150,
    svg: (
      <svg
        x={cx150}
        y={cy150}
        xmlns="http://www.w3.org/2000/svg"
        width="81"
        height="50"
        viewBox="0 0 81 50"
      >
        <g fill="none" fillRule="evenodd" transform="rotate(60 43.746 63.77)">
          <path fill="#FFF" d="M7.5.826L10 85H4L6.5.826a.5.5 0 0 1 1 0z" />
          <circle cx="7" cy="85" r="7" fill="#D44030" fillRule="nonzero" />
        </g>
      </svg>
    )
  }
];
export function generateSVG(angle) {
  return find(svg, ["property", angle]).svg;
}
