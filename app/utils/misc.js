/* eslint import/no-unresolved: 0 */
/* eslint import/extensions: 0 */
import { toast } from 'react-toastify';
import { DEFAULT_AUTO_CLOSE_DELAY } from './constants';
/* eslint-disable */
/**
 * this function will take below params and returns querystring value
 * if now found then returns null
 * @param {string} _name name of query param
 * @param {string} url full url
 */
export function getQueryParameterByName(_name, url) {
  if (!url) {
    console.error('url is required');
    return;
  }
  const name = _name.replace(/[\[\]]/g, '\\$&');
  let regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
  let results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

/**
 * this methods show/udpates toast
 * @param {Object} TOASTS current active toasts key object
 * @param {string} key key for current toast
 * @param {String} message msg should appear in toast
 * @param {String} type type for toast default = error
 * @param {Number} extraDelay (optional) if we want to add more delay ot toast default = 0
 */
export const showToast = (TOASTS, key, message, extraDelay = 0, type = 'error') => {
  const toastId = TOASTS[key];
  if (toast.isActive(toastId)) {
    toast.update(toastId, {
      render: message,
      autoClose: DEFAULT_AUTO_CLOSE_DELAY + extraDelay,
      type,
    });
  } else {
    TOASTS[key] = toast(message, {
      autoClose: DEFAULT_AUTO_CLOSE_DELAY + extraDelay,
      type,
    });
  }
};
