export default {
  splash: {
    logo: {
      effect: 'fadeIn',
      duration: 900,
    },
    brand: {
      effect: 'fadeInUp',
      duration: 900,
    },
  },

  home: {
    scoreCard: {
      effect: '',
      duration: 700,
    },
    fantasyfirstCard: {
      effect: 'fadeIn',
      duration: 1100,
    },
    fantasyHeading: {
      effect: '',
      duration: 1100,
    },
    fantasyContent: {
      effect: 'fadeInUp',
      duration: 1000,
    },
    newsHeading: {
      effect: '',
      duration: 1100,
    },
    newsContent: {
      effect: 'fadeInUp',
      duration: 1200,
    },
    reportsHeading: {
      effect: '',
      duration: 1100,
    },
    reportsContent: {
      effect: 'fadeInUp',
      duration: 1000,
    },
    viewAllContent: {
      effect: 'fadeInUp',
      duration: 1200,
    },
  },

  scoreCard: {
    run: {
      effect: '',
    },
    image: {
      effect: '',
    },
    mom: {
      effect: '',
    },
  },

  scoreDetails: {
    parent: {
      effect: '',
      duration: 800,
    },
    scoreCard: {
      effect: 'ZoomIn',
      duration: 1000,
    },
    live: {
      effect: 'fadeIn',
      duration: 1500,
    },
    scoreBoard: {
      effect: 'fadeIn',
      duration: 1500,
    },
    commentary: {
      effect: 'fadeIn',
      duration: 1500,
    },
    fantasyBoard: {
      effect: 'fadeIn',
      duration: 1500,
    },
    playingSquad: {
      effect: 'fadeIn',
      duration: 1500,
    },
  },

  ArticleDetails: {
    header: {
      effect: '',
      duration: 1100,
    },
    content: {
      effect: 'fadeInUp',
      duration: 1200,
    },
  },

  viewAllAnalysis: {
    parent: {
      effect: '',
      duration: 1500,
    },
    card: {
      effect: 'fadeInLeft',
      duration: 1000,
    },
  },

  schedule: {
    tags: {
      effect: '',
      duration: 400,
    },
  },

  grounds: {
    parent: {
      effect: '',
    },
  },

  aboutGround: {
    value: {
      effect: '',
    },
  },

  countryGround: {
    parent: {
      effect: '',
    },
  },

  groundDetails: {
    image: {
      effect: '',
    },
  },

  groundStats: {
    score: {
      effect: '',
    },
    series: {
      effect: '',
    },
  },

  playerDetails: {
    image: {
      effect: '',
    },
    stats: {
      effect: 'fadeIn',
      duration: 1000,
    },
    playerNews: {
      effect: 'fadeIn',
      duration: 1000,
    },
  },

  playerList: {
    card: {
      effect: 'fadeInLeft',
      duration: 1000,
    },
  },

  playerCard: {
    card: {
      effect: '',
      duration: 800,
    },
  },

  playerContent: {
    desc: {
      effect: '',
    },
  },

  commentary: {
    tags: {
      effect: '',
    },
    card: {
      effect: 'fadeInUp',
      duration: 1000,
    },
  },

  featuredArticles: {
    card: {
      effect: 'fadeInUp',
    },
  },

  series: {
    parent: {
      effect: '',
    },
    tags: {
      effect: '',
    },
    list: {
      effect: '',
    },
  },

  teams: {
    parent: {
      effect: '',
    },
    card: {
      effect: '',
      duration: 2000,
    },
  },

  teamsSchedule: {
    tags: {
      effect: '',
    },
    card: {
      effect: 'fadeIn',
      duration: 1000,
    },
    list: {
      effect: '',
      duration: 1000,
    },
  },

  emptyState: {
    effect: '',
    img: {
      // effect: 'zoomIn',
      duration: 1200,
    },
    text: {
      // effect: 'fadeInUp',
      duration: 1200,
    },
  },

  more: {
    parent: {
      effect: 'fadeInLeft',
      duration: 1500,
    },
  },

  listItem: {
    content: {
      effect: 'fadeInLeft',
      duration: 1200,
    },
  },

  articleCard: {
    card: {
      effect: 'fadeInUp',
      duration: 1500,
    },
  },
  playerProjector: {
    image: {
      effect: 'fadeIn',
      duration: 900,
    },
  },
};
