export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';
export const AUTHOR_DATE_FORMAT = 'D/MM/YYYY';
export const AUTHOR_DATE_FORMAT_NAME = 'DD MMM YYYY';
export const AUTHOR_DATE_FORMAT_NUMBER = 'D-MM-YYYY';
export const DEFAULT_AUTO_CLOSE_DELAY = 5000;
