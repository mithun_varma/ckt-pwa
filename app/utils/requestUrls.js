// export const BASE_APP_URL = "https://fendpoint.crictec.com/api/"; //QA
// const BASE_APP_ARTICLE_URL = "https://fcmsbend.crictec.com/api/"; //QA
export const BASE_APP_URL = "https://apigateway.cricket.com/api/";
const BASE_APP_ARTICLE_URL = "https://cmscust.cricket.com/api/";

const BASE_GRAPH_URL = "https://fantasyapi.cricket.com/fantasy";

const CRICLYTICS_URL = `${BASE_APP_URL}factEngines/`;
const CRICLYTICS_MATCH_URL = `${BASE_APP_URL}matches/`;
const CRICLYTICS_PRE_MATCH_PROJECTIONS = `${BASE_APP_URL}predictions/`;
const BASE_SCHEDULE_MATCH_URL = BASE_APP_URL;

export const API_GET_CRICLYTICS_SCREEN = matchId => ({
  url: `${CRICLYTICS_URL}get-criclytics-for-match`,
  method: "GET",
  params: {
    matchId
  }
});

export const API_FANFIGHT_GRAPH = matchId => {
  const query = `
  query matchDetails($matchFeedID: String!){
      matchDetails(matchFeedID: $matchFeedID){
          matchFeedID
          #matchMomentDate
          #matchSquadAvailable
          #matchSquadAdded
          #matchIsVisible
          #matchLineUpStatus
          #matchLineUpMessage
          #matchLineUpMessageStatus
          #matchLiveMessage
          #matchLiveMessageStatus
          matchTeamHome
          #matchStarted
          #matchType
          matchTeamAway
          #matchStatus
          matchDetails
          matchSquad {
          clubName
          matchFeedID
          playersArray {
              playerFeedID
              cricketPlayerID
              playerName
              playerClubName
              playerRole
              playerImage
              playerCredits
              playerPoints
              playerMatchPoints
              projectedPoints
              playerSelectionPercentage
              recentPerformances {
              score
              wickets
              opposition
              }
          }
          }
          matchTeamHomeFlag
          matchTeamAwayFlag
          matchTeamHomeShort
          matchTeamAwayShort
      }
  }
`;
  return {
    url: BASE_GRAPH_URL,
    method: "POST",
    data: { query, variables: { matchFeedID: matchId } }
  };
};

export const API_GET_VIDEOS_ARTICLE = {
  url: `${BASE_APP_ARTICLE_URL}articles`,
  method: "GET",
  params: {
    types: ["video"],
    limit: 5
  }
};

export const API_SCORE_SUMMARY = {
  url: `${BASE_APP_URL}getScoreCardSummary`,
  method: "GET"
};

export const API_GET_SESSION_PHASE = ({matchId, day}) => ({
  url: `${BASE_APP_URL}getPhaseOfSessions`,
  method: "GET",
  params: {
    matchId,
    day
  }
});

export const API_SCORE_SUMMARY_TEST = {
  url: `${BASE_APP_URL}getScoreCardSummaryTest`,
  method: "GET"
};

export const API_INNING_SUMMARY = (matchId, inningId, teamId) => ({
  url: `${BASE_APP_URL}getInningSummary`,
  method: "GET",
  params: {
    matchId,
    inningId,
    teamId
  }
});

export const API_INN_PHASE = ({matchId, inningId}) => ({
  url: `${BASE_APP_URL.replace("/api/", "/views/")}getPhaseOfInnings`,
  method: "GET",
  params: {
    matchId,
    inningId
  }
});

export const API_LAST_10_BALLS = {
  url: `${BASE_APP_URL}getLastTenBalls`,
  method: "GET"
};

export const API_GET_QUALIFICATION_TABLE = {
  url: `${BASE_APP_URL}teams/get-qualification-table`,
  method: "GET"
};

export const API_GET_FIRST_BATTING_SCORE = matchId => ({
  url: `${CRICLYTICS_URL}squads-for-projection`,
  method: "GET",
  params: {
    matchId
  }
});
//team magic moments
export const API_GET_TEAM_MAGIC_MOMENT = teamId => ({
  url: `${BASE_APP_URL}fetch/fetch-images`,
  method: "GET",
  params: {
    type: "moments",
    category: "Team"
  }
});
//nostalgic
export const API_GET_TEAM_NOSTALGIC_MOMENT = teamId => ({
  url: `${BASE_APP_URL}fetch/fetch-images`,
  method: "GET",
  params: {
    type: "nostalgia",
    category: "Team"
  }
});
//nostalgic ends

export const API_GET_HOME_CRIC = matchId => ({
  url: `${BASE_APP_URL}predictions/get-criclytics-home-page`,
  method: "GET",
  params: {
    matchId
  }
});

export const API_GET_TIME_MACHINE = (matchId, inningNo, overNo) => ({
  url: `${CRICLYTICS_MATCH_URL}get-similar-matches`,
  method: "GET",
  params: {
    matchId,
    inningNo,
    overNo
  }
});

export const API_POST_DEATH_BOWLERS = {
  url: `${CRICLYTICS_URL}get-probab`,
  method: "POST",
  params: {}
};

export const API_GET_PRE_MATCH_PLAYER_PROJECTIONS = matchId => ({
  url: `${CRICLYTICS_PRE_MATCH_PROJECTIONS}get-pre-match-player-probs`,
  method: "GET",
  params: {
    matchId
  }
});

export const API_GET_DEATH_OVER_BOWLERS = matchId => ({
  url: `${CRICLYTICS_URL}get-death-bowlers`,
  method: "GET",
  params: {
    matchId
  }
});

export const API_GET_PRE_MATCH_PROBABILITY = matchId => ({
  url: `${CRICLYTICS_URL}get-prematch-batting-projections`,
  method: "GET",
  params: {
    matchId
  }
});

export const API_GET_MOMENTUM_SHIFT = matchId => ({
  url: `${CRICLYTICS_URL}get-top-four-momentum-shift`,
  method: "GET",
  params: {
    matchId
  }
});

export const API_GET_LIVE_BATTING_PROBABILITY = matchId => ({
  url: `${CRICLYTICS_URL}get-live-batting-probabilities`,
  method: "GET",
  params: {
    matchId
  }
});

export const API_GET_LIVE_BOWLER_PREDICITON = matchId => ({
  url: `${CRICLYTICS_URL}get-live-bowling-probabilities`,
  method: "GET",
  params: {
    matchId
  }
});

export const API_GET_PRE_ANALYSIS = {
  url: `${BASE_APP_ARTICLE_URL}articles`,
  method: "GET",
  params: {
    types: ["pre_match_analysis"],
    limit: 5
  }
};

export const SCORECARD_ENDPOINT = `${BASE_APP_URL}scorecards`;

export const API_GET_MATCHES_FOR_CRICLYTICS_LANDED = {
  url: `${CRICLYTICS_URL}get-matches-for-criclytics`,
  method: "GET",
  params: {}
};

export const API_GET_FEATURED_SCORES = {
  url: `${BASE_APP_URL}scorecards`,
  method: "GET",
  params: {
    filters: "featured",
    cardType: "Micro",
    limit: 5
  }
};

export const API_GET_MATCH_UPS = id => ({
  url: `${BASE_APP_URL}matches/get-matchups`,
  method: "GET",
  params: {
    id
  }
});
export const API_SCORE_DETAILS = matchId => ({
  url: `${BASE_APP_URL}scorecards/${matchId}`,
  method: "GET",
  params: {
    cardType: "Full"
  }
});
export const API_GET_FANTASY_ANALYSIS = {
  url: `${BASE_APP_ARTICLE_URL}articles`,
  method: "GET",
  params: {
    types: ["analysis"],
    limit: 5
  }
};
export const API_GET_NEWS_OPINION = {
  url: `${BASE_APP_ARTICLE_URL}articles`,
  method: "GET",
  params: {
    // types: ["news", "opinion"],
    types: ["news"],
    limit: 5
  }
};

export const API_GET_SUGGESTION_OPINION = {
  url: `${BASE_APP_ARTICLE_URL}articles`,
  method: "GET",
  params: {
    types: ["opinion"],
    limit: 5
  }
};

export const API_GET_MATCH_REPORT = {
  url: `${BASE_APP_ARTICLE_URL}articles`,
  method: "GET",
  params: {
    types: ["match-report"],
    limit: 5
  }
};

// export const API_GET_COMMENTARY = matchId =>({
//   url: `${BASE_APP_URL}ballbyballs/${matchId}`,
//   method: 'GET',
//   params: {
//     skip:0,
//     limit: 10,
//   },
// });

export const API_GET_BALL_BY_BALL_MONMENTUM = (matchId, overNo, inningNo) => ({
  url: `${BASE_APP_URL}ballbyballs/over-by-over`,
  method: "GET",
  params: {
    matchId,
    overNo,
    inningNo
  }
});
export const API_GET_COMMENTARY = {
  url: `${BASE_APP_URL}ballbyballs`,
  method: "GET",
  params: {
    limit: 10
  }
};

export const API_GET_COMMENTARY_OVER_SEPARATOR = matchId => ({
  url: `${BASE_APP_URL}ballbyballs/over-separator`,
  method: "GET",
  params: {
    matchId
  }
});

// Highlights with team wise API URL
// Required Params: matchId, inningNumber, teamKey, highlights, lastId, latestId
export const API_GET_COMMENTARY_HIGHLIGHTS = payload => ({
  url: `${BASE_APP_URL}ballbyballs/highlights${payload.matchId &&
    `?matchId=${payload.matchId}`}${
    payload.skip ? `&skip=${payload.skip}` : ""
  }${`&limit=${payload.limit}`}${payload.inningNumber &&
    `&inningNumber=${payload.inningNumber}`}${payload.teamKey &&
    `&teamKey=${payload.teamKey}`}${
    payload.lastId ? `&lastId=${payload.lastId}` : ""
  }${payload.highlights &&
    `&highlights=${payload.highlights.join("&highlights=")}`}`,
  method: "GET"
});

// Highlights Wagon  API URL
// Required Params: matchId, inningNumber, teamKey, type[for filter]
export const API_GET_HIGHLIGHTS_WAGON_STATS = payload => ({
  url: `${BASE_APP_URL}teams/getTeamWagon${payload.matchId &&
    `?matchId=${payload.matchId}`}${payload.inningNumber &&
    `&inningNumber=${payload.inningNumber}`}${payload.teamKey &&
    `&teamKey=${payload.teamKey}`}${payload.type &&
    `&type=${payload.type.join("&type=")}`}`,
  method: "GET"
});

export const API_GET_ARTICLE_DETAILS = matchId => ({
  url: `${BASE_APP_ARTICLE_URL}articles/${matchId}`,
  method: "GET",
  params: {}
});

export const API_GET_FANTASY_BOARD = matchId => ({
  url: `${BASE_APP_URL}fantasy-scorecards/${matchId}`,
  method: "GET",
  params: {}
});

// Schedule Api Urls
export const API_GET_SCHEDULE_MATCHES = {
  // url: `${BASE_SCHEDULE_MATCH_URL}matches`,
  url: `${BASE_SCHEDULE_MATCH_URL}series/by-status`,
  method: "GET",
  params: {
    // tags: ["international", "domestic", "odi", "t20", "test", "women"],
    status: "UPCOMING" // UPCOMING, RUNNING, COMPLETED
    // skip: 0,
    // limit: 10
  }
};
export const API_GET_SERIES_MATCHES_BY_STATUS = payload => {
  return {
    // url: `${BASE_SCHEDULE_MATCH_URL}matches`,
    url: `${BASE_SCHEDULE_MATCH_URL}series/schedule-for-series`,
    method: "GET",
    params: {
      seriesId: payload.payload.seriesId,
      status: "UPCOMING", // UPCOMING, RUNNING, COMPLETED
      skip: payload.payload.skip,
      limit: payload.payload.limit
    }
  };
};

// export const API_GET_SERIES_MATCHES_BY_STATUS = payload => {
//   https://qaapi.crictec.com/api/series/schedule-for-series?status=UPCOMING&seriesId=opta:1
//   // console.log(payload);
//   return {
//     url: `http://10.100.1.13:8080/crictec_endpoints/views/getScheduleForSeries/UPCOMING/${
//       payload.payload.seriesId
//     }/${payload.payload.skip}/${payload.payload.limit}`,
//     method: "GET"
//   };
// };

export const API_GET_FEATURED_ARTICLES = matchId => ({
  url: `${BASE_APP_ARTICLE_URL}articles`,
  method: "GET",
  params: {
    matchId,
    types: ["news", "analysis", "pre_match_analysis", "opinion"]
  }
});

// Series API urls
export const API_GET_SERIES = tags => {
  return {
    url: `${BASE_SCHEDULE_MATCH_URL}series?${tags &&
      `tags=${tags.join("&tags=")}`}&limit=${50}&skip=${0}`,
    method: "GET"
  };
};

// PlayersList API urls
//manas
export const API_GET_PLAYERS_LIST = {
  url: `${BASE_SCHEDULE_MATCH_URL}players/get-players-multi-collection`,
  method: "GET",
  params: {}
};

// Featured Players LIST API urls
export const API_GET_FEATURED_PLAYERS_LIST = {
  url: `${BASE_SCHEDULE_MATCH_URL}players/featured`,
  method: "GET",
  params: {
    skip: 0,
    limit: 5
  }
};

// Specific Player Details API url
export const API_GET_PLAYER_DETAILS = playerId => ({
  url: `${BASE_SCHEDULE_MATCH_URL}players/${playerId}`,
  method: "GET",
  params: {}
});

export const API_GET_PLAYER_MOMENTS = playerId => ({
  url: `${BASE_SCHEDULE_MATCH_URL}fetch/fetch-images`,
  method: "GET",
  params: {
    category: "Player",
    categoryId: playerId,
    type: "moments"
  }
});

export const API_GET_LIVE_BOWLER_PROJECTIONS = matchId => ({
  url: `${CRICLYTICS_URL}get-live-bowling-probabilities`,
  method: "GET",
  params: {
    matchId
  }
});

export const API_GET_LIVE_MATCH_PREDICTION = matchId => ({
  url: `${CRICLYTICS_URL}get-live-score-predictor`,
  method: "GET",
  params: {
    matchId
  }
});

// Player News url
export const API_PLAYER_NEWS = playerId => ({
  url: `${BASE_APP_ARTICLE_URL}articles`,
  method: "GET",
  params: {
    playerId,
    skip: 0,
    limit: 10
  }
});

export const API_SERIES_DETAILS = seriesId => ({
  url: `${BASE_APP_URL}series/${seriesId}`,
  method: "GET"
});

export const API_SERIES_DETAILS_SQUADS = seriesId => ({
  url: `${BASE_APP_URL}series/${seriesId}/squads`,
  method: "GET",
  params: {
    format: ["odi", "t20", "test"]
  }
});

export const API_SERIES_DETAILS_ARTICLES = seriesId => ({
  url: `${BASE_APP_ARTICLE_URL}articles`,
  method: "GET",
  params: {
    seriesId,
    types: ["news"]
  }
});

export const API_SERIES_DETAILS_VENUES = seriesId => ({
  url: `${BASE_APP_URL}series/${seriesId}/venues`,
  method: "GET"
});

//adding now
export const API_GET_MORE_TEAM_LIST_BY_CATEGORY = {
  url: `${BASE_APP_URL}teams/getTeamCatByType`,
  method: "GET"
};

//
export const API_TEAMS_LIST = {
  url: `${BASE_APP_URL}/teams/getAllTeamCategory`,
  method: "GET",
  params: {
    // tags: ['international', 'domestic', 'leagues', 'womens'],
    // tags: ['international'],
    // skip: 0,
    // limit: 0
  }
};

export const API_TEAM_DETAILS = teamId => ({
  url: `${BASE_APP_URL}teams/latest-data`,
  method: "GET",
  params: {
    teamId,
    format: "test"
  }
});

export const API_TEAM_SCHEDULE = (teamId, date) => ({
  url: `${BASE_APP_URL}teams/schedules`,
  method: "GET",
  params: {
    teamId,
    date
    // liveStatus: ["ongoing", "upcoming"]
    // tags: ['international', 'domestic', 'leagues', 'womens'],
  }
});

export const API_TEAM_RESULTS = teamId => ({
  url: `${BASE_APP_URL}teams/${teamId}/matches`,
  method: "GET",
  params: {
    liveStatus: ["completed"]
    // tags: ['international', 'domestic', 'leagues', 'womens'],
  }
});

export const API_TRENDING_GROUND_LIST = {
  url: `${BASE_APP_URL}venues/trending`,
  method: "GET",
  params: {
    skip: 0,
    limit: 5
  }
};

export const API_POPULAR_GROUND_LIST = {
  url: `${BASE_APP_URL}venues/popular`,
  method: "GET",
  params: {
    skip: 0,
    limit: 5
  }
};

export const API_GROUND_LIST = {
  url: `${BASE_APP_URL}venues`,
  method: "GET",
  params: {}
};
export const API_GROUND_LIST_NEW = {
  url: `${BASE_APP_URL}admin/get-stadium-collection`,
  method: "GET",
  params: {}
};

export const API_GROUND_DETAILS = venueId => ({
  url: `${BASE_APP_URL}venuestats/${venueId}`,
  method: "GET",
  params: {}
});

export const API_GROUND_MAGIC_MOMENTS = venueId => ({
  url: `${BASE_APP_URL}/fetch/fetch-images/`,
  method: "GET",
  params: {
    category: "Stadium",
    categoryId: venueId,
    type: "moments"
  }
});

// Team Squads
export const API_TEAM_SQUADS = teamId => ({
  url: `${BASE_APP_URL}squads`,
  method: "GET",
  params: {
    teamId
  }
});

// Team News url
export const API_TEAM_ARTICLES = teamId => ({
  url: `${BASE_APP_ARTICLE_URL}articles`,
  method: "GET",
  params: {
    teamId
  }
});

// Latest Ranking url
export const API_RANKING_LATEST = {
  url: `${BASE_APP_URL}/ranks/latest`,
  method: "GET",
  params: {
    format: "odi",
    role: "team",
    gender: "male"
  }
};

// Gallery Photos/Videos Url //commented out by manas
// export const API_GET_GALLERY = {
//   url: `${BASE_APP_ARTICLE_URL}fetch/fetch-images`, ///api/fetch/fetch-images?
//   method: "GET",
//   params: {}
// };
// Gallery Photos/Videos Url
export const API_GET_GALLERY = {
  url: `${BASE_APP_URL}upload/get-photoGallery`,
  method: "GET",
  params: {
    category: "Series"
  }
};

export const API_GET_IMAGES = {
  url: `${BASE_APP_ARTICLE_URL}articles`,
  method: "GET",
  params: {
    types: ["image"]
  }
};

export const API_GET_GALLERY_BY_ID = id => ({
  url: `${BASE_APP_ARTICLE_URL}gallery/${id}`,
  method: "GET",
  params: {}
});

export const API_GET_VIDEO_GALLERY = {
  url: `${BASE_APP_ARTICLE_URL}gallery`,
  method: "GET",
  params: {
    type: "video",
    limit: 5
  }
};

export const API_GET_VIDEOS_BY_TYPE = {
  url: `${BASE_APP_ARTICLE_URL}articles`,
  method: "GET",
  params: {
    types: ["video"]
  }
};

export const API_GET_VIDEOS_BY_ID = id => ({
  url: `${BASE_APP_ARTICLE_URL}articles/${id}`,
  method: "GET",
  params: {}
});

// Points Table API url
export const API_GET_POINTS_TABLE = {
  url: `${BASE_APP_URL}admin/get-points-table`,
  method: "GET",
  params: {}
};

// Records API url
export const API_GET_RECORDS = {
  url: `${BASE_APP_URL}records/get-records`,
  method: "GET",
  params: {
    format: "test", // odi,t20,test
    recordType: "runs", // runs,average,strikeRate,hundreds,fifties,fours,sixes
    playingType: "batting" // batting,bowling
  }
};

//polls

export const API_GET_POLL_HOME = {
  url: `${BASE_APP_URL}polls/get-poll-for-home-screen`,
  method: "GET"
};

export const API_GET_POLL_BY_MATCH_ID = {
  url: `${BASE_APP_URL}polls/get-match-polls`,
  method: "GET",
  params: {
    id: ""
  }
};

export const API_GET_POLL_BY_POLL_ID = {
  url: `${BASE_APP_URL}polls/get-poll-for-poll-id`,
  method: "GET",
  params: {
    pollId: ""
  }
};

export const API_POST_POLL = {
  url: `${BASE_APP_URL}polls/add`,
  method: "GET",
  params: {
    id: "",
    option: ""
  }
};

// Dekhiye Bhaiya, ye Fan-Fight waala API hai
export const API_GET_FANFIGHT_PLAYERS = matchId => ({
  url: `${BASE_APP_URL}fanfight/get-players-for-team`,
  method: "GET",
  params: {
    matchId
  }
});

export const API_SAVE_FANFIGHT_TEAM = {
  url: `${BASE_APP_URL}fanfight/save-fanfight-team`,
  method: "POST",
  params: {}
};

// API FOR FETCHING ALL VIDEOS
export const API_GET_VIDEOS = () => ({
  url: `${BASE_APP_URL}upload/get-videoGallery`,
  method: "GET",
  params: {
    category: "Series"
  }
});

export const API_GET_MATCH_UPS_PLAYER_ONE = payload => {
  return {
    url: `${BASE_APP_URL}matches/get-player-one-data`,
    method: "GET",
    params: {
      crictecMatchId: payload.payload
    }
  };
};

export const API_GET_MATCH_UPS_FOR_SELECTED_PLAYER = payload => {
  return {
    url: `${BASE_APP_URL}matches/get-player-matchups`,
    method: "GET",
    params: {
      crictecMatchId: payload.payload.matchId,
      playerId: payload.payload.playerId,
      role: payload.payload.role
    }
  };
};

export const API_GET_KEY_STATS = payload => {
  return {
    url: `${BASE_APP_URL}criclytics/get-key-stats`,
    method: "GET",
    params: {
      crictecMatchId: payload.payload
    }
  };
};
