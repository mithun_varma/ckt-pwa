import React, { Component } from "react";

import {
  screenWidth,
  screenHeight
} from "./../../../styleSheet/screenSize/ScreenDetails";

import rightArrowBlack from "./../../../styleSheet/svg/rightArrowBlack.svg";
//
import historicMatch from "./../../../styleSheet/svg/historicMatch.svg";
import newsArticle from "./../../../styleSheet/svg/newsArticle.svg";
//
import playerIcon from "./../../../styleSheet/svg/playerIcon.svg";
import teamIcon from "./../../../styleSheet/svg/teamIcon.svg";
import stadiumsIcon from "./../../../styleSheet/svg/stadiumsIcon.svg";
//
import photoGallery from "./../../../styleSheet/svg/photoIcons.svg";
import videosIcon from "./../../../styleSheet/svg/videosIcon.svg";
//
import rankingsIcon from "./../../../styleSheet/svg/rankingsIcon.svg";
import recordsIcon from "./../../../styleSheet/svg/recordsIcon.svg";

const dataArrayFirst = [
  {
    icon: historicMatch,
    text: "Historic Match"
  },
  {
    icon: newsArticle,
    text: "News and Article"
  }
];
const dataArraySecond = [
  {
    icon: playerIcon,
    text: "Player"
  },
  {
    icon: teamIcon,
    text: "Teams"
  },
  {
    icon: stadiumsIcon,
    text: "Stadiums"
  }
];
const dataArraySecondNew = [
  {
    icon: photoGallery,
    text: "Photo Gallery"
  },
  {
    icon: videosIcon,
    text: "Videos"
  }
];
const dataArrayThird = [
  {
    icon: rankingsIcon,
    text: "Rankings"
  },
  {
    icon: recordsIcon,
    text: "Records"
  }
];
// const dataArrayFourth = [
//   {
//     icon: settingsIcon,
//     text: "Settings"
//   },
//   {
//     icon: crictecIcon,
//     text: "About CricTec"
//   },
//   {
//     icon: feedbackIcon,
//     text: "Feedback"
//   }
// ];

export default class MoreScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeOption: "" //set default value
    };
    this._handelNavigationClick = this._handelNavigationClick.bind(this);
  }
  _handelNavigationClick(event) {
    const navigateTo = event.target.innerHTML;

    const history = this.props;
    if (navigateTo === "Player") {
      this.props.history.push("/players");
    } else if (navigateTo === "Teams") {
      this.props.history.push("/teams");
    } else if (navigateTo === "Historic Match") {
      this.props.history.push("/historicMatch");
    } else if (navigateTo === "News and Article") {
      this.props.history.push("/article-ui");
    } else if (navigateTo === "Stadiums") {
      this.props.history.push("/Stadiums");
    } else if (navigateTo === "Photo Gallery") {
      this.props.history.push("/photoGallery");
    } else if (navigateTo === "Videos") {
      this.props.history.push("/videos");
    } else if (navigateTo === "Rankings") {
      this.props.history.push("/rankings");
    } else if (navigateTo === "Records") {
      this.props.history.push("/records");
    } else if (navigateTo === "Settings") {
      this.props.history.push("/settings");
    } else if (navigateTo === "About CricTec") {
      this.props.history.push("/about");
    } else if (navigateTo === "Feedback") {
      this.props.history.push("/scoreCard");
    }
    this.props.showMoreScreen();
  }
  render() {
    const history = this.props;
    return <div>{this.showMoreMenu()}</div>;
  }
  showMoreMenu() {
    return (
      <div style={pageStyle.showMoreMenu}>
        <div style={pageStyle.moreHeading}>More</div>
        <div style={pageStyle.menuWhiteContainer}>
          {dataArrayFirst.map(singleWhite, key => {
            return (
              <div
                key={key}
                style={pageStyle.menuWhiteSingleBox}
                onClick={this._handelNavigationClick}
              >
                <div style={pageStyle.menuWhiteSingleBoxIcon}>
                  <img src={singleWhite.icon} />
                </div>
                <div style={pageStyle.menuWhiteSingleBoxText}>
                  {singleWhite.text}
                </div>
                <div style={pageStyle.menuWhiteSingleBoxSpace}>{null}</div>
                <div style={pageStyle.menuWhiteSingleBoxArrow}>
                  <img src={rightArrowBlack} alt="right-arrow" />
                </div>
              </div>
            );
          })}
        </div>
        <div style={pageStyle.menuWhiteContainer}>
          {dataArraySecond.map((singleWhite, key) => {
            return (
              <div
                key={key}
                style={pageStyle.menuWhiteSingleBox}
                onClick={this._handelNavigationClick}
              >
                <div style={pageStyle.menuWhiteSingleBoxIcon}>
                  <img src={singleWhite.icon} />
                </div>
                <div style={pageStyle.menuWhiteSingleBoxText}>
                  {singleWhite.text}
                </div>
                <div style={pageStyle.menuWhiteSingleBoxSpace}>{null}</div>
                <div style={pageStyle.menuWhiteSingleBoxArrow}>
                  <img src={rightArrowBlack} alt="right-arrow" />
                </div>
              </div>
            );
          })}
        </div>
        <div style={pageStyle.menuWhiteContainer}>
          {dataArraySecondNew.map((singleWhite, key) => {
            return (
              <div
                key={key}
                style={pageStyle.menuWhiteSingleBox}
                onClick={this._handelNavigationClick}
              >
                <div style={pageStyle.menuWhiteSingleBoxIcon}>
                  <img src={singleWhite.icon} />
                </div>
                <div style={pageStyle.menuWhiteSingleBoxText}>
                  {singleWhite.text}
                </div>
                <div style={pageStyle.menuWhiteSingleBoxSpace}>{null}</div>
                <div style={pageStyle.menuWhiteSingleBoxArrow}>
                  <img src={rightArrowBlack} alt="right-arrow" />
                </div>
              </div>
            );
          })}
        </div>
        <div style={pageStyle.menuWhiteContainer}>
          {dataArrayThird.map(singleWhite => {
            return (
              <div
                style={pageStyle.menuWhiteSingleBox}
                onClick={this._handelNavigationClick}
              >
                <div style={pageStyle.menuWhiteSingleBoxIcon}>
                  <img src={singleWhite.icon} />
                </div>
                <div style={pageStyle.menuWhiteSingleBoxText}>
                  {singleWhite.text}
                </div>
                <div style={pageStyle.menuWhiteSingleBoxSpace}>{null}</div>
                <div style={pageStyle.menuWhiteSingleBoxArrow}>
                  <img src={rightArrowBlack} alt="right-arrow" />
                </div>
              </div>
            );
          })}
        </div>
        {/* <div style={pageStyle.menuWhiteContainer}>
          {dataArrayFourth.map(singleWhite, key => {
            return (
              <div
                keye={key}
                style={pageStyle.menuWhiteSingleBox}
                onClick={this._handelNavigationClick}
              >
                <div style={pageStyle.menuWhiteSingleBoxIcon}>
                  <img src={singleWhite.icon} />
                </div>
                <div style={pageStyle.menuWhiteSingleBoxText}>
                  {singleWhite.text}
                </div>
                <div style={pageStyle.menuWhiteSingleBoxSpace}>{null}</div>
                <div style={pageStyle.menuWhiteSingleBoxArrow}>
                  <img src={rightArrowBlack} alt="right-arrow" />
                </div>
              </div>
            );
          })}
        </div> */}
      </div>
    );
  }
}

const pageStyle = {
  menuWhiteSingleBoxArrow: {
    display: "flex",
    flex: 0.2,
    flexDirection: "flex-start"
  },
  menuWhiteSingleBoxSpace: {
    display: "flex",
    flex: 0.1
  },
  menuWhiteSingleBoxText: {
    display: "flex",
    flex: 0.6,
    justifyContent: "flex-start"
  },
  menuWhiteSingleBoxIcon: {
    display: "flex",
    flex: 0.1,
    marginLeft: screenWidth * 0.033,
    justifyContent: "flex-start"
  },
  menuWhiteSingleBox: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    padding: "16px 0px 16px 0px"
  },
  menuWhiteContainer: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#ffffff",
    boxShadow: "0px 3px 6px 0px #13000000",
    width: screenWidth * 0.911,
    color: "#141b2f",
    fontSize: "14px",
    marginTop: "12px",
    boxRadius: "3px"
  },
  moreHeading: {
    display: "flex",
    fontSize: "22px",
    color: "#141b2f",
    fontWeight: "700",
    marginTop: screenHeight * 0.025
  },
  showMoreMenu: {
    display: "flex",
    flexDirection: "column",
    flex: 1,
    backgroundColor: "#edeff4",
    width: screenWidth,
    height: screenHeight * 0.91,
    paddingLeft: screenWidth * 0.044,
    paddingRight: screenWidth * 0.044
  },
  bottomMenuOption: {
    display: "flex",
    flexDirection: "row",
    flex: 1,
    boxShadow: "0px -3px 3px 0px #1e000000",
    backGroundColor: "#edeff4"
  },
  conatiner: {
    display: "flex",
    flexDirection: "column",
    flex: 1,
    zIndex: "1",
    width: screenWidth,
    height: screenHeight
  },
  menuTagContainer: {
    display: "flex",
    flexDirection: "row",
    flex: 0.069,
    height: screenHeight * 0.092,
    width: screenWidth,
    boxShadow: "0px -3px 3px 0px #1e000000",
    backGroundColor: "#ffffff"
  },
  screenContainer: {
    display: "flex",
    flexDirection: "column",
    flex: 0.93,
    height: screenHeight * 0.91,
    width: screenWidth,
    overflow: "auto"
  },
  moreOptionContainer: {
    display: "flex",
    flexDirection: "column",
    flex: 0.93,
    width: screenWidth,
    position: "absolute",
    backGroundColor: "#fffff",
    zIndex: "999"
  }
};
