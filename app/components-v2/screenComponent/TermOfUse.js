import React, {Component} from 'react';
import { screenWidth } from '../../styleSheet/screenSize/ScreenDetails';
import Header from "./../commonComponent/Header";
import { gradientBlack,white, black,gradientRedOrangeLeft } from '../../styleSheet/globalStyle/color';


export default class TermOfUse extends Component {
    constructor(props){
        super(props);
        this.state={
            isHeaderBackground: true
        };

    }
    render(){
        return(
            <div>
            <div
            style={{
              fontFamily: "Montserrat",
              fontWeight: 700,
              fontSize: 22,
              color: white,
              marginBottom: 12,
              minHeight: "90px",
              paddingLeft: "16px",
              textTransform: "capitalize",
              background: gradientRedOrangeLeft
            }}
          >
            {null}
          </div>

          <div>
            <Header
              title={"Terms of Use"}
              isHeaderBackground={this.state.isHeaderBackground}
              leftArrowBack
              textColor={white}
              backgroundColor={gradientRedOrangeLeft}
              history={this.props.history}
              leftIconOnClick={() => {
                this.props.history.goBack();
              }}
            />
          </div>
          <div style={pageStyle.conatiner}>
                <div style={{display:'flex',flex:1, fontSize:'16px',fontWeight:'700',padding:'10px'}}>
                    TERMS AND CONDITIONS OF USE
                </div>
                <div style={pageStyle.heading}>
                    INTRODUCTION
                </div>
                <div style={pageStyle.paragraph}>
                    This website, www.cricket.com (&quot;Site&quot;, “Website”), and its related mobile applications (“Apps”) are
                    owned and operated by Crictec Media Limited (hereafter referred to as &quot;Crictec&quot;, &quot;the Company&quot;, “we”,
                    “us” and “our”). These terms and conditions of use govern your access to and use of the Site and Apps. If
                    you do not agree to be bound by these terms and conditions, you should stop using the Site and Apps
                    immediately.
                </div>
                <div style={pageStyle.paragraph} >
                    For the purpose of these terms and conditions of use, wherever the context so requires &quot;you&quot; or &quot;your&quot; or
                    &quot;user&quot; shall mean any natural or legal person who (a) has agreed to become a member of the Site and
                    Apps by registering for an account on the Site or through the Apps; or (b) browses the Site and/or Apps.
                </div>
                <div style={pageStyle.paragraph}>
                    Please note that by accessing or using the Site and Apps you agree to be bound by these terms and
                    conditions of use and these terms and conditions of use are a legally binding agreement between you and
                    the Company, the owner of the Site and Apps.
                </div>
                <div  style={pageStyle.heading}>
                    INFORMATION ABOUT US
                </div>
                <div style={pageStyle.paragraph}>
                    Crictec is a private limited company and registered in Ireland under company number 630962 and having
                    its registered address at 2 nd Floor, Palmerston House, Fenian Street, Dublin 2, Ireland.
                </div>
                <div style={pageStyle.heading}>
                    CHANGES TO THESE TERMS AND CONDITIONS OF USE
                </div>
                <div style={pageStyle.paragraph}>
                    The Company shall have the right at any time to change or modify the terms and conditions applicable to
                    your use of the Site and/or Apps, or any part thereof, or to impose new conditions, including but not
                    limited to, adding fees and charges for use. Such changes, modifications, additions or deletions shall be
                    effective immediately upon notice thereof, which may be given by means including but not limited to,
                    posting on the Site and/or Apps, or by electronic or conventional mail, or by any other means by which
                    the user obtains notice thereof. Any use of the Site or Apps by the user after such notice shall be deemed
                    to constitute acceptance by the user of such changes, modifications or additions.
                </div>
                <div style={pageStyle.heading}>
                    REGISTRATION
                </div>
                <div style={pageStyle.paragraph}>
                    In order to access certain restricted areas and services on the Site and Apps, you may apply to register for
                    an account. By applying to register for an account, you represent and warrant that all information you
                    provide, now or in the future, is accurate and that you will maintain the accuracy of such information. The
                    Company reserves the right, in its sole discretion, to refuse registration of an account for any reason.
                </div>
                <div style={pageStyle.paragraph}>
                    If you choose, or are provided with, a password or any other log-in details to access your account as part
                    of our security procedures, you must treat such information as confidential and you must not disclose it or
                    make it available to any third party. You agree to immediately notify us of any unauthorised use of your
                    log-in details or of any other breach of security that might impact on your use of the Site and/or Apps
                    and/or ours. The Company shall not be liable for any loss or damage arising from your failure to comply
                    with this provision.
                </div>
                <div style={pageStyle.paragraph}>
                    Your registration is valid from the date you first log-in to the Site or Apps and is automatically renewed
                    every time you log-in thereafter. If you do not log-in to the Site or Apps for a continuous period of 90
                    days your registration could be automatically cancelled.
                </div>
                <div style={pageStyle.heading}>
                    LICENCE TO ACCESS AND USE THE SITE AND APPS
                </div>
                <div style={pageStyle.paragraph}>
                    Subject to these terms and conditions of use, the Company hereby grants you the right to access and use
                    the Site and Apps only for your personal and non-commercial use and in a way that these terms and
                    conditions of use permit and for no other purpose.
                </div>
                <div style={pageStyle.paragraph}>
                    You may post and share links, images, text and other content from the Site and the Apps on social media
                    platforms like Facebook, Twitter, Google+, Pinterest, with appropriate link-back to the original source.                
                </div>
                <div style={pageStyle.heading}>
                    RESTRICTED USES
                </div>
                <div style={pageStyle.paragraph}>
                    In relation to the Site and the Apps, you agree that you shall:
                </div>
                <div style={pageStyle.paragraph}>
                    a) not modify, copy, make derivative works, reproduce, republish, upload, download, post, transmit
                    or distribute in any way any material or content from the Site or the Apps including code and
                    software, except where specifically authorised by us;

                </div>
                <div style={pageStyle.paragraph}>
                        b) not licence, sell, rent, lease, transfer, assign, distribute, host or otherwise commercially exploit the
                        Site or the Apps, whether in whole or in part, or any content displayed on such;
                </div>
                <div style={pageStyle.paragraph}>
                    c) not access, view or use the Site or the Apps in order to build a competitor website, app, platform,
                    service, competition or game;
                </div>
                <div style={pageStyle.paragraph}>
                    d) not access, screen scrape, retrieve or index any portion of the Site or the Apps or reframe or
                    reformat any of the content thereon;
                </div>
                <div style={pageStyle.paragraph}>
                    e) not use the Site or the Apps to harvest or otherwise collect by any means data, program material
                    or any other information whatsoever (including without limitation, email addresses and personal
                    data of other users);
                </div>
                <div style={pageStyle.paragraph}>
                    f) not misuse the Site or the Apps by knowingly or recklessly introducing viruses, trojans, worms,
                    logic bombs or other material which is malicious or harmful;
                </div>
                <div style={pageStyle.paragraph}>
                    g) not attempt to gain unauthorised access to the Site or the Apps, the server on which the Site
                    and/or Apps are stored or any server, computer or database connected to the Site or Apps;
                </div>
                <div style={pageStyle.paragraph}>
                    h) not attack the Site or the Apps via a denial-of-service attack or a distributed denial-of-service
                    attack;
                </div>
                <div style={pageStyle.paragraph}>
                    i) not carry out any form of network monitoring or intercept any communications transmitted in
                    connection with the Site or the Apps;
                </div>
                <div style={pageStyle.paragraph}>
                    j) ensure that your use of the Site and the Apps complies at all times with applicable laws and is
                    strictly in accordance with these terms and conditions of use;
                </div>
                <div style={pageStyle.paragraph}>
                    k) not use the Site or the Apps in a manner that infringes someone else’s rights, including
                    Intellectual Property and privacy rights;
                </div>
                <div style={pageStyle.paragraph}>
                    l) not use the Site or the Apps for any advertising, marketing, charitable or political purposes,
                    except where specifically authorised by us;
                </div>
                <div style={pageStyle.paragraph}>
                    m) not apply to register for an account on the Site and Apps by automated means or under fraudulent
                    pretenses;
                </div>
                <div style={pageStyle.paragraph}>
                    n) not use or attempt to use another user’s account, log-in details or credentials or attempt to obtain
                    or solicit such from them; and
                </div>
                <div style={pageStyle.paragraph}>
                    o) not use the Site or the Apps in any way that is unlawful, deceptive or fraudulent or in any manner
                    that has such a purpose or effect, including without limitation providing false data to Crictec in
                    order to apply for registration of an account or to pay for a service.
                </div>
                <div style={pageStyle.heading}>
                    USE OF COMMUNICATION/ PUBLIC FORUMS
                </div>
                <div style={pageStyle.paragraph}>
                    The Site and Apps may contain message/bulletin boards, chat rooms, comment sections under article
                    posts or blog posts or other message or communication facilities (collectively, &quot;Forums&quot;), you agree to
                    use the Forums only to send and receive messages and material that are proper and related to the
                    particular Forum. You agree that when using a Forum, you shall:
                </div>
                <div style={pageStyle.paragraph}>
                    a) not defame, abuse, harass, stalk, threaten or otherwise violate the legal rights (such as rights of
                    privacy and Intellectual Property) of others;

                </div>
                <div style={pageStyle.paragraph}>
                    b) not publish, post, distribute or disseminate any defamatory, infringing, obscene, indecent or
                    unlawful material or information;
                </div>
                <div style={pageStyle.paragraph}>
                    c) not upload files or other materials that contain software or other material protected by Intellectual
                    Property (or by rights of privacy) unless you own or control the rights thereto or are otherwise
                    permitted by law to do so;
                </div>
                <div style={pageStyle.paragraph}>
                    d) not upload files that contain viruses, corrupted files, or any other similar software or programs
                    that may damage the operation of the Site and the Apps or another’s computer, device or
                    information technology system;
                </div>
                <div style={pageStyle.paragraph}>
                    e) not conduct or forward surveys, contests, chain letters or spam;
                </div>
                <div style={pageStyle.paragraph}>
                    f) not download any file posted by another user of a Forum that you know, or reasonably should
                    know, cannot be legally distributed in such manner; and
                </div>
                <div style={pageStyle.paragraph}>
                g) comply with any content rules that we may issue from time to time.
                </div>
                <div style={pageStyle.paragraph}>
                You represent and warrant that that you have the right to submit and make publicly available any content
                posted to the Forums. User content posted by you shall be subject to relevant laws and may be removed,
                or subject to investigation under applicable laws. You agree that the Company may disclose or preserve
                user content if required to do so by law or in the good faith belief that such preservation or disclosure is
                reasonably necessary to: (a) comply with law or legal process; (b) respond to claims that any user content
                violates the rights of third parties; or (c) protect the rights, property, or personal safety of the Company,
                other users and the public. Furthermore, if you are found to be in non-compliance with the laws and
                regulations of your territory or these terms and conditions of use, we may in accordance with the section
                entitled ‘Termination and Suspension’ terminate your account/block your access to the Site and Apps and
                we reserve the right to remove any user content that is not compliant with these terms and conditions of
                use.
                </div>
                <div style={pageStyle.paragraph}>
                    All Forums are public and not private communications. Chats, postings, conferences, and other
                    communications by other users are not endorsed by the Company, and such communications shall not be
                    considered reviewed, screened, or approved by the Company. The Company reserves the right for any
                    reason to remove without notice any content posted to the Forums received from users, including without
                    limitation message board postings or comments posted under articles or blog posts.
                </div>
                <div style={pageStyle.heading}>
                    INTELLECTUAL PROPERTY
                </div>
                <div style={pageStyle.paragraph}>
                    All proprietary rights (including Intellectual Property) in the Site and the Apps, including the design of
                    such and any information and content contained therein, are the valuable and exclusive property of the
                    Company (or its licensors, where applicable), and nothing in these terms and conditions of use shall be
                    construed as transferring or assigning any such ownership rights or any other interest in such rights to you
                    or any other person or entity, except where otherwise stated.
                </div>
                <div style={pageStyle.paragraph}>
                    For the purposes of these terms and conditions of use, &quot;Intellectual Property&quot; shall mean all intellectual
                    and industrial property rights including without limitation, logos, brand names, images, designs,
                    photographs, video clips, other materials that appear as part of a website or mobile application, copyright,
                    database rights and rights in computer software, domain names, business names, trade marks, service
                    marks, trade dress, rights in get-up and goodwill, the right to sue for passing off and any other intellectual
                    property rights whether registered or unregistered and existing now or in the future
                </div>
                <div style={pageStyle.heading}>
                    PRIVACY
                </div>
                <div style={pageStyle.paragraph}>
                    Your privacy is important to us. Please read our Privacy Statement
                     to understand how we collect, use and process your personal data. We recommend that you do
                    this before using our Site or any of our Apps.
                </div>
                <div style={pageStyle.heading}>
                    ERRORS, CHANGES AND UNAVAILABILITY OF THE SITE AND APPS
                </div>
                <div style={pageStyle.paragraph}>
                    The contents and information published on the Site and the Apps may include inaccuracies or
                    typographical errors from time to time. The Company may, but shall not be obliged, from time to time
                    update the contents of the Site and the Apps. The Company does not guarantee the accuracy,
                    completeness, reliability or currency of the contents and information published on the Site and the Apps
                    and does not accept any responsibility for keeping such up to date and complete or any liability for any
                    failure to do so.
                </div>
                <div style={pageStyle.paragraph}>
                    The Company and/or its respective suppliers may, without notice to you, make improvements and/or
                    changes in the Site and the Apps or any part thereof. This means that we may add or remove temporarily
                    or permanently any content, feature, component or other functionality of the Site and the Apps. The
                    Company shall not be liable to you or any third party for any changes to the Site and the Apps or any part
                    thereof. Any changes to the Site and the Apps shall also be subject to these terms and conditions of use. If
                    you are not happy with such changes, you can cease using the Site and the Apps.
                </div>
                <div style={pageStyle.paragraph}>
                    The Site and the Apps may be temporarily unavailable from time to time for various reasons including,
                    without limitation, due to required maintenance, telecommunications interruptions, or other disruptions.
                    As such, access to the Site and Apps are provided on an “as is” and “as available” basis as set out below
                    in the section entitled ‘Liability and Indemnification’. The Company will not be liable if for any reason
                    the Site, the Apps or any part thereof is unavailable at any time.
                </div>
                <div style={pageStyle.heading}>
                    SECURITY OF COMMUNICATIONS
                </div>
                <div style={pageStyle.paragraph}>
                    Although we have taken all reasonable security precautions, the nature of communication via the Internet
                    and other electronic means is such that we cannot guarantee the privacy or confidentiality of any
                    information relating to you passing by such methods. In accessing the Site and the Apps, you accept that
                    communications may not be free from interference by third parties and may not remain confidential.
                </div>
                <div style={pageStyle.heading}>
                    LINKS TO THIRD PARTY WEBSITES
                </div>
                <div style={pageStyle.paragraph}>
                    The Site and Apps may contain links to websites owned or operated by parties other than Crictec. Such
                    links are provided for your convenience only. Crictec does not monitor or control outside websites and is
                    not responsible for their content. Crictec’s inclusion of links to an outside website does not imply any
                    endorsement of the material on our Site or Apps or, unless expressly disclosed otherwise, any
                    sponsorship, affiliation or association with its owner, operator or sponsor.
                </div>
                <div style={pageStyle.heading}>
                    SWEEPSTAKES, CONTESTS AND PROMOTIONS
                </div>
                <div style={pageStyle.paragraph}>
                    Any sweepstakes, contest or similar promotion made available through the Company’s Site and/or Apps
                    or for which the Company may, from time to time, send e-mail messages to you, will be governed by
                    official rules that are separate from these terms and conditions of use. By participating in any such
                    sweepstakes, contest or similar promotion, you will become subject to its specific official rules. Note,
                    however, that you remain subject to these terms and conditions of use to the extent that they do not
                    conflict with the applicable official rules.
                </div>
                <div style={pageStyle.heading}>
                    LIABILITY AND INDEMNIFICATION
                </div>
                <div style={{display:'flex', flex:1, fontSize:'14px', fontWeight:'500'}}>
                    Important – Please read this section carefully as it addresses the Company’s liability to you.
                </div>
                <div style={pageStyle.paragraph}>
                    You expressly agree that your use of the Site and the Apps is at your sole risk.
                </div>
                <div style={pageStyle.paragraph}>
                    You hereby acknowledge and agree that the Site and the Apps are available for use ‘as is’ and ‘as
                    available’, with no warranties of any kind whatsoever and that, without prejudice to the generality of the
                    foregoing, we make no warranty regarding, and shall have no responsibility for, the accuracy, availability,
                    reliability, security, fitness for purpose or performance of the Site or the Apps or the contents thereof. To
                    the fullest extent permitted by applicable law, we hereby expressly exclude all conditions, warranties,
                    guarantees and other terms which might otherwise be implied by statute, common law or otherwise.
                </div>
                <div style={pageStyle.paragraph}>
                    Nothing in these terms and conditions of use excludes or limits the Company’s liability for death or
                    personal injury resulting from its negligence, fraud or fraudulent misrepresentation. Notwithstanding the
                    foregoing, in no event shall the Company and/or its associated entities be liable to you in connection with
                    the Site and the Apps for any direct, indirect, punitive, incidental, special or consequential damages or
                    losses howsoever arising whether based on contract, tort (including negligence or breach of statutory
                    duty), misrepresentation (whether innocent or negligent) strict liability or otherwise and in no
                    circumstances shall they be liable for:
                </div>
                <div style={pageStyle.paragraph}>
                     your inability to use the Site, the Apps or any products or services made available through such;
                </div>
                <div style={pageStyle.paragraph}>
                     any decision made or action taken in reliance on any content displayed on the Site and/or the
                    Apps;
                </div>
                <div style={pageStyle.paragraph}>
                     any delay or service interruption to the Site or the Apps;
                </div>
                <div style={pageStyle.paragraph}>
                     the loss of use, data, profits or opportunity or arising out of or in connection with the access, use,
                    performance or functionality of the Site, Apps or any of their contents;

                </div>
                <div style={pageStyle.paragraph}>
                     the conduct of any third party in relation to the Site and Apps; and
                </div>
                <div style={pageStyle.paragraph}>
                     loss of or damage to any device or other property.
                </div> 
                <div style={pageStyle.heading}>
                    TERMINATION AND SUSPENSION
                </div>
                <div style={pageStyle.paragraph}>
                    The Company reserves the right, in its sole discretion and for any reason, to suspend or terminate your
                    access to all or part of the Site and/or Apps at any time without notice, and without issuing any refunds if
                    the Company suspects or knows that the user is in breach of these terms and conditions of use or for any
                    actual or improper use of the Site and/or the Apps. If you use multiple accounts, you may have action
                    taken against all of your accounts.
                </div>
                <div style={pageStyle.paragraph}>
                    The Company reserves the right to withdraw the Site and/or the Apps at any time and without notice to
                    you. We shall not be liable to the user or any third party for any loss or damage howsoever arising from
                    the termination or withdrawal of the Site and/or Apps or any part thereof.
                </div>
                <div style={pageStyle.heading}>
                    SEVERABILITY
                </div>
                <div style={pageStyle.paragraph}>
                    To the extent that any provisions of these terms and conditions of use are held by any court or any
                    competent authority to be invalid, unlawful or unenforceable, then such provision shall be severed,
                    modified or deleted from the remaining terms, conditions and provisions, which will continue to be valid
                    to the fullest extent permitted by applicable law.
                </div>
                <div style={pageStyle.heading}>
                PRECEDENCE
                </div>
                <div style={pageStyle.paragraph}>
                In the event of any conflict and/or inconsistency as between the provisions contained in these terms and
                conditions of use and those in any official rules for sweepstakes, contest or similar promotion, the official
                rules shall take precedence.
                </div>
                <div style={pageStyle.heading}>
                WAIVER                
                </div>
                <div style={pageStyle.paragraph}>
                    The failure by the Company to perform any of its obligations under these terms and conditions of use, or
                    to exercise any of its rights or remedies under these terms and conditions of use, shall not constitute a
                    waiver of such rights or remedies and shall not release you from compliance with such obligations. A
                    waiver by the Company of any default by you shall not constitute a waiver of any subsequent or future
                    default. No waiver by the Company of any of your obligations under these terms and conditions of use
                    shall be effective unless it is expressly stated to be a waiver and is communicated to you in writing.
                </div>
                <div style={pageStyle.heading}>
                    GOVERNING LAW AND JURISDICTION
                </div>
                <div style={pageStyle.paragraph}>
                    These terms and conditions of use and any disputes or claims arising out of or in connection with it or its
                    subject matter or formation (including non-contractual disputes or claims) shall be governed by and
                    interpreted in accordance with the laws of Ireland. For the avoidance of doubt, mandatory consumer laws
                    in the country in which you live shall continue to apply to these terms and conditions of use.
                </div>
                <div style={pageStyle.paragraph}>
                    The user and Crictec agree that the courts of Ireland shall have non-exclusive jurisdiction to settle any
                    dispute or claim arising out of or in connection with these terms and conditions of use or its subject matter
                    or formation (including non-contractual disputes or claims). You can bring a claim to enforce your
                    consumer protection rights in connection with these terms and conditions of use in the country in which
                    you live.
                </div>
                <div style={pageStyle.heading}>
                    CONTACT US
                </div>
                <div style={pageStyle.paragraph}>
                    If you wish to contact us, please email info@cricket.com.
                </div>
            </div>
            </div>
        );
    }

}
const pageStyle={
    conatiner:{
        display:'flex',
        flex:1,
        flexDirection:'column',
        fontFamily:'Montserrat',
        backgroundColor:'#fff',
        marginLeft:screenWidth*0.0411,
        marginRight:screenWidth*0.0411,
    },
    heading:{
        padding:'5px 10px 5px 10px',
        display:'flex',
        flex:1,
        fontWeight:'500',
        fontSize:'14px',
    },
    paragraph:{
        padding:'1px 10px 1px 10px',
        display:'flex',
        flex:1,
        fontSize:'14px',
        fontWeight:'300',
    }
}