import React, {Component} from 'react';
import { screenWidth, screenHeight } from '../../styleSheet/screenSize/ScreenDetails';
import crictecIcon from "./../../styleSheet/svg/crictecIconNew.svg";
import Header from "./../commonComponent/Header";
import { gradientBlack,white, black,gradientRedOrangeLeft } from '../../styleSheet/globalStyle/color';
import HrLine from '../commonComponent/HrLine';
import ChevronRight from "@material-ui/icons/ChevronRight";
import { grey_8 } from '@material-ui/core/colors';

export default class AboutCrictec extends Component {
    constructor(props){
        super(props);
        this.state={
            isHeaderBackground:true
        }

    }
    render(){
        return(
            <div>
            <div
            style={{
              fontFamily: "Montserrat",
              fontWeight: 700,
              fontSize: 22,
              color: white,
              marginBottom: 12,
              minHeight: "90px",
              paddingLeft: "16px",
              textTransform: "capitalize",
              background: gradientRedOrangeLeft
            }}
          >
            {null}
          </div>


          <div>
            <Header
              title={"About Crictec"}
              isHeaderBackground={this.state.isHeaderBackground}
              leftArrowBack
              textColor={white}
              backgroundColor={gradientRedOrangeLeft}
              history={this.props.history}
              leftIconOnClick={() => {
                this.props.history.goBack();
              }}
            />
          </div>
            <div style={pageStyle.conatiner}>
                <div style={pageStyle.singleRow} onClick={()=>{this.props.history.push("/termOfUse")}}>
                    <img src={crictecIcon} alt="policy" style={pageStyle.imageRow} width="25px" height="25px" />
                    <div style={pageStyle.textRow}>
                        Term of Use
                    </div>
                    <div style={pageStyle.arrow}>
                        <ChevronRight style={{ fontSize: 24, color: grey_8 }} />
                    </div>
                   
                </div>
                <div style={pageStyle.line}>
                     {null}
                    </div>
                <div style={pageStyle.singleRow} onClick={()=>{this.props.history.push("/privacyPolicy")}}>
                     <img src={crictecIcon} alt="policy" style={pageStyle.imageRow} width="25px" height="25px" />
                    <div style={pageStyle.textRow}>
                        Privacy Policy
                    </div>
                    <div style={pageStyle.arrow}>
                        <ChevronRight style={{ fontSize: 24, color: grey_8 }} />
                    </div>
                   
                </div>
                <div style={pageStyle.line}>
                     {null}
                    </div>
                <div style={pageStyle.singleRow} onClick={()=>{this.props.history.push("/cookiesPolicy")}}>
                    <img src={crictecIcon} alt="policy" style={pageStyle.imageRow} width="25px" height="25px" />
                    <div style={pageStyle.textRow}>
                        Cookies Policy
                    </div>
                    <div style={pageStyle.arrow}>
                        <ChevronRight style={{ fontSize: 24, color: grey_8 }} />
                    </div>
                    
                </div>
                <div style={pageStyle.line}>
                     {null}
                    </div>
            </div>
            </div>
        );
    }
}

const pageStyle={
    line:{
        display:"flex",
        // flex:1,
        height:'1px',
        background: "linear-gradient(to right, #ffffff, #e3e4e6, #e3e4e6, #ffffff)"
    },
    conatiner:{
        display:'flex',
        flex:1,
        flexDirection:'column',
        // width:screenWidth*0.911,
        marginLeft:screenWidth*0.0411,
        marginRight:screenWidth*0.0411,
        // paddingBottom:'20px',
        backgroundColor:'#fff',
        fontFamily:'Montserrat',
        color:'rgb(20, 27, 47)',
        fontWeight: 700,
        borderRadius: 3,
    },
    singleRow:{
        display:'flex',
        flexDirection:'row',
        paddingLeft:'16px',
        paddingTop:'10px',
        paddingBottom:'10px'
    },
    imageRow:{
        display:'flex',
        flex:0.05,
        justifyContent:'center',
        borderRadius:'50%'

    },
    textRow:{
        display:"flex",
        flex:0.90,
        paddingLeft:'20px',
        justifyContent:'flex-start',
        fontSize:'14px',
        fontWeight:'500',
    },
    arrow:{
        display:'flex',
        flex:0.10,
        paddingRight:"15px"

    }
}