import React, { Component } from "react";
import Slider from "react-slick";
import Header from "./../../commonComponent/Header";
import { grey_10, white } from "../../../styleSheet/globalStyle/color";
import { screenHeight } from "../../../styleSheet/screenSize/ScreenDetails";

export class PhotoView extends Component {
  render() {
    const { title, data } = this.props;
    const settings = {
      customPaging: i => {
        return (
          // <a>
          //   <img src={`${img}`} />
          // </a>
          <div
            style={{
              height: "52px",
              width: "52px",
              background: `url(${data[i].path})`,
              backgroundPosition: "top center",
              backgroundSize: "cover"
            }}
          />
        );
      },
      dots: true,
      dotsClass: "slick-dots slick-thumb",
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true
    };
    return (
      <div
        style={{
          zIndex: "999",
          position: "fixed",
          top: 0
        }}
      >
        <Header
          title={title}
          isHeaderBackground={true}
          titleFont={16}
          leftArrowBack
          textColor={white}
          backgroundColor={grey_10}
          history={this.props.history}
          leftIconOnClick={() => {
            this.props.toggleView(false);
          }}
        />
        <div
          style={{
            display: "flex",
            width: "100vw",
            height: screenHeight * 0.93,
            background: grey_10,
            position: "fixed"
          }}
        >
          <div
            style={{
              width: "100vw",
              marginTop: "20vh",
              overflow: "false"
            }}
          >
            <Slider {...settings}>
              {data &&
                data.map(img => (
                  <div>
                    <div
                      // src={img}
                      style={{
                        background: `url(${img.path})`,
                        backgroundSize: "cover",
                        backgroundPosition: "top center",
                        height: "300px",
                        backgroundRepeat: "no-repeat"
                      }}
                    />
                    {/* <img
                      src={img.path}
                      width="100%"
                      style={{ objectFit: "cover" }}
                    /> */}
                    <p
                      style={{
                        color: white,
                        fontFamily: "Mont400",
                        fontSize: "14px",
                        padding: "8px"
                      }}
                    >
                      {img.shortDescription}
                    </p>
                  </div>
                ))}
            </Slider>
            <div
              style={{ backgroundColor: grey_10, height: screenHeight * 0.4 }}
            >
              {null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PhotoView;
