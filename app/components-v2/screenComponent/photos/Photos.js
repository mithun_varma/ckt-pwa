import React, { Component } from "react";
import Header from "../../commonComponent/Header";
import { black } from "../../../styleSheet/globalStyle/color";
import PhotoImageConatiner from "./PhotoImageConatiner";
import { Helmet } from "react-helmet";

export class Photos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      isHeaderBackground: false,
      titlePlaced: false
    };
  }
  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = event => {
    const scrollTop = event.target.scrollingElement.scrollTop;
    if (scrollTop > 21 && !this.state.titlePlaced) {
      this.setState({
        // title: "Records",
        title: "Photo Gallery",
        titlePlaced: true,
        isHeaderBackground: true
      });
    } else if (scrollTop < 22) {
      this.setState({
        title: "",
        titlePlaced: false,
        isHeaderBackground: false
      });
    }
  };
  render() {
    const { history, photos } = this.props;
    return (
      <div>
        <div>
          <Helmet titleTemplate="%s | cricket.com">
            <title>
              Cricket Match Photo Gallery | Match Turning Point Images{" "}
            </title>
            <meta
              name="description"
              content="View Cricket Match Photo Galleries from the latest matches. See the best moments captured on the field."
            />
            {/* <meta
            name="keywords"
            content={(article.articleDetails && article.articleDetails.seoKeywords) || ''}
          /> */}
            <link
              rel="canonical"
              href={`www.cricket.com${history &&
                history.location &&
                history.location.pathname}`}
            />
          </Helmet>
          <Header
            title={"Photo Gallery"}
            isHeaderBackground={this.state.isHeaderBackground}
            leftArrowBack
            textColor={black}
            backgroundColor={"#edeff4"}
            history={this.props.history}
            isHeaderBackground={true}
            leftIconOnClick={() => {
              this.props.history.goBack();
            }}
          />
          <div
            style={{
              paddingTop: 42,
              fontFamily: "Montserrat",
              fontWeight: 700,
              fontSize: 22,
              color: black,
              // minHeight: "90px",
              paddingLeft: "16px",
              textTransform: "capitalize",
              background: "#edeff4"
            }}
          >
            {null}
          </div>
        </div>
        <div style={{ marginTop: 0 }}>
          {photos.map(data => {
            return <PhotoImageConatiner imageData={data} />;
          })}
        </div>
      </div>
    );
  }
}
// const pageStyle = {
//   headerStyle: {
//     display: "flex",
//     marginRight: screenWidth * 0.044,
//     fontWeight: "bold",
//     fontSize: screenWidth * 0.061,
//     marginLeft: screenWidth * 0.044,
//     fontFamily: "Montserrat-Bold",
//     color: "#141b2f"
//   }
// };
export default Photos;
