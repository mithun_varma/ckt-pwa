/* eslint-disable global-require */
/**
 *
 * Scoreboard
 *
 */

/* eslint-disable indent */
/* eslint no-unused-vars: 0 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint-disable no-nested-ternary */

import React from "react";
import PropTypes from "prop-types";
import ArticleCard from "../../Articles/ArticleCard";
import Loader from "../../commonComponent/Loader";
import EmptyState from "../../commonComponent/EmptyState";
import HrLine from "../../commonComponent/HrLine";
// import animate from '../../utils/animationConfig';

class FeaturedArticles extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = () => {
    this.props.fetchFeaturedArticles({ matchId: this.props.matchId });
    // this.props.fetchFeaturedArticles({});
  };
  cardOnClick = path => {
    this.props.history.push(path);
  };

  render() {
    const { articles, loading, articleHost } = this.props;
    return (
      <div>
        {loading ? (
          <Loader styles={{ height: "140px" }} noOfLoaders={2} />
        ) : articles && articles.length > 0 ? (
          articles.map(article => (
            <div>
              <ArticleCard
                article={article}
                articleHost={articleHost}
                cardType="news"
                cardOnClick={this.cardOnClick}
              />
              <HrLine />
            </div>
          ))
        ) : (
          <EmptyState msg="No articles found" />
        )}
      </div>
    );
  }
}

FeaturedArticles.propTypes = {
  fetchFeaturedArticles: PropTypes.func,
  articles: PropTypes.array,
  matchId: PropTypes.string,
  history: PropTypes.object,
  loading: PropTypes.bool,
  articleHost: PropTypes.string
};

export default FeaturedArticles;
