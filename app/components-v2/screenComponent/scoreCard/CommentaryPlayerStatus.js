import React, { Component } from "react";
import { gradientGrey, grey_10 } from "../../../styleSheet/globalStyle/color";

export class CommentaryPlayerStatus extends Component {
  render() {
    const { playerStats, background, players, isMomentum } = this.props;
    // console.log("DAta: ", this.props);
    return (
      <div
        style={{
          padding: "6px 16px",
          background: background || gradientGrey
        }}
      >
        {/* {[1, 2].map(item => ( */}
        {/* ========================= */}
        <div
          style={{
            display: "flex",
            flex: 1
          }}
        >
          <div
            style={{
              display: "flex",
              // width: "60%",
              flex: 0.56,
              fontSize: 10,
              color: grey_10,
              padding: "4px 0",
              overflow: "overlay"
            }}
          >
            <div
              style={{
                fontFamily: "Montserrat",
                fontWeight: 400,
                flex: 0.7,
                textOverflow: "ellipsis",
                whiteSpace: "nowrap",
                overflow: "hidden",
                fontSize: 11
              }}
            >
              {isMomentum
                ? playerStats.batsman && playerStats.batsman.name
                : playerStats.batsman &&
                  playerStats.batsman.key &&
                  players &&
                  players[playerStats.batsman.key]
                  ? // ? playerStats.batsman.name
                    players[playerStats.batsman.key].name + "*"
                  : "-"}
              {/* {!isMomentum &&
              playerStats.batsman &&
              playerStats.batsman.key &&
              players &&
              players[playerStats.batsman.key]
                ? // ? playerStats.batsman.name
                  players[playerStats.batsman.key].name + "*"
                : "-"} */}
            </div>
            <div
              style={{
                flex: 0.3,
                fontFamily: "Montserrat",
                fontWeight: 600,
                textAlign: "right",
                marginRight: 6
              }}
            >
              {`${playerStats.batsman ? playerStats.batsman.runs : ""} ${
                playerStats.batsman &&
                (playerStats.batsman.ballCount ||
                  playerStats.batsman.ballCount == 0)
                  ? "(" + playerStats.batsman.ballCount + ")"
                  : ""
              }`}
              {/* 777 (400 balls) */}
            </div>
          </div>

          <div
            style={{
              display: "flex",
              // width: "60%",
              flex: 0.44,
              fontSize: 10,
              color: grey_10,
              padding: "4px 0",
              overflow: "overlay"
            }}
          >
            <div
              style={{
                fontFamily: "Montserrat",
                fontWeight: 400,
                flex: 0.6,
                textOverflow: "ellipsis",
                whiteSpace: "nowrap",
                overflow: "hidden",
                fontSize: 11
              }}
            >
              {isMomentum
                ? playerStats.bowlerView && playerStats.bowlerView.name
                : playerStats.bowlerView &&
                  playerStats.bowlerView.key &&
                  players &&
                  players[playerStats.bowlerView.key]
                  ? // ? playerStats.bowlerView.name
                    players[playerStats.bowlerView.key].name
                  : "-"}
            </div>
            <div
              style={{
                flex: 0.4,
                fontFamily: "Montserrat",
                fontWeight: 600,
                textAlign: "right"
              }}
            >
              {`${
                playerStats.bowlerView &&
                (playerStats.bowlerView.wickets ||
                  playerStats.bowlerView.wickets == 0)
                  ? playerStats.bowlerView.wickets
                  : 0
              }/${
                playerStats.bowlerView && playerStats.bowlerView.runs
                  ? playerStats.bowlerView.runs
                  : 0
              }`}
            </div>
          </div>
        </div>
        {/* ========================= */}
        {/* ))} */}

        {/* =========================== */}

        <div
          style={{
            display: "flex",
            flex: 1
          }}
        >
          <div
            style={{
              display: "flex",
              // width: "60%",
              flex: 0.56,
              fontSize: 10,
              color: grey_10,
              padding: "4px 0",
              overflow: "overlay"
            }}
          >
            <div
              style={{
                fontFamily: "Montserrat",
                fontWeight: 400,
                flex: 0.7,
                textOverflow: "ellipsis",
                whiteSpace: "nowrap",
                overflow: "hidden",
                fontSize: 11
              }}
            >
              {isMomentum
                ? playerStats.nonBatsman && playerStats.nonBatsman.name
                : playerStats.nonBatsman &&
                  playerStats.nonBatsman.key &&
                  players &&
                  players[playerStats.nonBatsman.key]
                  ? // ? playerStats.nonBatsman.name
                    players[playerStats.nonBatsman.key].name
                  : "-"}
            </div>
            <div
              style={{
                flex: 0.4,
                fontFamily: "Montserrat",
                fontWeight: 600,
                textAlign: "center",
                marginRight: 6,
                textAlign: "right"
              }}
            >
              {`${playerStats.nonBatsman ? playerStats.nonBatsman.runs : ""} ${
                playerStats.nonBatsman &&
                (playerStats.nonBatsman.ballCount ||
                  playerStats.nonBatsman.ballCount == 0)
                  ? "(" + playerStats.nonBatsman.ballCount + ")"
                  : ""
              }`}
              {/* 777 (400 balls) */}
            </div>
          </div>

          <div
            style={{
              display: "flex",
              // width: "60%",
              flex: 0.44,
              fontSize: 10,
              color: grey_10,
              padding: "4px 0",
              overflow: "overlay"
            }}
          >
            <div
              style={{
                fontFamily: "Montserrat",
                fontWeight: 400,
                flex: 0.6,
                textOverflow: "ellipsis",
                whiteSpace: "nowrap",
                overflow: "hidden",
                fontSize: 11
              }}
            >
              {/* Runs Scored */}
              {this.props.teamName}
            </div>
            <div
              style={{
                flex: 0.4,
                fontFamily: "Montserrat",
                fontWeight: 600,
                textAlign: "right"
              }}
            >
              {`${
                playerStats && playerStats.teamRuns ? playerStats.teamRuns : "0"
              }/`}
              {`${
                playerStats && playerStats.teamWickets
                  ? playerStats.teamWickets
                  : "0"
              }`}
              {/* 77/8 */}
            </div>
          </div>
        </div>

        {/* =========================== */}
      </div>
    );
  }
}

export default CommentaryPlayerStatus;
