import React, { Component } from "react";
import {
  white,
  gradientGrey,
  grey_8,
  grey_10,
  grey_6
} from "../../../styleSheet/globalStyle/color";
import LiveScoreBoardList from "./LiveScoreBoardList";
import HrLine from "../../commonComponent/HrLine";

export class LiveScore extends Component {
  componentDidMount() {}
  getBallColor(ball) {
    switch (ball) {
      case "6":
        return "sixball";
      case "4":
        return "fourball";
      case "w":
        return "wicketball";
      default:
        return "";
    }
  }
  /* eslint-disable-next-line consistent-return */
  strikeDetails = (score, playerType) => {
    if (playerType === "bowler" && score.now && score.now.bowler) {
      const player = score.now["bowler"];
      const details = score.players[player];
      if (!details) return null;
      // const { bowling } = details.innings[score.now.innings];
      const bowling =
        details.innings &&
        details.innings[score.now.innings] &&
        details.innings[score.now.innings].bowling;
      return (
        bowling && (
          <React.Fragment key={player}>
            <LiveScoreBoardList
              history={this.props.history}
              name={details.name}
              playerId={details.key}
              runs={bowling.overStr || 0}
              balls={bowling.maidenOvers || 0}
              fours={bowling.runs || 0}
              sixes={bowling.wickets || 0}
              strikeRate={bowling.economy || 0}
              rootStyles={{
                background: white
              }}
              nameStyles={{
                fontSize: 11,
                fontWeight: 600
              }}
            />
          </React.Fragment>
        )
      );
    } else if (playerType === "batsmen") {
      /* eslint-disable-next-line consistent-return */
      /* eslint-disable-next-line array-callback-return */
      const data = ["striker", "nonStriker"].map((key, index) => {
        if (!score.now || !score.now[key]) return null;
        const player = score.now[key];
        const details = score.players[player];
        if (!details) return null;
        const batting =
          details.innings &&
          details.innings[score.now.innings] &&
          details.innings[score.now.innings].batting;
        return (
          batting && (
            <React.Fragment key={player}>
              {/* <tr key={`${index + 1}`}>
              <td>{`${details.name} ${key === 'striker' ? '*' : ''}`}</td>
              <td>{batting.runs || 0}</td>
              <td>{batting.balls || 0}</td>
              <td>{batting.fours || 0}</td>
              <td>{batting.sixes || 0}</td>
              <td>{batting.strikeRate || 0}</td>
            </tr> */}
              <LiveScoreBoardList
                history={this.props.history}
                // name={`${details.name + ((score.teams && score.now && score.now.teamKey && score.teams[score.now.teamKey])  ? details.key != score.teams[score.now.teamKey].captain ? " (c)" : details.key == score.teams[score.now.teamKey].keeper ? " (wk)" : "" : "")}${key === "striker" ? "*" : ""}`}
                name={`${details.name}${key === "striker" ? "*" : ""}`}
                playerId={details.key}
                runs={batting.runs || 0}
                balls={batting.balls || 0}
                fours={batting.fours || 0}
                sixes={batting.sixes || 0}
                strikeRate={batting.strikeRate || 0}
                rootStyles={{
                  background: white
                }}
                nameStyles={{
                  fontSize: 11,
                  fontWeight: 600
                }}
              />
            </React.Fragment>
          )
        );
      });
      return data;
    }
  };
  overs = (balls, index) => {
    if (balls) {
      let val = 0;
      const over = balls;
      const ballArray = balls
        .split(/(,|;)/)
        .filter(Ball => (Ball === "," || Ball === ";" ? null : Ball));
      ballArray.map(ball => {
        if (ball[0] === "e" || ball[0] === "r" || ball[0] === "b")
          val += Number(ball[1]);
        return null;
      });
      if (over.length <= 2) {
        return (
          <li
            key={index}
            className={`overNumber ${
              over[0] === "r" || over[0] === "b"
                ? this.getBallColor(over[1])
                : this.getBallColor(over)
            }`}
          >
            {over[0] === "r" || over[0] === "b" ? over[1] : over}
          </li>
        );
      }
      return (
        <li
          key={index}
          className={`overNumber ${this.getBallColor(
            ballArray[ballArray.length - 1] === "by"
              ? "b"
              : ballArray[ballArray.length - 1]
          )}`}
        >
          {`${val === 0 || val === 1 ? "" : val}${
            ballArray[ballArray.length - 1] === "by"
              ? "b"
              : ballArray[ballArray.length - 1]
          }`}
        </li>
      );
    }
    return null;
  };

  getPartnership = score => {
    let inningId = score && score.now && score.now.inningId;
    let teamKey = score && score.now && score.now.teamKey;
    let partnership =
      score.innings &&
      score.innings[`${teamKey}_${inningId}`] &&
      score.innings[`${teamKey}_${inningId}`].partnership;
    let valueStr = "";
    if (
      partnership &&
      (partnership.runs || partnership.runs == 0) &&
      (partnership.balls || partnership.balls == 0)
    ) {
      valueStr = `${partnership.runs}(${partnership.balls})`;
      return valueStr;
    }
    return "-";
  };

  getLastWicket = score => {
    let inningId = score && score.now && score.now.inningId;
    let teamKey = score && score.now && score.now.teamKey;
    let players = score && score.players;
    let lastWicket =
      score.innings &&
      score.innings[`${teamKey}_${inningId}`] &&
      // score.innings[`${teamKey}_${inningId}`].partnership;
      score.innings[`${teamKey}_${inningId}`].lastWicket;
    let valueStr = "";
    let playerKey = (lastWicket && lastWicket) || "";

    if (playerKey && players) {
      valueStr =
        players[playerKey] &&
        players[playerKey].innings &&
        players[playerKey].innings[inningId] &&
        players[playerKey].innings[inningId].batting
          ? `${players[playerKey].name} ${
              players[playerKey].innings[inningId].batting.runs
            } (${players[playerKey].innings[inningId].batting.balls}) `
          : "";
      return valueStr;
    }
    return "";
  };

  render() {
    const { score } = this.props;
    if (!(score && score.now)) {
      return <div />;
    }
    const strikeBatsmen = score && this.strikeDetails(score, "batsmen");
    const strikeBowler = score && this.strikeDetails(score, "bowler");
    return (
      <div>
        <LiveScoreBoardList
          name="BATSMEN"
          runs="R"
          balls="B"
          fours="4s"
          sixes="6s"
          strikeRate="SR"
        />
        {/* <LiveScoreBoardList value={["BATSMEN", "R", "B", "4S", "6S", "SR"]} /> */}
        <HrLine />
        {strikeBatsmen}
        <HrLine />
        <LiveScoreBoardList
          name="BOWLER"
          runs="O"
          balls="M"
          fours="R"
          sixes="W"
          strikeRate="ER"
        />
        {strikeBowler}
        <div
          style={{
            display: "flex",
            flex: 1,
            justifyContent: "space-between",
            padding: "12px 16px",
            background: gradientGrey,
            fontFamily: "Montserrat",
            fontSize: 11,
            color: grey_10,
            position: "relative"
          }}
        >
          <div
            style={{
              flex:
                score.now &&
                score.now.req &&
                score.now.req.requiredRunRate != null
                  ? 0.33
                  : 0.5
            }}
          >
            <span style={{ color: grey_8 }}>P’ship: </span>
            <span>{this.getPartnership(score)}</span>
          </div>
          {/* No need of Last wicket */}
          {false &&
            this.getLastWicket(score) && (
              <div>
                <span style={{ color: grey_8 }}>Last wicket: </span>
                <span>{this.getLastWicket(score)}</span>
              </div>
            )}
          {/* Current Run Rate */}
          <div
            style={{
              flex:
                score.now &&
                score.now.req &&
                score.now.req.requiredRunRate != null
                  ? 0.33
                  : 0.5,
              textAlign:
                score.now &&
                score.now.req &&
                score.now.req.requiredRunRate != null
                  ? "center"
                  : "right"
            }}
          >
            <span style={{ color: grey_8 }}>RR: </span>
            <span>{(score.now && score.now.runRate) || 0}</span>
          </div>
          {/* Required Rate */}
          {score.now &&
            score.format != "test" &&
            score.now.req &&
            score.now.req.requiredRunRate != null && (
              <div style={{ flex: 0.33, textAlign: "right" }}>
                <span style={{ color: grey_8 }}>REQ RR: </span>
                <span>{score.now.req.requiredRunRate || 0}</span>
              </div>
            )}

          {/* Line Separator */}
          {score.now &&
            score.now.req &&
            score.now.req.requiredRunRate != null && (
              <div
                style={{
                  position: "absolute",
                  left: "34%",
                  top: "34%",
                  width: 1,
                  height: 14,
                  background: grey_6
                }}
              />
            )}
          {score.now &&
            score.now.req &&
            score.now.req.requiredRunRate != null && (
              <div
                style={{
                  position: "absolute",
                  left: "66%",
                  top: "34%",
                  width: 1,
                  height: 14,
                  background: grey_6
                }}
              />
            )}
          <div />
        </div>
      </div>
    );
  }
}

export default LiveScore;
