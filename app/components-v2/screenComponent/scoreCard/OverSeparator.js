import React, { Component } from "react";
import OverCard from "../../commonComponent/OverCard";
import BallCard from "../../commonComponent/BallCard";
import HrLine from "../../commonComponent/HrLine";

export class OverSeparator extends Component {
  render() {
    const { overs } = this.props;
    return (
      overs.length > 0 && (
        <div>
          <div
            style={{
              display: "flex",
              flex: 1,
              padding: "8px 16px",
              // background: gradientGrey
              background: "linear-gradient(to left, #ffffff, #f8f8f9 18%, #f8f8f9 85%, #ffffff)"
            }}
          >
            <div
              style={{
                flex: 0.14,
                display: "flex",
                justifyContent: "flex-end",
                alignItems: "center"
              }}
            >
              <OverCard
                data={{
                  title: overs[0] ? overs[0].overNo : "-",
                  caption: "over"
                }}
              />
            </div>
            <div
              style={{
                flex: 0.86,
                // width: "86%",
                display: "flex",
                overflow: "scroll",
                padding: "0 8px"
              }}
            >
              {/* <BallCard balls={[0, 1, 2, "W", 4, 6]} /> */}
              <BallCard balls={overs} />
            </div>
          </div>
          <HrLine />
        </div>
      )
    );
  }
}

export default OverSeparator;
