import React, {Component} from 'react';
import {screenWidth, screenHeight} from './../../../styleSheet/screenSize/ScreenDetails';
import HrLine from '../../commonComponent/HrLine';

export default class BallByBall extends Component{
    constructor(props){
        super(props);

    }
    componentDidMount(){


    }
    render(){
        const values = this.props.commentaryData.values;
        return(
            <div style={pageStyle.ballByBallContainer}>
            {
                values.map((data)=>{
                    const run = data.runs;
                    const overString = data.overString;
                    const comment =data.comment;
                    return(
                        <div>
                            {this.commentaryConatiner(run,overString,comment)}
                            <HrLine />
                        </div>
                    );
                })

            }
            </div>
        );
    }
    commentaryConatiner(run, over, commentary){
        return(
            <div style={pageStyle.commentaryContainer}>
                <div style={pageStyle.commentaryValueField}>
                    {this._handelValue(run,over)}
                </div>
                <div style={pageStyle.commentaryField} dangerouslySetInnerHTML={{__html:commentary}}/>
          
            </div>
        );
    }

    _handelValue(run, over){
        if(run == '0'){
            return(
                <div style={pageStyle._handelConatiner}>
                    {this.boxDyanamicNoRun(over)}
                </div>
            );

        }
        else if(run == '1'){
            return(
                <div>
                    {this.boxDyanamicOne(over)}
                </div>
            );

        }
        else if (run == '2'){
            return(
                <div>
                    {this.boxDyanamicTwo(over)}
                </div>
            );

        }
        else if (run== '3'){
            return(
                <div>
                    {this.boxDyanamicThree(over)}
                </div>

            );

        }
        else if ( run == '4'){
            return(
                <div>
                    {this.boxDyanamicFour(over)}
                </div>
            );

        }
        else if(run == '5'){
            return(
                <div>
                    {null}
                </div>
            );

        }
        else if ( run == '6'){
            return(
                <div>
                    {this.boxDyanamicSix(over)}
                </div>
            );

        }
        else if (run == 'W'){
            return(
                <div>
                    {this.boxDyanamicWicket(over)}
                </div>
            );

        }
    }

    boxDyanamicNoRun(over){
        return(
            <div style={pageStyle.boxContainerFour}>
               <div style={pageStyle.topHalf}>
                    {"0"}
                </div>
                <div style={{display:'flex',width:screenWidth*0.08,marginLeft:screenWidth*0.018,backgroundColor:'#fff',height:1}}>
                {null}
                </div>
                <div style={pageStyle.lowerHalf}>
                    {over}
                </div>
            </div>
        );
    }


    boxDyanamicFour(over){
        return(
            <div style={pageStyle.boxContainerFour}>
               <div style={pageStyle.topHalf}>
                    {"4"}
                </div>
                <div style={{display:'flex',width:screenWidth*0.08,marginLeft:screenWidth*0.018,backgroundColor:'#fff',height:1}}>
                {null}
                </div>
                <div style={pageStyle.lowerHalf}>
                    {over}
                </div>
            </div>
        );
    }
    boxDyanamicSix(over){
        return(
            <div style={pageStyle.boxContainerSix}>
                <div style={pageStyle.topHalf}>
                    {"6"}
                </div>
                <div style={{display:'flex',width:screenWidth*0.08,marginLeft:screenWidth*0.018,backgroundColor:'#fff',height:1}}>
                {null}
                </div>
                <div style={pageStyle.lowerHalf}>
                    {over}
                </div>
            </div>
        );
    }
    boxDyanamicWicket(over){
        return(
            <div style={pageStyle.boxContainerWicket}>
                <div style={pageStyle.topHalf}>
                    {"W"}
                </div>
                <div style={{display:'flex',width:screenWidth*0.08,marginLeft:screenWidth*0.018,backgroundColor:'#fff',height:1}}>
                {null}
                </div>
                <div style={pageStyle.lowerHalf}>
                    {over}
                </div>
            </div>
        );
    }
    boxDyanamicOne(over){
        return(
            <div style={pageStyle.boxContainerWicket}>
                <div style={pageStyle.topHalf}>
                    {"1"}
                </div>
                <div style={{display:'flex',width:screenWidth*0.08,marginLeft:screenWidth*0.018,backgroundColor:'#fff',height:1}}>
                {null}
                </div>
                <div style={pageStyle.lowerHalf}>
                    {over}
                </div>
            </div>
        );
    }
    boxDyanamicTwo(over){
        return(
            <div style={pageStyle.boxContainerWicket}>
                <div style={pageStyle.topHalf}>
                    {"2"}
                </div>
                <div style={{display:'flex',width:screenWidth*0.08,marginLeft:screenWidth*0.018,backgroundColor:'#fff',height:1}}>
                {null}
                </div>
                <div style={pageStyle.lowerHalf}>
                    {over}
                </div>
            </div>
        );
    }
    boxDyanamicThree(over){
        return(
            <div style={pageStyle.boxContainerWicket}>
                <div style={pageStyle.topHalf}>
                    {"3"}
                </div>
                <div style={{display:'flex',width:screenWidth*0.08,marginLeft:screenWidth*0.018,backgroundColor:'#fff',height:1}}>
                {null}
                </div>
                <div style={pageStyle.lowerHalf}>
                    {over}
                </div>
            </div>
        );
    }
    boxDyanamicOver(over){
        return(
            <div style={pageStyle.boxContainerWicket}>
                <div style={pageStyle.topHalf}>
                    {over}
                </div>
                <div style={{display:'flex',width:screenWidth*0.08,marginLeft:screenWidth*0.018,backgroundColor:'#fff',height:1}}>
                {null}
                </div>
                <div style={pageStyle.lowerHalf}>
                    {'over'}
                </div>
            </div>
        );

    }
    circularRun(over){
        return(
        <div>
        </div>
        )
    }
    circularSix(){
        return(
            <div>

            </div>
        );

    }
    circularWicket(){
        return(
            <div style={pageStyle.circularConatiner}>
                <div style={pageStyle.circularRunConatiner}>
                    1
                </div>
                <div style={pageStyle.circularOverConatiner}>
                    {Over}
                </div>

            </div>
        );
    }

    

}
const pageStyle={
    commentaryValueField:{
        display:'flex',
        flex:0.20,
        justifyContent:'flex-start',
    },
    commentaryField:{
        display:'flex',
        flex:0.70,
        justifyContent:'flex-start',
        wrap:'flex-wrap',
        color:'#4a4a4a',
        fontSize:'12px',

    },
    ballByBallContainer:{
        display:'flex',
        flexDirection:'column',
        backgroundColor:'#fff',
        paddingLeft:'16px',


    },
    commentaryContainer:{
        display:'flex',
        flex:1,
        height:screenHeight*0.1105,
        width:screenWidth*0.911,
        alignItems:'center',
    },

    circularRunConatiner:{
        display:'flex',
        flex:0.50,
        border:'1px solid #9b9b9b',
        borderRadius:'14px',
        justifyContent:'center',
        alignItems:'center',
    },
    circularOverConatiner:{
        display:'flex',
        flex:0.50,
        justifyContent:'center',
        alignItems:'center',
        marginTop:'9px',

    },
    circularConatiner:{
        display:'flex',
        flexDirection:'column',
        height:screenHeight*0.115,
        width:screenWidth*0.07,
        flex:1,
        alignItems:'center',

    },
    topHalf:{
        display:'flex',
        paddingTop:'3px',
        paddingBottom:'3px',
        paddingLeft:'5px',
        paddingRight:'5px',
        justifyContent:'center',
        fontSize:'21px',
    },
    lowerHalf:{
        display:'flex',
        paddingBottom:'2px',
        paddingTop:'2px',
        paddingRight:'5px',
        paddingLeft:'5px',
        justifyContent:'center',
        fontSize:'10px'
    },
    boxContainerFour:{
        display:'flex',
        flexDirection:'column',
        flex:1,
        borderRadius:'3px',
        boxShadow:'0px 3px 6px 0px #13000000',
        backgroundColor:'#35a863',
        color:'#fff',
        width:screenWidth*0.116,
    },
    boxContainerSix:{
        display:'flex',
        flexDirection:'column',
        flex:1,
        borderRadius:'3px',
        boxShadow:'0px 3px 6px 0px #13000000',
        backgroundColor:'#4a90e2',
        color:'#fff',
        width:screenWidth*0.116,
    },
    boxContainerWicket:{
        display:'flex',
        flexDirection:'column',
        flex:1,
        borderRadius:'3px',
        boxShadow:'0px 3px 6px 0px #13000000',
        backgroundColor:'#d64b4b',
        color:'#fff',
        width:screenWidth*0.116,
    },

}