import React, { Component} from 'react';
import BallByBall from './ballByBall';
import {screenWidth, screenHeight} from './../../../styleSheet/screenSize/ScreenDetails';
import { gradientGrey } from '../../../styleSheet/globalStyle/color';


export default class Live extends Component{
    constructor(props){
        super(props);
        this.state={
            activeButton:'BALL BY BALL',
        }
        this._handelActionButton = this._handelActionButton.bind(this);

    }
    componentDidMount(){

    }
    render(){
        return(
            <div>
                {this.liveScoreCard()}
            </div>
        );
    }
    liveScoreCard(){
        const BattingData = this.props.BattingData;
        return(
            <div>
                <div>
                    {this.lastRowPatnership()}
                </div>
                <div>
                    {this.actionBar()}
                </div>
                <div>
                    {
                        (this.state.activeButton) == 'BALL BY BALL'?
                        <div>
                            {this.BallByBall()}
                        </div>
                        :
                        <div>
                            {this.overByOver()}
                        </div>
                    }
                </div>
            </div>
        );
    }
    lastRowPatnership(){
        const patnership = this.props.patnerShip;
        const lastWicket= this.props.lastWicket['battingStatistics'][1];
        const run = this.props.lastWicket['battingStatistics'][1].runs;
        const ball = this.props.lastWicket['battingStatistics'][1].balls;
        const name =this.props.lastWicket.playerName;
        return(
            <div style={pageStyle.lastRowConatiner}>
                <div style={pageStyle.lastRowField}>
                    <div style={{color:'#727682'}}>
                       { "P'SHIP : "}
                    </div> {patnership}
                </div>
                <div style={pageStyle.lastRowField}>
                    <div style={{color:'#727682'}}>
                       { "LAST WKT : "}
                       </div>
                    {" "+name+" "+run+"("+ball+')'}
                </div>

            </div>
        );
    }
    BallByBall(){
        return(
            <div>
                <BallByBall   liveData={this.props.liveData} commentaryData={this.props.commentaryData}/>
            </div>
        );

    }
    overByOver(){
        return(
            <div>
                {/* <OverByOver /> */}
            </div>
        );
    }
    actionBar(){
        var styleBall = "";
        var styleOver = "";
        if(this.state.activeButton == 'OVER BY OVER'){
            styleBall = pageStyle.unActiveButton;
            styleOver = pageStyle.activeButton;

        }
        else{
            styleBall = pageStyle.unActiveButton;
            styleOver = pageStyle.activeButton;

        }
        var styleBall = pageStyle.activeButton;
        var styleOver = pageStyle.unActiveButton;

        return(
            <div style={pageStyle.actionBarContainer}>
                <div style={styleBall} onClick={this._handelActionButton}>
                    BALL BY BALL
                </div>
                <div style={styleOver}  onClick={this._handelActionButton}> 
                    OVER BY OVER
                </div>
            </div>
        );
    }

    _handelActionButton(event){
        const activeButton=event.target.innerHTML;
        this.setState({activeButton});


    }

}
const pageStyle ={
    activeButton:{
        display:'flex',
        flex:0.27,
        color:'#f76b1c',
        fontSize:'10px',
        border:'1px solid #e0e1e4',
        padding:'8px 11px 8px 11px',
        

    },
    unActiveButton:{
        display:'flex',
        flex:0.27,
        color:'#a1a4ac',
        fontSize:'10px',
        border:'1px solid #e0e1e4',
        padding:'8px 11px 8px 11px',

    },
    actionBarContainer:{
        display:'flex',
        flexDirection:'row',
        flex:1,
        height:screenHeight*0.1109,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#fff',
        width:screenWidth*0.911,


    },
    lastRowConatiner:{
        display:'flex',
        flexDirection:'row',
        width:screenWidth*0.911,
        height:screenHeight*0.071,
        alignItems:'center',
        backgroundImage:gradientGrey,
        paddingLeft:screenWidth*0.044,
        flex:1,
    },
    lastRowField:{
        display:'flex',
        flex:0.50,
        justifyContent:'flex-start',
        alignItems:'center',
        fontSize:'10px',
        color:'#141b2f'



    }

}