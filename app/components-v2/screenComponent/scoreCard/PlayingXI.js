import React, { Component } from "react";
import { grey_10 } from "../../../styleSheet/globalStyle/color";
import HrLine from "../../commonComponent/HrLine";
import { Link } from "react-router-dom";

class PlayingXI extends Component {
  render() {
    const { score } = this.props;
    if ((!score && !score.players) || !score.teams || !score.battingOrder)
      return <div />;

    const teamKeys = Object.keys(score.teams);
    const firstTeam = score.firstBatting ? score.firstBatting : teamKeys[0];
    const secondTeam =
      teamKeys.indexOf(score.firstBatting) === 1 ? teamKeys[0] : teamKeys[1];
    return (
      <div
        style={{
          padding: 16,
          fontFamily: "Montserrat",
          fontSize: 14,
          color: grey_10
        }}
      >
        <div>
          <Link
            to={`/teams/${
              score.teams[firstTeam] ? score.teams[firstTeam].teamKey : ""
            }`}
            style={{
              color: grey_10
            }}
          >
            <span
              style={{
                fontWeight: 700
              }}
            >
              {score.teams[firstTeam] && score.teams[firstTeam].shortName}:{" "}
            </span>
          </Link>
          {score.teams[firstTeam] &&
            score.teams[firstTeam].probablePlayingXi &&
            score.teams[firstTeam].probablePlayingXi.map(
              (player, key) =>
                player && (
                  <Link
                    to={`/players/${player.key}`}
                    key={key}
                    style={{
                      textDecoration: "none",
                      color: grey_10
                    }}
                  >
                    <span
                      style={{
                        fontWeight: 400,
                        lineHeight: "24px"
                      }}
                    >
                      {player && player.name
                        ? player.name +
                          (player.key == score.teams[firstTeam].captain &&
                          player.key == score.teams[firstTeam].keeper
                            ? " (c) (wk)"
                            : player.key == score.teams[firstTeam].captain
                              ? " (c)"
                              : player.key == score.teams[firstTeam].keeper
                                ? " (wk)"
                                : "") +
                          (score.teams[firstTeam].probablePlayingXi.length - 1 >
                          key
                            ? ", "
                            : "")
                        : "-"}
                    </span>
                  </Link>
                )
            )}
        </div>
        <div
          style={{
            margin: "10px 0"
          }}
        >
          <HrLine />
        </div>
        <div>
          <Link
            to={`/teams/${
              score.teams[secondTeam] ? score.teams[secondTeam].teamKey : ""
            }`}
            style={{
              color: grey_10
            }}
          >
            <span
              style={{
                fontWeight: 700
              }}
            >
              {score.teams[secondTeam] && score.teams[secondTeam].shortName}:{" "}
            </span>
          </Link>
          {score.teams[secondTeam] &&
            score.teams[secondTeam].probablePlayingXi &&
            score.teams[secondTeam].probablePlayingXi.map(
              (player, key) =>
                player && (
                  <Link
                    to={`/players/${player.key}`}
                    key={key}
                    style={{
                      textDecoration: "none",
                      color: grey_10
                    }}
                  >
                    <span
                      style={{
                        fontWeight: 400,
                        lineHeight: "24px"
                      }}
                    >
                      {player && player.name
                        ? player.name +
                          (player.key == score.teams[secondTeam].captain &&
                            player.key == score.teams[secondTeam].keeper
                              ? " (c) (wk)"
                              : player.key == score.teams[secondTeam].captain
                            ? " (c)"
                            : player.key == score.teams[secondTeam].keeper
                              ? " (wk)"
                              : "") +
                          (score.teams[secondTeam].probablePlayingXi.length -
                            1 >
                          key
                            ? ", "
                            : "")
                        : "-"}
                    </span>
                  </Link>
                )
            )}
        </div>
      </div>
    );
  }
}

export default PlayingXI;
