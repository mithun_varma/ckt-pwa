import React, { Component } from "react";
import { Link } from "react-router-dom";
import CardGradientTitle from "../../commonComponent/CardGradientTitle";
import {
  white,
  grey_10,
  grey_8,
  blue_grey
} from "../../../styleSheet/globalStyle/color";
import PlayerListItems from "./PlayerListItems";

import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import moment from "moment";

export class MatchInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      teamA: false,
      teamB: false
    };
  }

  togglePlayingXI = type => {
    this.setState({
      [type]: !this.state[type]
    });
  };

  redirectTo = id => {
    this.props.history.push(`/grounds/${id}`);
  };

  render() {
    const { score, onlyPlayingXI } = this.props;
    // if ((!score && !score.players) || !score.teams || !score.battingOrder) return <div />;
    if ((!score && !score.players) || !score.teams || !score.battingOrder)
      return <div />;

    const teamKeys = Object.keys(score.teams);
    const firstTeam = score.firstBatting ? score.firstBatting : teamKeys[0];
    const secondTeam =
      teamKeys.indexOf(score.firstBatting) === 1 ? teamKeys[0] : teamKeys[1];
    // const activeTeam = this.state.activeTab ? secondTeam : firstTeam;
    return (
      <div>
        {!onlyPlayingXI && (
          <div>
            {/* <CardGradientTitle title="INFO" /> */}
            <div
              onClick={() => {
                this.props.history.push(
                  `/series/${score.season}/${score.seriesId}`
                );
              }}
            >
              <CardGradientTitle
                title={score.season || score.title}
                isRightIcon
              />
            </div>
            {score.format == "test" && (
              <div
                style={{
                  display: "flex",
                  flex: 1,
                  fontFamily: "Montserrat",
                  fontSize: 12,
                  color: grey_10,
                  padding: "10px 16px",
                  fontWeight: 400
                }}
              >
                <div style={{ flex: 0.4 }}>Date</div>
                <div style={{ flex: 0.6, fontWeight: 600 }}>{`${moment(
                  score.startDate.iso
                ).format("Do MMM")}${score.endDate.iso &&
                  " - " +
                    moment(score.endDate.iso).format("Do MMM, YYYY")}`}</div>
                {/* <div style={{ flex: 0.6 }}>Mon, Jan 07 - Thu, Jan 10</div> */}
              </div>
            )}
            {score.format == "test" && (
            <div
              style={{
                display: "flex",
                flex: 1,
                fontFamily: "Montserrat",
                fontSize: 12,
                color: grey_10,
                padding: "10px 16px",
                fontWeight: 400
              }}
            >
              <div style={{ flex: 0.4 }}>Time</div>
              <div style={{ flex: 0.6, fontWeight: 600 }}>
                {`${moment(score.startDate.iso).format(
                  "h:mm A "
                )} ${window
                  .moment()
                  .tz(window.moment.tz.guess(true))
                  .format("z")}`}
              </div>
            </div>
            )}
            {score.format != "test" && (
            <div
              style={{
                display: "flex",
                flex: 1,
                fontFamily: "Montserrat",
                fontSize: 12,
                color: grey_10,
                padding: "10px 16px",
                fontWeight: 400
              }}
            >
              <div style={{ flex: 0.4 }}>Date & Time</div>
              <div style={{ flex: 0.6, fontWeight: 600 }}>
                {`${moment(score.startDate.iso).format(
                  "Do MMM YYYY, h:mm A "
                )} ${window
                  .moment()
                  .tz(window.moment.tz.guess(true))
                  .format("z")}`}
              </div>
            </div>
            )}
            <div
              style={{
                display: "flex",
                flex: 1,
                fontFamily: "Montserrat",
                fontSize: 12,
                color: grey_10,
                padding: "10px 16px",
                fontWeight: 400
              }}
            >
              <div style={{ flex: 0.4 }}>Match No.</div>
              <div style={{ flex: 0.6, fontWeight: 600 }}>
                {score.matchDescription || "-"}
                {/* {`${score.totalMatches ? " / " + score.totalMatches : ""}`} */}
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flex: 1,
                fontFamily: "Montserrat",
                fontSize: 12,
                color: grey_10,
                padding: "10px 16px",
                fontWeight: 400
              }}
            >
              <div style={{ flex: 0.4 }}>Toss</div>
              <div style={{ flex: 0.6, fontWeight: 600 }}>
                {`${score.originalTossDecision ? score.originalTossDecision : "-"}`}
              </div>
            </div>
          </div>
        )}

        <div>
          {!onlyPlayingXI &&
            // score.status != "notstarted" && (
            score.firstBallStatus != "UPCOMING" && (
              <CardGradientTitle
                title={
                  score.teams &&
                  score.teams[firstTeam] &&
                  score.teams[firstTeam].playingElevenDeclared
                    ? "PLAYING XI"
                    : "PROBABLE PLAYING XI"
                }
              />
            )}
          {(onlyPlayingXI ||
            // score.status != "notstarted"
            score.firstBallStatus != "UPCOMING") && (
            <div onClick={() => this.togglePlayingXI("teamA")}>
              <PlayerListItems
                title={
                  score.teams[firstTeam] && score.teams[firstTeam].shortName
                }
                titleStyles={{
                  fontWeight: 700
                }}
                avatar={
                  score.teams[firstTeam] && score.teams[firstTeam].avatar
                    ? score.teams[firstTeam].avatar
                    : "https://via.placeholder.com/50"
                }
                rightElement={
                  this.state.teamA ? (
                    <ExpandLess style={{ color: blue_grey }} />
                  ) : (
                    <ExpandMore style={{ color: blue_grey }} />
                  )
                }
              />
            </div>
          )}
          {false &&
            this.state.teamA &&
            score.teams[firstTeam] &&
            score.teams[firstTeam].playingElevenDeclared &&
            score.teams[firstTeam].playingXi &&
            score.teams[firstTeam].playingXi.map(playerId => {
              const player = score.players[playerId];
              return (
                player && (
                  <PlayerListItems
                    title={player.name || "-"}
                    avatar={player.avatar || "https://via.placeholder.com/50"}
                    avatarStyles={{
                      backgroundColor: grey_10,
                      borderRadius: "50%",
                      width: 24,
                      height: 24
                    }}
                    rightElement={
                      // AllRounder Bowler Batsman Keeper
                      player.role &&
                      ["AllRounder", "Bowler", "Batsman", "Keeper"].indexOf(
                        player.role
                      ) > -1 && (
                        <img
                          // src={"https://via.placeholder.com/50"}
                          src={require(`../../../images/${player.role}.svg`)}
                          style={{
                            width: 20,
                            height: 20,
                            borderRadius: "50%"
                          }}
                        />
                      )
                    }
                  />
                )
              );
            })}
          {/* Probable Playing XI Section */}
          {this.state.teamA &&
            score.teams[firstTeam] &&
            // !score.teams[firstTeam].playingElevenDeclared &&
            score.teams[firstTeam].probablePlayingXi &&
            score.teams[firstTeam].probablePlayingXi.map(player => {
              // const player = score.players[playerId];
              return (
                player && (
                  <Link to={`/players/${player.key}`}>
                    <PlayerListItems
                      title={player && player.name ? player.name + ( (player.key == score.teams[firstTeam].captain && player.key == score.teams[firstTeam].keeper) ? " (c) (wk)" : player.key == score.teams[firstTeam].captain ? " (c)" : player.key == score.teams[firstTeam].keeper ? " (wk)" : "") : "-"}
                      avatar={
                        player && player.avatar
                          ? player.avatar
                          : "https://via.placeholder.com/50"
                      }
                      avatarStyles={{
                        backgroundColor: grey_10,
                        borderRadius: "50%",
                        width: 24,
                        height: 24
                      }}
                      rightElement={
                        // AllRounder Bowler Batsman Keeper
                        player &&
                        player.role &&
                        ["AllRounder", "Bowler", "Batsman", "Keeper"].indexOf(
                          player.role
                        ) > -1 && (
                          <img
                            // src={"https://via.placeholder.com/50"}
                            src={require(`../../../images/${player.role}.svg`)}
                            style={{
                              width: 20,
                              height: 20,
                              borderRadius: "50%"
                            }}
                          />
                        )
                      }
                    />
                  </Link>
                )
              );
            })}
          {/* Probable Playing XI Section */}

          {/* <HrLine /> */}
          {(onlyPlayingXI ||
            // score.status != "notstarted"
            score.firstBallStatus != "UPCOMING") && (
            <div onClick={() => this.togglePlayingXI("teamB")}>
              <PlayerListItems
                title={
                  score.teams[secondTeam] && score.teams[secondTeam].shortName
                }
                titleStyles={{
                  fontWeight: 700
                }}
                avatar={
                  score.teams[secondTeam] && score.teams[secondTeam].avatar
                    ? score.teams[secondTeam].avatar
                    : "https://via.placeholder.com/50"
                }
                rightElement={
                  this.state.teamB ? (
                    <ExpandLess style={{ color: blue_grey }} />
                  ) : (
                    <ExpandMore style={{ color: blue_grey }} />
                  )
                }
              />
            </div>
          )}
          {false &&
            this.state.teamB &&
            score.teams[secondTeam] &&
            score.teams[secondTeam].playingElevenDeclared &&
            score.teams[secondTeam].playingXi &&
            score.teams[secondTeam].playingXi.map(playerId => {
              const player = score.players[playerId];
              return (
                player && (
                  <PlayerListItems
                    title={player.name || "-"}
                    avatar={player.avatar || "https://via.placeholder.com/50"}
                    avatarStyles={{
                      backgroundColor: grey_10,
                      borderRadius: "50%",
                      width: 24,
                      height: 24
                    }}
                    rightElement={
                      // AllRounder Bowler Batsman Keeper
                      player.role &&
                      ["AllRounder", "Bowler", "Batsman", "Keeper"].indexOf(
                        player.role
                      ) > -1 && (
                        <img
                          // src={"https://via.placeholder.com/50"}
                          src={require(`../../../images/${player.role}.svg`)}
                          style={{
                            width: 20,
                            height: 20,
                            borderRadius: "50%"
                          }}
                        />
                      )
                    }
                  />
                )
              );
            })}
          {/* Probable Playing XI Section */}
          {this.state.teamB &&
            score.teams[secondTeam] &&
            // !score.teams[secondTeam].playingElevenDeclared &&
            score.teams[secondTeam].probablePlayingXi &&
            score.teams[secondTeam].probablePlayingXi.map(player => {
              // const player = score.players[playerId];
              return (
                player && (
                  <Link to={`/players/${player.key}`}>
                    <PlayerListItems
                      title={player && player.name ? player.name + ((player.key == score.teams[firstTeam].captain && player.key == score.teams[firstTeam].keeper) ? " (c) (wk)" :player.key == score.teams[secondTeam].captain ? " (c)" : player.key == score.teams[secondTeam].keeper ? " (wk)" : "") : "-"}
                      avatar={
                        player && player.avatar
                          ? player.avatar
                          : "https://via.placeholder.com/50"
                      }
                      avatarStyles={{
                        backgroundColor: grey_10,
                        borderRadius: "50%",
                        width: 24,
                        height: 24
                      }}
                      rightElement={
                        // AllRounder Bowler Batsman Keeper
                        player &&
                        player.role &&
                        ["AllRounder", "Bowler", "Batsman", "Keeper"].indexOf(
                          player.role
                        ) > -1 && (
                          <img
                            // src={"https://via.placeholder.com/50"}
                            src={require(`../../../images/${player.role}.svg`)}
                            style={{
                              width: 20,
                              height: 20,
                              borderRadius: "50%"
                            }}
                          />
                        )
                      }
                    />
                  </Link>
                )
              );
            })}
          {/* Probable Playing XI Section */}
        </div>

        {/* Venue Guide Section */}
        {!onlyPlayingXI && (
          <div
            style={{
              fontFamily: "Montserrat",
              fontSize: 12,
              color: grey_10,
              fontWeight: 600
            }}
          >
            <CardGradientTitle title="VENUE GUIDE" />
            <div onClick={() => this.redirectTo(score.venueId)}>
              <CardGradientTitle
                title={score.venue || "-"}
                rootStyles={{
                  background: white,
                  padding: "12px 16px"
                }}
                titleStyles={
                  {
                    // fontWeight: 500
                  }
                }
                isRightIcon
              />
            </div>
            {false && (
              <div
                style={{
                  flex: 1,
                  display: "flex",
                  alignItems: "center",
                  padding: "10px 16px 5px"
                }}
              >
                <div style={{ flex: 0.4, color: grey_10, fontWeight: 400 }}>
                  Capacity
                </div>
                <div style={{ flex: 0.6 }}>{score.venueCapacity || "-"}</div>
              </div>
            )}
            {false && (
              <div
                style={{ flex: 1, display: "flex", padding: "5px 16px 10px" }}
              >
                <div style={{ flex: 0.4, color: grey_8 }}>Weather Report</div>
                <div style={{ flex: 0.6 }}>
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <div
                      style={{
                        marginRight: 10,
                        fontSize: 26,
                        fontWeight: 400
                      }}
                    >
                      25&deg; C
                    </div>
                    <div>
                      <img
                        // src={require("../../../images/about.png")}
                        alt=""
                        style={{
                          width: 30,
                          height: 30
                        }}
                      />
                    </div>
                  </div>
                  <div>No. of Matches</div>
                </div>
              </div>
            )}
          </div>
        )}
        {/* Venue Guide Section closing */}

        {/* MAtch Officials Section */}
        {!onlyPlayingXI && (
          <div
            style={{
              fontFamily: "Montserrat",
              color: grey_8,
              fontSize: 12,
              color: grey_10
            }}
          >
            <CardGradientTitle title="MATCH OFFICIALS" />
            <div
              style={{
                display: "flex",
                flex: 1,
                padding: "8px 16px"
              }}
            >
              <div style={{ flex: 0.4 }}>Umpires</div>
              <div style={{ flex: 0.6, color: grey_10, fontWeight: 600 }}>
                {score.matchOfficials.firstUmpire}
                {`${
                  score.matchOfficials.secondUmpire
                    ? ", " + score.matchOfficials.secondUmpire
                    : "-"
                }`}
                {/* Chris Gaffaney Kumara Dharnasena */}
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flex: 1,
                padding: "8px 16px"
              }}
            >
              <div style={{ flex: 0.4 }}>Third Umpire</div>
              <div style={{ flex: 0.6, color: grey_10, fontWeight: 600 }}>
                {score.matchOfficials.thirdUmpire || "-"}
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flex: 1,
                padding: "8px 16px"
              }}
            >
              <div style={{ flex: 0.4 }}>Match Referee</div>
              <div style={{ flex: 0.6, color: grey_10, fontWeight: 600 }}>
                {score.matchOfficials.matchReferee || "-"}
              </div>
            </div>
          </div>
        )}
        {/* MAtch Officials Section closing */}
      </div>
    );
  }
}

export default MatchInfo;

const role = "AllRounder";
