import React, { Component } from "react";
import {
  white,
  cardShadow,
  avatarShadow,
  gradientGrey,
  grey_10,
  orange_2_1,
  blue_grey,
  grey_8
} from "../../../styleSheet/globalStyle/color";
import Notifications from "@material-ui/icons/Notifications";
import NotificationsNone from "@material-ui/icons/NotificationsNone";
import moment from "moment";
import HrLine from "../../commonComponent/HrLine";
import Polling from "../../commonComponent/Polling";
import { isAndroid } from "react-device-detect";
// import {
//   subscribeToMatch,
//   unSubscribeToMatch
// } from "../../../push-notification";

export class MiniScoreCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hrString: "",
      matchId: ""
    };
    this.timeout = 0;
  }

  handleNotification = matchId => {
    // if (this.state.matchId) {
    //   unSubscribeToMatch(matchId);
    // } else {
    //   subscribeToMatch(matchId);
    // }
    this.setState(
      {
        matchId: this.state.matchId ? "" : matchId
      },
      () => {
        if (isAndroid) {
          document
            .getElementById("notification-bell")
            .animate(
              [
                { transform: "rotate(35deg)" },
                { transform: "rotate(-30deg)" },
                { transform: "rotate(25deg)" },
                { transform: "rotate(-20deg)" },
                { transform: "rotate(15deg)" },
                { transform: "rotate(-10deg)" },
                { transform: "rotate(5deg)" },
                { transform: "rotate(0deg)" }
              ],
              {
                duration: 800,
                iterations: 1
              }
            );
        }
      }
    );
  };

  componentDidMount = () => {
    if (
      this.props.score &&
      this.props.score.status === "UPCOMING" &&
      this.props.score.startDate
    ) {
      this.handleCountdownTimer();
    }
  };
  handleCountdownTimer = () => {
    const startDate = this.props.score && this.props.score.startDate;
    let countDownDate = new Date(startDate).getTime() - 1800000;
    // console.log("inside : ", countDownDate);
    let now = new Date().getTime();
    // Find the distance between now and the count down date
    let distance = countDownDate - now;
    // Time calculations for days, hours, minutes and seconds
    // let days = Math.floor(distance / (1000 * 60 * 60 * 24));
    let hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((distance % (1000 * 60)) / 1000);
    let result =
      (hours > 0 ? hours + (hours > 1 ? " hrs " : " hr ") : "") +
      minutes +
      (minutes > 1 ? " mins " : " min ") +
      " to toss";
    // let result = hours + " hrs " + minutes + " min for toss";
    // Output the result in an element with id="time-section"
    // If the count down is over, write some text
    if (distance < 0) {
      clearInterval(this.timeout);
      // console.log("NO: ", result);
      this.setState({
        hrString: "0 hr 0 min to toss"
      });
      // return "0 hr 0 min to toss";
      // return "EXPIRED";
    } else {
      // console.log("yes: ", result);
      this.setState({
        hrString: result
      });
      // return result;
      // document.getElementById("time-section").innerHTML = result || "testing";
    }
    // console.log("called : ", startDate);
    this.timeout = setInterval(() => {
      // Get today's date and time
      let countDownDate = new Date(startDate).getTime() - 1800000;
      // console.log("inside : ", countDownDate);
      let now = new Date().getTime();
      // Find the distance between now and the count down date
      let distance = countDownDate - now;
      // Time calculations for days, hours, minutes and seconds
      // let days = Math.floor(distance / (1000 * 60 * 60 * 24));
      let hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((distance % (1000 * 60)) / 1000);
      let result =
        (hours > 0 ? hours + (hours > 1 ? " hrs " : " hr ") : "") +
        minutes +
        (minutes > 1 ? " mins " : " min ") +
        " to toss";
      // let result = hours + " hrs " + minutes + " min for toss";
      // Output the result in an element with id="time-section"
      // If the count down is over, write some text
      if (distance < 0) {
        clearInterval(this.timeout);
        // console.log("NO: ", result);
        this.setState({
          hrString: "0 hr 0 min to toss"
        });
        // return "0 hr 0 min to toss";
        // return "EXPIRED";
      } else {
        // console.log("yes: ", result);
        this.setState({
          hrString: result
        });
        // return result;
        // document.getElementById("time-section").innerHTML = result || "testing";
      }
    }, 1000);
  };

  componentWillUnmount = () => {
    clearInterval(this.timeout);
  };

  render() {
    // console.log("mini: ", this.props);
    const { score } = this.props;
    // console.log(score);
    if(!score) {
      return null
    }
    const { inningCollection } = score;
    let inningsArray = [];
    let teamCollection = [
      {
        teamName:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.fullName
            : score.teamB.fullName,
        shortName:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.shortName
            : score.teamB.shortName,
        teamKey:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.teamKey
            : score.teamB.teamKey,
        runs: [],
        wickets: [],
        overs: [],
        declared: [],
        followOn: [],
        avatar:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.avatar
            : score.teamB.avatar
      },
      {
        teamName:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.fullName
            : score.teamB.fullName,
        shortName:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.shortName
            : score.teamB.shortName,
        teamKey:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.teamKey
            : score.teamB.teamKey,
        runs: [],
        wickets: [],
        overs: [],
        declared: [],
        followOn: [],
        avatar:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.avatar
            : score.teamB.avatar
      }
    ];

    if (score.status !== "UPCOMING") {
      for (const order of score.inningOrder) {
        let team = order.split("_")[0];
        if (team === score.firstBatting) {
          (teamCollection[0].runs = [
            ...teamCollection[0].runs,
            score.inningCollection[order].runs
          ]),

          // console.log(order);
          // console.log(teamCollection[0]);
            (teamCollection[0].wickets = [
              ...teamCollection[0].wickets,
              score.inningCollection[order].wickets
            ]),
            (teamCollection[0].overs = [
              ...teamCollection[0].overs,
              score.inningCollection[order].overs
            ]),
            (teamCollection[0].declared = [
              ...teamCollection[0].declared,
              score.inningCollection[order].declared
            ]),
            (teamCollection[0].followOn = [
              ...teamCollection[0].followOn,
              score.inningCollection[order].followOn
            ]);
        } else {
          (teamCollection[1].runs = [
            ...teamCollection[1].runs,
            score.inningCollection[order].runs
          ]),
            (teamCollection[1].wickets = [
              ...teamCollection[1].wickets,
              score.inningCollection[order].wickets
            ]),
            (teamCollection[1].overs = [
              ...teamCollection[1].overs,
              score.inningCollection[order].overs
            ]),
            (teamCollection[1].declared = [
              ...teamCollection[1].declared,
              score.inningCollection[order].declared
            ]),
            (teamCollection[1].followOn = [
              ...teamCollection[1].followOn,
              score.inningCollection[order].followOn
            ]);
        }
      }
    } else if (score.status == "UPCOMING") {
      for (let i = 0; i < teamCollection.length; i++) {
        (teamCollection[i].teamName =
          score[i == 0 ? "teamA" : "teamB"].fullName),
          (teamCollection[i].shortName =
            score[i == 0 ? "teamA" : "teamB"].shortName),
          (teamCollection[i].teamKey =
            score[i == 0 ? "teamA" : "teamB"].teamKey);
        (teamCollection[i].runs = []),
          (teamCollection[i].wickets = []),
          (teamCollection[i].overs = []),
          (teamCollection[i].declared = []),
          (teamCollection[i].followOn = []),
          (teamCollection[i].avatar = score[i == 0 ? "teamA" : "teamB"].avatar);
      }
    }

    inningsArray.push({
      inningA: {
        teamName: score.teamA.fullName,
        shortName: score.teamA.shortName,
        teamKey: score.teamA.teamKey,
        overs: null,
        wickets: [],
        runs: []
      },
      inningB: {
        teamName: score.teamB.fullName,
        shortName: score.teamB.shortName,
        teamKey: score.teamB.teamKey,
        overs: null,
        wickets: [],
        runs: []
      }
    });
    let updatedInningsArray = inningsArray[0];
    if (inningCollection === null) {
      return null;
    } else {
      for (const key of Object.keys(inningCollection)) {
        if (
          inningCollection &&
          inningCollection[key].teamKey === score.teamA.teamKey
        ) {
          updatedInningsArray.inningA.overs =
            inningCollection && inningCollection[key].overs;
          updatedInningsArray.inningA.wickets.push(
            inningCollection && inningCollection[key].wickets
          );
          updatedInningsArray.inningA.runs.push(
            inningCollection && inningCollection[key].runs
          );
          updatedInningsArray.inningA.teamName = score.teamA.fullName;
          updatedInningsArray.inningA.shortName = score.teamA.shortName;
          updatedInningsArray.inningA.teamKey = score.teamA.teamKey;
        } else if (
          inningCollection &&
          inningCollection[key].teamKey === score.teamB.teamKey
        ) {
          updatedInningsArray.inningB.overs =
            inningCollection && inningCollection[key].overs;
          updatedInningsArray.inningB.wickets.push(
            inningCollection && inningCollection[key].wickets
          );
          updatedInningsArray.inningB.runs.push(
            inningCollection && inningCollection[key].runs
          );

          updatedInningsArray.inningB.teamName = score.teamB.fullName;
          updatedInningsArray.inningB.shortName = score.teamB.shortName;
          updatedInningsArray.inningB.teamKey = score.teamB.teamKey;
        }
      }
    }

    return (
      <div
        style={{
          background: white,
          borderRadius: 3,
          boxShadow: cardShadow
        }}
      >
        <div
          style={{
            padding: "12px"
          }}
        >
          {/* Score List Section */}
          {teamCollection &&
            teamCollection.map((team, teamIndex) => (
              <div
                key={team.teamKey}
                style={{
                  display: "flex",
                  flex: 1,
                  padding: "2px 0",
                  fontFamily: "Montserrat",
                  fontWeight: 400,
                  fontSize: 14,
                  color: grey_10,
                  position: "relative"
                }}
              >
                {teamIndex == 0 &&
                  score.status == "UPCOMING" && (
                    <div
                      style={{
                        position: "absolute",
                        top: 2,
                        left: "29%",
                        // background: "#f1f1f1",
                        borderLeft: "1px solid #e2e2e2",
                        fontFamily: "Montserrat",
                        fontSize: 14,
                        fontWeight: 400,
                        color: grey_10,
                        padding: "0 18px"
                      }}
                    >
                      <div>
                        {score && moment(score.startDate).format("Do MMMM ")}
                      </div>
                      <div style={{ marginTop: 5 }}>
                        {score &&
                          moment(score.startDate).format("h:mm A ") +
                            window
                              .moment()
                              .tz(window.moment.tz.guess(true))
                              .format("z")}
                      </div>
                    </div>
                  )}
                <div
                  style={{
                    display: "flex",
                    flex: 0.3,
                    alignItems: "center"
                  }}
                >
                  <img
                    src={
                      team.avatar && team.avatar !== "DEFAULT"
                        ? team.avatar
                        : require("../../../images/flag_empty.svg")
                    }
                    alt=""
                    style={{
                      width: 24,
                      height: 16,
                      boxShadow: avatarShadow,
                      marginRight: 14
                    }}
                  />
                  <span style={{ fontWeight: 400, fontSize: 14 }}>
                    {(team.shortName &&
                      team.shortName.substring(0, 4).toUpperCase()) ||
                      "-"}
                  </span>
                </div>
                <div
                  style={{
                    display: "flex",
                    flex: 0.7,
                    alignItems: "center",
                    justifyContent: "space-between"
                  }}
                >
                  <div style={{ fontWeight: 400, letterSpacing: "0.36px" }}>
                    {score.status !== "UPCOMING" ? (
                      <span
                        style={{
                          // fontWeight: `${
                          //   score.status === "RUNNING" &&
                          //   score.currentScoreCard &&
                          //   team.teamKey === score.currentScoreCard.teamKey
                          //     ? "600"
                          //     : "400"
                          // }`,
                          fontSize: 14
                        }}
                      >
                        <span style={{
                          fontWeight: `${
                            score.status === "RUNNING" &&
                            score.currentScoreCard &&
                            team.teamKey === score.currentScoreCard.teamKey &&
                            team.runs.length < 2
                              ? "600"
                              : "400"
                          }`
                        }}>
                        <div style={{
                          display: "inline-block",
                          minWidth: score.format == "TEST" ? 58 : "auto"
                        }}>
                        {`${
                          team.runs[0] !== undefined ? `${team.runs[0]}` : ""
                        }${
                          team.wickets[0] !== undefined && team.wickets[0] < 10
                            ? `/${team.wickets[0]}`
                            : ""
                        }`}
                        {
                          team.declared[0]
                            ? <span style={{
                              color: "#d64b4b",
                              fontWeight: 400
                            }}>{` d`}</span>
                            : ""
                        }
                        </div>
                        <span style={{ fontWeight: 400, marginLeft: 8 }}>
                          {(score.format !== "TEST" ||
                            score.status === "RUNNING" &&
                            score.currentScoreCard &&
                            team.teamKey === score.currentScoreCard.teamKey &&
                            team.runs.length < 2
                          ) &&
                            team.overs[0] &&
                            `(${team.overs[0]})`}
                        </span>
                        </span>
                        {/* {team.runs.length > 1 &&
                          `& ${team.runs[1]}/${team.wickets[1]}`} */}
                        <span style={{
                          fontWeight: `${
                            score.status === "RUNNING" &&
                            score.currentScoreCard &&
                            team.teamKey === score.currentScoreCard.teamKey &&
                            team.runs.length > 1
                              ? "600"
                              : "400"
                          }`
                        }}>
                        {team.runs.length > 1 &&
                          ` & ${
                            team.runs[1] !== undefined ? team.runs[1] : ""
                          }${
                            team.wickets[1] !== undefined &&
                            team.wickets[1] < 10
                              ? "/" + team.wickets[1]
                              : ""
                          }`}
                          {
                            team.declared.length > 1 && team.declared[1]
                              ? <span style={{
                                color: "#d64b4b",
                                fontWeight: 400
                              }}>{` d`}</span>
                              : ""
                          }
                          {
                            team.followOn.length > 1 && team.followOn[1]
                              ? <span style={{
                                color: "#d64b4b",
                                fontWeight: 400
                              }}>{` f/o`}</span>
                              : ""
                          }
                        <span style={{ fontWeight: 400, marginLeft: 8 }}>
                          {team.runs.length > 1 &&
                            (score.format !== "TEST" ||
                            score.status === "RUNNING" &&
                            score.currentScoreCard &&
                            team.teamKey === score.currentScoreCard.teamKey &&
                            team.runs.length > 1) &&
                            `(${team.overs[1]})`}
                        </span>

                        {/* To show DLS method text */}
                        {score &&
                          score.dlApplied &&
                          teamIndex == 1 && (
                            <span
                              style={{
                                color: grey_8,
                                fontSize: 12,
                                fontWeight: 400
                              }}
                            >
                              {" "}
                              (DLS)
                            </span>
                          )}
                      </span>
                      </span>
                    ) : null}
                  </div>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center"
                    }}
                  >
                    {score &&
                      teamIndex == 0 &&
                      score.status === "RUNNING" &&
                      score.firstBallStatus === "RUNNING" && (
                        // score.statusOverView === "RUNNING" &&
                        <span
                          style={{
                            display: "flex",
                            alignItems: "center",
                            border: "solid 0.6px #979797",
                            borderRadius: "7.5px",
                            padding: "1px 8px",
                            // display: "inline-block",
                            fontSize: "8px",
                            textTransform: "uppercase"
                          }}
                        >
                          <span
                            style={{
                              width: "5px",
                              height: "5px",
                              borderRadius: "50%",
                              marginLeft: "-2px",
                              marginRight: "4px",
                              backgroundColor: "#35a863",
                              display: "inline-block"
                            }}
                          />
                          live
                        </span>
                      )}
                    {!this.props.isTimeMachine &&
                      score &&
                      teamIndex == 0 &&
                      // score.status === "RUNNING" ||
                      (score.status === "UPCOMING" ||
                        score.firstBallStatus == "UPCOMING") && (
                        //   score.statusOverView == "PRE_MATCH"
                        <div
                          style={{ display: "flex" }}
                          onClick={
                            () => this.handleNotification(score.matchId)
                            // console.log("")
                          }
                        >
                          {this.state.matchId ? (
                            <Notifications
                              id="notification-bell"
                              style={{
                                fill: "#a70f14",
                                marginLeft: 4,
                                fontSize: 24
                              }}
                            />
                          ) : (
                            <NotificationsNone
                              id="notification-bell"
                              style={{
                                marginLeft: 4,
                                fontSize: 24,
                                color: blue_grey
                              }}
                            />
                          )}
                        </div>
                      )}
                  </div>
                </div>
              </div>
            ))}
          {/* Score List Section Closing */}

          {/* line separator for Criclytics time machine */}
          {this.props.isTimeMachine && (
            <div
              style={{
                margin: "14px 0"
              }}
            >
              <HrLine />
            </div>
          )}

          {/* Time Capsule Section */}
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              margin: "16px 0 8px"
            }}
          >
            <div style={{ display: "flex", padding: "0 6px 0 0" }}>
              <img
                src={require("../../../images/capsuleLeftWing.svg")}
                alt=""
                style={{ width: 40, height: 2 }}
              />
            </div>
            <div
              className="line_limit"
              style={{
                background: orange_2_1,
                fontFamily: "Montserrat",
                fontSize: 11,
                color: grey_10,
                padding: "2px 4px",
                borderRadius: "12px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                textAlign: "center",
                minWidth: "75%",
                maxWidth: "95%",
                whiteSpace: "initial"
              }}
            >
              {score.status !== "UPCOMING" ? (
                (score.status && score.status === "RUNNING"
                  ? score.toss
                  : score.statusStr) ||
                `${moment(score.startDate).format(
                  "Do MMM YYYY, h:mm A "
                )} ${window
                  .moment()
                  .tz(window.moment.tz.guess(true))
                  .format("z")}`
              ) : score.toss && score.status == "UPCOMING" ? (
                <div>{score.toss}</div>
              ) : new Date(score.startDate).getTime() - new Date().getTime() >
              88200000 ? (
                `${moment(score.startDate).format(
                  "Do MMM YYYY, h:mm A "
                )} ${window
                  .moment()
                  .tz(window.moment.tz.guess(true))
                  .format("z")}`
              ) : (
                this.state.hrString
              )}
            </div>
            <div style={{ display: "flex", padding: "0 0 0 6px" }}>
              <img
                src={require("../../../images/capsuleRightWing.svg")}
                alt=""
                style={{ width: 40, height: 2 }}
              />
            </div>
          </div>
          {/* Time Capsule Section closing */}
        </div>
        {!this.props.isTimeMachine && <HrLine />}
        {/* MOM section */}
        {!this.props.isTimeMachine && (
          <div
            style={{
              position: "relative",
              background: gradientGrey,
              borderRadius: 3
            }}
          >
            {score.criclyticsAvailable ?
              <img
                src={require("../../../images/criclyticsRectangle.svg")}
                alt=""
                style={{
                  position: "absolute",
                  bottom: -1,
                  right: -1,
                  width: 52,
                  height: 52
                }}
                onClick={() => {
                  this.props.history.push(
                    `/criclytics-slider/${this.props.matchId}`
                  );
                }}
              />
            : 
              ""
            }
            <div
              style={{
                width: score.criclyticsAvailable ? "83%" : ""
              }}
            >
              {/* MAN OF THE MATCH card */}
              {score.status === "COMPLETED" &&
              score.manOfTheMatch &&
              score.manOfTheMatch[0] ? (
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    fontFamily: "Montserrat",
                    color: grey_10,
                    padding: "10px 12px",
                    borderBottomLeftRadius: 3
                    // width: "83%"
                  }}
                  onClick={() =>
                    this.props.history.push(
                      `/players/${score.manOfTheMatch[0].playerId}`
                    )
                  }
                >
                  <div
                    style={{
                      height: 38,
                      width: 38,
                      overflow: "hidden",
                      borderRadius: "50%",
                      marginRight: 12
                    }}
                  >
                    <img
                      src={
                        score.manOfTheMatch &&
                        score.manOfTheMatch[0] &&
                        score.manOfTheMatch[0].avatar
                          ? score.manOfTheMatch[0].avatar
                          : require("../../../images/Man of the match-09.svg")
                      }
                      alt=""
                      style={{
                        width: "100%",
                        objectFit: "cover"
                      }}
                    />
                  </div>
                  <div
                    style={{
                      justifyContent: "space-between",
                      flexDirection: "column"
                    }}
                  >
                    <div style={{ fontSize: 12, fontWeight: 500 }}>
                      {score.manOfTheMatch &&
                        score.manOfTheMatch[0] &&
                        score.manOfTheMatch[0].name}
                    </div>
                    <div style={{ fontSize: 11, fontWeight: 400 }}>
                      Player of the Match
                    </div>
                  </div>
                </div>
              ) : score.status === "RUNNING" || score.status === "UPCOMING" ? (
                <div style={{}}>
                  <Polling
                    style={{
                      padding: "2px 15px",
                      display: "flex",
                      minHeight: 52
                    }}
                    data={(score && score.prediction && score.prediction) || {}}
                    score={score}
                  />
                </div>
              ) : (
                // <div
                //   style={{
                //     display: "flex",
                //     alignItems: "center",
                //     fontFamily: "Montserrat",
                //     fontWeight: 500,
                //     fontSize: 12,
                //     padding: "12px 8px"
                //   }}
                // >
                //   Man of the match yet to anounce...
                // </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "flex-start",
                    // background: gradientGrey,
                    alignItems: "center",
                    fontFamily: "Montserrat",
                    padding: "7px 8px",
                    color: grey_10,
                    ...this.props.rootStyles
                  }}
                  onClick={this.props.handleScoreCard}
                >
                  <div
                    style={{
                      height: 44,
                      width: 44,
                      overflow: "hidden",
                      borderRadius: "50%",
                      marginRight: 12
                    }}
                  >
                    <img
                      src={
                        score.matchCancelled
                          ? require("../../../images/Abandoned_Icon.png")
                          : require("../../../images/Man of the match-09.svg")
                      }
                      alt=""
                      style={{
                        width: "100%",
                        objectFit: "cover"
                      }}
                    />
                  </div>

                  <div
                    style={{
                      justifyContent: "space-between",
                      // flex: 0.95,
                      flexDirection: "column"
                    }}
                  >
                    <div
                      style={{
                        fontSize: 13,
                        fontWeight: 500,
                        whiteSpace: "nowrap",
                        overflow: "hidden",
                        textOverflow: "ellipsis"
                      }}
                    >
                      {score.matchCancelled ? "Match Abandoned" : "TBA"}
                    </div>
                    {!score.matchCancelled ? (
                      <div style={{ fontSize: 12, fontWeight: 400 }}>
                        Player of the Match
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              )}
            </div>
          </div>
        )}
        {/* MOM section closing */}
      </div>
    );
  }
}

export default MiniScoreCard;
