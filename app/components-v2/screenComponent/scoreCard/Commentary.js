import React, { Component } from "react";
import {
  grey_10,
  red_10,
  grey_6,
  grey_8,
  green_10,
  blue_10,
  white,
  orange_1_1
} from "../../../styleSheet/globalStyle/color";

import OverCard from "../../commonComponent/OverCard";
import HrLine from "../../commonComponent/HrLine";
import Loader from "../../commonComponent/Loader";
import EmptyState from "../../commonComponent/EmptyState";
import InfiniteScroll from "react-infinite-scroll-component";
import * as scoreDetailsActions from "containers/ScoreDetails/actions";
import WagonWheel from "../../commonComponent/WagonWheel";
import CardGradientTitle from "../../commonComponent/CardGradientTitle";
import ScrollButtons from "../../Rankings/ScrollButtons";
import CommentaryPlayerStatus from "./CommentaryPlayerStatus";
import OverSeparator from "./OverSeparator";
import ButtonBar from "../../commonComponent/ButtonBar";
import Sticky from "react-stickynode";
import InningSeparator from "./InningSeparator";
import InningSeparator2 from "./InningSeparator2";

const TAGS_CONFIG = [
  { label: "All", key: "all", filters: ["wicket", "four", "six", "milestone"] },
  { label: "Fours", key: "four", filters: ["four"] },
  { label: "Sixes", key: "six", filters: ["six"] },
  { label: "Wickets", key: "wicket", filters: ["wicket"] }
  // { label: "Milestones", key: "milestone", filters: ["milestone"] }
];
const WAGON_FILTERS = {
  all: ["four", "six", "wicket"],
  four: ["four"],
  six: ["six"],
  wicket: ["wicket"]
};
const FILTERS_MSG_OBJ = {
  all: "fours, sixes and wickets",
  four: "four",
  six: "sixes",
  wicket: "wickets"
};
export class Commentary extends Component {
  setTime = "";
  state = {
    lastData: [],
    loading: true,
    loader: false,
    loaders: false,
    activeButton: "all",
    activeTeam: "IND",
    separator: {
      over: "",
      summary: []
    },
    activeTeam: "",
    teamsList: [],
    teamsObj: {},
    isTeamSet: false,
    reversedOverSeparator: false,
    overSeparatorData: [],
    selectedWagonPoint: "",
    inningNumberArr: []
  };

  reverseOverSeparator = () => {
    this.setState({
      overSeparatorData:
        this.props.overSeparator.length > 0
          ? this.props.overSeparator.reverse()
          : [],
      reversedOverSeparator: true
    });
  };

  handleActiveTeam = activeTeam => {
    this.setState({
      activeTeam
    });
  };

  componentDidMount = () => {
    if (this.props.activeTab === "Highlights") {
      // this.props.getCommentaryData({
      //   params: {
      //     matchId: this.props.matchId,
      //     highlights: TAGS_CONFIG[0].filters
      //   }
      // });
    }
    // if(this.props.activeTab === 'Commentary' || this.props.activeTab === 'Live') {
    //   this.props.getCommentaryData({
    //     params: { matchId: "opta:44229", skip: 0, limit: 10 },
    //     // params: { matchId: "opta:44225", skip: 0, limit: 10 },
    //     isCommentry: true
    //   });
    // }
    if (
      this.props.activeTab === "Commentary" ||
      this.props.activeTab === "Live"
    ) {
      this.props.fetchCommentaryOverSeparator({
        matchId: this.props.match.params.matchId
        // inningNumber: 2
      });
    }
  };

  static getDerivedStateFromProps(props, state) {
    if (props.data.lastFetched !== state.lastFetched) {
      return {
        lastData: props.data.values,
        lastFetched: props.data.lastFetched,
        loader: false,
        loaders: false
      };
    }
    return null;
  }

  componentWillUnmount = () => {
    setTimeout(() => {
      // this.props.commentaryStopPollingReq();
    }, 100);
  };

  handleActiveButton = (activeButton, filters) => {
    if (activeButton !== this.state.activeButton) {
      this.setState({ activeButton, filters, selectedWagonPoint: "" }, () => {
        // this.props.getCommentaryData({
        // debugger
        this.props.commentaryHighlights({
          params: {
            matchId: this.props.matchId,
            highlights: filters,
            teamKey:
              this.state.teamsObj[this.state.activeTeam] &&
              this.state.teamsObj[this.state.activeTeam].teamKey,
            inningNumber:
              this.state.teamsObj[this.state.activeTeam] &&
              this.state.teamsObj[this.state.activeTeam].inning,
            skip: 0,
            limit: 10
          }
        });

        this.props.highlightsWagonStats({
          matchId: this.props.matchId,
          teamKey:
            this.state.teamsObj[this.state.activeTeam] &&
            this.state.teamsObj[this.state.activeTeam].teamKey,
          inningNumber:
            this.state.teamsObj[this.state.activeTeam] &&
            this.state.teamsObj[this.state.activeTeam].inning,
          type: WAGON_FILTERS[activeButton]
        });
      });
    }
  };

  fetchMoreData = () => {
    this.setState({
      loading: true,
      loader: true
    });
    if (this.props.activeTab === "Commentary") {
      this.props.dispatch(
        scoreDetailsActions.getPagingCommentaryData({
          params: {
            matchId: this.props.match.params.matchId,
            limit: 10,
            lastId: this.props.data.values[this.props.data.values.length - 1]
              .key
          },
          isCommentry: true
        })
      );
    } else if (this.props.activeTab === "Highlights") {
      this.props.dispatch(
        // scoreDetailsActions.getPagingCommentaryData({
        //   params: {
        //     limit: 10,
        //     lastId: this.props.data.values[this.props.data.values.length - 1]
        //       .key,
        //     matchId: this.props.match.params.matchId,
        //     highlights: this.props.data.filters.highlights
        //   }
        // })

        scoreDetailsActions.getPagingHighlightsData({
          params: {
            limit: 10,
            lastId: this.props.data.values[this.props.data.values.length - 1]
              .key,
            matchId: this.props.match.params.matchId,
            highlights: this.props.data.filters.highlights,
            teamKey: this.state.teamsObj[this.state.activeTeam].teamKey,
            inningNumber: this.state.teamsObj[this.state.activeTeam].inning
            // highlights: TAGS_CONFIG[0].filters
          }
        })
      );
    }
  };

  handleActiveTeam = activeTeam => {
    this.setState(
      {
        activeTeam,
        activeButton: "all",
        selectedWagonPoint: ""
      },
      () => {
        this.props.commentaryHighlights({
          params: {
            matchId: this.props.matchId,
            highlights: TAGS_CONFIG[0].filters,
            teamKey: this.state.teamsObj[activeTeam].teamKey,
            inningNumber: this.state.teamsObj[activeTeam].inning,
            skip: 0,
            limit: 10
          }
        });

        this.props.highlightsWagonStats({
          matchId: this.props.matchId,
          teamKey: this.state.teamsObj[activeTeam].teamKey,
          inningNumber: this.state.teamsObj[activeTeam].inning,
          type: WAGON_FILTERS.all
        });
      }
    );
  };

  teamsList = () => {
    let score = this.props.score;
    let arr = [],
      inningNumberArr = [],
      obj = {};
    if (score && score.battingOrder) {
      score.battingOrder.map((team, key) => {
        score.teams[team[0]].shortName;
        obj[`${score.teams[team[0]].shortName}${key + 1}`] = {
          inning: key + 1,
          // inning: score.battingOrder.length - key,
          teamKey: team[0]
        };
        // arr.push(score.teams[team[0]].shortName);
        // arr.unshift(score.teams[team[0]].shortName);
        // arr.unshift(`${score.teams[team[0]].shortName}${key + 1}`);
        arr.push(`${score.teams[team[0]].shortName}${key + 1}`);
        inningNumberArr.push(team[1]);
      });
    }
    if (arr.length > 0) {
      this.setState({
        activeTeam: arr[arr.length - 1],
        teamsList: arr,
        teamsObj: obj,
        isTeamSet: true,
        inningNumberArr: inningNumberArr
      });
    }
    return true;
  };

  render() {
    const data = this.state.lastData;
    const { activeButton } = this.state;
    const { wagonPoints, wagonPointsLoading, scoreSummary } = this.props;
    const totalFour = wagonPoints
      ? wagonPoints.filter(e => {
          return e.runs == "four";
        }).length
      : 0;
    const totalSix = wagonPoints
      ? wagonPoints.filter(e => {
          return e.runs == "six";
        }).length
      : 0;
    const totalWicket = wagonPoints
      ? wagonPoints.filter(e => {
          return e.wicket == true;
        }).length
      : 0;
    return (
      <div style={{}}>
        {this.props.activeTab == "Highlights" && (
          <div>
            {!this.state.isTeamSet ? this.teamsList() : ""}
            {this.state.teamsList.length > 0 && (
              <Sticky
                enabled
                top={94}
                // bottomBoundary={3000}
                innerZ={995}
                activeClass="highlightsButtonBar"
              >
                <ButtonBar
                  items={this.state.teamsList}
                  activeItem={this.state.activeTeam}
                  onClick={this.handleActiveTeam}
                  showIndex={this.state.teamsList.length > 2}
                  isTrimValue={4}
                  inningNumberArr={this.state.inningNumberArr}
                  // itemStyles={{
                  //   padding: "8px 20px"
                  // }}
                />
              </Sticky>
            )}
          </div>
        )}
        {/* {"teams: "+ this.teamsList()} */}
        {this.props.activeTab == "Highlights" && (
          <div>
            {wagonPointsLoading ? (
              <Loader styles={{ height: "239px" }} noOfLoaders={1} />
            ) : (
              <Sticky
                enabled
                top={140}
                // bottomBoundary={3000}
                innerZ={994}
                activeClass="highlightsButtonBar"
              >
                <WagonWheel
                  wagon={
                    wagonPoints
                      ? wagonPoints.filter(e => {
                          return !e.wicket;
                        })
                      : []
                  }
                  showWicketImage={this.state.activeButton == "wicket"}
                  selectedWagonPoint={this.state.selectedWagonPoint}
                />
                {/* REMOVE this module after implementing Total status */}
                {/* <div style={{
                      fontFamily: "Montserrat",
                      fontSize: 12,
                      color: grey_8,
                      // fontWeight: 700,
                      textAlign: "center",
                      background: "linear-gradient(to left, #ffffff, #f8f8f9 18%, #f8f8f9 85%, #ffffff)",
                      paddingBottom: 10,
                      height: 40
                    }} /> */}
                {
                  <div
                    style={{
                      fontFamily: "Montserrat",
                      fontSize: 12,
                      color: grey_8,
                      // fontWeight: 700,
                      textAlign: "center",
                      background:
                        "linear-gradient(to left, #ffffff, #f8f8f9 18%, #f8f8f9 85%, #ffffff)",
                      paddingBottom: 10
                    }}
                  >
                    {activeButton == "all" && <span>Total </span>}
                    {(activeButton == "all" || activeButton == "four") && (
                      <span>
                        <span>Fours: </span>
                        <span
                          style={{
                            color: grey_10,
                            marginRight: 6,
                            fontWeight: 500
                          }}
                        >
                          {totalFour}
                        </span>
                      </span>
                    )}
                    {(activeButton == "all" || activeButton == "six") && (
                      <span>
                        <span>Sixes: </span>
                        <span
                          style={{
                            color: grey_10,
                            marginRight: 6,
                            fontWeight: 500
                          }}
                        >
                          {totalSix}
                        </span>
                      </span>
                    )}
                    {(activeButton == "all" || activeButton == "wicket") && (
                      <span>
                        <span>Wickets: </span>
                        <span
                          style={{
                            color: grey_10,
                            marginRight: 6,
                            fontWeight: 500
                          }}
                        >
                          {totalWicket}
                        </span>
                      </span>
                    )}
                  </div>
                }
              </Sticky>
            )}

            <Sticky
              enabled
              top={398}
              // bottomBoundary={3000}
              innerZ={994}
              activeClass="highlightsButtonBar"
            >
              <ScrollButtons
                items={TAGS_CONFIG}
                activeItem={this.state.activeButton}
                onClick={this.handleActiveButton}
                rootStyles={{
                  justifyContent: "space-between",
                  padding: "12px 10px"
                }}
              />
            </Sticky>
          </div>
        )}
        {/* <HrLine /> */}
        {this.props.overSeparator.length > 0 &&
        this.props.overSeparator.length != this.state.overSeparatorData &&
        this.props.commentaryFilter == "OVER BY OVER" &&
        !this.state.reversedOverSeparator
          ? this.reverseOverSeparator()
          : null}
        <div
          style={{
            height:
              this.props.activeTab == "Commentary" &&
              this.props.commentaryFilter == "OVER BY OVER"
                ? 560
                : 0,
            overflowY: "scroll"
          }}
        >
          {this.props.activeTab == "Commentary" &&
          this.props.commentaryFilter == "OVER BY OVER" &&
          this.props.overSeparator.length > 0 ? (
            // (key == 0 || data[key].overNo != data[key - 1].overNo) && (
            this.state.overSeparatorData.map((inning, inningIndex) => (
              // this.props.overSeparator.reverse().map((inning, inningIndex) =>
              // Inning-Separator for Over-By-Over Testing
              <div>
                  {/* OVER_BY_OVER INNING SEPARATOR */}
                  <div>
                    {inning && inning[0] && inning[0][0] && inning[0][0].inningsLastball &&
                      <InningSeparator2
                        getInningSummary={this.props.getInningSummary}
                        inningSummary={this.props.inningSummary}
                        list={{
                          inningNumber:
                            this.state.overSeparatorData.length - inningIndex,
                          teamKey: inning[0][0].teamKey
                        }}
                        matchId={this.props.match.params.matchId}
                      />
                    }
                  </div>
                  {/* OVER_BY_OVER INNING SEPARATOR CLOSING */}
                {inning.map((over, overIndex) => (
                  <div key={`${overIndex + 1}`}>
                    <OverSeparator overs={over || []} />
                  </div>
                ))}
              </div>
            ))
          ) : (
            <div>
              <EmptyState
                img={require("../../../images/emptyState.svg")}
                imageStyle={{
                  width: "40%",
                  height: "auto"
                }}
                msg="Over data not found"
                msgStyle={{
                  marginTop: 10
                }}
              />
            </div>
          )}
        </div>

        {/* players Commentary Section */}
        {data.length > 0 &&
        ((this.props.activeTab == "Commentary" &&
          this.props.commentaryFilter !== "OVER BY OVER") ||
          this.props.activeTab == "Highlights") ? (
          <InfiniteScroll
            dataLength={data.length}
            next={this.fetchMoreData}
            hasMore={this.state.loading}
            loader={
              this.state.loader && (
                <Loader styles={{ height: "75px" }} noOfLoaders={5} />
              )
            }
            height={560}
          >
            {data.map((list, key) => (
              <div
                // onClick={() => {
                //   this.props.activeTab == "Commentary" &&
                //     (list.runs == 4 || list.runs == 6) &&
                //     this.props.showGroundGraph(list.key);
                // }}
                key={`${key + 1}`}
              >
                {false &&
                  this.props.activeTab == "Commentary" &&
                  key == 0 &&
                  data &&
                  data[key] && (
                    // list && list.inningNumber != data[key-1].inningNumber &&
                    <div>
                      <CardGradientTitle
                        title={`End of ${list.battingTeamShortName} innings`}
                        subtitle={`${list.battingTeamShortName &&
                          list.battingTeamShortName
                            .substring(0, 4)
                            .toUpperCase()}()`}
                      />
                    </div>
                  )}

                {/* New Inning Separator */}
                {this.props.activeTab == "Commentary" &&
                  list.inningsLastball && (
                    <InningSeparator2
                      getInningSummary={this.props.getInningSummary}
                      inningSummary={this.props.inningSummary}
                      list={list}
                      matchId={this.props.match.params.matchId}
                    />
                  )}
                {/* New Inning Separator closing */}
                {/* Inning Separtaor Block */}
                {false &&
                  this.props.activeTab == "Commentary" &&
                  list.inningsLastball && (
                    // list.ballNo == 6 &&
                    // list.valid &&
                    <div
                      style={{
                        border: "2px solid #2e3857",
                        fontFamily: "Montserrat"
                      }}
                    >
                      {/* black card */}
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          alignItems: "center",
                          background: "#2e3857",
                          padding: "12px 8px",
                          color: "#FFF",
                          fontSize: 12,
                          fontFamily: "Montserrat",
                          fontWeight: 500
                        }}
                      >
                        <div
                          style={{
                            display: "flex",
                            alignItems: "center"
                          }}
                        >
                          <div
                            style={{
                              width: 24,
                              height: 16,
                              marginRight: 6
                            }}
                          >
                            <img
                              src={
                                (this.props.score &&
                                  this.props.score.teams &&
                                  this.props.score.teams[list.teamKey] &&
                                  this.props.score.teams[list.teamKey]
                                    .avatar) ||
                                require("../../../images/flag_empty.svg")
                              }
                              alt=""
                              style={{
                                width: 24,
                                height: 16
                              }}
                            />
                          </div>
                          <div>
                            {`End of ${(this.props.score &&
                              this.props.score.teams &&
                              this.props.score.teams[list.teamKey] &&
                              this.props.score.teams[list.teamKey].shortName) ||
                              ""} innings`}
                          </div>
                        </div>
                        <div>
                          <span
                            style={{
                              marginRight: 6,
                              fontWeight: 700
                            }}
                          >
                            {`${list.teamRuns || 0}${
                              list.teamWickets > 0 ? `/${list.teamWickets}` : ""
                            }`}
                            {/* 309/5 */}
                          </span>
                          <span>
                            ({list.overString &&
                            list.overString.indexOf(".6") > 0 &&
                            list.valid
                              ? list.overNo
                              : list.overString})
                          </span>
                        </div>
                      </div>
                      {/* black card closing */}
                      {/* Tets Comp */}
                      <InningSeparator
                        getScoreSummary={this.props.getScoreSummary}
                        scoreSummary={this.props.scoreSummary}
                        list={list}
                        matchId={this.props.match.params.matchId}
                      />
                      {/* Tets Comp closing */}
                      {/* Status card */}
                      {/* 1 */}

                      {/* Status card closing */}
                    </div>
                  )}
                {/* Inning Separtaor Block closing */}

                {this.props.activeTab == "Commentary" &&
                  this.props.commentaryFilter == "BALL BY BALL" &&
                  // (key == 0 || data[key].overNo != data[key - 1].overNo) && (
                  // (key > 0 &&
                  //   data &&
                  //   data[key].overNo != data[key - 1].overNo) &&
                  // this.props.overSeparator.length > 0 && (
                  // (data[key].overNo != data[key - 1].overNo) && (

                  data &&
                  data[key] &&
                  data[key].overString &&
                  (data[key].overString.indexOf(".6") > -1 &&
                    data[key].valid) &&
                  this.props.overSeparator.length > 0 && (
                    <div>
                      <OverSeparator
                        overs2={
                          this.props.overSeparator[
                            `${list.crictecMatchId}:${list.inningNumber}:${
                              list.overNo
                            }`
                          ] || []
                        }
                        overs={
                          (this.props.overSeparator[list.inningNumber - 1] &&
                            this.props.overSeparator[list.inningNumber - 1][
                              this.props.overSeparator[list.inningNumber - 1]
                                .length - list.overNo
                            ]) ||
                          []
                        }
                      />
                    </div>
                  )}
                {this.props.activeTab == "Commentary" &&
                  this.props.commentaryFilter == "BALL BY BALL" &&
                  // (key == 0 || data[key].overNo != data[key - 1].overNo) && (
                  // (key > 0 &&
                  //   data &&
                  //   data[key].overNo != data[key - 1].overNo) && (
                  // (data && (data[key].overString.indexOf(".6") > -1 && data[key+1].overString.indexOf(".6") == -1) &&

                  data &&
                  data[key] &&
                  data[key].overString &&
                  (data[key].overString.indexOf(".6") > -1 &&
                    data[key].valid) && (
                    <div>
                      <CommentaryPlayerStatus
                        playerStats={list}
                        players={
                          this.props.score && this.props.score.players
                            ? this.props.score.players
                            : {}
                        }
                        teamName={
                          this.props.score &&
                          this.props.score.teams &&
                          this.props.score.teams[list.teamKey] &&
                          this.props.score.teams[list.teamKey].shortName
                            ? this.props.score.teams[list.teamKey].shortName
                                .substring(0, 4)
                                .toUpperCase()
                            : "-"
                        }
                      />
                      <HrLine />
                    </div>
                  )}

                {(this.props.commentaryFilter == "BALL BY BALL" ||
                  this.props.activeTab == "Highlights") && (
                  <div>
                    <div
                      style={{
                        display: "flex",
                        flex: 1,
                        padding: "8px 16px"
                        // flexWrap: "wrap"
                      }}
                      onClick={() => {
                        this.props.activeTab == "Commentary" &&
                          (list.runs == 4 || list.runs == 6) &&
                          this.props.showGroundGraph(list.key);
                        this.props.activeTab == "Highlights" &&
                          this.setState({
                            selectedWagonPoint: list.key
                          });
                      }}
                    >
                      <div
                        style={{
                          display: "flex",
                          flex:
                            (list.runs == 4 || list.runs == 6) &&
                            this.props.activeTab == "Commentary"
                              ? 0.86
                              : 1
                        }}
                      >
                        <div
                          style={
                            {
                              // display: "flex",
                              // alignItems: "center"
                            }
                          }
                        >
                          <OverCard
                            rootStyles={{
                              // background: red_10
                              background:
                                list.fifty || list.hundred
                                  ? orange_1_1
                                  : list.runs == 6
                                    ? blue_10
                                    : list.runs == 4
                                      ? green_10
                                      : list.wicketCount
                                        ? red_10
                                        : white,
                              border:
                                !list.fifty &&
                                !list.hundred &&
                                list.runs != 4 &&
                                list.runs != 6 &&
                                !list.wicketCount &&
                                `1px solid ${grey_6}`,
                              color:
                                !list.fifty &&
                                !list.hundred &&
                                list.runs != 4 &&
                                list.runs != 6 &&
                                !list.wicketCount
                                  ? grey_8
                                  : white
                            }}
                            titleStyles={{
                              borderColor:
                                !list.fifty &&
                                !list.hundred &&
                                list.runs != 4 &&
                                list.runs != 6 &&
                                !list.wicketCount &&
                                `${grey_6}`,
                              color:
                                !list.fifty &&
                                !list.hundred &&
                                list.runs != 4 &&
                                list.runs != 6 &&
                                !list.wicketCount
                                  ? grey_8
                                  : white,
                              fontSize:
                                list.extras > 0 && list.extraType ? 16 : 21
                            }}
                            data={{
                              // title: list.wicketCount ? "w" : list.runs,
                              title: list.fifty
                                ? "F"
                                : list.hundred
                                  ? "H"
                                  : list.wicketCount
                                    ? "w"
                                    : list.extras > 0 && list.extraType
                                      ? list.extraType.slice(0, 3)
                                      : list.runs,
                              caption: list.overString || "-"
                            }}
                          />
                        </div>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "column",
                            // justifyContent: "center",
                            padding: "0 4px 0 14px"
                          }}
                        >
                          <div
                            style={{
                              fontFamily: "Montserrat",
                              fontWeight: 500,
                              fontSize: 12,
                              color: grey_6
                            }}
                          >
                            {/* {list.comment || "-"} */}
                            <div
                              dangerouslySetInnerHTML={{
                                __html:
                                  list.comment &&
                                  list.comment.replace(
                                    "</li>,<li>",
                                    "</li><li>"
                                  )
                              }}
                            />
                          </div>
                          {/* Ball Status of commentary */}
                          {/* <div
                            style={{
                              fontFamily: "Montserrat",
                              fontWeight: 500,
                              fontSize: 12,
                              color: grey_10
                            }}
                          >
                            {list.ballStatus || "-"}
                          </div> */}
                        </div>
                      </div>
                      {this.props.activeTab == "Commentary" &&
                        (list.runs == 4 || list.runs == 6) && (
                          <div
                            style={{
                              display: "flex",
                              flex: 0.14,
                              justifyContent: "flex-end",
                              alignItems: "flex-start"
                            }}
                          >
                            <img
                              src={require("../../../images/groundImageCommentary.png")}
                              alt=""
                              style={{
                                width: 30,
                                height: 30
                              }}
                            />
                          </div>
                        )}
                    </div>
                    <HrLine />
                  </div>
                )}
                {this.props.showGroundGraphFlag[list.key] &&
                  this.props.commentaryFilter == "BALL BY BALL" &&
                  this.props.activeTab == "Commentary" &&
                  (list.runs == 4 || list.runs == 6) && (
                    <WagonWheel
                      wagon={[
                        {
                          x: list.xCoordinate,
                          y: list.yCoordinate
                        }
                      ]}
                    />
                  )}
              </div>
            ))}
          </InfiniteScroll>
        ) : (
          <div>
            {this.props.activeTab == "Commentary" &&
            this.props.commentaryFilter == "OVER BY OVER" ? null : (
              // <EmptyState msg={`${this.props.activeTab == "Highlights" ? `No ${FILTERS_MSG_OBJ[this.state.activeButton]} so far` : "Commentary not found"} ${this.props.activeTab} not found`} />
              <EmptyState
                img={require("../../../images/emptyState.svg")}
                imageStyle={{
                  width: "40%",
                  height: "auto"
                }}
                msg={`${
                  this.props.activeTab == "Highlights"
                    ? `No ${FILTERS_MSG_OBJ[this.state.activeButton]} so far`
                    : "Commentary not found"
                }`}
                msgStyle={{
                  marginTop: 10
                }}
              />
            )}
          </div>
        )}
      </div>
    );
  }
}

export default Commentary;
