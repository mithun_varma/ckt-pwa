import React, {Component} from 'react';
import { screenWidth, screenHeight } from '../../../styleSheet/screenSize/ScreenDetails';

export default class Highlights extends Component{
    constructor(props){
        super(props);
        this.state={
            activeTeam:'team1',

        }
        this._handelToggel =this._handelToggel.bind(this);
        this._handelTeamSelection= this._handelTeamSelection.bind(this);

    }
    componnetDidMount(){

    }
    render(){
        return(
            <div style={pageStyle.highlightsContainer}>
                <div style={pageStyle.teamSelectionConatiner}>
                    {this.teamSelection()}
                </div>
                <div>
                    {null}

                </div>
                <div style={pageStyle.typeOfHighlitsContainer}>
                    {this.toggelActionBar()}
                </div>
                <div>

                </div>

            </div>
        );
    }
    teamSelection(){
        var team1Style = pageStyle.teamSelectionActive;
        var team2Style = pageStyle.teamSelectionUnActive;
        if(this.state.activeTeam == 'team1'){
            team1Style = pageStyle.teamSelectionActive;
            team2Style = pageStyle.teamSelectionUnActive;
        }
        else{
            team2Style = pageStyle.teamSelectionActive;
            team1Style = pageStyle.teamSelectionUnActive;

        }
        return(
            <div style={pageStyle.teamSelectionConatiner}>
                <div  style={team1Style} name="team1" onClick={this._handelTeamSelection}> 
                    Team 1
                </div>
                <div style={team2Style} name="team2" onClick={this._handelTeamSelection}> 
                    Team 2
                </div>
            </div>
        );
    }
    _handelTeamSelection(event){
        const team = event.target.name;
        this.setState({activeTeam:'team2'});

    }

        
    fours(){
        return(
            <div>
                {this.boxDyanamicFour(32.5)}
            </div>
        );
    }
    sixes(){
        return(
            <div>
                {this.boxDyanamicSix(33.5)}

            </div>
        );

    }
    wicket(){
        return(
            <div>
                {this.boxDyanamicWicket(24.6)}
            </div>
        );

    }
    boxDyanamicFour(over){
        return(
            <div style={pageStyle.boxContainerFour}>
               <div style={pageStyle.topHalf}>
                    {"4"}
                </div>
                <div style={{display:'flex',width:screenWidth*0.08,marginLeft:screenWidth*0.018,backgroundColor:'#fff',height:1}}>
                {null}
                </div>
                <div style={pageStyle.lowerHalf}>
                    {over}
                </div>
            </div>
        );
    }
    boxDyanamicSix(over){
        return(
            <div style={pageStyle.boxContainerSix}>
                <div style={pageStyle.topHalf}>
                    {"6"}
                </div>
                <div style={{display:'flex',width:screenWidth*0.08,marginLeft:screenWidth*0.018,backgroundColor:'#fff',height:1}}>
                {null}
                </div>
                <div style={pageStyle.lowerHalf}>
                    {over}
                </div>
            </div>
        );
    }
    boxDyanamicWicket(over){
        return(
            <div style={pageStyle.boxContainerWicket}>
                <div style={pageStyle.topHalf}>
                    {"W"}
                </div>
                <div style={{display:'flex',width:screenWidth*0.08,marginLeft:screenWidth*0.018,backgroundColor:'#fff',height:1}}>
                {null}
                </div>
                <div style={pageStyle.lowerHalf}>
                    {over}
                </div>
            </div>
        );
    }
    toggelActionBar(){
        return(
            <div style={pageStyle.actionBarContainer}>
                <div style={{display:'flex',flex:0.10}}>
                    {null}
                </div>
                <div style={pageStyle.activeButton}>
                    All
                </div>
                <div style={{display:'flex',flex:0.10}}>
                    {null}
                </div>
                <div style={pageStyle.unactiveButton}>
                    Fours
                </div>
                <div style={{display:'flex',flex:0.10}}>
                    {null}
                </div>
                <div style={pageStyle.activeButton}>
                    Sixes
                </div>
                <div style={{display:'flex',flex:0.10}}>
                    {null}
                </div>
                <div style={pageStyle.unactiveButton}>
                    Wickets
                </div>
                <div style={{display:'flex',flex:0.10}}>
                    {null}
                </div>
            </div>
        );
    }
    
    _handelToggel(event){

    }
}

const pageStyle={
    teamSelectionActive:{
        display:'flex',
        flex:0.50,
        width:screenWidth*0.26,
        height:screenHeight*0.044,
        alignItems:'center',
        justifyContent:'center',
        color:'#f76d1d',
        fontSize:'10px',
        backgroundColor:'#e0e1e4',

    },
    teamSelectionUnActive:{
        display:'flex',
        flex:0.50,
        width:screenWidth*0.26,
        height:screenHeight*0.044,
        alignItems:'center',
        justifyContent:'center',
        color:'#fff',
        fontSize:'10px',
        backgroundColor:'#e0e1e4',

    },
    highlightsContainer:{
        display:'flex',
        flexDirection:'column',
        flex:1,
        width:screenWidth*0.911,
        backgroundColor:'#fff',
        boxShadow:'0px 3px 6px 0px #13000000',

    },
    teamSelectionConatiner:{
        display:'flex',
        flexDirection:'row',
        width:screenWidth*0.53,
        height:screenHeight*0.044,
        justifyContent:'center',
        alignItems:'center',

    },
    typeOfHighlitsContainer:{
        display:'flex',
        flexDirection:'row',
        flex:1,
    },
    topHalf:{
        display:'flex',
        paddingTop:'3px',
        paddingBottom:'3px',
        paddingLeft:'5px',
        paddingRight:'5px',
        justifyContent:'center',
        fontSize:'21px',
    },
    lowerHalf:{
        display:'flex',
        paddingBottom:'2px',
        paddingTop:'2px',
        paddingRight:'5px',
        paddingLeft:'5px',
        justifyContent:'center',
        fontSize:'10px'
    },
    boxContainerFour:{
        display:'flex',
        flexDirection:'column',
        flex:1,
        borderRadius:'3px',
        boxShadow:'0px 3px 6px 0px #13000000',
        backgroundColor:'#35a863',
        color:'#fff',
        width:screenWidth*0.116,
    },
    boxContainerSix:{
        display:'flex',
        flexDirection:'column',
        flex:1,
        borderRadius:'3px',
        boxShadow:'0px 3px 6px 0px #13000000',
        backgroundColor:'#4a90e2',
        color:'#fff',
        width:screenWidth*0.116,
    },
    boxContainerWicket:{
        display:'flex',
        flexDirection:'column',
        flex:1,
        borderRadius:'3px',
        boxShadow:'0px 3px 6px 0px #13000000',
        backgroundColor:'#d64b4b',
        color:'#fff',
        width:screenWidth*0.116,
    },
    container:{
        display:'flex',
        flexDirection:'column',
        flex:1,
        width:screenWidth*0.911,
        marginLeft:screenWidth*0.025,

    },
    actionBarContainer:{
        display:'flex',
        flex:1,
        flexDirection:'row',
        padding:'4px',
        justifyContent:'center'

    },
    activeButton:{
        display:'flex',
        flex:0.15,
        backgroundColor:'#f76b1c',
        boxShadow:'0px 3px 6px 0px #13000000',
        justifyContent:'center',
        paddingLeft:'6.5px',
        paddingRight:'6.5px',
        paddingTop:'2px',
        paddingBottom:'2px',
        fontSize:'12px',
        letterSpacing:'0.6px',
        color:'#fff',
        borderRadius:'3px',
        letterSpacing:'0.6px',
    },
    unactiveButton:{
        flex:0.15,
        display:'flex',
        color:'#9b9b9b',
        display:'flex',
        paddingLeft:'6.5px',
        paddingRight:'6.5px',
        paddingTop:'2px',
        paddingBottom:'2px',
        backgroundColor:'#fafafa',
        boxShadow:'0px 3px 6px 0px #13000000',
        justifyContent:'center',
        fontSize:'12px',
        letterSpacing:'0.6px',
        border:'0.5px solid #979797',
        borderRadius:'3px',
        letterSpacing:'0.6px'
    }
}