import React, {Component} from 'react';
import { screenHeight, screenWidth } from '../../../styleSheet/screenSize/ScreenDetails';
import ScoreCard from './scoreCard';
import Live from './live';
import map from 'lodash/map';
import without from 'lodash/without';
import BatsmanScoreCard from '../../commonComponent/BatsmanScoreBoard';
import BowlerScoreCard from './../../commonComponent/BowlerScoreBoard';
import Highlights from './highlight';

const tabBarData =['Live', 'Scorecard', 'Highlights']

export default class ScoreCardTeamDetails extends Component{
    constructor(props){
        super(props);
        this.state={
            activeTab:'Scorecard',
            team1Active:true,
            team2Active:false,
        };
        this._handelTopActionClick = this._handelTopActionClick.bind(this);
    }
    componentDidMount(){
       
    }
    render(){
        return(
            <div style={{display:'flex',flex:1, flexDirection:'column'}}>
                <div>
                    {this.topActionBar()}
                </div>
                <div>
                    {this.showRequiredScreen()}
                </div>
                
            </div>
        );
    }
    showRequiredScreen(){
        const {activeTab } = this.state;
        
        if(activeTab === 'Live'){
            return(
                <div>
                    {this.live()}
                </div>
            );
        }
        else if ( activeTab === 'Scorecard'){
            return(
                <div style={{display:'flex', flex:1, flexDirection:'column'}}>
                    {this.ScoreCard()}
                </div>

            );
        }
        else if ( activeTab === 'Highlights'){
            return(
                <div>
                    {this.highlits()}

                </div>
            )
        }
     }
    highlits(){
        return(
            <div>
                <Highlights />
            </div>
        );
    }
    live(){
        const rawData= this.props.live;
        const playerStatsViews = rawData['playerStatsViews'];
        const liveData = this.props.live['fullScorecard'];
        const matchMiniScoreCard =liveData['matchMiniScorecard'];
        const isMatchRunnig = matchMiniScoreCard.matchStatus;
        const nonStriker = liveData.nonStriker;
        const striker = liveData.striker;
        const thisOverRuns = matchMiniScoreCard.thisOverRuns;
        const bowler = "sha_khan";
        const currentScoreCard = matchMiniScoreCard['currentScoreCard'];
        const wicketOrder = currentScoreCard['wicketOrder'];
        const lastWicket = wicketOrder[wicketOrder.length-1]; // code name
        const patnershipr ="not coming";
        var livePlayerBatsman=[];
        var livePlayerBowler=[];
        livePlayerBatsman.push(without(map(playerStatsViews, function(o) {if (o.key == striker) return o;}), undefined)[0]);
        livePlayerBatsman.push(without(map(playerStatsViews, function(o) {if (o.key == nonStriker) return o;}), undefined)[0]);
        livePlayerBowler.push(without(map(playerStatsViews, function(o) {if (o.key == bowler) return o;}), undefined)[0]);
        const  lastWicketBatsman= without(map(playerStatsViews, function(o) {if (o.key == lastWicket) return o;}), undefined)[0];
            return(
                <div>
                   <BatsmanScoreCard  teamAData ={livePlayerBatsman} total ={'NA'} extra={'NA'}/>
                   <BowlerScoreCard teamAData ={livePlayerBowler} />
                   <Live patnerShip={"NA"} lastWicket={lastWicketBatsman} liveData={liveData} commentaryData={this.props.commentaryData}/>
                </div>
            ); 
    }
    ScoreCard(){
        const {venueDetails} =this.props;
        return(
            <div>
              <ScoreCard playerStatsViews={this.props.playerStatsViews} collectionOfAllInning={this.props.collectionOfAllInning} venueDetails={venueDetails}/>
            </div>
        );
    }
    topActionBar(){
        const {activeTab} = this.state;
        var Live = pageStyle.singleOptionActive;
        var Scorecard = pageStyle.singleOptionUnActive;
        var Highlights = pageStyle.singleOptionUnActive;
        if(activeTab === 'Live'){
            Live = pageStyle.singleOptionActive;
            Scorecard = pageStyle.singleOptionUnActive;
            Highlights = pageStyle.singleOptionUnActive;
        }
        else if (activeTab === 'Scorecard'){
            Live = pageStyle.singleOptionUnActive;
            Scorecard = pageStyle.singleOptionActive;
            Highlights = pageStyle.singleOptionUnActive;

        }
        else if(activeTab === 'Highlights'){
            Live = pageStyle.singleOptionUnActive;
            Scorecard = pageStyle.singleOptionUnActive;
            Highlights = pageStyle.singleOptionActive;

        }
        return(
            <div style={pageStyle.topActionBarConatiner}>
                <div style={pageStyle.empty}>
                    {null}
                </div>
                <div style={Live} id={tabBarData[0]} onClick={this._handelTopActionClick}>
                    {tabBarData[0]}
                </div>
                <div style={pageStyle.empty}>
                    {null}
                </div>
                <div style={Scorecard} id={tabBarData[1]} onClick={this._handelTopActionClick}>
                    {tabBarData[1]}
                </div>
                <div style={pageStyle.empty}>
                    {null}
                </div>
                <div style={Highlights} id={tabBarData[2]} onClick={this._handelTopActionClick}>
                    {tabBarData[2]}
                </div>
                <div style={pageStyle.empty}>
                    {null}
                </div>
            </div>
        );
    }
    _handelTopActionClick(event){
        const selected = event.target.innerHTML;
        this.setState({activeTab:selected});
    }

}

const pageStyle={
    topActionBarConatiner:{
        display:'flex',
        flexDirection:'row',
        flex:1,
        width:screenWidth*0.911,
        backgroundColor:'#fff',
        boxShadow:'0px 3px 6px 0px #13000000',
        alignItems:'center',
    },
    singleOptionActive:{
        display:'flex',
        flex:0.15,
        fontSize:'12px',
        color:'#141b2f',
        height:screenHeight*0.0609,
        paddingTop:'12px',
        borderBottom:'2px solid #f76b1c',
    },
    singleOptionUnActive:{
        display:'flex',
        flex:0.15,
        fontSize:'12px',
        color:'#141b2f',
        paddingTop:'12px',
        height:screenHeight*0.0609,

    },
    empty:{
        display:'flex',
        flex:0.10,

    }

}