import React, { Component } from "react";
import {
  white,
  grey_10,
} from "../../../styleSheet/globalStyle/color";
import ArrowBack from "@material-ui/icons/ArrowBack";
import moment from "moment";

export class ScoreDetailsHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  getTitle = score => {
    const status = score && score.matchStatus ? score.matchStatus : false;
    let title = "";
    if (status == "COMPLETED") {
      title = score && score.statusStr ? score.statusStr : "";
    } else if (status == "RUNNING") {
      title =
        score && score.currentScoreCard && score.currentScoreCard.teamKey
          ? score.teamA.teamKey == score.currentScoreCard.teamKey
            ? `${score.teamA.shortName} ${score.currentScoreCard.runs}/${
                score.currentScoreCard.wickets
              } (${score.currentScoreCard.overs})`
            : score.teamB.teamKey == score.currentScoreCard.teamKey
              ? `${score.teamB.shortName} ${score.currentScoreCard.runs}${
                  (score.currentScoreCard.wickets ||
                    score.currentScoreCard.wickets == 0) &&
                  score.currentScoreCard.wickets < 10
                    ? "/" + score.currentScoreCard.wickets
                    : ""
                } (${score.currentScoreCard.overs})`
              : ""
          : score && score.matchShortName
            ? score.matchShortName +
              (score.relatedName ? ", " + score.relatedName : "")
            : "";
    } else if (status == "UPCOMING") {
      title = score && score.matchShortName ? score.matchShortName : "";
    }
    return title;
  };

  render() {
    const { isHeaderBackground, score } = this.props;
    return (
      <div
        style={{
          position: "fixed",
          top: 0,
          left: 0,
          right: 0,
          zIndex: 999999
        }}
      >
        <div
          style={{
            position: "relative",
            background: isHeaderBackground ? white : "transparent",
            // background: isHeaderBackground ? gradientRedOrange : "transparent",
            color: isHeaderBackground ? grey_10 : white,
            // color: white,
            width: "100%",
            height: 56,
            display: "flex",
            flex: 1,
            alignItems: "center",
            padding: "0 10px",
            // transform: "translateZ(0)",
            // transition: "all .5s",
            boxShadow: isHeaderBackground
              ? "0 3px 3px 0 rgba(0, 0, 0, 0.12)"
              : "none"
          }}
        >
          {/* <div> */}
          {isHeaderBackground && (
            <ArrowBack
              // style={{ color: isHeaderBackground ? grey_10 : white, }}
              style={{ color: isHeaderBackground ? grey_10 : white }}
              onClick={e => {
                e.preventDefault();
                this.props.leftIconOnClick();
              }}
            />
          )}
          {/* </div> */}
          <div
            style={{
              display: "flex",
              alignItems: "center",
              fontFamily: "Montserrat",
              // color: grey_10,
              fontWeight: 600,
              fontSize: 14
            }}
          >
            <div style={{ padding: "0px 45px 0 4px" }}>
              {isHeaderBackground && this.getTitle(score)}
            </div>
            {/* Upcoming Status Section */}
            {score &&
              isHeaderBackground &&
              score.matchStatus == "UPCOMING" && (
                <div
                  style={{
                    fontSize: 11,
                    padding: "0 11px",
                    // borderLeft: `1px solid ${grey_6}`
                    borderLeft: "1px solid #e2e2e2",
                    fontWeight: 500
                  }}
                >
                  {score &&
                    score.startDate && (
                      <div>
                        {moment(this.props.score.startDate).format(
                          "D MMM YYYY"
                        )}
                      </div>
                    )}
                  <div style={{ color: isHeaderBackground ? grey_10 : white }}>
                    {score && score.venue ? score.venue : ""}
                  </div>
                </div>
              )}
          </div>
          {isHeaderBackground && score.criclyticsAvailable && (
            <img
              src={require("../../../images/criclyticsRectangle.svg")}
              alt=""
              style={{
                position: "absolute",
                bottom: -1,
                right: -1,
                width: 56,
                height: 56,
                opacity: isHeaderBackground ? 1 : 0,
                transition: isHeaderBackground
                  ? "opacity .5s linear"
                  : "visibility 0s .5s, opacity .5s linear"
              }}
              onClick={this.props.redirectToCriclytics}
            />
          )}
        </div>
      </div>
    );
  }
}

export default ScoreDetailsHeader;
