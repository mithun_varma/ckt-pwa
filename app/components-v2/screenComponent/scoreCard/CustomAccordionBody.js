import React from "react";
import PropTypes from "prop-types";
import {
  gradientGrey,
  grey_8,
  white,
  grey_10
} from "../../../styleSheet/globalStyle/color";
// import { AccordionItemBody } from 'react-accessible-accordion';
import ExpandMore from "@material-ui/icons/ExpandMore";
import ExpandLess from "@material-ui/icons/ExpandLess";
import { isAndroid } from "react-device-detect";

/* eslint-disable react/prefer-stateless-function */
/* eslint-disable prefer-destructuring */

const battingKeys = ["runs", "balls", "fours", "sixes", "strikeRate"];
// const bowlingKeys = ['overs', 'maidens', 'runs', 'wickets', 'wide', 'no_ball', 'economyRate'];
const bowlingKeys = ["overs", "maidenOvers", "runs", "wickets", "economy"];

class CustomAccordionBody extends React.Component {
  state = {
    // hideSquard: false,
    fallOfWicketsToggle: true
  };
  handleToggleSquard = () =>
    this.setState(ps => ({
      hideSquard: !ps.hideSquard
    }));

  // shouldComponentUpdate = (nextProps, nextState) =>
  //   !nextProps.score.equals(this.props.score) || nextState.hideSquard !== this.state.hideSquard;

  getFAllOfWickets = (item, key) => {
    let name = item && item.split(" at ")[0];
    let scoreAt = item && item.split(" at ")[1].split(" in ")[0];
    let overAt = item && item.split(" in ")[1];
    return (
      item && (
        <div
          key={key}
          style={{
            display: "flex",
            flex: 1,
            padding: "10px 10px",
            justifyContent: "space-between"
          }}
        >
          <div>{`${scoreAt}/${key + 1}`}</div>
          <div style={{ color: grey_8 }}>{name}</div>
          <div>{overAt}</div>
        </div>
      )
    );
    // })
  };

  render() {
    const { inning, score, order } = this.props;
    if (!score || !score.teams) return <React.Fragment />;
    const { players } = score;
    // TODO: refine this code
    const bowlingTeam =
      Object.keys(score.teams).indexOf(order[0]) === 0
        ? score.innings[`${Object.keys(score.teams)[0]}_${order[1]}`]
        : score.innings[`${Object.keys(score.teams)[1]}_${order[1]}`];
    if (score.teams && Object.keys(score.teams).length > 0)
      return (
        <div className="accorCcntent">
          <div className="table-responsive">
            {/* <table className="table scorecard-table"> */}
            <table className="table scorecard-table">
              <thead
                className="scorecard-table-header"
                style={{
                  background: isAndroid ? gradientGrey : "#f0f1f2",
                  fontFamily: "Montserrat",
                  fontSize: 28,
                  color: grey_8
                }}
              >
                <tr>
                  <th>BATSMEN</th>
                  <th>R</th>
                  <th>B</th>
                  <th>4s</th>
                  <th>6s</th>
                  <th>SR</th>
                </tr>
              </thead>
              <tbody>
                {inning.battingOrder &&
                  inning.battedPlayers &&
                  // inning.battingOrder.map(player => {
                  inning.battedPlayers.map(player => {
                    const batsMan = players[player];
                    const batting =
                      batsMan &&
                      batsMan.innings &&
                      batsMan.innings[order[1]] &&
                      batsMan.innings[order[1]].batting;
                    let battingStatus = "";
                    // if (batting.runs) {
                    // if (batting && batting.balls) {
                    if (batting && batting.dismissed) {
                      // battingStatus = batting.dismissedAt.outStr
                      battingStatus = batting.outStr ? batting.outStr : "-";
                    } else {
                      battingStatus = "Not Out";
                    }
                    // }
                    return (
                      batting && (
                        <tr
                          key={player}
                          className={batting.runs ? "" : ""}
                          style={{
                            fontFamily: "Montserrat"
                          }}
                        >
                          <td>
                            <span
                              style={{
                                fontSize: 12,
                                fontWeight: 500,
                                color: grey_10
                              }}
                              onClick={() =>
                                this.props.history.push(
                                  `/players/${batsMan.key}`
                                )
                              }
                            >
                              {batsMan.name +
                                (score.teams && inning && inning.teamKey && score.teams[inning.teamKey]
                                  ? (batsMan.key ==
                                  score.teams[inning.teamKey].captain && batsMan.key ==
                                  score.teams[inning.teamKey].keeper)
                                  ? " (c) (wk)"
                                  : batsMan.key ==
                                    score.teams[inning.teamKey].captain
                                    ? " (c)"
                                    : batsMan.key ==
                                      score.teams[inning.teamKey].keeper
                                      ? " (wk)"
                                      : ""
                                  : "")}
                            </span>
                            <small
                              style={{
                                color: battingStatus == "Not Out" && "#9b000d",
                                fontWeight:
                                  battingStatus == "Not Out" ? 600 : 400,
                                textTransform:
                                  battingStatus == "Not Out" && "lowercase"
                              }}
                            >
                              {battingStatus}
                            </small>
                          </td>
                          {battingKeys.map(battingKey => {
                            let rate = null;
                            if (battingKey === "strikeRate")
                              rate = batting[battingKey]
                                ? // ? batting[battingKey].toFixed(2)
                                  batting[battingKey]
                                : " 0.00";
                            else
                              rate = batting[battingKey]
                                ? batting[battingKey]
                                : 0;
                            //
                            return (
                              <td
                                style={{
                                  color:
                                    battingKey == "runs" ? grey_10 : grey_8,
                                  fontWeight: battingKey == "runs" ? 500 : 400
                                }}
                                key={battingKey}
                              >
                                {rate}
                              </td>
                            );
                          })}
                        </tr>
                      )
                    );
                  })}
              </tbody>
            </table>
            <div className="wicketFall-Card">
              <ul>
                <li
                  style={{
                    fontFamily: "Montserrat",
                    fontWeight: 500
                  }}
                >
                  Extras
                  <span className="total-wickets-right">
                    {inning.extras} (b {inning.byes} , lb {inning.legByes} , nb{" "}
                    {inning.noBalls} , wd {inning.wides})
                  </span>
                </li>
                <li
                  style={{
                    // background: gradientBlackSecondary,
                    background: grey_10,
                    color: white,
                    fontFamily: "Montserrat",
                    fontSize: 12,
                    padding: "14px 10px",
                    fontWeight: 500
                  }}
                >
                  Total
                  <span
                    className="total-wickets-right"
                    style={{
                      fontWeight: 600
                    }}
                  >
                    {inning.runs}
                    {inning.wickets < 10
                      ? "/" + inning.wickets
                      : ""} ({inning.overs / 1} overs, RR : &nbsp;
                    {inning.runRate})
                  </span>
                </li>
                {// inning.wickets < 9 &&
                inning.wickets < 10 &&
                  inning.yetToBat &&
                  inning.yetToBat.length > 0 && (
                    // true && (
                    <li style={{ fontFamily: "Montserrat" }}>
                      <span
                        style={{
                          fontWeight: 600
                        }}
                      >
                        {inning.completed ? "Did not bat:" : "Yet to Bat:"}
                      </span>
                      <ul className="full-wicket">
                        {// inning.battingOrder &&
                        // inning.battedPlayers &&
                        // score.teams[order[0]].playingXi &&
                        // score.teams[order[0]].playingXi
                        // inning.battingOrder
                        // .filter(
                        //   value => inning.battingOrder.indexOf(value) === -1
                        // )
                        // .filter(
                        //   value => inning.battedPlayers.indexOf(value) === -1
                        // )
                        inning.yetToBat &&
                          inning.yetToBat.map((player, key) => {
                            const batsMan = players[player];
                            // const { batting } = batsMan && batsMan.innings && batsMan.innings[order[1]];
                            // const batting =
                            //   batsMan &&
                            //   batsMan.innings &&
                            //   batsMan.innings[order[1]] &&
                            //   batsMan.innings[order[1]].batting;
                            // // ;
                            // let battingStatus = "";
                            // let playerName = "";
                            // if (typeof batting === "undefined") {
                            //   return null;
                            // }
                            // if (batting.runs) {
                            //   if (batting.dismissed) {
                            //     battingStatus = batting.outStr;
                            //   } else {
                            //     battingStatus = "Not Out";
                            //   }
                            // }

                            // if (
                            //   !batting ||
                            //   (!batting.dismissed && batting.balls == 0)
                            // ) {
                            //   playerName = batsMan.name;
                            // }
                            // console.log("Player: ", player, batsMan)
                            return (
                              // playerName && (
                              <li
                                key={`${key + 1}`}
                                style={{ fontFamily: "Montserrat" }}
                              >
                                {(batsMan && batsMan.name && batsMan.name) ||
                                  ""}
                                {inning.yetToBat.length - 1 > key && ","}
                                {/* {key < 10 && ","} */}
                                {/* {Object.keys(score.teams[order[0]].playingXi).length -1 > key && ","} */}
                                {/* <small>{battingStatus}</small> */}
                              </li>
                              // )
                            );
                          })}
                      </ul>
                    </li>
                  )}
                {/* Fall of wickets old place */}
              </ul>
            </div>
          </div>
          <div className="table-responsive">
            {/* <ul>
              <li>
                Extra
                <span className="total-wickets-right">
                  {inning.extras} (b {inning.byes} , lb {inning.leg_byes} , nb {inning.no_balls} ,
                  wd {inning.wides})
                </span>
              </li>
              <li>
                Total
                <span className="total-wickets-right">
                  {inning.runs}/{inning.wickets} ({inning.overs} overs, RR : &nbsp;
                  {inning.runRate})
                </span>
              </li>
              <li>
                Fall Of Wickets:
                <ul className="full-wicket">
                  {inning.wicketOrder &&
                    inning.wicketOrder
                      .map(player => ({
                        player,
                        wicket: score.players[player].innings[order[1]].batting.dismissedAt,
                      }))
                      .map(({ wicket, player }, index) => (
                        <li key={player}>
                          {` ${wicket.wicketIndex}-${wicket.teamRuns} (${
                            score.players[player].name
                          }, ${wicket.overStr})${
                            inning.wicketOrder.length === index + 1 ? '' : ', '
                          }`}
                        </li>
                      ))}
                </ul>
              </li>
            </ul> */}
            {/* <HrLine /> */}
            <table
              className="table bowlerCard"
              style={{
                fontFamily: "Montserrat"
              }}
            >
              <thead
                style={{
                  background: isAndroid ? gradientGrey : "#f0f1f2",
                  fontFamily: "Montserrat",
                  fontSize: 28,
                  color: grey_8
                  // borderTop: "1px solid #e1e5f0"
                }}
              >
                <tr>
                  <th>BOWLERS</th>
                  <th>O</th>
                  <th>M</th>
                  <th>R</th>
                  <th>W</th>
                  {/* <th>NB</th> */}
                  {/* <th>WD</th> */}
                  <th>Econ</th>
                </tr>
              </thead>
              <tbody>
                {bowlingTeam &&
                  bowlingTeam.bowlingOrder &&
                  bowlingTeam.bowlingOrder.map(player => {
                    const bowler = players[player];
                    const bowling =
                      bowler &&
                      bowler.innings &&
                      bowler.innings[order[1]] &&
                      bowler.innings[order[1]].bowling;
                    return (
                      bowling && (
                        <tr key={player}>
                          <td
                            style={{
                              color: grey_10,
                              fontSize: 11,
                              fontWeight: 500
                            }}
                            onClick={() =>
                              this.props.history.push(`/players/${bowler.key}`)
                            }
                          >
                            {bowler.name}
                          </td>
                          {bowlingKeys.map(key => {
                            if (key === "economy")
                              return (
                                <td key={key}>
                                  {bowling[key] ? bowling[key] : " 0.00"}
                                </td>
                              );
                            if (key === "maidenOvers")
                              return (
                                <td key={key}>
                                  {bowling[key] || bowling[key] === 0
                                    ? bowling[key]
                                    : "0"}
                                </td>
                              );
                            if (key === "overs")
                              return (
                                <td key={key}>
                                  {bowling["overStr"] ||
                                  bowling["overStr"] === 0
                                    ? bowling["overStr"]
                                    : "0"}
                                </td>
                              );
                            return (
                              <td key={key}>
                                {bowling[key] || bowling[key] === 0
                                  ? bowling[key]
                                  : "-"}
                              </td>
                            );
                          })}
                        </tr>
                      )
                    );
                  })}
              </tbody>
            </table>

            {inning.wickets !== 0 && (
              // <li>
              //   Fall Of Wickets:
              //   <ul className="full-wicket">
              //     {inning.wicketOrder &&
              //       inning.wicketOrder
              //         .map(player => ({
              //           player,
              //           // wicket: score.players[player].innings[order[1]].batting.dismissedAt,
              //           wicket:
              //             score.players[player].innings[order[1]].batting
              //               .cdcDissmisedAtView
              //         }))
              //         .map(({ wicket, player }, index) => (
              //           <li key={player}>
              //             {` ${wicket.wicketIndex}-${wicket.teamRuns} (${
              //               score.players[player].name
              //             }, ${wicket.overStr})${
              //               inning.wicketOrder.length === index + 1
              //                 ? ""
              //                 : ", "
              //             }`}
              //           </li>
              //         ))}
              //         Hello
              //   </ul>
              // </li>
              <div
                style={{
                  fontFamily: "Montserrat",
                  fontWeight: 600,
                  fontSize: 11,
                  color: grey_10
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flex: 1,
                    justifyContent: "space-between",
                    alignItems: "center",
                    padding: "10px",
                    borderBottom: "1px solid #e1e5f0",
                    borderTop: "1px solid #e1e5f0",
                    background: gradientGrey
                  }}
                  onClick={() => {
                    this.setState({
                      fallOfWicketsToggle: !this.state.fallOfWicketsToggle
                    });
                  }}
                >
                  <div>Fall of Wickets</div>
                  {this.state.fallOfWicketsToggle ? (
                    <ExpandLess style={{ fontSize: 20 }} />
                  ) : (
                    <ExpandMore style={{ fontSize: 20 }} />
                  )}
                </div>
                {this.state.fallOfWicketsToggle && (
                  <div>
                    {inning.fallOfWickets &&
                      inning.fallOfWickets.length &&
                      inning.fallOfWickets.map((item, key) =>
                        this.getFAllOfWickets(item, key)
                      )}
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      );
    return <React.Fragment />;
  }
}

CustomAccordionBody.propTypes = {
  score: PropTypes.object.isRequired,
  inning: PropTypes.object.isRequired,
  order: PropTypes.array.isRequired
};
export default CustomAccordionBody;
