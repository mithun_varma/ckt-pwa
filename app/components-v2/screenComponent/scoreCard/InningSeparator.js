import React, { Component } from "react";
import { grey_10, white } from "../../../styleSheet/globalStyle/color";
import HrLine from "../../commonComponent/HrLine";

class InningSeparator extends Component {
  componentDidMount() {
    this.props.getScoreSummary({
      matchId: this.props.matchId
      // matchId: "2019265-33-new-zealand-vs-pakistan"
    });
  }

  render() {
    const { scoreSummary, list } = this.props;
    return (
      <div style={{
        background: white
      }}>
        {/* Status card */}
        {/* 1 */}
        <div
          style={{
            display: "flex",
            flex: 1,
            color: grey_10,
            fontSize: 12
          }}
        >
          <div
            style={{
              display: "flex",
              flex: 0.5,
              justifyContent: "space-between",
              padding: "0px 6px",
              margin: "12px 0",
              borderRight: "1px solid #e3e4e6"
            }}
          >
            <div>
              {(list.inningNumber == 1 &&
                scoreSummary &&
                scoreSummary.teamScoreA &&
                scoreSummary.teamScoreA.batsmanSummary &&
                scoreSummary.teamScoreA.batsmanSummary.topBatsman &&
                scoreSummary.teamScoreA.batsmanSummary.topBatsman.shortName) ||
                ""}
              {(list.inningNumber == 2 &&
                scoreSummary &&
                scoreSummary.teamScoreB &&
                scoreSummary.teamScoreB.batsmanSummary &&
                scoreSummary.teamScoreB.batsmanSummary.topBatsman &&
                scoreSummary.teamScoreB.batsmanSummary.topBatsman.shortName) ||
                ""}
            </div>
            <div
              style={{
                fontWeight: 600
              }}
            >
              {(list.inningNumber == 1 &&
                scoreSummary &&
                scoreSummary.teamScoreA &&
                scoreSummary.teamScoreA.batsmanSummary &&
                scoreSummary.teamScoreA.batsmanSummary.topBatsman &&
                scoreSummary.teamScoreA.batsmanSummary.topBatsman.runs) ||
                ""}
              {(list.inningNumber == 2 &&
                scoreSummary &&
                scoreSummary.teamScoreB &&
                scoreSummary.teamScoreB.batsmanSummary &&
                scoreSummary.teamScoreB.batsmanSummary.topBatsman &&
                scoreSummary.teamScoreB.batsmanSummary.topBatsman.runs) ||
                ""}
            </div>
          </div>
          <div
            style={{
              display: "flex",
              flex: 0.5,
              justifyContent: "space-between",
              padding: "0px 6px",
              margin: "12px 0"
            }}
          >
            <div>
              {(list.inningNumber == 1 &&
                scoreSummary &&
                scoreSummary.teamScoreA &&
                scoreSummary.teamScoreA.bowlerSummary &&
                scoreSummary.teamScoreA.bowlerSummary.topBowler &&
                scoreSummary.teamScoreA.bowlerSummary.topBowler.shortName) ||
                ""}
              {(list.inningNumber == 2 &&
                scoreSummary &&
                scoreSummary.teamScoreB &&
                scoreSummary.teamScoreB.bowlerSummary &&
                scoreSummary.teamScoreB.bowlerSummary.topBowler &&
                scoreSummary.teamScoreB.bowlerSummary.topBowler.shortName) ||
                ""}
            </div>
            <div
              style={{
                fontWeight: 600
              }}
            >
              {list.inningNumber == 1 &&
              scoreSummary &&
              scoreSummary.teamScoreA &&
              scoreSummary.teamScoreA.bowlerSummary &&
              scoreSummary.teamScoreA.bowlerSummary.topBowler
                ? (scoreSummary.teamScoreA.bowlerSummary.topBowler.wickets ||
                    0) +
                  "/" +
                  (scoreSummary.teamScoreA.bowlerSummary.topBowler.runs || 0)
                : ""}
              {list.inningNumber == 2 &&
              scoreSummary &&
              scoreSummary.teamScoreB &&
              scoreSummary.teamScoreB.bowlerSummary &&
              scoreSummary.teamScoreB.bowlerSummary.topBowler
                ? (scoreSummary.teamScoreB.bowlerSummary.topBowler.wickets ||
                    0) +
                  "/" +
                  (scoreSummary.teamScoreB.bowlerSummary.topBowler.runs || 0)
                : ""}

              {/* 2/43 */}
            </div>
          </div>
        </div>
        <HrLine />
        {/* 2 */}
        <div
          style={{
            display: "flex",
            flex: 1,
            color: grey_10,
            fontSize: 12
          }}
        >
          <div
            style={{
              display: "flex",
              flex: 0.5,
              justifyContent: "space-between",
              padding: "0px 6px",
              margin: "12px 0",
              borderRight: "1px solid #e3e4e6"
            }}
          >
            <div>
              {(list.inningNumber == 1 &&
                scoreSummary &&
                scoreSummary.teamScoreA &&
                scoreSummary.teamScoreA.batsmanSummary &&
                scoreSummary.teamScoreA.batsmanSummary.runnerBatsman &&
                scoreSummary.teamScoreA.batsmanSummary.runnerBatsman
                  .shortName) ||
                ""}
              {(list.inningNumber == 2 &&
                scoreSummary &&
                scoreSummary.teamScoreB &&
                scoreSummary.teamScoreB.batsmanSummary &&
                scoreSummary.teamScoreB.batsmanSummary.runnerBatsman &&
                scoreSummary.teamScoreB.batsmanSummary.runnerBatsman
                  .shortName) ||
                ""}
            </div>
            <div
              style={{
                fontWeight: 600
              }}
            >
              {(list.inningNumber == 1 &&
                scoreSummary &&
                scoreSummary.teamScoreA &&
                scoreSummary.teamScoreA.batsmanSummary &&
                scoreSummary.teamScoreA.batsmanSummary.runnerBatsman &&
                scoreSummary.teamScoreA.batsmanSummary.runnerBatsman.runs) ||
                ""}
              {(list.inningNumber == 2 &&
                scoreSummary &&
                scoreSummary.teamScoreB &&
                scoreSummary.teamScoreB.batsmanSummary &&
                scoreSummary.teamScoreB.batsmanSummary.runnerBatsman &&
                scoreSummary.teamScoreB.batsmanSummary.runnerBatsman.runs) ||
                ""}
            </div>
          </div>
          <div
            style={{
              display: "flex",
              flex: 0.5,
              justifyContent: "space-between",
              padding: "0px 6px",
              margin: "12px 0"
            }}
          >
            <div>
              {(list.inningNumber == 1 &&
                scoreSummary &&
                scoreSummary.teamScoreA &&
                scoreSummary.teamScoreA.bowlerSummary &&
                scoreSummary.teamScoreA.bowlerSummary.runnerBowler &&
                scoreSummary.teamScoreA.bowlerSummary.runnerBowler.shortName) ||
                ""}
              {(list.inningNumber == 2 &&
                scoreSummary &&
                scoreSummary.teamScoreB &&
                scoreSummary.teamScoreB.bowlerSummary &&
                scoreSummary.teamScoreB.bowlerSummary.runnerBowler &&
                scoreSummary.teamScoreB.bowlerSummary.runnerBowler.shortName) ||
                ""}
            </div>
            <div
              style={{
                fontWeight: 600
              }}
            >
              {list.inningNumber == 1 &&
              scoreSummary &&
              scoreSummary.teamScoreA &&
              scoreSummary.teamScoreA.bowlerSummary &&
              scoreSummary.teamScoreA.bowlerSummary.runnerBowler
                ? (scoreSummary.teamScoreA.bowlerSummary.runnerBowler.wickets ||
                    0) +
                  "/" +
                  (scoreSummary.teamScoreA.bowlerSummary.runnerBowler.runs || 0)
                : ""}
              {list.inningNumber == 2 &&
              scoreSummary &&
              scoreSummary.teamScoreB &&
              scoreSummary.teamScoreB.bowlerSummary &&
              scoreSummary.teamScoreB.bowlerSummary.runnerBowler
                ? (scoreSummary.teamScoreB.bowlerSummary.runnerBowler.wickets ||
                    0) +
                  "/" +
                  (scoreSummary.teamScoreB.bowlerSummary.runnerBowler.runs || 0)
                : ""}
            </div>
          </div>
        </div>
        {/* Status card closing */}
      </div>
    );
  }
}

export default InningSeparator;
