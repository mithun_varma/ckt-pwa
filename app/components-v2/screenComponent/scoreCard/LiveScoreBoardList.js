import React, { Component } from "react";
import {
  gradientGrey,
  grey_8,
  grey_10
} from "../../../styleSheet/globalStyle/color";

export class LiveScoreBoardList extends Component {
  render() {
    const {
      isIcon,
      name,
      runs,
      balls,
      fours,
      sixes,
      strikeRate,
      playerId
    } = this.props;
    return (
      <div>
        <div
          style={{
            display: "flex",
            flex: 1,
            alignItems: "center",
            padding: "12px 16px",
            background: gradientGrey,
            fontFamily: "Montserrat",
            fontSize: 10,
            letterSpacing: "0.5px",
            color: grey_8,
            ...this.props.rootStyles
          }}
        >
          <div
            style={{
              display: "flex",
              alignItems: "center",
              flex: 0.4,
              color: grey_10,
              ...this.props.nameStyles
            }}
            onClick={() => this.props.history.push(`/players/${playerId}`)}
          >
            {isIcon && (
              <img
                src={require("../../../images/flame.png")}
                alt=""
                style={{
                  width: 8,
                  height: 12,
                  marginRight: 4
                }}
              />
            )}
            {name || "-"}
          </div>
          <div
            style={{
              display: "flex",
              flex: 0.12,
              justifyContent: "center",
              color: grey_10
            }}
          >
            {runs}
          </div>
          <div
            style={{
              display: "flex",
              flex: 0.12,
              justifyContent: "center"
            }}
          >
            {balls}
          </div>
          <div
            style={{
              display: "flex",
              flex: 0.1,
              justifyContent: "center"
            }}
          >
            {fours}
          </div>
          <div
            style={{
              display: "flex",
              flex: 0.1,
              justifyContent: "center"
            }}
          >
            {sixes}
          </div>
          <div
            style={{
              display: "flex",
              flex: 0.16,
              justifyContent: "flex-end"
            }}
          >
            {strikeRate}
          </div>
        </div>
      </div>
    );
  }
}

export default LiveScoreBoardList;
