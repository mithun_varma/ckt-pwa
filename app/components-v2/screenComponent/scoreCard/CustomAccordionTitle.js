/**
 *
 * Scoreboard
 *
 */

/* eslint-disable indent */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */

import React from "react";
import PropTypes from "prop-types";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import {
  white,
  grey_10,
  blue_grey,
  red_Orange
} from "../../../styleSheet/globalStyle/color";

// import styled from 'styled-components';

const CustomAccordionTitle = ({
  score,
  order,
  inningsConstant,
  inning,
  index,
  toggle,
  onClick,
  inningNumber
}) => {
  // const toggleOpen = !!toggle[index];
  
  return (
    <React.Fragment>
      <div
        // className={toggleOpen ? "accorHeader active" : "accorHeader"}
        className={toggle ? "accorHeader active" : "accorHeader"}
        onClick={e => {
          e.preventDefault();
          onClick(index);
        }}
      >
        <div
          className="squardList"
          style={{
            // background: toggleOpen ? gradientOrangeLeft : white,
            // background: "toggleOpen" ? blue_grey : white,
            background: toggle ? red_Orange : white,
            fontSize: 12,
            fontFamily: "Montserrat"
            // color: white
          }}
        >
          <div className="squardList__content">
            <div
              className="squardList__list"
              style={{
                display: "flex",
                justifyContent: "space-between",
                paddingRight: 10
              }}
            >
              <span
                style={{
                  // color: "toggleOpen" ? white : grey_10,
                  color: toggle ? white : grey_10,
                  letterSpacing: "0.4px",
                  fontFamily: "Montserrat"
                }}
              >
                <strong
                  style={{
                    fontFamily: "Montserrat",
                    fontWeight: 600,
                    fontSize: 12
                  }}
                >
                  {score.format == "test" && score.teams[order[0]] ?
                    score.teams[order[0]].shortName + " " +inningNumber + " inn." : ""}
                  {score.format != "test" && score.teams[order[0]] ?
                    score.teams[order[0]].shortName + " " +(inning.inningId == 2 ? " super over" : ""): ""}
                </strong>
              </span>
              <span
                style={{
                  // color: "toggleOpen" ? white : grey_10,
                  color: toggle ? white : grey_10,
                  fontFamily: "Montserrat",
                  fontWeight: 600,
                  fontSize: 12
                }}
              >
                {/* {score.format === "test" ? `${inningsConstant[order[1]]}` : ""} */}
                {` ${inning.runs}${
                  inning.wickets < 10 ? "/" + inning.wickets : " "
                } (${inning.overs})`}
              </span>
            </div>
            <div className="squardList__list">
              <span
                className="squardList__list__icon"
                style={{
                  display: "flex"
                }}
              >
                {/* {toggleOpen ? ( */}
                {toggle ? (
                  <ExpandLess style={{ color: white }} />
                ) : (
                  <ExpandMore style={{ color: blue_grey }} />
                )}
              </span>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

CustomAccordionTitle.propTypes = {
  score: PropTypes.object.isRequired,
  order: PropTypes.array.isRequired,
  inningsConstant: PropTypes.object.isRequired,
  inning: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired
};

export default CustomAccordionTitle;
