import React, { Component } from "react";
import { grey_10, white } from "../../../styleSheet/globalStyle/color";
import HrLine from "../../commonComponent/HrLine";

class InningSeparator2 extends Component {
  componentDidMount() {
    this.props.getInningSummary({
      matchId: this.props.matchId,
      inningId: (this.props.list && this.props.list.inningNumber) || "",
      teamId: (this.props.list && this.props.list.teamKey) || ""
    });
  }

  render() {
    const { list, inningSummary } = this.props;
    const teamScoreA =
      inningSummary &&
      inningSummary.length > 0 &&
      inningSummary[list.inningNumber - 1] &&
      inningSummary[list.inningNumber - 1].teamScoreA &&
      list &&
      list.inningNumber
        ? inningSummary[list.inningNumber - 1].teamScoreA
        : false;
    return (
      // NEW CODE
      teamScoreA ? (
        <div
          style={{
            background: white,
            border: "2px solid #2e3857",
            fontFamily: "Montserrat"
          }}
        >
          {/* Balck Card */}
          {
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                background: "#2e3857",
                padding: "12px 8px",
                color: "#FFF",
                fontSize: 12,
                fontFamily: "Montserrat",
                fontWeight: 500
              }}
            >
              <div
                style={{
                  display: "flex",
                  alignItems: "center"
                }}
              >
                <div
                  style={{
                    width: 24,
                    height: 16,
                    marginRight: 6
                  }}
                >
                  <img
                    src={
                      teamScoreA && teamScoreA.avatar
                        ? teamScoreA.avatar
                        : require("../../../images/flag_empty.svg")
                    }
                    alt=""
                    style={{
                      width: 24,
                      height: 16
                    }}
                  />
                </div>
                <div>{`${(teamScoreA &&
                  teamScoreA.teamName &&
                  teamScoreA.teamName.toUpperCase()) ||
                  ""}`}</div>
              </div>
              <div>
                <span
                  style={{
                    marginRight: 6,
                    fontWeight: 700
                  }}
                >
                  {`${(teamScoreA && teamScoreA.runs) || 0}${
                    teamScoreA && teamScoreA.wickets > 0
                      ? `/${teamScoreA.wickets}`
                      : ""
                  }`}
                </span>
                <span>
                  {teamScoreA && teamScoreA.overs
                    ? teamScoreA.overs.indexOf(".0") > 0
                      ? "(" + teamScoreA.overs.split(".")[0] + ")"
                      : "(" + teamScoreA.overs + ")"
                    : ""}
                </span>
              </div>
            </div>
          }
          {/* Status card */}
          {/* 1 */}
          <div
            style={{
              display: "flex",
              flex: 1,
              color: grey_10,
              fontSize: 12
            }}
          >
            <div
              style={{
                display: "flex",
                flex: 0.5,
                justifyContent: "space-between",
                padding: "0px 6px",
                margin: "12px 0",
                borderRight: "1px solid #e3e4e6"
              }}
            >
              <div>
                {(teamScoreA &&
                  teamScoreA.batsmanSummary &&
                  teamScoreA.batsmanSummary.topBatsman &&
                  teamScoreA.batsmanSummary.topBatsman.shortName) ||
                  ""}
              </div>
              <div
                style={{
                  fontWeight: 600
                }}
              >
                {(teamScoreA &&
                  teamScoreA.batsmanSummary &&
                  teamScoreA.batsmanSummary.topBatsman &&
                  teamScoreA.batsmanSummary.topBatsman.runs) ||
                  ""}
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flex: 0.5,
                justifyContent: "space-between",
                padding: "0px 6px",
                margin: "12px 0"
              }}
            >
              <div>
                {(teamScoreA &&
                  teamScoreA.bowlerSummary &&
                  teamScoreA.bowlerSummary.topBowler &&
                  teamScoreA.bowlerSummary.topBowler.shortName) ||
                  ""}
              </div>
              <div
                style={{
                  fontWeight: 600
                }}
              >
                {teamScoreA &&
                teamScoreA.bowlerSummary &&
                teamScoreA.bowlerSummary.topBowler
                  ? (teamScoreA.bowlerSummary.topBowler.wickets || 0) +
                    "/" +
                    (teamScoreA.bowlerSummary.topBowler.runs || 0)
                  : ""}
              </div>
            </div>
          </div>
          <HrLine />
          {/* 2 */}
          <div
            style={{
              display: "flex",
              flex: 1,
              color: grey_10,
              fontSize: 12
            }}
          >
            <div
              style={{
                display: "flex",
                flex: 0.5,
                justifyContent: "space-between",
                padding: "0px 6px",
                margin: "12px 0",
                borderRight: "1px solid #e3e4e6"
              }}
            >
              <div>
                {(teamScoreA &&
                  teamScoreA.batsmanSummary &&
                  teamScoreA.batsmanSummary.runnerBatsman &&
                  teamScoreA.batsmanSummary.runnerBatsman.shortName) ||
                  ""}
              </div>
              <div
                style={{
                  fontWeight: 600
                }}
              >
                {(teamScoreA &&
                  teamScoreA.batsmanSummary &&
                  teamScoreA.batsmanSummary.runnerBatsman &&
                  teamScoreA.batsmanSummary.runnerBatsman.runs) ||
                  ""}
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flex: 0.5,
                justifyContent: "space-between",
                padding: "0px 6px",
                margin: "12px 0"
              }}
            >
              <div>
                {(teamScoreA &&
                  teamScoreA.bowlerSummary &&
                  teamScoreA.bowlerSummary.runnerBowler &&
                  teamScoreA.bowlerSummary.runnerBowler.shortName) ||
                  ""}
              </div>
              <div
                style={{
                  fontWeight: 600
                }}
              >
                {teamScoreA &&
                teamScoreA.bowlerSummary &&
                teamScoreA.bowlerSummary.runnerBowler
                  ? (teamScoreA.bowlerSummary.runnerBowler.wickets || 0) +
                    "/" +
                    (teamScoreA.bowlerSummary.runnerBowler.runs || 0)
                  : ""}
              </div>
            </div>
          </div>
          {/* Status card closing */}
        </div>
      ) : (
        ""
      )

      // OLD CODE
      // <div
      //   style={{
      //     background: white,
      //     border: "2px solid #2e3857",
      //     fontFamily: "Montserrat"
      //   }}
      // >
      //   {/* Balck Card */}
      //   {
      //     <div
      //       style={{
      //         display: "flex",
      //         justifyContent: "space-between",
      //         alignItems: "center",
      //         background: "#2e3857",
      //         padding: "12px 8px",
      //         color: "#FFF",
      //         fontSize: 12,
      //         fontFamily: "Montserrat",
      //         fontWeight: 500
      //       }}
      //     >
      //       <div
      //         style={{
      //           display: "flex",
      //           alignItems: "center"
      //         }}
      //       >
      //         <div
      //           style={{
      //             width: 24,
      //             height: 16,
      //             marginRight: 6
      //           }}
      //         >
      //           <img
      //             src={
      //               list.inningNumber == 1
      //                 ? (scoreSummary &&
      //                     scoreSummary.teamScoreA &&
      //                     scoreSummary.teamScoreA.avatar) ||
      //                   require("../../../images/flag_empty.svg")
      //                 : list.inningNumber == 2
      //                   ? (scoreSummary &&
      //                       scoreSummary.teamScoreB &&
      //                       scoreSummary.teamScoreB.avatar) ||
      //                     require("../../../images/flag_empty.svg")
      //                   : require("../../../images/flag_empty.svg")
      //             }
      //             alt=""
      //             style={{
      //               width: 24,
      //               height: 16
      //             }}
      //           />
      //         </div>
      //         <div>
      //           {`${(list.inningNumber == 1 &&
      //             scoreSummary &&
      //             scoreSummary.teamScoreA &&
      //             scoreSummary.teamScoreA.teamName) ||
      //             "" ||
      //             ((list.inningNumber == 2 &&
      //               scoreSummary &&
      //               scoreSummary.teamScoreB &&
      //               scoreSummary.teamScoreB.teamName) ||
      //               "") ||
      //             ""}`}
      //         </div>
      //       </div>
      //       <div>
      //         <span
      //           style={{
      //             marginRight: 6,
      //             fontWeight: 700
      //           }}
      //         >
      //           {`${(list.inningNumber == 1 &&
      //             scoreSummary &&
      //             scoreSummary.teamScoreA &&
      //             scoreSummary.teamScoreA.runs) ||
      //             0 ||
      //             ((list.inningNumber == 2 &&
      //               scoreSummary &&
      //               scoreSummary.teamScoreB &&
      //               scoreSummary.teamScoreB.runs) ||
      //               0) ||
      //             0}${
      //             (list.inningNumber == 1 &&
      //               scoreSummary &&
      //               scoreSummary.teamScoreA &&
      //               scoreSummary.teamScoreA.wickets) > 0
      //               ? `/${scoreSummary.teamScoreA.wickets}`
      //               : (list.inningNumber == 2 &&
      //                   scoreSummary &&
      //                   scoreSummary.teamScoreB &&
      //                   scoreSummary.teamScoreB.wickets) > 0
      //                 ? `/${scoreSummary.teamScoreB.wickets}`
      //                 : ""
      //           }`}
      //           {/* 309/5 */}
      //         </span>
      //         <span>
      //           {/* ({list.overString &&
      //                       list.overString.indexOf(".6") > 0 &&
      //                       list.valid
      //                         ? list.overNo
      //                         : list.overString}) */}
      //           ({list.inningNumber == 1
      //             ? scoreSummary &&
      //               scoreSummary.teamScoreA &&
      //               scoreSummary.teamScoreA.overs &&
      //               (scoreSummary.teamScoreA.overs.indexOf(".0") > 0
      //                 ? scoreSummary.teamScoreA.overs.split(".")[0]
      //                 : scoreSummary.teamScoreA.overs)
      //             : list.inningNumber == 2
      //               ? scoreSummary &&
      //                 scoreSummary.teamScoreB &&
      //                 scoreSummary.teamScoreB.overs &&
      //                 (scoreSummary.teamScoreB.overs.indexOf(".0") > 0
      //                   ? scoreSummary.teamScoreB.overs.split(".")[0]
      //                   : scoreSummary.teamScoreB.overs)
      //               : ""})
      //           {/* ({
      //                         (list.inningNumber == 1 &&
      //                         scoreSummary &&
      //                         scoreSummary.teamScoreA &&
      //                         scoreSummary.teamScoreA.overs &&
      //                         (scoreSummary.teamScoreA.overs.indexOf(".6") > 0 ?
      //                         scoreSummary.teamScoreA.overs.split(".")[0]
      //                         : scoreSummary.teamScoreA.overs))
      //                         (list.inningNumber == 2 &&
      //                         scoreSummary &&
      //                         scoreSummary.teamScoreB &&
      //                         scoreSummary.teamScoreB.overs &&
      //                         (scoreSummary.teamScoreB.overs.indexOf(".6") > 0 ?
      //                         scoreSummary.teamScoreB.overs.split(".")[0] : scoreSummary.teamScoreB.overs))
      //                       }) */}
      //         </span>
      //       </div>
      //     </div>
      //   }
      //   {/* Status card */}
      //   {/* 1 */}
      //   <div
      //     style={{
      //       display: "flex",
      //       flex: 1,
      //       color: grey_10,
      //       fontSize: 12
      //     }}
      //   >
      //     <div
      //       style={{
      //         display: "flex",
      //         flex: 0.5,
      //         justifyContent: "space-between",
      //         padding: "0px 6px",
      //         margin: "12px 0",
      //         borderRight: "1px solid #e3e4e6"
      //       }}
      //     >
      //       <div>
      //         {(list.inningNumber == 1 &&
      //           scoreSummary &&
      //           scoreSummary.teamScoreA &&
      //           scoreSummary.teamScoreA.batsmanSummary &&
      //           scoreSummary.teamScoreA.batsmanSummary.topBatsman &&
      //           scoreSummary.teamScoreA.batsmanSummary.topBatsman.shortName) ||
      //           ""}
      //         {(list.inningNumber == 2 &&
      //           scoreSummary &&
      //           scoreSummary.teamScoreB &&
      //           scoreSummary.teamScoreB.batsmanSummary &&
      //           scoreSummary.teamScoreB.batsmanSummary.topBatsman &&
      //           scoreSummary.teamScoreB.batsmanSummary.topBatsman.shortName) ||
      //           ""}
      //       </div>
      //       <div
      //         style={{
      //           fontWeight: 600
      //         }}
      //       >
      //         {(list.inningNumber == 1 &&
      //           scoreSummary &&
      //           scoreSummary.teamScoreA &&
      //           scoreSummary.teamScoreA.batsmanSummary &&
      //           scoreSummary.teamScoreA.batsmanSummary.topBatsman &&
      //           scoreSummary.teamScoreA.batsmanSummary.topBatsman.runs) ||
      //           ""}
      //         {(list.inningNumber == 2 &&
      //           scoreSummary &&
      //           scoreSummary.teamScoreB &&
      //           scoreSummary.teamScoreB.batsmanSummary &&
      //           scoreSummary.teamScoreB.batsmanSummary.topBatsman &&
      //           scoreSummary.teamScoreB.batsmanSummary.topBatsman.runs) ||
      //           ""}
      //       </div>
      //     </div>
      //     <div
      //       style={{
      //         display: "flex",
      //         flex: 0.5,
      //         justifyContent: "space-between",
      //         padding: "0px 6px",
      //         margin: "12px 0"
      //       }}
      //     >
      //       <div>
      //         {(list.inningNumber == 1 &&
      //           scoreSummary &&
      //           scoreSummary.teamScoreA &&
      //           scoreSummary.teamScoreA.bowlerSummary &&
      //           scoreSummary.teamScoreA.bowlerSummary.topBowler &&
      //           scoreSummary.teamScoreA.bowlerSummary.topBowler.shortName) ||
      //           ""}
      //         {(list.inningNumber == 2 &&
      //           scoreSummary &&
      //           scoreSummary.teamScoreB &&
      //           scoreSummary.teamScoreB.bowlerSummary &&
      //           scoreSummary.teamScoreB.bowlerSummary.topBowler &&
      //           scoreSummary.teamScoreB.bowlerSummary.topBowler.shortName) ||
      //           ""}
      //       </div>
      //       <div
      //         style={{
      //           fontWeight: 600
      //         }}
      //       >
      //         {list.inningNumber == 1 &&
      //         scoreSummary &&
      //         scoreSummary.teamScoreA &&
      //         scoreSummary.teamScoreA.bowlerSummary &&
      //         scoreSummary.teamScoreA.bowlerSummary.topBowler
      //           ? (scoreSummary.teamScoreA.bowlerSummary.topBowler.wickets ||
      //               0) +
      //             "/" +
      //             (scoreSummary.teamScoreA.bowlerSummary.topBowler.runs || 0)
      //           : ""}
      //         {list.inningNumber == 2 &&
      //         scoreSummary &&
      //         scoreSummary.teamScoreB &&
      //         scoreSummary.teamScoreB.bowlerSummary &&
      //         scoreSummary.teamScoreB.bowlerSummary.topBowler
      //           ? (scoreSummary.teamScoreB.bowlerSummary.topBowler.wickets ||
      //               0) +
      //             "/" +
      //             (scoreSummary.teamScoreB.bowlerSummary.topBowler.runs || 0)
      //           : ""}

      //         {/* 2/43 */}
      //       </div>
      //     </div>
      //   </div>
      //   <HrLine />
      //   {/* 2 */}
      //   <div
      //     style={{
      //       display: "flex",
      //       flex: 1,
      //       color: grey_10,
      //       fontSize: 12
      //     }}
      //   >
      //     <div
      //       style={{
      //         display: "flex",
      //         flex: 0.5,
      //         justifyContent: "space-between",
      //         padding: "0px 6px",
      //         margin: "12px 0",
      //         borderRight: "1px solid #e3e4e6"
      //       }}
      //     >
      //       <div>
      //         {(list.inningNumber == 1 &&
      //           scoreSummary &&
      //           scoreSummary.teamScoreA &&
      //           scoreSummary.teamScoreA.batsmanSummary &&
      //           scoreSummary.teamScoreA.batsmanSummary.runnerBatsman &&
      //           scoreSummary.teamScoreA.batsmanSummary.runnerBatsman
      //             .shortName) ||
      //           ""}
      //         {(list.inningNumber == 2 &&
      //           scoreSummary &&
      //           scoreSummary.teamScoreB &&
      //           scoreSummary.teamScoreB.batsmanSummary &&
      //           scoreSummary.teamScoreB.batsmanSummary.runnerBatsman &&
      //           scoreSummary.teamScoreB.batsmanSummary.runnerBatsman
      //             .shortName) ||
      //           ""}
      //       </div>
      //       <div
      //         style={{
      //           fontWeight: 600
      //         }}
      //       >
      //         {(list.inningNumber == 1 &&
      //           scoreSummary &&
      //           scoreSummary.teamScoreA &&
      //           scoreSummary.teamScoreA.batsmanSummary &&
      //           scoreSummary.teamScoreA.batsmanSummary.runnerBatsman &&
      //           scoreSummary.teamScoreA.batsmanSummary.runnerBatsman.runs) ||
      //           ""}
      //         {(list.inningNumber == 2 &&
      //           scoreSummary &&
      //           scoreSummary.teamScoreB &&
      //           scoreSummary.teamScoreB.batsmanSummary &&
      //           scoreSummary.teamScoreB.batsmanSummary.runnerBatsman &&
      //           scoreSummary.teamScoreB.batsmanSummary.runnerBatsman.runs) ||
      //           ""}
      //       </div>
      //     </div>
      //     <div
      //       style={{
      //         display: "flex",
      //         flex: 0.5,
      //         justifyContent: "space-between",
      //         padding: "0px 6px",
      //         margin: "12px 0"
      //       }}
      //     >
      //       <div>
      //         {(list.inningNumber == 1 &&
      //           scoreSummary &&
      //           scoreSummary.teamScoreA &&
      //           scoreSummary.teamScoreA.bowlerSummary &&
      //           scoreSummary.teamScoreA.bowlerSummary.runnerBowler &&
      //           scoreSummary.teamScoreA.bowlerSummary.runnerBowler.shortName) ||
      //           ""}
      //         {(list.inningNumber == 2 &&
      //           scoreSummary &&
      //           scoreSummary.teamScoreB &&
      //           scoreSummary.teamScoreB.bowlerSummary &&
      //           scoreSummary.teamScoreB.bowlerSummary.runnerBowler &&
      //           scoreSummary.teamScoreB.bowlerSummary.runnerBowler.shortName) ||
      //           ""}
      //       </div>
      //       <div
      //         style={{
      //           fontWeight: 600
      //         }}
      //       >
      //         {list.inningNumber == 1 &&
      //         scoreSummary &&
      //         scoreSummary.teamScoreA &&
      //         scoreSummary.teamScoreA.bowlerSummary &&
      //         scoreSummary.teamScoreA.bowlerSummary.runnerBowler
      //           ? (scoreSummary.teamScoreA.bowlerSummary.runnerBowler.wickets ||
      //               0) +
      //             "/" +
      //             (scoreSummary.teamScoreA.bowlerSummary.runnerBowler.runs || 0)
      //           : ""}
      //         {list.inningNumber == 2 &&
      //         scoreSummary &&
      //         scoreSummary.teamScoreB &&
      //         scoreSummary.teamScoreB.bowlerSummary &&
      //         scoreSummary.teamScoreB.bowlerSummary.runnerBowler
      //           ? (scoreSummary.teamScoreB.bowlerSummary.runnerBowler.wickets ||
      //               0) +
      //             "/" +
      //             (scoreSummary.teamScoreB.bowlerSummary.runnerBowler.runs || 0)
      //           : ""}
      //       </div>
      //     </div>
      //   </div>
      //   {/* Status card closing */}
      // </div>
    );
  }
}

export default InningSeparator2;
