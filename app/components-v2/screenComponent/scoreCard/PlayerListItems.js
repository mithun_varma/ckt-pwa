import React, { Component } from "react";
import { grey_10, avatarShadow } from "../../../styleSheet/globalStyle/color";
import HrLine from "../../commonComponent/HrLine";

export class PlayerListItems extends Component {
  render() {
    const { title, avatar, rightElement, onClick, index } = this.props;
    return (
      <div>
        <div
          style={{
            padding: "12px 16px",
            display: "flex",
            flex: 1,
            alignItems: "center",
            justifyContent: "space-between"
          }}
          onClick={() => onClick && onClick(index)}
        >
          <div style={{ display: "flex", alignItems: "center" }}>
            <div
              style={{
                width: 24,
                height: 18,
                backgroundImage: `url(${avatar})`,
                backgroundSize: "cover",
                backgroundPosition: "top",
                backgroundRepeat: "no-repeat",
                marginRight: 12,
                boxShadow: avatarShadow,
                ...this.props.avatarStyles
              }}
            />

            <div
              style={{
                fontFamily: "Montserrat",
                fontSize: 12,
                color: grey_10,
                ...this.props.titleStyles
              }}
            >
              {title}
            </div>
          </div>
          <div style={{ display: "flex" }}>
            {rightElement}
            {/* <ExpandLess /> */}
          </div>
        </div>
        <HrLine />
      </div>
    );
  }
}

export default PlayerListItems;
