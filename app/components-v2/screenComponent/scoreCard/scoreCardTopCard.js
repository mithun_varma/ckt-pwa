import React, {Component} from 'react';
import { screenWidth, screenHeight } from '../../../styleSheet/screenSize/ScreenDetails';
import Polling from './../../commonComponent/Polling';
import IndiaIcon from './../../../styleSheet/svg/IndiaIcon.png';

export default class ScoreCardTop extends Component{
    constructor(props){
        super(props);

    }
    componentDidMount(){

    }
    render(){
        const {prediction,forTopScoreCard} = this.props;
        const collectionOfAllInning = this.props.collectionOfAllInning;
        const data = collectionOfAllInning[0];
        const dataArray = Object.keys(data).map(function(key) { return [Number(key), data[key]] });
        const requiredToWinIf = prediction.summary;
        const firstTeamFlag = IndiaIcon;

        return(
            <div style={pageStyle.container}>
                <div style={{display:'flex', flex:1,flexDirection:'column',color:'#000', fontSize:'12px'}}>
                    {    
                    dataArray.map((data)=>{
                        {
                        return(
                        <div style={pageStyle.flagFirstConatiner}>
                            <div style={pageStyle.flagRow}>
                                <img src={firstTeamFlag} height='auto' width="30px" style={{paddingRight:'5px'}}/>
                                {(data[1].teamKey).toUpperCase()}
                            </div>
                            <div style={pageStyle.flagScore}>
                                {data[1].runs+'/'+data[1].wickets}
                            </div>
                            <div style={{display:'flex', flex:0.50}}>
                                {null}
                            </div>
                        </div>
                        )

                        }
                        
                    })
                
                    }

                </div>

         
            <div style={pageStyle.requiredToWin}>
                {requiredToWinIf}
            </div>
            <div style={pageStyle.winningPercentage}>
                <Polling data={prediction} />
            </div>
            </div>
        );
    }
    live(isActive){
        if(isActive){
            return(
                <div style={pageStyle.liveContainer}>
                    <div style={{display:'flex', flex:0.59}}>
                        {null}
                    </div>
                    <div style={pageStyle.liveButton}>
                        {null}
                    </div>
                    <div style={pageStyle.live}>
                        LIVE
                    </div>
                </div>
            );
        }
        else{
            return(
                <div>
                    {null}

                </div>
            );
        }
    }

}

const pageStyle={
    requiredToWin:{
        display:'flex',
        flex:1,
        width:screenWidth*0.66,
        backgroundColor:'#FFF1E9',
        borderRadius:'10px',
        color:'#141b2f',
        fontSize:'12px',
        wrap:'flex-wrap',
        marginLeft:screenWidth*0.15,
        marginTop:'10px',
        justifyContent:'center',
        alignItems:'center',
        

    },
    liveContainer:{
        display:'flex',
        flex:1,
        backgroundColor:'#fdfdfd',
        fontSize:'9px',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
    },
    live:{
       color:'#4a4a4a',
       letterSpacing:'0.36px',
        display:'flex',
        justifyContent:'center',
        flex:0.40,

    },
    liveImage:{
        width:'12px',
        height:'12px',
        borderRadius:'6px',
        backgroundColor:'#84c71a',
        display:'flex',
        justifyContent:'center',
        flex:0.50,

    },
    container:{
        display:'flex',
        flexDirection:'column',
        flex:1,
        width:screenWidth*0.911,
        height:screenHeight*0.30,
        backgroundColor:'#fff',
        boxShadow:'0px 3px 6px 0px #13000000', 
    },
    flagFirstConatiner:{
        display:'flex',
        flexDirection:'row',
        flex:1,
    },
    flagRow:{
        display:'flex',
        flex:0.25,
        justifyContent:'flex-start',
        padding:'4px 0px 4px 16px',
        fontSize:'14px',
        color:'#141b2f',
    },
    flagScore:{
        display:'flex',
        flex:0.25,
        justifyContent:'flex-start',
        padding:'4px 0px 4px 16px',
        fontSize:'14px',
    },
    flagScoreSecond:{
        display:'flex',
        flex:0.75,
        justifyContent:'flex-start',
        padding:'4px 0px 4px 16px',
        fontSize:'14px',
    },
    liveButton:{
        display:'flex',
        flex:0.001,
        justifyContent:'center',
        padding:'4px 0px 4px 16px',
        height:'10px',
        width:'10px',
        borderRadius:'10px',
        backgroundColor:'#84c71a',

    }


}