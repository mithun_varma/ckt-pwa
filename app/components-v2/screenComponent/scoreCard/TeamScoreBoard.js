/**
 *
 * Header
 *
 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-non-interactive-element-interact: 0 */
/* eslint jsx-a11y/anchor-is-valid: 0 */
/* eslint react-no-danger: 0 */

import React from "react";
import PropTypes from "prop-types";
// import ChevronRight from '@material-ui/icons/ChevronRight';
import ExpandMore from "@material-ui/icons/ExpandMore";
import ExpandLess from "@material-ui/icons/ExpandLess";
import CustomAccordionTitle from "./CustomAccordionTitle";
import CustomAccordionBody from "./CustomAccordionBody";
import EmptyState from "../../commonComponent/EmptyState";
// import styled from 'styled-components';
const inningsConstant = {
  "1": "1st innings",
  "2": "2nd innings",
  "3": "3rd innings",
  "4": "4th innings",
  superover: "Superover"
};

class TeamScoreBoard extends React.Component {
  constructor(props) {
    super(props);
    const toggle = [true, false, false, false];
    // toggle[this.props.score.battingOrder.length - 1] = true;
    this.state = {
      toggle,
      toggleOverview: false,
      // details: false,
      initial: true,
      openTab: {
        0: true
      }
    };
  }

  static getDerivedStateFromProps = (nextProps, state) => {
    if (state.initial) {
      return {
        initial: false,
        openTab: {
          [nextProps.score &&
          nextProps.score.battingOrder &&
          nextProps.score.battingOrder.length > 0
            ? nextProps.score.battingOrder.length - 1
            : "0"]: true
        }
      };
    }
    return null;
  };

  onClick = e => {
    this.setState({
      ...this.state,
      openTab: {
        ...this.state.openTab,
        [e]: !this.state.openTab[e]
      }
    });
    // const toggle = this.state.toggle.slice();
    // [...Array(10)].map((val, i) => {
    //   if (e === i) {
    //     toggle[e] = !this.state.toggle[e];
    //   } else {
    //     toggle[i] = false;
    //   }
    //   return null;
    // });
    // this.setState({
    //   toggle
    // });
  };
  componentDidMount() {
    // this.props.fetchCommentary({ matchId: this.props.matchId });
  }
  render() {
    const { score } = this.props;
    // console.log("TeamsScore: ", score);
    if (!score || !score.battingOrder) return <div />;
    return (
      <React.Fragment>
        {score && score.battingOrder && score.battingOrder.length > 0 ? (
          score.battingOrder.map((order, index) => {
            const key = `${order[0]}_${order[1]}`;
            const inning = score.innings[key];
            return (
              // <AccordionItem expanded key={key}>
              <div
                role="tabpanel"
                className="tab-pane fade"
                id="scoreboard"
                key={key}
              >
                <div className="accordion tscore-accordion">
                  <div className="">
                    <CustomAccordionTitle
                      score={score}
                      order={order}
                      inningsConstant={inningsConstant}
                      inning={inning}
                      index={index}
                      // toggle={this.state.toggle}
                      toggle={this.state.openTab[index]}
                      onClick={this.onClick}
                      inningNumber={
                        score.battingOrder.length > 2
                          ? inning.inningId == 1
                            ? "1st"
                            : inning.inningId == 2
                              ? "2nd"
                              : ""
                          : ""
                      }
                    />
                    <div
                      style={{
                        // display: this.state.toggle[index] ? "block" : "none"
                        display: this.state.openTab[index] ? "block" : "none"
                      }}
                    >
                      <CustomAccordionBody
                        history={this.props.history}
                        score={score}
                        inning={inning}
                        order={order}
                      />
                    </div>
                  </div>
                </div>
              </div>
            );
          })
        ) : (
          <div>
            <EmptyState
              img={require("../../../images/emptyState.svg")}
              imageStyle={{
                width: "40%",
                height: "auto"
              }}
              msg="No scorecard available so far"
              msgStyle={{
                marginTop: 10
              }}
            />
          </div>
        )}
        {false && (
          <div
            className="tab-pane fade"
            role="presentation"
            onClick={e => {
              e.preventDefault();
              this.setState({ toggleOverview: !this.state.toggleOverview });
            }}
          >
            <div className="accordion score-accordion">
              <div className="card">
                <div
                  className={
                    this.state.toggleOverview
                      ? "accorHeader active"
                      : "accorHeader"
                  }
                >
                  <div className="squardList">
                    <div className="squardList__content">
                      <div className="squardList__list">
                        <span>
                          <strong>Match Details</strong>
                        </span>
                      </div>
                      <div className="squardList__list">
                        <span className="squardList__list__icon">
                          {this.state.toggleOverview ? (
                            <ExpandLess style={{ color: "red" }} />
                          ) : (
                            <ExpandMore style={{ color: "red" }} />
                          )}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="accorCcntent"
                style={{
                  display: this.state.toggleOverview ? "block" : "none"
                }}
              >
                {score && (
                  <div className="table-responsive">
                    <div className="matchDetails">
                      <ul>
                        <li>
                          <span className="name">Venue</span>
                          <span className="description">{score.venue}</span>
                        </li>
                        <li>
                          <span className="name">Series</span>
                          <span className="description">
                            <a href="#">{score.season && score.season.name}</a>
                          </span>
                        </li>
                        <li>
                          <span className="name">Match</span>
                          <span className="description">{score.name}</span>
                        </li>
                        <li>
                          <span className="name">Toss</span>
                          <span className="description">
                            {/* {score.teams[score.toss.won].name}, elected to {score.toss.decision}
                          &nbsp;first */}
                            test
                          </span>
                        </li>
                      </ul>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}

TeamScoreBoard.propTypes = {
  score: PropTypes.object.isRequired
};

export default TeamScoreBoard;
