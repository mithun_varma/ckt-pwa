import React, {Component} from 'react';
import BatsmanScoreCard from './../../commonComponent/BatsmanScoreBoard';
import BowlerScoreCard from './../../commonComponent/BowlerScoreBoard';
import { screenWidth, screenHeight } from '../../../styleSheet/screenSize/ScreenDetails';
import { gradientOrange } from '../../../styleSheet/globalStyle/color';
import openIconBlack from './../../../styleSheet/svg/openIconBlack.svg';

export default class InningScoreCard extends Component{
    constructor(props){
        super(props);
        this.state={
            isCardOpen:false,

        }
        this._handelCardOpenClick=this._handelCardOpenClick.bind(this);


    }
    componentDidMount(){

    }
    _handelCardOpenClick(){
        this.setState({isCardOpen:!this.state.isCardOpen});
    }
    render(){
        const {extraField} = this.props;
        const {collectionOfAllInning} = this.props;
        // console.log('--------------------collectionOfAllInning--------------',collectionOfAllInning);
        const playerStatsViews = this.props.playerStatsViews;
        var batsmanScore = [];
        var bowlerScore = [];
        const battingOrder = collectionOfAllInning[1]['battingOrder'];
        const bowlingOrder = collectionOfAllInning[1]['bowlingOrder'];

        for(let i = 0; i<battingOrder.length ; i++){
            batsmanScore.push(playerStatsViews[battingOrder[i]]);
        }

        for (let j=0; j<11; j++){
            bowlerScore.push(playerStatsViews[battingOrder[j]]);
        }
        var style = pageStyle.unActiveTeam;
        if(this.state.isCardOpen){
            style = pageStyle.activeTeam;
        }
        return(
            <div style={pageStyle.teamContainer} onClick={this._handelCardOpenClick}>
                <div style={style}>
                    <div style={{display:'flex', flex:0.70, justifyContent:"flex-start", paddingLeft:'16px'}}>
                        {(collectionOfAllInning[1].teamKey).toUpperCase()}
                    </div>
                    <div style={{display:'flex', flex:0.20, justifyContent:'center'}}>
                        {collectionOfAllInning[1].runs +"/"+ collectionOfAllInning[1].wickets}
                    </div>
                    <div style={{display:'flex', flex:0.10, justifyContent:'center'}}>
                        <img src={openIconBlack} width="12px" height='12px' />
                    </div>
                </div>
                <div style={pageStyle.teamScoreDetails}>
                    {
                        (this.state.isCardOpen)?
                        <div style={pageStyle.scoreCardDetails}>
                            <BatsmanScoreCard  batsmanScore ={batsmanScore} extraField={collectionOfAllInning}/>
                            <BowlerScoreCard  bowlerScore={bowlerScore} extraField={collectionOfAllInning}/>
                        </div>
                        :
                        <div>
                            {null}
                        </div>
                    }
                </div>

            </div>
        );
    }

}

const pageStyle={
    scoreCardDetails:{
        display:'flex',
        flexDirection:'column',
        flex:1,

    },
    teamContainer:{
        display:"flex",
        flexDirection:'column',
        fontSize:'14px',
        fontWeight:'700',

    },
    scoreCardContainer:{
        display:'flex',
        flex:1,
        flexContainer:'column',
        width:screenWidth*0.911,
        marginTop:'5px',
    },
    activeTeam:{
        display:'flex',
        flex:1,
        width:screenWidth*0.911,
        height:screenHeight*0.070,
        backgroundImage:gradientOrange,
        color:'#fff',
        fontSize:'12px',
        flexDirection:'row',
        alignItems:'center',
    },
    unActiveTeam:{
        display:'flex',
        flex:1,
        width:screenWidth*0.911,
        height:screenHeight*0.070,
        backgroundColor:'#fff',
        color:'#141b2f',
        fontSize:'12px',
        flexDirection:'row',
        alignItems:'center',

    }
}