import React, { Component} from 'react';
import { screenWidth, screenHeight } from '../../../styleSheet/screenSize/ScreenDetails';
import TopHeader from './../../commonComponent/TopHeader';
import ScoreCardTop from './scoreCardTopCard';
import ScoreCardTeamDetails from './scoreCardTeamDetails';
export default class ScoreCardDetails extends Component{
    constructor(props){
        super(props);
        this.state={

        }

    }
    componentDidMount(){

    }
    render(){
            const {scoreDetailsFull} = this.props;
            // console.log('coming from details', scoreDetailsFull);
            const matchName = scoreDetailsFull.matchName;
            const matchMiniScoreCard=scoreDetailsFull['fullScorecard']['matchMiniScorecard'];
            const currentScoreCard = matchMiniScoreCard['currentScoreCard']; // not using cuurent data.
            const prediction= matchMiniScoreCard['prediction'];
            const playingOrderKey = matchMiniScoreCard['inningOrder'];
            const inningCollection = matchMiniScoreCard['inningCollection'];
            const playerStatsViews = scoreDetailsFull['playerStatsViews'];
            // starting the inning 
            var inning1Key="";
            var inning2Key="";
            var inning3Key="";
            var inning4Key="";
            var collectionOfAllInning =[];

            // one inning 
            if(playingOrderKey.length<2){
                inning1Key=playingOrderKey[0];
                const inningScore1 = inningCollection[inning1Key];
                collectionOfAllInning =[
                    {
                        1:inningScore1,
                    }
                ]
            }
            else if (playingOrderKey.length <3){
                inning1Key = playingOrderKey[0];
                inning2Key = playingOrderKey[1];
                const inningScore1 = inningCollection[inning1Key];
                const inningScore2 = inningCollection[inning2Key];
                collectionOfAllInning =[
                    {
                        1:inningScore1,
                        2:inningScore2,
                    }
                ]

            }
            else if(playingOrderKey.length <4){
                inning1Key = playingOrderKey[0];
                inning2Key = playingOrderKey[1];
                inning3Key = playingOrderKey[2];
                const inningScore1 = inningCollection[inning1Key];
                const inningScore2 = inningCollection[inning2Key];
                const inningScore3 = inningCollection[inning3Key];
                collectionOfAllInning =[
                    {
                        1:inningScore1,
                        2:inningScore2,
                        3:inningScore3,
                    }
                ]
            }
            else if (playingOrderKey.length <5){
                inning1Key = playingOrderKey[0];
                inning2Key = playingOrderKey[1];
                inning3Key = playingOrderKey[2];
                inning4Key = playingOrderKey[3];
                const inningScore1 = inningCollection[inning1Key];
                const inningScore2 = inningCollection[inning2Key];
                const inningScore3 = inningCollection[inning3Key];
                const inningScore4 = inningCollection[inning4Key];
                collectionOfAllInning =[
                    {
                        1:inningScore1,
                        2:inningScore2,
                        3:inningScore3,
                        4:inningScore4,
                    }
                ]
            }

            const venueDetails=[
                {
                    'venue':scoreDetailsFull.venue,
                    'seriesName':scoreDetailsFull.seriesName,
                    'matchName':scoreDetailsFull.matchName,
                    'toss':matchMiniScoreCard.toss,

                },
            ]

            const forTopScoreCard=
            {
                'inningCollection':matchMiniScoreCard['inningCollection'],
                'inningOrder':matchMiniScoreCard['inningOrder'],
            }
            // starting live data
            const liveData = scoreDetailsFull;


            // starting Higlightsdata
            const highlightData=[]
        
            if(scoreDetailsFull != 'null'){
                return(
                    <div style={pageStyle.conatiner}>
                        <div style={pageStyle.topHeaderContainer}>
                            <TopHeader type='black' title={matchName} />
                        </div>
                        <div style={pageStyle.topScoreCard}>
                            <ScoreCardTop collectionOfAllInning={collectionOfAllInning}  prediction={prediction} forTopScoreCard={forTopScoreCard}/>
                        </div>
                        <div style={pageStyle.scoreCardContainer}>
                            <ScoreCardTeamDetails playerStatsViews={playerStatsViews} collectionOfAllInning={collectionOfAllInning} allField={forTopScoreCard} venueDetails={venueDetails} live={liveData} highlights={highlightData} commentaryData={this.props.commentaryData}/>
                        </div>
                    </div>
                );
            }
            else{
                return(null);
            }

        
    }

}

const pageStyle ={
    conatiner:{
        display:'flex',
        flexDirection:'column',
        flex:1
    },
    topHeaderContainer:{
        display:'flex',
        flex:1,
        width:screenWidth*1.0,
    },
    topScoreCard:{
        display:'flex',
        flex:1,
        width:screenWidth*0.911,
        marginLeft: screenWidth*0.044,
        marginTop:-screenHeight*0.082,
    },
    scoreCardContainer:{
        display:'flex',
        flex:1,
        width:screenWidth*0.911,
        marginTop:'50px',
        marginLeft:screenWidth*0.044,
        
    }

}