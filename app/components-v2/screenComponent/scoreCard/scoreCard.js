import React,{ Component} from 'react';
import { screenWidth } from '../../../styleSheet/screenSize/ScreenDetails';
import MatchDetailsScoreCard from './../../commonComponent/MatchDeatilsScoreCard';
import InningScoreCard from './inningScoreCard';

export default class ScoreCard extends Component{
    constructor(props){
        super(props);
        this.state={
            team1Active:true,
            team2Active:false,
        }

    }
    componentDidMount(){

    }
    render(){
        //start
        const {extraField} = this.props;
        const {venueDetails } = this.props;
        
        const data = this.props.collectionOfAllInning[0];
        const dataArray = Object.keys(data).map(function(key) { return [Number(key), data[key]] });
        // console.log(teamAData,extraField,venueDetails);

        return(
          <div style={pageStyle.scoreCardContainer}>
                <div>
                    {
                        dataArray.map((data)=>{
                            return(
                                <InningScoreCard playerStatsViews={this.props.playerStatsViews} collectionOfAllInning={data} extraField={extraField}/>
                            );
                        })
                    
                    }
                </div>
                <div>
                    <MatchDetailsScoreCard  venueDetails={venueDetails}/>
                </div>
        </div>
        );
    }

}

const pageStyle={
    scoreCardDetails:{
        display:'flex',
        flexDirection:'column',
        flex:1,

    },
    teamContainer:{
        display:"flex",
        flexDirection:'column',
        // flex:1,
        fontSize:'12px',

    },
    scoreCardContainer:{
        display:'flex',
        flex:1,
        flexDirection:'column',
        width:screenWidth*0.911,
        marginTop:'5px',
    },
}