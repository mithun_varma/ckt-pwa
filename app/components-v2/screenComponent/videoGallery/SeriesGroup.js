import React, { Component } from 'react'
import DateView from "../components-ui/DateView";
import HrLine from "../components/commonComponent/HrLine";
import SeriesInfo from "../components-ui/SeriesInfo";

export class SeriesGroup extends Component {
    constructor(props) {
        
        super(props)
    }
    render() {
        
        const {date, series} = this.props.data
        return (
            <div>
                <DateView content={date} />
                <HrLine />
                {
                    (series!=null && series.map(data => (
                        <div>
                        <SeriesInfo data={data}/>
                        <HrLine />
                        </div>
                    )))
                }
            </div>
        )
    }
}

export default SeriesGroup