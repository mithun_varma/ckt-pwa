import React, { Component } from "react";
import {
  screenWidth,
  screenHeight
} from "../styleSheet/screenSize/ScreenDetails";
import {
  white,
  orangeColor,
  cardShadow
} from "../styleSheet/globalStyle/color";
import HrLine from "../components/commonComponent/HrLine";

export class TrendingVideo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      thumbnail: this.props.thumbnail,
      caption: this.props.caption
    };
  }
  render() {
    return (
      <div style={pageStyle.cardStyle}>
        <div style={pageStyle.cardHeadingStyle}>Trending</div>
        <HrLine />
        <div style={pageStyle.bigImageStyle}>
          <div style={pageStyle.imageBackGroundstyle}> </div>
          <div style={pageStyle.playButtonStyle}>
            <img
              width={screenWidth * 0.111}
              height={screenWidth * 0.111}
              src={require("../images/playButton.png")}
            />
          </div>
          {this.state.thumbnail != null ? (
            <img
              width={screenWidth * 0.822}
              height={screenWidth * 0.37}
              src={this.state.thumbnail}
            />
          ) : (
            <div />
          )}
        </div>
        <div style={pageStyle.captionStyle}>{this.state.caption}</div>
      </div>
    );
  }
}

const pageStyle = {
  cardStyle: {
    marginTop: screenWidth * 0.05,
    marginLeft: screenWidth * 0.044,
    width: screenWidth * 0.911,
    height: screenWidth * 0.667,
    borderRaduis: screenWidth * 0.0084,
    backgroundColor: white,
    boxShadow: cardShadow,
    paddingTop: screenWidth * 0.0388
  },
  cardHeadingStyle: {
    display: "flex",
    flexDirection: "row",
    marginLeft: screenWidth * 0.044,
    fontSize: screenWidth * 0.0333,
    fontFamily: "Montserrat-Medium",
    justifyContent: "space-between",
    marginBottom: screenWidth * 0.0388,
    alignItems: "center"
  },
  bigImageStyle: {
    display: "flex",
    marginTop: screenWidth * 0.034,
    // height: "130px",
    // width: "296px",
    marginLeft: screenWidth * 0.044,
    marginRight: screenWidth * 0.044,
    position: "relative",
    marginBottom: screenWidth * 0.02
  },
  imageBackGroundstyle: {
    position: "absolute",
    top: "0px",
    bottom: "0px",
    right: "0px",
    left: "0px",
    background: "linear-gradient(to right, #a63ba1, #00fa9441)",
    opacity: "0.3"
  },
  playButtonStyle: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    marginLeft: screenWidth * 0.356,
    marginTop: screenWidth * 0.125
  },
  captionStyle: {
    display: "flex",
    flexWrap: "wrap",
    marginLeft: screenWidth * 0.044,
    marginBottom: screenWidth * 0.033,
    marginRight: screenWidth * 0.12,
    fontSize: screenWidth * 0.0278,
    letterSpacing: screenWidth * 0.000833,
    fontFamily: "Montserrat-Regular",
    color: "#141b2f"
  }
};
export default TrendingVideo;
