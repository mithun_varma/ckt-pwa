import React, { Component } from 'react'
import { screenWidth } from '../styleSheet/screenSize/ScreenDetails'
import { white, cardShadow } from "../styleSheet/globalStyle/color";

export class MediumVideoCard extends Component {
    constructor(props) {
        
        super(props)
        this.state = { thumbnail: this.props.thumbnail, caption: this.props.caption }
    }
    render() {
        return (
            <div >
                <div style={pageStyle.bigImageStyle}>
                    <div style={pageStyle.imageBackGroundstyle} > </div>
                    <div style={pageStyle.playButtonStyle}>
                        <img width={screenWidth * 0.08333} height={screenWidth * 0.08333} src={require("../images/playButton.png")} />
                        </div>
                        <div style={{marginRight: screenWidth*0.03333}} >
                        {(this.state.thumbnail != null) ? <img width="100%" height="100%" src={this.state.thumbnail} /> : <div />}
                        </div>
                </div>
                <div style={pageStyle.captionStyle}>
                    {this.state.caption}
                </div>
            </div>
        )
    }
}

const pageStyle = {
    cardStyle: {
        marginTop: screenWidth * 0.05,
        marginLeft: screenWidth * 0.044,
        width: screenWidth * 0.911,
        height: screenWidth * 0.667,
        borderRaduis: screenWidth * 0.0084,
        backgroundColor: white,
        boxShadow: cardShadow,
        paddingTop: screenWidth * 0.0388
    },
    cardHeadingStyle: {
        display: "flex",
        flexDirection: "row",
        marginLeft: screenWidth * 0.044,
        fontSize: screenWidth * 0.0333,
        fontFamily: "Montserrat-Medium",
        justifyContent: "space-between",
        marginBottom: screenWidth * 0.0388,
        alignItems: "center"
    },
    bigImageStyle: {
        display: "flex",
        marginTop: screenWidth * 0.034,
        // height: "130px",
        // width: "296px",
        // marginLeft: screenWidth * 0.044,
        // marginRight: screenWidth * 0.0334,
        position: "relative",
        marginBottom: screenWidth * 0.02,
        // width: screenWidth*0.527,
        // height: screenWidth*0.3056,
        // paddingRight: screenWidth*0.1

        // width: screenWidth*0.527,
        // height: screenWidth*0.3056,
        // paddingLeft: screenWidth*0.1
        // zIndex:"3",
        // opacity: ".3"
    },
    imageBackGroundstyle: {
        position: "absolute",
        top: "0px",
        bottom: "0px",
        right: screenWidth*0.0333,
        left: "0px",
        // screenWidth*0.1
        background: "linear-gradient(to right, #a63ba1, #00fa9441)",
        opacity: "0.3"
    },
    playButtonStyle: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        marginLeft: screenWidth*0.240,
        marginTop: screenWidth*0.1111,
    },
    captionStyle: {
        display: "flex",
        flexWrap: "wrap",
        // marginLeft: screenWidth * 0.044,
        marginBottom: screenWidth * 0.05,
        marginRight: screenWidth * 0.2,
        fontSize: screenWidth*0.0278,
        letterSpacing: screenWidth*0.000833,
        fontFamily: "Montserrat-Regular",
        color: "#141b2f"
    }
}
export default MediumVideoCard;