import React, { Component } from 'react'
import { screenWidth } from '../styleSheet/screenSize/ScreenDetails';
import { gradientBlackGrey } from '../styleSheet/globalStyle/color';

export class DateView extends Component {
    constructor(props) {
        super(props)
        this.state = {content: props.content}
    }
    render() {
        return (
            <div style={pageStyle.dateStyle}>
                <div style={pageStyle.backgroundStyle}></div>
                {this.state.content}
            </div>
        )
    }
}

const pageStyle = {
    dateStyle: {
        position: "relative",
        height: screenWidth*0.10278,
        width: screenWidth*0.911,
        paddingLeft: screenWidth*0.044,
        paddingTop: screenWidth*0.033,
        fontFamily: "Montserrat-Regular",
        fontSize: screenWidth*0.02778,
        textTransform: "uppercase",
        color: "#727682"
    },
    backgroundStyle: {
        position: "absolute",
        top: "0px",
        bottom: "0px",
        right: "0px",
        left: "0px",
        background: gradientBlackGrey,
        opacity: "0.3"
    }
}

export default DateView