import React, { Component } from 'react'
import { screenWidth } from '../styleSheet/screenSize/ScreenDetails'
import TrendingVideo from './TrendingVideo'
import LatestVideos from './LatestVideos'
import OtherVideos from './OtherVideos'

const VideoData = {
    "caption": "IND vs AUS ist Test day 5 live scores and Commentry 2018 Series",
    "thumbnail": "https://www.hindustantimes.com/rf/image_size_960x540/HT/p2/2018/11/24/Pictures/cricket-aus-ind_6b610778-efb6-11e8-8d4e-8c144d313201.jpg"

}

const latestVideos = [
    {
        "caption": "IND vs ENG ist Test",
        "thumbnail": "https://www.hindustantimes.com/rf/image_size_960x540/HT/p2/2018/11/24/Pictures/cricket-aus-ind_6b610778-efb6-11e8-8d4e-8c144d313201.jpg"
    },
    {
        "caption": "IND vs PAK Ist ODI",
        "thumbnail": "https://www.hindustantimes.com/rf/image_size_960x540/HT/p2/2018/11/24/Pictures/cricket-aus-ind_6b610778-efb6-11e8-8d4e-8c144d313201.jpg"
    },
    {
        "caption": "IND vs AUS Ist ODI",
        "thumbnail": "https://www.hindustantimes.com/rf/image_size_960x540/HT/p2/2018/11/24/Pictures/cricket-aus-ind_6b610778-efb6-11e8-8d4e-8c144d313201.jpg"
    },
    {
        "caption": "PAK vs AUS Ist ODI",
        "thumbnail": "https://www.hindustantimes.com/rf/image_size_960x540/HT/p2/2018/11/24/Pictures/cricket-aus-ind_6b610778-efb6-11e8-8d4e-8c144d313201.jpg"
    }
]

export class VideoGallery extends Component {
    constructor(props) {
        super(props)
        this.state = { data: VideoData }
    }
    render() {
        
        return (
            <div style={{paddingBottom: screenWidth*0.03333, paddingTop: screenWidth * 0.1333}}>
                <div style={pageStyle.headerStyle} >
                    Videos
                </div>
                <TrendingVideo caption={this.state.data.caption} thumbnail={this.state.data.thumbnail} />
                <LatestVideos data={latestVideos} />
                <OtherVideos data={latestVideos} />
            </div>
        )
    }
}
const pageStyle = {
    headerStyle: {
        display: "flex",
        flexDirection: "column",
        marginRight: screenWidth * 0.044,
        fontWeight: "bold",
        fontSize: screenWidth * 0.061,
        marginLeft: screenWidth * 0.044,
        fontFamily: "Montserrat-Bold",
        color: "#141b2f",
        // marginTop: screenWidth * 0.1333,
        marginBottom: screenWidth * 0.03334
    }
}
export default VideoGallery