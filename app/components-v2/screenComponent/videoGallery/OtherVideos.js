import React, { Component } from 'react'
import { white, cardShadow } from "../styleSheet/globalStyle/color";
import { screenWidth } from '../styleSheet/screenSize/ScreenDetails'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import HrLine from "../components/commonComponent/HrLine";
import MediumVideoCard from './MediumVideoCard'

export class OtherVideos extends Component {
    constructor(props) {
        super(props)
        this.state = { data: props.data }
    }
    render() {
        
        return (
            <div style={pageStyle.cardStyle}>
                <div style={pageStyle.cardHeadingStyle}>
                    Other Video
                </div>
                <HrLine />
                <Slider  {...settings}>
                    {
                        (this.state.data != null)
                            ?
                            (this.state.data.map(video => (
                                <div>
                                    <MediumVideoCard
                                        thumbnail={video.thumbnail}
                                        caption={video.caption}
                                    />
                                </div>
                            )))
                            :
                            <div></div>
                    }
                </Slider>
            </div>
        )
    }
}
const pageStyle = {
    cardStyle: {
        display: "flex",
        flexDirection: "column",
        position: "relative",
        marginTop: screenWidth * 0.0333,
        marginLeft: screenWidth * 0.044,
        width: screenWidth * 1.19,
        height: screenWidth * 0.57,
        borderRaduis: screenWidth * 0.0084,
        backgroundColor: white,
        boxShadow: cardShadow,
        paddingTop: screenWidth * 0.0388,
        paddingLeft: screenWidth*0.044,
        // marginBottom: screenWidth * 0.05,
    },
    cardHeadingStyle: {
        display: "flex",
        flexDirection: "row",
        // marginLeft: screenWidth * 0.044,
        fontSize: screenWidth * 0.0333,
        fontFamily: "Montserrat-Medium",
        justifyContent: "space-between",
        marginBottom: screenWidth * 0.0388,
        alignItems: "center"
    },
    sliderStyle: {
        display: "flex",
        position: "absolute",
        top: "0px",
        bottom: "0px",
        right: "0px",
        left: "0px",
        background: "linear-gradient(to right, #a63ba1, #00fa9441)",
        // opacity: "0.3"
    }
}
const settings = {
    // dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    useCSS: true,
    vertical: false
};
export default OtherVideos