import React, { Component } from 'react'
import { white, cardShadow } from "../styleSheet/globalStyle/color";
import { screenWidth } from '../styleSheet/screenSize/ScreenDetails';
import HrLine from "../components/commonComponent/HrLine";
import TabBar from "../components/commonComponent/TabBar";
import SeriesGroup from "../components-ui/SeriesGroup";

const FORMAT_TABS = ["All", "International", "Domestic", "League"];
const data= [
    {
        "date": "OCTOBER 2018",
        "series": [
            {
                "seriesName": "India vs West Indies 2018",
                "dateRange": "Oct 4 - Nov 11",
                "tests": 3,
                "odis": 5,
                "t20s": 3
            }
        ]
    },

    {
        "date": "November 2018",
        "series": [
            {
                "seriesName": "ICC Womens T20 World Cup 2018",
                "dateRange": "Nov 9 - Nov 25",
                "tests": 3,
                "odis": 5,
                "t20s": 3
            },
            {
                "seriesName": "Mzansi Super League 2018",
                "dateRange": "Nov 16 - Dec 16",
                "tests": 3,
                "odis": 5,
                "t20s": 3
            },
            {
                "seriesName": "ICC Womens T20 World Cup 2018",
                "dateRange": "Nov 21 - Jan 18",
                "tests": 3,
                "odis": 5,
                "t20s": 3
            }
        ]
    }
]
export class SeriesCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: "All",
            data: data
        };
    }

    handleActiveTab = activeTab => {
        this.setState({
            activeTab
        });
    };

    render() {
        return (
            <div style={{ paddingTop: screenWidth * 0.1333 }}>
                <div style={pageStyle.headerStyle} >
                    Series
                </div>
                <div style={pageStyle.cardStyle}>
                    {/* <div style={{ marginBottom: screenWidth * 0.030556 }}> */}
                    <TabBar
                        items={FORMAT_TABS}
                        activeItem={this.state.activeTab}
                        onClick={this.handleActiveTab}
                    />
                    <HrLine />
                    {
                    (this.state.data!=null && this.state.data.map(card => (
                        <SeriesGroup data={card}/>
                    )))
                    }
                </div>
            </div>
        )
    }
}
const pageStyle = {
    cardStyle: {
        display: "flex",
        flexDirection: "column",
        position: "relative",
        marginTop: screenWidth * 0.0333,
        marginLeft: screenWidth * 0.044,
        width: screenWidth * 0.9111,
        height: screenWidth * 1.5361,
        borderRaduis: screenWidth * 0.0084,
        backgroundColor: white,
        boxShadow: cardShadow,
        // paddingTop: screenWidth * 0.0388,
        // paddingLeft: screenWidth * 0.02778,
        // marginBottom: screenWidth * 0.05,
    },
    headerStyle: {
        display: "flex",
        flexDirection: "column",
        marginRight: screenWidth * 0.044,
        fontWeight: "bold",
        fontSize: screenWidth * 0.061,
        marginLeft: screenWidth * 0.044,
        fontFamily: "Montserrat-Bold",
        color: "#141b2f",
        // marginTop: screenWidth * 0.1333,
        // marginBottom: screenWidth * 0.03334
    }
}
export default SeriesCard