import React, {Component} from 'react';
import {screenWidth, screenHeight} from './../../../styleSheet/screenSize/ScreenDetails';
import rightArrowBlack from './../../../styleSheet/svg/rightArrowBlack.svg';
import HrLine from './../../commonComponent/HrLine';
import { getDate } from 'date-fns';


const typeFormat =[ 'TEST', 'ODI','T20'];
export default class TeamHistory extends Component {
    constructor(props){
        super(props);
        this.state={
            isActive:'TEST',
    

        }
        this._handelSelectionClick = this._handelSelectionClick.bind(this);
        this._handelSeriesClick=this._handelSeriesClick.bind(this);
    }
    componentDidMount(){

    }
    render(){
        const {teamHistory} = this.props;
       
        
       
       
       
        //ODI
        const odiLowest = teamHistory['lowestScoreMatch']['ODI'] || null;
        const odiHighest = teamHistory['highestScoreMatch']['ODI'] || null;
        const odiDebut = teamHistory['debutMatch']['ODI'] || null;
        const recentSeriesODI = teamHistory['seriesMiniView']['ODI'];

        //TEST
        const testLowest = teamHistory['lowestScoreMatch']['TEST'] || null;
        const testHighest = teamHistory['highestScoreMatch']['TEST'] || null;
        const testDebut = teamHistory['debutMatch']['TEST'] || null;
        const recentSeriesTEST = teamHistory['seriesMiniView']['TEST'];

        //T20
        const t20Lowest = teamHistory['lowestScoreMatch']['T20'] || null;
        const t20Highest = teamHistory['highestScoreMatch']['T20'] || null
        const t20Debut = teamHistory['debutMatch']['T20'] || null;
        const recentSeriesT20 = teamHistory['seriesMiniView']['T20'];

        // console.log(recentSeriesODI,recentSeriesTEST,recentSeriesT20,"check ------")


        if(this.state.isActive === 'TEST' && recentSeriesTEST != null){
            return(
                <div style={pageStyle.teamHistoryContainer}>
                    <div style={{display:'flex', height:screenHeight*0.070}}>
                        {this.topNavBar()}
                    </div>
                    <HrLine />
                    <div style={pageStyle.recentSeriesTextHeading}>
                        Recent Series
                    </div>
                    <HrLine />
                    <div style={{display:'flex', flex:1,flexDirection:'column', width:screenWidth*0.911}}>
                        {
                            recentSeriesTEST.map((data)=>{
                                return(

                                        <div>
                                            {this.recentSeries(data)}
                                            <HrLine />
                                        </div>
                                );
                            })
                            
                        }
                    </div>
                 
                <div style={{display:'flex',flexDirection:'column',marginTop:'20px'}}>
                    <div>
                        {this.importantMatch(testDebut,'Test debut' )}
                    </div>
                    <HrLine />
                    <div>
                        {this.importantMatch(testHighest, 'Highest Score')}
                    </div>
                    <HrLine />
                    <div>
                        {this.importantMatch(testLowest, 'Lowest Score')}
                    </div>
                </div>

            </div>
            );
        }
        else if(this.state.isActive === 'ODI' && recentSeriesODI != null){
            return(
                <div style={pageStyle.teamHistoryContainer}>
                  <div style={{display:'flex', height:screenHeight*0.070}}>
                        {this.topNavBar()}
                    </div>
                    <HrLine />
                    <div style={pageStyle.recentSeriesTextHeading}>
                        Recent Series
                    </div>
                    <HrLine />
                    <div style={{display:'flex', flex:1,flexDirection:'column', width:screenWidth*0.911}}>
                        {
                            recentSeriesODI.map((data)=>{
                                return(
                                    <div >
                                        {this.recentSeries(data)}
                                        <HrLine />
                                    </div>
                                );
                            })
                            
                        }
                    </div>
                <div style={{display:'flex',flexDirection:'column',marginTop:'20px'}}>
                    <div>
                        {this.importantMatch(odiDebut, 'ODI debut')}
                    </div>
                    <HrLine />
                    <div>
                        {this.importantMatch(odiHighest, 'Highest Score')}
                    </div>
                    <HrLine />
                    <div>
                        {this.importantMatch(odiLowest, 'Lowest Score')}
                    </div>
                </div>

            </div>
            );

        }
        else if(this.state.isActive === 'T20' && recentSeriesT20 != null){
            return(
                <div style={pageStyle.teamHistoryContainer}>
                  <div style={{display:'flex', height:screenHeight*0.070}}>
                        {this.topNavBar()}
                    </div>
                    <HrLine />
                    <div style={pageStyle.recentSeriesTextHeading}>
                        Recent Series
                    </div>
                    <HrLine />
                    <div style={{display:'flex', flex:1,flexDirection:'column', width:screenWidth*0.911}}>
                        {
                            recentSeriesT20.map((data)=>{
                                return(
                                    <div >
                                        {this.recentSeries(data)}
                                        <HrLine />
                                    </div>
                                );
                            })
                            
                        }
                    </div>
                <div style={{display:'flex',flexDirection:'column',marginTop:'20px'}}>
                    <div>
                        {this.importantMatch(t20Debut, 'T20 debut')}
                    </div>
                    <HrLine />
                    <div>
                        {this.importantMatch(t20Highest, 'Highest Score')}
                    </div>
                    <HrLine />
                    <div>
                        {this.importantMatch(t20Lowest, 'Lowest Score')}
                    </div>
                </div>

            </div>
            );

        }
        else {
            return(
                <div style={pageStyle.teamHistoryContainer}>
                    <div style={{display:'flex', height:screenHeight*0.070}}>
                        {this.topNavBar()}
                    </div>
                    <HrLine />
                    <div style={{display:'flex', flex:1, width:screenWidth*0.911, padding:'20px 20px 20px 20px', fontSize:'14px'}}>
                        No data found for this team
                    </div>
                    <HrLine />
                </div>
            );
        }
    }
    topNavBar(){
        var testStyle = pageStyle.topNavBarMenuActive;
        var odiStyle = pageStyle.topNavBarMenuUnActive;
        var t20Style =pageStyle.topNavBarMenuUnActive;
        const {isActive} =this.state;
        if(isActive === 'ODI'){
            odiStyle = pageStyle.topNavBarMenuActive;  
            testStyle = pageStyle.topNavBarMenuUnActive;
            t20Style =pageStyle.topNavBarMenuUnActive;
        } 
        else if( isActive === 'TEST'){
            testStyle = pageStyle.topNavBarMenuActive;
            t20Style= pageStyle.topNavBarMenuUnActive;
            odiStyle = pageStyle.topNavBarMenuUnActive;
        }
        else if( isActive === 'T20'){
            t20Style = pageStyle.topNavBarMenuActive;
            testStyle= pageStyle.topNavBarMenuUnActive;
            odiStyle =pageStyle.topNavBarMenuUnActive;
        }

        return(
            <div style={pageStyle.topNavBarContainer}>
                {/* <div style={pageStyle.emptyStyle}>
                    {null}
                </div> */}
                <div  style={testStyle}  onClick={this._handelSelectionClick}>
                    TEST
                </div>

                <div  style={odiStyle}  onClick={this._handelSelectionClick}>
                    ODI
                </div>

                <div  style={t20Style}  onClick={this._handelSelectionClick}>
                    T20
                </div>
                {/* <div style={pageStyle.emptyStyle}>
                    {null}
                </div> */}

            </div>
        );
    }

    _handelSelectionClick(event){
        const value =event.target.innerHTML;
        
        this.setState({isActive:value});
    }
    recentSeries(data){
    
        const seriesId = data.seriesId;
        const seriesName =data.seriesName
        return(
            <div>
                <div style={pageStyle.recentSeriesContainer}  onClick={()=>{this._handelSeriesClick(seriesId,seriesName)}} >
                    <div style={pageStyle.recentSeriesTextContainer}>
                        <div style={pageStyle.recentSeriesTextBold}>
                            { data.seriesName}
                        </div>   
                        <div style={pageStyle.recentSeriesText}>
                            {data.seriesStatus}
                        </div>
                    </div>
                    <div style={pageStyle.recentSeriesIcon}>
                        <img src={rightArrowBlack} width="14px" height='14px' alt='rightArrowBlack' />
                    </div>
                </div>
                <HrLine />
            </div>
        );
    }
    _handelSeriesClick(seriesId,seriesName){
        // this.props.history.push(`/series/${seriesName}/${seriesId}`);
        this.props.history.push(
            `/series/${seriesName ? 
                seriesName.toLowerCase().replace(/[ /]/g, "-")
                : 
                ""
            }/${seriesId}`
          );

    }

    importantMatch(data, name){
        // console.log(data);
        const dateInMs = data.date;
        const date = new Date(dateInMs)
        const viewDate = date && date.toString().substring(0,15);
        return(
            <div style={pageStyle.importantMatchContainer}>
                <div style={pageStyle.importantMatchType}>
                    <div style={pageStyle.importantMatchHeading}>
                        {name}
                    </div>
                    <div style={pageStyle.importantMatchValue}>
                        {viewDate}
                    </div>
                </div>
                <div style={pageStyle.importantMatchSecondType}>
                    <div  style={pageStyle.importantMatchSecondValue}>
                        {/* {(data.title).substring(0,9) } */}
                        {data.title}
                    </div>
                    <div  style={pageStyle.importantMatchValue}>
                        {data.info}
                    </div>
                </div>
            </div>
        );
    }
   

}

const pageStyle={

    recentSeriesTextHeading:{
        fontFamily:'Montserrat',
        fontWeight:'500',
        display:'flex', 
        flex:0.50, 
        fontSize:'12px', 
        color:'#141b2f',
        alignItems:'center', 
        paddingLeft:'16px' ,
        paddingTop:'12px',
        paddingBottom:'12px',


    },
    emptyLine:{
        display:'flex',
        flex:1,
        backgroundImage:'linear-gradient(to right,#ffffff, #f0f1f2, #f0f1f2, #f0f1f2, #ffffff)',
        height:'1px',
        width:screenWidth*0.911,

    },
    container:{
        display:'flex',
        flex:1,
        flexDirection:'column',
        backgroundColor:'#fff',
        boxShadow:'0px 3px 6px 0px #13000000',
        width:screenWidth*0.911,
        marginLeft:screenWidth*0.044,

    },
    importantMatchSecondValue:{
        display:'flex',
        flex:0.50,
        color:'#141b2f',
        fontSize:'12px',
        letterSpacing:'0.33px',
        padding:'0px 0px 3px 0px',

    },
    importantMatchHeading:{
        display:'flex',
        flex:0.50,
        color:'#727682',
        fontSize:'12px',
        fontWeight:'700',
        letterSpacing:'0.33px',
        padding:'5px 0px 0px 0px',
        alignItems:'center',
        justifyContent:'flex-start',
    },
    emptyStyle:{
        display:'flex',
        flex:0.125,

    },
    importantMatchValue:{
        fontFamily:'Montserrat',
        display:'flex',
        flex:0.50,
        color:'#141b2f',
        fontSize:'12px',
        fontWeight:'300',
        padding:'0px 0px 0px 16px',
        alignItems:'center',
        justifyContent:'flex-start',
    },

    importantMatchSecondType:{
        display:'flex',
        flexDirection:'row',
        flex:0.50,
        alignItems:'flex-start',
        justifyContent:'flex-start',
        paddingLeft:'16px',

    },
    importantMatchType:{
        display:'flex',
        flexDirection:'row',
        flex:0.50,
        alignItems:'center',
        justifyContent:'flex-start',
        paddingLeft:'16px',

    },
    importantMatchContainer:{
        display:'flex',
        flex:1,
        flexDirection:'column',
        width:screenWidth*0.911,
        paddingTop:'10px',
        paddingBottom:'10px',
    },
    recentSeriesText:{
        display:'flex',
        flex:0.30,
        color:'#141b2f',
        fontWeight:'300',
        justifyContent:'flex-start',
        alignItems:'flex-start',
    },
    recentSeriesTextBold:{
        display:'flex',
        flex:0.70,
        color:'#727682',
        fontWeight:'500',
        fontFamily:'Montserrat',
    },
    recentSeriesTextContainer:{
        display:'flex',
        flex:0.90,
        justifyContent:'flex-start',
        padding:'10px 0px 5px 16px',
        flexDirection:'column',

    },
    recentSeriesIcon:{
        display:'flex',
        flex:0.10,
        justifyContent:'center',
        paddingLeft:'10px',
        alignItems:'center',
    },
    recentSeriesContainer:{
        display:'flex',
        flexDirection:'row',
        backgroundImage:'linear-gradient(to right,#ffffff,#f0f1f2,#f0f1f2,#f0f1f2,#ffffff)',
        fontSize:'12px',
        height:screenHeight*0.078,
        fontFamily:'Montserrat',
    },
    topNavBarMenuUnActive:{
        display:'flex',
        flex:0.33,
        justifyContent:'center',
        color:'#727682',
        fontSize:'12px',
        letterSpacing:'0.6px',
        padding:'3px 0px 5px 0px',
        fontFamily:'Montserrat',
    },
    topNavBarMenuActive:{
        display:'flex',
        flex:0.33,
        justifyContent:'center',
        color:'#141b2f',
        fontSize:'12px',
        letterSpacing:'0.6px',
        fontWeight:'500',
        borderBottom:'2px solid #d44030',
        padding:'3px 0px 5px 0px',
        fontFamily:'Montserrat',
    },
    topNavBarContainer:{
        display:'flex',
        flex:1,
        flexDirection:'row',
        paddingTop:'10px',
        
    },
    teamHistoryContainer:{
        display:'flex',
        flexDirection:'column',
        flex:1,
        width:screenWidth*0.911,
        marginLeft:screenWidth*0.044,
        backgroundColor:'#fff',
        fontFamily:'Montserrat',
    }

}