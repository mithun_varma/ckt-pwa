import React, { Component } from "react";
import {
  grey_10,
  orange_2,
} from './../../../styleSheet/globalStyle/color';
import moment from "moment";
import Polling from "./../../commonComponent/Polling";

class HomeScoreCardTeam extends Component {
  render() {
    const { score } = this.props;
    let teamCollection = [
      {
        teamName:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.fullName
            : score.teamB.fullName,
        shortName:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.shortName
            : score.teamB.shortName,
        teamKey:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.teamKey
            : score.teamB.teamKey,
        runs: [],
        wickets: [],
        overs: [],
        avatar:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.avatar
            : score.teamB.avatar
      },
      {
        teamName:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.fullName
            : score.teamB.fullName,
        shortName:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.shortName
            : score.teamB.shortName,
        teamKey:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.teamKey
            : score.teamB.teamKey,
        runs: [],
        wickets: [],
        overs: [],
        avatar:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.avatar
            : score.teamB.avatar
      }
    ];
    if (score.status !== "UPCOMING") {
      for (const order of score.inningOrder) {
        let team = order.split("_")[0];
        if (team === score.firstBatting) {
          (teamCollection[0].runs = [
            ...teamCollection[0].runs,
            score.inningCollection[order].runs
          ]),
            (teamCollection[0].wickets = [
              ...teamCollection[0].wickets,
              score.inningCollection[order].wickets
            ]),
            (teamCollection[0].overs = [
              ...teamCollection[0].overs,
              score.inningCollection[order].overs
            ]);
        } else {
          (teamCollection[1].runs = [
            ...teamCollection[1].runs,
            score.inningCollection[order].runs
          ]),
            (teamCollection[1].wickets = [
              ...teamCollection[1].wickets,
              score.inningCollection[order].wickets
            ]),
            (teamCollection[1].overs = [
              ...teamCollection[1].overs,
              score.inningCollection[order].overs
            ]);
        }
      }
    }

    return (
      <div>
        <div style={{ position: "relative", width: "96%" }}>
          {
            // score.status !== "COMPLETED" &&
            // !this.props.isSeries && (
            //   <div className={`notification-wrapper ${this.props.className}`}>
            //     <div className="outer-wrapper" onClick={this.props.handleNotification} >
            //       {
            //           // this.props.notificationActive ? 
            //           //   (
            //           //       <Notifications style={{fill:'#fff', marginTop: 10, marginLeft: 24, fontSize: 24 }} />
            //           //   ) 
            //           //   : 
            //           //   (
            //           //       <NotificationsNone style={{ marginTop: 10, marginLeft: 24, fontSize: 24}} />
            //           //   )
            //       }
            //     </div>
            //   </div>
            // )
            }
          <div style={{ padding: "8px 2px", width: "98%"}} >
            <div style={{ fontFamily: "Montserrat", fontWeight: 500, fontSize: 14, color: grey_10,  display: "flex", alignItems: "center", marginBottom: 4, whiteSpace: "nowrap", overflow: "hidden" }}>
              <div style={{display:'flex'}}>{score && score.relatedName ? score.relatedName : ""}</div>
                {
                  score &&
                    score.relatedName && (
                        <div style={{ padding: "0 6px",display:'flex',flex:0.05 }}>•</div>
                    )
                }
                <div style={ score.status !== "COMPLETED" ? 
                        {
                            maxWidth: "17ch",display:'flex',flex:0.90
                        }
                        : 
                        { 
                            maxWidth: "20ch" ,display:'flex',flex:0.90
                        }
                        }
                    className="series-text"
                >
                {
                        score && score.seriesName
                    ? score.seriesName.substring(0, 25)
                    : "--"
                }
                </div>
                {/* <div style={{display:'flex',alignItems:'flex-end',marginTop:'0px',marginLeft:'40px',flex:0.10,width:'10px',height:'10px'}}>
                    X
                </div> */}
            </div>
            <div style={{ fontFamily: "Montserrat", fontWeight: 500, fontSize: 12, color: "#727682", display: "flex", alignItems: "center", marginBottom: "8px" }}>
                {
                  score &&
                    score.status === "RUNNING" && (
                    <span style={{ border: "solid 0.6px #979797", borderRadius: "7.5px", padding: "1px 8px", display: "inline-block", fontSize: "8px", marginRight: "10px", textTransform: "uppercase" }} >
                        <span style={{ width: "5px", height: "5px", borderRadius: "50%", marginLeft: "-2px",  marginRight: "4px", backgroundColor: "#35a863",  display: "inline-block" }}/>
                        live
                    </span>
                    )
                }
                <img
                    src={require("../../../images/location-icon.svg")}
                    alt="" style={{ width: 9, height: 12, right: 10, top: 10 }}
                />
                <span style={{ marginLeft: 5, whiteSpace: "nowrap", maxWidth: "25ch", overflow: "hidden" }} >
                    {(score && score.venue && score.venue) || "Adelaid"}
                </span>
            </div>

            <div style={{ flex: 1, flexDirection: "column", backgroundColor: "#ffffff", justifyContent: "space-between" }}>
              <div style={{ flexDirection: "column", justifyContent: "space-between", position: "relative", fontFamily: "Montserrat" }} onClick={this.props.handleScoreCard} >
                {
                    teamCollection &&
                    teamCollection.map(team => (
                    <div key={team.teamKey} style={{ display: "flex" }}>
                      <div style={{ display: "flex", flex: 1, alignItems: "center", justifyContent: "space-between", flexDirection: "row", marginBottom: "6px"}}>
                      <div style={{flex: 0.4,display: "flex",alignItems: "center"}}>
                          <img src={ team.avatar ? team.avatar : require("../../../images/flag_empty.svg") }
                            alt="" style={{ width: 24, height: 16, marginRight: 14 }}
                          />
                          <span style={{ fontWeight: 400, fontSize: 14 }}>
                            {(team.shortName &&
                              team.shortName.substring(0, 3).toUpperCase()) ||
                              "-"}
                          </span>
                        </div>
                        <div style={{ flex: 0.6, flexDirection: "row" }}>
                          {score.status !== "UPCOMING" ? (
                            <span
                              style={{
                                fontWeight: `${
                                  score.status === "RUNNING" &&
                                  team.teamKey ===
                                    (score.currentScoreCard &&
                                      score.currentScoreCard.battingTeam)
                                    ? "700"
                                    : "400"
                                }`,
                                fontSize: 14
                              }}
                            >
                              {`${
                                team.runs[0] !== undefined
                                  ? `${team.runs[0]}`
                                  : ""
                              }${
                                team.wickets[0] !== undefined &&
                                team.wickets[0] < 10
                                  ? `/${team.wickets[0]}`
                                  : ""
                              }`}
                              <span style={{ fontWeight: 400, marginLeft: 8 }}>
                                {score.format !== "TEST" &&
                                  team.overs[0] &&
                                  `(${team.overs[0]})`}
                              </span>
                              {team.runs.length > 1 &&
                                `& ${
                                  team.runs[1] !== undefined ? team.runs[1] : ""
                                }${
                                  team.wickets[1] !== undefined &&
                                  team.wickets[1] < 10
                                    ? "/" + team.wickets[1]
                                    : ""
                                }`}
                              <span style={{ fontWeight: 400, marginLeft: 8 }}>
                                {team.runs.length > 1 &&
                                  score.format !== "TEST" &&
                                  `(${team.overs[1]})`}
                              </span>
                            </span>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  ))
                }
                {
                    score &&
                    score.status &&
                    score.status === "UPCOMING" && (
                        <div style={{ position: "absolute", top: 2, left: "40%", borderLeft: "1px solid #e2e2e2", fontFamily: "Montserrat", fontSize: 14, fontWeight: 400, color: grey_10, padding: "0 18px"  }}>
                        <div>
                            {score && moment(score.startDate).format("Do MMMM ")}
                        </div>
                        <div style={{ marginTop: 5 }}>
                            {score &&
                            moment(score.startDate).format("h:mm A ") + window.moment().tz(window.moment.tz.guess(true)).format('z')}
                        </div>
                        </div>
                    )
                }
              </div>
                <div style={{ display: "flex", justifyContent: "center", alignItems: "center", marginTop: 8, marginBottom: 8 }} onClick={this.props.handleScoreCard}>
                    <div style={{ display: "flex", padding: "0 8px" }}>
                        <img src={require("../../../images/capsuleLeftWing.png")} alt="" style={{ width: 40, height: 2 }}/>
                    </div>
                    <div className="line_limit" style={{ background: orange_2, fontFamily: "Montserrat", fontSize: 11, color: grey_10, padding: "2px 10px", borderRadius: "12px", display: "flex", justifyContent: "center", maxWidth: "85%", whiteSpace: "initial" }}>
                        {
                            score.status !== "UPCOMING"
                            ? (score.status && score.status === "RUNNING"
                                ? score.toss
                                : score.statusStr) ||
                            `${moment(score.startDate).format(
                                "Do MMM YYYY, h:mm A "
                            )} ${window.moment().tz(window.moment.tz.guess(true)).format('z')}`
                            : `${moment(score.startDate).format(
                                "Do MMM YYYY, h:mm A "
                            )} ${window.moment().tz(window.moment.tz.guess(true)).format('z')}`
                        }
                    </div>
                    <div style={{ display: "flex", padding: "0 8px" }}>
                        <img src={require("../../../images/capsuleRightWing.png")} alt="" style={{ width: 40, height: 2 }} />
                    </div>
                </div>
            </div>
                {
                    score.status === "COMPLETED" &&
                    score.manOfTheMatch &&
                    score.manOfTheMatch[0] ? 
                    (
                        <div style={{ display: "flex", flexDirection: "row", justifyContent: "flex-start", alignItems: "center", fontFamily: "Montserrat", paddingTop: "4px", color: grey_10, ...this.props.rootStyles }} onClick={this.props.handleScoreCard}>
                            <div style={{ height: 38, width: 38, overflow: "hidden", borderRadius: "50%", marginRight: 12 }}>
                                <img src={ (score.manOfTheMatch && score.manOfTheMatch[0] && score.manOfTheMatch[0].avatar && score.manOfTheMatch[0].avatar) || require("../../../images/Man of the match-09.svg")} alt="" style={{ width: "100%", objectFit: "cover" }} />
                            </div>
                            <div style={{ justifyContent: "space-between"}}>
                                <div style={{ fontSize: 13, fontWeight: 500, whiteSpace: "nowrap", overflow: "hidden", textOverflow: "ellipsis" }}>
                                    {score.manOfTheMatch &&
                                    score.manOfTheMatch[0] &&
                                    score.manOfTheMatch[0].name}
                                </div>
                                <div style={{ fontSize: 12, fontWeight: 400 }}>
                                    Player of the Match
                                </div>
                            </div>
                        </div>
                    ) 
                    : 
                    (score.status === "RUNNING" || score.status === "UPCOMING") &&
                    !this.props.isSeries ? (
                    <div
                        style={{
                        alignSelf: "center",
                        position: "relative"
                        }}
                        onClick={this.props.handleScoreCard}
                    >
                        <Polling
                        data={score && score.prediction && score.prediction}
                        score={score}
                        />
                    </div>
                    ) 
                    : 
                    (
                    <div style={{ display: "flex", alignItems: "center", fontFamily: "Montserrat", fontWeight: 500, fontSize: 12, padding: "14px 0px" }}>
                        No Data to show
                    </div>
                    )
                 }
          </div>
        </div>
      </div>
    );
  }
}

export default HomeScoreCardTeam;
