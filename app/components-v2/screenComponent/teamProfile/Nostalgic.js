import React, {Component} from 'react';
import { screenWidth, screenHeight } from '../../../styleSheet/screenSize/ScreenDetails';
import HrLine from '../../commonComponent/HrLine';
import EmptyState from './../../commonComponent/EmptyState';
// import { url } from 'inspector';

export default class Nostalgic extends Component{
    constructor(props){
        super(props);
        this.state={

        }
        this._handelNewsFull=this._handelNewsFull.bind(this);
    }
    componentDidMount(){

    }
    render(){
        const nostalgicArray =this.props.NostalgicArray;
        if(nostalgicArray != null){
            return(
                <div style={pageStyle.conatiner}>
                    <div style={pageStyle.conatinerTitle}>
                       Nostalgic Matches
                    </div>
                    <HrLine />
                    <div style={pageStyle.conatinerImage}>
                        {
                            nostalgicArray.map((data)=>{
                            return(
                                <div>
                                    {this.singlePage(data)}
                                </div>
                            )
                        })
                       }
                    </div>
                </div>
            );
        }
        else{
            return(
                <div style={{display:'flex',justifyContent:'center',alignItems:'center'}}>
                    <EmptyState />
                </div>
            )
        }
    }
    _handelNewsFull(){
        
       
    }
    singlePage(data){
        const type = data.type;
        const _id = data._id;
        const title =data.title;
        const seriesName = data.seriesName || "World Cup 1983"
        return(
            <div style={pageStyle.singlePageConatiner} id={data.type} onClick={()=>{this._handelNewsFull()}} >
                <div style={{display:'flex',flex:0.50,height:screenHeight*0.2125, width: screenWidth*0.778, backgroundImage: `url(${data.path})`, backgroundRepeat:'no-repeat',backgroundPosition:'center',backgroundSize:'cover'}}>
                    {null}
                </div>
                <div style={pageStyle.NostalgicText}>
                    {data.longDescription}
                </div>
                {/* <div style={pageStyle.NostalgicFooter}>
                    <div style={{marginTop:'16px'}}>
                        {this.scoreCardDetails()}
                    </div>
                    <div style={{marginTop:'4px'}}>
                        {this.scoreCardDetails()}
                    </div>
                    <div style={{marginTop:'12px',color:'#141b2f',fontFamily:'Montserrat',fontSize:'12px',letterSpacing:'0.3px'}}>
                        {seriesName}
                    </div>
                </div> */}
            </div>
        );

    }
    scoreCardDetails(team, score){
        return(
            <div style={{display:'flex',width:"166px",flexDirection:'row',fontFamily:'Oswald',fontSize:'18px'}}>
                <div>
                    {this.footerTeamName("I","N","D")}
                </div>
                <div style={{marginLeft:'4px'}}>
                    {this.footerBlackDesign("3")}
                </div>
                <div style={{marginLeft:'2px'}}>
                    {this.footerBlackDesign("0")}
                </div>
                <div style={{marginLeft:'2px'}}>
                    {this.footerBlackDesign("1")}
                </div>
                <div style={{marginLeft:'2px'}}>
                    {this.footerBlackDesign("-")}
                </div>
                <div style={{marginLeft:'2px'}}>
                    {this.footerBlackDesign("9")}
                </div>
            </div>
        );
    }
    footerTeamName(I,N,D){
        return(
            <div style={{display:'flex', width:"68px", height:'31px',flexDirection:'row', backgroundColor:'#141b2f',color:'#fadd8d',borderRadius:'2px'}}>
                <div style={{display:'flex', width:'22px', height:'31px',justifyContent:'center',alignItems:'center'}}>
                    I
                </div>
                <div style={{display:'flex',backgroundColor:'#ccc',width:'1px',height:'21px',marginTop:'5px'}}>
                   {null}
                </div>
                <div style={{display:'flex', width:'22px', height:'31px',justifyContent:'center',alignItems:'center'}}>
                    N
                </div>
                <div style={{display:'flex',backgroundColor:'#ccc',width:'1px',height:'21px',marginTop:'5px'}}>
                    {null}
                </div>
                <div style={{display:'flex', width:'22px', height:'31px',justifyContent:'center',alignItems:'center'}}>
                    D
                </div>
            </div>
        );
    }
    footerBlackDesign(num){
        const number = num || "I";
        return(
            <div style={{display:'flex',justifyContent:'center',alignItems:'center' ,height:"31px", width:"22px", backgroundColor:'#141b2f',color:'#fadd8d',borderRadius:'2px'}}>
                {number}
            </div>
        );
    }

}
const pageStyle={
    conatiner:{
        display:'flex',
        flex:1,
        flexDirection:'column',
        width:screenWidth*0.911,
        backgroundColor:'#fffaeb',
    },
    conatinerTitle:{
        display:'flex',
        color:'#141B2F',
        padding:'12px 0px 12px 16px',
        fontSize:'12px',
        fontFamily:'Montserrat',
        fontWeight:'500',
    },
    conatinerImage:{
        display:'flex',
        flex:1,
        flexDirection:'row',
        overflow:'scroll',
        marginLeft:'16px',

    },
    singlePageConatiner:{
        paddingRight:'12px',
        paddingTop:'12px',
        paddingBottom:'12px',
        flex:1,
        flexDirection:'column',
        overflow:'scroll'
    },
    NostalgicText:{
        display:'flex',
        flex:0.25,
        fontSize:'12px',
        color:'#141b2f',
        fontFamily:'Montserrat',
        height:'60px',
        paddingTop:'12px',
        paddingBottom:'16px',
        fontWeight:'300',
    },
    NostalgicFooter:{
        display:'flex',
        flex:0.25,
        flexDirection:'column'


    }

}