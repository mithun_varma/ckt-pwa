import React, { Component } from "react";
import {
  screenWidth,
  screenHeight
} from "../../../styleSheet/screenSize/ScreenDetails";
import Calender from "./../../commonComponent/CalenderWithTitle";
import TeamModalTrophy from "./../../commonComponent/TeamModalTrophy";
import TeamModalJersey from "./../../commonComponent/TeamModalJersey";
import {
  white,
  gradientRedOrangeLeft
} from "./../../../styleSheet/globalStyle/color";
import trophy from "./../../../styleSheet/svg/trophy.svg";
import TeamHistory from "./teamHistory";
import NewsComponent from "./NewsTeam";
import downArrowBlack from "./../../../styleSheet/svg/downArrowBlack.svg";
import upArrowBlack from "./../../../styleSheet/svg/upArrowBlack.png";
// import SocialMedia from './../../commonComponent/SocialMediaUpdate';
import Header from "./../../commonComponent/Header";
import allRounder from "./../../../styleSheet/svg/allRounder.svg";
import batsman from "./../../../styleSheet/svg/batsman.svg";
import bowler from "./../../../styleSheet/svg/baller.svg";
import HrLine from "../../commonComponent/HrLine";
import playerIcon from "../../../styleSheet/svg/playerIcon.svg";
import TeamMoments from "./TeamMoments";
import PhotoView from "./PhotoView";
import Nostalgic from "./Nostalgic";
import Keeper from "./../../../images/Keeper.svg";

export default class TeamProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showTeamJerseyScreen: true,
      showTeamTrophyScreen: false,
      title: "",
      isHeaderBackground: true,
      titlePlaced: false,
      // showPhoto: false
      clickToCloseTeam: false
    };
    this._handelTeamJerseyScreen = this._handelTeamJerseyScreen.bind(this);
    this._handelTeamTrophyScreen = this._handelTeamTrophyScreen.bind(this);
    this._handelSquadList = this._handelSquadList.bind(this);
    this.clickToCloseTeam = this.clickToCloseTeam.bind(this);
  }
  clickToCloseTeam() {
    this.setState({ clickToCloseTeam: true });
  }

  componentDidMount() {
  }

  componentWillUnmount() {
  }
  handleImageClick = val => {
    this.setState({
      showPhoto: val
    });
  };

  _handelTeamJerseyScreen() {
    this.setState({ showTeamJerseyScreen: !this.state.showTeamJerseyScreen });
  }
  _handelTeamTrophyScreen() {
    // console.log("trophy clicked");
    this.setState({ showTeamTrophyScreen: !this.state.showTeamTrophyScreen });
  }
  render() {
    const teamsDetails = this.props.teamsDetails;
    const teamDetailsData = teamsDetails.teamDetails[this.props.teamId];
    const NostalgicArray = this.props.teamNostalgicMoments;
    if (teamDetailsData != null) {
      const teamImage = teamDetailsData.avatar;
      const teamHistory = teamDetailsData;
      const teamMagicMoments = this.props.teamMagicMoments[0];
      return this.state.showPhoto ? (
        <PhotoView
          toggleView={val => this.handleImageClick(val)}
          title="Team Moments"
          data={teamMagicMoments}
        />
      ) : (
        <div style={pageStyle.container}>
          <div
            style={{
              fontFamily: "Montserrat",
              fontWeight: 700,
              fontSize: 22,
              color: white,
              marginBottom: 12,
              minHeight: "90px",
              paddingLeft: "16px",
              textTransform: "capitalize",
              background: gradientRedOrangeLeft
            }}
          >
            {null}
          </div>

          <div>
            <Header
              title={teamDetailsData.name}
              isHeaderBackground={this.state.isHeaderBackground}
              leftArrowBack
              textColor={white}
              backgroundColor={gradientRedOrangeLeft}
              history={this.props.history}
              leftIconOnClick={() => {
                this.props.history.goBack();
              }}
            />
          </div>
          <div style={pageStyle.image}>
            <img src={teamImage} style={{display:'flex', flex:1}} width={screenWidth*0.911} height={screenHeight*0.25} />
          </div>
          <div style={{ marginTop: -screenHeight * 0.035 }}>
            {this.buttonBelowImage()}
          </div>
          <div>
            {this.state.showTeamTrophyScreen ? (
              <div>{this.showTeamTrophyScreen()}</div>
            ) : (
              <div>{null}</div>
            )}
          </div>
          <div style={pageStyle.calenderContainer}>
            <Calender
              history={this.props.history}
              teamSchedule={this.props.teamSchedule}
              fetchTeamSchedule={this.props.fetchTeamSchedule}
            />
          </div>
          <div style={pageStyle.teamHistory}>
            <TeamHistory
              teamHistory={teamHistory}
              history={this.props.history}
            />
          </div>
          <div style={pageStyle.squad}>{this.squad()}</div>
          <div style={pageStyle.news}>
            <Nostalgic
              NostalgicArray={NostalgicArray}
              history={this.props.history}
            />
          </div>
          {this.props.teamNews && (
            <div style={pageStyle.news}>
              <NewsComponent
                playerNews={this.props.teamNews}
                history={this.props.history}
              />
            </div>
          )}
          <div
            style={{
              marginTop: "12px",
              marginLeft: screenWidth * 0.044,
              display:'flex',
              flex:1
            }}
          >
            <TeamMoments
              clickToCloseTeam={this.clickToCloseTeam}
              // handleImageClick={() => this.props.history.push("/photoview")}
              handleImageClick={() => this.handleImageClick(true)}
              imageData={teamMagicMoments}
            />
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
  handleImageClick = val => {
    this.setState({
      showPhoto: val
    });
  };
  showTeamJerseyScreen() {
    const teamsDetails = this.props.teamsDetails;
    const teamDetailsData = teamsDetails.teamDetails.undefined;
    const jerseyData = teamDetailsData.teamJersey;
    // console.log("hello jersey",teamDetailsData.teamJersey);
    return (
      <div>
        <TeamModalJersey
          jerseyData={jerseyData}
          closeJersey={this._handelTeamJerseyScreen}
        />
      </div>
    );
  }
  showTeamTrophyScreen() {
    const teamId = this.props.teamId;
    const teamsDetails = this.props.teamsDetails;
    const teamDetailsData = teamsDetails.teamDetails[teamId];
    const trophyData = teamDetailsData.trophyCabinet;
    return (
      <div>
        <TeamModalTrophy
          trophyData={trophyData}
          closeTrophy={this._handelTeamTrophyScreen}
        />
      </div>
    );
  }
  buttonBelowImage() {
    return (
      <div style={pageStyle.buttonBelowImageContainer}>
        <div style={pageStyle.eachButtonContainer}>
          <img
            style={pageStyle.eachButtomSvg}
            src={trophy}
            height="20px"
            width="20px"
            alt="trophy"
          />
          <div
            style={{
              backgroundColor: "#c96c70",
              height: "44px",
              maxWidth: "1px",
              alignItems: "center",
              justifyContent: "flex-start",
              flex: 0.01,
              border: "0.50 px solid #c96c70",
              marginLeft: "8px",
              marginRight: "8px"
            }}
          />
          <div
            style={pageStyle.eachButtonText}
            onClick={this._handelTeamTrophyScreen}
          >
            Trophy Cabinet
          </div>
        </div>
      </div>
    );
  }
  _handelSquadList() {
    this.setState({ showSquad: !this.state.showSquad });
  }
  squad() {
    var arrow = downArrowBlack;
    if (this.state.showSquad) {
      arrow = upArrowBlack;
    } else {
      arrow = downArrowBlack;
    }
    return (
      <div style={pageStyle.squadContainer}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            padding: "12px 16px",
            justifyContent: "space-between",
            width: "100%",
            alignItems: "center"
          }}
          onClick={this._handelSquadList}
        >
          <div style={pageStyle.squadText}>Squad</div>
          <div
            style={{
              flex: 0.7,
              display: "flex",
              flex:0.8,
            }}
          >
            {null}
          </div>
          <div
            style={{ flex: 0.1, display: "flex", justifyContent: "flex-end" }}
          >
            <img src={arrow} alt="arrow" />
          </div>
        </div>
        <HrLine />
        <div style={{ display: "flex", width: "100%" }}>
          {this.state.showSquad ? (
            <div style={{width: "100%"}}>{this.squadList()}</div>
          ) : (
            <div>{null}</div>
          )}
        </div>
      </div>
    );
  }
  squadList() {
    const teamsDetails = this.props.teamsDetails;
    const teamId = this.props.teamId;
    const teamDetailsData = teamsDetails.teamDetails[teamId];
    var squadArray = teamDetailsData['squad'];
    // console.log("------------ ----- ", teamDetailsData.squad);
    if (squadArray != null && squadArray !=[]) {
      return (
        <div style={pageStyle.squadListContainer}>
          <HrLine />
          {squadArray.map(player => {
            return (
              <div>
                <div style={pageStyle.squadList}>
                  <div style={{display: 'flex'}}>
                  <div style={pageStyle.squadImage}>
                    <img
                      src={player.avatar || playerIcon}
                      alt="playerImage"
                      height="25px"
                      width="25px"
                    />
                  </div>
                  <div style={pageStyle.squadName}>{player.shortName}</div>

                  </div>
                  <div
                    style={{
                      display: "flex",
                      flex: 0.1,
                      justifyContent: "flex-start"
                    }}
                  >
                    {null}
                  </div>
                  <div style={pageStyle.squadImage}>
                    {player.role == "Batsman" ? (
                      <img
                        src={batsman}
                        alt="playerImage"
                        height="25px"
                        width="25px"
                      />
                    ) : player.role == "All Rounder" ? (
                      <img
                        src={allRounder}
                        alt="playerImage"
                        height="25px"
                        width="25px"
                      />
                    ) : player.role == "Bowler" ? (
                      <img
                        src={bowler}
                        alt="playerImage"
                        height="25px"
                        width="25px"
                      />
                    ) :
                    player.role =="Keeper"?(
                      <img
                        src={Keeper}
                        alt="playerImage"
                        height="25px"
                        width="25px"
                      />

                    )
                    : 
                    (
                      null
                      // <img
                      //   src={batsman}
                      //   alt="playerImage"
                      //   height="25px"
                      //   width="25px"
                      // />
                    )}
                  </div>
                </div>
                <HrLine />
              </div>
            );
          })}
        </div>
      );
    } 
    else {
      return (
        <div style={pageStyle.squadListContainer}>
          No squad found for this team
        </div>
      );
    }
  }
}
const pageStyle = {
  squadName: {
    display: "flex",
    // flex: 0.6,
    justifyContent: "flex-start",
    alignItems: "center",
    fontFamily: "Montserrat",
    fontWeight: "300",
    fontSize: "14px",
    paddingLeft: "12px"
  },
  squadImage: {
    // marginLeft: 16,
    display: "flex",
    // flex: 0.1,
    // paddingLeft: "16px",
    // justifyContent: "flex-start",
    // alignItems: "center"
  },
  squadList: {
    display: "flex",
    flexDirection: "row",
    flex: 1,
    alignItems: "center",
    padding: 16,
    justifyContent: "space-between"
  },
  squadListContainer: {
    display: "flex",
    flexDirection: "column",
    flex: 1,
    // width: screenWidth * 0.911,
    backgroundColor: "#fff"
  },
  squadText: {
    display: "flex",
    flex: 0.1,
    fontFamily: "Montserrat",
    fontWeight: "500",
    fontSize: "12px",
    color: "#141b2f",
    justifyContent: "flex-start"
  },
  squadContainer: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    // width: screenWidth * 0.911,
    alignItems: "center",
    backgroundColor: "#fff",
    marginLeft: screenWidth * 0.044,
    marginRight:screenWidth * 0.044,
  },
  news: {
    display: "flex",
    marginTop: "12px",
    flex: 1,
    // width: screenWidth * 0.911,
    marginLeft: screenWidth * 0.044,
    marginRight:screenWidth * 0.044,
    
  },
  squad: {
    display: "flex",
    marginTop: "12px",
    flex: 1,
    // width: screenWidth * 0.911
    // marginLeft: screenWidth * 0.044,
    // marginRight:screenWidth * 0.044,

  },
  teamHistory: {
    display: "flex",
    marginTop: "12px",
    flex: 1,
    // width: screenWidth * 0.911
    marginRight:screenWidth * 0.044,
  },
  calenderContainer: {
    display: "flex",
    marginTop: "12px",
    flex: 1,
    // width: screenWidth * 0.911
    marginRight:screenWidth * 0.044,
  },
  eachButtomSvg: {
    display: "flex",
    flex: 0.2,
    // paddingLeft:'14px',
    justifyContent: "flex-start"
  },
  eachButtonText: {
    display: "flex",
    flex: 0.8,
    flexWrap: "wrap",
    fontWeight: "500",
    fontSize: "14px",
    fontFamily: "Montserrat"
  },
  eachButtonContainer: {
    display: "flex",
    flexDirection: "row",
    flex: 1,
    padding: "8px 0px 8px 8px",
    alignItems: "center",
    fontSize: "14px",
    height: "45px",
    width: "200px"
  },
  buttonBelowImageContainer: {
    display: "flex",
    flex: 1,
    flexDirection: "row",
    background: gradientRedOrangeLeft,
    width: screenWidth * 0.5,
    borderRadius: "3px",
    color: "#fff",
    fontSize: "12px",
    marginLeft: screenWidth * 0.25
  },
  container: {
    display: "flex",
    flex:1,
    flexDirection: "column",
    zIndex: "1",
    padding: "40px 0px",
    minHeight: "120vh"
  },
  image: {
    display: "flex",
    flex:1,
    height: screenHeight * 0.31,
    marginLeft: screenWidth * 0.044,
    marginTop: -screenHeight * 0.11,
    boxShadow: "0px 3px 6px 0px #13000000",
    borderRadius: "3px",
    marginRight:screenWidth * 0.044,
  }
};
