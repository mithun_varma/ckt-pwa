import React, { Component } from "react";
import {
  screenWidth,
  screenHeight
} from "../../../styleSheet/screenSize/ScreenDetails";

import {
  orange_1,
  grey_6,
  grey_4
} from "../../../styleSheet/globalStyle/color";
import HrLine from './../../commonComponent/HrLine';
import EmptyState from "./../../commonComponent/EmptyState";

const cardTitle = "You may also like";
export default class PlayerNews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardTitle: "News Title",
      newsData: []
    };
    this.truncate =this.truncate.bind(this);
  }
  componentDidMount() {
    this.setState({ cardTitle: cardTitle });
    // this.setState({ newsData: news });
  }

  _handelNewsFull(type,_id,title){

    this.props.history.push(`/articles/${type}/${_id}`);

  }
  _handelTeamNewsViewAll(type){
    this.props.history.push(`/articles?type=${type}`);
  }

  render() {
    // console.log(this.props.playerNews);
    const { playerNews } = this.props;
    return (
      <div style={pageStyle.playerNewsContainer}>
        <div style={pageStyle.playerNewsHead}>
          <span>Team News</span>
          {this.props.playerNews.length > 0 && (
            <span style={{ color: orange_1,fontSize:'12px',paddingRight:'12px' }} onClick={()=>{this._handelTeamNewsViewAll('news')}}>View All</span>
          )}
        </div>
        <HrLine />
        <div style={pageStyle.newsContainer}>
          {playerNews && playerNews.length > 0 ? (
                playerNews.map((data) => {
                  var dateOfPublish = data.publishedAt;
                  dateOfPublish = dateOfPublish.substring(0, 9);
                  const type = data.type;
                  const _id = data.id||data._id;
                  const title =data.title;
                  const imageUrl = data && data.imageData? 
                                    data.imageData.thumbnail||data.imageData.image || "https://via.placeholder.com/145"
                                    : 
                                    "https://via.placeholder.com/145"
                                     

                return (
                <div style={pageStyle.playerNewsView} id={data.type} onClick={()=>{this._handelNewsFull(type,_id,title )}}>
                    <div
                    style={{
                        ...pageStyle.playerNewsViewImage,
                        background: `url(${imageUrl}) top center no-repeat`,
                        backgroundSize: "cover",
                        backgroundPosition:'center',
                        borderTopLeftRadius:'2px',
                        borderTopRightRadius:'2px',
                    }}
                    />
                    <div style={{ padding: "12px", background: grey_4,borderBottomLeftRadius:'2px',borderBottomRightRadius:'2px' }}>
                      <div style={pageStyle.playerNewsViewHeading}>
                          {data.title}
                      </div>
                      <div style={pageStyle.playerNewsViewDescription}>
                          {this.truncate(data.description,20)}
                      </div>
                      <div
                          style={{
                          display: "flex",
                          flexDirection: "column",
                          width: screenWidth * 0.8,
                          marginTop: "9px"
                          }}
                          >
                          <div style={pageStyle.playerNewsViewAuthor}>
                          {data.author}
                          </div>
                          <div style={pageStyle.playerNewsViewDate}>
                            <span>{dateOfPublish}</span>
                            <span style={{ padding: "0 6px" }}> &bull;</span>
                            <span style={{ color: '#727682',fontSize:'11px' }}>
                                {data.readTime}
                            </span>
                          </div>
                      </div>
                    </div>
                </div>
                );
            }
            )
        
          )
           : 
           (
            <EmptyState msg="no news found" />
          )}
        </div>
        {/* <div style={pageStyle.playerNewsMoreNews}>
                    Related News
                </div> */}
      </div>
    );
  }
  truncate(str, no_words) {
    if(str != null){
      const sendString= str.split(" ").splice(0,no_words).join(" ");
      return sendString+ " ..."
    }
    
  }
}

const pageStyle = {
  newsContainer: {
    display: "flex",
    flexDirection: "row",
    backgroundColor: "#fff",
    overflowY: "scroll",
    marginTop: "12px",
  },
  playerNewsContainer: {
    display: "flex",
    flexDirection: "column",
    overflow: "hidden",
    backgroundColor: "#fff",
    paddingLeft:'16px',
    paddingBottom:'16px',
    paddingTop:'12px',
    fontFamily: "Montserrat",
    width:screenWidth*0.911,
  },
  playerNewsHead: {
    display: "flex",
    justifyContent: "space-between",
    color: "#141b2f",
    fontSize: "14px",
    paddingBottom: "12px",
    fontWeight: "500"
  },
  playerNewsView: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#fff",
    width: screenWidth*0.645,
    paddingRight:'12px',
    boxShadow: "0 3px 6px 0 #13000000",
    borderRadius:'2px',

  },
  playerNewsViewImage: {
    display: "flex",
    height: screenHeight * 0.2468
  },
  playerNewsViewTime: {
    display: "flex",
    width: screenWidth * 0.35,
    padding: "5px",
    color: "#ffffff",
    justifyContent: "center",
    alignItems: "center",
    backgroundImage: "linear-gradient(to right, #ea6550, #fa9441)",
    marginTop: "-20px"
  },
  playerNewsViewHeading: {
    display: "flex",
    color: "#141b2f",
    fontSize: "14px",
    fontWeight: "500",
    fontFamily: 'Montserrat',
  },
  playerNewsViewDescription: {
    display: "flex",
    color: "#141b2f",
    fontSize: "12px",
    marginTop: "8px",
    paddingTop: "5px",
    paddingBottom: "5px",
    paddingRight: "5px",

    fontWeight:'300',
  },
  playerNewsViewAuthor: {
    display: "flex",
    justifyContent: "flex-start",
    color: grey_6,
    fontSize: "12px",
    fontStyle: "italic"
  },
  playerNewsViewDate: {
    display: "flex",
    fontWeight: "500",
    color: "#141b2f",
    fontSize: "11px"
  },
  playerNewsMoreNews: {
    display: "flex",
    flex: 1,
    padding: "5px 5px 10px 0",
    color: "#f76b1c",
    fontSize: "14px",
    justifyContent: "flex-end"
  }
};
