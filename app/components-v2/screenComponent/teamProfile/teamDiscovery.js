import React, {Component} from 'react';
import TopHeader from './../../commonComponent/TopHeader';
import ImageContainer from '../../commonComponent/ImageContainerTeams';
import PlayerDiscoveryImage from './../../../styleSheet/images/PlayerDiscovery.jpg'
import { screenWidth } from '../../../styleSheet/screenSize/ScreenDetails';

export default class TeamsDiscovery extends Component{
    constructor(props){
        super(props);

    }
    componentDidMount(){

    }
    render(){
      const history = this.props.history;
      const teamList= this.props.teamList;
      loadMoreTeamsList = (type) => {
        let allTeamsList = this.props.teams.allTeamsList;
        let teamsListByCategory;
        for (let index = 0; index < allTeamsList.length; index++) {
          teamsListByCategory = allTeamsList[index];
          if (teamsListByCategory.type === type) {
            this.props.fetchMoreTeamListByCategory(
              {
                type: type,
                skip: teamsListByCategory.teams.length,
                limit: 5
              }
            )
            break;
          }
        }
      }

      if(teamList != null ){
        return(
            <div style={pageStyle.conatiner} >
                <TopHeader title = "Teams" history={this.props.history}/>
                <div style={{marginTop:-50}}>
                {
                  teamList.map((team)=>{
                    return(
                      <div style={pageStyle.other}>
                        <ImageContainer 
                            isAvatar
                            history={history}
                            data={team.teams}
                            rootStyles={{marginTop: 50}}
                            title={team.type}
                            getGroundDetails={this.loadMoreTeamsList}
                        />
                      </div>

                    );
                  })
                }
                </div>
                {/* 
           */}
            </div>
        );
      }
      else{

        return (null);
      }
    }
}

const pageStyle={
    conatiner:{
        display:'flex',
        flexDirection:'column',
        width:screenWidth*0.911,
    },
    TopHeader:{
        display:'flex',
    },
    avatar:{
        display:"flex",
        flexDirection:'row',
    },
    other:{
        display:'flex',
        overflow:'scroll',
        width:screenWidth*0.97,
    }



}

const popular = {
    header: "Popular Stadiums",
    content: [
      {
        name: "Eden Gardens, Kolkata",
        img: PlayerDiscoveryImage
      },
      {
        name: "MCG, Sydney",
        img: PlayerDiscoveryImage
      },
      {
        name: "Eden Gardens, Kolkata",
        img: PlayerDiscoveryImage
      },
      {
        name: "MCG, Sydney",
        img: PlayerDiscoveryImage
      },
      {
        name: "Eden Gardens, Kolkata",
        img:PlayerDiscoveryImage
      }
    ]
  };
  
  const trending = {
    header: "Trending Player",
    content: [
      {
        name: "Eden Gardens, Kolkata",
        img: PlayerDiscoveryImage
      },
      {
        name: "MCG, Sydney",
        img: PlayerDiscoveryImage
      },
      {
        name: "Eden Gardens, Kolkata",
        img: PlayerDiscoveryImage
      },
      {
        name: "MCG, Sydney",
        img: PlayerDiscoveryImage
      },
      {
        name: "Eden Gardens, Kolkata",
        img: PlayerDiscoveryImage
      }
    ]
  };
  const similarGrounds = {
    header: "Player Similar to Eden Gardens",
    content: [
      {
        name: "Eden Gardens, Kolkata",
        img: PlayerDiscoveryImage
      },
      {
        name: "MCG, Sydney",
        img: PlayerDiscoveryImage
      },
      {
        name: "Eden Gardens, Kolkata",
        img: PlayerDiscoveryImage
      },
      {
        name: "MCG, Sydney",
        img: PlayerDiscoveryImage
      },
      {
        name: "Eden Gardens, Kolkata",
        img: PlayerDiscoveryImage
      }
    ]
  };
  
  const mostMatches = {
    header: "Player with Most Matches",
    content: [
      {
        name: "Eden Gardens, Kolkata",
        img: PlayerDiscoveryImage
      },
      {
        name: "MCG, Sydney",
        img:PlayerDiscoveryImage
      },
      {
        name: "Eden Gardens, Kolkata",
        img:PlayerDiscoveryImage
      },
      {
        name: "MCG, Sydney",
        img: PlayerDiscoveryImage
      },
      {
        name: "Eden Gardens, Kolkata",
        img:PlayerDiscoveryImage
      }
    ]
  };
  
  const highestBatting = {
    header: "Player with Highest Batting S/R",
    content: [
      {
        name: "Eden Gardens, Kolkata",
        img: PlayerDiscoveryImage
      },
      {
        name: "MCG, Sydney",
        img: PlayerDiscoveryImage
      },
      {
        name: "Eden Gardens, Kolkata",
        img: PlayerDiscoveryImage
      },
      {
        name: "MCG, Sydney",
        img: PlayerDiscoveryImage
      },
      {
        name: "Eden Gardens, Kolkata",
        img:PlayerDiscoveryImage
      }
    ]
  };
  
  const highestBowling = {
    header: "Player with Highest Bowling S/R",
    content: [
      {
        name: "Eden Gardens, Kolkata",
        img: PlayerDiscoveryImage
      },
      {
        name: "MCG, Sydney",
        img: PlayerDiscoveryImage
      },
      {
        name: "Eden Gardens, Kolkata",
        img: PlayerDiscoveryImage
      },
      {
        name: "MCG, Sydney",
        img: PlayerDiscoveryImage
      },
      {
        name: "Eden Gardens, Kolkata",
        img: PlayerDiscoveryImage
      }
    ]
  };
  
  const favouriteStatium = {
    header: "Virat favourite Stadiums",
    content: [
      {
        name: "Eden Gardens, Kolkata",
        img: PlayerDiscoveryImage
      },
      {
        name: "MCG, Sydney",
        img: PlayerDiscoveryImage
      },
      {
        name: "Eden Gardens, Kolkata",
        img: PlayerDiscoveryImage
      },
      {
        name: "MCG, Sydney",
        img: PlayerDiscoveryImage
      },
      {
        name: "Eden Gardens, Kolkata",
        img: PlayerDiscoveryImage
      }
    ]
  };