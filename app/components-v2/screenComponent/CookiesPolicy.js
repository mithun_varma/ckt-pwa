import React, {Component} from 'react';
import { screenWidth } from '../../styleSheet/screenSize/ScreenDetails';
import Header from "./../commonComponent/Header";
import { white, gradientRedOrangeLeft } from '../../styleSheet/globalStyle/color';


export default class CookiesPolicy extends Component {
    constructor(props){
        super(props);
        this.state={
            isHeaderBackground: true
        };

    }
    render(){
        return(
            <div>
            <div
            style={{
              fontFamily: "Montserrat",
              fontWeight: 700,
              fontSize: 22,
              color: white,
              marginBottom: 12,
              minHeight: "90px",
              paddingLeft: "16px",
              textTransform: "capitalize",
              background: gradientRedOrangeLeft
            }}
          >
            {null}
          </div>

          <div>
            <Header
              title={"Cookies Policy"}
              isHeaderBackground={this.state.isHeaderBackground}
              leftArrowBack
              textColor={white}
              backgroundColor={gradientRedOrangeLeft}
              history={this.props.history}
              leftIconOnClick={() => {
                this.props.history.goBack();
              }}
            />
          </div>
          <div style={pageStyle.conatiner}>
                <div style={{display:'flex',flex:1, fontSize:'16px',fontWeight:'700',padding:'10px'}}>
                    Cookies Statement
                </div>
                <div style={pageStyle.heading}>
                    Cookies
                </div>
                <div style={pageStyle.paragraph}>
                Cricket.com (the “Site”) and its related mobile applications (the “Apps) use a number of different
                cookies on the Site and the Apps to help us remember details of your visit to the Site and Apps. The Site
                and its Apps are owned and operated by Crictec Media Limited, a company registered in Ireland with
                company number 630962 and having its registered address at 2 nd Floor, Palmerston House, Fenian
                Street, Dublin 2, Ireland.
                </div>
                <div style={pageStyle.paragraph} >
                A cookie is a small text file placed on your computer or other internet enabled device in order to add
                functionality. It allows a website to remember your actions or preferences over a period of time. Some
                of the cookies we use are essential for the Site and Apps to work. Other cookies that we used are not
                essential for the Site and the Apps to work but are useful to assist us to collect information about the
                number of visitors to our Site and Apps for example. For further information about cookies, or how to
                control or delete them, then we recommend you visit http://www.aboutcookies.org for detailed
                guidance.
                </div>
                <div style={pageStyle.paragraph}>
                The list below describes the cookies we use on the Site and Apps and what we use them for. To the
                extent that we may process personal data in connection with our use of cookies, our Privacy Statement
                [Insert link to Privacy Statement, once finalised] will apply to our use of such personal data and should
                be read in conjunction with this Cookies Statement. If you are not happy with our use of cookies, then
                we recommend that you either not use the Site and Apps, or manage your cookie settings in accordance
                with the ‘Managing and disabling cookies section’ below.
                </div>
                <div  style={pageStyle.heading}>
                Log Files
                </div>
                <div style={pageStyle.paragraph}>
                Like many other websites and mobile applications, the Site and Apps make use of log files. The
                information inside the log files includes internet protocol ( IP ) addresses, type of browser, Internet
                Service Provider ( ISP ), date/time stamp, referring/exit pages, and number of clicks to analyze trends,
                administer the site, track user’s movement around the site, and gather demographic information. IP
                addresses, and other such information generally are not linked to any information that might identify
                you.
                </div>
                <div style={pageStyle.heading}>
                    First Party Cookies and Web Beacons
                </div>
                <div style={pageStyle.paragraph}>
                The Site and Apps use cookies to store information about visitors’ preferences, records user-specific
                information on which pages the user accesses or visits, customises web page content based on visitors
                browser type or other information that the visitor sends via his/her browser.
                </div>
                <div style={pageStyle.paragraph}>
                Our Sites and Apps use different types of cookies to improve your experience and to provide services
                and advertising. We use:
                </div>
                <div style={pageStyle.paragraph}>
                 Essential cookies to enable services you specifically asked for.
                </div>
                <div style={pageStyle.paragraph}>
                 Functionality cookies to recognize when you return to our Site and/or Apps so that we can
remember your preferences.
                </div>
                <div style={pageStyle.paragraph}>
                 Analytics and performance cookies to count visitors and how our Site and Apps are used. These
help us understand our audience on an aggregate basis. For example number of visitors to a Site

or an App, where visitors are visiting from, how many pages they look at and how long they stay
on a page or visit. This information is not used to track individual users.
                </div>
                <div style={pageStyle.paragraph}>
                 Advertising cookies to measure interactions with ads and prevent showing the same ads to you
too often. Also to make advertising more relevant to you and more valuable to us and
advertisers. These cookies are anonymous and we cannot identify you from the cookies we
place.
                </div>
                <div style={pageStyle.paragraph}>
                The table below identifies the specific cookies we use, why and their duration
                </div>
                <div style={pageStyle.paragraph}>
                    <table style={pageStyle.table}>
                        <tr style={pageStyle.tr}>
                            <th style={pageStyle.thNew}>
                            Cookie name
                            </th>
                            <th style={pageStyle.thNew}>
                            Purpose
                            </th>
                            <th style={pageStyle.thNew}>
                            Expiry
                            </th>
                        </tr>
                        <tr style={pageStyle.tr}>
                            <th style={pageStyle.thNew}>
                            resultid
                            </th>
                            <th style={pageStyle.thNew}>
                            Fan polls tracking
                            </th>
                            <th style={pageStyle.thNew}>
                            30 days
                            </th>
                        </tr>
                        <tr style={pageStyle.tr}>
                            <th style={pageStyle.thNew}>
                            selected
                            Option
                            </th>
                            <th style={pageStyle.thNew}>
                            User selected option for specific fab poll
                            </th>
                            <th style={pageStyle.thNew}>
                            30 days
                            </th>
                        </tr>
                    </table>
                </div>
                
                <div style={pageStyle.heading}>
                    Third Party Cookies
                </div>
                <div style={pageStyle.paragraph}>
                    These are cookies set on your machine and device by external websites whose services are used on the
                    Site and Apps. Examples of these types of cookies are the sharing buttons across the Site and Apps that
                    allow visitors to share content onto social networks. Cookies are currently set by **LinkedIn, Twitter,
                    Facebook, Google+ and Pinterest. In order to implement these buttons, and connect them to the
                    relevant social networks and external sites, there are scripts from domains outside of our Site and Apps.
                    You should be aware that these websites may collect personal data about you and what you are doing
                    all around the internet, including on this website, and you should familiarize yourself with their
                    respective privacy statements and cookie statements to understand how they may use such
                    information.
                </div>
                <div style={pageStyle.paragraph}>
                    For analytics, we use Clevertap analytics to help us understand how visitors engage with our Site and
                    Apps. Clevertap Analytics uses cookies to store non personal data. You can find out more in their privacy
                    information.               
                </div>
                <div style={pageStyle.paragraph}>
                    Advertisers sometimes use their own cookies or similar technologies like web beacons for example to
                    better target their ads, to measure their ad campaign or to stop showing you an ad.
                </div>
                <div style={pageStyle.paragraph}>
                    The table below identifies the specific third party cookies we use, why and their duration:
                </div>

                <div style={pageStyle.paragraph}>
                    <table style={pageStyle.table}>
                        <tr style={pageStyle.tr}>
                            <th style={pageStyle.th}>
                            Provider
                            </th>
                            <th style={pageStyle.th}>
                            Cookie name
                            </th>
                            <th style={pageStyle.th}>
                            Purpose
                            </th>
                            <th style={pageStyle.th}>
                            Expiry
                            </th>
                        </tr>
                        <tr style={pageStyle.tr}>
                                <th style={pageStyle.th}>
                                Google
                                </th>
                                <th style={pageStyle.th}>
                                [  _ga ]
                                </th>
                                <th style={pageStyle.th}>
                                Used to distinguish users
                                </th>
                                <th style={pageStyle.th}>
                                2 years
                                </th>
                        </tr>
                        <tr style={pageStyle.tr}>
                            <th style={pageStyle.th}>
                            Google
                            </th>
                            <th style={pageStyle.th}>
                            [  _gat  ]
                            </th>
                            <th style={pageStyle.th}>
                            Used to throttle request rate
                            </th>
                            <th style={pageStyle.th}>
                            1 min
                            </th>
                        </tr>
                        <tr style={pageStyle.tr}>
                            <th style={pageStyle.th}>
                            Google
                            </th>
                            <th style={pageStyle.th}>
                            [  _gid  ]
                            </th>
                            <th style={pageStyle.th}>
                            Used to distinguish users
                            </th>
                            <th style={pageStyle.th}>
                            1 day
                            </th>
                        </tr>
                        <tr style={pageStyle.tr}>
                            <th style={pageStyle.th}>
                            Clevertap
                            </th>
                            <th style={pageStyle.th}>
                            [WZRK_S_
                            TEST-Z88
                            -4KR-845Z
                            ]
                            </th>
                            <th style={pageStyle.th}>
                            Used to distinguish users
                            </th>
                            <th style={pageStyle.th}>
                            1 day
                            </th>
                        </tr>
                        <tr style={pageStyle.tr}>
                            <th style={pageStyle.th}>
                            Clevertap
                            </th>
                            <th style={pageStyle.th}>
                            [WZRK_G]
                            </th>
                            <th style={pageStyle.th}>
                            Duplicate tracking
                            </th>
                            <th style={pageStyle.th}>
                            10 years
                            </th>
                        </tr>
                        
                    </table>

                </div>
                
                <div style={pageStyle.heading}>
                Managing and Disabling Cookies
                </div>
                <div style={pageStyle.paragraph}>
                If you wish to disable cookies, you may do so through your individual browser options. More detailed
                information about cookie management with specific web browsers can be found at the browsers&#39;
                respective websites.
                You can control and/or delete cookies as you wish. You can delete all cookies that are already on your
                device and you can set most browsers to prevent them from being placed. If you do this, however, you
                may have to manually adjust some preferences every time you visit a site and some services and
                functionalities may not work as intended or at all.
                </div>
                
                
            </div>
            </div>
        );
    }

}
const pageStyle={
    conatiner:{
        display:'flex',
        flex:1,
        flexDirection:'column',
        fontFamily:'Montserrat',
        backgroundColor:'#fff',
        marginLeft:screenWidth*0.0411,
        marginRight:screenWidth*0.0411,
    },
    heading:{
        padding:'5px 10px 5px 10px',
        display:'flex',
        flex:1,
        fontWeight:'500',
        fontSize:'14px',
    },
    paragraph:{
        padding:'1px 10px 1px 10px',
        display:'flex',
        flex:1,
        fontSize:'14px',
        fontWeight:'300',
    },
    table:{
        display:'flex',
        flex:1,
        flexDirection:"column",
        fontFamily:'Montserrat',
      },
      th:{
        display:'flex',
        flex:0.20,
        border: '1px solid #dddddd',
        textAlign: 'left',
        padding: '2px',
        fontSize:'12px',
        flexWrap: 'wrap',
        width:screenWidth*0.25,
      },
      thNew:{
        display:'flex',
        flex:0.30,
        border: '1px solid #dddddd',
        textAlign: 'left',
        padding: '2px',
        fontSize:'12px',
        flexWrap: 'wrap',
        width:screenWidth*0.25,
      },

      tr:{
          display:'flex',
          flex:1,
          flexDirection:'row'
      }
}