import React, {Component} from 'react';

import criclaticsButtonBlack from './../../../styleSheet/svg/criclaticsButtonBlack.svg';
import scheduleButtonBlack from './../../../styleSheet/svg/scheduleButtonBlack.svg';
import moreButtonBlack from './../../../styleSheet/svg/moreButtonBlack.svg';
import homeButtonBlack from './../../../styleSheet/svg/homeButtonBlack.svg';
import { screenHeight, screenWidth } from '../../../styleSheet/screenSize/ScreenDetails';


export default class LowerButton extends Component{
    constructor(props){
        super(props);
        this.state={

        };
        this._handelMoreClickParent=this._handelMoreClickParent.bind(this);
        this._handelLowerButtonClick = this._handelLowerButtonClick.bind(this);

    }
    componentDidMount(){

    }
    _handelMoreClickParent(){
        this.props.showMoreScreen();
    }

    _handelLowerButtonClick(event){
        const clicked = event.target.innerHTML;
        if(clicked==='HOME'){
            this.props.history.push("/");
            this.props._handelMoreScreenFromLowerButton();


        }else if (clicked==='SCHEDULE'){
            this.props.history.push("/schedule");
            this.props._handelMoreScreenFromLowerButton();

        }
        else if(clicked==='CRICLYTICS'){
            this.props.history.push("/players");
            this.props._handelMor_handelMoreScreenFromLowerButtoneClickParent();

        }
        else if (clicked === 'MORE'){
            this.props.showMoreScreen();

        }
        return "null";


    }
    render(){
        return(
            <div style={pageStyle.container}>
                <div style={{display:'flex',flex:0.10}}>
                    {null}
                </div>

                <div style={pageStyle.iconStyleConatiner} onClick ={this._handelLowerNavigatrion}>
                    <div style={pageStyle.iconStyle}>
                        <img src={homeButtonBlack} alt={"homeButton"} />
                    </div>
                    <div  key="value" style={pageStyle.iconNameStyle} onClick={this._handelLowerButtonClick }>
                        HOME
                    </div>
                </div>

                <div style={{display:'flex',flex:0.10}}>
                    {null}
                </div>

                <div style={pageStyle.iconStyleConatiner} onClick ={this._handelLowerNavigatrion}>
                    <div style={pageStyle.iconStyle}>
                        <img src={scheduleButtonBlack} alt={"scheduleButton"} />

                    </div>
                    <div  key="value" style={pageStyle.iconNameStyle} onClick={this._handelLowerButtonClick }>
                        SCHEDULE
                    </div>
                </div>

                <div style={{display:'flex',flex:0.10}}>
                    {null}
                </div>

                <div style={pageStyle.iconStyleConatiner} onClick ={this._handelLowerNavigatrion}>
                    <div style={pageStyle.iconStyle}>
                        <img src={criclaticsButtonBlack} alt={"criclatics"} />

                    </div>
                    <div  key="value" style={pageStyle.iconNameStyle} onClick={this._handelLowerButtonClick }>
                        CRICLYTICS
                    </div>
                </div>

                <div style={{display:'flex',flex:0.10}}>
                    {null}
                </div>

                <div style={pageStyle.iconStyleConatiner} onClick ={this._handelMoreClickParent}>
                    <div style={pageStyle.iconStyle}>
                        <img src={moreButtonBlack} alt ={'moreButton'} />
                    </div>
                    <div  key="value" style={pageStyle.iconNameStyle} onClick={this._handelLowerButtonClick }>
                        MORE
                    </div>
                </div>

                <div style={{display:'flex',flex:0.10}}>
                    {null}
                </div>
                
            </div>
        );
    }

}

const pageStyle ={
    iconNameStyle:{
        display:'flex',
        flex:1,
        justifyContent:'center',
        color:'#141b2f',
        fontSize:'10px',
        

    },
    iconStyle:{
        display:'flex',
        flex:1,
        justifyContent:'center',
        marginBottom:'2px',

    },
    iconStyleConatiner:{
        display:'flex',
        flex:0.15,
        flexDirection:'column',
        justifyContent:'center',
    },
    container:{
        display:'flex',
        flexDirection:'row',
        height:screenHeight*0.0875,
        width:screenWidth,
        backgroundColor:'#fff',
        boxShadow:'0px -3px 3px 0px #1e000000',
        alignItems:'center',
    },
}