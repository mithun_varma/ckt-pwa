/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import playerJersey from './../../assests_new/images/playerProfileJersseyNumber.png'

const widthScreen = window.innerWidth;
const heightScreen = window.innerHeight;


export default class PlayerScore extends Component {
    constructor(props) {
        super(props);
        this.state = {
            topActionBarActiveField: 'International',
        };
        this._handelTopActionBar = this._handelTopActionBar.bind(this);
    }
    componentDidMount() {

    }
    _handelTopActionBar(event) {

    }
    render() {
        return (
            <div style={pageStyle.container}>
                <div style={{ height: heightScreen * 0.05, display: 'flex', width: widthScreen * 0.90 }}>
                    {this.topActionBar()}
                </div>
                <div style={{ height: heightScreen * 0.06, display: 'flex', width: widthScreen * 0.90, flexDirection: 'column' }}>
                    <div style={{ dispaly: 'flex', flex: 1 }}>
                        {this.formatActionBar()}
                    </div>
                    <div style={{ dispaly: 'flex', flex: 1 }}>
                        {this.matchDetails()}
                    </div>
                    <div style={{ dispaly: 'flex', flex: 1 }}>
                        {this.scoreTotal()}
                    </div>
                    <div style={{ display: 'flex', flex: 1 }}>
                        {this.detailsScoreView()}
                    </div>


                </div>


            </div>
        );
    }

    topActionBar() {
        const applicableStyle = pageStyle.topActionBarButtonActive
        return (
            <div style={pageStyle.topActionBarButtonContainer}>
                <div style={pageStyle.topActionBarButtonDeactive} onClick={this._handelTopActionBar} name="International" >
                    International
                </div>
                <div style={applicableStyle} onClick={this._handelTopActionBar} name="Domestic" >
                    Domestic
                </div>
                <div style={pageStyle.topActionBarButtonDeactive} onClick={this._handelTopActionBar} name="IPL" >
                    IPL
                </div>
            </div>
        );
    }

    formatActionBar() {
        return (
            <div style={pageStyle.formatActionBar}>
                <div style={pageStyle.formatSideline} onClick={this._handelFormat} name="line">

                </div>
                <div style={pageStyle.formatActiveButton} onClick={this._handelFormat} name="test">
                    TEST

                </div>
                <div style={pageStyle.formatUnactiveButton} onClick={this._handelFormat} name="odi">
                    ODI

                </div>

                <div style={pageStyle.formatUnactiveButton} onClick={this._handelFormat} name="t20">
                    T20

                </div>
                <div style={pageStyle.formatSideline} onClick={this._handelFormat} name="line">

                </div>

            </div>
        );
    }

    matchDetails() {
        return (
            <div style={pageStyle.matchDetails}>
                <div style={pageStyle.matchDetailsData}>
                    <div style={{ display: 'flex', flexDirection: 'column', flex: 1 }}>
                        <div style={pageStyle.matchDetailsDataRowData} >
                            Hello 1

                        </div>
                        <div style={pageStyle.matchDetailsDataRowData}>
                            Hello 2
                        </div>
                    </div>

                </div>
                <div style={pageStyle.matchDetailsImage}>
                    <img src={playerJersey} width={widthScreen * 0.20} height={widthScreen * 0.20} alt="player jersey" />
                </div>
            </div>
        );


    }

    scoreTotal() {
        return (
            <div style={pageStyle.scoreTotalContainer}>
                <div style={pageStyle.scoreTotalContainerData}>
                    <div style={{ fontSize: '12px', color: '#141b2f' }}>
                        Runs
                        <div style={{ fontSize: '26px', color: '#141b2f' }}>
                            77
                        </div>
                    </div>

                </div>
                <div style={pageStyle.scoreTotalContainerData}>
                    <div style={{ fontSize: '12px', color: '#141b2f' }}>
                        Total Runs
                        <div style={{ fontSize: '26px', color: '#141b2f' }}>
                            6613
                        </div>
                    </div>

                </div>
            </div>
        );

    }

    detailsScoreView() {
        return (
            <div style={pageStyle.detailsScoreView}>
                <div style={pageStyle.detailsScoreViewRow}>
                    <div style={pageStyle.detailsScoreViewRowData}>
                        Text 01
                    </div>
                    <div style={pageStyle.detailsScoreViewRowData}>
                        Text 01
                    </div>
                    <div style={pageStyle.detailsScoreViewRowData}>
                        Text 01
                    </div>
                    <div style={pageStyle.detailsScoreViewRowData}>
                        Text 01
                    </div>
                </div>
                <div style={pageStyle.detailsScoreViewRow}>
                    <div style={pageStyle.detailsScoreViewRowData}>
                        Text 01
                    </div>
                    <div style={pageStyle.detailsScoreViewRowData}>
                        Text 01
                    </div>
                    <div style={pageStyle.detailsScoreViewRowData}>
                        Text 01
                    </div>
                    <div style={pageStyle.detailsScoreViewRowData}>
                        Text 01
                    </div>
                </div>

            </div>
        );

    }

    playerBio() {

    }

    playerNews() {

    }

    playerSocial() {

    }

    playerMoment() {

    }


}

const pageStyle = {
    container: {
        display: 'flex',
        width: widthScreen * 0.90,
        height: heightScreen * 0.65,
        backgroundColor: '#fff',
        boxShadow: '0px 3px 6px 0px #13000000',
        flexDirection: 'column',
    },

    topActionBarButtonContainer: {
        display: 'flex',
        flex: 1,
        width: widthScreen * 0.90,
        flexDirection: 'row',
        borderBottomWidth: '1px',
        borderBottomColor: '#e3e4e6',
        borderBottomStyle: 'solid',

    },
    topActionBarButtonActive: {
        display: 'flex',
        flex: 0.33,
        fontSize: '12px',
        fontWeight: '700',
        fontColor: '#141b2f',
        justifyContent: 'center',
        paddingTop: heightScreen * 0.02,
        paddingBottom: heightScreen * 0.02,
        letterSpacing: "0.05",
        fontFamily: 'Montserrat-Medium',
        borderBottomWidth: '2px',
        borderBottomColor: '#f76b1c',
        borderBottomStyle: 'solid',
    },
    topActionBarButtonDeactive: {
        display: 'flex',
        flex: 0.33,
        fontSize: '12px',
        fontColor: '#727682',
        justifyContent: 'center',
        paddingTop: heightScreen * 0.02,
        paddingBottom: heightScreen * 0.02,
        letterSpacing: "0.05",
        fontFamily: 'Montserrat-Medium',

    },
    formatActionBar: {
        display: 'flex',
        flexDirection: 'row',
        flex: 1,
        marginTop: heightScreen * 0.03,
        marginBottom: heightScreen * 0.03,
    },
    formatActiveButton: {
        display: 'flex',
        flex: 0.21,
        alignItems: 'center',
        justifyContent: 'center',
        height: heightScreen * 0.04,
        border: '1px solid #e0e1e4',
        fontSize: '13px',
        color: '#fff',
        fontFamily: 'Montserrat-Medium',
        backgroundImage: 'linear-gradient(to right, #fa9441, #ea6550)',

    },
    formatUnactiveButton: {
        display: 'flex',
        flex: 0.21,
        alignItems: 'center',
        justifyContent: 'center',
        height: heightScreen * 0.04,
        border: '1px solid #e0e1e4',
        fontSize: '13px',
        color: '#a1a4ac',
        fontFamily: 'Montserrat-Medium',

    },
    formatSideline: {
        display: 'flex',
        flex: 0.18,
    },
    matchDetails: {
        display: 'flex',
        flexDirection: 'row',
        flex: 1,
    },
    matchDetailsData: {
        display: 'flex',
        flex: 0.70,
        height: heightScreen * 0.20,
    },
    matchDetailsImage: {
        display: 'flex',
        flex: 0.30,
        height: heightScreen * 0.20,
    },
    matchDetailsDataRow: {
        display: 'flex',
        flex: 1,
        flexDirection: 'row',
    },
    matchDetailsDataRowData: {
        display: 'flex',
        flex: 0.5,
        justifyContent: 'flex-start',
    },
    scoreTotalContainer: {
        display: 'flex',
        flexDirection: 'row',
        flex: 1,
        backgroundImage: 'linear-gradient(to right, #ffffff ,#f0f1f2, #f0f1f2, #ffffff )',
        height: heightScreen * 0.10,
        // opacity: 0.5,
        alignItems: 'center',
        color: '#141b2f',

    },
    scoreTotalContainerData: {
        display: 'flex',
        flex: 0.50,
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: '#a1a4ac',
        justifyContent: 'center',
    },
    detailsScoreView: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        height: heightScreen * 0.25,
    },
    detailsScoreViewRow: {
        display: 'flex',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: '5px',
    },
    detailsScoreViewRowData: {
        dispaly: 'flex',
        flex: 0.25,
        justifyContent: 'center',
        alignItems: 'center',
        height: heightScreen * 0.12,
        borderRightWidth: '1px',
        borderBottomColor: '#a1a4ac',
        borderBottomStyle: 'solid',
        color: '#141b2f',
    },





}