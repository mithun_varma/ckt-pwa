import React, { Component } from "react";

import TabBar from "components-v2/commonComponent/TabBar";
import {
  white,
  grey_6,
  grey_10,
  grey_8,
  gradientGreenYellow,
  grey_2,
  darkRed
} from "../../../styleSheet/globalStyle/color";
import PillTabs from "../../../components/Common/PillTabs";
import bgMask from "../../../images/mask@3x.png";
import virat from "../../../images/bitmap-copy-2@3x.png";

export class PlayerCompare extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activePill: "Spin",
      activeItem: "T20",
      type: "defence"
    };
  }

  handleMatchSelect = val => {
    // console.log(val);
    // if (this.state.activePill !== val) {
    this.setState({
      activePill: val
    });
    // }
  };

  handleActiveTab = activeItem => {
    this.setState({
      activeItem
    });
  };

  handleTabChange = type => {
    this.setState({
      type
    });
  };
  render() {
    return (
      <div
        style={{
          background: white,
          padding: "10px 0 0",
          borderRadius: 3,
          marginTop: "9%",
          minHeight: "300px",
          position: "relative",
          fontWeight: "500",
          fontFamily: "Montserrat"
          // boxShadow: cardShadow
        }}
      >
        <TabBar
          items={["T20", "ODI", "TEST"]}
          activeItem={this.state.activeItem}
          onClick={this.handleActiveTab}
        />
        <PillTabs
          items={["Pace", "Spin"]}
          activeItem={this.state.activePill}
          onTabSelect={this.handleMatchSelect}
          rootStyles={{ width: "40%", margin: '16px auto' }}
        />
        <div style={pageStyle.containerStyle}>
          <div
            style={{
              width: "25%",
              display: "flex",
              alignItems: "center",
              flexDirection: "column",
              justifyContent: "flex-end",
              marginLeft: "3%"
            }}
          >
            <img
              src={virat}
              style={{
                width: "50px",
                height: "50px",
                objectFit: "contain",
                margin: "5%",
                marginLeft: "0",
                borderRadius: "50%",
                background: grey_10
              }}
            />
            <span style={{ fontWeight: "500" }}>VIRAT KOHLI</span>
          </div>
          <div
            style={pageStyle.tabStyle}
            onClick={() => this.handleTabChange("defence")}
          >
            <img
              src={
                this.state.type === "defence"
                  ? require("../../../images/def_active.svg")
                  : require("../../../images/def_inactive.svg")
              }
              style={{ marginBottom: "6px" }}
            />
            <span
              style={
                this.state.type === "defence"
                  ? { color: darkRed }
                  : { color: "inherit" }
              }
            >
              DEFENCE
            </span>
          </div>
          <div
            style={pageStyle.tabStyle}
            onClick={() => this.handleTabChange("aggression")}
          >
            <img
              src={
                this.state.type === "aggression"
                  ? require("../../../images/aggr_active.svg")
                  : require("../../../images/aggr_inactive.svg")
              }
              style={{ marginBottom: "6px" }}
            />
            <span
              style={
                this.state.type === "aggression"
                  ? { color: darkRed }
                  : { color: "inherit" }
              }
            >
              AGGRESSION
            </span>
          </div>
          <div
            style={pageStyle.tabStyle}
            onClick={() => this.handleTabChange("strikerate")}
          >
            <img
              src={
                this.state.type === "strikerate"
                  ? require("../../../images/strike_active.svg")
                  : require("../../../images/strike_inactive.svg")
              }
              style={{ marginBottom: "6px" }}
            />
            <span
              style={
                this.state.type === "strikerate"
                  ? { color: darkRed }
                  : { color: "inherit" }
              }
            >
              STRIKE RATE
            </span>
          </div>
        </div>
        <div
          style={{
            width: "75%",
            padding: "16px 12px 12px 32px",
            fontSize: "11px",
            color: grey_8,
            fontWeight: "400"
          }}
        >
          Criclytics predicts Virat Kohli and Aaron Finch to be similar in
          Defence against Spin.
        </div>
        <div
          style={{
            background: gradientGreenYellow,
            color: white,
            padding: "10px 0 32px 32px",
            position: "absolute",
            bottom: "50px",
            zIndex: 10,
            width: "100%"
          }}
        >
          <p style={{ fontSize: "12px" }}>Aaron Finch</p>
          <p style={{ fontSize: "10px", color: grey_2 }}>Australia</p>
        </div>
        <div>
          <img
            src={require("../../../images/bitmap-copy-6@3x.png")}
            style={{
              position: "absolute",
              right: 12,
              zIndex: 15,
              bottom: "50px",
              maxHeight: "60%"
            }}
          />
        </div>
      </div>
    );
  }
}

const pageStyle = {
  tabStyle: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "center",
    color: grey_6,
    width: "20%"
  },
  containerStyle: {
    background: `url(${bgMask})`,
    height: "80px",
    position: "absolute",
    zIndex: 20,
    bottom: 0,
    display: "flex",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    flex: 1,
    justifyContent: "space-evenly",
    fontFamily: "Montserrat",
    fontSize: "8px",
    paddingBottom: "8px",
    fontWeight: "500",
    width: "100%"
  }
};

export default PlayerCompare;
