import React, { Component } from "react";
import Slider from "react-slick";
import Header from "./../../commonComponent/Header";
import { grey_10, white } from "../../../styleSheet/globalStyle/color";

export class PhotoView extends Component {
  render() {
    const { title, data, initialSlide } = this.props;
    const settings = {
      customPaging: i => {
        return (
          // <a>
          //   <img src={`${img}`} />
          // </a>
          <div
            style={{
              height: "52px",
              width: "52px",
              background: `url(${data[i].path || data[i].imageData.thumbnail})`,
              backgroundPosition: "top center",
              backgroundSize: "cover"
            }}
          />
        );
      },
      dots: true,
      dotsClass: "slick-dots slick-thumb",
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true
    };
    return (
      <div
        style={{
          //   padding: "54px 0px 0px 0px",
          minHeight: "100vh",
          paddingTop: "5vh"
        }}
      >
        <Header
          title={title}
          isHeaderBackground={true}
          leftArrowBack
          textColor={white}
          backgroundColor={grey_10}
          history={this.props.history}
          leftIconOnClick={() => {
            this.props.toggleView(false);
          }}
        />
        <div
          style={{
            display: "flex",
            width: "100vw",
            height: "95vh",
            overflowY: "hidden",
            background: grey_10
          }}
        >
          <div
            style={{
              // width: "100vw",
              width: "90vw",
              margin: "0 auto",
              //   height: "93vh",
              //   background: grey_10
              // margin: "auto 10px"
              // display: 'flex'
              // height: "80vh",
              marginTop: `${data[0].path ? "20vh" : "10vh"}`
            }}
          >
            <Slider
              {...settings}
              initialSlide={initialSlide ? initialSlide : 0}
            >
              {data &&
                data.map(
                  img =>
                    img.path ? (
                      <div>
                        <div
                          // src={img}
                          style={{
                            background: `url(${img.path ||
                              img.imageData.image})`,
                            backgroundSize: "cover",
                            backgroundPosition: "top center",
                            height: "300px",
                            backgroundRepeat: "no-repeat"
                          }}
                        />
                        {/* <img
                      src={img.path}
                      width="100%"
                      style={{ objectFit: "cover" }}
                    /> */}
                        <p
                          style={{
                            color: white,
                            fontFamily: "Mont400",
                            fontSize: "14px",
                            padding: "8px"
                          }}
                        >
                          {img.shortDescription}
                        </p>
                      </div>
                    ) : (
                      <img src={img.imageData.image} />
                    )
                )}
            </Slider>
          </div>
        </div>
      </div>
    );
  }
}

export default PhotoView;
