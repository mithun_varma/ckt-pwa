/* eslint-disable prettier/prettier */
/* eslint-disable no-undef */
import React, { Component } from 'react';
import { screenWidth } from '../../../styleSheet/screenSize/ScreenDetails';
import facebookIcon from './../../../styleSheet/svg/facebookIcon.svg';
import instaIcon from './../../../styleSheet/svg/instaIcon.svg';
import twitterIcon from './../../../styleSheet/svg/twitterIcon.svg';
import HrLine from '../../commonComponent/HrLine';

const title ="You may also like"
const twitter="https://twitter.com/imVkohli";
const facebook ="https://twitter.com/imVkohli?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor";
const insta ="https://twitter.com/imVkohli?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor";

export default class PlayerSocial extends Component {
    constructor(props) {
        super(props);
        this.state = {
            twitter:'',
            facebook:'',
            inst:''
        };
        this._handelFacebook= this._handelFacebook.bind(this);
        this._handelInsta = this._handelInsta.bind(this);
        this._handelTwitter = this._handelTwitter.bind(this);
    }
    componentDidMount() {
        this.setState({
            twitter,
            facebook,
            insta
        })

    }
    _handelFacebook(){
        const fbLink = this.state.facebook;
        window.open(fbLink, "_blank");
    }
    _handelInsta(){
        const insta = this.state.insta;
        window.open(twitter, "_blank");
    }
    _handelTwitter(){
        const twitter = this.state.twitter
        window.open(twitter, "_blank");
    }
    render() {
        return (
            <div style={pageStyle.container}>
                <div style={{ display: 'flex', flexDirection: 'row', flex: 1, marginBottom:'12px' }}>
                    <div style={pageStyle.socialHeading}>
                        {title}
                    </div>
                    <div style={pageStyle.socialMenu} onClick={this._handelFacebook}>
                        <img src={facebookIcon} width='24px' height='24px' />
                    </div>
                    <div style={pageStyle.socialMenu} onClick={this._handelInsta}>
                        <img src={instaIcon} width='24px' height='24px' />
                    </div>
                    <div style={pageStyle.socialMenu} onClick={this._handelTwitter}>
                        <img src={twitterIcon} width='24px' height='24px' />
                    </div>

                </div>
                <HrLine />
                <div style={pageStyle.socialView}>
                    Data from social
                    {/* <a href="https://twitter.com/TwitterDev?ref_src=twsrc%5Etfw" className="twitter-follow-button" data-show-count="false">Follow @TwitterDev</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> */}

                    {/* <iframe src="https://twitter.com/TwitterDev?ref_src=twsrc%5Etfw" /> */}
                </div>

            </div>
        );
    }

}

const pageStyle = {
    container: {
        // marginTop:'12px',
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        padding: '16px',
        // width:screenWidth*0.911,
        // marginLeft:screenWidth*0.044,
        backgroundColor:'#fff',
        fontFamily: 'Montserrat',

    },
    socialHeading: {
        // padding:'12px',
        display: 'flex',
        flex: 0.80,
        justifyContent: 'flex-start',
        color: '#141b2f',
        fontSize: '12px',
        fontWeight: '600',
    },
    socialMenu: {
        // paddingTop:'10px',
        display: 'flex',
        flex: 0.20,
        justifyContent: 'center',
        width: screenWidth * 0.18,
    },
    socialView: {
        margin:'12px',
        display: 'flex',
        flex: 1,
        // width:screenWidth*0.9,
        // height:screenHeight *0.51,
    },

}