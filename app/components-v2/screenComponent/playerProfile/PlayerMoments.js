import React from "react";
import HrLine from "../../commonComponent/HrLine";
import { white } from "../../../styleSheet/globalStyle/color";
import EmptyState from "../../commonComponent/EmptyState";

class PlayerMoments extends React.Component {
  render() {
    const { imageData } = this.props;
    return (
      <div style={pageStyle.container}>
        <div style={pageStyle.header}>Player Moments</div>
        <HrLine />
        {imageData && imageData.length > 0 ? (
          <React.Fragment>
            <div
              style={{
                backgroundImage: `linear-gradient(to right, rgba(166, 59, 161, 0.3), rgba(250, 148, 65, 0)), url(${
                  imageData[0].path
                })`,
                minHeight: "150px",
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
                backgroundPosition: "top center",
                // backgroundBlendMode: "lighten",
                marginTop: "16px"
              }}
              onClick={() => this.props.handleImageClick(0)}
            />
            {imageData.length > 1 && (
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  marginTop: "8px"
                }}
              >
                <div
                  style={{
                    ...pageStyle.smallImg,
                    backgroundImage: `linear-gradient(to right, rgba(40, 174, 244, 0.5), rgba(250, 148, 65, 0)), url(${
                      imageData[1].path
                    })`
                  }}
                  onClick={() => this.props.handleImageClick(1)}
                />
                {imageData.length > 2 && (
                  <div
                    style={{
                      ...pageStyle.smallImg,
                      backgroundImage: `linear-gradient(to right, rgba(69, 149, 72, 0.5), rgba(250, 148, 65, 0)), url(${
                        imageData[2].path
                      })`
                    }}
                    onClick={() => this.props.handleImageClick(2)}
                  />
                )}
                {imageData.length > 3 && (
                  <div
                    style={{
                      ...pageStyle.smallImg,
                      backgroundImage: `linear-gradient(to right, rgba(20, 27, 47, 0.5), rgba(20, 27, 47, 0.5)), url(${
                        imageData[3].path
                      })`,
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      fontSize: "24px",
                      color: white
                    }}
                    onClick={() => this.props.handleImageClick(3)}
                  >
                    {imageData.length - 4 === 0
                      ? ""
                      : `+${imageData.length - 4}`}
                  </div>
                )}
              </div>
            )}
          </React.Fragment>
        ) : (
          <EmptyState />
        )}
      </div>
    );
  }
}

const pageStyle = {
  container: {
    // marginTop:'12px',
    display: "flex",
    flexDirection: "column",
    flex: 1,
    padding: "16px",
    fontSize: "12px",
    // width:screenWidth*0.911,
    // marginLeft:screenWidth*0.044,
    backgroundColor: "#fff",
    fontFamily: "Montserrat"
  },
  header: {
    display: "flex",
    fontWeight: "500",
    paddingBottom: "16px"
  },
  smallImg: {
    minHeight: "100px",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
    // backgroundBlendMode: "lighten",
    width: "32%"
  }
};

export default PlayerMoments;
