import React, { Component } from "react";
import Header from "./../../commonComponent/Header";
import ImageContainer from "./../../commonComponent/ImageContainerPlayers";
import PlayerDiscoveryImage from "./../../../styleSheet/images/PlayerDiscovery.jpg";
import { Helmet } from "react-helmet";
import {
  gradientRedOrangeLeft,
  white,
  gradientIPL,
  gradientNonIpl
} from "../../../styleSheet/globalStyle/color";
import EmptyState from "../../commonComponent/EmptyState";

export default class PlayerDiscovery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      isHeaderBackground: true,
      titlePlaced: false
    };
    this.handleScroll = this.handleScroll.bind(this);
  }
  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = event => {
    const scrollTop = event.target.scrollingElement.scrollTop;
    // console.log("object: ", scrollTop);
    if (scrollTop > 26 && !this.state.titlePlaced) {
      this.setState({
        // title: "Records",
        title: "Players",
        titlePlaced: true,
        isHeaderBackground: true
      });
    } else if (scrollTop < 27) {
      this.setState({
        title: "",
        titlePlaced: false,
        isHeaderBackground: true
      });
    }
  };
  render() {
    // console.log(this.props.playerDiscoveryData);
    const { playerDiscoveryData, history, loading } = this.props;

    return (
      <div style={pageStyle.conatiner}>
        <Helmet titleTemplate="%s | crictec.com">
          <title>
            Cricket Players Data | Check out the Complete Bio of Cricket Players
          </title>
          <meta
            name="description"
            content="Top cricket players bio, match details, batting, bowling statistics of their test, one day internationals or IPL matches. Check out the complete cricket data of the players"
          />
          <meta
            name="keywords"
            content="popular cricket players, indian cricket players, ipl player list, cricket players, ipl team players"
          />
          <link
            rel="canonical"
            href={`www.cricket.com${history &&
              history.location &&
              history.location.pathname}`}
          />
        </Helmet>
        {/* <TopHeader title = "Players" history={this.props.history}/> */}
        <Header
          title={"Players"}
          isHeaderBackground={true}
          leftArrowBack
          textColor={white}
          backgroundColor={gradientRedOrangeLeft}
          history={history}
          leftIconOnClick={() => {
            history.push("/more");
          }}
        />
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh"
            }}
          >
            <img
              src={require("../../../images/CricketLoader.gif")}
              // style={{ margin: "40% auto" }}
            />
          </div>
        ) : playerDiscoveryData && playerDiscoveryData.length > 0 ? (
          <React.Fragment>
            <div
              style={{
                fontFamily: "Montserrat",
                fontWeight: 600,
                fontSize: 22,
                color: white,
                marginBottom: 12,
                minHeight: "100px",
                paddingLeft: "16px",
                textTransform: "capitalize",
                background: gradientRedOrangeLeft
              }}
            />
            <div style={{ marginTop: "-80px" }}>
              {playerDiscoveryData && playerDiscoveryData.length > 0 ? (
                playerDiscoveryData.map((player, index) => (
                  <div style={pageStyle.otherNew} key={player.type}>
                    <ImageContainer
                      isAvatar={
                        player.type.toLowerCase().indexOf("popular") > -1
                          ? true
                          : false
                      }
                      history={history}
                      data={player.players}
                      rootStyles={{ marginTop: 10 }}
                      title={player.type}
                      gradient={index % 2 === 0 ? gradientIPL : gradientNonIpl}
                    />
                  </div>
                ))
              ) : (
                <EmptyState />
              )}
            </div>
          </React.Fragment>
        ) : (
          <EmptyState msg="No Players Found" />
        )}
      </div>
    );
  }
}

const pageStyle = {
  conatiner: {
    padding: "42px 0px",
    minHeight: "120vh"
  },
  TopHeader: {
    display: "flex"
  },
  avatar: {
    display: "flex",
    flexDirection: "row"
  },
  other: {
    // display: "flex",
    // overflow: "scroll",
    // width: screenWidth * 0.97
    maxWidth: "95%"
  },
  otherNew: {
    maxWidth: "95%"
    // display: "flex",
    // overflow: "scroll",
    // width: screenWidth * 0.97
    // marginTop: -screenHeight * 0.15
  }
};

const popular = {
  header: "Popular Stadiums",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    },
    {
      name: "MCG, Sydney",
      img: PlayerDiscoveryImage
    },
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    },
    {
      name: "MCG, Sydney",
      img: PlayerDiscoveryImage
    },
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    }
  ]
};

const trending = {
  header: "Trending Player",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    },
    {
      name: "MCG, Sydney",
      img: PlayerDiscoveryImage
    },
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    },
    {
      name: "MCG, Sydney",
      img: PlayerDiscoveryImage
    },
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    }
  ]
};
const similarGrounds = {
  header: "Player Similar to Eden Gardens",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    },
    {
      name: "MCG, Sydney",
      img: PlayerDiscoveryImage
    },
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    },
    {
      name: "MCG, Sydney",
      img: PlayerDiscoveryImage
    },
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    }
  ]
};

const mostMatches = {
  header: "Player with Most Matches",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    },
    {
      name: "MCG, Sydney",
      img: PlayerDiscoveryImage
    },
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    },
    {
      name: "MCG, Sydney",
      img: PlayerDiscoveryImage
    },
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    }
  ]
};

const highestBatting = {
  header: "Player with Highest Batting S/R",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    },
    {
      name: "MCG, Sydney",
      img: PlayerDiscoveryImage
    },
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    },
    {
      name: "MCG, Sydney",
      img: PlayerDiscoveryImage
    },
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    }
  ]
};

const highestBowling = {
  header: "Player with Highest Bowling S/R",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    },
    {
      name: "MCG, Sydney",
      img: PlayerDiscoveryImage
    },
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    },
    {
      name: "MCG, Sydney",
      img: PlayerDiscoveryImage
    },
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    }
  ]
};

const favouriteStatium = {
  header: "Virat favourite Stadiums",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    },
    {
      name: "MCG, Sydney",
      img: PlayerDiscoveryImage
    },
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    },
    {
      name: "MCG, Sydney",
      img: PlayerDiscoveryImage
    },
    {
      name: "Eden Gardens, Kolkata",
      img: PlayerDiscoveryImage
    }
  ]
};
