import React, { Component } from "react";
import PropTypes from "prop-types";
import imagePlayer from "./../../../styleSheet/images/imagePlayer.jpg";
import defaultPlayer from "../../../images/fallbackProjection.png";
import moment from "moment";
import { screenWidth } from "../../../styleSheet/screenSize/ScreenDetails";
import { white, grey_8, orange_1 } from "../../../styleSheet/globalStyle/color";

export default class PlayerDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "about player"
    };
  }
  componentDidMount = () => {};

  handleActiveTab = activeTab => {
    this.setState({
      activeTab
    });
  };

  handlePlayerAge = personalDetails => {
    if (personalDetails && personalDetails.dob) {
      const dob = new Date(personalDetails.dob);
      const ageDifMs = Date.now() - dob.getTime();
      const ageDate = new Date(ageDifMs); // miliseconds from epoch
      const result = `${Math.abs(ageDate.getUTCFullYear() - 1970)} Years`;
      return result;
    }
    return "-";
  };

  render() {
    const { playerDetails, history, fetchPlayerNews, players } = this.props;
    // console.log(playerDetails, "player #####", history, players);
    // console.log("dadaL : ", this.props);
    if (playerDetails != null) {
      return (
        <div
          style={{
            display: "flex",
            background: white,
            // width: "90%",
            // margin: "0 auto",
            minHeight: "340px",
            justifyContent: "space-between"
          }}
        >
          <div
            style={{
              width: "60%",
              height: playerDetails.avatar ? "unset" : 340
            }}
          >
            <img
              style={{ height: "95%", marginTop: "10%" }}
              src={playerDetails.avatar ? playerDetails.avatar : defaultPlayer}
            />
          </div>
          <div
            style={{
              //   marginTop: "10%",
              fontFamily: "Montserrat",
              fontSize: "12px",
              padding: "10% 0%",
              width: "40%",
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-around"
            }}
          >
            <div style={{ fontFamily: "oswald500" }}>
              <p
                style={{
                  fontSize: "24px",
                  // fontWeight: "600",
                  fontFamily: "oswald300",
                  lineHeight: "24px"
                }}
              >
                {playerDetails.displayName.split(" ")[0]}
              </p>
              <p
                style={{
                  fontSize: "24px",
                  // fontWeight: "600",
                  lineHeight: "24px",
                  marginTop: 4
                }}
              >
                {playerDetails.displayName.split(" ").length > 2
                  ? `${playerDetails.displayName.split(" ")[1]} ${
                      playerDetails.displayName.split(" ")[2]
                    }`
                  : playerDetails.displayName.split(" ")[1]}
              </p>
              {/* <p style={{ color: grey_6, fontSize: "10px", marginTop: "8px" }}>
                {playerDetails.personalDetails.role
                  ? playerDetails.personalDetails.role
                  : "--"}
                |{" "}
                {playerDetails.nationality
                  ? playerDetails.nationality
                  : "--"}
              </p> */}
            </div>
            <div>
              <p style={{ color: orange_1, fontWeight: "600" }}>Born</p>
              <p style={{ fontWeight: "500" }}>
                {playerDetails.personalDetails &&
                  moment(playerDetails.personalDetails.dob).format(
                    "DD MMM YYYY"
                  )}
              </p>
              <p style={{ color: grey_8 }}>
                {playerDetails.personalDetails &&
                  `(${moment().diff(
                    playerDetails.personalDetails.dob,
                    "years"
                  )} years)`}
              </p>
            </div>
            <div>
              <p style={{ color: orange_1, fontWeight: "600" }}>
                Batting Style
              </p>
              <p style={{ fontWeight: "500" }}>
                {playerDetails.personalDetails &&
                  playerDetails.personalDetails.battingStyle}
              </p>
            </div>
            <div>
              <p style={{ color: orange_1, fontWeight: "600" }}>
                Bowling Style
              </p>
              <p style={{ fontWeight: "500" }}>
                {playerDetails.personalDetails &&
                  playerDetails.personalDetails.bowlingStyle}
              </p>
            </div>
          </div>
          {/* {this.playerPreview()} */}
        </div>
      );
    } else {
      return null;
    }
  }

  playerPreview() {
    const { playerDetails } = this.props;
    const image = playerDetails; // image not avilable
    return (
      <div style={pageStyle.playerPreviewContainer}>
        {/* <div style={{ display: 'flex', width: screenWidth * 0.05 }}>
					{}
				</div> */}
        <div style={pageStyle.playerPreviewStyle}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              flex: 1,
              padding: "9px 9px 0px 0px"
            }}
          >
            <div style={{ display: "flex", flex: 0.5 }}>
              <img src={imagePlayer} width={screenWidth * 0.45} />
            </div>
            <div style={{ display: "flex", flex: 0.5, marginLeft: "5px" }}>
              {this.playerDetails()}
            </div>
          </div>
        </div>
        {/* <div style={{ display: 'flex', width: screenWidth * 0.05 }}>
					{}
				</div> */}
      </div>
    );
  }
  playerDetails() {
    const { playerDetails } = this.props;
    const firstName = playerDetails.displayName.substr(
      0,
      playerDetails.displayName.indexOf(" ")
    );
    const lastName = playerDetails.displayName.substr(
      playerDetails.displayName.indexOf(" ")
    );
    const role =
      playerDetails.personalDetails && playerDetails.personalDetails.role;
    const country = playerDetails.teams[0];
    const battingStyle =
      playerDetails.personalDetails &&
      playerDetails.personalDetails.battingStyle;
    const bowlingStyle =
      playerDetails.personalDetails &&
      playerDetails.personalDetails.bowlingStyle;
    const dob =
      playerDetails.personalDetails && playerDetails.personalDetails.dob;

    return (
      <div style={{ display: "flex", flexDirection: "column", flex: 1 }}>
        <div style={{ display: "flex", flex: 0.3, flexDirection: "column" }}>
          <div
            style={{
              display: "flex",
              flex: 0.4,
              fontSize: "24px",
              color: "#141b2f",
              fontWeight: "500"
            }}
          >
            {/* {firstName.toUpperCase()} */}
          </div>
          <div
            style={{
              display: "flex",
              flex: 0.4,
              fontSize: "24px",
              color: "#141b2f",
              fontWeight: "700"
            }}
          >
            {/* {lastName.toUpperCase()} */}
          </div>
          <div
            style={{
              display: "flex",
              flex: 0.2,
              fontColor: "#141b2f",
              fontSize: "10px"
            }}
          >
            {/* {role.toUpperCase()+' | '+country.toUpperCase()} */}
          </div>
        </div>

        <div style={{ display: "flex", flex: 0.02 }}>{}</div>

        <div style={{ display: "flex", flex: 0.15, flexDirection: "column" }}>
          <div
            style={{
              display: "flex",
              flex: 0.4,
              fontSize: "12px",
              color: "#f76b1c",
              fontWeight: "700"
            }}
          >
            Born
          </div>
          <div
            style={{
              display: "flex",
              flex: 0.4,
              fontSize: "12px",
              color: "#141b2f"
            }}
          >
            {dob}
          </div>
          <div
            style={{
              display: "flex",
              flex: 0.2,
              fontColor: "#a1a4ac",
              fontSize: "10px"
            }}
          >
            (31 Years)
          </div>
        </div>

        <div style={{ display: "flex", flex: 0.04 }}>{}</div>

        <div style={{ display: "flex", flex: 0.15, flexDirection: "column" }}>
          <div
            style={{
              display: "flex",
              flex: 0.4,
              fontSize: "12px",
              color: "#f76b1c",
              fontWeight: "700"
            }}
          >
            Batting Style
          </div>
          <div
            style={{
              display: "flex",
              flex: 0.4,
              fontSize: "12px",
              color: "#141b2f"
            }}
          >
            {battingStyle.toUpperCase()}
          </div>
        </div>

        <div style={{ display: "flex", flex: 0.04 }}>{}</div>

        <div style={{ display: "flex", flex: 0.15, flexDirection: "column" }}>
          <div
            style={{
              display: "flex",
              flex: 0.4,
              fontSize: "12px",
              color: "#f76b1c",
              fontWeight: "700"
            }}
          >
            Bowling Style
          </div>
          <div
            style={{
              display: "flex",
              flex: 0.4,
              fontSize: "12px",
              color: "#141b2f"
            }}
          >
            {bowlingStyle.toUpperCase()}
          </div>
        </div>
      </div>
    );
  }
}
const pageStyle = {
  container: {
    display: "flex"
    // width: screenWidth*0.911,
    // height: screenWidth * 0.71,
  },
  playerPreviewStyle: {
    display: "flex",
    // width: screenWidth * 0.911,
    justifyContent: "center",
    backgroundColor: "#fff",
    boxShadow: "0px 3px 6px 0px #13000000"
  },
  playerPreviewContainer: {
    display: "flex",
    marginTop: "0px",
    flexDirection: "row"
  }
};

PlayerDetails.propTypes = {
  playerDetails: PropTypes.object,
  history: PropTypes.object,
  fetchPlayerDetails: PropTypes.func,
  playerId: PropTypes.string,
  fetchPlayerNews: PropTypes.func,
  players: PropTypes.object
};
