import React, { Component } from "react";
// import BackgroundComponent from "./../../commonComponent/BackgroundComponent";
import Header from "./../../commonComponent/Header";
import Modal from "react-responsive-modal";
import HrLine from "components-v2/commonComponent/HrLine";
import { Helmet } from "react-helmet";
import {
  screenWidth,
  screenHeight
} from "../../../styleSheet/screenSize/ScreenDetails";
import {
  gradientRedOrangeLeft,
  white,
  grey_8,
  gradientGrey,
  cardShadow,
  grey_4
} from "../../../styleSheet/globalStyle/color";
import PlayerNews from "../../commonComponent/NewsComponent";
import PlayerDetails from "./PlayerDetails";
import PlayerBiodata from "./PlayerBiodata";
import PlayerMoments from "./PlayerMoments";
import PlayerStats from "./playerStats";
import ImageScrollerCArd from "../../commonComponent/ImageScollerCard";
import EmptyState from "../../commonComponent/EmptyState";
import PhotoView from "./PhotoView";
import CardGradientTitle from "../../commonComponent/CardGradientTitle";
import FeatureArticleCard from "../../Articles/FeatureArticleCard";

export default class PlayerProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      isHeaderBackground: true,
      activeTab: "International",
      activeButton: "TEST",
      isBatting: true,
      titlePlaced: false,
      isTrophyModal: false,
      showPhoto: false,
      initialSlide: 0
    };
    this.handleScroll = this.handleScroll.bind(this);
    // console.log("###Coming here for sure", this.props);
  }
  //   componentDidMount() {}
  //   handleScroll(e) {
  //     const bottom =
  //       e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight;
  //     console.log(bottom);
  //   }
  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = event => {
    const scrollTop = event.target.scrollingElement.scrollTop;
    // console.log("object: ", scrollTop);
    if (scrollTop > 26 && !this.state.titlePlaced) {
      this.setState({
        // title: "Records",
        title: this.props.playerDetails.name,
        titlePlaced: true,
        isHeaderBackground: true
      });
    } else if (scrollTop < 27) {
      this.setState({
        title: "",
        titlePlaced: false,
        isHeaderBackground: true
      });
    }
  };

  handleActiveButton = activeButton => {
    // console.log(activeButton);
    this.setState({
      activeButton
    });
  };

  handleActiveTab = activeTab => {
    this.setState({
      activeTab
    });
  };
  handleToggle = type => {
    // console.log("toggle button clilcked", type);
    if (type === "bat") {
      this.setState({
        isBatting: true
      });
    } else {
      this.setState({
        isBatting: false
      });
    }
  };

  openTrophyModal = () => {
    this.setState({
      isTrophyModal: true
    });
  };

  _handleCloseModal = () => {
    this.setState({
      isTrophyModal: false
    });
  };

  handleImageClick = val => {
    // console.log(val);
    this.setState({
      showPhoto: val
    });
  };

  handleShowPhoto = key => {
    console.log(key);
    this.setState({
      showPhoto: true,
      initialSlide: key
    });
  };
  render() {
    const { history } = this.props;
    const {
      playerDetails,
      featuredPlayers,
      playerNews,
      playerMoments
    } = this.props;

    // console.log(playerMoments, "---------sending in this way---------");
    // console.log("playerDetails", playerDetails, history);
    // const playerNews = playerDetails.playerNews;
    // console.log(history,"hello");
    // if (playerDetails != null) {
    return this.state.showPhoto ? (
      <PhotoView
        toggleView={val => this.handleImageClick(val)}
        title="player moments"
        initialSlide={this.state.initialSlide}
        data={playerMoments}
      />
    ) : (
      <React.Fragment>
        <Helmet titleTemplate="%s | cricket.com">
          <title>
            {`${playerDetails &&
              playerDetails.displayName} | Complete Bio and Match Statistics of ${playerDetails &&
              playerDetails.displayName}`}
          </title>
          <meta
            name="description"
            content={`${playerDetails &&
              playerDetails.displayName} complete bio, match details, batting, bowling statistics of his test, one day internationals or IPL matches. Check out the complete cricket history of ${playerDetails &&
              playerDetails.displayName}`}
          />
          <meta
            name="keywords"
            content={`${playerDetails &&
              playerDetails.displayName}, ${playerDetails &&
              playerDetails.displayName} age, ${playerDetails &&
              playerDetails.displayName} photos, ${playerDetails &&
              playerDetails.displayName} biography, ${playerDetails &&
              playerDetails.displayName} cricketer`}
          />
          <link
            rel="canonical"
            href={`www.cricket.com${history &&
              history.location &&
              history.location.pathname}`}
          />
        </Helmet>
        <div
          style={{
            padding: "42px 0px"
            // minHeight: "120vh"
          }}
        >
          <Header
            title={"Player Profile"}
            isHeaderBackground={true}
            leftArrowBack
            textColor={white}
            backgroundColor={gradientRedOrangeLeft}
            history={history}
            leftIconOnClick={() => {
              history.goBack();
            }}
          />
          <div
            style={{
              fontFamily: "Montserrat",
              fontWeight: 600,
              fontSize: 22,
              color: white,
              marginBottom: 12,
              minHeight: "90px",
              paddingLeft: "16px",
              textTransform: "capitalize",
              background: gradientRedOrangeLeft
            }}
          >
            {/* {title} */}
            {/* player profile */}
          </div>
          <div style={{ width: "90%", margin: "0 auto", marginTop: "-80px" }}>
            <div>
              <PlayerDetails playerDetails={playerDetails} />
              <div
                style={{
                  fontSize: "14px",
                  fontWeight: "600",
                  fontFamily: "Montserrat",
                  width: "50%",
                  background: gradientRedOrangeLeft,
                  color: white,
                  minHeight: "45px",
                  display: "flex",
                  justifyContent: "space-around",
                  alignItems: "center",
                  borderRadius: "4px",
                  position: "absolute",
                  marginLeft: "20%",
                  marginTop: "-6%"
                }}
                onClick={this.openTrophyModal}
              >
                <div
                  style={{
                    height: "45px",
                    display: "flex",
                    paddingRight: "5%",
                    borderRight: "0.5px solid rgba(255,255,255,0.39)"
                  }}
                >
                  <img src={require("../../../images/shape-copy.svg")} />
                </div>
                <div>Trophy Cabinet</div>
              </div>
            </div>
            <div>
              <PlayerStats
                activeTab={this.state.activeTab}
                activeButton={this.state.activeButton}
                handleActiveTab={this.handleActiveTab}
                handleActiveButton={this.handleActiveButton}
                handleToggle={this.handleToggle}
                isBatting={this.state.isBatting}
                playerData={playerDetails}
              />
            </div>
            <div style={pageStyle.PlayerBiodata}>
              <PlayerBiodata playerDetails={playerDetails} history={history} />
            </div>
            {/* <div style={{ marginTop: "12px" }}>
              <PlayerCompare />
            </div> */}
            {playerNews &&
              playerNews.length > 0 && (
                <div style={{ marginTop: "12px" }}>
                  {/* <PlayerNews playerNews={playerNews} history={history} /> */}
                  <div
                    style={{
                      background: white,
                      borderRadius: 3,
                      boxShadow: cardShadow,
                      marginBottom: 10
                    }}
                  >
                    <CardGradientTitle
                      title="You may also like"
                      rootStyles={{
                        background: white
                      }}
                    />
                    <div
                      style={{
                        // display: "flex",
                        // flexDirection: "row",
                        // flex: 1,
                        overflowX: "scroll",
                        overflowY: "hidden",
                        whiteSpace: "nowrap",
                        paddingLeft: "12px"
                      }}
                    >
                      {playerNews.map((item, itemKey) => (
                        <div
                          style={{
                            width: playerNews.length == 1 ? "96.5%" : "80%",
                            display: "inline-block",
                            overflow: "hidden",
                            marginRight: 12
                          }}
                          key={`${itemKey + 1}`}
                        >
                          <FeatureArticleCard
                            article={item}
                            // articleHost={}
                            cardOnClick={path => this.props.history.push(path)}
                            cardType={item.type}
                            rootStyles={{
                              padding: "16px 0px",
                              borderRadius: 3
                            }}
                            contentStyles={{
                              display: "flex",
                              flexDirection: "column",
                              justifyContent: "space-between",
                              padding: "0 12px 6px",
                              whiteSpace: "initial",
                              minHeight: 142,
                              background: grey_4
                            }}
                          />
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              )}
            {/* <div style={{ marginTop: "12px" }}>
            <PlayerSocial />
          </div> */}
            {playerMoments &&
              playerMoments.length > 0 && (
                <div style={{ marginTop: "12px" }}>
                  <PlayerMoments
                    // handleImageClick={() => this.props.history.push("/photoview")}
                    handleImageClick={this.handleShowPhoto}
                    imageData={playerMoments}
                  />
                </div>
              )}
            {playerDetails &&
              playerDetails.similarPlayers &&
              playerDetails.similarPlayers.length > 0 && (
                <div style={{ marginTop: "12px" }}>
                  <ImageScrollerCArd
                    players={
                      playerDetails &&
                      playerDetails.similarPlayers &&
                      playerDetails.similarPlayers.length > 0 &&
                      playerDetails
                    }
                    isNotFeatured
                    history={history}
                    title="View Similar Players"
                  />
                </div>
              )}
          </div>
        </div>
        <Modal
          open={this.state.isTrophyModal}
          //   closeIconSize={"20"}
          showCloseIcon={true}
          onClose={this._handleCloseModal}
          center
          styles={{ width: "500px !important" }}
        >
          <div
            style={{
              fontFamily: "Montserrat",
              fontSize: "12px",
              width: "80vw"
            }}
          >
            <div style={pageStyle.title}>Trophy Cabinet</div>
            <div style={pageStyle.trophyList}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  flex: 1
                }}
              >
                {playerDetails &&
                playerDetails.trophyCabinets &&
                playerDetails.trophyCabinets.length > 0 ? (
                  playerDetails.trophyCabinets.map(trophy => (
                    <React.Fragment>
                      <div style={pageStyle.trophyListRow}>
                        <div style={pageStyle.trophy}>
                          <img
                            src={
                              trophy.avatar ||
                              require("../../../images/group-17.svg")
                            }
                            alt="trophy image"
                            height="41px"
                            width="41px"
                          />
                        </div>
                        <div style={pageStyle.trophyText}>
                          <div style={pageStyle.trophyTextName}>
                            {trophy.name}
                          </div>
                          <div style={pageStyle.trophyTextYear}>
                            {trophy.year}
                          </div>
                        </div>
                      </div>
                      <HrLine style={{ margin: "4px 0px" }} />
                    </React.Fragment>
                  ))
                ) : (
                  <EmptyState msg="No Trophies Found" />
                )}
              </div>
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
    // } else {
    //   return null;
    // }
  }
  componentDidCatch() {
    console.log("error occured in playerprofile component");
  }
}

const pageStyle = {
  conatiner: {
    display: "flex",
    flexDirection: "column",
    flex: 1
  },
  trophyList: {
    display: "flex",
    flex: 1
  },
  trophyModalStyle: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    width: screenWidth * 0.911,
    backgroundColor: "#fff"
  },
  title: {
    display: "flex",
    flex: 1,
    background: gradientGrey,
    alignItems: "center",
    justifyContent: "flex-start",
    color: "#141b2f",
    // height: screenHeight * 0.0609,
    marginTop: "-15px",
    fontSize: "12px",
    padding: "12px"
  },
  TopHeaderPlayer: {
    display: "flex",
    height: screenHeight * 0.2625
  },
  NewsPlayerProfile: {
    display: "flex",
    marginTop: "12px"
  },
  PlayerDetails: {
    display: "flex",
    marginTop: -screenHeight * 0.11
  },
  PlayerBiodata: {
    display: "flex",
    marginTop: "12px"
  },
  modalContainer: {
    display: "flex",
    flex: 1,
    zIndex: "9999",
    position: "absolute"
  },
  trophyCabinetContainer: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    width: screenWidth * 0.911,
    backgroundColor: "#fff"
  },
  trophyListContainer: {
    display: "flex",
    flexDirection: "column",
    flex: 1,
    width: screenWidth * 0.8
  },
  trophyListRow: {
    display: "flex",
    flexDirection: "row",
    flex: 1,
    alignItems: "center"
    // width: screenWidth * 0.8,
    // height: screenHeight * 0.123
  },
  tophyField: {
    display: "flex",
    flex: 1,
    flexDirection: "row",
    // height: screenHeight * 0.1218,
    // width: screenWidth * 0.911,
    backgroundColor: "#fff"
  },
  trophy: {
    display: "flex",
    flex: 0.1,
    alignItems: "center",
    justifyContent: "flex-start",
    border: "1px solid #fa9441",
    borderRadius: "50%",
    padding: "6px"
  },
  trophyText: {
    display: "flex",
    alignItems: "flex-start",
    flexDirection: "column",
    paddingLeft: "16px"
  },
  trophyTextName: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    color: grey_8,
    fontSize: "12px"
  },
  trophyTextYear: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    color: "#141b2f",
    fontSize: "12px",
    letterSpacing: "0.33px",
    fontWeight: "600"
  }
};
