import React, { Component } from "react";
import {
  white,
  grey_10,
  orange_1,
  grey_8,
  gradientGrey,
  grey_6
} from "../../../styleSheet/globalStyle/color";
import ButtonBar from "components-v2/commonComponent/ButtonBar";
import TabBar from "components-v2/commonComponent/TabBar";
import HrLine from "components-v2/commonComponent/HrLine";
import moment from "moment";

export default class PlayerStats extends Component {
  getTeamName = tName => {
    // console.log(tName);
    const lower = tName.toLowerCase().replace(/ /g, "");
    // console.log(lower);
    switch (lower) {
      case "india":
        return "IND";
      case "westindies":
        return "WI";
      case "zimbabwe":
        return "ZIM";
      case "pakistan":
        return "PAK";
      case "australia":
        return "AUS";
      case "srilanka":
        return "SL";
      case "bangladesh":
        return "BAN";
      case "england":
        return "ENG";
      case "southafrica":
        return "RSA";
      case "newzealand":
        return "NZ";
      case "ireland":
        return "IRE";
      case "afghanistan":
        return "AFG";
      case "unitedarabemirates":
        return "UAE";
      default:
        return tName;
    }
  };

  getShortName = name => {
    // console.log(name);
    if (name && name.toLowerCase().indexOf("vs") > -1) {
      const splitName = name.toLowerCase().split("vs ");
      return `${this.getTeamName(splitName[0])} v ${this.getTeamName(
        splitName[1]
      )}`;
    }
    // if (name && name.indexOf("Vs") > -1) {
    //   const splitName = name.split("Vs ");
    //   return `${this.getTeamName(splitName[0])} v ${this.getTeamName(
    //     splitName[1]
    //   )}`;
    // }
    // if (name && name.indexOf("VS") > -1) {
    //   const splitName = name.split("VS ");
    //   return `${this.getTeamName(splitName[0])} v ${this.getTeamName(
    //     splitName[1]
    //   )}`;
    // }
  };
  render() {
    const { playerData, activeButton, isBatting } = this.props;
    // console.log(playerData);
    return (
      <div
        style={{
          background: white,
          padding: "10px 0 0",
          borderRadius: 3,
          marginTop: "9%"
          // boxShadow: cardShadow
        }}
      >
        <TabBar
          items={["International", "IPL"]}
          activeItem={this.props.activeTab}
          onClick={this.props.handleActiveTab}
        />

        <div
          style={{
            marginTop: 12
          }}
        >
          {this.props.activeTab === "International" && (
            <ButtonBar
              items={["TEST", "ODI", "T20I"]}
              activeItem={this.props.activeButton}
              onClick={this.props.handleActiveButton}
              // onClick={this.handleMatchSelect}
            />
          )}
          <div
            style={{
              padding: "16px",
              fontFamily: "Montserrat",
              fontWeight: "400",
              fontSize: "12px"
            }}
          >
            <div
              style={{
                display: "flex",
                fontSize: "12px",
                marginBottom: "16px"
              }}
            >
              <div style={{ width: "40%", fontWeight: "600" }}>
                <p style={{ color: orange_1 }}>Debut</p>
                <p style={{ textTransform: "uppercase" }}>
                  {this.props.activeTab === "International"
                    ? playerData.debutMatch &&
                      playerData.debutMatch[
                        activeButton === "T20I" ? "T20" : activeButton
                      ] &&
                      playerData.debutMatch[
                        activeButton === "T20I" ? "T20" : activeButton
                      ].title
                      ? this.getShortName(
                          playerData.debutMatch[
                            activeButton === "T20I" ? "T20" : activeButton
                          ].title
                        )
                      : "--"
                    : playerData.debutMatch && playerData.debutMatch["IPL"]
                      ? playerData.debutMatch["IPL"].title
                      : "--"}
                </p>
              </div>
              <div style={{ color: grey_10 }}>
                <p>
                  {this.props.activeTab === "IPL"
                    ? playerData.debutMatch &&
                      playerData.debutMatch["IPL"] &&
                      playerData.debutMatch["IPL"].title
                      ? moment(playerData.debutMatch["IPL"].date).format(
                          "DD MMM YYYY"
                        )
                      : ""
                    : playerData.debutMatch &&
                      playerData.debutMatch[
                        activeButton === "T20I" ? "T20" : activeButton
                      ] &&
                      playerData.debutMatch[
                        activeButton === "T20I" ? "T20" : activeButton
                      ].date
                      ? moment(
                          playerData.debutMatch[
                            activeButton === "T20I" ? "T20" : activeButton
                          ].date
                        ).format("DD MMM YYYY")
                      : ""}
                </p>
                <p>
                  {this.props.activeTab === "IPL"
                    ? playerData.debutMatch &&
                      playerData.debutMatch["IPL"] &&
                      playerData.debutMatch["IPL"].info
                    : (playerData.debutMatch &&
                        playerData.debutMatch[
                          activeButton === "T20I" ? "T20" : activeButton
                        ] &&
                        playerData.debutMatch[
                          activeButton === "T20I" ? "T20" : activeButton
                        ].info) ||
                      ""}
                </p>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                fontSize: "12px",
                marginBottom: "24px"
              }}
            >
              <div style={{ width: "40%", fontWeight: "600" }}>
                <p style={{ color: orange_1 }}>Recent Match</p>
                <p style={{ textTransform: "uppercase" }}>
                  {playerData.recentMatch && this.props.activeTab === "IPL"
                    ? (playerData.recentMatch["IPL"] &&
                        playerData.recentMatch["IPL"].title) ||
                      ""
                    : playerData.recentMatch &&
                      playerData.recentMatch[
                        activeButton === "T20I" ? "T20" : activeButton
                      ] &&
                      playerData.recentMatch[
                        activeButton === "T20I" ? "T20" : activeButton
                      ].title
                      ? this.getShortName(
                          playerData.recentMatch[
                            activeButton === "T20I" ? "T20" : activeButton
                          ].title
                        )
                      : "--"}
                </p>
              </div>
              <div style={{ color: grey_10 }}>
                <p>
                  {this.props.activeTab === "IPL"
                    ? (playerData.recentMatch &&
                        playerData.recentMatch["IPL"] &&
                        playerData.recentMatch["IPL"].date &&
                        moment(playerData.recentMatch["IPL"].date).format(
                          "DD MMM YYYY"
                        )) ||
                      ""
                    : (playerData.recentMatch &&
                        playerData.recentMatch[
                          activeButton === "T20I" ? "T20" : activeButton
                        ] &&
                        playerData.recentMatch[
                          activeButton === "T20I" ? "T20" : activeButton
                        ].date &&
                        moment(
                          playerData.recentMatch[
                            activeButton === "T20I" ? "T20" : activeButton
                          ].date
                        ).format("DD MMM YYYY")) ||
                      ""}
                </p>
                <p>
                  {playerData.recentMatch && this.props.activeTab === "IPL"
                    ? (playerData.recentMatch["IPL"] &&
                        playerData.recentMatch["IPL"].info) ||
                      ""
                    : (playerData.recentMatc &&
                        playerData.recentMatch[
                          activeButton === "T20I" ? "T20" : activeButton
                        ] &&
                        playerData.recentMatch[
                          activeButton === "T20I" ? "T20" : activeButton
                        ].info) ||
                      ""}
                </p>
              </div>
            </div>
            <HrLine />
            <div
              style={{
                display: "flex",
                padding: "8px",
                textAlign: "center",
                background: gradientGrey
              }}
            >
              <div
                style={{
                  width: "50%",
                  borderRight: `1px solid ${grey_6}`
                }}
              >
                <p style={{ color: grey_8 }}>Matches</p>
                <p style={{ fontSize: "26px", fontWeight: "600" }}>
                  {isBatting
                    ? this.props.activeTab === "International"
                      ? (playerData.batFieldStats &&
                          playerData.batFieldStats[
                            `${activeButton.toLowerCase()}`
                          ] &&
                          playerData.batFieldStats[
                            `${activeButton.toLowerCase()}`
                          ].matchesPlayed) ||
                        "0"
                      : (playerData.batFieldStats &&
                          playerData.batFieldStats["ipl"] &&
                          playerData.batFieldStats["ipl"].matchesPlayed) ||
                        "0"
                    : this.props.activeTab === "International"
                      ? (playerData.bowlStats &&
                          playerData.bowlStats[
                            `${activeButton.toLowerCase()}`
                          ] &&
                          playerData.bowlStats[`${activeButton.toLowerCase()}`]
                            .matchesPlayed) ||
                        "0"
                      : (playerData.bowlStats &&
                          playerData.bowlStats["ipl"] &&
                          playerData.bowlStats["ipl"].matchesPlayed) ||
                        "0"}
                </p>
              </div>
              <div style={{ width: "50%" }}>
                <p style={{ color: grey_8 }}>
                  {isBatting ? "Runs" : "Wickets"}
                </p>
                <p style={{ fontSize: "26px", fontWeight: "600" }}>
                  {isBatting
                    ? this.props.activeTab === "International"
                      ? (playerData.batFieldStats &&
                          playerData.batFieldStats[
                            `${activeButton.toLowerCase()}`
                          ] &&
                          playerData.batFieldStats[
                            `${activeButton.toLowerCase()}`
                          ].runs) ||
                        "0"
                      : (playerData.batFieldStats &&
                          playerData.batFieldStats["ipl"] &&
                          playerData.batFieldStats["ipl"].runs) ||
                        "0"
                    : this.props.activeTab === "International"
                      ? (playerData.bowlStats &&
                          playerData.bowlStats[
                            `${activeButton.toLowerCase()}`
                          ] &&
                          playerData.bowlStats[`${activeButton.toLowerCase()}`]
                            .wickets) ||
                        "0"
                      : (playerData.bowlStats &&
                          playerData.bowlStats["ipl"] &&
                          playerData.bowlStats["ipl"].wickets) ||
                        "0"}
                </p>
              </div>
            </div>
            <HrLine />
            <div
              style={{
                display: "flex",
                marginTop: "24px",
                //   justifyContent: "space-between",
                textAlign: "center"
              }}
            >
              <div style={{ width: "25%", position: "relative" }}>
                <p style={{ fontWeight: "500", fontSize: "14px" }}>
                  {isBatting
                    ? this.props.activeTab === "International"
                      ? (playerData.batFieldStats &&
                          playerData.batFieldStats[
                            `${activeButton.toLowerCase()}`
                          ] &&
                          playerData.batFieldStats[
                            `${activeButton.toLowerCase()}`
                          ].inningsPlayed) ||
                        "0"
                      : (playerData.batFieldStats &&
                          playerData.batFieldStats["ipl"] &&
                          playerData.batFieldStats["ipl"].inningsPlayed) ||
                        "0"
                    : this.props.activeTab === "International"
                      ? (playerData.bowlStats &&
                          playerData.bowlStats[
                            `${activeButton.toLowerCase()}`
                          ] &&
                          playerData.bowlStats[`${activeButton.toLowerCase()}`]
                            .inningsPlayed) ||
                        "0"
                      : (playerData.bowlStats &&
                          playerData.bowlStats["ipl"] &&
                          playerData.bowlStats["ipl"].inningsPlayed) ||
                        "0"}
                </p>
                <p>Innings</p>
                <span
                  style={{
                    width: "1px",
                    background: "#a1a4ac",
                    opacity: "0.5",
                    height: "80%",
                    display: "inline-block",
                    position: "absolute",
                    top: "10%",
                    right: "0"
                  }}
                />
              </div>
              <div style={{ width: "25%", position: "relative" }}>
                <p style={{ fontWeight: "500", fontSize: "14px" }}>
                  {isBatting
                    ? this.props.activeTab === "International"
                      ? (playerData.batFieldStats &&
                          playerData.batFieldStats[
                            `${activeButton.toLowerCase()}`
                          ] &&
                          playerData.batFieldStats[
                            `${activeButton.toLowerCase()}`
                          ].runs) ||
                        "0"
                      : (playerData.batFieldStats &&
                          playerData.batFieldStats["ipl"] &&
                          playerData.batFieldStats["ipl"].runs) ||
                        "0"
                    : this.props.activeTab === "International"
                      ? (playerData.bowlStats &&
                          playerData.bowlStats[
                            `${activeButton.toLowerCase()}`
                          ] &&
                          playerData.bowlStats[`${activeButton.toLowerCase()}`]
                            .balls) ||
                        "0"
                      : (playerData.bowlStats &&
                          playerData.bowlStats["ipl"] &&
                          playerData.bowlStats["ipl"].balls) ||
                        "0"}
                </p>
                <p>{isBatting ? "Runs" : "Balls"}</p>
                <span
                  style={{
                    width: "1px",
                    background: "#a1a4ac",
                    opacity: "0.5",
                    height: "80%",
                    display: "inline-block",
                    position: "absolute",
                    top: "10%",
                    right: "0"
                  }}
                />
              </div>
              <div style={{ width: "25%", position: "relative" }}>
                <p style={{ fontWeight: "500", fontSize: "14px" }}>
                  {isBatting
                    ? `${
                        this.props.activeTab === "IPL"
                          ? (playerData.batFieldStats &&
                              playerData.batFieldStats["ipl"] &&
                              playerData.batFieldStats["ipl"].centuries) ||
                            "0"
                          : (playerData.batFieldStats &&
                              playerData.batFieldStats[
                                `${activeButton.toLowerCase()}`
                              ] &&
                              playerData.batFieldStats[
                                `${activeButton.toLowerCase()}`
                              ].centuries) ||
                            "0"
                      } / ${
                        this.props.activeTab === "IPL"
                          ? (playerData.batFieldStats &&
                              playerData.batFieldStats["ipl"] &&
                              playerData.batFieldStats["ipl"].fifties) ||
                            "0"
                          : (playerData.batFieldStats &&
                              playerData.batFieldStats[
                                `${activeButton.toLowerCase()}`
                              ] &&
                              playerData.batFieldStats[
                                `${activeButton.toLowerCase()}`
                              ].fifties) ||
                            "0"
                      }`
                    : this.props.activeTab === "IPL"
                      ? (playerData.bowlStats &&
                          playerData.bowlStats["ipl"] &&
                          playerData.bowlStats["ipl"].economy) ||
                        "0"
                      : (playerData.bowlStats &&
                          playerData.bowlStats[
                            `${activeButton.toLowerCase()}`
                          ] &&
                          playerData.bowlStats[`${activeButton.toLowerCase()}`]
                            .economy) ||
                        "0"}
                </p>
                <p> {isBatting ? "100s / 50s" : "Economy"}</p>
                <span
                  style={{
                    width: "1px",
                    background: "#a1a4ac",
                    opacity: "0.5",
                    height: "80%",
                    display: "inline-block",
                    position: "absolute",
                    top: "10%",
                    right: "0"
                  }}
                />
              </div>
              <div style={{ width: "25%", position: "relative" }}>
                <p style={{ fontWeight: "500", fontSize: "14px" }}>
                  {isBatting
                    ? `${
                        this.props.activeTab === "IPL"
                          ? (playerData.batFieldStats &&
                              playerData.batFieldStats["ipl"] &&
                              playerData.batFieldStats["ipl"].fours) ||
                            "0"
                          : (playerData.batFieldStats &&
                              playerData.batFieldStats[
                                `${activeButton.toLowerCase()}`
                              ] &&
                              playerData.batFieldStats[
                                `${activeButton.toLowerCase()}`
                              ].fours) ||
                            "0"
                      } / ${
                        this.props.activeTab === "IPL"
                          ? (playerData.batFieldStats &&
                              playerData.batFieldStats["ipl"] &&
                              playerData.batFieldStats["ipl"].sixes) ||
                            "0"
                          : (playerData.batFieldStats &&
                              playerData.batFieldStats[
                                `${activeButton.toLowerCase()}`
                              ] &&
                              playerData.batFieldStats[
                                `${activeButton.toLowerCase()}`
                              ].sixes) ||
                            "0"
                      }`
                    : `${
                        this.props.activeTab === "IPL"
                          ? (playerData.bowlStats &&
                              playerData.bowlStats["ipl"] &&
                              playerData.bowlStats["ipl"].fiveWickets) ||
                            "0"
                          : (playerData.bowlStats &&
                              playerData.bowlStats[
                                `${activeButton.toLowerCase()}`
                              ] &&
                              playerData.bowlStats[
                                `${activeButton.toLowerCase()}`
                              ].fiveWickets) ||
                            "0"
                      } / ${
                        this.props.activeTab === "IPL"
                          ? (playerData.bowlStats &&
                              playerData.bowlStats["ipl"] &&
                              playerData.bowlStats["ipl"].tenWickets) ||
                            "0"
                          : (playerData.bowlStats &&
                              playerData.bowlStats[
                                `${activeButton.toLowerCase()}`
                              ] &&
                              playerData.bowlStats[
                                `${activeButton.toLowerCase()}`
                              ].tenWickets) ||
                            "0"
                      }`}
                </p>
                <p>{isBatting ? "4s / 6s" : "5w / 10w"}</p>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                marginTop: "24px",
                //   justifyContent: "space-between",
                textAlign: "center"
              }}
            >
              <div style={{ width: "25%", position: "relative" }}>
                <p style={{ fontWeight: "500", fontSize: "14px" }}>
                  {isBatting
                    ? this.props.activeTab === "IPL"
                      ? (playerData.batFieldStats &&
                          playerData.batFieldStats["ipl"] &&
                          playerData.batFieldStats["ipl"].avg) ||
                        "0"
                      : (playerData.batFieldStats &&
                          playerData.batFieldStats[
                            `${activeButton.toLowerCase()}`
                          ] &&
                          playerData.batFieldStats[
                            `${activeButton.toLowerCase()}`
                          ].avg) ||
                        "0"
                    : this.props.activeTab === "IPL"
                      ? (playerData.bowlStats &&
                          playerData.bowlStats["ipl"] &&
                          playerData.bowlStats["ipl"].avg) ||
                        "0"
                      : (playerData.bowlStats &&
                          playerData.bowlStats[
                            `${activeButton.toLowerCase()}`
                          ] &&
                          playerData.bowlStats[`${activeButton.toLowerCase()}`]
                            .avg) ||
                        "0"}
                </p>
                <p>Average</p>
                <span
                  style={{
                    width: "1px",
                    background: "#a1a4ac",
                    opacity: "0.5",
                    height: "80%",
                    display: "inline-block",
                    position: "absolute",
                    top: "10%",
                    right: "0"
                  }}
                />
              </div>
              <div style={{ width: "25%", position: "relative" }}>
                <p style={{ fontWeight: "500", fontSize: "14px" }}>
                  {isBatting
                    ? this.props.activeTab === "IPL"
                      ? (playerData.batFieldStats &&
                          playerData.batFieldStats["ipl"] &&
                          playerData.batFieldStats["ipl"].strikeRate &&
                          playerData.batFieldStats["ipl"].strikeRate.toFixed(
                            2
                          )) ||
                        "0"
                      : (playerData.batFieldStats &&
                          playerData.batFieldStats[
                            `${activeButton.toLowerCase()}`
                          ] &&
                          playerData.batFieldStats[
                            `${activeButton.toLowerCase()}`
                          ].strikeRate &&
                          playerData.batFieldStats[
                            `${activeButton.toLowerCase()}`
                          ].strikeRate.toFixed(2)) ||
                        "0"
                    : this.props.activeTab === "IPL"
                      ? (playerData.bowlStats &&
                          playerData.bowlStats["ipl"] &&
                          playerData.bowlStats["ipl"].strikeRate &&
                          playerData.bowlStats["ipl"].strikeRate.toFixed(2)) ||
                        "0"
                      : (playerData.bowlStats &&
                          playerData.bowlStats[
                            `${activeButton.toLowerCase()}`
                          ] &&
                          playerData.bowlStats[`${activeButton.toLowerCase()}`]
                            .strikeRate) ||
                        "0"
                        ? (playerData.bowlStats &&
                            playerData.bowlStats[
                              `${activeButton.toLowerCase()}`
                            ] &&
                            playerData.bowlStats[
                              `${activeButton.toLowerCase()}`
                            ].strikeRate &&
                            playerData.bowlStats[
                              `${activeButton.toLowerCase()}`
                            ].strikeRate.toFixed(2)) ||
                          "0"
                        : "--"}
                </p>
                <p>SR</p>
                <span
                  style={{
                    width: "1px",
                    background: "#a1a4ac",
                    opacity: "0.5",
                    height: "80%",
                    display: "inline-block",
                    position: "absolute",
                    top: "10%",
                    right: "0"
                  }}
                />
              </div>
              <div style={{ width: "25%", position: "relative" }}>
                <p style={{ fontWeight: "500", fontSize: "14px" }}>
                  {isBatting
                    ? this.props.activeTab === "IPL"
                      ? (playerData.batFieldStats &&
                          playerData.batFieldStats["ipl"] &&
                          playerData.batFieldStats["ipl"].highestScore) ||
                        "0"
                      : (playerData.batFieldStats &&
                          playerData.batFieldStats[
                            `${activeButton.toLowerCase()}`
                          ] &&
                          playerData.batFieldStats[
                            `${activeButton.toLowerCase()}`
                          ].highestScore) ||
                        "0"
                        ? (playerData.batFieldStats &&
                            playerData.batFieldStats[
                              `${activeButton.toLowerCase()}`
                            ] &&
                            playerData.batFieldStats[
                              `${activeButton.toLowerCase()}`
                            ].highestScore) ||
                          "0"
                        : "--"
                    : this.props.activeTab === "IPL"
                      ? (playerData.bowlStats &&
                          playerData.bowlStats["ipl"] &&
                          playerData.bowlStats["ipl"].bestBallInInnings) ||
                        "0"
                        ? (playerData.bowlStats &&
                            playerData.bowlStats["ipl"] &&
                            playerData.bowlStats["ipl"].bestBallInInnings) ||
                          "0"
                        : "--"
                      : (playerData.bowlStats &&
                          playerData.bowlStats[
                            `${activeButton.toLowerCase()}`
                          ] &&
                          playerData.bowlStats[`${activeButton.toLowerCase()}`]
                            .bestBallInInnings) ||
                        "0"
                        ? (playerData.bowlStats &&
                            playerData.bowlStats[
                              `${activeButton.toLowerCase()}`
                            ] &&
                            playerData.bowlStats[
                              `${activeButton.toLowerCase()}`
                            ].bestBallInInnings) ||
                          "0"
                        : "--"}
                </p>
                <p>{isBatting ? "Highest" : "Best"}</p>
              </div>
              <div
                style={{
                  width: "25%",
                  position: "relative",
                  display: "flex"
                }}
              >
                <div
                  style={{
                    width: "50%",
                    background: "rgba(74, 74, 74, 0.2)",
                    borderTopLeftRadius: "50%",
                    borderBottomLeftRadius: "50%"
                  }}
                  onClick={() => this.props.handleToggle("bat")}
                >
                  <div
                    style={{
                      height: "80%",
                      //   width: "74%",
                      margin: "10%",
                      display: "flex",
                      justifyContent: "center",
                      // background: gradientRedOrangeDeg,
                      borderRadius: "50%"
                    }}
                    className={this.props.isBatting ? "bg_red" : ""}
                  >
                    <img
                      style={{ width: "auto", objectFit: "contain" }}
                      src={require("../../../images/bat-icon.svg")}
                    />
                  </div>
                </div>
                {/* <div
                  style={{
                    width: "10%",
                    height: "80%",
                    background: grey_6
                  }}
                /> */}
                <div
                  style={{
                    width: "50%",
                    background: "rgba(74, 74, 74, 0.2)",
                    borderTopRightRadius: "50%",
                    borderBottomRightRadius: "50%"
                  }}
                  onClick={() => this.props.handleToggle("bowl")}
                >
                  <div
                    style={{
                      height: "80%",
                      //   width: "74%",
                      margin: "10%",
                      display: "flex",
                      justifyContent: "center",
                      // background: gradientRedOrangeDeg,
                      borderRadius: "50%"
                    }}
                    className={!this.props.isBatting ? "bg_red" : ""}
                  >
                    <img
                      style={{ width: "auto", objectFit: "contain" }}
                      src={require("../../../images/ball-icon-white.svg")}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
