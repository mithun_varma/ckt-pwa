/* eslint-disable prettier/prettier */
import React, { Component } from "react";
import HrLine from "components-v2/commonComponent/HrLine";
import KeyboardArrowDown from "@material-ui/icons/KeyboardArrowDown";
import { grey_8 } from "../../../styleSheet/globalStyle/color";

export default class PlayerBio extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true
    };
  }
  componentDidMount() {}
  render() {
    const { playerDetails, history } = this.props;
    return (
      <div style={pageStyle.container}>
        <div
          style={pageStyle.playerBioHeading}
          onClick={() => this.setState({ isOpen: !this.state.isOpen })}
        >
          Player Bio <KeyboardArrowDown style={{ color: grey_8 }} />
        </div>
        {this.state.isOpen && (
          <React.Fragment>
            <HrLine style={{ marginTop: "12px" }} />
            <div style={pageStyle.playerBioType}>About</div>
            <div style={pageStyle.playerBioPersonal} className="line-clamp">
              {playerDetails.about ? playerDetails.about : ""}
            </div>
            <div
              style={pageStyle.moreScreen}
              onClick={() => {
                history.push(
                  `/playerbio/${
                    playerDetails.crictecPlayerId
                  }/${playerDetails.displayName
                    .replace(/ /g, "-")
                    .toLowerCase()}`
                );
              }}
            >
              {playerDetails.about && "more"}
            </div>
            <div style={pageStyle.playerBioType}>Teams</div>
            <div style={pageStyle.playerBioTeams}>
              {playerDetails.teams && playerDetails.teams.length > 0
                ? playerDetails.teams.map((team, index) => {
                    return `${team !== null ? team : ""}${
                      index !== playerDetails.teams.length - 1 ? ", " : ""
                    }`;
                  })
                : "--"}
            </div>
            {/* </div> */}
            {/* <div style={pageStyle.moreScreen}>{"more"}</div> */}
          </React.Fragment>
        )}
      </div>
    );
  }
}

const pageStyle = {
  container: {
    display: "flex",
    flexDirection: "column",
    // flex: 1,
    padding: "12px 16px",
    // width:screenWidth*0.911,
    backgroundColor: "#fff",
    // marginLeft:screenWidth*0.044,
    fontFamily: "Montserrat",
    minWidth: "100%"
  },
  playerBioHeading: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    // padding: "12px 0 12px 12px",
    fontSize: "12px",
    // paddingBottom: "12px",
    fontWeight: "500"
    // border: '1px solid #e3e4e6',
    // fontFamily: 'Montserrate-Medium',
  },
  playerBioType: {
    // display: 'flex',
    // flex: 1,
    fontWeight: "600",
    fontSize: "12px",
    letterSpacing: "0.3",
    // justifyContent: 'flex-start',
    color: "#f76b1c",
    marginTop: "12px"
    // marginLeft: screenWidth * 0.044
  },
  playerBioPersonal: {
    // display: "flex",
    // flex: 1,
    fontSize: "12px",
    color: "#141b2f",
    justifyContent: "flex-start",
    // marginLeft: screenWidth * 0.044,
    paddingTop: "5px"
  },
  playerBioTeams: {
    display: "flex",
    flex: 1,
    fontSize: "12px",
    color: "#141b2f",
    justifyContent: "flex-start",
    // marginLeft: screenWidth * 0.044,
    paddingTop: "5px"
  },
  moreScreen: {
    fontSize: "12px",
    color: "#f76b1c",
    display: "flex",
    textTransform: "capitalize",
    justifyContent: "flex-end",
    alignItems: "flex-end",
    padding: "5px"
  }
};
