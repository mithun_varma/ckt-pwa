import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";

// import FantasyResearchCenter from "../../components-v2/Fantasy/FantasyResearchCenter";

import { cardShadow, white } from "../../styleSheet/globalStyle/color";

import fact_engine_icon from "../../images/fact_engine_icon.png";
import QualificationProbability from "./QualificationProbability";
import Header from "../../components-v2/commonComponent/Header";
import BottomBar from "components-v2/commonComponent/BottomBar";

import { createStructuredSelector } from "reselect";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";

import makeSelectCriclytics from "../../containers/Criclytics/selectors";
import reducer from "../../containers/Criclytics/reducer";
import saga from "../../containers/Criclytics/saga";
import * as CriclyticsAction from "../../containers/Criclytics/actions";

export class PlayoffsProjection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mockData: {},
      isHeaderBackground: true
    };
  }

  componentDidMount() {
    this.props.fetchQualificationTable({ seriesId: "opta:1" });
    window.addEventListener("scroll", this.handleScroll);
  }

  render() {
    const { criclytics, history } = this.props;
    return (
      <React.Fragment>
        <div
          style={{
            background: "linear-gradient(176deg, #0b1533, #050b1b)",
            overflowY: "scroll",
            height: "100vh"
          }}
        >
          <div
            style={{
              // background: "#0b1533",
              padding: "54px 16px 16px"
            }}
          >
            <Header
              backgroundColor="#0b1533"
              textColor="#fff"
              isHeaderBackground={this.state.isHeaderBackground}
              title={"Criclytics ™️"}
              leftArrowBack
              leftIconOnClick={() => history.goBack()}
              history={history}
            />
            {/* <div
              style={{
                ...style.headerText,
                display: this.state.displayHeader,
                marginLeft: 10
              }}
            >
              Criclytics ™️
            </div> */}
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              paddingBottom: 90
            }}
          >
            <div
              style={{
                margin: "0 16px",
                // padding: 16,
                background: "#edeff4",
                borderRadius: 3,
                fontFamily: "Montserrat",
                boxShadow: cardShadow
              }}
            >
              <div
                style={{
                  padding: "16px"
                }}
              >
                <div
                  style={{
                    fontFamily: "Montserrat",
                    fontSize: "20px",
                    fontWeight: "bold",
                    color: "#141b2f"
                  }}
                >
                  Worldcup Final 4 Projection
                </div>
                <div
                  style={{
                    padding: "16px 0",
                    paddingBottom: "0",
                    fontFamily: "Montserrat",
                    fontSize: "14px",
                    fontWeight: "300",
                    lineHeight: "1.5",
                    color: "#727682"
                  }}
                >
                  Checkout how your team's fortune has altered over the course
                  of the tournament.
                </div>
              </div>

              <div
                style={{
                  background: white,
                  padding: "16px",
                  display: "flex",
                  boxShadow: "0 3px 6px 0 rgba(0, 0, 0, 0.08)",
                  display: "none" // NOT DISPLAYING FACT ENGINE ANYMORE ***********************************************
                }}
              >
                <div
                  style={{
                    flex: "0.2"
                  }}
                >
                  <div
                    style={
                      {
                        // borderRadius: "50%",
                        // border: "solid 1px #d2d2d2"
                      }
                    }
                  >
                    <img
                      src={fact_engine_icon}
                      alt=""
                      style={{
                        width: "75%",
                        marginTop: "10%",
                        padding: "10%"
                      }}
                    />
                  </div>
                </div>

                <div
                  style={{
                    flex: "0.8"
                  }}
                >
                  <div
                    style={{
                      fontSize: "12px",
                      fontWeight: "normal",
                      color: "#141b2f"
                    }}
                  >
                    Fact Engine
                  </div>
                  <div
                    style={{
                      fontSize: "12px",
                      fontWeight: "normal",
                      lineHeight: "1.5",
                      color: "#727682"
                    }}
                  >
                    The Indian openers have been averaging 41.4 in 2019
                  </div>
                </div>
              </div>
              {criclytics.qualificationTable &&
                criclytics.qualificationTable.length > 0 && (
                  <QualificationProbability
                    playoffProjectionsData={criclytics.qualificationTable}
                    isDesktopComponent={false}
                  />
                )}
            </div>
          </div>
        </div>
        <BottomBar
          {...this.props.history}
          tabs={[
            { key: "home", route: "/" },
            { key: "schedule", route: "schedule" },
            { key: "Criclytics", route: "criclytics" },
            { key: "news", route: "articles" },
            { key: "more", route: "more" }
          ]}
        />
      </React.Fragment>
    );
  }
}

PlayoffsProjection.propTypes = {
  criclytics: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  criclytics: makeSelectCriclytics()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchQualificationTable: payload =>
      dispatch(CriclyticsAction.getQualificationTable(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "criclytics", reducer });
const withSaga = injectSaga({ key: "criclytics", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(PlayoffsProjection);

const normal = "normal";
const style = {
  headerText: {
    fontFamily: "Montserrat",
    fontSize: "22px",
    fontWeight: 600,
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: normal,
    letterSpacing: normal,
    color: "#ffffff"
  }
};
