import React, { Component } from "react";
import {
  gradientGrey,
  grey_8,
  cardShadow,
  white
} from "../../styleSheet/globalStyle/color";
import HrLine from "../../components-v2/commonComponent/HrLine";
// import FantasyResearchCenter from "../../components-v2/Fantasy/FantasyResearchCenter";

import { Slider, Rail, Handles, Tracks } from 'react-compound-slider'
import { SliderRail, Handle, Track } from "./RangeSliderComponents";


export class RangeSlider extends Component {
  constructor(props) {
    super(props);

  }

  handleOnChange = (value) => {
    this.props.handleMatchesUpdate(value);
  }

  render() {
    const sliderStyle = {
      position: "relative",
      width: "100%",
      padding: "20px 0"
    };

    this.state = {
      mockData: {},
      domain: this.props.isControllingSlider ? [0, this.props.totalNumberOfMatches - 1] : [0, 100],
      // defaultValues: [this.props.isControllingSlider ? (this.props.currentNumberOfMatches): this.props.value],
      defaultValues: this.props.isControllingSlider ? [(this.props.currentNumberOfMatches)]: [this.props.value],
      colorCode: this.props.colorCode
    }

    const { domain, defaultValues, colorCode } = this.state
    const {team} = this.props


    return (
      <React.Fragment>
        <Slider
          mode={1}
          step={1}
          domain={domain}
          rootStyle={sliderStyle}
          // onSlideEnd={this.handleOnChange}
          // onChange={this.handleOnChange}
          onUpdate={this.handleOnChange}
          values={defaultValues}
          disabled={this.props.disabled}
        >
          <Rail>
            {({ getRailProps }) => <SliderRail getRailProps={getRailProps} isControllingSlider={this.props.isControllingSlider} />}
          </Rail>
          <Handles>
            {({ handles, getHandleProps }) => (
              <div className="slider-handles">
                {handles.map((handle, index) => (
                  <Handle
                    key={handle.id}
                    handleIndex={index}
                    handle={handle}
                    domain={domain}
                    colorCode={colorCode}
                    getHandleProps={getHandleProps}
                    isControllingSlider={this.props.isControllingSlider}
                  />
                 ))} 
              </div>
            )}
          </Handles>
          <Tracks left={true} right={false}>
            {({ tracks, getTrackProps }) => (
              <div className="slider-tracks">
                {tracks.map(({ id, source, target }) => (
                  <Track
                    key={id}
                    source={source}
                    target={target}
                    getTrackProps={getTrackProps}
                    colorCode={colorCode}
                    isControllingSlider={this.props.isControllingSlider}
                  />
                ))}
              </div>
            )}
          </Tracks>
          {/* <Ticks count={5}>
                {({ ticks }) => (
                  <div className="slider-ticks">
                    {ticks.map(tick => (
                      <Tick key={tick.id} tick={tick} count={ticks.length} />
                    ))}
                  </div>
                )}
              </Ticks> */}
        </Slider>
      </React.Fragment>
    );
  }
}

export default RangeSlider;