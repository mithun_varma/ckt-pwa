import React, { Fragment } from "react";
import PropTypes from "prop-types";

// *******************************************************
// RAIL
// *******************************************************

export function SliderRail({ getRailProps, isControllingSlider }) {
  const railOuterStyle = {
    position: "absolute",
    width: "100%",
    height: 7,
    transform: "translate(0%, -50%)",
    borderRadius: 5,
    cursor: isControllingSlider ? "pointer" : "default"
    // border: '1px solid white',
    // backgroundImage: `linear-gradient(to right, ${colorCode}, #e3e4e6, #eeeeef, #ffffff)`
  };

  const railInnerStyle = {
    position: "absolute",
    width: "100%",
    height: 7,
    transform: "translate(0%, -50%)",
    borderRadius: 5,
    pointerEvents: "none",
    //   backgroundColor: "rgb(155,155,155)",
    background: isControllingSlider
      ? "#cacaca"
      : `linear-gradient(to right, #e3e4e6, #e3e4e6, #eeeeef, #ffffff)`
  };

  return (
    <Fragment>
      <div style={railOuterStyle} {...getRailProps()} />
      <div style={railInnerStyle} />
    </Fragment>
  );
}

SliderRail.propTypes = {
  getRailProps: PropTypes.func.isRequired
};

// *******************************************************
// HANDLE COMPONENT
// *******************************************************
export function Handle({
  domain: [min, max],
  handle: { id, value, percent },
  disabled,
  getHandleProps,
  colorCode,
  handleIndex,
  isControllingSlider
}) {
  return (
    <Fragment>
      <div
        style={{
          left: `${percent}%`,
          position: "absolute",
          transform: "translate(-50%, -50%)",
          WebkitTapHighlightColor: "rgba(0,0,0,0)",
          zIndex: 5,
          width: 28,
          height: 42,
          cursor: isControllingSlider ? "pointer" : "default",
          // border: '1px solid white',
          backgroundColor: "none"
        }}
        {...getHandleProps(id)}
      />
      <div
        role="slider"
        aria-valuemin={min}
        aria-valuemax={max}
        aria-valuenow={value}
        style={{
          flexDirection: "column",
          justifyContent: "center",
          left: `${percent}%`,
          position: "absolute",
          transform: "translate(-50%, -50%)",
          transition: isControllingSlider ? "none" : "all 0.3s ease-out",
          zIndex: 2,
          width: isControllingSlider ? 25 : 30,
          height: isControllingSlider ? 25 : 30,
          borderRadius: "50%",
          // borderStyle: "solid",
          // borderWidth: "1px",
          // borderImageSlice: 1,
          border: `1px solid ${colorCode[0]}`,
          boxShadow: "1px 1px 1px 1px rgba(0, 0, 0, 0.3)",
          // backgroundColor: disabled ? "#666" : "#ffc400",
          backgroundColor: "#ffffff",
          // display: (handleIndex === 0) ? "none" : "flex",
          display: "flex",
          fontSize: "10px",
          marginTop: isControllingSlider ? 0 : -2
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginTop: "4%"
          }}
        >
          {/* {!isControllingSlider ? (value !== null ? (value < 100 ? `${value}${isControllingSlider ? "" : ("%")}` : "Q") : "NA") : ( */}
          {!isControllingSlider ? (
            value !== null ? (
              value == 0 ? (
                "NQ"
              ) : value < 100 ? (
                `${value}${isControllingSlider ? "" : "%"}`
              ) : (
                "Q"
              )
            ) : (
              "NA"
            )
          ) : (
            <div style={{}}>
              <div>
                <div
                  style={{
                    position: "absolute",
                    padding: "3px 6px",
                    borderRadius: 4,
                    color: "white",
                    fontFamily: "Montserrat",
                    fontWeight: 300,
                    // background: "rgba(0, 0, 0, 0.95)",
                    background: "linear-gradient(110deg, #353e59, #1e2437)",
                    marginLeft: "-90%",
                    marginTop: "-120%",
                    minWidth: 26,
                    textAlign: "center",
                    whiteSpace: "nowrap"
                  }}
                >
                  {/* Match {value !== null && `${value }${isControllingSlider ? "" : ("%")}`} */}
                  Match{" "}
                  {value ? `${value}${isControllingSlider ? "" : "%"}` : 0}
                </div>
                <div
                  style={{
                    position: "absolute",
                    width: 0,
                    height: 0,
                    borderStyle: "solid",
                    borderWidth: "10px 5px 0 5px",
                    borderTopWidth: 9,
                    borderRightWidth: 10,
                    borderBottomWidth: 0,
                    borderLeftWidth: 10,
                    borderColor: "#2c3244 transparent transparent transparent",
                    marginTop: "-50%",
                    marginLeft: "-15%"
                  }}
                />
              </div>
              <img
                style={{
                  height: 14,
                  width: 14,
                  marginTop: 3,
                  marginLeft: 1,
                }}
                src={require("../../images/playoff-control-slider-icon.svg")}
              />
            </div>
          )}
        </div>
      </div>
    </Fragment>
  );
}

Handle.propTypes = {
  domain: PropTypes.array.isRequired,
  handle: PropTypes.shape({
    id: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
    percent: PropTypes.number.isRequired
  }).isRequired,
  getHandleProps: PropTypes.func.isRequired,
  disabled: PropTypes.bool
};

Handle.defaultProps = {
  disabled: false
};

// *******************************************************
// KEYBOARD HANDLE COMPONENT
// Uses a button to allow keyboard events
// *******************************************************
export function KeyboardHandle({
  domain: [min, max],
  handle: { id, value, percent },
  disabled,
  getHandleProps
}) {
  return (
    <button
      role="slider"
      aria-valuemin={min}
      aria-valuemax={max}
      aria-valuenow={value}
      style={{
        left: `${percent}%`,
        position: "absolute",
        transform: "translate(-50%, -50%)",
        zIndex: 2,
        width: 24,
        height: 24,
        borderRadius: "50%",
        boxShadow: "1px 1px 1px 1px rgba(0, 0, 0, 0.3)",
        backgroundColor: disabled ? "#666" : "#ffc400"
      }}
      {...getHandleProps(id)}
    />
  );
}

KeyboardHandle.propTypes = {
  domain: PropTypes.array.isRequired,
  handle: PropTypes.shape({
    id: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
    percent: PropTypes.number.isRequired
  }).isRequired,
  getHandleProps: PropTypes.func.isRequired,
  disabled: PropTypes.bool
};

KeyboardHandle.defaultProps = {
  disabled: false
};

// *******************************************************
// TRACK COMPONENT
// *******************************************************
export function Track({
  source,
  target,
  getTrackProps,
  disabled,
  isControllingSlider,
  colorCode
}) {
  return (
    <div
      style={{
        position: "absolute",
        transform: "translate(0%, -50%)",
        transition: isControllingSlider ? "none" : "all 0.3s ease-out",
        height: 7,
        zIndex: 1,
        // backgroundColor: disabled ? "#999" : "#b28900",
        // backgroundColor: "#ffffff",
        backgroundImage: `linear-gradient(to right, ${colorCode[0]}, ${
          colorCode[1]
        })`,
        borderRadius: 5,
        cursor: isControllingSlider ? "pointer" : "default",
        left: `${source.percent}%`,
        width: `${target.percent - source.percent}%`
      }}
      {...getTrackProps()}
    />
  );
}

Track.propTypes = {
  source: PropTypes.shape({
    id: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
    percent: PropTypes.number.isRequired
  }).isRequired,
  target: PropTypes.shape({
    id: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
    percent: PropTypes.number.isRequired
  }).isRequired,
  getTrackProps: PropTypes.func.isRequired,
  disabled: PropTypes.bool
};

Track.defaultProps = {
  disabled: false
};

// *******************************************************
// TICK COMPONENT
// *******************************************************
export function Tick({ tick, count, format }) {
  return (
    <div>
      <div
        style={{
          position: "absolute",
          marginTop: 14,
          width: 1,
          height: 5,
          backgroundColor: "rgb(200,200,200)",
          left: `${tick.percent}%`
        }}
      />
      <div
        style={{
          position: "absolute",
          marginTop: 22,
          fontSize: 10,
          textAlign: "center",
          marginLeft: `${-(100 / count) / 2}%`,
          width: `${100 / count}%`,
          left: `${tick.percent}%`
        }}
      >
        {format(tick.value)}
      </div>
    </div>
  );
}

Tick.propTypes = {
  tick: PropTypes.shape({
    id: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
    percent: PropTypes.number.isRequired
  }).isRequired,
  count: PropTypes.number.isRequired,
  format: PropTypes.func.isRequired
};

Tick.defaultProps = {
  format: d => d
};
