import React, { Component } from "react";
import { cardShadow, white } from "../../styleSheet/globalStyle/color";
import HrLine from "../../components-v2/commonComponent/HrLine";
// import FantasyResearchCenter `from "../../components-v2/Fantasy/FantasyResearchCenter";
import map from "lodash/map";
import { RangeSlider } from "./RangeSlider";

export class QualificationProbability extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initial: true,
      mockData: {},
      sliderValue: 10,
      matchesUpdated: true,
      currentNumberOfMatches: 0,
      totalNumberOfMatches: 0,
      teamProbabilities: []
    };
  }

  static getDerivedStateFromProps = (nextprops, state) => {
    if (state.initial && nextprops.playoffProjectionsData) {
      return {
        ...state,
        currentNumberOfMatches:
          nextprops.playoffProjectionsData.length !== 0
            ? nextprops.playoffProjectionsData.length - 1
            : 0,
        totalNumberOfMatches:
          (nextprops.playoffProjectionsData.length !== 0 &&
            nextprops.playoffProjectionsData.length) ||
          0,
        teamProbabilities: nextprops.playoffProjectionsData[0].pointsTable,
        initial: false
      };
    }
  };

  handleMatchesUpdate = chosenNumberOfMatches => {
    if (
      chosenNumberOfMatches[0] !== null &&
      chosenNumberOfMatches[0] <= this.props.playoffProjectionsData.length - 1
    ) {
      this.setState({
        teamProbabilities: this.props.playoffProjectionsData[
          chosenNumberOfMatches[0]
        ].pointsTable,
        // matchesUpdated: !this.state.matchesUpdated,
        currentNumberOfMatches: chosenNumberOfMatches[0]
      });
    }
  };

  handleGoLive = () => {
    this.setState({
      currentNumberOfMatches:
        this.state.totalNumberOfMatches === 1
          ? 0
          : this.state.totalNumberOfMatches
    });
  };

  render() {
    const { playoffProjectionsData } = this.props;
    // console.log(playoffProjectionsData);
    return (
      <React.Fragment>
        <div
          style={{
            display: "flex",
            background: !this.props.isDesktopComponent && white,
            padding: 16,
            // marginTop: this.props.isDesktopComponent ? 16 : 0,
            boxShadow: !this.props.isDesktopComponent && cardShadow,
            paddingTop: this.props.isDesktopComponent ? 12 : 16
          }}
        >
          <div
            style={{
              fontSize: this.props.isDesktopComponent ? 20 : 12,
              fontWeight: this.props.isDesktopComponent ? "bold" : "normal",
              color: "#141b2f"
            }}
          >
            Qualification Probability
          </div>
        </div>
        <HrLine />
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            background: white,
            padding: "16px"
          }}
        >
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "center"
            }}
          >
            <div
              style={{
                flex: "0.99"
              }}
            >
              {/* {map(this.state.teamProbabilities, (team, key) => ( */}
              {this.state.teamProbabilities &&
                Object.keys(this.state.teamProbabilities) &&
                Object.keys(this.state.teamProbabilities)
                  .sort()
                  .map((team, key) => (
                    <div
                      key={team}
                      style={{
                        display: "flex",
                        flexDirection: "row"
                      }}
                    >
                      <div
                        style={{
                          display: "flex",
                          flex: 0.2,
                          alignItems: "center",
                          justifyContent: "center",
                          fontSize: "10px",
                          color: "#43495a"
                        }}
                      >
                        {this.state.teamProbabilities[team].teamName}
                      </div>
                      <div
                        style={{
                          flex: 0.7,
                          alignItems: "center",
                          justifyContent: "center",
                          zIndex: 0
                        }}
                      >
                        {/* {console.log("Quaaaaa > playoffProjectionsData > ", playoffProjectionsData)}
                  {console.log("Quaaaaa > currentNumberOfMatches > ", this.state.currentNumberOfMatches)}
                  {console.log("Quaaaaa > playoffProjectionsData[this.state.currentNumberOfMatches].pointsTable[team.name] " + team.name + "  > ", playoffProjectionsData[this.state.currentNumberOfMatches].pointsTable[team.name])} */}
                        <RangeSlider
                          value={
                            (playoffProjectionsData[
                              this.state.currentNumberOfMatches
                            ] &&
                              playoffProjectionsData[
                                this.state.currentNumberOfMatches
                              ].pointsTable[team] &&
                              playoffProjectionsData[
                                this.state.currentNumberOfMatches
                              ].pointsTable[team].probability) ||
                            0
                          }
                          disabled={true}
                          colorCode={this.state.teamProbabilities[team].color}
                          team={this.state.teamProbabilities[team]}
                          // handleMatchesUpdate={() => ({})}
                        />
                      </div>
                    </div>
                  ))}
            </div>
            <div
              style={{
                display: "flex",
                flex: "0.01"
              }}
            >
              <div
                style={{
                  display: "flex",
                  borderLeft: "1px dashed #dadada",
                  width: "1px",
                  marginLeft: "-1000%"
                }}
              />
            </div>
          </div>

          <div
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "flex-end",
              background: white
            }}
          >
            <div
              style={{
                fontSize: "10px",
                fontWeight: 600,
                color: "#141b2f",
                padding: "2px 8px",
                paddingRight: this.props.isDesktopComponent ? 8 : 2,
                marginRight: this.props.isDesktopComponent ? "5%" : "0%"
              }}
            >
              Semi-final
            </div>
          </div>
        </div>

        <div
          style={{
            display: "flex",
            flexDirection: this.props.isDesktopComponent ? "row" : "column",
            background: white,
            padding: "16px 8px",
            paddingTop: !this.props.isDesktopComponent && 0,
            borderRadius: 3,
            boxShadow: !this.props.isDesktopComponent && cardShadow
          }}
        >
          <div
            style={{
              display: "flex",
              flex: this.props.isDesktopComponent ? 0.79 : 1
            }}
          >
            <div
              style={{
                display: "flex",
                flex: 0.18,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <div
                style={{
                  fontSize: "10px",
                  fontWeight: "normal",
                  color: "#43495a"
                }}
              >
                Match 0
              </div>
            </div>
            <div
              style={{
                flex: 0.6,
                alignItems: "center",
                justifyContent: "center",
                marginLeft: "6%",
                marginRight: "6%",
                touchAction: "none",
                zIndex: 0
              }}
            >
              <div>
                <RangeSlider
                  value={this.state.currentNumberOfMatches}
                  disabled={this.state.totalNumberOfMatches === 1}
                  colorCode={["#d74730", "#d74730"]}
                  currentNumberOfMatches={this.state.currentNumberOfMatches}
                  totalNumberOfMatches={this.state.totalNumberOfMatches}
                  isControllingSlider={true}
                  team={{ name: "Controlling Slider" }}
                  handleMatchesUpdate={value => this.handleMatchesUpdate(value)}
                />
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flex: 0.2,
                alignItems: "center",
                justifyContent: "center"
                // marginLeft: "4%"
              }}
            >
              <div
                style={{
                  fontSize: "10px",
                  fontWeight: "normal",
                  color: "#43495a"
                }}
              >
                Match{" "}
                {/* {this.state.currentNumberOfMatches}  */}
                {this.state.totalNumberOfMatches - 1 }
              </div>
            </div>
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: this.props.isDesktopComponent ? "center" : "flex-end",
              marginTop: this.props.isDesktopComponent ? 0 : 15,
              flex: 0.2
            }}
          >
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
                borderRadius: "8px",
                backgroundColor:
                  this.state.totalNumberOfMatches ===
                  this.state.currentNumberOfMatches + 1
                    ? "#525252"
                    : "#d44030",
                marginRight: "3%",
                width: this.props.isDesktopComponent ? "50%" : "20%"
              }}
              onClick={this.handleGoLive}
            >
              <div
                style={{
                  fontSize: "10px",
                  color: white,
                  padding: "2px 8px",
                  whiteSpace: "nowrap",
                  cursor:
                    this.state.totalNumberOfMatches ===
                    this.state.currentNumberOfMatches + 1
                      ? "default"
                      : "pointer"
                }}
              >
                GO LIVE
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default QualificationProbability;

const teamProbabilitiesValues = [
  {
    name: "MI",
    probability: 0,
    color: "#0f6fae"
  },
  {
    name: "CSK",
    probability: 0,
    color: "#f0b12e"
  },
  {
    name: "RCB",
    probability: 0,
    color: "#f34039"
  },
  {
    name: "KKR",
    probability: 0,
    color: "#885ca7"
  },
  {
    name: "SRH",
    probability: 0,
    color: "#fd8e20"
  },
  {
    name: "KXIP",
    probability: 0,
    color: "#d03e49"
  },
  {
    name: "RR",
    probability: 0,
    color: "#ee7fce"
  },
  {
    name: "DC",
    probability: 0,
    color: "#0075bd"
  }
];

const teamProbabilitiesToggledValues = [
  {
    name: "MI",
    probability: 48,
    color: "#0f6fae"
  },
  {
    name: "CSK",
    probability: 52,
    color: "#f0b12e"
  },
  {
    name: "RCB",
    probability: 83,
    color: "#f34039"
  },
  {
    name: "KKR",
    probability: 34,
    color: "#885ca7"
  },
  {
    name: "SRH",
    probability: 68,
    color: "#fd8e20"
  },
  {
    name: "KXIP",
    probability: 100,
    color: "#d03e49"
  },
  {
    name: "RR",
    probability: 34,
    color: "#ee7fce"
  },
  {
    name: "DC",
    probability: 58,
    color: "#0075bd"
  }
];
