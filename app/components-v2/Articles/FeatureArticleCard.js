import React, { Component } from "react";
import {
  white,
  red_Orange,
  grey_10,
  grey_8,
  grey_6
} from "../../styleSheet/globalStyle/color";
import moment from "moment";
import Person from "@material-ui/icons/Person";
// import { AUTHOR_DATE_FORMAT_NUMBER } from "utils/constants";
import Truncate from "react-truncate";

let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  ClevertapReact.initialize("W88-4KR-845Z");
}
export class FeatureArticleCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeItem: "saved"
    };
  }

  render() {
    const { article, cardType, articleHost } = this.props;

    const imageUrl =
      article && article.imageData && article.imageData.featureThumbnail
        ? article.imageData.featureThumbnail
        : article.imageData && article.imageData.thumbnail
          ? article.imageData.thumbnail
          : article.imageData && article.imageData.image
            ? article.imageData.image
            : "https://via.placeholder.com/145";
    return (
      <div
        style={{
          padding: 16,
          ...this.props.rootStyles
        }}
        onClick={e => {
          e.preventDefault();
          ClevertapReact.event("Article", {
            source: "Article",
            articleId: article.id || article._id
          });
          this.props.cardOnClick(
            // `/articles/${cardType}/${article.title
            // .toLowerCase()
            // .replace(/ /g, "-")
            // .replace(/[`~!@#$%^&*()_|+\=?;:'",.]/g, "")}/${article._id ||
            // article.id}`
            `/articles/${cardType}/${
            article.id ?
              article.id
                // .toLowerCase()
                // .replace(/ /g, "-")
                // .replace(/[`~!@#$%^&*()_|+\=?;:'",.]/g, "")
              :
              article._id}`


          );
        }}
      >
        <div
          style={{
            borderRadius: 2,
            height: 142,
            display: "flex",
            position: "relative",
            backgroundImage: `url(${imageUrl})`,
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "top center"
          }}
        >
          {/* <img
            // src={require("../../images/articleFeatured.png")}
            src={imageUrl}
            alt=""
            style={{
              width: "100%",
              height: "100%",
              borderRadius: 2
            }}
          /> */}
          {article.matchName && (
            <div
              className="clip-title"
              style={{
                position: "absolute",
                bottom: 12,
                left: 0,
                color: white,
                // padding: "2px 18px 2px 16px",
                padding: "2px 18px 2px 12px",
                fontFamily: "mont400",
                // fontWeight: 500,
                fontSize: 11,
                background: red_Orange,
                borderRadius: "0 3px 3px 0",
                textTransform: "uppercase"
              }}
            >
              {article.matchName}
            </div>
          )}
        </div>
        <div style={{ ...this.props.contentStyles }}>
          <div
            style={{
              color: grey_10,
              fontFamily: "Montserrat",
              fontWeight: 600,
              fontSize: 14,
              padding: "10px 0 4px 0"
            }}
          >
            <Truncate lines={2} ellipsis={<span>...</span>}>
              {(article && article.title) || "-"}
            </Truncate>
          </div>
          <div
            style={{
              color: grey_10,
              fontFamily: "Montserrat",
              // fontWeight: 700,
              fontSize: 12
            }}
          >
            <Truncate lines={2} ellipsis={<span>...</span>}>
              {(article && article.description) || "-"}
            </Truncate>
          </div>
          <div
            style={{
              color: grey_8,
              fontFamily: "Montserrat",
              fontWeight: 500,
              fontSize: 10,
              fontStyle: "italic",
              paddingTop: 6,
              display: "flex"
            }}
          >
            <Person style={{ fontSize: 12, color: grey_6, marginRight: 4 }} />
            {article && article.author}
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <div
              style={{
                color: grey_10,
                fontFamily: "Montserrat",
                fontWeight: 500,
                fontSize: 10
              }}
            >
              <span>
                {(article && moment(article.createdAt).format("DD MMM YYYY")) ||
                  "-"}
              </span>
              <span
                style={{
                  padding: "0 6px"
                }}
              >
                {article && article.readTime ? "•" : ""}
              </span>
              <span
                style={{
                  color: grey_6
                }}
              >
                {article && article.readTime ? article.readTime : ""}
              </span>
            </div>
            {/* <div style={{ display: "flex" }}>
              <Bookmark
                style={{
                  // color: orange_10,
                  color: grey_6,
                  fontSize: 26
                }}
              />
            </div> */}
          </div>
        </div>
      </div>
    );
  }
}

export default FeatureArticleCard;
