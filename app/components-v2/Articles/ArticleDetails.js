import React, { Component } from "react";
import ButtonBar from "../commonComponent/ButtonBar";
import HrLine from "../commonComponent/HrLine";
import CardGradientTitle from "../commonComponent/CardGradientTitle";
import { Helmet } from "react-helmet";
import {
  white,
  orange_10,
  gradientOrange,
  grey_10,
  grey_8,
  grey_6,
  grey_4,
  grey_2,
  cardShadow,
  gradientOrangeLeft,
  gradientRedOrange
} from "../../styleSheet/globalStyle/color";
import FeatureArticleCard from "./FeatureArticleCard";
import Bookmark from "@material-ui/icons/Bookmark";
import SwapVert from "@material-ui/icons/SwapVert";
import Person from "@material-ui/icons/Person";
import Favorite from "@material-ui/icons/Favorite";
import Reply from "@material-ui/icons/Reply";
import PropTypes from "prop-types";
import moment from "moment";
import { AUTHOR_DATE_FORMAT_NUMBER } from "utils/constants";
import PreviewChart from "../commonComponent/PreviewChart";

export class ArticleDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      frameHeight: 100,
      frameWidth: 100
    };
  }

  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions);
  }

  componentWillUnmount() {
    // window.removeEventListener('resize', this.updateDimensions);
  }

  updateDimensions = () => {
    let height = document.body.scrollHeight;
    let width = document.body.scrollWidth;
    this.setState({
      frameHeight: height * 1.4
    });
  };

  getElementByType = elementData => {
    switch (elementData.type) {
      case "text":
        return (
          <React.Fragment>
            {/* <p>{elementData.data.replace('<p>', '').replace('</p>', '')}</p> */}
            <div dangerouslySetInnerHTML={{ __html: elementData.data }} />
            <br />
          </React.Fragment>
        );
      case "image":
        return (
          <React.Fragment>
            <img
              alt="article_image"
              style={{ width: "100%" }}
              src={elementData.data.url}
            />
            <br />
          </React.Fragment>
        );
      case "embed":
        return (
          <React.Fragment>
            {elementData.data.type == "s3" ? (
              elementData.data.url ? (
                <video controls width="100%" height="200">
                  <source src={elementData.data.url} type="video/ogg" />
                  <source src={elementData.data.url} type="video/mp4" />
                </video>
              ) : null
            ) : elementData.data.url ? (
              <iframe
                title="article_video"
                style={{ width: "100%", height: "200px" }}
                style={{
                  width: "100%",
                  height:
                    elementData.data &&
                    elementData.data.url &&
                    elementData.data.url.indexOf("infogram") > -1
                      ? "500px"
                      : "200px"
                }}
                // style={{
                //   width: "100%",
                //   height:
                //     elementData.data &&
                //     elementData.data.url &&
                //     elementData.data.url.indexOf("infogram") > -1
                //       ? this.state.frameHeight
                //       : "200px"
                // }}
                // src={this.getVideoId(elementData.data.url)}
                src={
                  elementData.data &&
                  elementData.data.url &&
                  elementData.data.url.indexOf("infogram") > -1
                    ? elementData.data.url
                    : this.getVideoId(elementData.data.url)
                }
              />
            ) : null}
          </React.Fragment>
        );
      case "quote":
        return (
          <React.Fragment>
            <blockquote>
              {elementData.data.content.replace("<p>", "").replace("</p>", "")}
            </blockquote>
          </React.Fragment>
        );
      case "h2":
        return (
          <React.Fragment>
            <h2 dangerouslySetInnerHTML={{ __html: elementData.data }} />
          </React.Fragment>
        );
      case "ol":
        return (
          <React.Fragment>
            <ol>
              {elementData.data.map((li, index) => (
                <li key={`list_${index}`}>
                  {li.content.replace("<p>", "").replace("</p>", "")}
                </li>
              ))}
            </ol>
          </React.Fragment>
        );
      case "ul":
        return (
          <React.Fragment>
            <ul>
              {elementData.data.map((li, index) => (
                <li key={`list_${index}`}>
                  {li.content.replace("<p>", "").replace("</p>", "")}
                </li>
              ))}
            </ul>
          </React.Fragment>
        );
      case "chart":
        return (
          <React.Fragment>
            <PreviewChart
              pieWidth={200}
              pieHeight={200}
              legendWidth={200}
              legendHeight={100}
              // content={3}
              content={elementData.data}
            />
          </React.Fragment>
        );
      default:
        return null;
    }
  };

  cardOnClick = path => {
    this.props.history.push(path);
    // path.split("/")[path.split("/").length-1]
    // this.props.fetchArticleDetails({ id: this.props.match.params.articleId });
    this.props.fetchArticleDetails({
      id: path.split("/")[path.split("/").length - 1]
    });
  };

  getVideoId = url => {
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    const match = url.match(regExp);

    if (match && match[2].length === 11) {
      return `//www.youtube.com/embed/${match[2]}`;
    }
    return "error";
  };

  render() {
    const {
      article,
      article: { articleDetails, articleHost }
    } = this.props;
    // const imageUrl =
    //   articleHost &&
    //   articleDetails &&
    //   articleDetails.imageData &&
    //   articleDetails.imageData.image
    //     ? articleHost + articleDetails.imageData.image
    //     : "https://via.placeholder.com/320x240";

    const imageUrl =
      // articleHost &&
      articleDetails &&
      articleDetails.imageData &&
      articleDetails.imageData.image
        ? articleDetails.imageData.image
        : "https://via.placeholder.com/145";

    const videoUrl =
      articleDetails && articleDetails.videoData && articleDetails.videoData.url
        ? articleDetails.videoData.url
        : "https://via.placeholder.com/145";
    const parsedData = articleDetails.content && articleDetails.content;
    return (
      <React.Fragment>
        <Helmet titleTemplate="%s | cricket.com">
          <title>{`${article.articleDetails &&
            article.articleDetails.title} | ${article.articleDetails &&
            article.articleDetails.type}`}</title>
          <meta
            name="description"
            content={
              (article.articleDetails &&
                article.articleDetails.seoDescription) ||
              (article.articleDetails && article.articleDetails.description)
            }
          />
          <meta
            name="keywords"
            content={
              (article.articleDetails && article.articleDetails.seoKeywords) ||
              ""
            }
          />
          <meta itemprop="width" content="595" />
          <meta itemprop="height" content="450" />
          <meta itemprop="url" content={imageUrl} />

          <meta property="og:video" content={videoUrl} />
          <meta property="og:video:url" content={videoUrl} />
          <meta property="og:video:secure_url" content={videoUrl} />
          <meta property="og:video:type" content="video/mp4" />
          <meta
            property="og:video:type"
            content="application/x-shockwave-flash"
          />
          <meta property="og:video:width" content="595" />
          <meta property="og:video:height" content="450" />

          <meta property="og:image" itemprop="image" content={imageUrl} />
          <meta property="og:width" content="595" />
          <meta property="og:height" content="450" />
          <meta property="og:image:width" content="595" />
          <meta property="og:image:height" content="450" />
          <meta property="og:image:secure_url" content={imageUrl} />
          <meta
            property="og:title"
            content={
              (article.articleDetails && article.articleDetails.title) ||
              "cricket.com article"
            }
          />
          <meta
            property="og:description"
            content={
              (article.articleDetails &&
                article.articleDetails.seoDescription) ||
              (article.articleDetails && article.articleDetails.description) ||
              "cricket.com article "
            }
          />
          <meta
            property="og:url"
            content={"https://www.cricket.com" + this.props.match.url}
          />
          <meta property="og:type" content="Article" />
          <meta property="fb:app_id" content="631889693979290" />
          <meta property="og:site_name" content="cricket.com" />
          <link
            rel="canonical"
            href={`https://www.cricket.com${
              this.props.history.location.pathname
            }`}
          />
          {/* <link rel="apple-touch-icon" href="http://mysite.com/img/apple-touch-icon-57x57.png" /> */}
        </Helmet>
        <div
          style={{
            height: 176,
            // backgroundImage: `url(${require("../../images/articleFeatured.png")})`,
            backgroundImage: `url(${imageUrl})`,
            backgroundSize: "cover",
            backgroundPosition: "top",
            backgroundRepeat: "no-repeat"
          }}
        >
          {/* <img
            src={require("../../images/articleFeatured.png")}
            alt=""
            style={{
              width: "100%",
              height: "100%"
            }}
          /> */}
          <meta itemprop="width" content="595" />
          <meta itemprop="height" content="450" />
          <meta itemprop="url" content={imageUrl} />

          <meta property="og:video" content={videoUrl} />
          <meta property="og:video:url" content={videoUrl} />
          <meta property="og:video:secure_url" content={videoUrl} />
          <meta property="og:video:type" content="video/mp4" />
          <meta
            property="og:video:type"
            content="application/x-shockwave-flash"
          />
          <meta property="og:video:width" content="595" />
          <meta property="og:video:height" content="450" />

          <meta property="og:image" itemprop="image" content={imageUrl} />
          <meta property="og:width" content="595" />
          <meta property="og:height" content="450" />
          <meta property="og:image:width" content="595" />
          <meta property="og:image:height" content="450" />
          <meta property="og:image:secure_url" content={imageUrl} />
          <meta
            property="og:title"
            content={
              (article.articleDetails && article.articleDetails.title) ||
              "cricket.com article"
            }
          />
          <meta
            property="og:description"
            content={
              (article.articleDetails &&
                article.articleDetails.seoDescription) ||
              (article.articleDetails && article.articleDetails.description) ||
              "cricket.com article "
            }
          />
          <meta
            property="og:url"
            content={"https://www.cricket.com" + this.props.match.url}
          />
          <meta property="og:type" content="Article" />
          <meta property="fb:app_id" content="631889693979290" />
          <meta property="og:site_name" content="cricket.com" />
        </div>
        {/* Body Section */}
        <div style={{ padding: "0 16px" }}>
          {/* Section 1 */}
          <div
            style={{
              background: white,
              borderRadius: 3,
              boxShadow: cardShadow,
              marginBottom: 10,
              marginTop: -30
            }}
          >
            {articleDetails &&
              articleDetails.matchName && (
                <CardGradientTitle
                  title={
                    articleDetails && articleDetails.matchName
                      ? articleDetails.matchName
                      : ""
                  }
                  rootStyles={{
                    borderRadius: 3,
                    background: white
                  }}
                  // rightElement={
                  //   <div
                  //     style={{
                  //       display: "flex"
                  //     }}
                  //   >
                  //     <Bookmark style={{ color: orange_10 }} />
                  //   </div>
                  // }
                />
              )}
            <div className="article-details" style={{ padding: 16 }}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  fontSize: 10,
                  fontFamily: "Montserrat",
                  fontWeight: 600,
                  color: grey_10
                }}
              >
                {/* <div>25-01-2019</div> */}
                <div>
                  {(articleDetails &&
                    moment(articleDetails.publishedAt).format(
                      AUTHOR_DATE_FORMAT_NUMBER
                    )) ||
                    "-"}
                </div>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    color: grey_6,
                    fontStyle: "italic",
                    textTransform: "capitalize"
                  }}
                >
                  <Person
                    style={{ color: grey_6, fontSize: 16, marginRight: 2 }}
                  />
                  {articleDetails && articleDetails.author}
                </div>
              </div>
              <div
                style={{
                  color: grey_10,
                  fontFamily: "Montserrat",
                  fontWeight: 700,
                  fontSize: 14,
                  padding: "10px 0"
                }}
              >
                {(articleDetails && articleDetails.title) || "-"}
              </div>
              {articleDetails.title && (
                <div
                  style={{
                    display: "flex"
                  }}
                >
                  <a
                    target="_blank"
                    href={`https://www.facebook.com/sharer.php?u=https://www.cricket.com${encodeURIComponent(
                      // href={`https://touch.facebook.com/sharer.php?u=https://www.cricket.com${encodeURIComponent(
                      this.props.history.location.pathname
                    )}&amp;text=${articleDetails.title}&amp;via=cricket.com`}
                    rel="noreferrer"
                  >
                    <img
                      src={require("../../images/fb.svg")}
                      alt=""
                      style={{
                        width: 20,
                        height: 20
                      }}
                    />
                  </a>
                  <a
                    href={`https://twitter.com/intent/tweet?url=https://www.cricket.com${encodeURIComponent(
                      this.props.history.location.pathname
                    )}&amp;text=${articleDetails.title}&amp;via=cricket.com`}
                    target="_blank"
                  >
                    <img
                      src={require("../../images/twitter.svg")}
                      alt=""
                      style={{
                        width: 20,
                        height: 20,
                        margin: "0 10px"
                      }}
                    />
                  </a>
                  <a
                    href={`https://wa.me/?text=${
                      articleDetails.title
                    } - https://www.cricket.com${encodeURIComponent(
                      this.props.history.location.pathname
                    )} (Sent via cricket.com)`}
                    target="_blank"
                  >
                    <img
                      src={require("../../images/whatsapp.svg")}
                      alt=""
                      style={{
                        width: 20,
                        height: 20
                      }}
                    />
                  </a>
                </div>
              )}
              <div
                style={{
                  fontSize: 12,
                  fontFamily: "Montserrat",
                  color: grey_10,
                  fontStyle: "italic"
                }}
              >
                {(articleDetails && articleDetails.description) || "-"}
              </div>
              <div
                className="article-inner-content"
                style={{
                  marginTop: 16,
                  fontSize: 12,
                  fontFamily: "Montserrat",
                  color: grey_10
                }}
              >
                {parsedData &&
                  parsedData.length > 0 &&
                  parsedData.map(elementObj =>
                    this.getElementByType(elementObj)
                  )}
              </div>

              <div>
                <div
                  style={{
                    color: grey_10,
                    fontFamily: "Montserrat",
                    fontWeight: 700,
                    fontSize: 12,
                    padding: "10px 0 6px"
                  }}
                >
                  Tags
                </div>
                <div
                  style={{
                    display: "flex",
                    flexWrap: "wrap"
                  }}
                >
                  {articleDetails.tags &&
                    articleDetails.tags.map((tag, key) => (
                      <div>
                        <span
                          style={{
                            // background: gradientOrangeLeft,
                            background: gradientRedOrange,
                            padding: "2px 6px",
                            color: white,
                            borderRadius: 3,
                            fontFamily: "Montserrat",
                            fontWeight: 700,
                            fontSize: 12,
                            marginRight: 6
                          }}
                        >
                          {tag.name}
                        </span>
                      </div>
                    ))}
                </div>
              </div>
            </div>
          </div>
          {/* Section 1 closing */}

          {/* Section 2 Related Articles */}
          {articleDetails.relatedArticles &&
            articleDetails.relatedArticles.length > 0 && (
              <div
                style={{
                  background: white,
                  borderRadius: 3,
                  boxShadow: cardShadow,
                  marginBottom: 10
                }}
              >
                <CardGradientTitle
                  title="You may also like"
                  rootStyles={{
                    background: white
                  }}
                />
                <div
                  style={{
                    // display: "flex",
                    // flexDirection: "row",
                    // flex: 1,
                    overflowX: "scroll",
                    overflowY: "hidden",
                    whiteSpace: "nowrap",
                    paddingLeft: "12px"
                  }}
                >
                  {articleDetails.relatedArticles.map((item, itemKey) => (
                    <div
                      style={{
                        width:
                          articleDetails.relatedArticles.length == 1
                            ? "96.5%"
                            : "80%",
                        display: "inline-block",
                        overflow: "hidden",
                        marginRight: 12
                      }}
                      key={`${itemKey + 1}`}
                    >
                      <FeatureArticleCard
                        article={item}
                        articleHost={article.articleHost}
                        cardOnClick={this.cardOnClick}
                        cardType={item.type}
                        rootStyles={{
                          padding: "16px 0px",
                          borderRadius: 3
                        }}
                        contentStyles={{
                          padding: "0 12px 6px",
                          whiteSpace: "initial",
                          background: grey_4,
                          minHeight: 118
                        }}
                      />
                    </div>
                  ))}
                </div>
              </div>
            )}
          {/* Section 2 closing */}

          {/* Section 3 */}
          {false && (
            <div
              style={{
                background: white,
                borderRadius: 3,
                boxShadow: cardShadow,
                marginBottom: 10
              }}
            >
              <CardGradientTitle
                title="Comments"
                rootStyles={{
                  borderRadius: 3,
                  background: white
                }}
                rightElement={
                  <div
                    style={{
                      display: "flex"
                    }}
                  >
                    <span
                      style={{
                        color: grey_6,
                        fontFamily: "Montserrat",
                        fontSize: 12
                      }}
                    >
                      Last
                    </span>
                    <SwapVert style={{ color: grey_6, fontSize: 18 }} />
                  </div>
                }
              />
              <div>
                <div
                  style={{ padding: 10, display: "flex", alignItems: "center" }}
                >
                  <div style={{ display: "flex", flex: 0.2 }}>
                    <div
                      style={{
                        width: 39,
                        height: 39,
                        borderRadius: "50%",

                        backgroundImage: `url(${require("../../images/dhoni.png")})`,
                        backgroundSize: "cover",
                        backgroundPosition: "center",
                        backgroundRepeat: "no-repeat"
                      }}
                    />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flex: 0.8
                    }}
                  >
                    <input
                      type="text"
                      placeholder="comment"
                      style={{
                        width: "100%",
                        background: grey_2,
                        padding: "12px 6px",
                        boxSizing: "border-box",
                        border: `1px solid ${grey_2}`,
                        borderRadius: 2,
                        color: grey_8,
                        outline: "none"
                        // outlineColor: "#dbe2e0"
                      }}
                    />
                  </div>
                </div>
                <HrLine />
                {[1, 2, 3, 4].map(item => (
                  <div>
                    <div style={{ padding: 10, display: "flex" }}>
                      <div style={{ display: "flex", flex: 0.2 }}>
                        <div
                          style={{
                            width: 39,
                            height: 39,
                            borderRadius: "50%",

                            backgroundImage: `url(${require("../../images/dhoni.png")})`,
                            backgroundSize: "cover",
                            backgroundPosition: "center",
                            backgroundRepeat: "no-repeat"
                          }}
                        />
                      </div>
                      <div
                        style={{
                          display: "flex",
                          flex: 0.8,
                          flexDirection: "column",
                          fontFamily: "Montserrat"
                        }}
                      >
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                            alignItems: "center"
                          }}
                        >
                          <div
                            style={{
                              color: grey_10,
                              fontWeight: 700,
                              fontSize: 12
                            }}
                          >
                            Gautham Varma
                          </div>
                          <div
                            style={{
                              color: grey_8,
                              fontWeight: 600,
                              fontSize: 11
                            }}
                          >
                            30/12/2018
                          </div>
                        </div>
                        <div
                          style={{
                            color: grey_8,
                            fontWeight: 600,
                            fontSize: 12
                          }}
                        >
                          Hardik Pandya doesn’t deserve to play in the squad!
                        </div>
                        <div
                          style={{
                            display: "flex",
                            alignItems: "center",
                            fontWeight: 600,
                            fontSize: 12,

                            letterSpacing: "0.2px"
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              alignItems: "center",
                              color: orange_10
                            }}
                          >
                            <Favorite
                              style={{
                                color: orange_10,
                                fontSize: 14,
                                marginRight: 4
                              }}
                            />
                            <span>Liked</span>
                          </div>
                          <div
                            style={{
                              display: "flex",
                              alignItems: "center",
                              marginLeft: 6,
                              color: grey_8
                            }}
                          >
                            <Reply
                              style={{
                                color: grey_6,
                                fontSize: 18,
                                marginRight: 4
                              }}
                            />
                            <span>Reply(1)</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <HrLine />
                  </div>
                ))}
              </div>
            </div>
          )}
          {/* Section 3 closing */}
        </div>
        {/* Body Section Closing */}
      </React.Fragment>
    );
  }
}

export default ArticleDetails;

const tags = [
  "Hardik Pandya",
  "India",
  "Gavaskar-Hadlee Series",
  "New Zealand",
  "India",
  "New Zealand",
  "India"
];
