import React, { Component } from "react";
import FeatureArticleCard from "./FeatureArticleCard";
import ArticleCard from "./ArticleCard";
import ButtonBar from "../commonComponent/ButtonBar";
import HrLine from "../commonComponent/HrLine";
import CardGradientTitle from "../commonComponent/CardGradientTitle";
import {
  white,
  orange_10,
  cardShadow
} from "../../styleSheet/globalStyle/color";

const BUTTON_CONFIG = ["news thread", "saved"];
export class ArticleListing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeButton: "saved"
    };
  }

  handleActiveButton = activeButton => {
    this.setState({
      activeButton
    });
  };

  render() {
    return (
      <div
        style={{
          padding: 16
        }}
      >
        {/* Section 1 */}
        <div
          style={{
            background: white,
            borderRadius: 3,
            boxShadow: cardShadow,
            marginBottom: 10
          }}
        >
          <div>
            <ButtonBar
              items={BUTTON_CONFIG}
              activeItem={this.state.activeButton}
              onClick={this.handleActiveButton}
              increaseLastItemWidth
            />
            <HrLine />
            <CardGradientTitle title="Latest News" />
          </div>
          <FeatureArticleCard />
          <HrLine />
          <ArticleCard />
        </div>
        {/* Section 1 closing */}
        {/* Section 2 */}
        <div
          style={{
            background: white,
            borderRadius: 3,
            boxShadow: cardShadow,
            marginBottom: 10
          }}
        >
          <ArticleCard />
        </div>
        {/* Section 2  closing */}

        {/* Section 3 */}
        <div
          style={{
            background: white,
            borderRadius: 3,
            boxShadow: cardShadow,
            marginBottom: 10
          }}
        >
          <CardGradientTitle
            title="Pre-Match Analysis"
            subtitle="See All Articles"
            rootStyles={{
              background: white
            }}
            subtitleStyles={{
              color: orange_10,
              paddingRight: 8
            }}
          />

          <div>
            <FeatureArticleCard />
            <HrLine />
            <FeatureArticleCard />
          </div>
        </div>
        {/* Section 3 closing */}
        {/* Section 4 */}
        <div
          style={{
            background: white,
            borderRadius: 3,
            boxShadow: cardShadow,
            marginBottom: 10
          }}
        >
          <CardGradientTitle
            title="Our Suggestions"
            subtitle="See All Suggestions"
            rootStyles={{
              background: white
            }}
            subtitleStyles={{
              color: orange_10,
              paddingRight: 8
            }}
          />

          <div>
            <FeatureArticleCard />
            <HrLine />
            <FeatureArticleCard />
          </div>
        </div>
        {/* Section 4 closing */}
      </div>
    );
  }
}

export default ArticleListing;
