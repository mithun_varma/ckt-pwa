/**
 *
 * ArticleCard
 *
 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint indent: 0 */
import React, { Component } from "react";
import {
  white,
  red_Orange,
  grey_10,
  grey_8,
  grey_6
} from "../../styleSheet/globalStyle/color";
import moment from "moment";
import Person from "@material-ui/icons/Person";
import Truncate from "react-truncate";

let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  // ClevertapReact.initialize("TEST-Z88-4KR-845Z"); // Old Key
  ClevertapReact.initialize("W88-4KR-845Z");
  // initializeFirebase();
  // askForPermissioToReceiveNotifications();
}
export class ArticleCard extends Component {
  render() {
    const { article, cardType, articleHost, isVideo, rootStyles } = this.props;
    const imageUrl =
      article && article.imageData && article.imageData.thumbnail
        ? article.imageData.thumbnail
        : article.imageData && article.imageData.featureThumbnail
          ? article.imageData.featureThumbnail
          : article.imageData && article.imageData.image
            ? article.imageData.image
            : "https://via.placeholder.com/145";

    return (
      <div
        style={{ padding: 16, display: "flex", flex: 1, ...rootStyles }}
        onClick={e => {
          e.preventDefault();

          ClevertapReact.event("Article", {
            source: "Article",
            articleId: article.id || article._id
          });

          isVideo
            ? this.props.cardOnClick(`/videos/${article.id || article._id}`)
            : this.props.cardOnClick(
              //   `/articles/${cardType}/${article.title
              //     .toLowerCase()
              //     .replace(/ /g, "-")
              //     .replace(/[`~!@#$%^&*()_|+\=?;:'",.]/g, "")}/${article._id}`
              // );
              `/articles/${cardType}/${

              article.id ?
                article.id
                  // .toLowerCase()
                  // .replace(/ /g, "-")
                  // .replace(/[`~!@#$%^&*()_|+\=?;:'",.]/g, "")
                :
                article._id}`
            );
        }}
      >
        <div
          style={{
            flex: 0.4,
            height: 106,
            position: "relative",
            backgroundImage: `url(${imageUrl})`,
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "top center",
            borderRadius: 2
          }}
        >
          {/* <img
            src={require("../../images/articleImage.jpg")}
            alt=""
            style={{
              width: "100%",
              height: "100%",
              borderRadius: 2
            }}
          /> */}
          {isVideo && (
            <img
              src={require("../../images/videoIco.svg")}
              alt=""
              style={{
                position: "absolute",
                top: "35%",
                left: "35%"
              }}
            />
          )}
          {!isVideo &&
            article.matchName && (
              <div
                className="clip-title"
                style={{
                  position: "absolute",
                  bottom: 8,
                  left: 0,
                  color: white,
                  // padding: "2px 10px 2px 16px",
                  padding: "2px 10px 2px 12px",
                  fontFamily: "mont400",
                  // fontWeight: 400,
                  fontSize: 11,
                  background: red_Orange,
                  borderRadius: "0 3px 3px 0",
                  textTransform: "uppercase"
                }}
              >
                {article.matchName}
              </div>
            )}
        </div>
        <div style={{ flex: 0.6, paddingLeft: 8 }}>
          <div
            style={{
              color: grey_10,
              fontFamily: "Montserrat",
              fontWeight: 600,
              fontSize: 12,
              padding: "0px 0 4px 0"
            }}
          >
            <Truncate lines={2} ellipsis={<span>...</span>}>
              {(article && article.title) || "-"}
            </Truncate>
          </div>
          <div
            style={{
              color: grey_10,
              fontFamily: "Montserrat",
              // fontWeight: 700,
              fontSize: 11
            }}
          >
            {/* {(article && article.description) || "-"} */}
            <Truncate lines={2} ellipsis={<span>...</span>}>
              {(article && article.description) || "--"}
            </Truncate>
          </div>
          <div
            style={{
              color: grey_8,
              fontFamily: "Montserrat",
              fontWeight: 500,
              fontSize: 10,
              fontStyle: "italic",
              paddingTop: 4,
              display: "flex"
            }}
          >
            <Person style={{ fontSize: 12, color: grey_6, marginRight: 4 }} />
            {article && article.author}
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <div
              style={{
                color: grey_10,
                fontFamily: "Montserrat",
                fontWeight: 500,
                fontSize: 10
              }}
            >
              <span>
                {(article && moment(article.createdAt).format("DD MMM YYYY")) ||
                  "-"}
              </span>
              <span
                style={{
                  padding: "0 4px"
                }}
              >
                {article && article.readTime ? "•" : ""}
              </span>
              <span
                style={{
                  color: grey_6
                }}
              >
                {article && article.readTime ? article.readTime : ""}
              </span>
            </div>
            {/* <div style={{ display: "flex" }}>
              <Bookmark
                style={{
                  // color: orange_10,
                  color: grey_6,
                  fontSize: 20
                }}
              />
            </div> */}
          </div>
        </div>
      </div>
    );
  }
}

export default ArticleCard;
