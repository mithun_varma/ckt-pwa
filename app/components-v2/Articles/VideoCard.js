import React, { Component } from "react";
import CardGradientTitle from "components-v2/commonComponent/CardGradientTitle";
import { white, cardShadow, grey_10 } from "../../styleSheet/globalStyle/color";
import EmptyState from "../../components-v2/commonComponent/EmptyState";

let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  ClevertapReact.initialize("W88-4KR-845Z");
}

export class VideoCard extends Component {
  render() {
    // console.log(this.props.videos[0].thumbnail);
    // const {
    //   videos,
    //   videosLoading,
    //   similarPlayers,
    //   isNotFeatured
    // } = this.props;
    // console.log(this.props);
    return (
      <div
        style={{
          background: white,
          borderRadius: 3,
          boxShadow: cardShadow,
          marginBottom: 10
        }}
      >
        <CardGradientTitle
          title={this.props.title}
          isRightIcon={this.props.isRightIcon}
          handleCardClick={() => this.props.history.push(`/videos`)}
          rootStyles={{
            background: white,
            borderRadius: 3,
            fontSize: 12
          }}
        />

        <div
          style={{
            padding: 12,
            overflowX: "scroll",
            display: "flex"
          }}
        >
          {this.props.loading && this.props.loading ? (
            <img
              src={require("../../images/CricketLoader.gif")}
              style={{
                width: "100px",
                marginLeft: "35%"
                // marginTop: "10vh"
                // height: 200,
                // background: "#fff"
              }}
            />
          ) : this.props.videos && this.props.videos.length > 0 ? (
            this.props.videos.map((list, key) => (
              <div
                style={{
                  width: 190,
                  padding: "4px 0",
                  marginRight: 12
                }}
                key={`${key + 1}`}
                onClick={() =>{
                  ClevertapReact.event("Video", {
                    source: "Video",
                    articleId: list.id || list._id
                  });
                  this.props.history.push(`/videos/${list.id || list._id}`)
                }
                }
              >
                <div
                  style={{
                    width: 190,
                    height: 110,
                    background: "#eeeeee",
                    backgroundImage: `url('${list.thumbnail ||
                      list.imageData
                        .thumbnail}'),linear-gradient(to right, #a63ba1, rgba(250, 148, 65, 0))`,
                    backgroundPosition: "top center",
                    backgroundSize: "cover",
                    backgroundRepeat: "no-repeat",
                    borderRadius: "3px 3px 0 0"
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      height: "inherit",
                      position: "relative"
                    }}
                  >
                    <img src={require("../../images/videoIco.svg")} />
                    {list.readTime && (
                      <div
                        style={{
                          display: "flex",
                          position: "absolute",
                          bottom: "8px",
                          right: "8px",
                          padding: "4px 8px",
                          background: "rgba(0,0,0,0.5)",
                          borderRadius: "2px",
                          color: "#fff",
                          fontFamily: "mont500"
                        }}
                      >
                        {list.readTime}
                      </div>
                    )}
                  </div>
                </div>
                <div
                  style={{
                    fontFamily: "mont400",
                    color: grey_10,
                    fontSize: 12,
                    marginTop: 12
                  }}
                >
                  {list.title}
                </div>
                {/* <div
                  style={{
                    background: gradientRedNewLeft,
                    padding: 8,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between",
                    borderRadius: "0 0 3px 3px"
                  }}
                >
                  <div
                    style={{
                      fontFamily: "Montserrat",
                      fontWeight: 600,
                      color: white,
                      fontSize: 11,
                      maxWidth: "18ch",
                      whiteSpace: "nowrap"
                    }}
                  >
                    <p>{list && list.title}</p>
                  </div>
                </div> */}
              </div>
            ))
          ) : (
            <EmptyState
              msg="No Videos found"
              rootStyles={{
                height: 170
              }}
            />
          )}
        </div>
      </div>
    );
  }
}

export default VideoCard;
