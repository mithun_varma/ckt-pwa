import React, { Component } from "react";
import { grey_10, white } from "../../styleSheet/globalStyle/color";
import TabBar from "../commonComponent/TabBar";
import ListItems from "../commonComponent/ListItems";
import ButtonBar from "../commonComponent/ButtonBar";

const FORMAT_DATA = ["batting", "bowling"];
const MATCH_TYPE = ["test", "odi", "t20"];
class Records extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeButton: "batting",
      activeTab: "test"
    };
  }

  handleActiveButton = activeButton => {
    this.setState({
      activeButton
    });
  };

  handleActiveTab = activeTab => {
    this.setState({
      activeTab
    });
  };

  render() {
    return (
      <div
        style={{
          padding: 16,
          minHeight: "100vh"
        }}
      >
        {/* Records Title */}
        <div
          style={{
            fontFamily: "Montserrat",
            fontWeight: 700,
            fontSize: 22,
            color: grey_10,
            marginBottom: 12
          }}
        >
          Records
        </div>

        {/* Records Card Section */}
        <div
          style={{
            background: white,
            borderRadius: 3,
            paddingTop: 8
          }}
        >
          <ButtonBar
            items={FORMAT_DATA}
            activeItem={this.state.activeButton}
            onClick={this.handleActiveButton}
          />

          <TabBar
            items={MATCH_TYPE}
            activeItem={this.state.activeTab}
            onClick={this.handleActiveTab}
          />
          {/* <HrLine /> */}

          {ListData.map(item => <ListItems title={item} />)}
        </div>
      </div>
    );
  }
}

export default Records;

const ListData = [
  "Most Runs",
  "Highest Scores",
  "Best Batting Average",
  "Best Batting Strike Rate",
  "Most Hundreds",
  "Most Fifties",
  "Most Fours",
  "Most Sixes"
];
