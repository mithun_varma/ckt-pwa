import React, { Component } from "react";
import {
  grey_10,
  white,
  gradientGrey,
  grey_8,
  cardShadow
} from "../../styleSheet/globalStyle/color";
import HrLine from "../commonComponent/HrLine";
import Loader from "components-v2/commonComponent/Loader";
import EmptyState from "components-v2/commonComponent/EmptyState";

class RecordDetails extends Component {
  render() {
    const { keys, type, data } = this.props;
    return (
      // <div
      //   style={
      //     {
      //       padding: 16,
      //       minHeight: "100vh"
      //     }
      //   }
      // >
      // {/* Records Title */}
      // {/* <div
      //   style={{
      //     fontFamily: "Montserrat",
      //     fontWeight: 700,
      //     fontSize: 22,
      //     color: grey_10,
      //     marginBottom: 12
      //   }}
      // >
      //   Records
      // </div> */}

      // {/* Records Card Section */}
      <div
        style={{
          background: white,
          borderRadius: 3,
          boxShadow: cardShadow
        }}
      >
        {/* header section */}
        <div
          style={{
            display: "flex",
            fontFamily: "Montserrat",
            fontWeight: 400,
            color: grey_8,
            fontSize: 10,
            background: gradientGrey,
            padding: "12px 0 12px 10px",
            alignItems: "center",
            textAlign: "center",
            // textTransform: "uppercase",
            borderRadius: "3px 3px 0 0"
          }}
        >
          <div style={{ flex: 0.4, textAlign: "left" }}>
            {/* Batsmen */}
            {type}
          </div>
          {keys.map((header, index) => (
            <div style={{ flex: 0.15 }}>{header.label}</div>
          ))}
          {/* <div style={{ flex: 0.15 }}>I</div>
            <div style={{ flex: 0.15 }}>R</div>
            <div style={{ flex: 0.15 }}>AVG</div> */}
        </div>
        <HrLine />

        {/* List section */}

        {this.props.loading ? (
          <Loader styles={{ height: "50px" }} noOfLoaders={5} />
        ) : data.length > 0 ? (
          data.map((item, index) => (
            <div>
              <div
                style={{
                  display: "flex",
                  fontFamily: "Montserrat",
                  fontWeight: 400,
                  color: grey_8,
                  fontSize: 12,
                  // background: gradientGrey,
                  padding: "18px 0 18px 10px",
                  alignItems: "center",
                  textAlign: "center",
                  textTransform: "capitalize"
                }}
              >
                <div
                  style={{
                    flex: 0.4,
                    textAlign: "left",
                    color: grey_10,
                    fontWeight: 500
                  }}
                >
                  {item.name || "-"}
                </div>
                {keys.map((value, keyIndex) => (
                  <div style={{ flex: 0.15 }}>
                    {/* {"item[keyIndex].key"} */}
                    {/* {JSON.stringify(item)} */}
                    {item[value.key]}
                  </div>
                ))}
                {/* <div style={{ flex: 0.15 }}>200</div> */}
                {/* <div style={{ flex: 0.15 }}>329</div>
                <div style={{ flex: 0.15 }}>15239</div>
                <div style={{ flex: 0.15 }}>53.89</div> */}
              </div>
              {index !== data.length - 1 && <HrLine />}
            </div>
          ))
        ) : (
          <EmptyState msg="No records found" />
        )}
      </div>
      // {/* </div> */}
    );
  }
}

export default RecordDetails;

const ListData = [1, 2, 3, 4, 54];
