import React from "react";

export class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
    // You can also log the error to an error reporting service
    // console.warn(error, info);
  }

  render() {
    if (this.state.hasError) {
      return (
        <div
          style={{
            padding: "225px 0px"
          }}
        >
          <center>
            <img
              style={{
                width: "70%"
              }}
              src={require("../../images/emptyState.svg")}
            />
          </center>

          <h5
            style={{
              textAlign: "center",
              fontFamily: "Montserrat",
              fontSize: "18px",
              paddingTop: "10px"
            }}
          >
            Oops something went wrong.
          </h5>
        </div>
      );
    }
    return this.props.children;
  }
}
