import React, { Component } from "react";
import BackgroundComponent from "../commonComponent/BackgroundComponent";
import ImageContainer from "../commonComponent/ImageContainer";

export class TeatTeam extends Component {
  render() {
    return (
      <div
        style={{ background: "#edeff4", minHeight: "100vh", paddingBottom: 20 }}
      >
        <BackgroundComponent title="dimmy" />
        <ImageContainer data={popular} isAvatar />
        <ImageContainer
          rootStyles={{
            marginTop: 12
          }}
          data={trending}
        />
      </div>
    );
  }
}

export default TeatTeam;

const popular = {
  header: "Popular Stadiums",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: require("../../images/place_holder_1.png")
    },
    {
      name: "MCG, Sydney",
      img: require("../../images/place_holder_1.png")
    },
    {
      name: "Eden Gardens, Kolkata",
      img: require("../../images/place_holder_1.png")
    },
    {
      name: "MCG, Sydney",
      img: require("../../images/place_holder_1.png")
    },
    {
      name: "Eden Gardens, Kolkata",
      img: require("../../images/place_holder_1.png")
    }
  ]
};
const trending = {
  header: "Trending Stadiums",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    }
  ]
};
