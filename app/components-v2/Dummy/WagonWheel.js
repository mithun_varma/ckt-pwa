import React, { Component } from "react";
import { PathLine } from "react-svg-pathline";

export class WagonWheel extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const wagonPos = {
      // x: 150,
      x: -79.345,
      // y: -160
      y: 156.75
    };
    let { wagon } = this.props;
    if (wagon == null) wagon = wagonPos;
    return (
      <div
        style={{
          padding: 16
        }}
      >
        <div
          style={{
            background: "white",
            borderRadius: 3,
            padding: "14px 0",
            display: "flex",
            justifyContent: "center"
            // position: "relative"
          }}
        >
          <div
            style={{
              width: 203,
              height: 203,
              position: "relative",
              // display: "flex",
              background: "lightgrey",
              borderRadius: "50%"
            }}
          >
            <img src={require("../../images/ground.png")} />
            <svg style={pageStyle.lineStyle}>
              {wagon.y > 0 ? (
                <PathLine
                  points={[
                    { x: 100, y: 85.6 },
                    {
                      x: (wagon.x / 170.0) * 100 + 100,
                      y: 85.6 - (wagon.y / 160) * 85.6
                    }
                  ]}
                  stroke="#4a90e2"
                  strokeWidth="1"
                  fill="none"
                  // r={10}
                />
              ) : (
                <React.Fragment>
                  <PathLine
                    points={[
                      { x: 99, y: 85 },
                      {
                        x: (170 / 170.0) * 100 + 100,
                        y: 85.6 - (0 / 180) * 114
                      }
                    ]}
                    stroke="#4a90e2"
                    strokeWidth="1"
                    fill="none"
                    // r={10}
                  />

                  <PathLine
                    points={[
                      { x: 99, y: 85 },
                      {
                        x: (wagon.x / 170.0) * 100 + 100,
                        y: 85.6 - (wagon.y / 180) * 114
                      }
                    ]}
                    stroke="#4a90e2"
                    strokeWidth="1"
                    fill="none"
                    // r={10}
                  />
                </React.Fragment>
              )}
            </svg>
            <div
              style={{
                width: 4,
                height: 4,
                borderRadius: "50%",
                background: "red",
                position: "absolute",
                top: "43%",
                left: "49.3%"
              }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default WagonWheel;

const pageStyle = {
  lineStyle: {
    position: "absolute",
    left: "1%", //"50%",
    top: "2%", //"43%",
    height: "inherit"
  }
};
