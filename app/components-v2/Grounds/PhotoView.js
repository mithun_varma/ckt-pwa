// import React, { Component } from "react";
// import Slider from "react-slick";
// import Header from "./../commonComponent/Header";
// import { grey_10, white } from "../../styleSheet/globalStyle/color";
// // import Virat from "../../images/kohli.png";

// import Eden from "../../images/eden.jpg";
// import WD from "../../images/wD.jpg";
// import AU from "../../images/AU.jpg";
// import Lords from "../../images/lords.jpg";
// let moments =[];
// export class PhotoView extends Component {
//   render() {
//     const { title, data } = this.props;
//     console.log(this.props);

//      moments = this.props.data && this.props.data.map(m=>{
//       let obj ={};
//       obj.img = m.path;
//       obj.text = m.shortDescription;

//       return obj
//     })
//     // const moments = [
//     //   {
//     //       img: Eden,
//     //       text: 'Dummy text for eden'
//     //   },
//     //   {
//     //       img: WD,
//     //       text: 'Dummy text for Wankhede'
//     //   },
//     //   {
//     //       img: AU,
//     //       text: 'Dummy text AU'
//     //   },
//     //   {
//     //       img: Lords,
//     //       text: 'Dummy text Lords'
//     //   },
//     //   {
//     //       img: Eden,
//     //       text: 'Dummy text 1'
//     //   },
//     //   {
//     //       img: AU,
//     //       text: 'Dummy text o'
//     //   },
//     //   {
//     //       img: WD,
//     //       text: 'Dummy text p'
//     //   },
//     //   {
//     //       img: Lords,
//     //       text: 'Dummy text u'
//     //   },
//     //   {
//     //       img: WD,
//     //       text: 'Dummy text e'
//     //   },
//     //   {
//     //       img: AU,
//     //       text: 'Dummy text z'
//     //   },
     
//     // ]
//     const images = [
//       Eden,
//       WD,
//       Eden,
//       WD,
//       Eden,
//       WD,
//       Eden,
//       WD,
//       Eden,
//       WD,
//       Eden
//     ];
//     const settings = {
//       // customPaging: () => {
//       //   for (let img of moments) {
//       //     return (
//       //       // <a>
//       //       //   <img src={`${img}`} />
//       //       // </a>
//       //       <div
//       //         style={{
//       //           height: "52px",
//       //           width: "52px",
//       //           background: `url(${img.img})`,
//       //           backgroundPosition: "top center",
//       //           backgroundSize: "cover"
//       //         }}
//       //       />
//       //     );
//       //   }
//       // },
//       customPaging: (i) => {
//           return (
//             <div
//               style={{
//                 height: "52px",
//                 width: "52px",
//                 background: `url(${moments[i].img})`,
//                 backgroundPosition: "top center",
//                 backgroundSize: "cover"
//               }}
//             />
//           );
//       },
//       dots: true,
//       dotsClass: "slick-dots slick-thumb",
//       infinite: false,
//       speed: 500,
//       slidesToShow: 1,
//       slidesToScroll: 1,
//       arrows: false,
//       fade: true
//     };
//     return (
//       <div
//         style={{
//           //   padding: "54px 0px 0px 0px",
//           minHeight: "100vh",
//         }}
//       >
//         {/* <Header
//           title={title}
//           isHeaderBackground={true}
//           leftArrowBack
//           textColor={white}
//           backgroundColor={grey_10}
//           history={this.props.history}
//           leftIconOnClick={() => {
//             this.props.toggleView(false);
//           }}
//         /> */}
//         <div
//           style={{
//             display: "flex",
//             width: "100vw",
//             height: "93vh",
//             background: grey_10
//           }}
//         >
//           <div
//             style={{
//               width: "100vw",
//               //   height: "93vh",
//               //   background: grey_10
//               // margin: "auto 10px"
//               // display: 'flex'
//               // height: "80vh",
//               marginTop: "28vh"
//             }}
//           >
//             <Slider {...settings}>
//               {moments &&
//                 moments.map(img => (
//                   <div>
//                     {/* <div
//                       // src={img}
//                       style={{
//                         background: `url(${img})`,
//                         backgroundSize: "cover",
//                         backgroundPosition: "top center",
//                         height: "200px"
//                       }}
//                     /> */}
//                     <img
//                       src={img.img}
//                       width="100%"
//                       height="200px"
//                       style={{ objectFit: "cover" }}
//                     />
//                     <p
//                       style={{
//                         color: white,
//                         fontFamily: "Mont400",
//                         fontSize: "14px",
//                         padding: "8px"
//                       }}
//                     >
//                       {img.text}
//                     </p>
//                   </div>
//                 ))}
//             </Slider>
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

// export default PhotoView;


import React, { Component } from "react";
import Slider from "react-slick";
import Header from "./../commonComponent/Header";
import { grey_10, white } from "../../styleSheet/globalStyle/color";
// import Virat from "../../../images/virat2.png";
// import dummy from "../../../images/dhoni.png";

// import Eden from "../../images/eden.jpg";
// import WD from "../../images/wD.jpg";
// import AU from "../../images/AU.jpg";
// import Lords from "../../images/lords.jpg";

export class PhotoView extends Component {
  render() {
    const { title, data } = this.props;
    const settings = {
      customPaging: i => {
        return (
          // <a>
          //   <img src={`${img}`} />
          // </a>
          <div
            style={{
              height: "52px",
              width: "52px",
              background: `url(${data[i].path})`,
              backgroundPosition: "top center",
              backgroundSize: "cover"
            }}
          />
        );
      },
      dots: true,
      dotsClass: "slick-dots slick-thumb",
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true
    };
    return (
      <div
        style={{
          //   padding: "54px 0px 0px 0px",
          minHeight: "100vh",
          paddingTop: "0"
        }}
      >
        <Header
          title={title}
          isHeaderBackground={true}
          leftArrowBack
          textColor={white}
          backgroundColor={grey_10}
          history={this.props.history}
          leftIconOnClick={() => {
            this.props.toggleView(false);
          }}
        />
        <div
          style={{
            display: "flex",
            width: "100vw",
            height: "93vh",
            background: grey_10
          }}
        >
          <div
            style={{
              width: "100vw",
              //   height: "93vh",
              //   background: grey_10
              // margin: "auto 10px"
              // display: 'flex'
              // height: "80vh",
              marginTop: "20vh"
            }}
          >
            <Slider {...settings}>
              {data &&
                data.map(img => (
                  <div>
                    <div
                      // src={img}
                      style={{
                        background: `url(${img.path})`,
                        backgroundSize: "cover",
                        backgroundPosition: "top center",
                        height: "300px",
                        backgroundRepeat: 'no-repeat'
                      }}
                    />
                    {/* <img
                      src={img.path}
                      width="100%"
                      style={{ objectFit: "cover" }}
                    /> */}
                    <p
                      style={{
                        color: white,
                        fontFamily: "Mont400",
                        fontSize: "14px",
                        padding: "8px"
                      }}
                    >
                      {img.shortDescription}
                    </p>
                  </div>
                ))}
            </Slider>
          </div>
        </div>
      </div>
    );
  }
}

export default PhotoView;
