import React from "react";
import HrLine from "../commonComponent/HrLine";

import {
  white
} from "../../styleSheet/globalStyle/color";

let momentTumbNails = [];
class PlayerMoments extends React.Component {

  componentDidMount() {
    momentTumbNails =this.props.moments && this.props.moments.map(m=>{
      return m.path
    })
  }
  render() {
    return (
      <div style={pageStyle.container}>
        <div style={pageStyle.header}>Magical Moments</div>
        <HrLine />
        <div
          style={{
            backgroundImage: `linear-gradient(to right, #a63ba1, rgba(250, 148, 65, 0)), url(${momentTumbNails[0]})`,
            minHeight: "150px",
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "top center",
            backgroundBlendMode: "lighten",
            marginTop: "16px"
          }}
          onClick={this.props.handleImageClick}
        />
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginTop: "8px"
          }}
        >
          <div
            style={{
              ...pageStyle.smallImg,
              backgroundImage: `linear-gradient(to right, #28aef4, rgba(250, 148, 65, 0)), url(${momentTumbNails[1]})`
            }}
          />
          <div
            style={{
              ...pageStyle.smallImg,
              backgroundImage: `linear-gradient(to right, #459548, rgba(250, 148, 65, 0)), url(${momentTumbNails[2]})`
            }}
          />
          <div
            style={{
              ...pageStyle.smallImg,
              backgroundImage: `linear-gradient(to right, #459548, rgba(250, 148, 65, 0)), url(${momentTumbNails[3]})`,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              fontSize: "24px",
              color: white
            }}
          >
            {/* +10 */}
            {this.props.moments.length > 4 ? "+"+(this.props.moments.length - 4) : ""}
          </div>
        </div>
      </div>
    );
  }
}

const pageStyle = {
  container: {
    // marginTop:'12px',
    display: "flex",
    flexDirection: "column",
    flex: 1,
    padding: "16px",
    fontSize: "12px",
    // width:screenWidth*0.911,
    // marginLeft:screenWidth*0.044,
    backgroundColor: "#fff",
    fontFamily: "Montserrat"
  },
  header: {
    display: "flex",
    fontWeight: "500",
    paddingBottom: "16px"
  },
  smallImg: {
    minHeight: "100px",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
    backgroundBlendMode: "lighten",
    width: "32%"
  }
};

export default PlayerMoments;
