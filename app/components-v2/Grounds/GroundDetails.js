import React, { Component } from "react";
import { hrLineStyle } from "config/styleConfig";
import CircularProgressbar from "react-circular-progressbar";
import moment from "moment";
import PlayerMoments from "./PlayerMoments";
import { AUTHOR_DATE_FORMAT_NAME } from "utils/constants";
import "../../../node_modules/react-circular-progressbar/dist/styles.css";
import PhotoView from "./PhotoView";
import { Helmet } from "react-helmet";
import {
  grey_10,
  white,
  orange_10,
  grey_1,
  grey_6,
  cardShadow
} from "../../styleSheet/globalStyle/color";
import SeparatedListItem from "../commonComponent/SeparatedListItem";
import TabBar from "../commonComponent/TabBar";
import ButtonBar from "../commonComponent/ButtonBar";

import Loader from "components-v2/commonComponent/Loader";

const FORMAT_TABS = ["INTERNATIONAL", "IPL"];
const MATCH_TYPE = ["TEST", "ODI", "T20"];
class GroundDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "INTERNATIONAL",
      activeButton: "ODI",
      showPhoto: false,
      showButtonTab: true
    };
  }

  handleActiveTab = activeTab => {
    this.setState(
      {
        activeTab,
        activeButton:
          activeTab == "INTERNATIONAL" ? MATCH_TYPE[0] : this.state.activeButton
      },
      () => {
        if (activeTab === "INTERNATIONAL") {
          this.setState({
            showButtonTab: true
          });
        } else if (activeTab === "IPL") {
          this.setState(
            {
              showButtonTab: false,
              activeButton: "IPL"
            },
            () => {}
          );
        }
      }
    );
  };

  handleActiveButton = activeButton => {
    if (this.state.activeTab !== "IPL") {
      this.setState({
        activeButton
      });
    }
  };

  handleStringToNumber = value => {
    let res = Number(value.split("%")[0]);
    return res;
  };

  componentDidMount() {
    this.props.fetchGroundMagicMoments({
      venueId: this.props.id
    });
  }

  handleImageClick = val => {
    this.props.toggleHeader();
    this.setState({
      showPhoto: val
    });
  };

  handleValidateFields = (data, type) => {
    if (
      data &&
      data[this.state.activeTab] &&
      data[this.state.activeTab][this.state.activeButton] &&
      data[this.state.activeTab][this.state.activeButton][type]
    ) {
      return data[this.state.activeTab][this.state.activeButton][type];
    } else {
      return false;
    }
  };

  render() {
    const {
      grounds,
      grounds: {
        groundDetails,
        groundDetails: { competition }
      }
    } = this.props;

    return this.state.showPhoto ? (
      <PhotoView
        toggleView={val => this.handleImageClick(val)}
        title="magical moments"
        data={this.props.grounds.moments}
      />
    ) : (
      <div
      // style={{ background: "#edeff4", minHeight: "100vh", paddingBottom: 20 }}
      >
        <Helmet titleTemplate="%s | cricket.com">
          <title>{`${groundDetails && groundDetails.name} - ${groundDetails &&
            groundDetails.city}, ${groundDetails &&
            groundDetails.country &&
            groundDetails.country} | Matches and Stats`}</title>
          <meta
            name="description"
            content={`${groundDetails && groundDetails.name} is a cricket ground, located in ${groundDetails && groundDetails.city}, ${groundDetails && groundDetails.country}. Check out the highest, lowest, or the average score and many more stats of this stadium.`}
          />
          {/* <meta
            name="keywords"
            content={(article.articleDetails && article.articleDetails.seoKeywords) || ''}
          /> */}
          <link
            rel="canonical"
            href={`www.cricket.com${history &&
              history.location &&
              history.location.pathname}`}
          />
        </Helmet>

        <div style={pageStyle.profileImageContainer}>
          <img
            src={
              groundDetails.image || "https://via.placeholder.com/328x203.png"
            }
            alt="Ground Image"
            style={pageStyle.profileImageStyle}
          />
        </div>
        {/* Stadium Detail Card */}
        <div style={pageStyle.cardContainer}>
          <div style={pageStyle.cardTitleStyle}>Stadium Details</div>
          <div style={hrLineStyle} />
          {grounds.groundDetailsLoading ? (
            <div
              style={{
                paddingBottom: 2
              }}
            >
              <Loader styles={{ height: "194px" }} noOfLoaders={1} />
            </div>
          ) : (
            <div>
              <SeparatedListItem
                lable="City"
                value={grounds.groundDetails.city || "-"}
              />
              <div style={hrLineStyle} />
              <SeparatedListItem
                lable="Capacity"
                value={grounds.groundDetails.capacity || "-"}
              />
              <div style={hrLineStyle} />
              <SeparatedListItem
                lable="Team’s Homeground"
                value={
                  grounds.groundDetails.teamHomeGround
                    ? grounds.groundDetails.teamHomeGround.join(", ")
                    : "-"
                }
              />
              <div style={hrLineStyle} />
              <SeparatedListItem
                lable="Player’s Homeground "
                value={
                  grounds.groundDetails.playerHomeGround
                    ? grounds.groundDetails.playerHomeGround.join(", ")
                    : "-"
                }
              />
              <div style={hrLineStyle} />
              <SeparatedListItem
                lable="Bowling Ends"
                value={grounds.groundDetails.bowlingEnds || "-"}
              />
            </div>
          )}
        </div>
        {/* Stadium Detail Card */}

        {/* Match format card */}
        <div
          style={{
            margin: "12px 16px",
            borderRadius: 3,
            background: white,
            boxShadow: cardShadow
          }}
        >
          <TabBar
            items={FORMAT_TABS}
            activeItem={this.state.activeTab}
            onClick={this.handleActiveTab}
          />
          {/* <div style={hrLineStyle} /> */}
          {this.state.showButtonTab === true ? (
            <ButtonBar
              items={MATCH_TYPE}
              activeItem={this.state.activeButton}
              onClick={this.handleActiveButton}
            />
          ) : null}

          <div style={hrLineStyle} />
          {grounds.groundDetailsLoading ? (
            <div
              style={{
                padding: 12
              }}
            >
              <Loader styles={{ height: "64px" }} noOfLoaders={4} />
            </div>
          ) : (
            <div>
              <div
                style={{
                  display: "flex",
                  flex: 1,
                  flexDirection: "column",
                  padding: "0 16px",
                  fontSize: 12,
                  color: grey_10,
                  fontFamily: "Montserrat",
                  fontWeight: 400
                }}
              >
                <SeparatedListItem
                  rootStyles={{ padding: "4px 0" }}
                  // lable="1st ODI"
                  lable={`1st ${this.state.activeButton}`}
                  // value={   groundDetails.venueStats && groundDetails.venueStats.venueStat[this.state.activeButton].istMatch.matchName}
                  value={
                    groundDetails.venueStats &&
                    groundDetails.venueStats.venueStat[
                      this.state.activeButton
                    ] &&
                    groundDetails.venueStats.venueStat[this.state.activeButton]
                      .istMatch.startDate &&
                    moment(
                      groundDetails.venueStats &&
                        groundDetails.venueStats.venueStat[
                          this.state.activeButton
                        ] &&
                        groundDetails.venueStats.venueStat[
                          this.state.activeButton
                        ].istMatch.startDate
                    ).format(AUTHOR_DATE_FORMAT_NAME)
                  }
                />
                <SeparatedListItem
                  lable={
                    groundDetails.venueStats &&
                    groundDetails.venueStats.venueStat[
                      this.state.activeButton
                    ] &&
                    groundDetails.venueStats.venueStat[this.state.activeButton]
                      .istMatch.matchName
                  }
                  value={
                    groundDetails.venueStats &&
                    groundDetails.venueStats.venueStat[
                      this.state.activeButton
                    ] &&
                    groundDetails.venueStats.venueStat[this.state.activeButton]
                      .istMatch.matchStatusStr
                  }
                  rootStyles={{ padding: "4px 0" }}
                  labelStyles={{
                    color: grey_10
                  }}
                />
                <div style={hrLineStyle} />
              </div>
              <div
                style={{
                  display: "flex",
                  flex: 1,
                  flexDirection: "column",
                  padding: "0 16px",
                  fontSize: 12,
                  color: grey_10,
                  fontFamily: "Montserrat",
                  fontWeight: 400
                }}
              >
                <SeparatedListItem
                  rootStyles={{ padding: "4px 0" }}
                  lable={`Recent ${this.state.activeButton}`}
                  value={
                    groundDetails.venueStats &&
                    groundDetails.venueStats.venueStat[
                      this.state.activeButton
                    ] &&
                    groundDetails.venueStats.venueStat[this.state.activeButton]
                      .lastMatch.startDate &&
                    moment(
                      groundDetails.venueStats &&
                        groundDetails.venueStats.venueStat[
                          this.state.activeButton
                        ] &&
                        groundDetails.venueStats.venueStat[
                          this.state.activeButton
                        ].lastMatch.startDate
                    ).format(AUTHOR_DATE_FORMAT_NAME)
                  }
                />
                <SeparatedListItem
                  lable={
                    groundDetails.venueStats &&
                    groundDetails.venueStats.venueStat[
                      this.state.activeButton
                    ] &&
                    groundDetails.venueStats.venueStat[this.state.activeButton]
                      .lastMatch.matchName
                  }
                  value={
                    groundDetails.venueStats &&
                    groundDetails.venueStats.venueStat[
                      this.state.activeButton
                    ] &&
                    groundDetails.venueStats.venueStat[this.state.activeButton]
                      .lastMatch.matchStatusStr
                  }
                  rootStyles={{ padding: "4px 0" }}
                  labelStyles={{
                    color: grey_10
                  }}
                />
                <div style={hrLineStyle} />
              </div>

              {/* Chart Section */}
              <div
                style={{
                  position: "relative",
                  display: "flex",
                  flex: 1,
                  padding: "12px 0",
                  background: `linear-gradient(to right, ${white}, ${grey_1}, ${grey_1}, ${white})`
                }}
              >
                <div
                  style={{
                    position: "absolute",
                    width: 1,
                    height: "54%",
                    background: grey_6,
                    opacity: 0.4,
                    top: "28%",
                    left: "50%"
                  }}
                />
                <div
                  style={{
                    flex: 0.5,
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    color: grey_10
                  }}
                >
                  <div
                    style={{
                      width: "48%",
                      height: 100,
                      textAlign: "center",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "space-around"
                    }}
                  >
                    <div
                      style={{
                        fontFamily: "Montserrat",
                        fontWeight: 400,
                        fontSize: 12,
                        color: grey_10
                      }}
                    >
                      Average 1st Innings Score
                    </div>
                    <div
                      style={{
                        fontFamily: "Oswald-SemiBold",
                        fontSize: 26,
                        height: 60,
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      {groundDetails.venueStats &&
                        groundDetails.venueStats.venueStat[
                          this.state.activeButton
                        ] &&
                        groundDetails.venueStats.venueStat[
                          this.state.activeButton
                        ].avgIstInningScore}
                      {/* {this.handleValidateFields(
                        competition,
                        "averageScoreFirstInning"
                      ) || "-"} */}
                    </div>
                  </div>
                </div>
                <div
                  style={{
                    flex: 0.5,
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    color: grey_10
                  }}
                >
                  <div
                    style={{
                      width: "64%",
                      height: 100,
                      textAlign: "center",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "space-around",
                      alignItems: "center"
                    }}
                  >
                    <div
                      style={{
                        fontFamily: "Montserrat",
                        fontWeight: 400,
                        fontSize: 12,
                        color: grey_10
                      }}
                    >
                      Team Batting First Winning %
                    </div>
                    <div
                      style={{
                        width: 54,
                        height: 60
                      }}
                    >
                      <CircularProgressbar
                        className="stadiumCircularProgressBar"
                        percentage={
                          groundDetails.venueStats &&
                          groundDetails.venueStats.venueStat[
                            this.state.activeButton
                          ] &&
                          groundDetails.venueStats.venueStat[
                            this.state.activeButton
                          ].teamBatFirstWin
                        }
                        text={ groundDetails.venueStats &&
                          groundDetails.venueStats.venueStat[
                            this.state.activeButton
                          ] &&
                          groundDetails.venueStats.venueStat[
                            this.state.activeButton
                          ].teamBatFirstWin}
                        styles={{
                          path: { stroke: `rgba(622, 52, 129, ${15 / 100})` },
                          text: { fill: "#dadada", fontSize: "16px" }
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>

              {/* Item Section with Arrow */}
              <div style={hrLineStyle} />

              <div
                style={{
                  background: `linear-gradient(to right, ${white}, ${grey_1}, ${grey_1}, ${white})`
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flex: 1,
                    flexDirection: "column",
                    padding: "0 16px",
                    fontSize: 12,
                    color: grey_10,
                    fontFamily: "Montserrat",
                    fontWeight: 400,
                    position: "relative"
                  }}
                >
                  <div
                    style={{
                      position: "absolute",
                      top: "34%",
                      right: 16
                    }}
                  >
                    <img
                      src={require("../../images/right_arrow.svg")}
                      alt="right arrow"
                      style={{
                        width: 8,
                        height: 12
                      }}
                    />
                  </div>

                  <SeparatedListItem
                    lable="Highest Team Score"
                    value={
                      groundDetails.venueStats &&
                      groundDetails.venueStats.venueStat[
                        this.state.activeButton
                      ] &&
                      groundDetails.venueStats.venueStat[
                        this.state.activeButton
                      ].highestScoreMatch.startDate &&
                      moment(
                        groundDetails.venueStats &&
                          groundDetails.venueStats.venueStat[
                            this.state.activeButton
                          ] &&
                          groundDetails.venueStats.venueStat[
                            this.state.activeButton
                          ].highestScoreMatch.startDate
                      ).format(AUTHOR_DATE_FORMAT_NAME)
                    }
                    // value={   groundDetails.venueStats && groundDetails.venueStats.venueStat[this.state.activeButton].highestScoreMatch}
                    rootStyles={{ padding: "4px 0" }}
                  />
                  <SeparatedListItem
                    lable={
                      groundDetails.venueStats &&
                      groundDetails.venueStats.venueStat[
                        this.state.activeButton
                      ] &&
                      groundDetails.venueStats.venueStat[
                        this.state.activeButton
                      ].highestScoreMatch.matchName
                    }
                    value={
                      groundDetails.venueStats &&
                      groundDetails.venueStats.venueStat[
                        this.state.activeButton
                      ] &&
                      groundDetails.venueStats.venueStat[
                        this.state.activeButton
                      ].highestScoreMatch.scoreStr
                    }
                    rootStyles={{ padding: "4px 0" }}
                    labelStyles={{
                      color: grey_10
                    }}
                  />
                  <div style={hrLineStyle} />
                </div>
              </div>

              <div
                style={{
                  background: `linear-gradient(to right, ${white}, ${grey_1}, ${grey_1}, ${white})`
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flex: 1,
                    flexDirection: "column",
                    padding: "0 16px",
                    fontSize: 12,
                    color: grey_10,
                    fontFamily: "Montserrat",
                    fontWeight: 400,
                    position: "relative"
                  }}
                >
                  <div
                    style={{
                      position: "absolute",
                      top: "34%",
                      right: 16
                    }}
                  >
                    <img
                      src={require("../../images/right_arrow.svg")}
                      alt="right arrow"
                      style={{
                        width: 8,
                        height: 12
                      }}
                    />
                  </div>

                  <SeparatedListItem
                    lable="Lowest Team Score"
                    value={
                      groundDetails.venueStats &&
                      groundDetails.venueStats.venueStat[
                        this.state.activeButton
                      ] &&
                      groundDetails.venueStats.venueStat[
                        this.state.activeButton
                      ].lowestScoreMatch.startDate &&
                      moment(
                        groundDetails.venueStats &&
                          groundDetails.venueStats.venueStat[
                            this.state.activeButton
                          ] &&
                          groundDetails.venueStats.venueStat[
                            this.state.activeButton
                          ].lowestScoreMatch.startDate
                      ).format(AUTHOR_DATE_FORMAT_NAME)
                    }
                    // value={   groundDetails.venueStats && groundDetails.venueStats.venueStat[this.state.activeButton].lowestScoreMatch}
                    rootStyles={{ padding: "4px 0" }}
                  />
                  <SeparatedListItem
                    lable={
                      groundDetails.venueStats &&
                      groundDetails.venueStats.venueStat[
                        this.state.activeButton
                      ] &&
                      groundDetails.venueStats.venueStat[
                        this.state.activeButton
                      ].lowestScoreMatch.matchName
                    }
                    value={
                      groundDetails.venueStats &&
                      groundDetails.venueStats.venueStat[
                        this.state.activeButton
                      ] &&
                      groundDetails.venueStats.venueStat[
                        this.state.activeButton
                      ].lowestScoreMatch.scoreStr
                    }
                    rootStyles={{ padding: "4px 0" }}
                    labelStyles={{
                      color: grey_10
                    }}
                  />
                  <div style={hrLineStyle} />
                </div>
              </div>
            </div>
          )}
        </div>
        {/* Closing */}

        {/* View Similar Stadiums Section */}
        {/* <GroundsImageContainer header={"View Similar Stadiums"} isCompare /> */}
        {/* closing */}

        {/* Historic Moments at Eden Gardens */}
        {/* <GroundsImageContainer
            header={"Historic Moments at Eden Gardens"}
            isMoments
          /> */}
        { this.props.grounds.moments && 
          this.props.grounds.moments.length > 0 ? 
          <div style={{ marginTop: "12px", padding: "12px" }}>
            <PlayerMoments
              // handleImageClick={() => this.props.history.push("/photoview")}
              handleImageClick={() => this.handleImageClick(true)}
              show={this.props.show}
              moments={
                this.props.grounds.moments.length > 0
                  ? this.props.grounds.moments
                  : []
              }
            />
          </div>
        : "" }
        {/* closing */}
      </div>
    );
  }
}

export default GroundDetails;

const pageStyle = {
  profileImageContainer: {
    margin: "0 16px",
    marginTop: -124,
    borderRadius: 3,
    background: "lightgrey",
    height: 203,
    boxShadow: cardShadow
  },
  profileImageStyle: {
    width: "100%",
    height: "100%",
    borderRadius: 3
  },
  cardContainer: {
    background: white,
    margin: "10px 16px 0",
    borderRadius: 3,
    padding: "0 16px",
    boxShadow: cardShadow
  },
  cardTitleStyle: {
    padding: "12px 0px",
    background: white,
    fontFamily: "Montserrat",
    fontWeight: 600,
    fontSize: 12,
    color: grey_10
  },
  listItemSingle: {
    display: "flex",
    flex: 1,
    padding: "12px 0",
    fontSize: 12,
    fontFamily: "Montserrat",
    fontWeight: 600
  },
  listItemSingleLable: {
    display: "flex",
    flex: 0.5,
    color: orange_10
  },
  listItemSingleValue: {
    display: "flex",
    flex: 0.5,
    paddingLeft: 10,
    color: grey_10
  }
};
