/**
 *
 * GroundsDiscovery
 *
 */

import React from "react";
import BackgroundComponent from "../commonComponent/BackgroundComponent";
import ImageContainer from "../commonComponent/ImageContainer";

/* eslint-disable react/prefer-stateless-function */
class GroundsDiscovery extends React.Component {
  render() {
    return (
      <div
        style={{ background: "#edeff4", minHeight: "100vh", paddingBottom: 20 }}
      >
        <BackgroundComponent title={"Stadiums"} />
        <ImageContainer data={popular} isAvatar />
        <ImageContainer
          rootStyles={{
            marginTop: 12
          }}
          data={trending}
        />
        <ImageContainer
          rootStyles={{
            marginTop: 12
          }}
          data={similarGrounds}
          // poweredStadium
        />
        <ImageContainer
          rootStyles={{
            marginTop: 12
          }}
          data={mostMatches}
        />
        <ImageContainer
          rootStyles={{
            marginTop: 12
          }}
          data={highestBatting}
        />
        <ImageContainer
          rootStyles={{
            marginTop: 12
          }}
          data={highestBowling}
        />
        <ImageContainer
          rootStyles={{
            marginTop: 12
          }}
          data={favouriteStatium}
        />
      </div>
    );
  }
}

GroundsDiscovery.propTypes = {};

export default GroundsDiscovery;

const popular = {
  header: "Popular Stadiums",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: require("../../images/place_holder_1.png")
    },
    {
      name: "MCG, Sydney",
      img: require("../../images/place_holder_1.png")
    },
    {
      name: "Eden Gardens, Kolkata",
      img: require("../../images/place_holder_1.png")
    },
    {
      name: "MCG, Sydney",
      img: require("../../images/place_holder_1.png")
    },
    {
      name: "Eden Gardens, Kolkata",
      img: require("../../images/place_holder_1.png")
    }
  ]
};

const trending = {
  header: "Trending Stadiums",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    }
  ]
};
const similarGrounds = {
  header: "Stadiums Similar to Eden Gardens",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    }
  ]
};

const mostMatches = {
  header: "Stadium with Most Matches",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    }
  ]
};

const highestBatting = {
  header: "Stadiums with Highest Batting S/R",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    }
  ]
};

const highestBowling = {
  header: "Stadiums with Highest Bowling S/R",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    }
  ]
};

const favouriteStatium = {
  header: "Virat favourite Stadiums",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    }
  ]
};
