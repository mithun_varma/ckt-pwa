import React from "react";

class TestChart extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      divWidth: 343
    };
    // this.chartDiv = React.createRef();
  }
  refCallback = element => {
    if (element) {
      if (element.getBoundingClientRect().width !== this.state.divWidth) {
        this.setState({
          divWidth: element.getBoundingClientRect().width
        });
      }
    }
  };

  getTotal = (data, type) => {
    let total = 0;
    for (let over of data) {
      if (type === "run") {
        total += +over.runs;
      } else {
        total += +over.wickets;
      }
    }
    return total;
  };

  render() {
    const { data, scoreCard } = this.props;
    return data ? (
      <React.Fragment>
        <div
          style={{
            position: "relative"
          }}
        >
          <div
            ref={this.refCallback}
            style={{
              display: "flex",
              alignItems: "flex-end",
              marginTop: 8
            }}
          >
            {data.map((arr, key) => (
              <div>
                {+arr.wickets !== 0 &&
                  [...Array(+arr.wickets)].map(() => (
                    <div
                      style={{
                        display: "block",
                        background: "#d23e2f",
                        borderRadius: "50%",
                        width: 5,
                        height: 5,
                        marginTop: "2px",
                        marginLeft: "auto",
                        marginRight: "auto",
                        marginBottom: 2
                      }}
                    />
                  ))}
                <div
                  style={{
                    width:
                      (this.state.divWidth - (data.length - 1)) / data.length,
                    height: +arr.runs * 10,
                    marginRight: key !== data.length - 1 ? 1 : 0,
                    background:
                      arr.teamId === scoreCard.firstBatting
                        ? "#d9c5cc"
                        : "#753e5c"
                  }}
                />
              </div>
            ))}
          </div>
          <div
            style={{
              height: 1,
              width: this.state.divWidth,
              borderBottom: "1px dashed #a9b3d0",
              position: "absolute",
              // bottom: 0
              bottom: 32 + (this.getTotal([...data], "run") / data.length) * 10
            }}
          />
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              padding: "8px 20px",
              color: "#fff",
              fontSize: 11,
              fontWeight: 600,
              background: "#172132"
            }}
          >
            <div>
              <p>
                <span style={{ opacity: 0.6 }}>Overs</span> {data.length}
              </p>
            </div>
            <div>
              <p>
                <span style={{ opacity: 0.6 }}>Runs</span>{" "}
                {this.getTotal([...data], "run")}
              </p>
            </div>
            <div>
              <p>
                <span style={{ opacity: 0.6 }}>Wickets</span>{" "}
                {this.getTotal([...data])}
              </p>
            </div>
          </div>
        </div>
      </React.Fragment>
    ) : (
      ""
    );
  }
}

export default TestChart;
