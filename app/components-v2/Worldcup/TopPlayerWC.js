import React from "react";

class TopPlayerWC extends React.PureComponent {
  render() {
    const { playerData } = this.props;
    return (
      <div
        style={{
          display: "flex",
          alignItems: "flex-end",
          flex: 1,
          fontFamily: "Montserrat",
          background: `linear-gradient(to right, #172132, #172132), url(${require("../../images/cricket_bg.png")})`,
          backgroundPosition: "top left",
          backgroundSize: "contain",
          backgroundRepeat: "no-repeat",
          backgroundBlendMode: "color",
          minHeight: playerData.avatar ? 171 : 110,
          //   background: `#172132`,
          // backgroundImage: `url(${require("../../images/cricket_bg.svg")})`,
          // background: `linear-gradient(to right, #172132, #172132), url(${require("../../images/cricket_bg.png")})`,
          color: "#fff",
          padding: "0px 16px 0px 8px"
        }}
      >
        {playerData &&
          playerData.avatar && (
            <div
              style={{
                flex: 0.4,
                marginBottom: 22,
                height: 130,
                backgroundImage: `url(${playerData.avatar || ""})`,
                backgroundSize: "cover",
                backgroundPosition: "top left"
              }}
            >
              {/* <img
                style={{
                  height: "inherit",
                  objectFit: "cover",
                  overflow: "hidden"
                  // width: 100
                }}
                src={(playerData && playerData.avatar) || ""}
              /> */}
            </div>
          )}

        <div
          style={{
            flex: 0.6,
            marginBottom: 32
          }}
        >
          <div
            style={{
              display: "flex",
              alignItems: "center",
              marginBottom: 8
            }}
          >
            <img
              style={{
                width: 20,
                height: 12
              }}
              src={playerData.teamAvatar || require("../../images/flag_empty.svg")}
            />
            <p style={{ marginLeft: 8 }}>
              <span style={{ fontWeight: 600 }}>
                {playerData.shortName.split(" ")[0]}
              </span>
              <span style={{ marginLeft: 2 }}>
                {playerData.shortName.split(" ")[1]}{" "}
                {playerData.shortName.split(" ").length > 2 &&
                  playerData.shortName.split(" ")[2]}
              </span>
            </p>
          </div>
          <div>
            <div
              style={{
                border: "1px solid #4a536c",
                borderRadius: 2
              }}
            >
              {playerData.battingStatistics[0].balls ? (
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    borderBottom: playerData.bowlingStatistics[0].balls
                      ? "1px solid #4a536c"
                      : ""
                  }}
                >
                  <p
                    style={{
                      padding: "4px 12px",
                      background: "#394360",
                      minWidth: "45%",
                      fontSize: 16,
                      fontWeight: 600,
                      textAlign: "center"
                    }}
                  >
                    <span>
                      {playerData.battingStatistics[0].runs}
                      {!playerData.battingStatistics[0].dismissed && "*"}
                      <span
                        style={{
                          fontSize: 11,
                          fontWeight: 400,
                          paddingLeft: 4
                        }}
                      >
                        ({playerData.battingStatistics[0].balls})
                      </span>
                    </span>
                  </p>
                  <p
                    style={{
                      fontSize: 11
                    }}
                  >
                    <span
                      style={{
                        padding: "4px 12px",
                        borderRight: "1px solid #798198"
                      }}
                    >
                      <span style={{ opacity: 0.9 }}>4s </span>
                      <span style={{ fontWeight: 600 }}>
                        {playerData.battingStatistics[0].fours}
                      </span>
                    </span>
                    <span
                      style={{
                        padding: "4px 12px"
                      }}
                    >
                      <span style={{ opacity: 0.9 }}>6s </span>
                      <span style={{ fontWeight: 600 }}>
                        {playerData.battingStatistics[0].sixes}
                      </span>
                    </span>
                  </p>
                </div>
              ): ""}
              {playerData.bowlingStatistics[0].balls ? (
                <div
                  style={{
                    display: "flex",
                    alignItems: "center"
                  }}
                >
                  <p
                    style={{
                      padding: "4px 12px",
                      background: "#394360",
                      minWidth: "45%",
                      textAlign: "center",
                      fontSize: 16
                    }}
                  >
                    <span style={{ fontWeight: 600 }}>
                      {`${playerData.bowlingStatistics[0].wickets}/${
                        playerData.bowlingStatistics[0].runs
                      }`}
                      <span
                        style={{
                          fontSize: 11,
                          fontWeight: 400,
                          paddingLeft: 4
                        }}
                      >
                        ({playerData.bowlingStatistics[0].overs})
                      </span>
                    </span>
                  </p>
                  <p
                    style={{
                      padding: "4px 12px",
                      fontSize: 11
                    }}
                  >
                    <span style={{ opacity: 0.9 }}>Eco </span>
                    <span style={{ fontWeight: 600 }}>
                      {(+playerData.bowlingStatistics[0].economy).toFixed(2)}
                    </span>
                  </p>
                </div>
              ): ""}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TopPlayerWC;
