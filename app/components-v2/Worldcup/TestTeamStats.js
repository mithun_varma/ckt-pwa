import React, { Component } from "react";
import {
  grey_8,
  orange_8,
  grey_5,
  grey_5_1,
  green_10,
  red_10,
  cardShadow,
  avatarShadow
} from "../../styleSheet/globalStyle/color";

export default class TestTeamStas extends Component {
  getCurrentPrediction = inningId => {
    switch (inningId) {
      case 1:
        return "predictedScore";
      case 2:
        return "secondPredictedScore";
      case 3:
        return "thirdPredictedScore";
      case 4:
        return "fourthPredictedScore";
    }
  };

  getCurrentPredictWicket = inningId => {
    switch (inningId) {
      case 1:
        return "predictedWicket";
      case 2:
        return "secondPredictedWicket";
      case 3:
        return "thirdPredictedWicket";
      case 4:
        return "fourthPredictedWicket";
    }
  };
  render() {
    const { data, scoreCard } = this.props;
    // console.log(data, "live");
    if (!data || !scoreCard) {
      return null;
    }
    const teams = [
      scoreCard.currentScoreCard.battingTeam === scoreCard.teamA.teamKey
        ? scoreCard.teamA
        : scoreCard.teamB,
      scoreCard.currentScoreCard.battingTeam !== scoreCard.teamA.teamKey
        ? scoreCard.teamA
        : scoreCard.teamB
    ];
    return (
      <div>
        <div
          style={{
            display: "flex",
            flex: 1,
            // alignItems: "center",
            justifyContent: "space-between",
            padding: "14px 16px"
          }}
        >
          {/* 1 */}
          <div
            style={{
              textAlign: "left",
              flex: 0.2,
              paddingTop: 5
            }}
          >
            <div
              style={{
                fontFamily: "Oswald",
                fontWeight: 400,
                fontSize: 20
              }}
            >
              {`${scoreCard && scoreCard.currentScoreCard.runs}${`/${
                scoreCard.currentScoreCard.wickets
              }`}`}
            </div>
            <div
              style={{
                fontSize: 12,
                color: grey_8,
                minHeight: 18
              }}
            >
              {`(${scoreCard && scoreCard.currentScoreCard.overs})`}
            </div>
          </div>

          {/* 2 */}
          <div
            style={{
              display: "flex",
              flex: 0.6,
              flexDirection: "column",
              alignItems: "center",
              position: "relative"
            }}
          >
            <div
              style={{
                position: "absolute",
                display: "flex",
                left: 0,
                right: 0,
                top: "26%",
                justifyContent: "space-between"
              }}
            >
              <div
                style={{
                  width: 6,
                  height: 6,
                  borderRadius: "50%",
                  background: orange_8
                }}
              />
              <div
                style={{
                  width: "90%",
                  borderTop: `1px dashed ${grey_5}`,
                  marginTop: 3
                }}
              />
              <div
                style={{
                  width: 6,
                  height: 6,
                  borderRadius: "50%",
                  background: green_10
                }}
              />
            </div>
            <div
              style={{
                width: 34,
                height: 24,
                marginTop: 7,
                zIndex: 1
              }}
            >
              <img
                src={teams[0].avatar}
                alt=""
                style={{
                  width: "100%",
                  height: "auto",
                  boxShadow: avatarShadow
                }}
              />
            </div>
            <div
              style={{
                fontWeight: 600,
                fontSize: 14,
                marginTop: 10
              }}
            >
              {teams[0].shortName}
            </div>
          </div>

          {/* 3 */}
          <div
            style={{
              textAlign: "right",
              flex: 0.2,
              paddingTop: 5
            }}
          >
            <div
              style={{
                fontFamily: "Oswald",
                fontWeight: 400,
                fontSize: 20,
                color: red_10
              }}
            >
              {`${
                data[data.length - 1][
                  this.getCurrentPrediction(data[data.length - 1].inningNo)
                ]
              }/${
                data[data.length - 1][
                  this.getCurrentPredictWicket(data[data.length - 1].inningNo)
                ]
              }`}
            </div>
            <div
              style={{
                fontSize: 12,
                color: grey_8
              }}
            >
              Runs
            </div>
          </div>
        </div>
      </div>
    );
  }
}
