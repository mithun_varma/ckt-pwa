import React, { Component } from "react";
import TopPlayerWC from "./TopPlayerWC";
import TeamScoreWC from "./TeamScoreWC";
import TeamScoreWCTest from "./TeamScoreWCTest";
import TopPlayerWCTest from "./TopPlayerWCTest";
import EmptyState from "../commonComponent/EmptyState";

const BestBowler = ({ data }) => {
  return (
    <div
      style={{
        color: "#fff",
        fontFamily: "Montserrat",
        alignItems: "flex-end",
        padding: 8,
        //   background: "#2e3857",
        fontSize: 14,
        display: "flex",
        justifyContent: "space-between",
        position: "relative",
        background: `linear-gradient(to right, #2e3857, #2e3857), url(${require("../../images/cricket_bg_btm.png")})`,
        backgroundPosition: "top center",
        backgroundSize: "contain",
        backgroundRepeat: "no-repeat",
        backgroundBlendMode: "color",
        minHeight: 72
      }}
    >
      <div
        style={{
          display: "flex",
          alignItems: "baseline"
        }}
      >
        <img
          src={data.teamAvatar || require("../../images/flag_empty.svg")}
          style={{ width: 20, height: 12, marginRight: 8 }}
        />
        {data &&
          data.playerName && (
            <div>
              <p style={{ fontWeight: 600 }}>{data.playerName.split(" ")[0]}</p>
              <p>
                {data.playerName.split(" ")[1]}{" "}
                {data.playerName.split(" ").length > 2 &&
                  data.playerName.split(" ")[2]}
              </p>
            </div>
          )}
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-end"
        }}
      >
        {data.battingStatistics[0].balls &&
        +data.battingStatistics[0].balls > 0 ? (
          <p>
            <span style={{ fontWeight: 600, marginRight: 2 }}>
              {data.battingStatistics[0].runs}
              {!data.battingStatistics[0].dismissed && "*"}
            </span>{" "}
            ({data.battingStatistics[0].balls})
          </p>
        ) : (
          ""
        )}
        {data.bowlingStatistics &&
          data.bowlingStatistics[0] &&
          (data.bowlingStatistics[0].wickets ||
            data.bowlingStatistics[0].wickets == 0) && (
            <p>
              <span style={{ fontWeight: 600, marginRight: 2 }}>{`${
                data.bowlingStatistics[0].wickets
              }/${data.bowlingStatistics[0].runs || 0}`}</span>
              {data.bowlingStatistics[0].overs
                ? "(" + data.bowlingStatistics[0].overs + ")"
                : ""}
            </p>
          )}
      </div>
      <div
        style={{
          position: "absolute",
          left: "35%",
          height: 90,
          width: 100,
          bottom: 0,
          overflow: "hidden"
        }}
      >
        <img style={{ width: "inherit" }} src={data.avatar || ""} />
      </div>
    </div>
  );
};


// ================================
// Best Bowler for Test

const BestBowlerTest = ({ data }) => {
  return (
    <div
      style={{
        color: "#fff",
        fontFamily: "Montserrat",
        alignItems: "flex-end",
        padding: 8,
        //   background: "#2e3857",
        fontSize: 14,
        display: "flex",
        // justifyContent: "space-between",
        justifyContent: "flex-end",
        position: "relative",
        background: `linear-gradient(to right, #2e3857, #2e3857), url(${require("../../images/cricket_bg_btm.png")})`,
        // backgroundPosition: "top center",
        backgroundSize: "contain",
        backgroundRepeat: "no-repeat",
        backgroundBlendMode: "color",
        minHeight: 72,
        flex: 1
      }}
    >
      {/* Player Image */}
      <div
        style={{
          position: "absolute",
          left: "0%",
          height: 90,
          width: 100,
          bottom: 0,
          overflow: "hidden"
        }}
      >
        <img style={{ width: "inherit" }} src={data.avatar || ""} />
      </div>
      {/* Player Image Closing */}

      {/* Player Name */}
      <div
        style={{
          display: "flex",
          alignItems: "baseline",
          flex: 0.54,
          justifyContent: "flex-end"
        }}
      >
        <img
          src={data.teamAvatar || require("../../images/flag_empty.svg")}
          style={{ width: 20, height: 12, marginRight: 8 }}
        />
        {data &&
          data.playerName && (
            <div>
              <p style={{ fontWeight: 600 }}>{data.playerName.split(" ")[0]}</p>
              <p>
                {data.playerName.split(" ")[1]}{" "}
                {data.playerName.split(" ").length > 2 &&
                  data.playerName.split(" ")[2]}
              </p>
            </div>
          )}
      </div>
      {/* Player Name Closing */}

      {/* Stats Column 1 */}
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-end",
          // marginLeft: 10,
          flex: 0.23,
          // minHeight: 42
        }}
      >
        <p style={{
          height: 21
        }}>
        {
        data.battingStatistics[0].balls &&
        +data.battingStatistics[0].balls > 0 ? (
          <p>
            <span style={{ fontWeight: 600, marginRight: 2 }}>
              {data.battingStatistics[0].runs || 0}
              {!data.battingStatistics[0].dismissed && "*"}
            </span>{" "}
            ({data.battingStatistics[0].balls || 0})
          </p>
        ) : (
          ""
        )}
        </p>
        {/* Hard Coded 1 */}
        {/* <p>
          <span style={{ fontWeight: 600, marginRight: 2 }}>
            {27}
            {"*"}
          </span>{" "}
          (49)
        </p> */}
        {/* Hard Coded 1 clsoing */}
        <p style={{
          height: 21
        }}>
        {
          data.bowlingStatistics &&
          data.bowlingStatistics[0] &&
          (data.bowlingStatistics[0].wickets ||
            data.bowlingStatistics[0].wickets == 0) && (
            <p>
              <span style={{ fontWeight: 600, marginRight: 2 }}>{`${
                data.bowlingStatistics[0].wickets
              }/${data.bowlingStatistics[0].runs || 0}`}</span>
              {data.bowlingStatistics[0].overs
                ? "(" + data.bowlingStatistics[0].overs + ")"
                : ""}
            </p>
          )}
          </p>
        {/* Hard Coded 2 */}
        {/* <p>
          <span
            style={{ fontWeight: 600, marginRight: 2 }}
          >{`${7}/${30}`}</span>
          {"(" + 8 + ")"}
        </p> */}
        {/* Hard Coded 2 closing */}
      </div>
      {/* Stats Column 1 closing */}

      {/* Stats Column 2 */}
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-end",
          // marginLeft: 10,
          flex: 0.23,
          // minHeight: 42
        }}
      >
        {/* Hard Coded 1 */}
        <p style={{
          height: 21
        }}>
        {
        data.battingStatistics[1] && data.battingStatistics[1].balls &&
        +data.battingStatistics[1].balls > 0 ? (
          <p>
            <span style={{ fontWeight: 600, marginRight: 2 }}>
              {data.battingStatistics[1].runs || 0}
              {!data.battingStatistics[1].dismissed && "*"}
            </span>{" "}
            ({data.battingStatistics[1].balls || 0})
          </p>
        ) : (
          ""
        )}
        </p>
        {/* Hard Coded 1 clsoing */}
        {/* Hard Coded 2 */}
        <p style={{
          height: 21
        }}>
        {
          data.bowlingStatistics &&
          data.bowlingStatistics[1] &&
          (data.bowlingStatistics[1].wickets ||
            data.bowlingStatistics[1].wickets == 0) && (
            <p>
              <span style={{ fontWeight: 600, marginRight: 2 }}>{`${
                data.bowlingStatistics[1].wickets
              }/${data.bowlingStatistics[1].runs || 0}`}</span>
              {data.bowlingStatistics[1].overs
                ? "(" + data.bowlingStatistics[1].overs + ")"
                : ""}
            </p>
          )}
          </p>
        {/* Hard Coded 2 closing */}
      </div>
      {/* Stats Column 2 closing */}

      {/* <div
        style={{
          position: "absolute",
          left: "35%",
          height: 90,
          width: 100,
          bottom: 0,
          overflow: "hidden"
        }}
      >
        <img style={{ width: "inherit" }} src={data.avatar || ""} />
      </div> */}
    </div>
  );
};

// Best Bowler for Test Closing


class ScoreSummary extends Component {
  render() {
    const { data } = this.props;
    return data.bestBatsman &&
      data.bestBowler &&
      data.teamScoreA &&
      data.teamScoreB ? (
      <div
        style={{
          background: "#172132"
        }}
      >
        
        {data.format && !data.format.match(/test/i) &&
          <TopPlayerWC playerData={data.bestBatsman} />
        }
        {/* {this.props.data2.format.match(/test/i) && */}
        {data.format && data.format.match(/test/i) &&
          <TopPlayerWCTest playerData={data.bestBatsman} />
        }
        <div
          style={{
            margin: "8px 8px 0px 8px",
            marginTop: -20
          }}
        >
          {data.format && !data.format.match(/test/i) &&
            <TeamScoreWC hasRadius data={data.teamScoreA} />
          }
          {data.format && !data.format.match(/test/i) &&
            <TeamScoreWC data={data.teamScoreB} />
          }

          {/* Test Match Component */}
          {data.format && data.format.match(/test/i) &&
            data.inningOrder && data.inningOrder.length > 0 &&
              data.inningOrder.map((teamInning, key) => (
              <TeamScoreWCTest
                inning={key}
                data={data[teamInning]}
                // data2={this.props.data2[teamInning]}
                // format={this.props.data2.format}
                format={data.format}
                // totalInning={this.props.data2.inningOrder.length}
                totalInning={data.inningOrder.length}
              />
            ))
          }
          {data.format && !data.format.match(/test/i) &&
            <BestBowler data={data.bestBowler} />
          }
          {data.format && data.format.match(/test/i) &&
            <BestBowlerTest data={data.bestBowler} />
          }
          
        </div>
      </div>
    ) : (
      <EmptyState msg="no scorecard summary found" />
    );
  }
}

export default ScoreSummary;
