import React, { Component } from "react";
import { cardShadow } from "../../styleSheet/globalStyle/color";
import HrLine from "../commonComponent/HrLine";

class TeamScoreWCTest extends Component {
  render() {
    const { data, data2, inning, hasRadius } = this.props;
    // console.log("object: ", this.props)
    return (
      <div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            color: "#141b2f",
            fontFamily: "Montserrat",
            background: "#f6f6f6",
            padding: 12,
            borderRadius: hasRadius ? "4px 4px 0px 0px" : ""
          }}
        >
          <div
            style={{
              fontSize: 10,
              display: "flex",
              alignItems: "center",
              fontWeight: 500,
              textTransform: "uppercase"
            }}
          >
            <img
              style={{
                width: 24,
                height: 16,
                marginRight: 8,
                boxShadow: cardShadow
              }}
              src={data.avatar || require("../../images/flag_empty.svg")}
            />
            {data.teamName || ""}
          </div>
          <div>
            <span style={{ fontSize: 14, fontWeight: 600 }}>
            {`${ inning < 2 ? data.runs[0] : data.runs[1] ||
              0}/${inning < 2 ? data.wickets[0] : data.wickets[1] || 0}`}
            </span>
            <span style={{ fontSize: 12, marginLeft: 6 }}>
              ({ inning < 2 ? data.overs[0] : data.overs[1] || 0})
            </span>
          </div>
        </div>
        <HrLine />
        {/* TopBatsMan and TopBowler Section */}
        <div
          style={{
            display: "flex",
            background: "#fff",
            justifyContent: "space-between",
            fontFamily: "Montserrat",
            fontSize: 12,
            flex: 1
          }}
        >
          <div
            style={{
              margin: "12px 0px",
              padding: "0px 8px",
              display: "flex",
              flex: "0.5",
              // minWidth: "50%",
              justifyContent: "space-between",
              borderRight: "1px solid rgb(227, 228, 230)"
            }}
          >
            <span>
              {inning < 2 &&(
              data.batsmanSummary1.topBatsman.shortName ||
                data.batsmanSummary1.topBatsman.displayName ||
                "")}
                {inning > 1 &&(
                  data.batsmanSummary2.topBatsman.shortName ||
                    data.batsmanSummary2.topBatsman.displayName ||
                    "")}
              {/* {data.batsmanSummary.topBatsman.shortName ||
                data.batsmanSummary.topBatsman.displayName ||
                ""} */}
            </span>
            <span style={{ fontWeight: 600 }}>
              {inning < 2 && (data.batsmanSummary1.topBatsman.runs || 0)}
              {inning > 1 && (data.batsmanSummary2.topBatsman.runs || 0)}
            </span>
          </div>
          <div
            style={{
              margin: "12px 8px",
              display: "flex",
              flex: "0.5",
              justifyContent: "space-between"
            }}
          >
            {inning < 2 && ((data.bowlerSummary1.topBowler.wickets ||
              data.bowlerSummary1.topBowler.wickets == 0) && (
              <div
                style={{
                  display: "flex",
                  flex: 1,
                  justifyContent: "space-between"
                }}
              >
                <span>
                  {data.bowlerSummary1.topBowler.shortName ||
                    data.bowlerSummary1.topBowler.displayName ||
                    "-"}
                </span>
                {(data.bowlerSummary1.topBowler.wickets ||
                  data.bowlerSummary1.topBowler.wickets == 0) && (
                  <span style={{ fontWeight: 600 }}>{`${
                    data.bowlerSummary1.topBowler.wickets
                  }/${data.bowlerSummary1.topBowler.runs || 0}`}</span>
                )}
              </div>
            ))}


            {inning > 1 && ((data.bowlerSummary2.topBowler.wickets ||
              data.bowlerSummary2.topBowler.wickets == 0) && (
              <div
                style={{
                  display: "flex",
                  flex: 1,
                  justifyContent: "space-between"
                }}
              >
                <span>
                  {data.bowlerSummary2.topBowler.shortName ||
                    data.bowlerSummary2.topBowler.displayName ||
                    "-"}
                </span>
                {(data.bowlerSummary2.topBowler.wickets ||
                  data.bowlerSummary2.topBowler.wickets == 0) && (
                  <span style={{ fontWeight: 600 }}>{`${
                    data.bowlerSummary2.topBowler.wickets
                  }/${data.bowlerSummary2.topBowler.runs || 0}`}</span>
                )}
              </div>
            ))}
          </div>
        </div>
        {/* TopBatsMan and TopBowler Section Closing */}
        <HrLine />
        {/* RunnerBatsMan and RunnerBowler Section */}
        {this.props.totalInning < 3 &&
        <div
          style={{
            display: "flex",
            background: "#fff",
            justifyContent: "space-between",
            fontFamily: "Montserrat",
            fontSize: 12,
            flex: 1
          }}
        >
          <div
            style={{
              margin: "12px 0px",
              padding: "0px 8px",
              display: "flex",
              flex: "0.5",
              // minWidth: "50%",
              justifyContent: "space-between",
              borderRight: "1px solid rgb(227, 228, 230)"
            }}
          >
            <span>
              {inning < 2 &&( data.batsmanSummary1.runnerBatsman.shortName ||
                data.batsmanSummary1.runnerBatsman.displayName ||
                "")}
              {inning > 1 &&( data.batsmanSummary2.runnerBatsman.shortName ||
                data.batsmanSummary2.runnerBatsman.displayName) ||
                ""}
            </span>
            <span style={{ fontWeight: 600 }}>
              { inning < 2 && (data.batsmanSummary1.runnerBatsman.runs || 0)}
              { inning > 1 && (data.batsmanSummary2.runnerBatsman.runs || 0)}
            </span>
          </div>
          <div
            style={{
              margin: "12px 8px",
              display: "flex",
              flex: "0.5",
              justifyContent: "space-between"
            }}
          >
            {inning < 2 && ((data.bowlerSummary1.runnerBowler.wickets ||
              data.bowlerSummary1.runnerBowler.wickets == 0) && (
              <div
                style={{
                  display: "flex",
                  flex: 1,
                  justifyContent: "space-between"
                }}
              >
                <span>
                  {data.bowlerSummary1.runnerBowler.shortName ||
                    data.bowlerSummary1.runnerBowler.displayName ||
                    ""}
                </span>
                {/* <span style={{ fontWeight: 600 }}>{`${
              data.bowlerSummary.runnerBowler.wickets
            }/${data.bowlerSummary.runnerBowler.runs}`}</span> */}
                {(data.bowlerSummary1.runnerBowler.wickets ||
                  data.bowlerSummary1.runnerBowler.wickets == 0) && (
                  <span style={{ fontWeight: 600 }}>{`${
                    data.bowlerSummary1.runnerBowler.wickets
                  }/${data.bowlerSummary1.runnerBowler.runs || 0}`}</span>
                )}
              </div>
            ))}

            {inning > 1 && ((data.bowlerSummary2.runnerBowler.wickets ||
              data.bowlerSummary2.runnerBowler.wickets == 0) && (
              <div
                style={{
                  display: "flex",
                  flex: 1,
                  justifyContent: "space-between"
                }}
              >
                <span>
                  {data.bowlerSummary2.runnerBowler.shortName ||
                    data.bowlerSummary2.runnerBowler.displayName ||
                    ""}
                </span>
                {(data.bowlerSummary2.runnerBowler.wickets ||
                  data.bowlerSummary2.runnerBowler.wickets == 0) && (
                  <span style={{ fontWeight: 600 }}>{`${
                    data.bowlerSummary2.runnerBowler.wickets
                  }/${data.bowlerSummary2.runnerBowler.runs || 0}`}</span>
                )}
              </div>
            ))}

          </div>
        </div>
        }
        {/* RunnerBatsMan and RunnerBowler Section Closing */}

        <HrLine />
      </div>
    );
  }
}

export default TeamScoreWCTest;
