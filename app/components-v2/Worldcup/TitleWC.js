import React, { Component } from "react";
import ChevronRight from "@material-ui/icons/ChevronRight";
import HrLine from "../commonComponent/HrLine";

class TitleWC extends Component {
  render() {
    const { title, hasRedirect, hasArrow } = this.props;
    return (
      <div onClick={this.props.handleRedirect}>
        <div
          style={{
            ...titleStyle,
            borderRadius: !hasArrow ? "2px 0px" : "",
            ...this.props.style
          }}
        >
          {title} {hasRedirect && <ChevronRight />}
          {!hasArrow && <div style={{ ...triangle }} />}
        </div>
        <HrLine
          style={{
            background:
              "linear-gradient(to right, #162031, rgba(227, 228, 230,0), rgba(227, 228, 230,0), #162031)"
          }}
        />
      </div>
    );
  }
}

const titleStyle = {
  display: "flex",
  position: "relative",
  justifyContent: "space-between",
  padding: 12,
  background: "#172132",
  color: "#fff",
  fontFamily: "Montserrat",
  fontSize: 10,
  fontWeight: 500,
  textTransform: "uppercase",
  alignItems: "center",
  // borderRadius: "2px 2px 0px 0px"
};
const triangle = {
  position: "absolute",
  width: 0,
  height: 0,
  borderLeft: "14px solid transparent",
  borderRight: "14px solid transparent",
  borderBottom: "14px solid #172132",
  top: -13,
  left: 16
};

export default TitleWC;
