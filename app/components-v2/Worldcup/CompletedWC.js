import React, { Component } from "react";
import TitleWC from "./TitleWC";
import ScoreSummary from "./ScoreSummary";
import CardGradientTitle from "../commonComponent/CardGradientTitle";
import HrLine from "../commonComponent/HrLine";
import ArticleCard from "../Articles/ArticleCard";
import { grey_10 } from "../../styleSheet/globalStyle/color";

let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  ClevertapReact.initialize("W88-4KR-845Z");
}

class CompletedWC extends Component {
  cardOnClick = path => {
    this.props.history.push(path);
  };

  render() {
    const { highlight } = this.props;
    return this.props.loading ? (
      <div
        style={{
          // width: "90%",
          // padding:'16px',
          minHeight: 400,
          background: "#fff",
          margin: "16px"
        }}
      >
        <img
          src={require("../../images/CricketLoader.gif")}
          style={{
            width: "100px",
            marginLeft: "30%",
            marginTop: "35%",
            // height: 200,
            background: "#fff"
          }}
        />
      </div>
    ) : (
      this.props.scoreData && (
        <div>
          {this.props.highlight &&
            this.props.highlight.length > 0 && (
              <React.Fragment>
                <div
                  style={{
                    padding: "12px 16px",
                    display: "flex",
                    alignItems: "center",
                    background: "linear-gradient(to right, #353e59, #1e2437)",
                    borderRadius: "3px 3px 0 0",
                    position: "relative"
                  }}
                >
                  <div
                    style={{
                      position: "absolute",
                      width: 0,
                      height: 0,
                      borderLeft: "14px solid transparent",
                      borderRight: "14px solid transparent",
                      borderBottom: "14px solid #353e59",
                      top: -13,
                      left: 16
                    }}
                  />
                  <span
                    style={{
                      color: "#fff",
                      // marginLeft: 8,
                      fontSize: 12
                    }}
                  >
                    {this.props.matchName}
                  </span>
                </div>
                <div>
                  <div
                    style={{
                      // width: "100%",
                      margin: "16px",
                      marginBottom: 0
                      // padding: "4px 0",
                      // marginRight: 12
                    }}
                    s
                    key={`${highlight[0].id || highlight[0]._id}`}
                    onClick={() => {

                      // this.props.history.push(
                        //   `/videos/${highlight[0].id || highlight[0]._id}`
                        // )
                        ClevertapReact.event("WorldCupCompleted", {
                          source: "WorldCupCompleted",
                          articleId: highlight[0].id || highlight[0]._id
                        })
                        this.props.videoRedirect(
                          `/videos/${highlight[0].id || highlight[0]._id}`
                          )
                      }
                    }
                  >
                    <div
                      style={{
                        width: "100%",
                        height: 130,
                        background: "#eeeeee",
                        backgroundImage: `url('${highlight[0].thumbnail ||
                          highlight[0].imageData
                            .thumbnail}'),linear-gradient(to right, #a63ba1, rgba(250, 148, 65, 0))`,
                        backgroundPosition: "top center",
                        backgroundSize: "cover",
                        backgroundRepeat: "no-repeat",
                        borderRadius: 2
                        // borderRadius: "3px 3px 0 0"
                      }}
                    >
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                          height: "inherit",
                          position: "relative"
                        }}
                      >
                        <img
                          style={{ width: 45 }}
                          src={require("../../images/videoIco.svg")}
                        />
                        {highlight[0].readTime && (
                          <div
                            style={{
                              display: "flex",
                              position: "absolute",
                              bottom: "8px",
                              right: "8px",
                              padding: "4px 8px",
                              background: "rgba(0,0,0,0.5)",
                              borderRadius: "2px",
                              color: "#fff",
                              fontFamily: "mont500"
                            }}
                          >
                            {highlight[0].readTime}
                          </div>
                        )}
                      </div>
                    </div>
                    <div
                      style={{
                        fontFamily: "mont400",
                        color: grey_10,
                        fontSize: 12,
                        margin: "12px 0px"
                      }}
                    >
                      {highlight[0].title}
                    </div>
                  </div>
                </div>
                <HrLine />
                {this.props.criclyticsData.articles && (
                  <div>
                    <ArticleCard
                      article={this.props.criclyticsData.articles[0]}
                      cardType="analysis"
                      key={this.props.criclyticsData.articles[0].id}
                      cardOnClick={this.cardOnClick}
                      rootStyles={{ padding: "12px 16px", background: "#fff" }}
                    />
                  </div>
                )}
              </React.Fragment>
            )}
          {this.props.scoreData && 
            this.props.scoreData.format && 
            !this.props.scoreData.abondoned ? (
            <div>
              <TitleWC
                handleRedirect={this.props.handleSummaryRedirect}
                title="Match summary"
                hasRedirect
                hasArrow={this.props.highlight.length > 0}
              />
              <ScoreSummary data={this.props.scoreData} />
            </div>
          ) : (
            ""
          )}
          {this.props.criclyticsData.articles &&
            this.props.highlight.length === 0 && (
              <div>
                <HrLine />
                <CardGradientTitle
                  title="POST MATCH REPORT"
                  rootStyles={{
                    padding: "12px 12px 12px 12px",
                    fontSize: 10
                  }}
                />
                <HrLine />
                <ArticleCard
                  article={this.props.criclyticsData.articles[0]}
                  cardType="analysis"
                  key={this.props.criclyticsData.articles[0].id}
                  cardOnClick={this.cardOnClick}
                  rootStyles={{ padding: "12px 16px", background: "#fff" }}
                />
              </div>
            )}
        </div>
      )
    );
  }
}

export default CompletedWC;
