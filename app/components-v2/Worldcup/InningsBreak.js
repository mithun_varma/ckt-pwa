import React, { Component } from "react";
import {
  gradientBlack,
  white,
  pink_3,
  red_Orange,
  cardShadow,
  gradientGrey,
  grey_9
} from "../../styleSheet/globalStyle/color";
import CardGradientTitle from "../commonComponent/CardGradientTitle";
import HrLine from "../commonComponent/HrLine";
import isEmpty from "lodash/isEmpty";
import IBChart from "./IBChart";
import TestChart from "./TestChart";
import CriclyticsTeamStats from "../Criclytics/CriclyticsCommon/CriclyticsTeamStats";
import TabBar from "../commonComponent/TabBar";
import TestTeamStats from "./TestTeamStats";

const completedSessions = ["session1", "session2"];

class InningsBreak extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeItem: ""
    };
  }

  static getDerivedStateFromProps = (nextProps, state) => {
    if (nextProps.isTest && !state.activeItem) {
      return {
        ...state,
        activeItem: Object.keys(nextProps.inningsData)[Object.keys(nextProps.inningsData).length - 1]
      };
    }
    return null;
  };
  // redirecToScoreDetails = () => {
  //   if (
  //     this.props.currentScoreData &&
  //     (this.props.currentScoreData.matchId ||
  //       this.props.currentScoreData.matchid)
  //   ) {
  //     this.props.history.push(
  //       `/score/${this.props.currentScoreData.matchId ||
  //         this.props.currentScoreData.matchid}`
  //     );
  //   }
  // };

  // redirecToCriclyticsSlider = () => {
  //   if (
  //     this.props.currentScoreData &&
  //     (this.props.currentScoreData.matchId ||
  //       this.props.currentScoreData.matchid)
  //   ) {
  //     this.props.history.push(
  //       `/criclytics-slider/${this.props.currentScoreData.matchId ||
  //         this.props.currentScoreData.matchid}`
  //     );
  //   }
  // };

  handleSessionTab = activeItem => {
    if (Object.keys(this.props.inningsData).indexOf(activeItem) > -1) {
      this.setState({
        activeItem
      });
    }
  };

  render() {
    const {
      currentScoreData: scorecard,
      inningsData,
      criclyticsData
    } = this.props;
    // console.log(scorecard);
    const teams = [
      scorecard.firstBatting === scorecard.teamA.teamKey
        ? scorecard.teamA
        : scorecard.teamB,
      scorecard.firstBatting !== scorecard.teamA.teamKey
        ? scorecard.teamA
        : scorecard.teamB
    ];
    return scorecard &&
      !isEmpty(scorecard.inningCollection) &&
      inningsData ? (
      <div>
        <div
          style={{
            padding: "8px 16px",
            display: "flex",
            alignItems: "center",
            background: gradientBlack,
            borderRadius: "3px 3px 0 0",
            position: "relative"
          }}
        >
          <div
            style={{
              position: "absolute",
              width: 0,
              height: 0,
              borderLeft: "14px solid transparent",
              borderRight: "14px solid transparent",
              borderBottom: "14px solid #353e59",
              top: -13,
              left: 16
            }}
          />
          <img
            src={require("../../images/criclytics.svg")}
            alt=""
            style={{
              width: 26,
              height: 26
            }}
          />
          <span
            style={{
              color: white,
              marginLeft: 8
            }}
          >
            Criclytics
          </span>
        </div>
        <div>
          <CardGradientTitle
            title={
              scorecard && scorecard.format === "TEST"
                ? "SESSION WISE SUMMARY"
                : "PHASES OF INNINGS"
            }
            isRightIcon
            rootStyles={{
              padding: "8px 8px 8px 16px",
              fontSize: 10
            }}
            handleCardClick={this.props.redirecToScoreDetails}
          />
          <HrLine />
          {scorecard && scorecard.format === "TEST" ? (
            <TabBar
              items={["session1", "session2", "session3"]}
              onClick={this.handleSessionTab}
              activeItem={this.state.activeItem}
            />
          ) : (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                color: "#141b2f",
                fontFamily: "Montserrat",
                background: "#f6f6f6",
                padding: 12,
                borderRadius: "4px 4px 0px 0px"
              }}
            >
              <div
                style={{
                  fontSize: 10,
                  display: "flex",
                  alignItems: "center",
                  fontWeight: 500,
                  textTransform: "uppercase"
                }}
              >
                <img
                  style={{
                    width: 24,
                    height: 16,
                    marginRight: 8,
                    boxShadow: cardShadow
                  }}
                  src={
                    scorecard.firstBatting === scorecard.teamA.teamKey
                      ? scorecard.teamA.avatar
                      : scorecard.teamB.avatar ||
                        require("../../images/flag_empty.svg")
                  }
                />
                {scorecard.firstBatting === scorecard.teamA.teamKey
                  ? scorecard.teamA.shortName
                  : scorecard.teamB.shortName}
              </div>
              <div>
                <span style={{ fontSize: 14, fontWeight: 600 }}>{`${
                  scorecard.inningCollection[`${scorecard.firstBatting}_1`].runs
                }/${
                  scorecard.inningCollection[`${scorecard.firstBatting}_1`]
                    .wickets
                }`}</span>
                <span style={{ fontSize: 12 }}>
                  ({
                    scorecard.inningCollection[`${scorecard.firstBatting}_1`]
                      .overs
                  })
                </span>
              </div>
              <div>
                <span style={{ fontSize: 10, fontWeight: 500 }}>{`${
                  scorecard.inningCollection[`${scorecard.firstBatting}_1`]
                    .runRate
                }`}</span>
                <span style={{ fontSize: 10, marginLeft: 2 }}>RR</span>
              </div>
            </div>
          )}
          <HrLine />
          {scorecard && scorecard.format === "TEST" ? (
            <TestChart
              data={this.props.inningsData[this.state.activeItem]}
              scoreCard={scorecard}
            />
          ) : (
            <IBChart
              PPlimit={scorecard && scorecard.format === "ODI" ? 10 : 6}
              data={inningsData}
              scoreCard={scorecard}
            />
          )}
          {criclyticsData &&
            criclyticsData.liveScorePredictor &&
            criclyticsData.liveScorePredictor.liveScores.length > 0 && (
              <React.Fragment>
                <CardGradientTitle
                  title="TEAM SCORE PROJECTIONS"
                  isRightIcon
                  rootStyles={{
                    padding: "8px 8px 8px 16px",
                    fontSize: 10
                  }}
                  handleCardClick={this.props.redirecToCriclyticsSlider}
                />{" "}
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    background: gradientGrey,
                    fontSize: 8,
                    fontWeight: 600,
                    letterSpacing: 0.6,
                    color: grey_9,
                    padding: "5px 16px"
                  }}
                >
                  <div style={{ flex: 0.2 }}>LIVE</div>
                  <div style={{ flex: 0.6, textAlign: "center" }}>TEAM</div>
                  <div style={{ flex: 0.2 }}>PROJECTED</div>
                </div>
                <HrLine />
                {scorecard && scorecard.format === "TEST" ? (
                  <TestTeamStats
                    scoreCard={scorecard}
                    data={
                      criclyticsData &&
                      criclyticsData.liveScorePredictor &&
                      criclyticsData.liveScorePredictor.liveScores
                    }
                  />
                ) : (
                  <CriclyticsTeamStats
                    scoreCard={scorecard}
                    inningsBreak
                    data={
                      criclyticsData &&
                      criclyticsData.liveScorePredictor &&
                      criclyticsData.liveScorePredictor.liveScores
                    }
                  />
                )}
                {false &&
                  criclyticsData.liveScorePredictor.liveScores[
                    criclyticsData.liveScorePredictor.liveScores.length - 1
                  ].predictvizMarginView && (
                    <React.Fragment>
                      <HrLine />
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          alignItems: "center",
                          padding: 16
                        }}
                      >
                        <span
                          style={{
                            fontSize: 11,
                            fontWeight: 500,
                            letterSpacing: 0.6
                          }}
                        >
                          PROJECTED RESULT
                        </span>
                        <span
                          style={{
                            background: pink_3,
                            padding: "3px 16px",
                            borderRadius: 18,
                            fontSize: 12,
                            fontWeight: 400,
                            color: red_Orange
                          }}
                        >
                          <span
                            style={{
                              fontWeight: 600,
                              padding: "0px 4px"
                            }}
                          >
                            {+criclyticsData.liveScorePredictor.liveScores[
                              criclyticsData.liveScorePredictor.liveScores
                                .length - 1
                            ].predictvizMarginView.innings === 1
                              ? teams[0].shortName
                              : teams[1].shortName}
                          </span>
                          {`to 
                    ${
                      criclyticsData.liveScorePredictor.liveScores[
                        criclyticsData.liveScorePredictor.liveScores.length - 1
                      ].predictvizMarginView.result
                    }
                     by`}
                          <span
                            style={{
                              fontWeight: 600,
                              padding: "0px 4px"
                            }}
                          >
                            {criclyticsData.liveScorePredictor.liveScores[
                              criclyticsData.liveScorePredictor.liveScores
                                .length - 1
                            ].predictvizMarginView.wickets
                              ? criclyticsData.liveScorePredictor.liveScores[
                                  criclyticsData.liveScorePredictor.liveScores
                                    .length - 1
                                ].predictvizMarginView.wickets
                              : criclyticsData.liveScorePredictor.liveScores[
                                  criclyticsData.liveScorePredictor.liveScores
                                    .length - 1
                                ].predictvizMarginView.runs}
                          </span>
                          {criclyticsData.liveScorePredictor.liveScores[
                            criclyticsData.liveScorePredictor.liveScores
                              .length - 1
                          ].predictvizMarginView.wickets
                            ? +criclyticsData.liveScorePredictor.liveScores[
                                criclyticsData.liveScorePredictor.liveScores
                                  .length - 1
                              ].predictvizMarginView.wickets > 1
                              ? "wickets"
                              : "wicket"
                            : +criclyticsData.liveScorePredictor.liveScores[
                                criclyticsData.liveScorePredictor.liveScores
                                  .length - 1
                              ].predictvizMarginView.runs > 1
                              ? "runs"
                              : "run"}
                        </span>
                      </div>
                      <HrLine />
                    </React.Fragment>
                  )}
              </React.Fragment>
            )}
        </div>
      </div>
    ) : (
      <div>null</div>
    );
  }
}

export default InningsBreak;

const dummyData = {
  "session 1": [
    {
      over: "1",
      runs: "10",
      wickets: "0",
      inningId: "1",
      teamKey: "cdc-5-new-zealand"
    },
    {
      over: "2",
      runs: "20",
      wickets: "0",
      inningId: "1",
      teamKey: "cdc-15-england"
    },
    {
      over: "3",
      runs: "25",
      wickets: "1",
      inningId: "1",
      teamKey: "cdc-5-new-zealand"
    }
  ],
  "session 2": [
    {
      over: "4",
      runs: "10",
      wickets: "0",
      inningId: "1",
      teamKey: "cdc-5-new-zealand"
    },
    {
      over: "5",
      runs: "10",
      wickets: "2",
      inningId: "1",
      teamKey: "cdc-5-new-zealand"
    },
    {
      over: "6",
      runs: "20",
      wickets: "1",
      inningId: "1",
      teamKey: "cdc-15-england"
    }
  ]
};
