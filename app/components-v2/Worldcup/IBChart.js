import React from "react";

class IBChart extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      divWidth: 343
    };
    // this.chartDiv = React.createRef();
  }
  refCallback = element => {
    if (element) {
      if (element.getBoundingClientRect().width !== this.state.divWidth) {
        this.setState({
          divWidth: element.getBoundingClientRect().width
        });
      }
    }
  };

  getTotal = (data, part, type) => {
    let total = 0;
    const splitData =
      part === "start"
        ? data.splice(0, this.props.PPlimit)
        : part === "mid"
          ? data.splice(this.props.PPlimit, 30)
          : data.splice(40, 10);
    for (let over of splitData) {
      if (type === "run") {
        total += +over.runs;
      } else {
        total += +over.wickets;
      }
    }
    return total;
  };
  render() {
    const { data, scoreCard } = this.props;
    console.log(this.props.PPlimit);
    return (
      <React.Fragment>
        <div
          style={{
            position: "relative"
          }}
        >
          <div
            ref={this.refCallback}
            style={{
              display: "flex",
              alignItems: "flex-end",
              marginTop: 8
            }}
          >
            {data.map((arr, key) => (
              <div>
                {+arr.wickets !== 0 &&
                  [...Array(+arr.wickets)].map(() => (
                    <div
                      style={{
                        display: "block",
                        background: "#d23e2f",
                        borderRadius: "50%",
                        width: 5,
                        height: 5,
                        marginTop: "2px",
                        marginLeft: "auto",
                        marginRight: "auto",
                        marginBottom: 2
                      }}
                    />
                  ))}
                <div
                  style={{
                    width:
                      (this.state.divWidth - (data.length - 1)) / data.length,
                    height: +arr.runs * 8,
                    marginRight: key !== data.length - 1 ? 1 : 0,
                    background:
                      +arr.over < this.props.PPlimit + 1 || +arr.over > 40
                        ? "#a9b3d0"
                        : "#7d869e"
                  }}
                />
              </div>
            ))}
          </div>
          <div
            style={{
              height: 1,
              width: this.state.divWidth,
              borderBottom: "1px dashed #a9b3d0",
              position: "absolute",
              // bottom: 0
              bottom:
                49 +
                +scoreCard.inningCollection[`${scoreCard.firstBatting}_1`]
                  .runRate *
                  8
            }}
          />
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              padding: "8px 20px",
              color: "#fff",
              fontSize: 11,
              fontWeight: 600,
              background: "linear-gradient(to right, #353e59, #1e2437)"
            }}
          >
            {data.length > 10 && (
              <div style={{ display: "flex" }}>
                <div>
                  <p style={{ opacity: 0.6 }}>
                    1-{`${
                      data.length > this.props.PPlimit
                        ? this.props.PPlimit
                        : data.length
                    }`}
                  </p>
                  <p>{`${this.getTotal(
                    [...data],
                    "start",
                    "run"
                  )}/${this.getTotal([...data], "start")}`}</p>
                </div>
              </div>
            )}
            {data.length > this.props.PPlimit + 1 && (
              <div>
                <p style={{ opacity: 0.6 }}>
                  {this.props.PPlimit + 1}-{`${
                    data.length >= 40 ? 40 : data.length
                  }`}
                </p>
                <p>{`${this.getTotal([...data], "mid", "run")}/${this.getTotal(
                  [...data],
                  "mid"
                )}`}</p>
              </div>
            )}
            {data.length > 40 && (
              <div>
                <p style={{ opacity: 0.6 }}>
                  41-{`${data.length == 50 ? 50 : data.length}`}
                </p>
                <p>{`${this.getTotal([...data], "end", "run")}/${this.getTotal(
                  [...data],
                  "end"
                )}`}</p>
              </div>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default IBChart;
