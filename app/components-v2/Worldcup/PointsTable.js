import React, { Component } from "react";
import { red_Orange } from "../../styleSheet/globalStyle/color";
import PointsTable from "../commonComponent/PointsTable";

let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  ClevertapReact.initialize("W88-4KR-845Z");
}

class PointTable extends Component {
  render() {
    const { loading, pointsTable, history } = this.props;
    return loading ? (
      <div
        style={{
          // width: "90%",
          // padding:'16px',
          minHeight: 400,
          background: "#fff",
          margin: "16px"
        }}
      >
        <img
          src={require("../../images/CricketLoader.gif")}
          style={{
            width: "100px",
            marginLeft: "30%",
            marginTop: "35%",
            // height: 200,
            background: "#fff"
          }}
        />
      </div>
    ) : (
      pointsTable.length > 0 && (
        <div style={{ margin: "0px 16px 12px" }}>
          <PointsTable
            title="Worldcup Points Table"
            table={pointsTable.slice(0, 4)}
            onClick={() => {
              ClevertapReact.event("Series", {
                source: "Series",
                seriesId: "opta:1"
              });
              history.push(`/series/odi-world-cup/opta:1/points`);
            }}
          />
          <div
            style={{
              background: "#fff",
              display: "flex",
              justifyContent: "flex-end",
              // margin: "0 16px",
              flexDirection: "column"
            }}
          >
            <button
              style={{
                background: "#ffe8d9",
                color: red_Orange,
                margin: "8px 16px",
                padding: "0 16px",
                fontSize: "10px",
                fontFamily: "mont400",
                border: "none",
                borderRadius: 18,
                minWidth: 160,
                display: "flex",
                alignSelf: "flex-end",
                alignItems: "center",
                textTransform: "capitalize"
              }}
              onClick={() => this.props.history.push(`/playoffsProjection`)}
            >
              <img
                src={require("../../images/shape-copy-3.svg")}
                style={{ marginRight: 8 }}
              />
              <div
                style={{
                  height: "26px",
                  width: "1px",
                  marginRight: "8px",
                  background: "#f9ddcb"
                }}
              />
              view all worldcup projections
            </button>
          </div>
        </div>
      )
    );
  }
}

export default PointTable;
