import React, { Component } from "react";
import { cardShadow } from "../../styleSheet/globalStyle/color";
import HrLine from "../commonComponent/HrLine";

class TeamScoreWC extends Component {
  render() {
    const { data, hasRadius } = this.props;
    return (
      <div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            color: "#141b2f",
            fontFamily: "Montserrat",
            background: "#f6f6f6",
            padding: 12,
            borderRadius: hasRadius ? "4px 4px 0px 0px" : ""
          }}
        >
          <div
            style={{
              fontSize: 10,
              display: "flex",
              alignItems: "center",
              fontWeight: 500,
              textTransform: "uppercase"
            }}
          >
            <img
              style={{
                width: 24,
                height: 16,
                marginRight: 8,
                boxShadow: cardShadow
              }}
              src={data.avatar || require("../../images/flag_empty.svg")}
            />
            {data.teamName || ""}
          </div>
          <div>
            <span style={{ fontSize: 14, fontWeight: 600 }}>{`${data.runs ||
              0}/${data.wickets || 0}`}</span>
            <span style={{ fontSize: 12, marginLeft: 6 }}>
              ({data.overs || 0})
            </span>
          </div>
        </div>
        <HrLine />
        <div
          style={{
            display: "flex",
            background: "#fff",
            justifyContent: "space-between",
            fontFamily: "Montserrat",
            fontSize: 12,
            flex: 1
          }}
        >
          <div
            style={{
              margin: "12px 0px",
              padding: "0px 8px",
              display: "flex",
              flex: "0.5",
              // minWidth: "50%",
              justifyContent: "space-between",
              borderRight: "1px solid rgb(227, 228, 230)"
            }}
          >
            <span>
              {data.batsmanSummary.topBatsman.shortName ||
                data.batsmanSummary.topBatsman.displayName ||
                ""}
            </span>
            <span style={{ fontWeight: 600 }}>
              {data.batsmanSummary.topBatsman.runs || 0}
            </span>
          </div>
          <div
            style={{
              margin: "12px 8px",
              display: "flex",
              flex: "0.5",
              justifyContent: "space-between"
            }}
          >
            {(data.bowlerSummary.topBowler.wickets ||
              data.bowlerSummary.topBowler.wickets == 0) && (
              <div
                style={{
                  display: "flex",
                  flex: 1,
                  justifyContent: "space-between"
                }}
              >
                <span>
                  {data.bowlerSummary.topBowler.shortName ||
                    data.bowlerSummary.topBowler.displayName ||
                    ""}
                </span>
                {(data.bowlerSummary.topBowler.wickets ||
                  data.bowlerSummary.topBowler.wickets == 0) && (
                  <span style={{ fontWeight: 600 }}>{`${
                    data.bowlerSummary.topBowler.wickets
                  }/${data.bowlerSummary.topBowler.runs || 0}`}</span>
                )}
              </div>
            )}
          </div>
        </div>
        <HrLine />
        <div
          style={{
            display: "flex",
            background: "#fff",
            justifyContent: "space-between",
            fontFamily: "Montserrat",
            fontSize: 12,
            flex: 1
          }}
        >
          <div
            style={{
              margin: "12px 0px",
              padding: "0px 8px",
              display: "flex",
              flex: "0.5",
              // minWidth: "50%",
              justifyContent: "space-between",
              borderRight: "1px solid rgb(227, 228, 230)"
            }}
          >
            <span>
              {data.batsmanSummary.runnerBatsman.shortName ||
                data.batsmanSummary.runnerBatsman.displayName ||
                ""}
            </span>
            <span style={{ fontWeight: 600 }}>
              {data.batsmanSummary.runnerBatsman.runs || 0}
            </span>
          </div>
          <div
            style={{
              margin: "12px 8px",
              display: "flex",
              flex: "0.5",
              justifyContent: "space-between"
            }}
          >
            {(data.bowlerSummary.runnerBowler.wickets ||
              data.bowlerSummary.runnerBowler.wickets == 0) && (
              <div
                style={{
                  display: "flex",
                  flex: 1,
                  justifyContent: "space-between"
                }}
              >
                <span>
                  {data.bowlerSummary.runnerBowler.shortName ||
                    data.bowlerSummary.runnerBowler.displayName ||
                    ""}
                </span>
                {/* <span style={{ fontWeight: 600 }}>{`${
              data.bowlerSummary.runnerBowler.wickets
            }/${data.bowlerSummary.runnerBowler.runs}`}</span> */}
                {(data.bowlerSummary.runnerBowler.wickets ||
                  data.bowlerSummary.runnerBowler.wickets == 0) && (
                  <span style={{ fontWeight: 600 }}>{`${
                    data.bowlerSummary.runnerBowler.wickets
                  }/${data.bowlerSummary.runnerBowler.runs || 0}`}</span>
                )}
              </div>
            )}
          </div>
        </div>
        <HrLine />
      </div>
    );
  }
}

export default TeamScoreWC;
