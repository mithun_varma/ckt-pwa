import groupBy from "lodash/groupBy";
import Slider from "rc-slider";
import Tooltip from "rc-tooltip";
import React, { Component } from "react";
import PillTabs from "../../components/Common/PillTabs";
import isEmpty from "lodash/isEmpty";
import HrLine from "./../commonComponent/HrLine";
import CardGradientTitle from "./../commonComponent/CardGradientTitle";
import EmptyState from "../commonComponent/EmptyState";
import PredictChart from "../commonComponent/PredictChart";
import Fact from "../Fact";
import {
  grey_10,
  grey_4,
  grey_8,
  orange_2_1,
  white
} from "./../../styleSheet/globalStyle/color";
import Polling from "./../commonComponent/Polling";
import { NotFound } from "../../containers/CriclyticsSlider";

const Handle = Slider.Handle;

const handle = props => {
  const { value, dragging, index, ...restProps } = props;
  return (
    <Tooltip
      prefixCls="rc-slider-tooltip"
      overlay={value}
      visible={dragging}
      placement="top"
      key={index}
    >
      <Handle value={value} {...restProps} />
    </Tooltip>
  );
};

export class LiveScorePredictor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value1: 2,
      value2: 2,
      activeTab: "",
      teamData: {},
      inningsData: null,
      isLive: true
    };
  }
  onSlider1Change = value1 => {
    // console.log(!("2" in this.state.inningsData),value1);
    if (
      value1 !== 0 &&
      value1 <=
        this.state.inningsData["1"][this.state.inningsData["1"].length - 1]
          .overNo
    ) {
      const predictText =
        this.state.inningsData &&
        this.state.inningsData["1"].filter(dt => +dt.overNo === value1);
      const teamAPrediction = {
        teamAPercentage: predictText[0].winvizView.battingTeamPercent,
        teamBpercentage: predictText[0].winvizView.bowlingTeamPercent,
        tiePercent: predictText[0].winvizView.tiePercent
      };
      this.setState({
        isLive: false,
        value1,
        teamAPrediction,
        winningText1: `${
          this.state.teamData[predictText[0].predictvizMarginView.innings]
        } to ${predictText[0].predictvizMarginView.result} by ${
          predictText[0].predictvizMarginView.runs
            ? `${predictText[0].predictvizMarginView.runs} runs`
            : `${predictText[0].predictvizMarginView.wickets} wickets`
        }`,
        prediction: predictText[0]
      });
      if (
        !("2" in this.state.inningsData) &&
        value1 ===
          +this.state.inningsData["1"][this.state.inningsData["1"].length - 1]
            .overNo
      ) {
        this.setState({
          isLive: true
        });
      }
    }
  };

  goLive = () => {
    const { currentMatch } = this.props;
    const { inningsData, teamData } = this.state;
    let teamAPrediction = {};
    let teamBPrediction = {};

    if ("2" in inningsData) {
      teamAPrediction = {
        teamAPercentage:
          inningsData["1"][inningsData["1"].length - 1].winvizView
            .battingTeamPercent,
        teamBpercentage:
          inningsData["1"][inningsData["1"].length - 1].winvizView
            .bowlingTeamPercent,
        tiePercent:
          inningsData["1"][inningsData["1"].length - 1].winvizView.tiePercent
      };
      teamBPrediction = {
        teamAPercentage:
          inningsData["2"][inningsData["2"].length - 1].winvizView
            .bowlingTeamPercent,
        teamBpercentage:
          inningsData["2"][inningsData["2"].length - 1].winvizView
            .battingTeamPercent,
        tiePercent:
          inningsData["2"][inningsData["2"].length - 1].winvizView.tiePercent
      };
    } else {
      teamAPrediction = {
        teamAPercentage:
          inningsData["1"][inningsData["1"].length - 1].winvizView
            .battingTeamPercent,
        teamBpercentage:
          inningsData["1"][inningsData["1"].length - 1].winvizView
            .bowlingTeamPercent,
        tiePercent:
          inningsData["1"][inningsData["1"].length - 1].winvizView.tiePercent
      };
    }
    this.setState({
      ...this.state,
      inningsData,
      isLive: true,
      scoreCard: currentMatch,
      teamData,
      activeTab: !("2" in inningsData) ? teamData["1"] : teamData["2"],
      liveTeamName: !("2" in inningsData) ? teamData["1"] : teamData["2"],
      prediction: !("2" in inningsData)
        ? inningsData["1"][inningsData["1"].length - 1]
        : inningsData["2"][inningsData["2"].length - 1],
      teamAPrediction,
      teamBPrediction,
      value1:
        "1" in inningsData
          ? +inningsData["1"][inningsData["1"].length - 1].overNo
          : 0,
      value2:
        !("2" in inningsData) || inningsData["2"].length === 0
          ? 0
          : +inningsData["2"][inningsData["2"].length - 1].overNo,
      winningText1:
        "1" in inningsData
          ? `${
              teamData[
                inningsData["1"][inningsData["1"].length - 1]
                  .predictvizMarginView.innings
              ]
            } to ${
              inningsData["1"][inningsData["1"].length - 1].predictvizMarginView
                .result
            } by ${
              inningsData["1"][inningsData["1"].length - 1].predictvizMarginView
                .runs
                ? `${
                    inningsData["1"][inningsData["1"].length - 1]
                      .predictvizMarginView.runs
                  } runs`
                : `${
                    inningsData["1"][inningsData["1"].length - 1]
                      .predictvizMarginView.wickets
                  } wickets`
            }`
          : "",
      winningText2:
        "2" in inningsData
          ? `${
              teamData[
                inningsData["2"][inningsData["2"].length - 1]
                  .predictvizMarginView.innings
              ]
            } to ${
              inningsData["2"][inningsData["2"].length - 1].predictvizMarginView
                .result
            } by ${
              inningsData["2"][inningsData["2"].length - 1].predictvizMarginView
                .runs
                ? `${
                    inningsData["2"][inningsData["2"].length - 1]
                      .predictvizMarginView.runs
                  } runs`
                : `${
                    inningsData["2"][inningsData["2"].length - 1]
                      .predictvizMarginView.wickets
                  } wickets`
            }`
          : ""
    });
  };

  onSlider2Change = value2 => {
    if (
      value2 !== 0 &&
      value2 <=
        this.state.inningsData["2"][this.state.inningsData["2"].length - 1]
          .overNo
    ) {
      const predictText =
        this.state.inningsData &&
        this.state.inningsData["2"].filter(dt => +dt.overNo === value2);
      const teamBPrediction = {
        teamAPercentage: predictText[0].winvizView.bowlingTeamPercent,
        teamBpercentage: predictText[0].winvizView.battingTeamPercent,
        tiePercent: predictText[0].winvizView.tiePercent
      };
      this.setState({
        isLive:
          value2 ==
          this.state.inningsData["2"][this.state.inningsData["2"].length - 1]
            .overNo
            ? true
            : false,
        value2,
        teamBPrediction,
        winningText2: `${
          this.state.teamData[predictText[0].predictvizMarginView.innings]
        } to ${predictText[0].predictvizMarginView.result} by ${
          predictText[0].predictvizMarginView.runs
            ? `${predictText[0].predictvizMarginView.runs} runs`
            : `${predictText[0].predictvizMarginView.wickets} wickets`
        }`,
        prediction: predictText[0]
      });
    }
  };

  onTabChange = ev => {
    // console.log(ev);
    if (ev !== this.state.activeTab) {
      this.setState({
        activeTab: ev
      });
    }
  };

  renderStats = () => {
    const styles = {
      root: {
        opacity: 0.97,
        borderRadius: "2px",
        backgroundImage: "linear-gradient(161deg, #0b1533, #050b1b)"
      },
      firstParent: {
        display: "flex",
        justifyContent: "space-between",
        padding: "9px 7px"
      },
      firstParentsFirstChild: {
        borderRadius: "7.5px",
        border: "solid 0.6px #9b9b9b",
        display: "flex",
        justifyContent: "center"
      },
      greenDot: {
        width: " 5px",
        height: "5px",
        backgroundColor: "#3bc306",
        alignSelf: "center",
        borderRadius: "50%",
        margin: "0px 4px"
      },
      LiveText: {
        fontFamily: "Montserrat",
        fontSize: "10px",
        fontWeight: 500,
        fontStyle: normal,
        fontStretch: normal,
        lineHeight: 1.1,
        letterSpacing: "1.1px",
        textAlign: "center",
        color: "#ccc",
        alignSelf: "center",
        padding: "2.5px 4px"
      },
      scoreCard: {
        fontFamily: "Montserrat",
        fontSize: "10px",
        fontWeight: 500,
        fontStyle: normal,
        fontStretch: normal,
        lineHeight: normal,
        letterSpacing: normal,
        textAlign: "center",
        color: "#ffffff",
        alignSelf: "center"
      },
      secondParent: {
        display: "flex",
        justifyContent: "space-between",
        padding: "9px 7px",
        alignItems: "center"
      },
      projectedScore: {
        fontFamily: "Montserrat",
        fontSize: "10px",
        fontWeight: normal,
        fontStyle: normal,
        fontStretch: normal,
        lineHeight: normal,
        letterSpacing: normal,
        textAlign: "center",
        color: "#ffffff"
        // paddingLeft: "10px"
      }
    };
    return (
      <div style={styles.root}>
        <div style={styles.firstParent}>
          <div style={styles.firstParentsFirstChild}>
            <div style={styles.greenDot} />
            <div style={styles.LiveText}>LIVE</div>
          </div>

          <div style={styles.scoreCard}>
            {this.state.liveTeamName ? this.state.liveTeamName : ""}
            {this.state.scoreCard &&
              ` ${this.state.scoreCard.currentScoreCard.runs}/${
                this.state.scoreCard.currentScoreCard.wickets
              }`}{" "}
            ({this.state.scoreCard &&
              this.state.scoreCard.currentScoreCard.overs})
          </div>
        </div>
        <HrLine />
        <div style={styles.secondParent}>
          <div style={styles.projectedScore}>Projected Score:</div>
          <div
            style={{
              fontFamily: "Montserrat",
              fontSize: "10px",
              fontWeight: normal,
              fontStyle: normal,
              fontStretch: normal,
              lineHeight: normal,
              letterSpacing: normal,
              textAlign: "center",
              color: "#ffffff",
              paddingLeft: "10px"
            }}
          >
            <span style={{ fontFamily: "oswald500", fontSize: 16 }}>
              {this.state.prediction && this.state.prediction.predictedScore}
            </span>
            <span
              style={{
                fontFamily: "oswald300",
                fontSize: 12,
                marginLeft: 4
              }}
            >
              ({this.state.prediction && this.state.prediction.predictedOver})
            </span>
          </div>
        </div>
      </div>
    );
  };

  static getDerivedStateFromProps = (props, state) => {
    const { data, currentMatch } = props;
    // console.log(props);
    if (
      props.data &&
      props.data.liveScores &&
      props.data.liveScores.length > 0 &&
      currentMatch &&
      !state.inningsData
    ) {
      const predictedScores = data.liveScores.filter(
        dt => !isEmpty(dt.winvizView)
      );

      // console.log(predictedScores);

      let inningsData = null;
      if (predictedScores.length > 0) {
        inningsData = groupBy(predictedScores, data => data.inningNo);
      }
      // console.warn(inningsData);
      const teamData = {};
      if (currentMatch) {
        teamData["1"] =
          currentMatch.firstBatting === currentMatch.teamA.teamKey
            ? currentMatch.teamA.shortName
            : currentMatch.teamB.shortName;
        teamData["2"] =
          currentMatch.firstBatting !== currentMatch.teamA.teamKey
            ? currentMatch.teamA.shortName
            : currentMatch.teamB.shortName;
      }
      if (!inningsData) {
        return null;
      }
      let teamAPrediction = {};
      let teamBPrediction = {};

      if ("2" in inningsData) {
        teamAPrediction = {
          teamAPercentage:
            inningsData["1"][inningsData["1"].length - 1].winvizView
              .battingTeamPercent,
          teamBpercentage:
            inningsData["1"][inningsData["1"].length - 1].winvizView
              .bowlingTeamPercent,
          tiePercent:
            inningsData["1"][inningsData["1"].length - 1].winvizView.tiePercent
        };
        teamBPrediction = {
          teamAPercentage:
            inningsData["2"][inningsData["2"].length - 1].winvizView
              .bowlingTeamPercent,
          teamBpercentage:
            inningsData["2"][inningsData["2"].length - 1].winvizView
              .battingTeamPercent,
          tiePercent:
            inningsData["2"][inningsData["2"].length - 1].winvizView.tiePercent
        };
      } else {
        teamAPrediction = {
          teamAPercentage:
            inningsData["1"][inningsData["1"].length - 1].winvizView
              .battingTeamPercent,
          teamBpercentage:
            inningsData["1"][inningsData["1"].length - 1].winvizView
              .bowlingTeamPercent,
          tiePercent:
            inningsData["1"][inningsData["1"].length - 1].winvizView.tiePercent
        };
      }
      return {
        ...state,
        inningsData,
        scoreCard: currentMatch,
        teamData,
        activeTab: !("2" in inningsData) ? teamData["1"] : teamData["2"],
        liveTeamName: !("2" in inningsData) ? teamData["1"] : teamData["2"],
        prediction: !("2" in inningsData)
          ? inningsData["1"][inningsData["1"].length - 1]
          : inningsData["2"][inningsData["2"].length - 1],
        teamAPrediction,
        teamBPrediction,
        value1:
          "1" in inningsData
            ? +inningsData["1"][inningsData["1"].length - 1].overNo
            : 0,
        value2:
          !("2" in inningsData) || inningsData["2"].length === 0
            ? 0
            : +inningsData["2"][inningsData["2"].length - 1].overNo,
        winningText1:
          "1" in inningsData
            ? `${
                inningsData["1"][inningsData["1"].length - 1]
                  .predictvizMarginView
                  ? teamData[
                      inningsData["1"][inningsData["1"].length - 1]
                        .predictvizMarginView.innings
                    ]
                  : ""
              } to ${
                inningsData["1"][inningsData["1"].length - 1]
                  .predictvizMarginView
                  ? inningsData["1"][inningsData["1"].length - 1]
                      .predictvizMarginView.result
                  : ""
              } by ${
                inningsData["1"][inningsData["1"].length - 1]
                  .predictvizMarginView &&
                inningsData["1"][inningsData["1"].length - 1]
                  .predictvizMarginView.runs
                  ? `${
                      inningsData["1"][inningsData["1"].length - 1]
                        .predictvizMarginView
                        ? inningsData["1"][inningsData["1"].length - 1]
                            .predictvizMarginView.runs
                        : ""
                    } runs`
                  : `${
                      inningsData["1"][inningsData["1"].length - 1]
                        .predictvizMarginView
                        ? inningsData["1"][inningsData["1"].length - 1]
                            .predictvizMarginView.wickets
                        : ""
                    } wickets`
              }`
            : "",
        winningText2:
          "2" in inningsData
            ? `${
                inningsData["2"][inningsData["2"].length - 1]
                  .predictvizMarginView
                  ? teamData[
                      inningsData["2"][inningsData["2"].length - 1]
                        .predictvizMarginView.innings
                    ]
                  : ""
              } to ${
                inningsData["2"][inningsData["2"].length - 1]
                  .predictvizMarginView
                  ? inningsData["2"][inningsData["2"].length - 1]
                      .predictvizMarginView.result
                  : ""
              } by ${
                inningsData["2"][inningsData["2"].length - 1]
                  .predictvizMarginView &&
                inningsData["2"][inningsData["2"].length - 1]
                  .predictvizMarginView.runs
                  ? `${
                      inningsData["2"][inningsData["2"].length - 1]
                        .predictvizMarginView
                        ? inningsData["2"][inningsData["2"].length - 1]
                            .predictvizMarginView.runs
                        : ""
                    } runs`
                  : `${
                      inningsData["2"][inningsData["2"].length - 1]
                        .predictvizMarginView
                        ? inningsData["2"][inningsData["2"].length - 1]
                            .predictvizMarginView.wickets
                        : ""
                    } wickets`
              }`
            : ""
      };
    }
    return null;
  };

  render() {
    // console.log(t);
    // this.state.initial;
    return !this.state.inningsData ? (
      <div>
        <div
          style={{
            padding: "16px 16px"
          }}
        >
          <div
            style={{
              background: grey_4
            }}
          >
            <NotFound message="No Live score Predictor found for the match" />
          </div>
        </div>
      </div>
    ) : (
      <div
        style={{
          padding: this.props.isHide ? "" : "16px 16px"
        }}
      >
        <div
          style={{
            background: grey_4
          }}
        >
          {!this.props.isHide && (
            <Fact
              header="Live Score Predictor"
              headerDescription="Is it going to be a high-scoring affair or a low-scoring bummer?"
              fact={
                this.props.currentMatch && this.props.currentMatch.factEngine
              }
              hideFactEngine
              hideHrLine
            />
          )}
          <div
            style={{
              marginTop: "2%"
            }}
          >
            <div
              style={{
                background: white
              }}
            >
              {!this.props.isHide && (
                <React.Fragment>
                  <div
                    style={{
                      ...styles.headerText,
                      flex: 1,
                      alignSelf: "center",
                      padding: "2.5% 4.9%"
                    }}
                  >
                    Who's winning?
                  </div>
                  <HrLine />
                  <div
                    style={{
                      padding: "8px 20px"
                    }}
                  >
                    <Polling
                      data={
                        this.state.activeTab === this.state.teamData["1"]
                          ? {
                              ...this.state.teamAPrediction,
                              teamA: this.state.teamData["1"],
                              teamB: this.state.teamData["2"]
                            }
                          : {
                              ...this.state.teamBPrediction,
                              teamA: this.state.teamData["1"],
                              teamB: this.state.teamData["2"]
                            }
                      }
                      score={false}
                    />
                  </div>
                  <CardGradientTitle title="Team Score Projections" />
                </React.Fragment>
              )}

              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  marginTop: this.props.isHide ? "-12px" : ""
                }}
              >
                <div
                  style={{
                    padding: "12px 0px 0px 12px"
                  }}
                >
                  {this.renderStats()}
                </div>
                {this.state.inningsData && (
                  <div
                    style={{
                      paddingTop: "12px",
                      fontFamily: "mont500",
                      fontSize: "10px",
                      paddingRight: "16px"
                    }}
                  >
                    Batting
                    <div style={{ marginTop: 10 }}>
                      <PillTabs
                        // items={[
                        //   this.state.teamData &&
                        //     this.state.teamData[
                        //       `${this.state.inningsData["1"][0].battingTeamId}`
                        //     ],
                        //   this.state.teamData &&
                        //     this.state.teamData[
                        //       `${this.state.inningsData["1"][0].bowlingTeamId}`
                        //     ]
                        // ]}
                        items={[
                          this.state.teamData["1"],
                          this.state.teamData["2"]
                        ]}
                        activeItem={this.state.activeTab}
                        isDisabled={
                          this.state.inningsData &&
                          !("2" in this.state.inningsData)
                        }
                        onTabSelect={this.onTabChange}
                      />
                    </div>
                  </div>
                )}
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  padding: "12px 16px"
                }}
              >
                <div
                  style={{
                    fontFamily: "Montserrat",
                    fontSize: "10px",
                    fontWeight: 500,
                    fontStyle: normal,
                    fontStretch: normal,
                    lineHeight: normal,
                    letterSpacing: normal,
                    color: "#141b2f"
                    // display: 'none'
                  }}
                >
                  <span
                    style={{
                      fontWeight: 600,
                      color: grey_10
                    }}
                  >
                    Projection:
                  </span>
                  <span
                    style={{
                      paddingLeft: 4,
                      color: grey_8
                    }}
                  >
                    {this.state.activeTab === this.state.teamData["1"]
                      ? this.state.winningText1
                      : this.state.winningText2}
                  </span>
                </div>
                <div
                  style={{
                    padding: "5px 5px",
                    borderRadius: "3px",
                    backgroundColor: orange_2_1,
                    display: "flex",
                    justifyContent: "space-around",
                    minWidth: "90px"
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between"
                    }}
                  >
                    <div
                      style={{
                        width: "4px",
                        height: "4px",
                        backgroundColor: "#28ae60",
                        borderRadius: "50%",
                        alignSelf: "center",
                        marginRight: "4px"
                      }}
                    />
                    <div
                      style={{
                        fontFamily: "Montserrat",
                        fontSize: "10px",
                        fontWeight: 500,
                        fontStyle: normal,
                        fontStretch: normal,
                        lineHeight: normal,
                        letterSpacing: "0.5px",
                        textAlign: "right",
                        color: "#141b2f",
                        alignSelf: "center"
                      }}
                    >
                      {this.state.teamData["1"]}
                    </div>
                  </div>
                  <div
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <div
                      style={{
                        width: "4px",
                        height: "4px",
                        backgroundColor: "#4382cb",
                        alignSelf: "center",
                        borderRadius: "50%",
                        margin: "4px"
                      }}
                    />
                    <div
                      style={{
                        fontFamily: "Montserrat",
                        fontSize: "10px",
                        fontWeight: 500,
                        fontStyle: normal,
                        fontStretch: normal,
                        lineHeight: normal,
                        letterSpacing: "0.5px",
                        textAlign: "right",
                        color: "#141b2f",
                        alignSelf: "center"
                      }}
                    >
                      {this.state.teamData["2"]}
                    </div>
                  </div>
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center"
                }}
              >
                {this.state.inningsData &&
                "1" in this.state.inningsData &&
                this.state.activeTab === this.state.teamData["1"] ? (
                  <React.Fragment>
                    {/* <div className="prediction-predictViz">
                      <span>PredictViz</span>
                      {/* <p>{this.state.winningText1 && this.state.winningText1}</p> 
                    </div> */}
                    <div
                      className="prediction-body"
                      style={{
                        height: "200px",
                        marginTop: "20px",
                        display: "flex",
                        justifyContent: "center"
                      }}
                    >
                      <PredictChart
                        data={
                          this.state.inningsData && this.state.inningsData["1"]
                        }
                        currentOver={this.state.value1}
                        lineColor="#28ae60"
                        maxXvalue={
                          this.props.currentMatch &&
                          this.props.currentMatch.format === "T20"
                            ? 20
                            : 50
                        }
                      />
                    </div>
                    <div
                      style={{
                        marginTop: "32px",
                        marginBottom: "16px",
                        display: "flex",
                        marginBottom: 20,
                        justifyContent: "center"
                      }}
                    >
                      <Slider
                        className="slider-main"
                        max={
                          this.props.currentMatch &&
                          this.props.currentMatch.format === "T20"
                            ? 20
                            : 50
                        }
                        step={1}
                        value={this.state.value1}
                        onChange={this.onSlider1Change}
                        handle={handle}
                        handleStyle={[
                          {
                            backgroundColor: "white",
                            border: "1px solid rgb(204, 215, 228)",
                            width: "30px",
                            height: "30px",
                            marginTop: "-12px",
                            boxShadow: "rgba(0, 0, 0, 0.1) 0px 2px 4px 0px"
                          }
                        ]}
                        trackStyle={[
                          { backgroundColor: "#a70e13", height: "4px" }
                        ]}
                        railStyle={{
                          backgroundColor: "#e8ebf3",
                          height: "4px"
                        }}
                      />
                    </div>
                  </React.Fragment>
                ) : this.state.inningsData &&
                "2" in this.state.inningsData &&
                this.state.inningsData["2"].length > 0 ? (
                  <React.Fragment>
                    {/* <div className="prediction-predictViz">
                      <span>PredictViz</span>
                      <p>
                        {this.state.winningText2 && this.state.winningText2}
                      </p>
                    </div> */}
                    <div
                      className="prediction-body"
                      style={{
                        height: "200px",
                        marginTop: "20px",
                        display: "flex",
                        justifyContent: "center"
                      }}
                    >
                      <PredictChart
                        data={
                          this.state.inningsData && this.state.inningsData["2"]
                        }
                        currentOver={this.state.value2}
                        innings2Data={
                          this.state.inningsData && this.state.inningsData["1"]
                        }
                        isSecond
                        lineColor="#4382cb"
                        secondInnColor="#28ae60"
                        maxXvalue={
                          this.props.currentMatch &&
                          this.props.currentMatch.format === "T20"
                            ? 20
                            : 50
                        }
                      />
                    </div>
                    <div
                      style={{
                        marginTop: "32px",
                        marginBottom: "16px",
                        display: "flex",
                        justifyContent: "center"
                      }}
                    >
                      <Slider
                        className="slider-main"
                        max={
                          this.props.currentMatch &&
                          this.props.currentMatch.format === "T20"
                            ? 20
                            : 50
                        }
                        step={1}
                        value={this.state.value2}
                        onChange={this.onSlider2Change}
                        handle={handle}
                        handleStyle={[
                          {
                            backgroundColor: "white",
                            border: "1px solid rgb(204, 215, 228)",
                            width: "30px",
                            height: "30px",
                            marginTop: "-12px",
                            boxShadow: "rgba(0, 0, 0, 0.1) 0px 2px 4px 0px"
                          }
                        ]}
                        trackStyle={[
                          { backgroundColor: "#a70e13", height: "4px" }
                        ]}
                        railStyle={{
                          backgroundColor: "#e8ebf3",
                          height: "4px"
                        }}
                      />
                    </div>
                  </React.Fragment>
                ) : (
                  <EmptyState
                    // img={require("../../images/no_ranking.png")}
                    msg="Second Innings Not Started Yet"
                  />
                )}
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  margin: "16px",
                  paddingBottom: "16px"
                }}
              >
                <button
                  className={this.state.isLive ? "golive active" : "golive"}
                  style={{
                    background: "#d44030",
                    color: "#fff",
                    letterSpacing: "0.6px",
                    border: "none",
                    borderRadius: "8px",
                    textTransform: "uppercase",
                    fontFamily: "mont500",
                    fontSize: "10px"
                    // padding: "4px"
                  }}
                  onClick={this.goLive}
                  disabled={this.state.isLive}
                >
                  go live
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LiveScorePredictor;

const normal = "normal";
const styles = {
  headerText: {
    fontFamily: "mont500",
    fontSize: "12px",
    fontWeight: "500",
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: normal,
    letterSpacing: normal,
    color: "#141b2f"
  }
};
// const slider = {};
let predictionMock = {
  summary: null,
  teamA: "KXIP",
  teamAPercentage: "65",
  teamB: "RR",
  teamBpercentage: "24",
  tiePercent: "11"
};
