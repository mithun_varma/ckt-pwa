import React, { Component } from "react";
import CardGradientTitle from "../commonComponent/CardGradientTitle";
import {
  gradientGrey,
  grey_9,
  pink_3,
  red_Orange,
  gradientBlack,
  white
} from "../../styleSheet/globalStyle/color";
import CriclyticsLiveStats from "./CriclyticsCommon/CriclyticsLiveStats";
import HrLine from "../commonComponent/HrLine";
import BallComponent from "../commonComponent/BallComponent";
import CriclyticsTeamStats from "./CriclyticsCommon/CriclyticsTeamStats";
import ChevronRight from "@material-ui/icons/ChevronRight";
import { isAndroid } from "react-device-detect";
import TestTeamStas from "../Worldcup/TestTeamStats";

class CriclyticsLiveHome extends Component {
  // redirecToScoreDetails = () => {
  //   if (
  //     this.props.currentScoreData &&
  //     (this.props.currentScoreData.matchId ||
  //       this.props.currentScoreData.matchid)
  //   ) {
  //     this.props.history.push(
  //       `/score/${this.props.currentScoreData.matchId ||
  //         this.props.currentScoreData.matchid}`
  //     );
  //   }
  // };

  // redirecToCriclyticsSlider = () => {
  //   if (
  //     this.props.currentScoreData &&
  //     (this.props.currentScoreData.matchId ||
  //       this.props.currentScoreData.matchid)
  //   ) {
  //     this.props.history.push(
  //       `/criclytics-slider/${this.props.currentScoreData.matchId ||
  //         this.props.currentScoreData.matchid}`
  //     );
  //   }
  // };

  render() {
    const { criclyticsData, currentScoreData } = this.props;
    const {
      battingAndBowlingProjectionsLive: {
        battingProbabilitiesWrapper: data,
        liveBowlingProjections: bowler
      }
    } = criclyticsData;
    const playerIds = [data && data.strikerId, data && data.nonStrikerId];
    const bowlerId = [bowler && bowler.bowlerId];
    const teams = [
      currentScoreData.firstBatting === currentScoreData.teamA.teamKey
        ? currentScoreData.teamA
        : currentScoreData.teamB,
      currentScoreData.firstBatting !== currentScoreData.teamA.teamKey
        ? currentScoreData.teamA
        : currentScoreData.teamB
    ];
    // console.log(batsmen, playerIds, Score, balls);
    return (
      <React.Fragment>
        <div
          style={{
            padding: "8px 8px 8px 16px",
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            background: gradientBlack,
            borderRadius: "3px 3px 0 0",
            position: "relative"
          }}
          onClick={this.props.redirecToCriclyticsSlider}
        >
          <div
            style={{
              position: "absolute",
              width: 0,
              height: 0,
              borderLeft: "14px solid transparent",
              borderRight: "14px solid transparent",
              borderBottom: "14px solid #353e59",
              top: -13,
              left: 16
            }}
          />
          <div
            style={{
              display: "flex",
              alignItems: "center"
            }}
          >
            <img
              src={require("../../images/criclytics.svg")}
              alt=""
              style={{
                width: 26,
                height: 26
              }}
            />
            <span
              style={{
                color: white,
                marginLeft: 8
              }}
            >
              Criclytics
            </span>
          </div>
          <ChevronRight style={{ color: white }} />
        </div>
        <div style={{ borderRadius: "0px 0px 2px 2px" }}>
          {this.props.last10balls && this.props.last10balls.length > 0 ? (
            <div>
              {/* Title Section */}
              <CardGradientTitle
                title="LAST 10 BALLS"
                isRightIcon
                rootStyles={{
                  padding: "8px 8px 8px 16px",
                  fontSize: 10
                }}
                handleCardClick={this.props.redirecToScoreDetails}
              />
              <div
                style={{
                  display: "flex",
                  justifyContent:
                    this.props.last10balls && this.props.last10balls.length > 9
                      ? "space-evenly"
                      : "flex-start",
                  // justifyContent: "space-evenly",
                  position: "relative",
                  padding: "22px 5px",
                  overflow: "hidden"
                }}
              >
                <BallComponent
                  // balls={["L", "W", "L", "L", "W", "L", "W", "L", "L", "W"]}
                  // balls={this.props.last10balls.slice(0,6)}
                  balls={this.props.last10balls}
                  rootStyles={{
                    margin: "0 2px"
                  }}
                  type="ball"
                  showSeparator
                />
                <div
                  style={{
                    position: "absolute",
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    background: isAndroid
                      ? "linear-gradient(to right, white, transparent 30%)"
                      : "transparent"
                  }}
                />
              </div>
            </div>
          ) : (
            ""
          )}

          {criclyticsData &&
            criclyticsData.liveScorePredictor &&
            criclyticsData.liveScorePredictor.liveScores &&
            criclyticsData.liveScorePredictor.liveScores.length > 0 && (
              <React.Fragment>
                <CardGradientTitle
                  title="TEAM SCORE PROJECTIONS"
                  isRightIcon
                  rootStyles={{
                    padding: "8px 8px 8px 16px",
                    fontSize: 10
                  }}
                  handleCardClick={this.props.redirecToCriclyticsSlider}
                />
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    background: gradientGrey,
                    fontSize: 8,
                    fontWeight: 600,
                    letterSpacing: 0.6,
                    color: grey_9,
                    padding: "5px 16px"
                  }}
                >
                  <div style={{ flex: 0.2 }}>LIVE</div>
                  <div style={{ flex: 0.6, textAlign: "center" }}>TEAM</div>
                  <div style={{ flex: 0.2 }}>PROJECTED</div>
                </div>
                <HrLine />
                {currentScoreData &&
                currentScoreData.format &&
                currentScoreData.format === "TEST" ? (
                  <TestTeamStas
                    scoreCard={this.props.currentScoreData}
                    data={
                      this.props.criclyticsData.liveScorePredictor.liveScores
                    }
                  />
                ) : (
                  <CriclyticsTeamStats
                    scoreCard={this.props.currentScoreData}
                    data={
                      this.props.criclyticsData.liveScorePredictor.liveScores
                    }
                  />
                )}
                {false &&
                  criclyticsData.liveScorePredictor.liveScores[
                    criclyticsData.liveScorePredictor.liveScores.length - 1
                  ].predictvizMarginView && (
                    <React.Fragment>
                      <HrLine />
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          alignItems: "center",
                          padding: 16
                        }}
                      >
                        <span
                          style={{
                            fontSize: 10,
                            fontWeight: 500,
                            letterSpacing: 0.6
                          }}
                        >
                          PROJECTED RESULT
                        </span>
                        <span
                          style={{
                            background: pink_3,
                            padding: "3px 12px",
                            borderRadius: 18,
                            fontSize: 10,
                            fontWeight: 400,
                            color: red_Orange
                          }}
                        >
                          <span
                            style={{
                              fontWeight: 600,
                              padding: "0px 4px"
                            }}
                          >
                            {+criclyticsData.liveScorePredictor.liveScores[
                              criclyticsData.liveScorePredictor.liveScores
                                .length - 1
                            ].predictvizMarginView.innings === 1
                              ? teams[0].shortName
                              : teams[1].shortName}
                          </span>
                          {`to 
                    ${
                      criclyticsData.liveScorePredictor.liveScores[
                        criclyticsData.liveScorePredictor.liveScores.length - 1
                      ].predictvizMarginView.result
                    }
                     by`}
                          <span
                            style={{
                              fontWeight: 600,
                              padding: "0px 4px"
                            }}
                          >
                            {criclyticsData.liveScorePredictor.liveScores[
                              criclyticsData.liveScorePredictor.liveScores
                                .length - 1
                            ].predictvizMarginView.wickets
                              ? criclyticsData.liveScorePredictor.liveScores[
                                  criclyticsData.liveScorePredictor.liveScores
                                    .length - 1
                                ].predictvizMarginView.wickets
                              : criclyticsData.liveScorePredictor.liveScores[
                                  criclyticsData.liveScorePredictor.liveScores
                                    .length - 1
                                ].predictvizMarginView.runs}
                          </span>
                          {criclyticsData.liveScorePredictor.liveScores[
                            criclyticsData.liveScorePredictor.liveScores
                              .length - 1
                          ].predictvizMarginView.wickets
                            ? +criclyticsData.liveScorePredictor.liveScores[
                                criclyticsData.liveScorePredictor.liveScores
                                  .length - 1
                              ].predictvizMarginView.wickets > 1
                              ? "wkts"
                              : "wkt"
                            : +criclyticsData.liveScorePredictor.liveScores[
                                criclyticsData.liveScorePredictor.liveScores
                                  .length - 1
                              ].predictvizMarginView.runs > 1
                              ? "runs"
                              : "run"}
                        </span>
                      </div>
                      <HrLine />
                    </React.Fragment>
                  )}
              </React.Fragment>
            )}
          {criclyticsData &&
            criclyticsData.battingAndBowlingProjectionsLive && (
              <React.Fragment>
                <CardGradientTitle
                  title="LIVE PLAYER PROJECTIONS"
                  isRightIcon
                  rootStyles={{
                    padding: "8px 8px 8px 16px",
                    fontSize: 10
                  }}
                  handleCardClick={this.props.redirecToCriclyticsSlider}
                />
                {/* Projection section starts */}
                <div>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                      background: gradientGrey,
                      fontSize: 8,
                      fontWeight: 600,
                      letterSpacing: 0.6,
                      color: grey_9,
                      padding: "5px 16px",
                      flex: 1
                    }}
                  >
                    <div style={{ flex: 0.2 }}>LIVE</div>
                    <div style={{ flex: 0.6, textAlign: "center" }}>
                      BATSMEN
                    </div>
                    <div style={{ flex: 0.2 }}>PROJECTED</div>
                  </div>
                  <HrLine />
                  <CriclyticsLiveStats data={data} batOrder={playerIds} />
                  {/* <HrLine /> */}
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      background: gradientGrey,
                      fontSize: 8,
                      fontWeight: 600,
                      letterSpacing: 0.6,
                      color: grey_9,
                      padding: "5px 16px"
                    }}
                  >
                    <div>BOWLER</div>
                  </div>
                  <HrLine />
                  <CriclyticsLiveStats
                    isBowler
                    batOrder={bowlerId}
                    data={bowler}
                  />
                </div>
              </React.Fragment>
            )}
          {/* Projection section closing */}
        </div>
      </React.Fragment>
    );
  }
}

export default CriclyticsLiveHome;
