import React from "react";
import {
  grey_4,
  grey_10,
  white,
  grey_8,
  gradientGrey,
  red_Orange,
  red_10
} from "../../../styleSheet/globalStyle/color";
import FactEngiene from "../../commonComponent/FactEngiene";
import EmptyFlag from "./../../../images/flag_empty.svg";
import HrLine from "../../commonComponent/HrLine";
import FallbackPlayerImage from "./../../../images/fallbackProjection.png";
import DeathOversImage from "./../../../images/death_overs_image.png";
import DeathOversNormalImage from "./../../../images/death_over_unselected.png";
import PlayerIconNormal from "./../../../images/playerIcon_normal.png";
import cloneDeep from "lodash/cloneDeep";
import map from "lodash/map";
import Modal from "react-responsive-modal";

// const mockData =
//     { "playerIds": [{ "name": "Prasidh Krishna", "avatar": "https://s3.ap-south-1.amazonaws.com/crictecdev/Players/Headshot/default-player-avtar.png", "overs": "2.0", "maiden": null, "runs": "24", "wickets": "0", "playerId": "cdc-7835-prasidh-krishna" }, { "name": "Lachlan Ferguson", "avatar": "https://s3.ap-south-1.amazonaws.com/crictecdev/Players/Headshot/cdc-4580-lh-ferguson-headshot.png", "overs": "3.0", "maiden": null, "runs": "32", "wickets": "0", "playerId": "cdc-4580-lh-ferguson" }, { "name": "Nitish Rana", "avatar": "https://s3.ap-south-1.amazonaws.com/crictecdev/Players/Headshot/default-player-avtar.png", "overs": "3.0", "maiden": null, "runs": "22", "wickets": "1", "playerId": "cdc-7824-nitish-rana" }], "matchId": "201953-17-royal-challengers-bangalore-vs-kolkata-knight-riders", "inningNumber": 1, "batPosition": 3, "totalRuns": 0, "totalWickets": 0, "totalOvers": 0, "remainingOvers": 4, "currentBowler": "cdc-2141-sp-narine", "checked": false }

const mockData = {
  playerIds: [],
  matchId: "201953-17-royal-challengers-bangalore-vs-kolkata-knight-riders",
  inningNumber: 1,
  batPosition: 3,
  totalRuns: 0,
  totalWickets: 0,
  totalOvers: 0,
  remainingOvers: 0,
  currentBowler: "cdc-2141-sp-narine",
  checked: false
};

const mockDataPost = {
  playerIds: [
    {
      name: "Chris Morris",
      avatar:
        "https://s3.ap-south-1.amazonaws.com/crictecdev/Players/Headshot/cdc-3156-ch-morris-headshot.png",
      overs: "17",
      maiden: null,
      runs: "14",
      wickets: "1",
      playerId: "cdc-3156-ch-morris"
    },
    {
      name: "Kagiso Rabada",
      avatar:
        "https://s3.ap-south-1.amazonaws.com/crictecdev/Players/Headshot/cdc-5483-k-rabada-headshot.png",
      overs: "18",
      maiden: null,
      runs: "11",
      wickets: "1",
      playerId: "cdc-5483-k-rabada"
    },
    {
      name: "Sandeep Lamichhane",
      avatar:
        "https://s3.ap-south-1.amazonaws.com/crictecdev/Players/Headshot/cdc-2083-s-lamichhane-headshot.png",
      overs: "19",
      maiden: null,
      runs: "11",
      wickets: "1",
      playerId: "cdc-2083-s-lamichhane"
    },
    {
      name: "Kagiso Rabada",
      avatar:
        "https://s3.ap-south-1.amazonaws.com/crictecdev/Players/Headshot/cdc-5483-k-rabada-headshot.png",
      overs: "20",
      maiden: null,
      runs: "12",
      wickets: "1",
      playerId: "cdc-5483-k-rabada"
    }
  ],
  matchId: "201973-20-royal-challengers-bangalore-vs-delhi-capitals",
  inningNumber: 1,
  batPosition: 3,
  totalRuns: 156,
  totalWickets: 8,
  totalOvers: 20,
  remainingOvers: 0,
  currentBowler: "cdc-3156-ch-morris",
  checked: false
};
class NewDeathOverSimulator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      useMockData: false,
      initial: true,
      selectedPlayers: [],
      deathOverBowlers: [],
      // maxOversPerBowler: 4,
      showPostRequestData: false,
      showDeathOverSimulatorScreen: false
    };
  }

  static getDerivedStateFromProps(nextProps, currentState) {
    try {
      const { currentMatch } = nextProps;
      const currentMatchData = currentMatch;
      let maxOvers;
      let maxOversPerBowler;
      if (currentMatchData) {
        maxOvers = currentMatchData.format === "ODI" ? 50 : 20;
        maxOversPerBowler = currentMatchData.format == "ODI" ? 10 : 4;
      }
      if (currentState.useMockData) {
        let data = mockData;
        let playerIds = data.playerIds;
        let receivedDataInitially = playerIds.length > 0;
        if (data.remainingOvers === 0 && receivedDataInitially) {
          receivedDataInitially = false;
        }
        if (
          playerIds &&
          currentState.deathOverBowlers.length === 0 &&
          currentState.initial
        ) {
          let originalCopy = [];
          for (let index = 0; index < playerIds.length; index++) {
            let playerData = playerIds[index];
            let overs = playerData.overs;
            // let remainingOvers = currentState.maxOversPerBowler - Number(overs);
            let remainingOvers = maxOversPerBowler - Number(overs);
            originalCopy.push({
              ...playerData,
              remainingOvers: remainingOvers
            });
          }
          return {
            originalCopy: originalCopy,
            originalDataCopy: { ...data },
            deathOverBowlers: [...playerIds],
            remainingOvers: data.remainingOvers,
            initial: false,
            receivedDataInitially: receivedDataInitially,
            maxOvers: maxOvers
          };
        } else if (currentState.reFetchingData) {
          // console.log("Refetchubg dtata ********** ", playerIds);
          if (playerIds) {
            let originalCopy = [];
            for (let index = 0; index < playerIds.length; index++) {
              let playerData = playerIds[index];
              let overs = playerData.overs;
              let remainingOvers = maxOversPerBowler - Number(overs);
              originalCopy.push({
                ...playerData,
                remainingOvers: remainingOvers
              });
            }
            return {
              originalCopy: originalCopy,
              originalDataCopy: { ...data },
              deathOverBowlers: [...playerIds],
              remainingOvers: data.remainingOvers,
              initial: false,
              receivedDataInitially: receivedDataInitially,
              maxOvers: maxOvers,
              selectedPlayers: [],
              reFetchingData: false
            };
          }
        } else {
          data = currentState.originalDataCopy;

          if (currentState.showPostRequestData) {
            if (nextProps.postData && nextProps.postData.status === 400) {
              return {
                postDataStatus: 400,
                postDataMessage: nextProps.postData.message,
                showPostDataError: true
              };
            }
          } else {
            if (data) {
              let playerIds = data.playerIds;
              if (!currentState.initial && currentState.resetOptions) {
                let originalCopy = [];
                for (let index = 0; index < playerIds.length; index++) {
                  let playerData = playerIds[index];
                  let overs = playerData.overs;
                  let remainingOvers = maxOversPerBowler - Number(overs);
                  originalCopy.push({
                    ...playerData,
                    remainingOvers: remainingOvers
                  });
                }
                return {
                  originalCopy: originalCopy,
                  originalDataCopy: { ...data },
                  deathOverBowlers: [...playerIds],
                  remainingOvers: data.remainingOvers,
                  selectedPlayers: [],
                  initial: false,
                  receivedDataInitially: receivedDataInitially,
                  maxOvers: maxOvers
                };
              }
            }
          }
        }
      } else {
        let { data } = nextProps;
        if (data) {
          let playerIds = data.playerIds;
          // console.log("Player  ******** ", playerIds)
          let receivedDataInitially = playerIds.length > 0;
          if (data.remainingOvers === 0 && receivedDataInitially) {
            receivedDataInitially = false;
          }
          if (
            playerIds &&
            currentState.deathOverBowlers.length === 0 &&
            currentState.initial
          ) {
            let originalCopy = [];
            for (let index = 0; index < playerIds.length; index++) {
              let playerData = playerIds[index];
              let overs = playerData.overs;
              let remainingOvers = maxOversPerBowler - Number(overs);
              originalCopy.push({
                ...playerData,
                remainingOvers: remainingOvers
              });
            }
            return {
              originalCopy: originalCopy,
              originalDataCopy: { ...data },
              deathOverBowlers: [...playerIds],
              remainingOvers: data.remainingOvers,
              initial: false,
              receivedDataInitially: receivedDataInitially,
              maxOvers: maxOvers
            };
          } else if (currentState.reFetchingData) {
            console.log("Refetchubg dtata ********** ", playerIds);
            if (playerIds) {
              let originalCopy = [];
              for (let index = 0; index < playerIds.length; index++) {
                let playerData = playerIds[index];
                let overs = playerData.overs;
                let remainingOvers = maxOversPerBowler - Number(overs);
                originalCopy.push({
                  ...playerData,
                  remainingOvers: remainingOvers
                });
              }
              return {
                originalCopy: originalCopy,
                originalDataCopy: { ...data },
                deathOverBowlers: [...playerIds],
                remainingOvers: data.remainingOvers,
                initial: false,
                receivedDataInitially: receivedDataInitially,
                maxOvers: maxOvers,
                selectedPlayers: [],
                reFetchingData: false
              };
            } else {
              return {
                selectedPlayers: []
              };
            }
          } else {
            data = currentState.originalDataCopy;
            // console.log("Next props here are ***** ",nextProps);
            if (currentState.showPostRequestData) {
              if (nextProps.postData && nextProps.postData.status === 400) {
                return {
                  postDataStatus: 400,
                  postDataMessage: nextProps.postData.message,
                  showPostDataError: true
                };
              }
            } else {
              if (data) {
                let playerIds = data.playerIds;
                if (!currentState.initial && currentState.resetOptions) {
                  let originalCopy = [];
                  for (let index = 0; index < playerIds.length; index++) {
                    let playerData = playerIds[index];
                    let overs = playerData.overs;
                    let remainingOvers = maxOversPerBowler - Number(overs);
                    originalCopy.push({
                      ...playerData,
                      remainingOvers: remainingOvers
                    });
                  }
                  return {
                    originalCopy: originalCopy,
                    originalDataCopy: { ...data },
                    deathOverBowlers: [...playerIds],
                    remainingOvers: data.remainingOvers,
                    selectedPlayers: [],
                    initial: false,
                    maxOvers: maxOvers
                  };
                }
              }
            }
          }
        }
      }
    } catch (e) {
      console.log("Exception inside getDerivedStateFromProps ", e);
    }

    return null;
  }

  getTeamScore = teamData => {
    if (teamData.runs && teamData.runs.length > 0) {
      return (
        teamData.runs + "/" + teamData.wickets + " (" + teamData.overs + ")"
      );
    } else {
      return "";
    }
  };

  getOriginalPlayer = playerName => {
    for (let index = 0; index < this.state.originalCopy.length; index++) {
      let player = this.state.originalCopy[index];
      if (player.name === playerName) {
        return player;
      }
    }
  };

  containsInSelectedPlayers = (selectedPlayers, playerName) => {
    for (let index = 0; index < selectedPlayers.length; index++) {
      let player = selectedPlayers[index];
      if (player.name === playerName) {
        return [true, index];
      }
    }
    return [false];
  };

  containsInDeathOvers = (deathOverBowlers, playerName) => {
    for (let index = 0; index < deathOverBowlers.length; index++) {
      let player = deathOverBowlers[index];
      if (player.name === playerName) {
        return true;
      }
    }
    return false;
  };

  addPlayersToDeathOvers = (
    originalCopy,
    deathOverBowlers,
    selectedPlayerName,
    selectedPlayers
  ) => {
    // console.log("adding player to death overs ", originalCopy)
    for (let index = 0; index < originalCopy.length; index++) {
      let player = originalCopy[index];
      if (player.name !== selectedPlayerName) {
        let result = this.containsInSelectedPlayers(
          selectedPlayers,
          player.name
        );
        // console.log("Contains inside selected player ", result);
        // console.log("Contains inside selected player ", player.name);
        let flag = result[0];
        if (flag) {
          if (!this.containsInDeathOvers(deathOverBowlers, player.name)) {
            // console.log("Contains inside death overs player ", player.name);
            let index = result[1];
            let remainingOvers = player.remainingOvers;
            // console.log("before adding to the death over simulator ", remainingOvers);
            if (remainingOvers > 0) {
              if (index != selectedPlayers.length - 1) {
                deathOverBowlers.push(player);
                // console.log("Add player to death over ", player.name);
              }
            }
          } else {
            // console.log("Contains inside death overs here ***** ", deathOverBowlers);
            // console.log("Contains inside death overs here ***** ", player.name);
          }
        } else {
          //if the player is not there in selected players already, he is in deathoverbowlers list
        }
      }
    }
    return deathOverBowlers;
  };

  onPlayerSelected = player => {
    let selectedPlayers = this.state.selectedPlayers;
    let stateRemainingOvers = this.state.remainingOvers;
    stateRemainingOvers -= 1;
    let originalCopy = this.state.originalCopy;
    let originalPlayer = this.getOriginalPlayer(player.name);
    originalCopy.splice(originalCopy.indexOf(originalPlayer), 1);
    let oversForSelectedPlayers = originalPlayer.remainingOvers;
    oversForSelectedPlayers -= 1;
    originalCopy.push({
      ...originalPlayer,
      remainingOvers: oversForSelectedPlayers
    });
    // console.log("Original copy of players after ", originalCopy);
    let deathOverBowlers;
    if (selectedPlayers.length === 0) {
      deathOverBowlers = this.state.deathOverBowlers;
      deathOverBowlers.splice(deathOverBowlers.indexOf(player), 1);
      selectedPlayers.push(player);

      this.setState({
        selectedPlayers: [...selectedPlayers],
        deathOverBowlers: [...deathOverBowlers],
        remainingOvers: stateRemainingOvers,
        originalCopy: originalCopy,
        resetOptions: false
      });
    } else {
      deathOverBowlers = this.state.deathOverBowlers;
      deathOverBowlers.splice(deathOverBowlers.indexOf(player), 1);
      selectedPlayers.push(player);
      deathOverBowlers = this.addPlayersToDeathOvers(
        originalCopy,
        deathOverBowlers,
        player.name,
        selectedPlayers
      );
      this.setState({
        selectedPlayers: [...selectedPlayers],
        deathOverBowlers: [...deathOverBowlers],
        remainingOvers: stateRemainingOvers,
        originalCopy: originalCopy,
        resetOptions: false
      });
    }
  };

  sendDeathOverRequest = resetOptions => {
    if (!this.state.receivedDataInitially) {
      return;
    }

    if (this.state.showPostDataError) {
      this.closeModal();
      return;
    }

    if (this.state.showPostRequestData || resetOptions) {
      this.setState({
        showPostRequestData: false,
        resetOptions: true
      });
    } else {
      let selectedPlayers = this.state.selectedPlayers;
      let remainingOvers = this.state.originalDataCopy.remainingOvers;
      // console.log("Reset options here ******* *********** ", selectedPlayers.length);
      // console.log("Reset options here ******* *********** ", remainingOvers);
      if (selectedPlayers.length === remainingOvers) {
        let requestToSend;
        if (this.state.useMockData) {
          requestToSend = cloneDeep(mockData);
        } else {
          requestToSend = cloneDeep(this.props.data);
        }

        // console.log("Request to send before ", requestToSend);
        requestToSend = {
          ...requestToSend,
          playerIds: [...selectedPlayers]
        };
        // console.log("Request to send after  ", requestToSend);
        this.setState({
          showPostRequestData: true
        });
        if (!this.state.useMockData) {
          this.props.postDeathBowlers(requestToSend);
        }
      }
    }
  };

  getSimulatedMatchInfo = () => {
    let postData;
    const { currentMatch } = this.props;
    if (this.state.useMockData) {
      postData = mockDataPost;
    } else {
      postData = this.props.postData;
    }

    let currentMatchData = currentMatch;
    let currentScoreBoard = currentMatchData.currentScoreCard;

    let battingTeam = currentScoreBoard.battingTeam;
    let teamA = currentMatchData.teamA;
    let teamB = currentMatchData.teamB;
    let currentBattingTeam;
    if (teamA.teamKey === battingTeam) {
      currentBattingTeam = teamA;
    } else {
      currentBattingTeam = teamB;
    }

    if (!postData) {
      return null;
    }

    if (postData && postData.length === 0) {
      return null;
    }

    if (postData && postData.status == 400) {
      return (
        <div
          style={{
            padding: "40px 0"
          }}
        >
          <div
            style={{
              textAlign: "center",
              fontWeight: 600,
              fontSize: 16
            }}
          >
            Timed out!
          </div>

          <div
            style={{
              textAlign: "center",
              fontWeight: 500,
              fontSize: 12,
              margin: "8px 0"
            }}
          >
            {"Please re-enter your death bowler choices."}
          </div>
        </div>
      );
    }

    let scoreDetails = postData.totalRuns.toString().split("");

    // console.log("Getting simulated data ********** ", scoreDetails);
    let playerDetails = postData.playerIds;

    return (
      <div
        style={{
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          paddingTop: 20,
          backgroundColor: grey_4,
          display: "flex"
        }}
      >
        <div
          style={{
            marginBottom: 10,
            flexDirection: "row",
            display: "flex"
          }}
        >
          <img
            src={
              currentBattingTeam.avatar ? currentBattingTeam.avatar : EmptyFlag
            }
            style={{
              width: "30px",
              height: "20px",
              marginRight: 10
            }}
          />

          <div style={pageStyles.teamNameFont}>
            {currentBattingTeam.fullName}
          </div>
        </div>
        <div
          style={{
            marginTop: "10px",
            marginBottom: "10px",
            flexDirection: "row",
            display: "flex"
          }}
        >
          {scoreDetails.map((item, index) => {
            return (
              <div
                key={index}
                style={{
                  backgroundColor: "#141b2f",
                  marginRight: "2px",
                  padding: "6px",
                  borderRadius: "3px"
                }}
              >
                <div style={pageStyles.scoreText}>{item}</div>
              </div>
            );
          })}

          <div
            style={{
              backgroundColor: "#141b2f",
              marginRight: 2,
              padding: 6,
              borderRadius: 3
            }}
          >
            <div style={pageStyles.scoreText}>{"-"}</div>
          </div>

          <div
            style={{
              backgroundColor: "#141b2f",
              marginRight: 2,
              padding: 6,
              borderRadius: 3
            }}
          >
            <div style={pageStyles.scoreText}>{postData.totalWickets}</div>
          </div>

          <div
            style={{
              backgroundColor: "#141b2f",
              marginRight: 2,
              padding: 6,
              borderRadius: 3,
              marginLeft: 20
            }}
          >
            <div style={pageStyles.scoreText}>{postData.totalOvers}</div>
          </div>
        </div>

        <div
          style={{
            display: "flex",
            flexDirection: "column",
            flex: 1,
            width: "100%"
          }}
        >
          <HrLine />
          <div
            style={{
              background:
                "linear-gradient(to right, #ffffff, #f0f1f2, #f0f1f2, #ffffff)",
              paddingTop: "10px",
              paddingBottom: "10px",
              justifyContent: "flex-end",
              width: "100%",
              flex: 1,
              display: "flex",
              flexDirection: "row"
            }}
          >
            <div
              style={{
                flex: 0.2
              }}
            >
              <div
                style={{
                  ...pageStyles.defaultFont,
                  textAlign: "right",
                  fontSize: 12
                }}
              >
                OVER
              </div>
            </div>

            <div
              style={{
                flex: 0.2
              }}
            >
              <div
                style={{
                  ...pageStyles.defaultFont,
                  textAlign: "right",
                  fontSize: 12
                }}
              >
                WICKETS
              </div>
            </div>

            <div
              style={{
                flex: 0.2
              }}
            >
              <div
                style={{
                  ...pageStyles.defaultFont,
                  textAlign: "center",
                  fontSize: 12
                }}
              >
                RUNS
              </div>
            </div>
          </div>
          <HrLine />
          {playerDetails.map((playerData, index) => {
            return (
              <div
                key={index}
                style={{
                  backgroundColor: "white"
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    width: "100%",
                    paddingTop: "10px",
                    paddingBottom: "10px",
                    alignItems: "center",
                    paddingLeft: "4px",
                    paddingRight: "10px"
                  }}
                >
                  <div
                    style={{
                      flex: 0.5,
                      flexDirection: "row",
                      alignItems: "center",
                      display: "flex"
                    }}
                  >
                    <img
                      src={
                        playerData.avatar
                          ? playerData.avatar
                          : FallbackPlayerImage
                      }
                      style={{
                        width: "30px",
                        height: "30px",
                        borderRadius: "50%",
                        backgroundColor: "rgba(124,125,129,1)",
                        marginRight: "10px",
                        overflow: "hidden",
                        objectFit: "contain"
                      }}
                    />

                    <div>
                      <div
                        style={{
                          ...pageStyles.defaultFont,
                          fontSize: 12,
                          color: grey_10
                        }}
                      >
                        {playerData.name}
                      </div>
                    </div>
                  </div>

                  <div
                    style={{
                      flex: 0.15
                    }}
                  >
                    <div
                      style={{
                        ...pageStyles.defaultFont,
                        fontSize: 12,
                        color: grey_10,
                        textAlign: "center"
                      }}
                    >
                      {playerData.overs}
                    </div>
                  </div>

                  <div
                    style={{
                      flex: 0.15
                    }}
                  >
                    <div
                      style={{
                        ...pageStyles.defaultFont,
                        fontSize: 12,
                        color: grey_10,
                        textAlign: "center"
                      }}
                    >
                      {playerData.wickets}
                    </div>
                  </div>

                  <div
                    style={{
                      flex: 0.15
                    }}
                  >
                    <div
                      style={{
                        ...pageStyles.defaultFont,
                        fontSize: 12,
                        color: grey_10,
                        textAlign: "right"
                      }}
                    >
                      {playerData.runs}
                    </div>
                  </div>
                </div>
                {index < playerDetails.length && <HrLine />}
              </div>
            );
          })}
        </div>
      </div>
    );
  };

  closeModal = () => {
    this.setState(
      {
        openModal: false,
        deathOverBowlers: [],
        initial: true,
        showPostDataError: false,
        postDataStatus: 200,
        postDataMessage: "",
        reFetchingData: true,
        showPostRequestData: false,
        selectedPlayers: []
      },
      () => {
        this.props.callDeathBowlers();
      }
    );
  };

  render() {
    const { data, currentMatch, playerStats, postData } = this.props;
    const { showDeathOverSimulatorScreen } = this.state;
    // console.log("Rendering state is ******** ", this.state)

    if (!currentMatch) {
      return null;
    }
    const maxOversValue =
      currentMatch && currentMatch.format == "ODI" ? 50 : 20;
    if (false && postData.status == 400) {
      return (
        <div
          style={{
            background: grey_4,
            minHeight: "90vh"
          }}
        >
          <Modal
            open={"this.state.isTrophyModal"}
            showCloseIcon={false}
            center
            styles={{
              modal: {
                width: "84%",
                borderRadius: 5
              }
            }}
          >
            <div
              style={{
                fontFamily: "mont600",
                fontSize: 18
              }}
            >
              <div
                style={{
                  textAlign: "center",
                  fontWeight: 600,
                  fontSize: 16
                }}
              >
                Timed Out!
              </div>
              <div
                style={{
                  textAlign: "center",
                  fontWeight: 500,
                  fontSize: 12,
                  marginTop: 8
                }}
              >
                Please re-enter your death bowler choices.
              </div>

              <div
                style={{
                  // width: "80%",
                  // background: red_Orange,
                  background: "#a70f14",
                  padding: "8px 0",
                  textAlign: "center",
                  marginTop: 30,
                  color: white,
                  fontSize: 14,
                  borderRadius: 3
                }}
                onClick={this.closeModal}
              >
                Try Again
              </div>
            </div>
          </Modal>
        </div>
      );
    }

    let remainingOvers = this.state.remainingOvers;
    let originalCopyRemainingOvers;
    if (this.state.originalDataCopy) {
      originalCopyRemainingOvers = this.state.originalDataCopy.remainingOvers;
    }

    console.log("******** ",remainingOvers);

    let playerDetails = this.state.deathOverBowlers;
    let selectedPlayers = this.state.selectedPlayers;
    let remainingBowlers = [];
    for (let index = 0; index < remainingOvers; index++) {
      remainingBowlers.push(index);
    }

    let teamCollection = [
      {
        teamName:
          currentMatch.teamA.teamKey === currentMatch.firstBatting
            ? currentMatch.teamA.fullName
            : currentMatch.teamB.fullName,
        shortName:
          currentMatch.teamA.teamKey === currentMatch.firstBatting
            ? currentMatch.teamA.shortName
            : currentMatch.teamB.shortName,
        teamKey:
          currentMatch.teamA.teamKey === currentMatch.firstBatting
            ? currentMatch.teamA.teamKey
            : currentMatch.teamB.teamKey,
        runs: [],
        wickets: [],
        overs: [],
        avatar:
          currentMatch.teamA.teamKey === currentMatch.firstBatting
            ? currentMatch.teamA.avatar
            : currentMatch.teamB.avatar
      },
      {
        teamName:
          currentMatch.teamA.teamKey !== currentMatch.firstBatting
            ? currentMatch.teamA.fullName
            : currentMatch.teamB.fullName,
        shortName:
          currentMatch.teamA.teamKey !== currentMatch.firstBatting
            ? currentMatch.teamA.shortName
            : currentMatch.teamB.shortName,
        teamKey:
          currentMatch.teamA.teamKey !== currentMatch.firstBatting
            ? currentMatch.teamA.teamKey
            : currentMatch.teamB.teamKey,
        runs: [],
        wickets: [],
        overs: [],
        avatar:
          currentMatch.teamA.teamKey !== currentMatch.firstBatting
            ? currentMatch.teamA.avatar
            : currentMatch.teamB.avatar
      }
    ];
    if (currentMatch.status !== "UPCOMING") {
      for (const order of currentMatch.inningOrder) {
        let team = order.split("_")[0];
        if (team === currentMatch.firstBatting) {
          (teamCollection[0].runs = [
            ...teamCollection[0].runs,
            currentMatch.inningCollection[order].runs
          ]),
            (teamCollection[0].wickets = [
              ...teamCollection[0].wickets,
              currentMatch.inningCollection[order].wickets
            ]),
            (teamCollection[0].overs = [
              ...teamCollection[0].overs,
              currentMatch.inningCollection[order].overs
            ]);
        } else {
          (teamCollection[1].runs = [
            ...teamCollection[1].runs,
            currentMatch.inningCollection[order].runs
          ]),
            (teamCollection[1].wickets = [
              ...teamCollection[1].wickets,
              currentMatch.inningCollection[order].wickets
            ]),
            (teamCollection[1].overs = [
              ...teamCollection[1].overs,
              currentMatch.inningCollection[order].overs
            ]);
        }
      }
    }

    let showCannotSimulate = false;

    if (
      originalCopyRemainingOvers !== selectedPlayers.length &&
      playerDetails.length === 0
    ) {
      showCannotSimulate = true;
    }

    // console.log(" player details are ***** ", playerDetails);
    // console.log(" player details are ***** ", this.state);

    // if (playerDetails && playerDetails.length === 0 || !this.state.receivedDataInitially) {
    //     console.log("Here *****************")
    // }
    let bowlingOrder =
      (currentMatch &&
        currentMatch.currentScoreCard &&
        currentMatch.currentScoreCard.bowlingOrder) ||
      [];

    return (
      <React.Fragment>
        <div
          style={{
            flex: 1,
            backgroundColor: grey_4,
            paddingTop: "10px"
          }}
        >
          <div
            style={{
              paddingTop: "10px",
              backgroundColor: "#edeff4",
              marginBottom: "10px",
              flex: 0.2
            }}
          >
            <div
              style={{
                paddingLeft: "16px",
                paddingRight: "16px"
              }}
            >
              <div
                style={{
                  ...pageStyles.headerFont,
                  marginBottom: "10px"
                }}
              >
                Death Overs Simulator
              </div>
              {!showDeathOverSimulatorScreen && (
                <div
                  style={{
                    fontSize: 14,
                    fontWeight: 500,
                    marginBottom: 6
                  }}
                >
                  What is Death Overs Simulator?
                </div>
              )}
              {!showDeathOverSimulatorScreen && (
                <div style={pageStyles.defaultFont}>
                  Pick a bowler from current bowling team to see how the match
                  changes the direction with your selection.
                </div>
              )}
              {showDeathOverSimulatorScreen && (
                <div style={pageStyles.defaultFont}>
                  What happens if a spinner bowls the last over? Pick the
                  bowlers and simulate the final score.
                </div>
              )}
            </div>
            {/* =============== */}
            {!showDeathOverSimulatorScreen && (
              <div
                style={
                  {
                    // minHeight: "69vh",
                    // position: "relative"
                  }
                }
              >
                <div
                  style={{
                    display: "flex",
                    padding: 16,
                    background: white,
                    margin: "10px 0",
                    fontSize: 14,
                    fontWeight: 400
                  }}
                >
                  <div
                    style={{
                      width: 56,
                      height: 38,
                      marginRight: 10
                    }}
                  >
                    <img
                      src={
                        currentMatch &&
                        currentMatch.teamA &&
                        currentMatch.teamA.teamKey &&
                        currentMatch.currentScoreCard &&
                        currentMatch.currentScoreCard.teamKey
                          ? currentMatch.teamA.teamKey ==
                            currentMatch.currentScoreCard.teamKey
                            ? currentMatch.teamA.avatar
                            : currentMatch.teamB.avatar
                          : require("../../../images/flag_empty.svg")
                      }
                      alt=""
                      style={{ width: "100%" }}
                    />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center"
                    }}
                  >
                    {currentMatch &&
                      currentMatch.currentScoreCard &&
                      currentMatch.currentScoreCard.inningId > 1 && (
                        <div
                          style={{
                            color: grey_8
                          }}
                        >
                          {(currentMatch && currentMatch.statusStr) || ""}
                          {/* <span>AUS</span> needs <span>31</span> runs to win */}
                        </div>
                      )}
                    <div>
                      <span
                        style={{
                          fontWeight: 500
                        }}
                      >
                        Current Score
                      </span>
                      <span
                        style={{
                          fontWeight: 600,
                          marginLeft: 6
                        }}
                      >
                        {currentMatch && currentMatch.currentScoreCard
                          ? currentMatch.currentScoreCard.runs +
                            "/" +
                            currentMatch.currentScoreCard.wickets +
                            " (" +
                            currentMatch.currentScoreCard.overs +
                            ")"
                          : ""}
                      </span>
                    </div>
                  </div>
                </div>

                <div
                  style={{
                    background: grey_10,
                    padding: 16,
                    color: white
                  }}
                >
                  {currentMatch &&
                  currentMatch.currentScoreCard &&
                  currentMatch.currentScoreCard.bowlingTeam &&
                  currentMatch.teamA &&
                  currentMatch.teamB &&
                  currentMatch.teamA.teamKey ==
                    currentMatch.currentScoreCard.bowlingTeam
                    ? currentMatch.teamA.shortName || ""
                    : currentMatch.teamB.shortName || ""}{" "}
                  Bowling Options
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    minHeight: "50vh",
                    justifyContent: "space-between"
                  }}
                >
                  <div
                    style={
                      {
                        // overflowY: "scroll"
                      }
                    }
                  >
                    <div
                      style={{
                        display: "flex",
                        flex: 1,
                        background: gradientGrey,
                        color: grey_8,
                        textAlign: "center",
                        padding: 11,
                        fontSize: 10
                      }}
                    >
                      <div style={{ flex: 0.4, textAlign: "left" }}>BOWLER</div>
                      <div style={{ flex: 0.12 }}>O</div>
                      <div style={{ flex: 0.12 }}>M</div>
                      <div style={{ flex: 0.12 }}>R</div>
                      <div style={{ flex: 0.12 }}>W</div>
                      <div style={{ flex: 0.12 }}>ECO</div>
                    </div>
                    <HrLine />
                    {/* {[1, 1, 1].map(item => ( */}
                    {playerStats &&
                      map(
                        playerStats,
                        (player, key) =>
                          bowlingOrder.indexOf(key) > -1 &&
                          currentMatch &&
                          currentMatch.currentScoreCard &&
                          currentMatch.currentScoreCard.inningId && (
                            <div>
                              <div
                                style={{
                                  display: "flex",
                                  flex: 1,
                                  background: white,
                                  color: grey_8,
                                  textAlign: "center",
                                  padding: "13px 11px",
                                  fontSize: 10
                                }}
                              >
                                <div
                                  style={{
                                    flex: 0.4,
                                    textAlign: "left",
                                    color: grey_10
                                  }}
                                >
                                  {/* {player && player.bowlingStatistics && player.bowlingStatistics[currentMatch.currentScoreCard.inningId]} */}
                                  {(player && player.playerName) || "-"}
                                  {/* Kuldeep Yadav */}
                                </div>
                                {/* <div style={{ flex: 0.12, color: grey_10 }}>3</div> */}
                                <div style={{ flex: 0.12, color: grey_10 }}>
                                  {(player &&
                                    player.bowlingStatistics &&
                                    player.bowlingStatistics[
                                      currentMatch.currentScoreCard.inningId
                                    ] &&
                                    player.bowlingStatistics[
                                      currentMatch.currentScoreCard.inningId
                                    ].overs) ||
                                    "0"}
                                </div>
                                <div style={{ flex: 0.12 }}>
                                  {(player &&
                                    player.bowlingStatistics &&
                                    player.bowlingStatistics[
                                      currentMatch.currentScoreCard.inningId
                                    ] &&
                                    player.bowlingStatistics[
                                      currentMatch.currentScoreCard.inningId
                                    ].maidenOvers) ||
                                    "0"}
                                </div>
                                <div style={{ flex: 0.12 }}>
                                  {(player &&
                                    player.bowlingStatistics &&
                                    player.bowlingStatistics[
                                      currentMatch.currentScoreCard.inningId
                                    ] &&
                                    player.bowlingStatistics[
                                      currentMatch.currentScoreCard.inningId
                                    ].runs) ||
                                    "0"}
                                </div>
                                <div style={{ flex: 0.12 }}>
                                  {(player &&
                                    player.bowlingStatistics &&
                                    player.bowlingStatistics[
                                      currentMatch.currentScoreCard.inningId
                                    ] &&
                                    player.bowlingStatistics[
                                      currentMatch.currentScoreCard.inningId
                                    ].wickets) ||
                                    "0"}
                                </div>
                                <div style={{ flex: 0.12 }}>
                                  {(player &&
                                    player.bowlingStatistics &&
                                    player.bowlingStatistics[
                                      currentMatch.currentScoreCard.inningId
                                    ] &&
                                    player.bowlingStatistics[
                                      currentMatch.currentScoreCard.inningId
                                    ].economy) ||
                                    "0"}
                                </div>
                              </div>
                              <HrLine />
                            </div>
                          )
                      )}
                  </div>
                  <div
                    style={{
                      // margin: 30,
                      padding: 14,
                      background: "#9b000d",
                      textAlign: "center",
                      color: white,
                      fontSize: 14,
                      fontWeight: 600
                      // borderRadius: 3,
                      // position: "absolute",
                      // bottom: 0,
                      // left: 0,
                      // right: 0
                    }}
                    onClick={() => {
                      this.setState({
                        showDeathOverSimulatorScreen: true
                      });
                    }}
                  >
                    Start Simulation
                  </div>
                </div>
              </div>
            )}
            {/* ================ */}
            {false &&
              showDeathOverSimulatorScreen && (
                <FactEngiene
                  data={currentMatch && currentMatch.factEngine}
                  type={"matchups"}
                />
              )}
          </div>

          {showDeathOverSimulatorScreen && (
            <div
              style={{
                flex: 0.8,
                backgroundColor: "white"
              }}
            >
              {currentMatch && !this.state.showPostRequestData ? (
                <div
                  style={{
                    display: "flex",
                    flex: 1,
                    flexDirection: "column"
                  }}
                >
                  <div
                    style={{
                      width: "100%",
                      paddingTop: "10px",
                      paddingBottom: "10px",
                      marginBottom: "10px",
                      paddingLeft: "4px",
                      paddingRight: "4px",
                      display: "flex",
                      flexDirection: "row"
                    }}
                  >
                    {teamCollection &&
                      teamCollection.map((teamData, index) => {
                        // console.log("Looping inside teamData :- " + teamData);
                        return (
                          <div
                            style={{
                              display: "flex",
                              flex: 0.5,
                              flexDirection: "row"
                            }}
                            key={index}
                          >
                            <div
                              style={{
                                marginRight: "10px"
                              }}
                            >
                              <img
                                src={
                                  teamData.avatar ? teamData.avatar : EmptyFlag
                                }
                                style={{
                                  width: "30px",
                                  height: "20px"
                                }}
                              />
                            </div>
                            <div
                              style={{
                                ...pageStyles.teamNameFont,
                                marginRight: "4px"
                              }}
                            >
                              {teamData.shortName}
                            </div>

                            <div
                              style={{
                                ...pageStyles.teamNameFont,
                                color: "#727682"
                              }}
                            >
                              {this.getTeamScore(teamData)}
                            </div>
                          </div>
                        );
                      })}
                  </div>
                  <HrLine />
                  {remainingBowlers.length > 0 && (
                    <div
                      style={{
                        textAlign: "center",
                        fontFamily: "Montserrat",
                        fontSize: "14px"
                      }}
                    >
                      Pick your choice of Bowlers
                    </div>
                  )}

                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      marginTop: "10px",
                      marginBottom: "10px",
                      width: "100%",
                      justifyContent: "center",
                      flex: 1,
                      overflow: "hidden"
                    }}
                  >
                    {selectedPlayers.map((playerData, index) => {
                      let avatar = playerData.avatar;
                      let name = playerData.name;
                      return (
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "center",
                            maxWidth: "80px",
                            marginRight: "2px",
                            flex: 1
                          }}
                          key={index}
                        >
                          <img
                            src={avatar ? avatar : FallbackPlayerImage}
                            style={{
                              width: "60px",
                              height: "60px",
                              borderRadius: "50%",
                              backgroundColor: "rgba(124,125,129,1)",
                              objectFit: "contain",
                              borderStyle: "solid",
                              borderWidth: "1px",
                              borderColor: "#d44030"
                            }}
                          />

                          <div
                            style={{
                              width: "75%",
                              textAlign: "center",
                              fontSize: "7px",
                              color: grey_10,
                              ...pageStyles.teamNameFont
                            }}
                          >
                            {name}
                          </div>
                        </div>
                      );
                    })}

                    {remainingBowlers.reverse().map((value, index) => {
                      return (
                        <div
                          style={{
                            // marginRight: "20px",
                            // flex: 1,
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "center"
                          }}
                          key={index}
                        >
                          <img
                            src={DeathOversNormalImage}
                            style={{
                              width: "60px",
                              height: "60px",
                              borderRadius: "50%",
                              borderStyle: index === 0 ? "solid" : "none",
                              borderWidth: "1px",
                              borderColor: "#d44030"
                            }}
                          />

                          <div
                            style={{
                              ...pageStyles.defaultFontBlack,
                              textAlign: "center"
                            }}
                          >
                            {/* {"Over " + (this.state.maxOvers - value)} */}
                            {"Over " + (maxOversValue - value)}
                          </div>
                        </div>
                      );
                    })}
                  </div>

                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: 1,
                      minHeight: "200px"
                    }}
                  >
                    {remainingOvers > 0 &&
                      playerDetails &&
                      playerDetails.map((value, index) => {
                        let avatar = value.avatar;
                        let name = value.name;
                        let bowlerData =
                          value.overs +
                          " - " +
                          (value.maiden ? value.maiden : "0") +
                          " - " +
                          value.runs +
                          " - " +
                          value.wickets;
                        return (
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "column"
                            }}
                            key={index}
                          >
                            <div
                              onClick={() => {
                                this.onPlayerSelected(value);
                              }}
                              style={{
                                paddingTop: "10px",
                                paddingBottom: "10px",
                                marginBottom: "4px",
                                paddingLeft: "10px",
                                paddingRight: "10px",
                                flexDirection: "row",
                                flex: 1,
                                display: "flex"
                              }}
                            >
                              <div
                                style={{
                                  display: "flex",
                                  flex: 0.8,
                                  flexDirection: "row",
                                  alignItems: "center"
                                }}
                              >
                                <img
                                  src={avatar ? avatar : FallbackPlayerImage}
                                  style={{
                                    width: "50px",
                                    height: "50px",
                                    borderRadius: "50%",
                                    backgroundColor: "rgba(124,125,129,1)",
                                    marginRight: "10px",
                                    objectFit: "contain"
                                  }}
                                />

                                <div
                                  style={{
                                    display: "flex",
                                    flexDirection: "column",
                                    justifyContent: "space-between"
                                  }}
                                >
                                  <div
                                    style={{
                                      ...pageStyles.playerNameFont,
                                      marginBottom: "4px"
                                    }}
                                  >
                                    {name}
                                  </div>

                                  <div style={pageStyles.bowlerDataFont}>
                                    {bowlerData}
                                  </div>
                                </div>
                              </div>

                              <div
                                style={{
                                  flex: 0.2,
                                  display: "flex",
                                  justifyContent: "center",
                                  alignItems: "center"
                                }}
                              >
                                <img
                                  src={PlayerIconNormal}
                                  style={{
                                    width: "30px",
                                    height: "30px",
                                    borderRadius: "50%",
                                    objectFit: "contain"
                                  }}
                                />
                              </div>
                            </div>
                            {index < playerDetails.length ? <HrLine /> : null}
                          </div>
                        );
                      })}

                    {playerDetails &&
                      playerDetails.length === 0 &&
                      !this.state.receivedDataInitially && (
                        <div
                          style={{
                            flex: 1,
                            paddingTop: "20px",
                            backgroundColor: grey_4,
                            display: "flex",
                            justifyContent: "center",
                            paddingLeft: "10px",
                            paddingRight: "10px"
                          }}
                        >
                          <div
                            style={{
                              ...pageStyles.teamNameFont,
                              fontSize: 14,
                              textAlign: "left",
                              wordWrap: "break-word"
                            }}
                          >
                            Death over simulator is closed for this innings
                          </div>
                        </div>
                      )}

                    {showCannotSimulate && (
                      <div
                        style={{
                          flex: 1,
                          paddingTop: 40,
                          alignItems: "center",
                          backgroundColor: grey_4,
                          minHeight: "200px"
                        }}
                      >
                        <div
                          style={{
                            ...pageStyles.teamNameFont,
                            fontSize: 14,
                            textAlign: "center"
                          }}
                        >
                          Cannot simulate with selected bowlers.
                        </div>
                      </div>
                    )}
                  </div>

                  <div
                    style={{
                      width: "100%",
                      paddingTop: "14px",
                      paddingBottom: "14px",
                      justifyContent: "center",
                      display: "flex",
                      // flex: 1,
                      alignItems: "center",
                      lineHeight: "18px",
                      verticalAlign: "middle",
                      background:
                        (remainingOvers === 0 &&
                          this.state.receivedDataInitially) ||
                        showCannotSimulate
                          ? "linear-gradient(to left, #ba5222, #a70f14)"
                          : "rgba(160,163,170,1)"
                    }}
                    onClick={() => {
                      if (showCannotSimulate) {
                        this.sendDeathOverRequest(true);
                      } else {
                      }
                      this.sendDeathOverRequest(false);
                    }}
                  >
                    <div style={pageStyles.simulateFont}>
                      {this.state.showPostRequestData || showCannotSimulate
                        ? "Try another option"
                        : "Simulate"}
                    </div>
                  </div>
                </div>
              ) : (
                <div
                  style={{
                    minHeight:
                      postData && postData.status == 400 ? "70vh" : "200px",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "space-between"
                  }}
                >
                  {postData &&
                    postData.status !== 400 && (
                      <div
                        style={{
                          marginBottom: "10px",
                          paddingTop: "10px",
                          width: "100%",
                          paddingLeft: "10px",
                          flexDirection: "column",
                          justifyContent: "flex-start"
                        }}
                      >
                        <div
                          style={{
                            ...pageStyles.teamNameFont,
                            marginBottom: 10
                          }}
                        >
                          {"Projected Score"}
                        </div>

                        <HrLine />
                      </div>
                    )}

                  {this.getSimulatedMatchInfo()}

                  <div
                    style={{
                      width: "100%",
                      paddingTop: "14px",
                      paddingBottom: "14px",
                      justifyContent: "center",
                      display: "flex",
                      // flex: 1,
                      alignItems: "center",
                      lineHeight: "18px",
                      verticalAlign: "middle",
                      background:
                        remainingOvers === 0
                          ? "linear-gradient(to left, #ba5222, #a70f14)"
                          : "rgba(160,163,170,1)"
                    }}
                    onClick={this.sendDeathOverRequest}
                  >
                    <div style={pageStyles.simulateFont}>
                      Try another option
                    </div>
                  </div>
                </div>
              )}
            </div>
          )}
        </div>
      </React.Fragment>
    );
  }
}

const pageStyles = {
  headerFont: {
    fontFamily: "Montserrat",
    fontWeight: "bold",
    color: grey_10,
    fontSize: "20px"
  },

  defaultFont: {
    color: "#727682",
    fontSize: "14px",
    fontFamily: "Montserrat"
  },

  teamNameFont: {
    fontFamily: "Montserrat",
    fontSize: "14px",
    color: grey_10
  },

  playerNameFont: {
    fontFamily: "Montserrat",
    fontWeight: 500,
    fontSize: "14px",
    color: grey_10
  },
  bowlerDataFont: {
    color: "#727682",
    fontSize: "12px",
    fontFamily: "Montserrat",
    fontWeight: 500
  },
  simulateFont: {
    color: "white",
    fontSize: "18px",
    fontFamily: "Montserrat",
    fontWeight: 500
  },
  scoreText: {
    fontFamily: "Oswald",
    fontWeight: 500,
    fontSize: 28,
    color: "#fadd8d"
  },
  defaultFontBlack: {
    fontFamily: "Montserrat",
    fontSize: "8px",
    color: grey_10
  }
};

export default NewDeathOverSimulator;
