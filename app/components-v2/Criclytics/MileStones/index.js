import React, { PureComponent } from "react";

import Fact from "../../Fact";
import { white } from "../../../styleSheet/globalStyle/color";
import ListItem from "../../commonComponent/ListItems";
import HrLine from "../../commonComponent/HrLine";
const kohli = require("../../../images/kohli.png");
const data = [
  {
    name: "a",
    pv: 50
  },
  {
    name: "b",
    pv: 20
  },
  {
    name: "c",
    pv: 10
  }
];
export class MileStone extends PureComponent {
  returnLoader = percent => (
    <React.Fragment>
      <div
        style={{
          padding: "0px 20px"
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "space-between"
          }}
        >
          <div style={styles.textStyles}>Current Carrer Test Runs</div>
          <div style={styles.textStyles}>Crossed</div>
        </div>
        <div
          style={{
            borderRadius: "10px",
            boxShadow: "inset 0 0 2px 0 rgba(196, 202, 214, 0.5)",
            backgroundColor: " #e8ebf3",
            position: "relative"
          }}
        >
          <div
            style={{
              position: "absolute",
              position: "absolute",
              // width: "2px",
              // height: "6px",
              top: -6.5,
              bottom: -6.5,
              left: `${percent}%`,

              borderLeft: "1px solid #000"
            }}
          />
          <div
            style={{
              position: "absolute",
              position: "absolute",
              // width: "2px",
              // height: "6px",
              top: -6.5,
              bottom: -6.5,
              left: `${percent + 40}%`,
              borderLeft: "1px solid #000"
            }}
          />
          <div
            style={{
              borderRadius: "3px",
              backgroundImage: "linear-gradient(83deg, #6254d2, #3fadd2)",
              width: `${percent}%`,
              height: "7px"
            }}
          />
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between"
          }}
        >
          <div style={styles.textStyles}>10887</div>
          <div style={styles.textStyles}>11000</div>
        </div>
      </div>
      <HrLine />
    </React.Fragment>
  );
  render() {
    const normal = "normal";
    return (
      <React.Fragment>
        <Fact
          fact={
            "In reprehenderit consequat amet et occaecat quis labore dolore elit nostrud sint ut."
          }
          header="MileStones"
          headerDescription="Some of your favorite players are nearing their personal milestones. Stay tuned"
        />
        <div
          style={{
            padding: "0px 16px"
          }}
        >
          <div
            style={{
              background: white
            }}
          >
            <div
              style={{
                fontFamily: " Montserrat",
                fontSize: "12px",
                fontWeight: 500,
                fontStyle: normal,
                fontStretch: normal,
                lineHeight: normal,
                letterSpacing: normal,
                color: "#141b2f"
              }}
            >
              Milestones
            </div>
            <ListItem
              isHR={true}
              avatarStyle={{
                width: "60px",
                height: "60px",
                objectFit: "contain",
                borderRadius: "50%"
              }}
              rightElement={<div />}
              belowTitle="India Batsman"
              isAvatar
              avatar={kohli}
              title="Ravindra Jadeja"
            />

            {this.returnLoader(30)}
            <ListItem
              isHR={true}
              avatarStyle={{
                width: "60px",
                height: "60px",
                objectFit: "contain",
                borderRadius: "50%"
              }}
              rightElement={<div />}
              belowTitle="Australia Batsman"
              isAvatar
              avatar={kohli}
              title="Nathan Lyon"
            />

            {this.returnLoader(60)}
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const normal = "normal";
const styles = {
  textStyles: {
    opacity: 0.8,
    fontFamily: "Montserrat",
    fontSize: "10px",
    fontWeight: 500,
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: normal,
    letterSpacing: normal,
    color: "#141b2f"
  }
};
export default MileStone;
