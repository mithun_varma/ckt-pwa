import React, { Component } from "react";
import CardGradientTitle from "../commonComponent/CardGradientTitle";
import {
  white,
  grey_5,
  grey_5_1,
  red_10,
  grey_8,
  grey_10,
  red_Orange,
  gradientBlack
} from "../../styleSheet/globalStyle/color";
import BallComponent from "../commonComponent/BallComponent";
import ClipTitle from "./CriclyticsCommon/ClipTitle";
import StatsCard from "./CriclyticsCommon/StatsCard";
import HrLine from "../commonComponent/HrLine";
import CriclyticsListItems from "./CriclyticsCommon/CriclyticsListItems";
import PillTabs from "../../components/Common/PillTabs";
import groupBy from "lodash/groupBy";
import isEmpty from "lodash/isEmpty";
import ChevronRight from "@material-ui/icons/ChevronRight";
import { isAndroid } from "react-device-detect";

class CriclyticsScheduleHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activetab: "",
      first: true
    };
  }

  static getDerivedStateFromProps(nextProps, currentState) {
    if (
      currentState.first &&
      nextProps.currentScoreData &&
      nextProps.currentScoreData.teamA
    ) {
      return {
        activetab: nextProps.currentScoreData.teamA.shortName,
        activeTeamKey: nextProps.currentScoreData.teamA.teamKey,
        first: false
      };
    }
    return null;
  }

  // redirecToCriclyticsSlider = () => {
  //   if (
  //     this.props.currentScoreData &&
  //     (this.props.currentScoreData.matchId ||
  //       this.props.currentScoreData.matchid)
  //   ) {
  //     this.props.history.push(
  //       `/criclytics-slider/${this.props.currentScoreData.matchId ||
  //         this.props.currentScoreData.matchid}`
  //     );
  //   }
  // };

  handleTabSelect = val => {
    this.setState({
      activetab: val,
      activeTeamKey:
        val === this.props.currentScoreData.teamA.shortName
          ? this.props.currentScoreData.teamA.teamKey
          : this.props.currentScoreData.teamB.teamKey
    });
  };
  render() {
    const { criclyticsData, currentScoreData } = this.props;
    let groupedPlayerList = groupBy(
      criclyticsData.preMatchPlayerProbsFinal.playerList,
      data => data.teamId
    );
    return criclyticsData.preMatchPlayerProbsFinal.matchId ===
      currentScoreData.matchId ? (
      <div>
        <div
          style={{
            padding: "8px 8px 8px 16px",
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            background: gradientBlack,
            borderRadius: "3px 3px 0 0",
            position: "relative"
          }}
          onClick={this.props.redirecToCriclyticsSlider}
        >
          <div
            style={{
              position: "absolute",
              width: 0,
              height: 0,
              borderLeft: "14px solid transparent",
              borderRight: "14px solid transparent",
              borderBottom: "14px solid #353e59",
              top: -13,
              left: 16
            }}
          />
          <div
            style={{
              display: "flex",
              alignItems: "center"
            }}
          >
            <img
              src={require("../../images/criclytics.svg")}
              alt=""
              style={{
                width: 26,
                height: 26
              }}
            />
            <span
              style={{
                color: white,
                marginLeft: 8
              }}
            >
              Criclytics
            </span>
          </div>
          <ChevronRight style={{ color: white }} />
        </div>
        {!isEmpty(criclyticsData.head2headStats) && (
          <div>
            {/* Title Section */}
            <CardGradientTitle
              title="RECENT FORM"
              isRightIcon
              rootStyles={{
                padding: "8px 8px 8px 16px",
                fontSize: 10
              }}
              handleCardClick={this.props.redirecToCriclyticsSlider}
            />
            {/* Recent Form Ball Stats Sections */}
            <div
              style={{
                display: "flex",
                flex: 1,
                background: white,
                padding: "12px 0 24px"
              }}
            >
              {/* Last Five Match Status section */}
              {/* 1 */}
              <div
                style={{
                  flex: 0.5,
                  textAlign: "center",
                  // background: "lightgrey",
                  borderRight: `1px solid ${grey_5}`
                }}
              >
                <div
                  style={{
                    fontSize: 12
                  }}
                >
                  {criclyticsData.head2headStats.teamA.teamShortName}
                </div>
                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    flex: 1,
                    justifyContent: "space-evenly",
                    // background: "linear-gradient(to right, white, transparent 90%)",
                    // background: "linear-gradient(to right, #ffffff, #00ffff)",
                    paddingTop: 8,
                    overflow: "hidden"
                  }}
                >
                  <BallComponent
                    // balls={["L", "W", "L", "L", "W"]}
                    balls={criclyticsData.head2headStats.teamA.lastFStatus}
                    type="match"
                  />
                  <div
                    style={{
                      position: "absolute",
                      top: 0,
                      left: 0,
                      right: 0,
                      bottom: 0,
                      background: isAndroid
                        ? "linear-gradient(to right, white, transparent 60%)"
                        : "transparent"
                    }}
                  />
                </div>
              </div>

              {/* 2 */}
              <div
                style={{
                  flex: 0.5,
                  textAlign: "center"
                  // background: "lightgrey",
                  // borderRight: `1px solid ${grey_5}`
                }}
              >
                <div
                  style={{
                    fontSize: 12
                  }}
                >
                  {criclyticsData.head2headStats.teamB.teamShortName}
                </div>
                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    flex: 1,
                    justifyContent: "space-evenly",
                    // background: "linear-gradient(to right, white, transparent 90%)",
                    // background: "linear-gradient(to right, #ffffff, #00ffff)",
                    paddingTop: 8,
                    overflow: "hidden"
                  }}
                >
                  <BallComponent
                    // balls={["L", "W", "L", "L", "W"]}
                    balls={criclyticsData.head2headStats.teamB.lastFStatus}
                    type="match"
                  />
                  <div
                    style={{
                      position: "absolute",
                      top: 0,
                      left: 0,
                      right: 0,
                      bottom: 0,
                      background: isAndroid
                        ? "linear-gradient(to right, white, transparent 60%)"
                        : "transparent"
                    }}
                  />
                </div>
              </div>
              {/* Last Five Match Status section closing */}
            </div>
            {/* Recent Form Ball Stats Sections closing */}

            {/* Head-to-Head Sections starts */}
            {criclyticsData &&
              criclyticsData.head2headStats &&
              criclyticsData.head2headStats.head2Head &&
              criclyticsData.head2headStats.head2Head.matches && (
                <div
                  style={{
                    position: "relative",
                    fontFamily: "Montserrat",
                    fontWeight: 400,
                    fontSize: 9,
                    color: grey_10
                    // borderTop: `1px solid ${grey_5_1}`
                  }}
                >
                  <ClipTitle
                    title="HEAD-TO-HEAD"
                    subText={
                      currentScoreData &&
                      currentScoreData.format &&
                      currentScoreData.format === "TEST"
                        ? " Last 10 Matches"
                        : " Last 3 Years"
                    }
                  />
                  {/* stats Card section */}
                  <div
                    style={{
                      display: "flex",
                      flex: 1,
                      padding: "10px 0 18px"
                    }}
                  >
                    <StatsCard
                      rootStyles={{
                        flex: 0.25
                      }}
                      title={criclyticsData.head2headStats.head2Head.matches}
                      subtitle="Matches"
                    />
                    <StatsCard
                      rootStyles={{
                        flex: 0.25
                      }}
                      title={criclyticsData.head2headStats.head2Head.teamA}
                      subtitle={
                        criclyticsData.head2headStats.teamA.teamShortName
                      }
                    />
                    <StatsCard
                      rootStyles={{
                        flex: 0.25
                      }}
                      title={criclyticsData.head2headStats.head2Head.teamB}
                      subtitle={
                        criclyticsData.head2headStats.teamB.teamShortName
                      }
                    />
                    <StatsCard
                      rootStyles={{
                        flex: 0.25,
                        border: "none"
                      }}
                      title={criclyticsData.head2headStats.head2Head.other}
                      subtitle={`${
                        currentScoreData &&
                        currentScoreData.format &&
                        currentScoreData.format === "TEST"
                          ? "Draw"
                          : "Tie/NR"
                      }`}
                    />
                    {/* criclyticsData.head2headStats.head2Head */}
                  </div>
                  {/* stats Card section closing */}
                </div>
              )}
            {/* Head-to-Head Sections closing */}

            {/* Stadium Details Sections starts */}
            {criclyticsData.head2headStats.venueStats.avgFirstInningScore && (
              <div
                style={{
                  position: "relative",
                  fontFamily: "Montserrat",
                  fontWeight: 400,
                  fontSize: 9,
                  color: grey_10
                  // borderTop: `1px solid ${grey_5_1}`
                }}
              >
                <ClipTitle
                  title="STADIUM DETAILS"
                  subText={
                    currentScoreData &&
                    currentScoreData.format &&
                    currentScoreData.format === "TEST"
                      ? " Last 5 Years"
                      : " Last 3 Years"
                  }
                />
                {/* title section */}
                {/* <div
                style={{
                  textAlign: "right",
                  marginRight: 15,
                  padding: "6px 0"
                }}
              >
                <span style={{ color: red_10, fontWeight: 600 }}>*</span>
                <span
                  style={{
                    fontWeight: 500,
                    fontSize: 10,
                    color: grey_8
                  }}
                >
                  For Last 3 Years
                </span>
              </div> */}
                {criclyticsData.head2headStats.venueStats.venueName && (
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      padding: "4px 12px"
                    }}
                  >
                    <img
                      src={require("../../images/location-icon-color.png")}
                      alt=""
                      style={{
                        width: 9,
                        height: 12
                      }}
                    />
                    <span
                      style={{
                        fontSize: 11,
                        fontWeight: 500,
                        marginLeft: 6
                      }}
                    >
                      {/* {criclyticsData.head2headStats.venueStats.venueName || "--"},{" "} */}
                      {criclyticsData.head2headStats.venueStats.venueCity ||
                        "--"}
                    </span>
                  </div>
                )}
                {/* stats Card section */}
                <div
                  style={{
                    display: "flex",
                    flex: 1,
                    padding: "10px 0 18px"
                  }}
                >
                  <StatsCard
                    rootStyles={{
                      flex: 0.34
                    }}
                    title={
                      criclyticsData.head2headStats.venueStats
                        .avgFirstInningScore || "--"
                    }
                    subtitle={
                      currentScoreData &&
                      currentScoreData.format &&
                      currentScoreData.format === "TEST"
                        ? "1st Innings Average Score"
                        : "1st Batting Average Score"
                    }
                  />
                  <StatsCard
                    rootStyles={{
                      flex: 0.34
                    }}
                    title={
                      criclyticsData.head2headStats.venueStats
                        .firstBattingWinPercent !== null
                        ? `${
                            criclyticsData.head2headStats.venueStats
                              .firstBattingWinPercent
                          }%`
                        : "--"
                    }
                    subtitle="1st Batting Winning"
                  />
                  <StatsCard
                    rootStyles={{
                      flex: 0.34
                    }}
                    title={
                      criclyticsData.head2headStats.venueStats
                        .highestScoreChased || "--"
                    }
                    subtitle={
                      currentScoreData &&
                      currentScoreData.format &&
                      currentScoreData.format === "TEST"
                        ? "Highest 4th Innings Chase"
                        : "Highest Score Chased"
                    }
                  />
                </div>
                {/* stats Card section closing */}
              </div>
            )}
          </div>
        )}
        {/* Stadium Details Sections closing */}
        {/* Projected Top Performances Sectrion Starts */}
        {!isEmpty(groupedPlayerList) && (
          <div>
            <HrLine />
            <CardGradientTitle
              title="PLAYER PROJECTION"
              isRightIcon
              rootStyles={{
                padding: "8px 8px 8px 12px",
                fontSize: 10
              }}
              handleCardClick={this.props.redirecToCriclyticsSlider}
            />
            <div>
              {/* pill tabs section starts */}
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  padding: 12
                }}
              >
                <span
                  style={{
                    fontWeight: 500,
                    fontSize: 12
                  }}
                >
                  Projected Top Performances
                </span>
                <PillTabs
                  items={[
                    currentScoreData.teamA.shortName,
                    currentScoreData.teamB.shortName
                  ]}
                  activeItem={this.state.activetab}
                  onTabSelect={this.handleTabSelect}
                  rootStyles={{ width: "26%" }}
                />
              </div>

              {/* ListItems Starts  */}
              {/* <div>{[1, 2, 3].map(item => <CriclyticsListItems />)}</div> */}
              <div>
                {groupedPlayerList[this.state.activeTeamKey].map(
                  (item, key) => <CriclyticsListItems data={item} key={key} />
                )}
              </div>

              {/* ListItems closing  */}

              <div
                style={{
                  padding: "20px 16px",
                  textAlign: "right"
                }}
              >
                <span
                  style={{
                    background: red_Orange,
                    padding: "6px 18px",
                    borderRadius: 18,
                    fontSize: 12,
                    fontWeight: 600,
                    color: white
                  }}
                  onClick={this.props.handleFantasy}
                >
                  Fantasy Research Centre
                </span>
              </div>
            </div>
          </div>
        )}
      </div>
    ) : (
      <div>...loading</div>
    );
    // {/* Projected Top Performances Sectrion closing */}
  }
}

export default CriclyticsScheduleHome;
