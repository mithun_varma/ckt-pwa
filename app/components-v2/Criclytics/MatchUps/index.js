import React, { PureComponent } from "react";
import PlayerProfile from "./PlayerProfile";
import size from "lodash/size";

import Fact from "../../Fact";
import { white } from "../../../styleSheet/globalStyle/color";
import HrLine from "../../commonComponent/HrLine";

export class PlayerComparision extends PureComponent {
  state = {
    toggle: {
      0: true
    }
  };

  handleToggleMatchups = index => {
    this.setState({
      toggle: {
        [index]: !this.state.toggle[index]
      }
    });
  };

  render() {
    const { data, currentMatch } = this.props;

    return size(data) > 0 ? (
      <div
        style={{
          padding: "0px 2px"
        }}
      >
        <div style={{ background: "#edeff4" }}>
          <Fact
            header="Player Match Ups"
            headerDescription="What happens when superstars collide? Tap the matchups to find interesting tidbits."
            fact={
              currentMatch && currentMatch.factEngine && currentMatch.factEngine
            }
          />
          <div>
            <div
              style={{
                background: white
              }}
            >
              <div
                style={{
                  ...styles.headerText,
                  flex: 1,
                  alignSelf: "center",
                  padding: "12px 16px"
                }}
              >
                Top 3 Match Ups
                <div
                  style={{
                    padding: "6px 0px"
                  }}
                />
                <HrLine />
              </div>

              {data.matchUpData &&
                data.matchUpData.map((ele, ind) => (
                  <div
                    key={ind}
                    style={{
                      padding: "12px 0px"
                    }}
                  >
                    <PlayerProfile
                      player={ele}
                      handleToggleMatchups={this.handleToggleMatchups}
                      show={this.state.toggle[ind]}
                      index={ind}
                    />
                    <div
                      style={{
                        padding: "12px 0px "
                      }}
                    />
                    <HrLine />
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    ) : (
      <div />
    );
  }
}
const normal = "normal";
const styles = {
  headerText: {
    fontFamily: "mont500",
    fontSize: "12px",
    // fontWeight: "500",
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: normal,
    letterSpacing: normal,
    color: "#141b2f"
  }
};
export default PlayerComparision;
