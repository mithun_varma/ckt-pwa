import convertCase from "lodash/startCase";
import React, { PureComponent } from "react";
import ProfileCard1 from "../../../images/ProfileCard1.png";
import { white } from "../../../styleSheet/globalStyle/color";
import ListItems from "../../commonComponent/ListItems";
const batIcon = require("../../../images/batsman_profile.png");
const ballIcon = require("../../../images/bowler_profile.png");

export class PlayerProfile extends PureComponent {

  matchUp = null;
  returnGradientAndIcon = playerRole => {
    return playerRole === "batsman"
      ? {
          icon: batIcon
        }
      : {
          icon: ballIcon
        };
  };

  render() {
    const { player } = this.props;
    const player1Data = this.returnGradientAndIcon(player.player1Role);
    const player2Data = this.returnGradientAndIcon(player.player2Role);

    return (
      <div
        ref={ref => (this.matchUp = ref)}
        onClick={() => {
          this.props.handleToggleMatchups(this.props.index)
          // window.scrollTo(0, this.matchUp.offsetTop);
        }}
        style={{
          padding: "0px 16px"
        }}
      >
        <div
          style={{
            position: "relative"
          }}
        >
          <div
            style={{
              ...styles.gradient,
              background: `linear-gradient(to bottom, ${
                player.player1Color[0]
              },  ${player.player1Color[1]})`,
              marginBottom: 3
            }}
          >
            <div
              style={{
                ...styles.bgImage,
                backgroundImage: `url ('${ProfileCard1}')`
              }}
            >
              <img
                src={require("../../../images/ProfileCard1.png")}
                alt=""
                style={{ width: "100%", height: "100%" }}
              />
            </div>

            <div style={styles.parent}>
              <div
                style={{
                  width: 76,
                  height: 76,
                  borderRadius: "50%",
                  overflow: "hidden",
                  margin: "0 10px",
                  background: "white"
                }}
              >
                <img src={player.player1Avatar} style={styles.profilePicture} />
              </div>

              <div style={{ flex: 1, alignSelf: "center" }}>
                <div style={styles.playerName}>{player.player1Name}</div>
                <span
                  style={{
                    display: "flex"
                  }}
                >
                  <span>
                    <img style={styles.icon} src={player1Data.icon} />
                  </span>
                  <span style={styles.country}>{player.player1TeamName}</span>
                </span>
              </div>
            </div>
          </div>
          {/* <div>VS</div> */}
          <div
            style={{
              position: "absolute",
              top: "46%"
            }}
          >
            <img
              src={require("../../../images/playerCompareSeparator.svg")}
              alt=""
              style={{
                width: "100%"
              }}
            />
            <span
              style={{
                position: "absolute",
                top: "-27%",
                right: "17%",
                fontSize: 14
              }}
            >
              vs
            </span>
          </div>
          {/* <img
          style={{
            height: "10%"
          }}
          src={shape}
        /> */}
          <div
            style={{
              ...styles.gradient,
              background: `linear-gradient(to bottom, ${
                player.player2Color[0]
              },  ${player.player2Color[1]})`,
              position: "initial"
            }}
          >
            <div
              style={{ ...styles.bgImage, background: `url ${ProfileCard1}` }}
            >
              <img
                src={require("../../../images/ProfileCard2.png")}
                alt=""
                style={{ width: "100%", height: "100%" }}
              />
            </div>

            <div style={styles.parent}>
              <div
                style={{
                  width: 76,
                  height: 76,
                  borderRadius: "50%",
                  overflow: "hidden",
                  margin: "0 10px",
                  background: "white"
                }}
              >
                <img src={player.player2Avatar} style={styles.profilePicture} />
              </div>

              <div style={{ flex: 1, alignSelf: "center" }}>
                <div style={styles.playerName}>{player.player2Name}</div>
                <span
                  style={{
                    display: "flex"
                  }}
                >
                  <span>
                    <img style={styles.icon} src={player2Data.icon} />
                  </span>
                  <span style={styles.country}>{player.player2TeamName}</span>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div
          style={{
            backgroundColor: white,
            display: this.props.show ? "block" : "none"
          }}
        >
          {Object.entries(this.props.player).map(([key, value]) => {
            return (
              key.indexOf("player") === -1 &&
              key.indexOf("status") === -1 && (
                <ListItems
                  key={key}
                  title={convertCase(key)}
                  rightElement={<div style={styles.rightText}>{value}</div>}
                />
              )
            );
          })}
        </div>
      </div>
    );
  }
}
const styles = {
  rightText: {
    fontFamily: " Oswald",
    fontSize: "14px",
    fontWeight: "normal",
    fontStyle: "normal",
    fontStretch: "normal",
    lineHeight: "normal",
    letterSpacing: "normal",
    textAlign: "right",
    color: "#141b2f"
  },
  gradient: {
    position: "relative",
    borderRadius: "4px"
    // marginTop: 2.4
  },
  bgImage: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "top center"
  },
  vs: {
    textAlign: "center",
    fontFamily: "Montserrat",
    fontSize: "14px",
    fontWeight: 500,
    fontStyle: "normal",
    fontStretch: "normal",
    lineHeight: "normal",
    letterSpacing: "normal",
    color: "#141b2f",
    margin: "10px 0",
    background: white
  },
  profilePicture: {
    width: "76px",
    // height: "76px",
    objectFit: "cover"
    // alignSelf: "center",
    // padding: "15px 0"
    // borderRadius: "50%",
    // background: "white",
    // margin: "0 10px"
  },
  parent: {
    borderRadius: "4px",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    padding: "11px 0"
  },
  playerName: {
    fontFamily: "Montserrat",
    fontSize: "16px",
    fontWeight: 600,
    fontStyle: "normal",
    fontStretch: "normal",
    lineHeight: 1.06,
    letterSpacing: "0.4px",
    color: "#ffffff",
    padding: "10px 0"
  },
  icon: {
    width: "15px",
    height: "20px",
    objectFit: "contain"
  },
  country: {
    paddingLeft: 7,
    opacity: 0.9,
    fontFamily: "Montserrat",
    fontSize: "12px",
    fontWeight: 600,
    fontStyle: "normal",
    fontStretch: "normal",
    lineHeight: 1.42,
    letterSpacing: "0.3px",
    color: "#ffffff"
  }
};
export default PlayerProfile;
