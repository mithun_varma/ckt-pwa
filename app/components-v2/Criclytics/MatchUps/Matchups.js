import React, { Component } from "react";
import {
  grey_10,
  red_Orange,
  grey_8,
  white,
  red_10,
  pink_1,
  grey_6
} from "../../../styleSheet/globalStyle/color";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import ListItems from "../../commonComponent/ListItems";
import PlayerListItems from "../../screenComponent/scoreCard/PlayerListItems";
import convertCase from "lodash/startCase";
import Fact from "../../Fact";

class Matchups extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPlayerList: false,
      fetchedPlayer1Data: false,
      fetchedPlayer2Data: false,
      playerOneIndex: false,
      playerTwoIndex: false,
      tryAgain: false,
      player2AlreadySelected: false
    };
  }

  tryAnothreMatchups = () => {
    this.setState(
      {
        // isPlayerList: false,
        fetchedPlayer1Data: false,
        fetchedPlayer2Data: false,
        playerOneIndex: false,
        playerTwoIndex: false,
        tryAgain: false,
        player2AlreadySelected: false
      },
      () => {
        this.props.getMatchUpsForPlayerOne(this.props.matchId);
      }
    );
  };

  getPlayerOneList = () => {
    this.props.getMatchUpsForPlayerOne(this.props.matchId);
    this.setState({
      isPlayerList: true
    });
  };

  anotherGetPlayerOneList = () => {
    if (this.state.playerTwoIndex) {
      this.props.getMatchUpsForSelectedPlayer({
        matchId: this.props.matchId,
        playerId: this.props.matchUpsForSelectedPlayer[
          this.state.playerTwoIndex
        ].player2,
        role: this.props.matchUpsForSelectedPlayer[this.state.playerTwoIndex]
          .player2Role
      });
    } else {
      this.props.getMatchUpsForPlayerOne(this.props.matchId);
    }
    this.setState({
      isPlayerList: true,
      fetchedPlayer1Data: false,
      fetchedPlayer2Data: false,
      playerOneIndex: false,
      playerTwoIndex: this.state.playerTwoIndex || false,
      tryAgain: false,
      player2AlreadySelected: this.state.playerTwoIndex ? true : false
    });
  };

  // getPlayerTwoList = () => {
  //   // debugger
  //   this.props.getMatchUpsForSelectedPlayer({
  //     matchId: this.props.matchId,
  //     playerId: this.props.matchUpsPlayerOneData[this.state.playerOneIndex]
  //       .player1
  //   });
  //   this.setState({
  //     fetchedPlayer1Data: true
  //   });
  // };

  selectPlayerOne = index => {
    this.setState(
      {
        playerOneIndex: index == 0 ? "0" : index,
        fetchedPlayer1Data: true
        // fetchedPlayer1Data: true
      },
      () => {
        if (!this.state.playerTwoIndex) {
          this.props.getMatchUpsForSelectedPlayer({
            matchId: this.props.matchId,
            playerId: this.props.matchUpsPlayerOneData[
              this.state.playerOneIndex
            ].player1,
            role: this.props.matchUpsPlayerOneData[this.state.playerOneIndex]
              .player1Role
          });
        }
      }
    );
  };

  selectPlayerTwo = index => {
    this.setState({
      playerTwoIndex: index == 0 ? "0" : index
      // fetchedPlayer1Data: true
    });
  };

  resetPlayerTwo = () => {
    if (this.state.playerOneIndex) {
      this.setState(
        {
          playerTwoIndex: false,
          player2AlreadySelected: false
        },
        () => {
          this.props.getMatchUpsForSelectedPlayer({
            matchId: this.props.matchId,
            playerId: this.props.matchUpsPlayerOneData[
              this.state.playerOneIndex
            ].player1,
            role: this.props.matchUpsPlayerOneData[this.state.playerOneIndex]
              .player1Role
          });
        }
      );
    }
  };

  render() {
    // console.log("state: ", this.state);
    return (
      <div
        style={{
          fontFamily: "Montserrat",
          fontSize: 12,
          color: grey_10,
          background: white
        }}
      >
        <Fact
          header="Match-ups"
          headerDescription="There have been many interesting face-offs between players. Some top encounters, here for you"
          hideFactEngine
        />
        {/* Red Header Section */}
        <div
          style={{
            backgroundImage: `url(${require("../../../images/matchups-background-red.png")})`,
            backgroundSize: "cover",
            backgroundPosition: "bottom",
            backgroundRepeat: "no-repeat",
            height: 168,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            position: "relative"
          }}
        >
          <div
            style={{
              color: white,
              background: "linear-gradient(to right, #141b2f, #273049)",
              position: "absolute",
              top: 0,
              left: 0,
              width: 120,
              height: 24,
              display: "flex",
              alignItems: "center",
              paddingLeft: 14,
              clipPath: "polygon(0 0, 100% 0, 85% 100%, 0 100%)"
            }}
          >
            Select Players
          </div>
          {/* matchups-background-red.png */}
          <div
            style={{
              display: "flex",
              // justifyContent: "space-evenly",
              justifyContent: "center",
              width: "100%"
            }}
          >
            {/* Player 1 */}
            <div
              style={{
                width: 140
              }}
            >
              <div
                style={{
                  position: "relative",
                  display: "flex",
                  justifyContent: "center"
                }}
              >
                <div
                  style={{
                    width: 60,
                    height: 60,
                    background: pink_1,
                    borderRadius: "50%",
                    overflow: "hidden"
                  }}
                >
                  <img
                    src={
                      this.state.playerOneIndex
                        ? this.props.matchUpsPlayerOneData[
                            this.state.playerOneIndex
                          ].player1Avatar ||
                          require("../../../images/fallbackProjection.png")
                        : // require("../../../images/vk.png") ||
                          require("../../../images/matchup-payer-color.png")
                    }
                    alt=""
                    style={{
                      width: "100%",
                      objectFit: "cover"
                    }}
                  />
                  <div
                    style={{
                      position: "absolute",
                      bottom: -14,
                      left: "40%"
                    }}
                  >
                    {/* <Add /> */}
                    <button
                      style={{
                        background: "white",
                        outline: "none",
                        border: "none",
                        borderRadius: "50%",
                        // display:
                        //   this.state.playerOneIndex && this.state.playerTwoIndex
                        //     ? "none"
                        //     : "flex",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        width: 28,
                        height: 28
                      }}
                    >
                      {this.state.playerOneIndex ? (
                        <EditIcon
                          style={{ color: grey_6, fontSize: 16 }}
                          onClick={this.anotherGetPlayerOneList}
                        />
                      ) : (
                        <AddIcon
                          style={{ color: red_Orange }}
                          onClick={this.getPlayerOneList}
                        />
                      )}
                    </button>
                  </div>
                </div>
              </div>
              <div
                style={{
                  color: white,
                  marginTop: 16,
                  textAlign: "center"
                }}
              >
                {this.state.playerOneIndex
                  ? this.props.matchUpsPlayerOneData[this.state.playerOneIndex]
                      .player1Name
                  : "Batsmen"}
                {/* Player 1 */}
              </div>
            </div>

            {/* Player 2 */}
            <div
              style={{
                width: 140
              }}
            >
              <div
                style={{
                  position: "relative",
                  display: "flex",
                  justifyContent: "center",
                  opacity:
                    this.state.playerOneIndex || this.state.playerTwoIndex
                      ? 1
                      : 0.6
                }}
              >
                <div
                  style={{
                    width: 60,
                    height: 60,
                    background: pink_1,
                    borderRadius: "50%",
                    overflow: "hidden"
                  }}
                >
                  <img
                    src={
                      this.state.playerTwoIndex
                        ? (this.props.matchUpsForSelectedPlayer &&
                            this.props.matchUpsForSelectedPlayer[
                              this.state.playerTwoIndex
                            ] &&
                            this.props.matchUpsForSelectedPlayer[
                              this.state.playerTwoIndex
                            ].player2Avatar) ||
                          require("../../../images/fallbackProjection.png")
                        : require("../../../images/matchup-payer-color.png")
                    }
                    alt=""
                    style={{
                      width: "100%",
                      objectFit: "cover"
                    }}
                  />
                  <div
                    style={{
                      position: "absolute",
                      bottom: -14,
                      left: "40%"
                    }}
                  >
                    {/* <Add /> */}
                    <button
                      style={{
                        background: white,
                        outline: "none",
                        border: "none",
                        borderRadius: "50%",
                        display: "flex",
                        // display:
                        //   this.state.playerOneIndex && this.state.playerTwoIndex
                        //     ? "none"
                        //     : "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        width: 28,
                        height: 28
                      }}
                      disabled={
                        !this.state.playerOneIndex && !this.state.playerTwoIndex
                      }
                      onClick={this.getPlayerTwoList}
                    >
                      {/* <AddIcon style={{ color: red_Orange }} /> */}
                      {this.state.playerTwoIndex ? (
                        <EditIcon
                          style={{ color: grey_6, fontSize: 16 }}
                          onClick={this.resetPlayerTwo}
                        />
                      ) : (
                        <AddIcon
                          style={{ color: red_Orange }}
                          // onClick={this.getPlayerOneList}
                        />
                      )}
                    </button>
                  </div>
                </div>
              </div>
              <div
                style={{
                  color: white,
                  marginTop: 16,
                  textAlign: "center"
                }}
              >
                {this.state.playerTwoIndex
                  ? (this.props.matchUpsForSelectedPlayer &&
                      this.props.matchUpsForSelectedPlayer[
                        this.state.playerTwoIndex
                      ] &&
                      this.props.matchUpsForSelectedPlayer[
                        this.state.playerTwoIndex
                      ].player2Name) ||
                    ""
                  : "Bowler"}
              </div>
            </div>
          </div>
        </div>
        {/* Red Header Section closing */}
        {/* Label for Select Players */}
        <div
          style={{
            textAlign: "center",
            color: red_Orange,
            letterSpacing: 0.6,
            fontWeight: 500,
            marginTop: -2,
            background: "transparent"
          }}
        >
          {this.state.isPlayerList &&
          !this.state.fetchedPlayer1Data &&
          this.props.matchUpsPlayerOneData.length > 0
            ? "Select Batsmen"
            : this.state.isPlayerList &&
              this.state.fetchedPlayer1Data &&
              !this.state.playerTwoIndex
              ? "Select Bowler"
              : this.state.isPlayerList && this.state.playerTwoIndex
                ? "Match up"
                : "Select Batsmen"}
        </div>
        <div
          style={{
            minHeight: "50vh",
            maxHeight: "50vh",
            overflow: "scroll",
            position: "relative"
          }}
        >
          {/* Players-1-List Items Section */}
          {/* Scenario 1 */}
          {this.state.isPlayerList &&
            !this.state.playerTwoIndex &&
            !this.state.fetchedPlayer1Data && (
              <div>
                {this.props.matchUpsPlayerOneData.length > 0 ? (
                  this.props.matchUpsPlayerOneData.map((players, key) => (
                    <PlayerListItems
                      key={key}
                      index={key}
                      avatar={
                        players.player1Avatar ||
                        require("../../../images/fallbackProjection.png")
                      }
                      avatarStyles={{
                        backgroundColor: grey_10,
                        borderRadius: "50%",
                        width: 34,
                        height: 34
                      }}
                      title={players.player1Name || "-"}
                      titleStyles={{
                        fontSize: 14
                      }}
                      rightElement={
                        players.player1Role &&
                        ["batsman", "bowler"].indexOf(players.player1Role) >
                          -1 && (
                          <img
                            src={require(`../../../images/${
                              players.player1Role
                            }-icon.svg`)}
                            style={{
                              width: 20,
                              height: 20
                              // borderRadius: "50%"
                            }}
                          />
                        )
                      }
                      onClick={this.selectPlayerOne}
                    />
                  ))
                ) : (
                  <div
                    style={{
                      textAlign: "center",
                      padding: "40px 0"
                    }}
                  >
                    No data available from last 3 years
                  </div>
                )}
              </div>
            )}

          {/* Scenario 2 */}
          {this.state.isPlayerList &&
            this.state.playerTwoIndex &&
            !this.state.fetchedPlayer1Data && (
              <div>
                {this.props.matchUpsPlayerOneData.length > 0 ? (
                  this.props.matchUpsPlayerOneData.map((players, key) => (
                    <PlayerListItems
                      key={key}
                      index={key}
                      avatar={
                        players.player1Avatar ||
                        require("../../../images/fallbackProjection.png")
                      }
                      avatarStyles={{
                        backgroundColor: grey_10,
                        borderRadius: "50%",
                        width: 34,
                        height: 34
                      }}
                      title={players.player1Name || "-"}
                      titleStyles={{
                        fontSize: 14
                      }}
                      rightElement={
                        players.player1Role &&
                        ["batsman", "bowler"].indexOf(players.player1Role) >
                          -1 && (
                          <img
                            src={require(`../../../images/${
                              players.player1Role
                            }-icon.svg`)}
                            style={{
                              width: 20,
                              height: 20
                              // borderRadius: "50%"
                            }}
                          />
                        )
                      }
                      onClick={this.selectPlayerOne}
                    />
                  ))
                ) : (
                  <div
                    style={{
                      textAlign: "center",
                      padding: "40px 0"
                    }}
                  >
                    No data available from last 3 years
                  </div>
                )}
              </div>
            )}
          {/* Players-1-List Items Section closing */}

          {/* Players-2-List Items Section */}
          {this.state.isPlayerList &&
            this.state.fetchedPlayer1Data &&
            !this.state.playerTwoIndex && (
              <div>
                {this.props.matchUpsForSelectedPlayer.length > 0 ? (
                  this.props.matchUpsForSelectedPlayer.map((players, key) => (
                    <PlayerListItems
                      key={key}
                      index={key}
                      avatar={
                        players.player2Avatar ||
                        require("../../../images/fallbackProjection.png")
                      }
                      avatarStyles={{
                        backgroundColor: grey_10,
                        borderRadius: "50%",
                        width: 34,
                        height: 34
                      }}
                      title={players.player2Name || "-"}
                      titleStyles={{
                        fontSize: 14
                      }}
                      rightElement={
                        players.player2Role &&
                        ["batsman", "bowler"].indexOf(players.player2Role) >
                          -1 && (
                          <img
                            src={require(`../../../images/${
                              players.player2Role
                            }-icon.svg`)}
                            style={{
                              width: 20,
                              height: 20
                              // borderRadius: "50%"
                            }}
                          />
                        )
                      }
                      onClick={this.selectPlayerTwo}
                    />
                  ))
                ) : (
                  <div
                    style={{
                      textAlign: "center",
                      padding: "40px 0"
                    }}
                  >
                    No player matching with selected player
                  </div>
                )}
              </div>
            )}
          {/* Players-2-List Items Section closing */}

          {/* Players Matchup section */}
          {this.state.playerOneIndex &&
            this.state.playerTwoIndex && (
              <div>
                <ListItems
                  key={"key"}
                  title={convertCase("Balls Bowled")}
                  titleStyle={{
                    fontSize: 13,
                    fontWeight: 400
                  }}
                  rightElement={
                    <div style={styles.rightText}>
                      {/* {
                      this.props.matchUpsForSelectedPlayer[
                        this.state.player2AlreadySelected ? this.state.playerOneIndex : this.state.playerTwoIndex
                      ].ballsFaced
                    } */}
                      {this.state.player2AlreadySelected
                        ? this.props.matchUpsPlayerOneData[
                            this.state.playerOneIndex
                          ].ballsFaced
                        : this.props.matchUpsForSelectedPlayer[
                            this.state.playerTwoIndex
                          ].ballsFaced}
                    </div>
                  }
                />
                <ListItems
                  key={"key"}
                  title={convertCase("Dismissals")}
                  titleStyle={{
                    fontSize: 13,
                    fontWeight: 400
                  }}
                  rightElement={
                    <div style={styles.rightText}>
                      {/* {
                      this.props.matchUpsForSelectedPlayer[
                        this.state.player2AlreadySelected ? this.state.playerOneIndex : this.state.playerTwoIndex
                      ].dismissals
                    } */}
                      {this.state.player2AlreadySelected
                        ? this.props.matchUpsPlayerOneData[
                            this.state.playerOneIndex
                          ].dismissals
                        : this.props.matchUpsForSelectedPlayer[
                            this.state.playerTwoIndex
                          ].dismissals}
                    </div>
                  }
                />
                <ListItems
                  key={"key"}
                  title={convertCase("Runs")}
                  titleStyle={{
                    fontSize: 13,
                    fontWeight: 400
                  }}
                  rightElement={
                    <div style={styles.rightText}>
                      {/* {
                      this.props.matchUpsForSelectedPlayer[
                        this.state.player2AlreadySelected ? this.state.playerOneIndex : this.state.playerTwoIndex
                      ].runsScored
                    } */}
                      {this.state.player2AlreadySelected
                        ? this.props.matchUpsPlayerOneData[
                            this.state.playerOneIndex
                          ].runsScored
                        : this.props.matchUpsForSelectedPlayer[
                            this.state.playerTwoIndex
                          ].runsScored}
                    </div>
                  }
                />
                <ListItems
                  key={"key"}
                  title={convertCase("Batting S/R")}
                  titleStyle={{
                    fontSize: 13,
                    fontWeight: 400
                  }}
                  rightElement={
                    <div style={styles.rightText}>
                      {/* {
                      this.props.matchUpsForSelectedPlayer[
                        this.state.player2AlreadySelected ? this.state.playerOneIndex : this.state.playerTwoIndex
                      ].battingSR
                    } */}
                      {this.state.player2AlreadySelected
                        ? this.props.matchUpsPlayerOneData[
                            this.state.playerOneIndex
                          ].battingSR
                        : this.props.matchUpsForSelectedPlayer[
                            this.state.playerTwoIndex
                          ].battingSR}
                    </div>
                  }
                />
                <ListItems
                  key={"key"}
                  title={convertCase("Bowling S/R")}
                  titleStyle={{
                    fontSize: 13,
                    fontWeight: 400
                  }}
                  rightElement={
                    <div style={styles.rightText}>
                      {/* {
                      this.props.matchUpsForSelectedPlayer[
                        this.state.player2AlreadySelected ? this.state.playerOneIndex : this.state.playerTwoIndex
                      ].bowlingSR
                    } */}
                      {this.state.player2AlreadySelected
                        ? this.props.matchUpsPlayerOneData[
                            this.state.playerOneIndex
                          ].bowlingSR
                        : this.props.matchUpsForSelectedPlayer[
                            this.state.playerTwoIndex
                          ].bowlingSR}
                    </div>
                  }
                />

                <div
                  style={{
                    textAlign: "center",
                    letterSpacing: 0.7,
                    fontWeight: 600,
                    color: white,
                    fontSize: 14,
                    background: red_Orange,
                    width: "100%",
                    padding: "8px 0",
                    // position: "absolute",
                    bottom: 0
                    // borderRadius: 30
                  }}
                  onClick={this.tryAnothreMatchups}
                >
                  Another Matchup
                </div>
              </div>
            )}
          {/* Players Matchup section closing */}

          {/* Initial State section */}
          {!this.state.isPlayerList && (
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                padding: " 14% 0 20%"
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  width: "50%",
                  textAlign: "center"
                }}
              >
                <img
                  src={require("../../../images/matchup-player-black.png")}
                  alt=""
                  style={{
                    width: 60,
                    height: 60
                  }}
                />
                <div
                  style={{
                    color: grey_8,
                    padding: "14px 0 22px"
                  }}
                >
                  Select your favourite player and check their match ups against
                  the opposition.
                </div>
                <div
                  style={{
                    letterSpacing: 0.7,
                    fontWeight: 600,
                    color: white,
                    fontSize: 14,
                    background: red_Orange,
                    width: "100%",
                    padding: "8px 0",
                    borderRadius: 30
                  }}
                  onClick={this.getPlayerOneList}
                >
                  Select
                </div>
              </div>
            </div>
          )}

          {/* Initial State section closing */}
        </div>
      </div>
    );
  }
}

export default Matchups;

const styles = {
  rightText: {
    fontFamily: " Oswald",
    fontSize: "14px",
    fontWeight: 400,
    textAlign: "right",
    color: grey_10
  }
};
