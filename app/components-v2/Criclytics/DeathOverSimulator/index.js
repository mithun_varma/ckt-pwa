import CheckCircle from "@material-ui/icons/CheckCircle";
import CheckCircleOutline from "@material-ui/icons/CheckCircleOutline";
import cloneDeep from "lodash/cloneDeep";
import React, { PureComponent } from "react";
import { white } from "../../../styleSheet/globalStyle/color";
import HrLine from "../../commonComponent/HrLine";
import ListItems from "../../commonComponent/ListItems";
import Fact from "../../Fact";
import PlayerTimeLine from "./PlayerTimeLine";
import SimulatedScreen from "./SimulatedScreen";

export class DeathOverSimulator extends PureComponent {
  state = {
    disabled: true,
    data: undefined,
    counter: 0,
    images: [],
    displaySimulated: false,
    legalBowlers: undefined,
    selectedBowlers: [],
    threshold: undefined,
    bowlerListToDisplay: [],
    remainingOvers: [],
    textToShow: "Simulate",
    className: "animated",
    maintainAnimation: false,
    name: []
  };

  getThreshHoldOvers = matchKind => {
    if (matchKind === "T20") {
      return 4;
    }
    if (matchKind === "ODI") {
      return 10;
    }
  };
  calculateRemainingOvers = (bowlers, matchKind) => {
    const threshold = this.getThreshHoldOvers(matchKind);

    return (
      bowlers &&
      bowlers.map(ele => {
        return {
          name: ele.name,
          remainingOvers: threshold - Math.floor(ele.overs),
          avatar: ele.avatar
        };
      })
    );
  };

  componentWillReceiveProps() {
    this.setState({
      remainingOvers: this.calculateRemainingOvers(
        this.props.data.playerIds,
        "T20"
      )
    });
  }

  handleBowlerSelection = (val, remainingOvers) => {
    if (this.state.selectedBowlers.length >= remainingOvers) {
      return;
    }

    const clone = cloneDeep(this.state.remainingOvers);
    this.state.remainingOvers[val - 1].remainingOvers -= 1;

    const Legal = this.isBowlerLegal(this.state.remainingOvers[val - 1], 4);

    if (Legal) {
      this.setState({
        legalBowlers: this.state.remainingOvers[val - 1]
      });
    } else {
      this.setState({
        legalBowlers: undefined
      });
    }
    this.setState(
      prevState => ({
        images: [
          ...prevState.images,
          this.state.remainingOvers[val - 1].avatar
        ],
        name: [...prevState.name, this.state.remainingOvers[val - 1].name],
        selectedBowlers: [
          ...prevState.selectedBowlers,
          this.state.remainingOvers[val - 1]
        ]
      }),
      () => {
        this.state.selectedBowlers.length >= remainingOvers
          ? this.setState({
              disabled: false
            })
          : this.setState({
              disabled: true
            });
      }
    );
    clone.splice(val - 1, 1);

    if (this.state.legalBowlers) {
      clone.push(this.state.legalBowlers);
      this.setState({
        remainingOvers: clone
      });
    }
    this.setState({
      remainingOvers: clone
    });
  };
  isBowlerLegal = (bowler, threshold) => {
    return parseInt(bowler.remainingOvers) - 1 === 0;
  };

  mincurrentMatchCard = props => {
    if (!props.over) {
      return (
        <div
          style={{
            display: "flex",
            padding: "13px 15px"
          }}
        >
          <img
            style={{
              width: "24px",
              height: "16px",
              objectFit: "contain"
            }}
            src={props.avatar}
          />

          <div style={styles.teamName}>{props.teamName}</div>
        </div>
      );
    }
    return (
      <div
        style={{
          display: "flex",
          padding: "13px 15px"
        }}
      >
        <img
          style={{
            width: "24px",
            height: "16px",
            objectFit: "contain"
          }}
          src={props.avatar}
        />

        <div style={styles.teamName}>{props.teamName}</div>
        <div style={styles.currentMatch}>
          {`${props.runs}/${props.wickets}`}
          {`(${props.over})`}
        </div>
      </div>
    );
  };
  render() {
    const { currentMatch } = this.props;
    let teamCollection = [
      {
        teamName:
          currentMatch &&
          currentMatch.teamA.teamKey === currentMatch.firstBatting
            ? currentMatch && currentMatch.teamA.fullName
            : currentMatch && currentMatch.teamB.fullName,
        shortName:
          currentMatch &&
          currentMatch.teamA.teamKey === currentMatch.firstBatting
            ? currentMatch && currentMatch.teamA.shortName
            : currentMatch && currentMatch.teamB.shortName,
        teamKey:
          currentMatch &&
          currentMatch.teamA.teamKey === currentMatch.firstBatting
            ? currentMatch && currentMatch.teamA.teamKey
            : currentMatch && currentMatch.teamB.teamKey,
        runs: [],
        wickets: [],
        overs: [],
        avatar:
          currentMatch &&
          currentMatch.teamA.teamKey === currentMatch.firstBatting
            ? currentMatch && currentMatch.teamA.avatar
            : currentMatch && currentMatch.teamB.avatar
      },
      {
        teamName:
          currentMatch &&
          currentMatch.teamA.teamKey !== currentMatch.firstBatting
            ? currentMatch && currentMatch.teamA.fullName
            : currentMatch && currentMatch.teamB.fullName,
        shortName:
          currentMatch &&
          currentMatch.teamA.teamKey !== currentMatch.firstBatting
            ? currentMatch && currentMatch.teamA.shortName
            : currentMatch && currentMatch.teamB.shortName,
        teamKey:
          currentMatch &&
          currentMatch.teamA.teamKey !== currentMatch.firstBatting
            ? currentMatch && currentMatch.teamA.teamKey
            : currentMatch && currentMatch.teamB.teamKey,
        runs: [],
        wickets: [],
        overs: [],
        avatar:
          currentMatch &&
          currentMatch.teamA.teamKey !== currentMatch.firstBatting
            ? currentMatch && currentMatch.teamA.avatar
            : currentMatch && currentMatch.teamB.avatar
      }
    ];
    if (currentMatch && currentMatch.status !== "UPCOMING") {
      for (const order of currentMatch.inningOrder) {
        let team = order.split("_")[0];
        if (team === currentMatch.firstBatting) {
          (teamCollection[0].runs = [
            ...teamCollection[0].runs,
            currentMatch.inningCollection[order].runs
          ]),
            (teamCollection[0].wickets = [
              ...teamCollection[0].wickets,
              currentMatch.inningCollection[order].wickets
            ]),
            (teamCollection[0].overs = [
              ...teamCollection[0].overs,
              currentMatch.inningCollection[order].overs
            ]);
        } else {
          (teamCollection[1].runs = [
            ...teamCollection[1].runs,
            currentMatch.inningCollection[order].runs
          ]),
            (teamCollection[1].wickets = [
              ...teamCollection[1].wickets,
              currentMatch.inningCollection[order].wickets
            ]),
            (teamCollection[1].overs = [
              ...teamCollection[1].overs,
              currentMatch.inningCollection[order].overs
            ]);
        }
      }
    } else if (currentMatch && currentMatch.status == "UPCOMING") {
      for (let i = 0; i < teamCollection.length; i++) {
        (teamCollection[i].teamName =
          currentMatch[i == 0 ? "teamA" : "teamB"].fullName),
          (teamCollection[i].shortName =
            currentMatch[i == 0 ? "teamA" : "teamB"].shortName),
          (teamCollection[i].teamKey =
            currentMatch[i == 0 ? "teamA" : "teamB"].teamKey);
        (teamCollection[i].runs = []),
          (teamCollection[i].wickets = []),
          (teamCollection[i].overs = []),
          (teamCollection[i].avatar =
            currentMatch[i == 0 ? "teamA" : "teamB"].avatar);
      }
    }

    // if (!this.props.data.remainingOvers)
    //   return (
    //     <div
    //       style={{
    //         height: "100vh"
    //       }}
    //     >
    //       <div style={styles.choice}>
    //         Death over simulator is closed for this innings
    //       </div>
    //       <center>
    //         <img
    //           style={{
    //             height: "250px",
    //             width: "250px",
    //             marginTop: "50px"
    //           }}
    //           src={require("../../../images/groundImageWicket.png")}
    //         />
    //       </center>
    //     </div>
    //   );

    return (
      <React.Fragment>
        {!this.props.isHide && (
          <Fact
            header="Death Overs Simulator"
            headerDescription={
              "What happens if a spinner bowls the last over? Pick the bowlers and simulate the final currentMatch."
            }
            fact={currentMatch && currentMatch.factEngine}
          />
        )}

        <div
          style={{
            backgroundColor: white,
            display: this.state.displaySimulated ? "none" : "inline"
          }}
        >
          <div
            style={{
              display: "flex",
              justifyContent: "space-between"
            }}
          >
            {teamCollection &&
              this.mincurrentMatchCard({
                teamName: teamCollection[0].shortName,
                runs: teamCollection[0].runs,
                over: teamCollection[0].overs,
                avatar: teamCollection[0].avatar,
                wickets: teamCollection[0].wickets
              })}
            <div
              style={{
                alignContent: "flex-end"
              }}
            >
              {teamCollection &&
                this.mincurrentMatchCard({
                  teamName: teamCollection[1].shortName,
                  runs: teamCollection[1].runs,
                  over: teamCollection[1].overs,
                  avatar: teamCollection[1].avatar,
                  wickets: teamCollection[1].wickets
                })}
            </div>
          </div>
          <HrLine />
          {this.props.data.remainingOvers ? (
            <div style={styles.choice}>Pick your choice of Bowlers</div>
          ) : null}
          <div
            style={{
              paddingTop: 31
            }}
          >
            <PlayerTimeLine
              name={this.state.name}
              numberOfAvatar={this.props.data.remainingOvers || 4}
              image={this.state.images}
            />
          </div>

          <div
            style={{
              paddingTop: 48
            }}
          >
            {this.state.remainingOvers &&
              this.state.remainingOvers.map((bowler, key) => {
                return (
                  <div>
                    <ListItems
                      onClick={val => {
                        this.handleBowlerSelection(
                          val,
                          this.props.data.remainingOvers || 4
                        );
                      }}
                      key={key}
                      param={key + 1}
                      isAvatar
                      avatar={bowler.avatar}
                      belowTitle={`Remaining Overs: ${
                        this.state.remainingOvers[key].remainingOvers
                      }`}
                      title={bowler.name}
                      rightElement={
                        bowler.isChecked ? (
                          <CheckCircle
                            style={{
                              color: "green"
                            }}
                          />
                        ) : (
                          <CheckCircleOutline
                            style={{
                              color: "grey"
                            }}
                          />
                        )
                      }
                    />
                  </div>
                );
              })}
          </div>
        </div>
        <div
          style={{
            display: this.state.displaySimulated ? "inline" : "none"
          }}
        >
          <SimulatedScreen
            currentMatch={
              currentMatch &&
              currentMatch.inningOrder &&
              currentMatch.inningOrder.length === 1
                ? teamCollection[0]
                : teamCollection[1]
            }
            rows={this.props.postData}
          />
        </div>
        <button
          onClick={() => {
            let requestToSend;
            this.state.textToShow === "Try another option"
              ? this.setState({
                  bowlerListToDisplay: this.state.data,
                  selectedBowlers: [],
                  images: [],
                  disabled: true,
                  name: [],
                  legalBowlers: undefined
                })
              : (requestToSend = cloneDeep(this.props.data));
            requestToSend
              ? (requestToSend["playerIds"] = this.state.selectedBowlers)
              : requestToSend;

            this.props.postDeathBowlers(requestToSend);

            this.setState(
              {
                displaySimulated: !this.state.displaySimulated,
                textToShow:
                  this.state.textToShow === "Simulate"
                    ? "Try another option"
                    : "Simulate"
              },
              () => {
                this.state.textToShow === "Try another option";
              }
            );
          }}
          disabled={this.state.disabled}
          style={{
            ...styles.button,
            backgroundImage: !this.state.disabled
              ? "linear-gradient(to left, #d44030, #9b000d)"
              : "",
            backgroundColor: !this.state.disabled ? "" : "#a1a4ac",
            margin: "10px 4% 10px 4%"
          }}
        >
          {this.state.textToShow}
        </button>
      </React.Fragment>
    );
  }
}
const normal = "normal";
const styles = {
  button: {
    width: "91%",
    color: "#fff",
    boxShadow: "0 0 6px 0 rgba(248, 142, 66, 0.3)",

    borderRadius: "3px",
    padding: "12px 0px"
  },
  teamName: {
    fontFamily: "mont500",
    fontSize: "12px",
    fontWeight: 500,
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: normal,
    letterSpacing: normal,
    textAlign: "center",
    color: "#141b2f",
    paddingLeft: "8px"
  },
  currentMatch: {
    fontFamily: "mont500",
    fontSize: "12px",
    fontWeight: 500,
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: normal,
    letterSpacing: normal,
    textAlign: "center",
    color: "#727682",
    paddingLeft: "8px"
  },
  choice: {
    fontFamily: "mont500",
    fontSize: "18px",
    fontWeight: 500,
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: normal,
    letterSpacing: normal,
    textAlign: "center",
    color: "#141b2f",
    padding: "22px 80px",
    paddingTop: "102px"
  }
};
export default DeathOverSimulator;
