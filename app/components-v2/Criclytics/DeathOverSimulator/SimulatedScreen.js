import React, { PureComponent } from "react";
import { grey_10, grey_8, white } from "../../../styleSheet/globalStyle/color";
import HrLine from "../../commonComponent/HrLine";

export class SimulatedScreen extends PureComponent {
  createTable = props => {
    return (
      <React.Fragment>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            background: props.isHeaderBackground
              ? " linear-gradient(to left, #ffffff, #f0f1f2 18%, #f0f1f2 85%, #ffffff)"
              : white,
            fontFamily: props.isHeaderBackground ? "mont400" : "mont500",
            color: props.isHeaderBackground ? grey_8 : grey_10,
            fontSize: 11,
            height: props.isHeaderBackground ? 42 : 52
          }}
        >
          <div
            style={{
              flex: 0.55
            }}
          >
            {!props.isHeaderBackground && (
              <div
                style={{
                  display: "flex",
                  alignItems: "center"
                }}
              >
                <div
                  style={{
                    width: "33px",
                    height: "33px",
                    overflow: "hidden",
                    borderRadius: "50%",
                    border: "1px solid #ea6550",
                    background: "#616469",
                    margin: "0 8px"
                  }}
                >
                  <img
                    style={{
                      // height: "40px",
                      width: "33px",

                      objectFit: "cover"
                    }}
                    src={props.data.avatar}
                  />
                </div>
                <div>{props.data.name}</div>
              </div>
            )}
          </div>

          <div
            style={{
              flex: 0.15,
              padding: "10px 10px"
            }}
          >
            {props.isHeaderBackground ? "OVERS" : props.data.overs}
          </div>
          <div
            style={{
              flex: 0.15,
              padding: "10px 10px"
            }}
          >
            {props.isHeaderBackground ? "WICKETS" : props.data.wickets}
          </div>
          <div
            style={{
              flex: 0.15,
              padding: "10px 10px"
            }}
          >
            {props.isHeaderBackground ? "RUNS" : props.data.runs}
          </div>
        </div>
        <HrLine />
      </React.Fragment>
    );
  };
  createScoreBoard = data => {
    data && data.push("-", this.props.rows.totalWickets);
    return (
      data &&
      data.map((ele, key) => {
        return (
          <React.Fragment key={key}>
            <div
              key={key}
              style={{
                borderRadius: "2px",
                backgroundColor: "#141b2f",
                alignSelf: "center",
                padding: "4px 6px",
                margin: "0 0.7%",
                position: "relative"
              }}
            >
              <div
                style={{
                  position: "relative",
                  fontFamily: "Oswald",
                  // fontSize:
                  //   window.screen.width > 380
                  //     ? "32px"
                  //     : window.screen.width > 340
                  //       ? "16px"
                  //       : "12px",
                  fontSize:
                    window.screen.width > 380
                      ? "32px"
                      : window.screen.width > 340
                        ? "24px"
                        : "28px",
                  fontWeight: "bold",
                  fontStyle: "normal",
                  fontStretch: "condensed",
                  lineHeight: "normal",
                  letterSpacing: "0.5px",
                  textAlign: "center",
                  color: "#fadd8d",
                  // padding: "6px 5px",
                  padding: "0px 2px",
                  zIndex: 2
                }}
              >
                {ele}
                {/* 6 */}
              </div>
              <div
                style={{
                  position: "absolute",
                  top: "50%",
                  left: 0,
                  right: 0,
                  height: 2,
                  border: "0.3px solid #fff",
                  zIndex: 1,
                  opacity: 0.4
                }}
              />
            </div>
          </React.Fragment>
        );
      })
    );
  };

  render() {
    const { rows } = this.props;

    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column"
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            padding: "15px 0px"
          }}
        >
          <img
            style={{
              height: "19px",
              width: "26px"
            }}
            src={this.props.currentMatch && this.props.currentMatch.avatar}
          />
          <div
            style={{
              paddingLeft: "7px",
              fontFamily: "Montserrat",
              fontSize: "15px"
            }}
          >
            {this.props.currentMatch && this.props.currentMatch.shortName}
          </div>
        </div>
        <div
          style={{
            display: "flex",

            justifyContent: "center"
          }}
        >
          {rows &&
            rows.totalRuns &&
            this.createScoreBoard(rows.totalRuns.toString().split(""))}
        </div>

        {this.createTable({ isHeaderBackground: true })}
        {rows &&
          rows.playerIds &&
          rows.playerIds.length > 0 &&
          rows.playerIds.map(ele => {
            return this.createTable({
              data: ele,
              isHeaderBackground: false
            });
          })}
      </div>
    );
  }
}

export default SimulatedScreen;
