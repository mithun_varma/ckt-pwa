import React, { PureComponent } from "react";
const defaultImage = require("../../../images/Simulator_Default.png");
export class PlayerTimeLine extends PureComponent {
  returnOverNumber = key => {
    let overs = [17, 18, 19, 20];

    const overToDisplay = overs.slice(
      4 - this.props.numberOfAvatar,
      overs.length
    );

    return overToDisplay[key];
  };

  render() {
    const { props } = this;
    const howMuchBoxToRender = Array.apply(null, {
      length: props.numberOfAvatar
    }).map(Number.call, Number);

    return (
      <center>
        <ol
          style={{
            width: "auto",
            marginLeft: "auto",
            marginRight: "auto"
          }}
          className="m-stepper"
        >
          {howMuchBoxToRender.map((ele, key) => {
            return (
              <li>
                <div
                  style={{
                    position: "relative"
                  }}
                >
                  <img
                    style={{
                      ...styles.image,
                      border: props.image[key]
                        ? "2px solid #f76b1c"
                        : "2px solid black"
                    }}
                    src={props.image[key] || defaultImage}
                  />

                  <div
                    style={{
                      overflow: "hidden",
                      fontSize: "12px",
                      position: "absolute",
                      fontFamily: "mont400",
                      marginTop: 4,
                      left: "-4px"
                    }}
                  >
                    {props.name[key]} {`Over \n ${this.returnOverNumber(key)}`}
                  </div>
                  <div />
                </div>
              </li>
            );
          })}
        </ol>
      </center>
    );
  }
}
const styles = {
  image: {
    height: "45px",
    width: "45px",
    objectFit: "contain",

    borderRadius: "45px"
  }
};
export default PlayerTimeLine;
