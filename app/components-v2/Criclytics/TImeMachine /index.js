import React, { PureComponent } from "react";
import HrLine from "../../commonComponent/HrLine";
import FactHeader from "../../commonComponent/FactHeader";
import Fact from "../../Fact";
import size from "lodash/size";
import moment from "moment";
import { red_Orange, white } from "../../../styleSheet/globalStyle/color";
import MiniScoreCard from "../../screenComponent/scoreCard/MiniScoreCard";

const normal = "normal";

export class TimeMachine extends PureComponent {
  state = {
    showSecondInnings: false,
    showStatus: false,
    hidePlayButton: false,
    currentScoreIndex: 0,
    animation: "animated",
    animationOvers: "animated"
  };

  componentWillReceiveProps() {
    this.props.currentMatch &&
      this.props.currentMatch.status !== "COMPLETED" &&
      this.setState({
        currentScoreIndex: 0,
        hidePlayButton: false,
        showStatus: false,
        showSecondInnings:
          this.props.currentMatch.inningOrder.length === 1 ? false : true
      });
    if (
      this.props.currentMatch &&
      this.props.currentMatch.inningOrder &&
      this.props.currentMatch.status === "COMPLETED" &&
      this.props.currentMatch.inningOrder.length >= 2
    ) {
      this.setState({
        showSecondInnings: true,
        hidePlayButton: true,
        currentScoreIndex: 1,
        showStatus: true
      });
    }
  }

  createScoreBoard = data => {
    return data.map((ele, key) => {
      return (
        <div
          key={key}
          style={{
            display: "flex",
            minWidth: 26,
            height: "100%",
            borderRadius: "2px",
            backgroundColor: "#141b2f",
            position: "relative",
            justifyContent: "center",
            alignItems: "center",
            marginRight: "2px",
            animationDelay: ".17s"
          }}
        >
          <div
            style={{
              position: "relative",
              fontFamily: "Oswald",
              fontSize:
                window.screen.width > 380
                  ? "32px"
                  : window.screen.width > 340
                    ? "24px"
                    : "28px",
              fontWeight: "bold",
              fontStyle: "normal",
              fontStretch: "condensed",
              lineHeight: "normal",
              letterSpacing: "0.5px",
              textAlign: "center",
              color: "#fadd8d",
              padding: "0px 2px",
              zIndex: 2
            }}
          >
            {ele}
          </div>
          <div
            style={{
              position: "absolute",
              top: "50%",
              left: 0,
              right: 0,
              height: 2,
              border: "0.3px solid #fff",
              zIndex: 1,
              opacity: 0.4
            }}
          />
        </div>
      );
    });
  };

  generateSchedule = data => {
    const textStyle = {
      fontFamily: "Montserrat",
      fontSize: "14px",
      fontWeight: 500,
      fontStretch: normal,
      lineHeight: normal,
      letterSpacing: normal,
      color: "#141b2f"
    };
    return (
      <div style={{ padding: "0 0 2.8% 4.9%" }}>
        <span style={{ ...textStyle, padding: "0 2px" }}>
          {data.tournament}
        </span>{" "}
        <span style={{ ...textStyle, padding: "0, 2px" }}>{data.title}</span>
        {data.venue && (
          <span style={{ ...textStyle, padding: "0 2px" }}>&middot;</span>
        )}
        <span style={{ ...textStyle, padding: "0 2px" }}>{data.venue}</span>
      </div>
    );
  };

  createScoreCard = data => {
    data && data.score && data.score.push("-", data.wickets);

    return (
      <div
        style={{
          display: "flex",
          flex: 1
        }}
      >
        <div
          style={{
            display: "flex",
            flex: 0.3
          }}
        >
          <img
            style={{
              width: "30px",
              height: "24px",
              objectFit: "contain",
              alignSelf: "center",
              margin: "0 6px 0 12px"
            }}
            src={data.flag}
          />
          <div
            style={{
              alignSelf: "center",
              fontFamily: "Montserrat",
              fontSize: "16px",
              fontWeight: normal,
              fontStyle: normal,
              fontStretch: normal,
              lineHeight: normal,
              letterSpacing: normal,
              color: "#141b2f"
            }}
          >
            {data.countryName}
          </div>
        </div>
        <div
          style={{
            display: "flex",
            flex: 0.7,
            justifyContent: "flex-start"
          }}
        >
          <div
            style={{
              display: "flex"
            }}
          >
            {data.score && this.createScoreBoard(data.score)}
          </div>
          <div
            style={{
              display: "flex",
              marginLeft: 20
            }}
          >
            <div
              className={this.state.animationOvers}
              style={{
                borderRadius: "2px",
                backgroundColor: "#141b2f",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                minWidth: 48,
                height: "100%",
                position: "relative",
                animationDelay: ".17s"
              }}
            >
              <div
                style={{
                  position: "relative",
                  fontFamily: "Oswald",
                  fontSize:
                    window.screen.width > 380
                      ? "32px"
                      : window.screen.width > 340
                        ? "24px"
                        : "28px",
                  fontWeight: "bold",
                  fontStyle: "normal",
                  fontStretch: "condensed",
                  lineHeight: "normal",
                  letterSpacing: "0.5px",
                  textAlign: "center",
                  color: "#fadd8d",
                  padding: "0px 2px",
                  zIndex: 2
                }}
              >
                {data.overs || 0}
              </div>
              <div
                style={{
                  position: "absolute",
                  top: "50%",
                  left: 0,
                  right: 0,
                  height: 2,
                  border: "0.3px solid #fff",
                  zIndex: 1,
                  opacity: 0.4
                }}
              />
            </div>
          </div>
        </div>
      </div>
    );
  };
  render() {
    const { data, currentMatch, history, matchId } = this.props;

    return (
      size(data) > 0 && (
        <React.Fragment>
          {!this.props.isHide && (
            // <Fact
            //   header="Time Machine"
            //   headerDescription="Did you Know : Every match is similar to 7 other mathes that have happened! Here's the closest match!"
            //   fact={
            //     !this.props.isHide && this.props.currentMatch && this.props.currentMatch.factEngine
            //   }
            // />
            <div>
              {/* <FactHeader header={"Time Machine"} description={"Did you Know : Every match is similar to 7 other mathes that have happened! Here's the closest match!"} /> */}
              <FactHeader
                header={"Similar Match Event"}
                description={
                  "What if today’s match were a remake of a match that had already happened?"
                }
              />
              <HrLine />
            </div>
          )}
          <div
            style={{
              marginBottom: 20
            }}
          >
            <MiniScoreCard
              score={currentMatch}
              history={history}
              matchId={matchId}
              isTimeMachine
            />
          </div>
          <div>
            <div
              style={{
                boxShadow: "0 3px 6px 0 rgba(0, 0, 0, 0.08)",
                backgroundColor: "#fffaeb",
                objectFit: "contain"
              }}
            >
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-start",
                  paddingLeft: "4.9%",
                  paddingBottom: "7.3%"
                }}
              >
                <div
                  style={{
                    borderRadius: "3px",
                    backgroundColor: "#141b2f"
                  }}
                >
                  <div
                    style={{
                      fontFamily: "Montserrat",
                      fontSize: "10px",
                      fontWeight: 500,
                      fontStyle: "normal",
                      fontStretch: "normal",
                      lineHeight: "normal",
                      letterSpacing: "0.5px",
                      color: "#ffffff",
                      padding: "4px 9px"
                    }}
                  >
                    {moment(
                      data &&
                        data[this.state.currentScoreIndex] &&
                        data[this.state.currentScoreIndex].date
                    ).format("DD MMMM YYYY")}
                  </div>
                </div>
              </div>
              <div style={{ display: "flex" }}>
                {this.generateSchedule({
                  matchNumber:
                    data &&
                    data[this.state.currentScoreIndex] &&
                    data[this.state.currentScoreIndex].matchNumber,
                  venue:
                    data &&
                    data[this.state.currentScoreIndex] &&
                    data[this.state.currentScoreIndex].venue,
                  title:
                    data &&
                    data[this.state.currentScoreIndex] &&
                    data[this.state.currentScoreIndex].displayMatchName
                })}
              </div>

              <div style={{ paddingBottom: "21px" }}>
                {this.createScoreCard({
                  flag:
                    data &&
                    data[this.state.currentScoreIndex] &&
                    data[this.state.currentScoreIndex].teamAAvatar
                      ? data[this.state.currentScoreIndex].teamAAvatar
                      : require("../../../images/flag_empty.svg"),
                  countryName:
                    data &&
                    data[this.state.currentScoreIndex] &&
                    data[this.state.currentScoreIndex].teamA.substring(0, 3),

                  score:
                    data &&
                    data[this.state.currentScoreIndex] &&
                    data[this.state.currentScoreIndex].teamARuns
                      .toString()
                      .split(""),
                  wickets:
                    data &&
                    data[this.state.currentScoreIndex] &&
                    data[this.state.currentScoreIndex].teamAWicket,
                  overs:
                    data &&
                    data[this.state.currentScoreIndex] &&
                    data[this.state.currentScoreIndex].teamAOver
                })}
              </div>
              <div />
              <div
                style={{
                  marginBottom: "21px",
                  display: this.state.showSecondInnings ? "block" : "none"
                }}
              >
                {this.createScoreCard({
                  flag:
                    data &&
                    data[this.state.currentScoreIndex] &&
                    data[this.state.currentScoreIndex].teamBAvatar
                      ? data[this.state.currentScoreIndex].teamBAvatar
                      : require("../../../images/flag_empty.svg"),
                  countryName:
                    data &&
                    data[this.state.currentScoreIndex] &&
                    data[this.state.currentScoreIndex].teamB
                      ? data[this.state.currentScoreIndex].teamB.substring(0, 3)
                      : "",
                  score:
                    data &&
                    data[this.state.currentScoreIndex] &&
                    data[this.state.currentScoreIndex].teamBRuns
                      .toString()
                      .split(""),
                  wickets:
                    data &&
                    data[this.state.currentScoreIndex] &&
                    data[this.state.currentScoreIndex].teamBWicket,
                  overs:
                    data &&
                    data[this.state.currentScoreIndex] &&
                    data[this.state.currentScoreIndex].teamBOver
                })}
              </div>

              <HrLine />

              <div>
                <div
                  style={{
                    borderRadius: "10px",
                    padding: "10px 8px"
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      display: !this.state.hidePlayButton ? "flex" : "none",
                      marginRight: 16
                    }}
                  >
                    <div
                      onClick={() => {
                        this.setState({
                          showSecondInnings: true,
                          showStatus: true,
                          currentScoreIndex: data.length - 1,
                          hidePlayButton: true,
                          animation: "animated flipInX",
                          animationOvers: "animated flipInY"
                        });
                      }}
                      style={{
                        borderRadius: "2px",
                        backgroundImage:
                          "linear-gradient(to left, #d44030, #9b000d)",
                        display: "flex",
                        // maxWidth: "20%",
                        padding: "5px 18px"
                      }}
                    >
                      <div
                        style={{
                          fontFamily: "Montserrat",
                          fontSize: "14px",
                          fontWeight: "600",
                          textAlign: "right",
                          color: "#ffffff"
                        }}
                      >
                        RESULT
                      </div>
                    </div>
                  </div>

                  {this.state.showStatus && (
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        padding: "4px 0"
                      }}
                    >
                      <div style={{ display: "flex", padding: "0 8px" }}>
                        <img
                          src={require("../../../images/capsuleLeftWingRed.png")}
                          alt=""
                          style={{ width: 26, height: 2 }}
                        />
                      </div>
                      <div
                        className="line_limit"
                        style={{
                          background: red_Orange,
                          fontFamily: "Montserrat",
                          fontSize: 11,
                          color: white,
                          padding: "2px 10px",
                          borderRadius: "12px",
                          display: "flex",
                          justifyContent: "center",
                          maxWidth: "85%",
                          whiteSpace: "initial"
                        }}
                      >
                        {data[this.state.currentScoreIndex].status}
                      </div>
                      <div style={{ display: "flex", padding: "0 8px" }}>
                        <img
                          src={require("../../../images/capsuleRightWingRed.png")}
                          alt=""
                          style={{ width: 26, height: 2 }}
                        />
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </React.Fragment>
      )
    );
  }
}

export default TimeMachine;
