import "!style-loader!css-loader!react-input-range/lib/css/index.css";
import cloneDeep from "lodash/cloneDeep";
import filter from "lodash/filter";

import React, { PureComponent } from "react";
import InputRange from "react-input-range";
import { grey_4, white } from "../../../styleSheet/globalStyle/color";
import CardGradientTitle from "../../commonComponent/CardGradientTitle";
import HrLine from "../../commonComponent/HrLine";
import Polling from "../../commonComponent/Polling";
import SmoothLineGraph from "../../commonComponent/SmoothLineGraph";
import Switch from "../../commonComponent/Switch";
import Fact from "../../Fact";

export class LiveScorePredictor extends PureComponent {
  state = {
    data: undefined,
    value: 5,
    predictedVisible: false
  };
  componentWillReceiveProps() {
    this.setState({
      data: filter(this.props.data.liveScores, function(o) {
        return o.inningNo === 1;
      })
    });
  }
  componentWillReceiveProps() {
    this.setState({
      data: filter(this.props.data.liveScores, function(o) {
        return o.inningNo === 1;
      })
    });
  }
  handleValueChange = value => {
    let x1, x2, y1, y2;
    // Filter the data here
    const clone = cloneDeep(this.state.data);
    x1 = clone[value - 1].overNo;
    y1 = clone[value - 1].currentScore;
    x2 = 8;
    y2 = clone[value - 1].predictedScore;
    let overDiff = x2 - x1;
    const distance =
      Math.round(Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2))) /
      overDiff;

    let counter = y1;
    clone.map(ele => {
      if (value < ele.overNo) {
        ele["Projected"] = true;
        ele["currentScore"] = counter + distance;
        counter += distance;
      }
    });

    this.setState({ data: clone, value, predictedVisible: true });
  };
  renderStats = () => {
    const styles = {
      root: {
        opacity: 0.97,
        borderRadius: "2px",
        backgroundImage: "linear-gradient(161deg, #0b1533, #050b1b)"
      },
      firstParent: {
        display: "flex",
        justifyContent: "space-between",
        padding: "9px 7px"
      },
      firstParentsFirstChild: {
        borderRadius: "7.5px",
        border: "solid 0.6px #9b9b9b",
        display: "flex",
        justifyContent: "center"
      },
      greenDot: {
        width: " 5px",
        height: "5px",
        backgroundColor: "#3bc306",
        alignSelf: "center",
        borderRadius: "50%",
        margin: "0px 4px"
      },
      LiveText: {
        fontFamily: "Montserrat",
        fontSize: "10px",
        fontWeight: 500,
        fontStyle: normal,
        fontStretch: normal,
        lineHeight: 1.1,
        letterSpacing: "1.1px",
        textAlign: "center",
        color: "#ccc",
        alignSelf: "center",
        padding: "2.5px 4px"
      },
      scoreCard: {
        fontFamily: "Montserrat",
        fontSize: "10px",
        fontWeight: 500,
        fontStyle: normal,
        fontStretch: normal,
        lineHeight: normal,
        letterSpacing: normal,
        textAlign: "center",
        color: "#ffffff",
        alignSelf: "center"
      },
      secondParent: {
        display: "flex",
        justifyContent: "space-between",
        padding: "9px 7px"
      },
      projectedScore: {
        fontFamily: "Montserrat",
        fontSize: "10px",
        fontWeight: normal,
        fontStyle: normal,
        fontStretch: normal,
        lineHeight: normal,
        letterSpacing: normal,
        textAlign: "center",
        color: "#ffffff",
        paddingLeft: "10px"
      }
    };
    return (
      <div style={styles.root}>
        <div style={styles.firstParent}>
          <div style={styles.firstParentsFirstChild}>
            <div style={styles.greenDot} />
            <div style={styles.LiveText}>LIVE</div>
          </div>

          <div style={styles.scoreCard}>IND 97/5 (10)</div>
        </div>
        <HrLine />
        <div style={styles.secondParent}>
          <div style={styles.projectedScore}>Projected Score:</div>
          <div
            style={{
              fontFamily: "Montserrat",
              fontSize: "10px",
              fontWeight: normal,
              fontStyle: normal,
              fontStretch: normal,
              lineHeight: normal,
              letterSpacing: normal,
              textAlign: "center",
              color: "#ffffff",
              paddingLeft: "10px"
            }}
          >
            IND 180/5 (20)
          </div>
        </div>
      </div>
    );
  };
  handleClick = val =>
    val === "IND"
      ? this.setState({
          data: filter(this.props.data.liveScores, function(o) {
            return o.inningNo === 1;
          })
        })
      : this.setState({
          data: filter(this.props.data.liveScores, function(o) {
            return o.inningNo === 2;
          })
        });
  handleChange = (event, value) => {
    this.setState({ value });
  };
  render() {
    const { data } = this.props;

    return (
      <div
        style={{
          padding: "16px 16px"
        }}
      >
        <div
          style={{
            background: grey_4
          }}
        >
          <Fact
            header="Live Score Predictor"
            headerDescription="What happens if a spinner bowled the last over ? Find out yourself"
            fact="Non in ut ex sunt enim nulla Lorem eiusmod irure reprehenderit sint incididunt."
          />
          <div
            style={{
              marginTop: "2%"
            }}
          >
            <div
              style={{
                background: white
              }}
            >
              <div
                style={{
                  ...styles.headerText,
                  flex: 1,
                  alignSelf: "center",
                  padding: "2.5% 4.9%"
                }}
              >
                Momentum Shift
              </div>
              <HrLine />
              <div
                style={{
                  padding: "8px 20px"
                }}
              >
                <Polling data={predictionMock} score={false} />
              </div>

              <CardGradientTitle title="Team Score Projections" />
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between"
                }}
              >
                <div
                  style={{
                    padding: "12px 12px"
                  }}
                >
                  {" "}
                  {this.renderStats()}
                </div>
                <div
                  style={{
                    paddingTop: "16px",
                    paddingRight: "17px"
                  }}
                >
                  Batting
                  <div>
                    <Switch
                      value1={"IND"}
                      value2={"AUS"}
                      callback={value => {
                        this.handleClick(value);
                      }}
                    />
                  </div>
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  padding: "12px 12px"
                }}
              >
                <div
                  style={{
                    fontFamily: "Montserrat",
                    fontSize: "10px",
                    fontWeight: 500,
                    fontStyle: normal,
                    fontStretch: normal,
                    lineHeight: normal,
                    letterSpacing: normal,
                    color: "#141b2f"
                  }}
                >
                  <span
                    style={{
                      fontWeight: 600,
                      color: "#ea6550"
                    }}
                  >
                    Projection:
                  </span>
                  <span
                    style={{
                      paddingLeft: 4
                    }}
                  >
                    IND to win by 20 runs
                  </span>
                </div>
                <div
                  style={{
                    padding: "5px 5px",
                    borderRadius: "3px",
                    backgroundColor: "#e3e4e6",
                    display: "flex",
                    justifyContent: "space-between"
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between"
                    }}
                  >
                    <div
                      style={{
                        width: "4px",
                        height: "4px",
                        backgroundColor: "#4a90e2",
                        borderRadius: "50%",
                        alignSelf: "center",
                        margin: "4px 5px"
                      }}
                    />
                    <div
                      style={{
                        fontFamily: "Montserrat",
                        fontSize: "10px",
                        fontWeight: 500,
                        fontStyle: normal,
                        fontStretch: normal,
                        lineHeight: normal,
                        letterSpacing: "0.5px",
                        textAlign: "right",
                        color: "#141b2f",
                        alignSelf: "center"
                      }}
                    >
                      IND
                    </div>
                  </div>
                  <div
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <div
                      style={{
                        width: "4px",
                        height: "4px",
                        backgroundColor: "#4a90e2",
                        alignSelf: "center",
                        borderRadius: "50%",
                        margin: "4px 5px"
                      }}
                    />
                    <div
                      style={{
                        fontFamily: "Montserrat",
                        fontSize: "10px",
                        fontWeight: 500,
                        fontStyle: normal,
                        fontStretch: normal,
                        lineHeight: normal,
                        letterSpacing: "0.5px",
                        textAlign: "right",
                        color: "#141b2f",
                        alignSelf: "center"
                      }}
                    >
                      AUS
                    </div>
                  </div>
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "center"
                }}
              >
                <SmoothLineGraph
                  predictedVisible={this.state.predictedVisible}
                  data={this.state.data}
                />
              </div>
              <HrLine />
              <div
                style={{
                  padding: 30
                }}
              >
                <InputRange
                  draggableTrack
                  formatLabel={value => `${value}cm`}
                  maxValue={8}
                  minValue={0}
                  value={this.state.value}
                  onChange={this.handleValueChange}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LiveScorePredictor;

const normal = "normal";
const styles = {
  headerText: {
    fontFamily: "mont500",
    fontSize: "12px",
    fontWeight: "500",
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: normal,
    letterSpacing: normal,
    color: "#141b2f"
  }
};
// const slider = {};
let predictionMock = {
  summary: null,
  teamA: "India",
  teamAPercentage: "65",
  teamB: "Pakistan",
  teamBpercentage: "24",
  tiePercent: "11"
};
