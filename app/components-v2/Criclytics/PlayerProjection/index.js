import { filter, find, findIndex, maxBy } from "lodash";
import React, { PureComponent } from "react";
import ReactSpeedometer from "react-d3-speedometer";
import { grey_2, white, grey_4 } from "../../../styleSheet/globalStyle/color";
import HorizontalList from "../../commonComponent/HorizontalList";
import HrLine from "../../commonComponent/HrLine";
import Switch from "../../commonComponent/Switch";
import Fact from "../../Fact";
import FanFight from "../../commonComponent/FanFight";
import Reveal from "react-reveal/Reveal";
import animate from "../../../utils/animationConfig";
import CardGradientTitle from "../../commonComponent/CardGradientTitle";

const batIcon = require("../../../images/Batsman.png");
const ballIcon = require("../../../images/ball_orange.png");
let pieRange;
export class PlayerProjection extends PureComponent {
  state = {
    slides: [],
    pieValue: 0,
    needleAngle: null,
    data: [],
    activeTab: "",
    maxValue: undefined,
    initialFilterMatch: false
  };

  static getDerivedStateFromProps(props, state) {
    if (
      !state.initialFilterMatch &&
      props.currentMatch &&
      props.data &&
      props.data.playerList &&
      props.data.playerList.length > 0
    ) {
      const currentMatch = props.currentMatch;
      let filterTeam = null;
      if (currentMatch !== undefined) {
        filterTeam = filter(props.data.playerList, function(o) {
          return o.teamId === currentMatch.teamA.teamKey;
        });
      }
      const slides = [];
      filterTeam &&
        filterTeam.length > 0 &&
        filterTeam.map((ele, ind) => {
          const obj = {
            image: ele.avatar,
            name: ele.playerName,
            active: ind === 0
          };
          slides.push(obj);
        });

      return {
        ...state,
        initialFilterMatch: true,
        data: filterTeam,
        pieValue: 0,
        needleAngle: 0,
        activeTab: currentMatch && currentMatch.teamA.teamKey,
        slides
      };
    }
    return null;
  }

  filterMatches = () => {
    const currentMatch = this.props.currentMatch;
    let filterTeam = null;
    if (currentMatch) {
      filterTeam = filter(this.props.data.playerList, function(o) {
        return o.teamId === currentMatch.teamA.teamKey;
      });
    }

    this.props.data.playerList &&
      this.setState(
        {
          data: filterTeam,
          pieValue: 0,
          needleAngle: 0,
          activeTab: currentMatch && currentMatch.teamA.shortName
        },
        () => {
          filterTeam &&
            filterTeam.length > 0 &&
            filterTeam.map((ele, ind) => {
              this.setState(prevState => ({
                slides: [
                  ...prevState.slides,
                  {
                    image: ele.avatar,
                    name: ele.playerName,
                    active: ind === 0
                  }
                ]
              }));
            });
        }
      );
  };

  componentDidMount() {
    this.handleActiveTeam(this.props.currentMatch.teamA.teamKey);
  }

  handleActiveTeam = value => {
    this.setState({
      slides: [],
      pieValue: 0,
      activeTab: value
    });

    const currentMatch = this.props.currentMatch;

    const filterTeam = filter(this.props.data.playerList, [
      "teamId",
      value === currentMatch.teamA.teamKey
        ? currentMatch.teamA.teamKey
        : currentMatch.teamB.teamKey
    ]);

    let slides = [];

    filterTeam.sort(function(a, b) {
      return a.avatar !== null && b.avatar === null
        ? -1
        : b.avatar !== null && a.avatar === null
          ? 1
          : 0;
    });

    filterTeam.length > 0 &&
      filterTeam.map((ele, ind) => {
        this.setState(prevState => ({
          data: filterTeam,
          slides: [
            ...prevState.slides,
            {
              image: ele.avatar,
              name: ele.playerName,
              active: ind === 0
            }
          ].sort(function(a, b) {
            return a.image !== null && b.image === null
              ? -1
              : b.image !== null && a.image === null
                ? 1
                : 0;
          })
        }));
      });
  };

  generatePlayerName = data => {
    return (
      <React.Fragment>
        <div
          style={{
            display: "flex"
          }}
        >
          <img
            style={{
              width: "32px",
              height: "4%",
              objectFit: "contain",
              alignSelf: "center",
              paddingRight: "10px"
            }}
            src={
              this.state.data &&
              this.state.data[this.state.pieValue] &&
              this.state.data[this.state.pieValue].playerRole === "Batsman"
                ? batIcon
                : ballIcon
            }
          />
          <div style={styles.batsmanName}>
            {this.state.slides.length > 0
              ? this.state.slides[this.state.pieValue].name
              : "Loading"}
          </div>
        </div>
        <div>
          <span style={styles.projectedText}>Projected</span>
          {this.state.data &&
            this.state.data[this.state.pieValue] &&
            this.state.data[this.state.pieValue].playerRole && (
              <span
                style={{
                  marginLeft: 6,
                  borderRadius: "12px",
                  backgroundColor: "#ffe8d9"
                }}
              >
                <span style={styles.projectedRuns}>
                  {this.state.data &&
                    this.state.data[this.state.pieValue].playerRole ===
                      "Batsman" &&
                    ` ${
                      this.state.data[this.state.pieValue].listProjections[0]
                        .bound
                    } - ${
                      this.state.data[this.state.pieValue].listProjections[2]
                        .bound
                    } runs`}

                  {this.state.data &&
                    this.state.data[this.state.pieValue].playerRole ===
                      "Bowler" &&
                    `${
                      this.state.data[this.state.pieValue].listProjections[0]
                        .bound
                    } - ${
                      this.state.data[this.state.pieValue].listProjections[2]
                        .bound
                    }
                     wickets`}
                </span>
              </span>
            )}
        </div>
      </React.Fragment>
    );
  };

  getPieRange = val => {
    // console.log(val);
    if (val < 10) {
      if (val <= 3) {
        return 3;
      } else if (val > 3 && val <= 6) {
        return 6;
      } else if (val > 6 && val < 10) {
        return 15;
      }
    } else {
      const dividedVal = Number(val) / 3;
      let pieValue = (parseInt(dividedVal / 10, 10) + 1) * 10 * 3;
      // console.log(pieValue);
      if (pieValue > 60 && pieValue <= 150) {
        pieValue = 150;
      }
      if (pieValue > 150 && pieValue <= 300) {
        pieValue = 300;
      }
      return pieValue;
    }
  };

  generatePlayerStats = data => {
    const Value =
      this.state.data &&
      this.state.data[this.state.pieValue] &&
      this.state.data[this.state.pieValue].listProjections[1].bound;
    const highValue =
      this.state.data &&
      this.state.data[this.state.pieValue] &&
      this.state.data[this.state.pieValue].listProjections[2].bound;

    if (
      this.state.data &&
      this.state.data[this.state.pieValue] &&
      this.state.data[this.state.pieValue].playerRole === "Batsman" &&
      highValue > 60
    ) {
      pieRange = 150;
    } else if (
      this.state.data &&
      this.state.data[this.state.pieValue] &&
      this.state.data[this.state.pieValue].playerRole === "Batsman" &&
      highValue <= 60
    ) {
      pieRange = 60;
    } else {
      pieRange = this.props.data.maxBallScore <= 3 ? 3 : 6;
    }
    return (
      <div
        style={{
          position: "relative",
          zIndex: 0
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "row",

            justifyContent: "center",
            overflow: "hidden",
            position: "relative"
          }}
        >
          <div
            style={{
              position: "absolute",
              right: 0,
              width: "56%",
              top: 14
            }}
          >
            {this.generatePlayerName()}
          </div>

          <div
            style={{
              position: "absolute",
              justifyContent: "flex-end",
              width: "100%",
              height: 110,
              backgroundColor: "#141b2f",
              zIndex: -1,
              bottom: 0,
              right: 0
            }}
          />
          <div
            style={{
              flex: 0.5,
              display: "flex",
              width: 128,
              height: 200
            }}
          >
            <Reveal
              key={this.state.pieValue}
              effect={animate.playerProjector.image.effect}
              duration={animate.playerProjector.image.duration}
            >
              <img
                src={
                  this.state.slides &&
                  this.state.slides[this.state.pieValue] &&
                  this.state.slides[this.state.pieValue].image !== null
                    ? this.state.slides[this.state.pieValue].image
                    : require("../../../images/fallbackProjection.png")
                }
                style={{
                  height: "100%"
                }}
              />
            </Reveal>
          </div>
          <div
            style={{
              flex: 0.5,
              display: "flex",
              flexDirection: "column",
              justifyContent: "flex-end"
            }}
          >
            <div
              key={Value}
              style={{
                width: 200,
                height: 120,
                marginBottom: "-11px"
              }}
            >
              <ReactSpeedometer
                value={Value}
                maxValue={this.getPieRange(highValue)}
                minValue={0}
                // forceRender={true}
                fluidWidth
                currentValueText=""
                ringWidth={30}
                maxSegmentLabels={3}
                needleHeightRatio={0.9}
                needleColor="#fff"
                startColor="#d44030"
                segments={3}
                endColor="#ffb11b"
                textColor={grey_2}
                valueFormat="d"
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  handleTeamSwitch = team => {
    this.handleActiveTeam(team.teamKey);
  };

  render() {
    const { data, isHide, currentMatch, isHideFanFightBanner } = this.props;

    return (
      <React.Fragment>
        {!isHide && (
          <Fact
            header="Match Projection"
            fact={currentMatch && currentMatch.factEngine}
            headerDescription="There are more to scorecards than strange patterns. Find out who could be today's top performer."
            hideFactEngine
          />
        )}

        <div>
          <div
            style={{
              background: white,
              ...this.props.homeStyle
            }}
          >
            {/* {!isHideFanFightBanner && (
              <div
                style={{
                  padding: "0 2px 10px"
                }}
              >
                <FanFight
                  rootStyles={{
                    margin: 0,
                    borderRadius: 0,
                    padding: "0 10px"
                  }}
                  url={
                    "https://fanfight.com/?utm_source=cdc&utm_medium=projection&utm_campaign=CDC_projection"
                  }
                />
              </div>
            )} */}
            {/* <HrLine /> */}
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                fontFamily: "mont600",
                flex: 1,
                fontSize: 12,
                background: "#fdfdfd",
                color: "#727682"
              }}
            >
              <div
                style={{
                  padding: "10px 0px",
                  flex: 0.49,
                  textAlign: "center",
                  // borderRight: "1px solid #c2c2c2",
                  borderBottom:
                    this.state.activeTab === currentMatch.teamA.teamKey
                      ? "2px solid #a70e13"
                      : "",
                  color:
                    this.state.activeTab === currentMatch.teamA.teamKey
                      ? "#141b2f"
                      : "",
                  fontFamily:
                    this.state.activeTab === currentMatch.teamA.teamKey
                      ? "mont600"
                      : "mont500"
                }}
                onClick={() => this.handleTeamSwitch(currentMatch.teamA)}
              >
                {this.props.team1Name ||
                  (currentMatch && currentMatch.teamA.name)}
              </div>
              <div
                style={{
                  width: 1,
                  height: 20,
                  marginTop: "3%",
                  background: "#c2c2c2"
                }}
              />
              <div
                style={{
                  padding: "10px 0px",
                  flex: 0.49,
                  textAlign: "center",
                  borderBottom:
                    this.state.activeTab === currentMatch.teamB.teamKey
                      ? "2px solid #a70e13"
                      : "",
                  color:
                    this.state.activeTab === currentMatch.teamB.teamKey
                      ? "#141b2f"
                      : "",
                  fontFamily:
                    this.state.activeTab === currentMatch.teamB.teamKey
                      ? "mont600"
                      : "mont500"
                }}
                onClick={() => this.handleTeamSwitch(currentMatch.teamB)}
              >
                {this.props.team2Name ||
                  (currentMatch && currentMatch.teamB.name)}
              </div>
            </div>
            <HrLine />
            {currentMatch &&
              currentMatch.format != "TEST" && (
                <CardGradientTitle
                  title="1st Batting Score Projection"
                  rootStyles={{
                    padding: 12,
                    color: "#727682",
                    fontSize: 10,
                    textTransform: "uppercase"
                  }}
                />
              )}
            {currentMatch &&
              currentMatch.format != "TEST" && (
                <div
                  style={{
                    display: "flex",
                    fontFamily: "mont600",
                    fontSize: 12,
                    justifyContent: "space-between",
                    alignItems: "center",
                    padding: "12px"
                  }}
                >
                  <div>Predicted Score</div>
                  <div style={{ display: "flex" }}>
                    {this.state.activeTab !== "" &&
                      data &&
                      data.preMatchTeamsProjections &&
                      data.preMatchTeamsProjections[
                        this.state.activeTab
                      ].teamScore
                        .split("")
                        .map((ele, key) => (
                          <div
                            key={key}
                            style={{
                              display: "flex",
                              minWidth: 26,
                              height: "100%",
                              borderRadius: "2px",
                              backgroundColor: "#141b2f",
                              position: "relative",
                              justifyContent: "center",
                              alignItems: "center",
                              marginRight: "2px",
                              animationDelay: ".17s"
                            }}
                          >
                            <div
                              style={{
                                position: "relative",
                                fontFamily: "Oswald",
                                fontSize:
                                  window.screen.width > 380
                                    ? "32px"
                                    : window.screen.width > 340
                                      ? "24px"
                                      : "28px",
                                fontWeight: "bold",
                                fontStyle: "normal",
                                fontStretch: "condensed",
                                lineHeight: "normal",
                                letterSpacing: "0.5px",
                                textAlign: "center",
                                color: "#fff",
                                padding: "0px 2px",
                                zIndex: 2
                              }}
                            >
                              {ele}
                            </div>
                            <div
                              style={{
                                position: "absolute",
                                top: "50%",
                                left: 0,
                                right: 0,
                                height: 2,
                                border: "0.3px solid #fff",
                                zIndex: 1,
                                opacity: 0.4
                              }}
                            />
                          </div>
                        ))}
                  </div>
                </div>
              )}
            <HrLine />
            <CardGradientTitle
              title="player Score Projector"
              rootStyles={{
                padding: 12,
                color: "#727682",
                fontSize: 10,
                textTransform: "uppercase"
              }}
            />
            {currentMatch &&
              currentMatch.format == "TEST" && (
                <div
                  style={{
                    fontFamily: "Montserrat",
                    fontSize: 10,
                    color: grey_4,
                    paddingLeft: 12
                  }}
                >
                  * Pojections are for the complete match
                </div>
              )}
            <div
              style={{
                padding: "25px 0px",
                background: "#fff"
              }}
            >
              {this.generatePlayerStats(data)}
            </div>
            <HorizontalList
              callBack={value => {
                if (value !== this.state.pieValue) {
                  const clone = Array.from(this.state.slides);

                  find(clone, ["active", true]).active = false;
                  clone[value].active = true;

                  const angle = findIndex(
                    this.state.data[value].listProjections,
                    maxBy(this.state.data[value].listProjections, function(o) {
                      return o.probabilities;
                    })
                  );
                  this.setState({
                    slides: clone,
                    needleAngle: angle,
                    pieValue: value
                  });
                }
              }}
              slides={this.state.slides}
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const normal = "normal";
const styles = {
  headerText: {
    fontFamily: "mont400",
    fontSize: "11px",
    // fontWeight: "500",
    textTransform: "uppercase",
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: normal,
    letterSpacing: normal,
    color: "#727682",
    padding: "5px 16px"
  },
  batsmanName: {
    fontFamily: "mont600",
    fontSize: "16px",
    fontWeight: 600,
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: 1.6,
    letterSpacing: normal,
    color: "#141b2f"
    // paddingLeft: 32
  },
  projectedText: {
    fontFamily: "Montserrat",
    fontSize: "12px",
    fontWeight: normal,
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: 1.42,
    letterSpacing: "0.3px",
    color: "#727682"
    // marginLeft: 35
  },
  projectedRuns: {
    fontFamily: "Montserrat",
    fontSize: "11px",
    fontWeight: normal,
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: 1.36,
    letterSpacing: normal,
    color: "#141b2f",
    padding: "2px 8px"
  }
};
export default PlayerProjection;
