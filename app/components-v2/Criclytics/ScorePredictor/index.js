import Add from "@material-ui/icons/AddCircleOutline";
import Cancel from "@material-ui/icons/Cancel";
import cloneDeep from "lodash/cloneDeep";
import differenceWith from "lodash/differenceWith";
import isEqual from "lodash/isEqual";
import size from "lodash/size";
import {ToastsContainer, ToastsStore, ToastsContainerPosition} from 'react-toasts';

import React, { PureComponent } from "react";
import {
  grey_8,
  white,
  red_Orange,
  blue_grey
} from "../../../styleSheet/globalStyle/color";
import CardGradientTitle from "../../commonComponent/CardGradientTitle";
import ListItem from "../../commonComponent/ListItems";
import Switch from "../../commonComponent/Switch";
import Fact from "../../Fact";
import HrLine from "../../commonComponent/HrLine";

export class ScorePredictor extends PureComponent {
  state = {
    data: [],
    currentSwitchVal: null,
    isProbableActive: true,
    textToShow: "",
    playing11: null,
    probable: null
  };

  filterTeamAndSquads = () => {
    let points = 0;
    const clone =
      this.props.data &&
      this.props.data[this.state.currentSwitchVal] &&
      cloneDeep(
        this.props.data[this.state.currentSwitchVal].probablePlayingEleven
      );

    const probable =
      size(this.props.data[this.state.currentSwitchVal]) &&
      differenceWith(
        this.props.data[this.state.currentSwitchVal].squadProjection,
        this.props.data[this.state.currentSwitchVal].probablePlayingEleven,
        isEqual
      );
    this.props.data &&
      this.props.data[this.state.currentSwitchVal] &&
      this.props.data[this.state.currentSwitchVal].probablePlayingEleven.map(
        ele => {
          ele.points === "NA" ? points : (points += parseInt(ele.points));
        }
      );
    this.setState({
      points,
      playing11: clone,
      probable,
      isProbableActive: true
    });
  };
  handleChange = value => {
    this.setState(
      {
        currentSwitchVal:
          value === this.props.currentMatch.teamA.shortName
            ? this.props.currentMatch.teamA.teamKey
            : this.props.currentMatch.teamB.teamKey
      },
      () => {
        this.filterTeamAndSquads();
      }
    );
  };
  componentWillMount() {
    this.setState(
      {
        currentSwitchVal:
          this.props.currentMatch && this.props.currentMatch.teamA.teamKey,
        data: this.props.data
      },
      () => {
        this.filterTeamAndSquads();
      }
    );
  }
  componentDidMount() {
    this.setState(
      {
        currentSwitchVal:
          this.props.currentMatch && this.props.currentMatch.teamA.teamKey,
        data: this.props.data
      },
      () => {
        this.filterTeamAndSquads();
      }
    );
  }
  componentWillReceiveProps() {
    this.setState(
      {
        currentSwitchVal:
          this.props.currentMatch && this.props.currentMatch.teamA.teamKey,
        data: this.props.data
      },
      () => {
        this.filterTeamAndSquads();
      }
    );
  }

  render() {
    const { currentMatch } = this.props;

    return (
      <React.Fragment>
        <Fact
          header="1st Batting Score Projector"
          headerDescription="Your team has been put into bat! Pick the playing XI and simulate the first innings score"
          fact={currentMatch && currentMatch.factEngine}
          hideFactEngine
        />
        {/* Red Button Projected Score */}

        <ToastsContainer position={ToastsContainerPosition.BOTTOM_CENTER}  store={ToastsStore}/>
        <div
          style={{
            backgroundImage: this.state.isProbableActive
              ? "linear-gradient(262deg, #d44030, #9b000d)"
              : "",
            backgroundColor: this.state.isProbableActive ? "" : "#a1a4ac",
            display: "flex",
            padding: "16px 17px",
            justifyContent: "space-between"
            // position: "sticky",
            // top: 140
            // position: "fixed",
            // bottom: 0,
            // top:0
            // left: 0,
            // right: 0
          }}
        >
          {!this.state.isProbableActive && (
            <div
              style={{
                fontFamily: "Montserrat",
                fontSize: "14px",
                fontWeight: 600,
                fontStyle: normal,
                fontStretch: normal,
                lineHeight: normal,
                letterSpacing: normal,
                color: "#ffffff",
                alignSelf: "center",
                textAlign: "center"
              }}
            >
              Please Pick 11 players
            </div>
          )}
          {this.state.isProbableActive && (
            <React.Fragment>
              <div
                style={{
                  fontFamily: "Montserrat",
                  fontSize: "14px",
                  fontWeight: 600,
                  fontStyle: normal,
                  fontStretch: normal,
                  lineHeight: normal,
                  letterSpacing: normal,
                  color: "#ffffff",
                  alignSelf: "center"
                }}
              >
                Projected Score
              </div>
              <div
                style={{
                  fontFamily: "Montserrat",
                  fontSize: "14px",
                  fontWeight: 600,
                  fontStyle: normal,
                  fontStretch: normal,
                  lineHeight: normal,
                  letterSpacing: normal,
                  color: "#ffffff",
                  alignSelf: "center"
                }}
              >
                |
              </div>
              <div
                style={{
                  alignItems: "center"
                }}
              >
                <span style={styles.teaText}>
                  {this.props.currentSwitchVal}
                </span>
                <span
                  style={{
                    fonFamily: "Oswald",
                    fontSize: "15px",
                    fontWeight: normal,
                    fontStyle: normal,
                    fontStretch: normal,
                    lineHeight: normal,
                    letterSpacing: normal,
                    textAlign: "right",
                    color: white,

                    padding: "0px 5px"
                  }}
                >
                  {this.state.points || 0}
                </span>
                <span style={styles.teaText}>runs</span>
              </div>
            </React.Fragment>
          )}
        </div>

        {/* Red Button Projected Score closing */}

        <div
          style={{
            display: "flex",
            justifyContent: "center",
            background: white
          }}
        >
          <div style={{ ...styles.headerText, flex: 1, alignSelf: "center" }}>
            Probable Playing XI
          </div>
          <div
            style={{
              padding: "2.7% 3.7%"
            }}
          >
            <Switch
              value1={currentMatch ? currentMatch.teamA.shortName : null}
              value2={currentMatch ? currentMatch.teamB.shortName : null}
              callback={value => {
                this.handleChange(value);
              }}
            />
          </div>
        </div>
        <HrLine />
        {this.state.playing11 &&
          this.state.playing11.map((item, index) => {
            return (
              <div key={item.key}>
                <div>
                  <div
                    style={{
                      background: white
                    }}
                  >
                    <ListItem
                      param={index + 1}
                      onClick={val => {
                        this.setState(prevState => ({
                          probable: [...prevState.probable, item],
                          isProbableActive: false
                        }));

                        this.state.playing11.splice(index, 1);
                      }}
                      isAvatar
                      avatar={
                        item.avatar ||
                        require("../../../images/fallbackProjection.png")
                      }
                      avatarWrapperStyles={{
                        height: 30,
                        width: 30,
                        overflow: "hidden",
                        borderRadius: "50%",
                        display: "initial",
                        background: grey_8
                      }}

                      avatarStyle={{
                        width: "100%",
                        objectFit: "cover"
                        // height: "unset"
                      }}
                      title={item.name}
                      rightElement={<Cancel style={{ color: red_Orange }} />}
                    />
                  </div>
                </div>
              </div>
            );
          })}

        <CardGradientTitle title="Squad" />
        {this.state.probable &&
          this.state.probable.map((item, index) => (
            <div key={item.key}>
              <div>
                <div
                  style={{
                    background: white
                  }}
                >
                  <ListItem
                    param={index + 1}
                    onClick={val => {
                      if (this.state.playing11.length === 11) {
                        let points = 0;
                        this.state.playing11.map(ele => {
                          ele.points === "NA"
                            ? points
                            : (points += parseInt(ele.points));
                        });

                        this.setState({
                          points: points,
                          isProbableActive: true
                        });
                        ToastsStore.error("Maximum players selected")
                        return;


                      } else {
                        this.setState(
                          prevState => ({
                            playing11: [...prevState.playing11, item]
                          }),
                          () => {
                            if (this.state.playing11.length === 11) {
                              let points = 0;
                              this.state.playing11.map(ele => {
                                ele.points === "NA"
                                  ? points
                                  : (points += parseInt(ele.points));
                              });

                              this.setState({
                                points: points,
                                isProbableActive: true
                              });
                              ToastsStore.error("Maximum players selected")
                              return;
                            }
                          }
                        );

                        this.state.probable.splice(index, 1);
                      }
                    }}
                    isAvatar
                    avatar={
                      item.avatar ||
                      require("../../../images/fallbackProjection.png")
                    }
                    avatarWrapperStyles={{
                      height: 30,
                      width: 30,
                      overflow: "hidden",
                      borderRadius: "50%",
                      display: "initial",
                      background: grey_8
                    }}
                    avatarStyle={{
                      width: "100%",
                      objectFit: "cover"
                      // height: "unset"
                    }}
                    title={item.name}
                    rightElement={<Add style={{ color: blue_grey }} />}
                  />
                </div>
              </div>
            </div>
          ))}
        
      </React.Fragment>
    );
  }
}

export default ScorePredictor;
const normal = "normal";
const styles = {
  headerText: {
    fontFamily: "Montserrat",
    fontSize: "12px",
    fontWeight: 500,
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: normal,
    letterSpacing: normal,
    color: "#141b2f",
    padding: "13px 16px"
  },
  teaText: {
    fonFamily: "Montserrat",
    fontSize: "14px",
    fontWeight: 600,
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: normal,
    letterSpacing: normal,
    color: "#ffffff"
  }
};
