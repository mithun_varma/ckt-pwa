import React, { Component } from "react";
import { grey_8, grey_6 } from "../../../styleSheet/globalStyle/color";

class PlayerStatsCard extends Component {
  render() {
    const { labelOne, labelTwo, type, data, teamName } = this.props;
    return (
      <div style={{ flex: 0.49, padding: 14 }}>
        <div
          style={{
            textAlign: "center"
          }}
        >
          {teamName || "-"}
        </div>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            padding: "12px 0"
          }}
        >
          <div
            style={{
              height: 24,
              width: 24,
              overflow: "hidden",
              borderRadius: "50%",
              background: grey_6
              // marginRight: 12
            }}
          >
            <img
              src={data && data.avatar || require("../../../images/fallbackProjection.png")}
              alt=""
              style={{
                width: "100%",
                objectFit: "cover"
              }}
            />
          </div>
          <div
            style={{
              fontSize: 12,
              fontWeight: 600,
              marginLeft: 8
            }}
          >
            {data && data.name || "-"}
          </div>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            fontSize: 12,
            fontWeight: 500,
            padding: "4px 0"
          }}
        >
          <div
            style={{
              color: grey_8
            }}
          >
            {labelOne}
          </div>
          <div
            style={{
              fontFamily: "Oswald"
            }}
          >
            {data && data[type] || 0}
          </div>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            fontSize: 12,
            fontWeight: 500,
            padding: "4px 0"
          }}
        >
          <div
            style={{
              color: grey_8
            }}
          >
            {labelTwo}
          </div>
          <div
            style={{
              fontFamily: "Oswald"
            }}
          >
            {data && data.average || "0"}
          </div>
        </div>
      </div>
    );
  }
}

export default PlayerStatsCard;
