import React, { Component } from "react";
import {
  grey_10,
  white,
  grey_4,
  grey_5_1,
  cardShadow,
  grey_8,
  grey_6,
  grey_5,
  red_Orange,
  red_8
} from "../../../styleSheet/globalStyle/color";
import CardGradientTitle from "../../commonComponent/CardGradientTitle";
import HrLine from "../../commonComponent/HrLine";
import StatsCard from "../CriclyticsCommon/StatsCard";
import PlayerStatsCard from "./PlayerStatsCard";
import Fact from "../../Fact";
import { NotFound } from "../../../containers/CriclyticsSlider";

class KeyStats extends Component {
  // state = {
  //   initial: false
  // };

  // static getDerivedStateFromProps = (nextprops, state) => {
  //   if (nextprops.data && nextprops.data.head2Head && !nextprops.initial) {
  //     return {
  //       initial: true
  //     };
  //   }
  //   return null;
  // };
  render() {
    const { data, format } = this.props;
    return data && data.headToHead ? (
      <div
        style={{
          fontFamily: "Montserrat",
          fontSize: 12,
          fontWeight: 400,
          color: grey_10,
          background: "#edeff4"
        }}
      >
        <Fact
          header="Key Stats"
          headerDescription=""
          hideFactEngine
          hideHrLine
        />
        {data.headToHead &&
          data.headToHead.head2Head &&
          data.headToHead.head2Head.matches &&
          +data.headToHead.head2Head.matches > 0 && (
            <div
              style={{
                background: white,
                boxShadow: cardShadow,
                marginTop: 20
              }}
            >
              <CardGradientTitle
                title="Head to Head"
                subtitle={`*last ${format == "TEST" ? "10 matches" : "3 years"}`}
              />
              <div
                style={{
                  padding: 16
                }}
              >
                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    textAlign: "center",
                    fontWeight: 600,
                    fontSize: 12,
                    color: white,
                    minHeight: 30,
                    background: "#d8d8d8",
                    justifyContent: "center"
                  }}
                >
                  <div
                    style={{
                      // width: "60%",
                      width:
                        data.headToHead &&
                        data.headToHead.head2Head &&
                        data.headToHead.head2Head.teamA &&
                        data.headToHead.head2Head.matches
                          ? (data.headToHead.head2Head.teamA /
                              data.headToHead.head2Head.matches) *
                              100 +
                            "%"
                          : "0%",
                      // background: "#1c83ca",
                      background:
                        (data.headToHead &&
                          data.headToHead.teamA &&
                          data.headToHead.teamA.color &&
                          data.headToHead.teamA.color.length > 1 &&
                          `linear-gradient(to right, ${
                            data.headToHead.teamA.color[0]
                          }, ${data.headToHead.teamA.color[1]})`) ||
                        red_Orange,
                      padding: "6px 0"
                    }}
                  >
                    {data.headToHead && data.headToHead.head2Head
                      ? data.headToHead.head2Head.teamA
                      : 0}
                  </div>
                  <div
                    style={{
                      // width: "30%",
                      width:
                        data.headToHead &&
                        data.headToHead.head2Head &&
                        data.headToHead.head2Head.teamB &&
                        data.headToHead.head2Head.matches
                          ? (data.headToHead.head2Head.teamB /
                              data.headToHead.head2Head.matches) *
                              100 +
                            "%"
                          : "0%",
                      // background: "#dbd023",
                      background:
                        (data.headToHead &&
                          data.headToHead.teamB &&
                          data.headToHead.teamB.color &&
                          data.headToHead.teamB.color.length > 1 &&
                          `linear-gradient(to right, ${
                            data.headToHead.teamB.color[0]
                          }, ${data.headToHead.teamB.color[1]})`) ||
                        red_8,
                      padding: "6px 0"
                    }}
                  >
                    {data.headToHead && data.headToHead.head2Head
                      ? data.headToHead.head2Head.teamB
                      : 0}
                  </div>
                  <div
                    style={{
                      // width: "10%",
                      width:
                        data.headToHead &&
                        data.headToHead.head2Head &&
                        data.headToHead.head2Head.other &&
                        data.headToHead.head2Head.matches
                          ? (data.headToHead.head2Head.other /
                              data.headToHead.head2Head.matches) *
                              100 +
                            "%"
                          : "0%",
                      background: "#d8d8d8",
                      padding: "6px 0",
                      textAlign: "center"
                    }}
                  >
                    {data.headToHead && data.headToHead.head2Head
                      ? data.headToHead.head2Head.other
                      : 0}
                  </div>
                </div>
                {/* line separator */}
                <div
                  style={{
                    position: "relative",
                    marginTop: 16
                  }}
                >
                  <div
                    style={{ width: "100%", height: 2, background: grey_5_1 }}
                  />
                  <div
                    style={{
                      background: white,
                      fontWeight: 500,
                      position: "absolute",
                      left: "39%",
                      top: -8,
                      padding: "0 12px"
                    }}
                  >
                    {data.headToHead && data.headToHead.head2Head
                      ? data.headToHead.head2Head.matches
                      : 0}{" "}
                    Matches
                  </div>
                </div>

                {/* countary name and label */}
                <div
                  style={{
                    display: "flex",
                    flex: 1,
                    marginTop: 22,
                    fontSize: 10
                  }}
                >
                  <div style={{ flex: 0.33 }}>
                    <div
                      style={{
                        width: 18,
                        height: 2,
                        // background: "linear-gradient(to right, #ffef05, #073e49)",
                        background:
                          (data.headToHead &&
                            data.headToHead.teamA &&
                            data.headToHead.teamA.color &&
                            data.headToHead.teamA.color.length > 1 &&
                            `linear-gradient(to right, ${
                              data.headToHead.teamA.color[0]
                            }, ${data.headToHead.teamA.color[1]})`) ||
                          red_Orange,
                        marginBottom: 3
                      }}
                    />
                    <div>
                      {data.headToHead &&
                      data.headToHead.teamA &&
                      data.headToHead.teamA.teamName
                        ? data.headToHead.teamA.teamName
                        : "-"}
                    </div>
                  </div>
                  <div style={{ flex: 0.33 }}>
                    <div
                      style={{
                        width: 18,
                        height: 2,
                        // background: "linear-gradient(to right, #ffef05, #073e49)",
                        background:
                          (data.headToHead &&
                            data.headToHead.teamB &&
                            data.headToHead.teamB.color &&
                            data.headToHead.teamB.color.length > 1 &&
                            `linear-gradient(to right, ${
                              data.headToHead.teamB.color[0]
                            }, ${data.headToHead.teamB.color[1]})`) ||
                          red_8,
                        marginBottom: 3
                      }}
                    />
                    <div>
                      {data.headToHead &&
                      data.headToHead.teamB &&
                      data.headToHead.teamB.teamName
                        ? data.headToHead.teamB.teamName
                        : "-"}
                    </div>
                  </div>
                  <div style={{ flex: 0.34 }}>
                    <div
                      style={{
                        width: 18,
                        height: 2,
                        background: "#d8d8d8",
                        marginBottom: 3
                      }}
                    />
                    <div>{`${format == "TEST" ? "Draw" : "No Result"}/Tie`}</div>
                  </div>
                </div>
              </div>
            </div>
          )}
        {/* Head to Head section Starts closing */}

        {/* Stadium Details card Section starts */}
        {data.headToHead &&
          data.headToHead.venueStats &&
        <div
          style={{
            background: white,
            boxShadow: cardShadow,
            margin: "16px 0"
          }}
        >
          <div
            style={{
              padding: "10px 16px",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              fontSize: 10,
              fontWeight: 500
            }}
          >
            <div style={{}}>STADIUM DETAILS</div>
            <div>
              *<span style={{ fontWeight: 400, color: grey_8 }}>
                {`Last ${format == "TEST" ? "5" :"3"} Years`}
              </span>
            </div>
          </div>
          <HrLine />
          {data.headToHead &&
            data.headToHead.venueStats &&
            data.headToHead.venueStats.venueName && (
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  padding: "16px 12px"
                }}
              >
                <img
                  src={require("../../../images/location-icon-color.png")}
                  alt=""
                  style={{
                    width: 9,
                    height: 12
                  }}
                />
                <span
                  style={{
                    fontSize: 12,
                    fontWeight: 500,
                    color: grey_8,
                    marginLeft: 6
                  }}
                >
                  {data.headToHead &&
                  data.headToHead.venueStats &&
                  data.headToHead.venueStats.venueCity
                    ? data.headToHead.venueStats.venueCity
                    : "-"}
                  {/* Wankhede Stadium, Mumbai [Hard coded] */}
                </span>
              </div>
            )}

          <div
            style={{
              display: "flex",
              flex: 1,
              padding: "10px 0 18px"
            }}
          >
            <StatsCard
              rootStyles={{
                flex: 0.34
              }}
              title={`1st ${format == "TEST" ? "Inning" :"Batting"} Average Score`}
              titleStyles={{
                fontSize: 10,
                color: grey_8,
                padding: "0 6px",
                textAlign: "center",
                fontFamily: "Montserrat",
                fontWeight: 400
              }}
              // subtitle="178"
              subtitle={
                data.headToHead && data.headToHead.venueStats
                  ? data.headToHead.venueStats.avgFirstInningScore || "0"
                  : "0"
              }
              subtitleStyles={{
                fontFamily: "Oswald",
                fontWeight: 500,
                fontSize: 26,
                color: grey_10
              }}
            />
            <StatsCard
              rootStyles={{
                flex: 0.34
              }}
              title={"1st Batting win percentage"}
              titleStyles={{
                fontSize: 10,
                color: grey_8,
                padding: "0 6px",
                textAlign: "center",
                fontFamily: "Montserrat",
                fontWeight: 400
              }}
              subtitle={
                data.headToHead && data.headToHead.venueStats
                  ? (data.headToHead.venueStats.firstBattingWinPercent || "0") +
                    "%"
                  : "0%"
              }
              subtitleStyles={{
                fontFamily: "Oswald",
                fontWeight: 500,
                fontSize: 26,
                color: grey_10
              }}
            />

            <StatsCard
              rootStyles={{
                flex: 0.34,
                border: "none"
              }}
              title={`Highest  ${format == "TEST" ? "4th innings" : "score"} chased`}
              titleStyles={{
                fontSize: 10,
                color: grey_8,
                padding: "0 6px",
                textAlign: "center",
                fontFamily: "Montserrat",
                fontWeight: 400
              }}
              subtitle={
                data.headToHead && data.headToHead.venueStats
                  ? data.headToHead.venueStats.highestScoreChased || "0"
                  : "0"
              }
              subtitleStyles={{
                fontFamily: "Oswald",
                fontWeight: 500,
                fontSize: 26,
                color: grey_10
              }}
            />
          </div>
          <HrLine />
          <div
            style={{
              padding: "10px 16px",
              fontSize: 12,
              fontWeight: 500,
              color: grey_8
            }}
          >
            Wickets Split
          </div>
          <div
            style={{
              display: "flex",
              flex: 1,
              padding: "10px 0 18px"
            }}
          >
            <StatsCard
              rootStyles={{
                flex: 0.5
              }}
              title={"Pace"}
              titleStyles={{
                fontSize: 10,
                color: grey_8,
                padding: "0 6px",
                textAlign: "center",
                fontFamily: "Montserrat",
                fontWeight: 400
              }}
              subtitle={
                data.headToHead && data.headToHead.venueStats
                  ? (data.headToHead.venueStats.paceWicketPercent || "0") + "%"
                  : "0%"
              }
              subtitleStyles={{
                fontFamily: "Oswald",
                fontWeight: 500,
                fontSize: 26,
                color: grey_10
              }}
            />
            <StatsCard
              rootStyles={{
                flex: 0.5,
                border: "none"
              }}
              title={"Spin"}
              titleStyles={{
                fontSize: 10,
                color: grey_8,
                padding: "0 6px",
                textAlign: "center",
                fontFamily: "Montserrat",
                fontWeight: 400
              }}
              subtitle={
                data.headToHead && data.headToHead.venueStats
                  ? (data.headToHead.venueStats.spinWicketPercent || "0") + "%"
                  : "0%"
              }
              subtitleStyles={{
                fontFamily: "Oswald",
                fontWeight: 500,
                fontSize: 26,
                color: grey_10
              }}
            />
          </div>
        </div>
        }
        {/* Stadium Details card Section closing */}

        {/* Top Run Scorer in Last 5 Matches Section starts */}
        {
          data &&
          data.topRunScorerData &&
          data.topRunScorerData.length > 0 &&
        <div
          style={{
            background: white,
            boxShadow: cardShadow
          }}
        >
          <div
            style={{
              padding: "10px 16px",
              fontSize: 10,
              fontWeight: 500
            }}
          >
            <div style={{}}>TOP RUN SCORER IN LAST 5 MATCHES</div>
          </div>
          <HrLine />
          <div
            style={{
              display: "flex",
              flex: 1
            }}
          >
            {/* Card 1 */}
            <PlayerStatsCard
              labelOne="Runs"
              labelTwo="Average"
              type="runs"
              data={
                data &&
                data.topRunScorerData &&
                data.topRunScorerData.length > 0
                  ? data.topRunScorerData[0]
                  : false
              }
              teamName={
                data && data.headToHead.teamA && data.headToHead.teamA.teamName
                  ? data.headToHead.teamA.teamName
                  : "-"
              }
            />
            <div
              style={{
                height: 50,
                display: "flex",
                alignSelf: "center",
                width: 1,
                background: grey_5
              }}
            />

            {/* Card 2 */}
            <PlayerStatsCard
              labelOne="Runs"
              labelTwo="Average"
              type="runs"
              data={
                data &&
                data.topRunScorerData &&
                data.topRunScorerData.length > 0
                  ? data.topRunScorerData[1]
                  : false
              }
              teamName={
                data && data.headToHead.teamB && data.headToHead.teamB.teamName
                  ? data.headToHead.teamB.teamName
                  : "-"
              }
            />
          </div>
        </div>
        }
        {/* Top Run Scorer in Last 5 Matches section closing */}

        {/* Top Wicket Taker in Last 5 Matches Section starts */}
        {
          data &&
          data.topWicketTakerData &&
          data.topWicketTakerData.length > 0 &&
        <div
          style={{
            background: white,
            boxShadow: cardShadow,
            margin: "16px 0"
          }}
        >
          <div
            style={{
              padding: "10px 16px",
              fontSize: 10,
              fontWeight: 500
            }}
          >
            <div style={{}}>TOP WICKET TAKER IN LAST 5 MATCHES</div>
          </div>
          <HrLine />
          <div
            style={{
              display: "flex",
              flex: 1
            }}
          >
            {/* Card 1 */}
            <PlayerStatsCard
              labelOne="Wickets"
              labelTwo="Average"
              type="wickets"
              data={
                data &&
                data.topWicketTakerData &&
                data.topWicketTakerData.length > 0
                  ? data.topWicketTakerData[0]
                  : false
              }
              teamName={
                data && data.headToHead.teamA && data.headToHead.teamA.teamName
                  ? data.headToHead.teamA.teamName
                  : "-"
              }
            />
            <div
              style={{
                height: 50,
                display: "flex",
                alignSelf: "center",
                width: 1,
                background: grey_5
              }}
            />

            {/* Card 2 */}
            <PlayerStatsCard
              labelOne="Wickets"
              labelTwo="Average"
              type="wickets"
              data={
                data &&
                data.topWicketTakerData &&
                data.topWicketTakerData.length > 0
                  ? data.topWicketTakerData[1]
                  : false
              }
              teamName={
                data && data.headToHead.teamB && data.headToHead.teamB.teamName
                  ? data.headToHead.teamB.teamName
                  : "-"
              }
            />
          </div>
        </div>
        }
        {/* Top Wicket Taker in Last 5 Matches section closing */}
      </div>
    ) : (
      <div>
        <div
          style={{
            padding: "16px 16px"
          }}
        >
          <div
            style={{
              background: grey_4
            }}
          >
            <NotFound message="No Keystats found for the match" />
          </div>
        </div>
      </div>
    );
  }
}

export default KeyStats;
