import React, { Component } from "react";
import {
  grey_8,
  orange_8,
  grey_5,
  grey_5_1,
  green_10,
  red_10,
  cardShadow
} from "../../../styleSheet/globalStyle/color";
import HrLine from "../../commonComponent/HrLine";
import maxBy from "lodash/maxBy";

class CriclyticsLiveStats extends Component {
  render() {
    const { data, batOrder, isBowler } = this.props;
    // console.log(data, batOrder);
    // const {
    //   battingAndBowlingProjectionsLive: { battingProbabilitiesWrapper }
    // } = data;
    // console.log(battingProbabilitiesWrapper);
    return  batOrder && batOrder.length > 0 ? (
      batOrder.map(
        (bat, key) =>
          bat && (
            <React.Fragment>
              <div
                style={{
                  display: "flex",
                  flex: 1,
                  // alignItems: "center",
                  justifyContent: "space-between",
                  padding: "14px 16px"
                }}
              >
                {/* 1 */}
                <div
                  style={{
                    textAlign: "left",
                    flex: 0.2,
                    paddingTop: 5
                  }}
                >
                  <div
                    style={{
                      fontFamily: "Oswald",
                      fontWeight: 400,
                      fontSize: 20
                    }}
                  >
                    {!isBowler
                      ? key === 0
                        ? `${data.strikerRuns}*`
                        : `${data.nonStrikerRuns}`
                      : `${data.wicketsTakenTillNow}/${data.bowlerRuns}`}
                  </div>
                  <div
                    style={{
                      fontSize: 12,
                      color: grey_8,
                      minHeight: 18
                    }}
                  >
                    {!isBowler
                      ? key === 0
                        ? `(${data.strikerBalls})`
                        : `(${data.nonStrikerBalls})`
                      : `(${data.oversBowledSoFar})`}
                  </div>
                </div>

                {/* 2 */}
                <div
                  style={{
                    display: "flex",
                    flex: 0.6,
                    flexDirection: "column",
                    alignItems: "center",
                    position: "relative"
                  }}
                >
                  <div
                    style={{
                      position: "absolute",
                      display: "flex",
                      left: 0,
                      right: 0,
                      top: "26%",
                      justifyContent: "space-between"
                    }}
                  >
                    <div
                      style={{
                        width: 6,
                        height: 6,
                        borderRadius: "50%",
                        background: orange_8
                      }}
                    />
                    <div
                      style={{
                        width: "90%",
                        borderTop: `1px dashed ${grey_5}`,
                        marginTop: 3
                      }}
                    />
                    <div
                      style={{
                        width: 6,
                        height: 6,
                        borderRadius: "50%",
                        background: green_10
                      }}
                    />
                  </div>
                  <div
                    style={{
                      height: 45,
                      width: 45,
                      overflow: "hidden",
                      borderRadius: "50%",
                      background: grey_5_1,
                      zIndex: 1
                    }}
                  >
                    <img
                      src={
                        !isBowler
                          ? data.playerBattingProbabilities[bat].avatar ||
                            require("../../../images/fallbackProjection.png")
                          : data.avatar ||
                            require("../../../images/fallbackProjection.png")
                      }
                      alt=""
                      style={{
                        width: "100%",
                        objectFit: "cover"
                      }}
                    />
                  </div>
                  <div
                    style={{
                      fontWeight: 600,
                      fontSize: 14,
                      marginTop: 10
                    }}
                  >
                    {!isBowler
                      ? data.playerBattingProbabilities[bat].playerName
                      : data.playerName}
                  </div>
                </div>

                {/* 3 */}
                <div
                  style={{
                    textAlign: "right",
                    flex: 0.2,
                    paddingTop: 5
                  }}
                >
                  <div
                    style={{
                      fontFamily: "Oswald",
                      fontWeight: 400,
                      fontSize: 20,
                      color: red_10
                    }}
                  >
                    {!isBowler
                      ? maxBy(
                          data.playerBattingProbabilities[bat].listProjections,
                          "probabilities"
                        ).bound
                      : maxBy(data.bowlingProjections, "probabilities").bound}
                  </div>
                  <div
                    style={{
                      fontSize: 12,
                      color: grey_8
                    }}
                  >
                    {!isBowler ? "Runs" : "Wickets "}
                  </div>
                </div>
              </div>
              {!isBowler && <HrLine />}
            </React.Fragment>
          )
      )
    ) : (
      <div>null</div>
    );
  }
}

export default CriclyticsLiveStats;
