import React, { Component } from "react";
import {
  grey_8,
  orange_8,
  grey_5,
  grey_5_1,
  green_10,
  red_10,
  cardShadow,
  avatarShadow
} from "../../../styleSheet/globalStyle/color";

class CriclyticsTeamStats extends Component {
  render() {
    const { data, scoreCard, inningsBreak } = this.props;
    const innings1 = data.filter(dt => dt.inningNo === 1);
    const innings2 = data.filter(dt => dt.inningNo === 2);
    const innings = [innings1, innings2];
    const teams = [
      scoreCard.firstBatting === scoreCard.teamA.teamKey
        ? scoreCard.teamA
        : scoreCard.teamB,
      scoreCard.firstBatting !== scoreCard.teamA.teamKey
        ? scoreCard.teamA
        : scoreCard.teamB
    ];
    return !inningsBreak ? (
      teams && teams.length > 0 && innings1.length > 0 ? (
        teams.map((team, key) => (
          <div
            style={{
              display: "flex",
              flex: 1,
              // alignItems: "center",
              justifyContent: "space-between",
              padding: "14px 16px"
            }}
          >
            {/* 1 */}
            <div
              style={{
                textAlign: "left",
                flex: 0.2,
                paddingTop: 5
              }}
            >
              <div
                style={{
                  fontFamily: "Oswald",
                  fontWeight: 400,
                  fontSize: 20
                }}
              >
                {scoreCard &&
                scoreCard.inningOrder.indexOf(`${team.teamKey}_1`) > -1
                  ? `${scoreCard.inningCollection[`${team.teamKey}_1`].runs}/${
                      scoreCard.inningCollection[`${team.teamKey}_1`].wickets
                    }`
                  : "--"}
              </div>
              {scoreCard &&
                scoreCard.inningOrder.indexOf(`${team.teamKey}_1`) > -1 && (
                  <div
                    style={{
                      fontSize: 12,
                      color: grey_8,
                      minHeight: 18
                    }}
                  >
                    {`(${
                      scoreCard.inningCollection[`${team.teamKey}_1`].overs
                    })`}
                  </div>
                )}
            </div>

            {/* 2 */}
            <div
              style={{
                display: "flex",
                flex: 0.6,
                flexDirection: "column",
                alignItems: "center",
                position: "relative"
              }}
            >
              <div
                style={{
                  position: "absolute",
                  display: "flex",
                  left: 0,
                  right: 0,
                  top: "26%",
                  justifyContent: "space-between"
                }}
              >
                <div
                  style={{
                    width: 6,
                    height: 6,
                    borderRadius: "50%",
                    background: orange_8
                  }}
                />
                <div
                  style={{
                    width: "90%",
                    borderTop: `1px dashed ${grey_5}`,
                    marginTop: 3
                  }}
                />
                <div
                  style={{
                    width: 6,
                    height: 6,
                    borderRadius: "50%",
                    background: green_10
                  }}
                />
              </div>
              <div
                style={{
                  width: 34,
                  height: 24,
                  marginTop: 7,
                  zIndex: 1
                }}
              >
                <img
                  src={team.avatar}
                  alt=""
                  style={{
                    width: "100%",
                    height: "auto",
                    // boxShadow: cardShadow
                    boxShadow: avatarShadow
                  }}
                />
              </div>
              <div
                style={{
                  fontWeight: 600,
                  fontSize: 14,
                  marginTop: 10
                }}
              >
                {team.shortName}
              </div>
            </div>

            {/* 3 */}
            <div
              style={{
                textAlign: "right",
                flex: 0.2,
                paddingTop: 5
              }}
            >
              <div
                style={{
                  fontFamily: "Oswald",
                  fontWeight: 400,
                  fontSize: 20,
                  color: red_10
                }}
              >
                {scoreCard.inningOrder.length > 1
                  ? key === 0
                    ? "--"
                    : innings2.length > 0
                      ? innings[key][innings[key].length - 1].predictedScore
                      : "--"
                  : key === 0
                    ? innings[key][innings[key].length - 1].predictedScore
                    : innings[0][innings[0].length - 1].secondPredictedScore}
              </div>
              <div
                style={{
                  fontSize: 12,
                  color: grey_8
                }}
              >
                {scoreCard.inningOrder.length > 1
                  ? key === 0
                    ? ""
                    : `(${
                        innings2.length > 0
                          ? innings[key][innings[key].length - 1].predictedOver
                          : "--"
                      })`
                  : key === 0
                    ? `(${innings[key][innings[key].length - 1].predictedOver})`
                    : `(${
                        innings[0][innings[0].length - 1].secondPredictedOVer
                      })`}
              </div>
            </div>
          </div>
        ))
      ) : (
        <div />
      )
    ) : (
      <div>
        <div
          style={{
            display: "flex",
            flex: 1,
            // alignItems: "center",
            justifyContent: "space-between",
            padding: "14px 16px"
          }}
        >
          {/* 1 */}
          <div
            style={{
              textAlign: "left",
              flex: 0.2,
              paddingTop: 5
            }}
          >
            <div
              style={{
                fontFamily: "Oswald",
                fontWeight: 400,
                fontSize: 20
              }}
            >
              --
            </div>
          </div>

          {/* 2 */}
          <div
            style={{
              display: "flex",
              flex: 0.6,
              flexDirection: "column",
              alignItems: "center",
              position: "relative"
            }}
          >
            <div
              style={{
                position: "absolute",
                display: "flex",
                left: 0,
                right: 0,
                top: "26%",
                justifyContent: "space-between"
              }}
            >
              <div
                style={{
                  width: 6,
                  height: 6,
                  borderRadius: "50%",
                  background: orange_8
                }}
              />
              <div
                style={{
                  width: "90%",
                  borderTop: `1px dashed ${grey_5}`,
                  marginTop: 3
                }}
              />
              <div
                style={{
                  width: 6,
                  height: 6,
                  borderRadius: "50%",
                  background: green_10
                }}
              />
            </div>
            <div
              style={{
                width: 34,
                height: 24,
                marginTop: 7,
                zIndex: 1
              }}
            >
              <img
                src={teams[1].avatar}
                alt=""
                style={{
                  width: "100%",
                  height: "auto",
                  boxShadow: avatarShadow
                }}
              />
            </div>
            <div
              style={{
                fontWeight: 600,
                fontSize: 14,
                marginTop: 10
              }}
            >
              {teams[1].shortName}
            </div>
          </div>

          {/* 3 */}
          <div
            style={{
              textAlign: "right",
              flex: 0.2,
              paddingTop: 5
            }}
          >
            <div
              style={{
                fontFamily: "Oswald",
                fontWeight: 400,
                fontSize: 20,
                color: red_10
              }}
            >
              {data[data.length - 1].secondPredictedScore}
            </div>
            ({data[data.length - 1].secondPredictedOVer})
          </div>
        </div>
      </div>
    );
  }
}

export default CriclyticsTeamStats;
