import React, { Component } from "react";
import {
  grey_5_1,
  grey_10,
  red_10,
  white
} from "../../../styleSheet/globalStyle/color";
import HrLine from "../../commonComponent/HrLine";

class ClipTitle extends Component {
  render() {
    return (
      <React.Fragment>
        <HrLine />
        <div
          style={{
            // position: "absolute",
            top: 0,
            left: 0,
            width: "100%",
            background: white,
            height: 23,
            // clipPath: "polygon(0 0, 100% 0, 79% 100%, 0 100%)",
            paddingLeft: 10,
            margin: "6px 0px",
            fontWeight: 500,
            fontSize: 12,
            color: grey_10,
            display: "flex",
            alignItems: "center",
            opacity: 0.8,
            justifyContent: "space-between"
          }}
        >
          {this.props.title}
          <div style={{ marginRight: 10, display: "flex" }}>
            <span
              style={{
                color: red_10,
                fontWeight: 600,
                verticalAlign: "middle"
              }}
            >
              *
            </span>
            <span
              style={{
                fontWeight: 500,
                fontSize: 10,
                color: grey_10
              }}
            >
              {this.props.subText}
            </span>
          </div>
        </div>
        <HrLine />
      </React.Fragment>
    );
  }
}

export default ClipTitle;
