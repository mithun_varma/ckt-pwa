import React, { Component } from "react";
import { grey_8, grey_5 } from "../../../styleSheet/globalStyle/color";

class StatsCard extends Component {
  render() {
    return (
      <div
        style={{
          flex: 0.25,
          textAlign: "center",
          borderRight: `1px solid ${grey_5}`,
          fontFamily: "Montserrat",
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
          ...this.props.rootStyles
        }}
      >
        <div
          style={{
            fontFamily: "Oswald",
            fontWeight: 500,
            fontSize: 26,
            ...this.props.titleStyles
          }}
        >
          {this.props.title}
        </div>
        <div
          style={{
            color: grey_8,
            padding: "0 6px",
            textAlign: "center",
            ...this.props.subtitleStyles
          }}
        >
          {this.props.subtitle}
        </div>
      </div>
    );
  }
}

export default StatsCard;
