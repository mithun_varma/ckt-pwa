import React, { Component } from "react";
import {
  grey_5_1,
  grey_10,
  grey_8,
  red_10
} from "../../../styleSheet/globalStyle/color";
import HrLine from "../../commonComponent/HrLine";

class CriclyticsListItems extends Component {
  render() {
    const { data } = this.props;
    return (
      <div>
        <div
          style={{
            padding: "12px 16px",
            display: "flex",
            flex: 1,
            alignItems: "center",
            justifyContent: "space-between"
          }}
        >
          <div style={{ display: "flex", alignItems: "center" }}>
            <div
              style={{
                height: 38,
                width: 38,
                overflow: "hidden",
                borderRadius: "50%",
                marginRight: 12,
                background: grey_5_1
              }}
            >
              <img
                src={
                  data.avatar ||
                  require("../../../images/fallbackProjection.png")
                }
                alt=""
                style={{
                  width: "100%",
                  objectFit: "cover"
                }}
              />
            </div>

            <div
              style={{
                fontFamily: "Montserrat",
                fontSize: 12,
                color: grey_10
              }}
            >
              <div
                style={{
                  fontWeight: 600,
                  fontSize: 14
                }}
              >
                {data.playerName}
              </div>
              <div
                style={{
                  fontWeight: 400,
                  fontSize: 12,
                  color: grey_8
                }}
              >
                {data.playerRole}
              </div>
            </div>
          </div>
          <div style={{ textAlign: "right" }}>
            <div
              style={{
                fontFamily: "Oswald",
                fontWeight: 400,
                fontSize: 20,
                color: red_10
              }}
            >
              {`${data.listProjections[0].bound} - ${
                data.listProjections[2].bound
              }`}
            </div>
            <div
              style={{
                fontSize: 10
              }}
            >
              {data.playerRole == "Batsman" ? "Runs" : "Wickets"}
            </div>
          </div>
        </div>
        <HrLine />
      </div>
    );
  }
}

export default CriclyticsListItems;
