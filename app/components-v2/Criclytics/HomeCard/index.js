import React, { PureComponent } from "react";
import ReactTouchEvents from "react-touch-events";
import { green_10, white } from "../../../styleSheet/globalStyle/color";
import HrLine from "../../commonComponent/HrLine";
import ListItem from "../../commonComponent/ListItems";
export class CriclyticsCard extends PureComponent {
  handleSwipe = direction => {
    this.props.history.push(`/criclytics-slider/${this.props.matchId}`);
  };

  state = {
    showShadow: false // no idea why is this here
  };
  render() {
    const { score } = this.props;
    // console.warn(score);
    let teamCollection = [
      {
        teamName:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.fullName
            : score.teamB.fullName,
        shortName:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.shortName
            : score.teamB.shortName,
        teamKey:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.teamKey
            : score.teamB.teamKey,
        runs: [],
        wickets: [],
        overs: [],
        avatar:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.avatar
            : score.teamB.avatar
      },
      {
        teamName:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.fullName
            : score.teamB.fullName,
        shortName:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.shortName
            : score.teamB.shortName,
        teamKey:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.teamKey
            : score.teamB.teamKey,
        runs: [],
        wickets: [],
        overs: [],
        avatar:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.avatar
            : score.teamB.avatar
      }
    ];
    if (score.status !== "UPCOMING") {
      for (const order of score.inningOrder) {
        let team = order.split("_")[0];
        if (team === score.firstBatting) {
          (teamCollection[0].runs = [
            ...teamCollection[0].runs,
            score.inningCollection[order].runs
          ]),
            (teamCollection[0].wickets = [
              ...teamCollection[0].wickets,
              score.inningCollection[order].wickets
            ]),
            (teamCollection[0].overs = [
              ...teamCollection[0].overs,
              score.inningCollection[order].overs
            ]);
        } else {
          (teamCollection[1].runs = [
            ...teamCollection[1].runs,
            score.inningCollection[order].runs
          ]),
            (teamCollection[1].wickets = [
              ...teamCollection[1].wickets,
              score.inningCollection[order].wickets
            ]),
            (teamCollection[1].overs = [
              ...teamCollection[1].overs,
              score.inningCollection[order].overs
            ]);
        }
      }
    } else if (score.status == "UPCOMING") {
      for (let i = 0; i < teamCollection.length; i++) {
        (teamCollection[i].teamName =
          score[i == 0 ? "teamA" : "teamB"].fullName),
          (teamCollection[i].shortName =
            score[i == 0 ? "teamA" : "teamB"].shortName),
          (teamCollection[i].teamKey =
            score[i == 0 ? "teamA" : "teamB"].teamKey);
        (teamCollection[i].runs = []),
          (teamCollection[i].wickets = []),
          (teamCollection[i].overs = []),
          (teamCollection[i].avatar = score[i == 0 ? "teamA" : "teamB"].avatar);
      }
    }
    console.log(teamCollection);
    return (
      <ReactTouchEvents onTap={this.handleSwipe}>
        <div>
          <div
            style={{
              backgroundColor: "transparent",
              padding: "10px 0px 0px 0px",
              cursor: "pointer"
            }}
          >
            {score.status == "RUNNING" && (
              <div
                style={{
                  padding: "8px 16px"
                }}
              >
                <div
                  style={{
                    display: "flex",
                    width: 40,
                    border: "1px solid #333333",
                    borderRadius: 13,
                    // padding: "0px 4px",
                    alignItems: "center",
                    fontSize: 8,
                    color: "#cccccc",
                    justifyContent: "center",
                    fontFamily: "mont500"
                  }}
                >
                  <span
                    style={{
                      background: green_10,
                      width: 4,
                      height: 4,
                      borderRadius: "50%"
                    }}
                  />
                  <span style={{ marginLeft: 4 }}>LIVE</span>
                </div>
              </div>
            )}
            <ListItem
              isHR={true}
              // customColorHR={}
              titleStyle={{
                color: "#a1a4ac",
                lineHeight: 1.79,
                fontWeight: 500,
                fontSize: "12px"

                // width: "50%"
              }}
              iconColor={white}
              title={`${score.relatedName || "NA"} . ${score.title}`}
              rootStyles={{
                padding: "0px 12px 0 16px"
              }}
            />
            <div
              style={{
                display: "flex",
                justifyContent: "flex-start",
                padding: "0 7px 10px",
                flex: 1
              }}
            >
              {score &&
                score.status !== "UPCOMING" && (
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: 1,
                      justifyContent: "flex-start",
                      paddingBottom: "8px"
                    }}
                  >
                    {teamCollection &&
                      teamCollection.length > 0 &&
                      teamCollection.map((team, index) => (
                        <div
                          style={{
                            display: "flex",
                            alignItems: "center",
                            flex: 1,
                            marginTop: 12
                          }}
                        >
                          <div style={{ flex: 0.3, display: "flex" }}>
                            <img
                              style={{
                                width: "auto",
                                height: "19px",
                                paddingLeft: "9px"
                              }}
                              src={
                                team.avatar
                                  ? team.avatar
                                  : team.teamName == "tbc" ||
                                    team.teamName == "TBC"
                                    ? require("../../../images/TBC-Flag.png")
                                    : require("../../../images/flag_empty.svg")
                              }
                            />
                            <div
                              style={{
                                alignSelf: "center",
                                paddingLeft: "10px",
                                fontFamily: "Montserrat",
                                fontSize: "12px",
                                fontWeight: "normal",
                                fontStyle: "normal",
                                fontStretch: "normal",
                                lineHeight: "normal",
                                letterSpacing: "normal",
                                textAlign: "center",
                                color: "#a1a4ac"
                              }}
                            >
                              {(team.shortName && team.shortName.length > 4
                                ? team.shortName.substring(0, 4).toUpperCase()
                                : team.shortName.toUpperCase()) || "-"}
                            </div>
                          </div>
                          <div
                            style={{
                              flex: 0.6,
                              alignSelf: "center",
                              paddingLeft: "10px",
                              fontFamily: "Montserrat",
                              fontWeight: `${
                                score.status === "RUNNING" &&
                                team.teamKey ===
                                  (score.currentScoreCard &&
                                    score.currentScoreCard.battingTeam)
                                  ? "700"
                                  : "500"
                              }`,
                              fontSize: "12px",
                              color: white
                            }}
                          >
                            {`${
                              team.runs[0] !== undefined
                                ? `${team.runs[0]}`
                                : ""
                            }${
                              team.wickets[0] !== undefined &&
                              team.wickets[0] < 10
                                ? `/${team.wickets[0]}`
                                : ""
                            }`}
                            <span style={{ fontWeight: 400, marginLeft: 8 }}>
                              {score.format !== "TEST" &&
                                team.overs[0] &&
                                `(${team.overs[0]})`}
                            </span>
                            {team.runs.length > 1 &&
                              ` & ${
                                team.runs[1] !== undefined ? team.runs[1] : ""
                              }${
                                team.wickets[1] !== undefined &&
                                team.wickets[1] < 10
                                  ? "/" + team.wickets[1]
                                  : ""
                              }`}
                            <span style={{ fontWeight: 400, marginLeft: 8 }}>
                              {team.runs.length > 1 &&
                                score.format !== "TEST" &&
                                `(${team.overs[1]})`}
                            </span>
                          </div>
                        </div>
                      ))}
                  </div>
                )}
              {score &&
                score.status === "UPCOMING" && (
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      flex: 1,
                      justifyContent: "flex-start",
                      padding: "8px 0"
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                        flex: 0.5,
                        marginBottom:12
                      }}
                    >
                      <img
                        style={{
                          width: "auto",
                          height: "19px",
                          paddingLeft: "9px"
                        }}
                        src={
                          teamCollection.length > 0 && teamCollection[0].avatar
                        }
                      />
                      <div
                        style={{
                          alignSelf: "center",
                          paddingLeft: "10px",
                          fontFamily: "Montserrat",
                          fontSize: "12px",
                          fontWeight: "normal",
                          fontStyle: "normal",
                          fontStretch: "normal",
                          lineHeight: "normal",
                          letterSpacing: "normal",
                          textAlign: "center",
                          color: "#a1a4ac"
                        }}
                      >
                        {teamCollection.length > 0 &&
                          teamCollection[0].shortName}
                      </div>
                    </div>
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                        flex: 0.5
                      }}
                    >
                      <img
                        style={{
                          width: "auto",
                          height: "19px",
                          paddingLeft: "9px"
                        }}
                        src={
                          teamCollection.length > 0 && teamCollection[1].avatar
                        }
                      />
                      <div
                        style={{
                          alignSelf: "center",
                          paddingLeft: "10px",
                          fontFamily: "Montserrat",
                          fontSize: "12px",
                          fontWeight: "normal",
                          fontStyle: "normal",
                          fontStretch: "normal",
                          lineHeight: "normal",
                          letterSpacing: "normal",
                          textAlign: "center",
                          color: "#a1a4ac"
                        }}
                      >
                        {teamCollection.length > 0 &&
                          teamCollection[1].shortName}
                      </div>
                    </div>
                  </div>
                )}
            </div>

            <HrLine
              style={{
                background:
                  "linear-gradient(to left, rgba(255, 255, 255, 0.15), #ffffff 17%, #ffffff 87%, rgba(255, 255, 255, 0.15))",
                opacity: 0.3
              }}
            />
          </div>
        </div>
      </ReactTouchEvents>
    );
  }
}

export default CriclyticsCard;
