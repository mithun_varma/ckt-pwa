import maxBy from "lodash/maxBy";
import React, { PureComponent } from "react";
import { grey_10, white } from "../../../styleSheet/globalStyle/color";
import { BarGraph } from "../../commonComponent/BarGraph";
import HrLine from "../../commonComponent/HrLine";
import ListItem from "../../commonComponent/ListItems";
import Fact from "../../Fact";

const findMax = arr => {
  let max = arr[0].probabilities;
  let val = "";
  let test = 0;
  arr.map((ele, ind) => {
    test = arr[ind].probabilities
    // if (arr[ind].probabilities > max) {
    if (+test > +max) {
      max = ele.probabilities;
      val = ele.bound
    }
  });
  if (!val && arr && arr[1] && arr[1].bound) {
    val = "< "+arr[1].bound.split("-")[0]
  }
  return val;
};
export class LivePlayerPrediction extends PureComponent {
  state = {
    data: null
  };

  generateBowlers = (bowler, bowlingData) => {
    return (
      <div
        style={{
          background: white,
          marginTop: "4.5%"
        }}
      >
        <div style={styles.heeaderText}>Wickets Projector</div>
        <HrLine />
        <ListItem
          isHR={true}
          avatarStyle={{
            width: "60px",
            height: "60px",
            objectFit: "contain",
            borderRadius: "50%"
          }}
          isAvatar
          avatar={
            bowlingData.avatar ||
            require("../../../images/fallbackProjection.png")
          }
          title={bowlingData.playerName}
          rightElement={
            <div
              style={{
                fontFamily: "Oswald",
                fontWeight: 500,
                fontSize: 30,
                color: grey_10,
                display: "flex",
                alignItems: "flex-end"
              }}
            >
              {`${bowlingData.wicketsTakenTillNow}/${bowlingData.bowlerRuns}`}{" "}
              <span
                style={{
                  fontFamily: "Oswald",
                  fontWeight: 500,
                  fontSize: 14,
                  margin: "0px 0 8px 5px"
                }}
              >
                ({bowlingData.oversBowledSoFar || 0})
              </span>
            </div>
          }
        />

        <React.Fragment>
          <div
            style={{
              padding: "10px 15px"
            }}
          >
            <div
              style={{
                borderRadius: "10px",
                backgroundColor: "#ffe8d9"
              }}
            >
              <div style={styles.CriclyticsPredicts}>
                Criclytics predicts {bowlingData.playerName} to pick{" "}
                {/* {bowler && maxBy(bowler, "probabilities").bound} wickets */}
                {bowler && findMax(bowler)} wickets
                {/* {bowler && bowler.sort((a,b) => (+a.probabilities < +b.probabilities) ? 1 : ((+b.probabilities < +a.probabilities) ? -1 : 0))[0].bound} wickets */}
              </div>
            </div>
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              marginTop: 20
            }}
          >
            <BarGraph kind="Bowler" data={bowler || null} />
          </div>
        </React.Fragment>
      </div>
    );
  };
  generateBatsman = (batsmen, players, score, balls) => {
    if (!batsmen) {
      return <div>No Data</div>;
    }

    return (
      <div
        style={{
          background: white
        }}
      >
        <div style={styles.heeaderText}>Batting Score Projector</div>
        <HrLine />

        {players.map((player, key) => {
          if (!player) {
            return null;
          }

          return (
            <React.Fragment key={key}>
              <ListItem
                isHR={true}
                avatarStyle={{
                  width: "60px",
                  height: "60px",
                  objectFit: "contain",
                  borderRadius: "50%"
                }}
                isAvatar
                avatar={
                  batsmen[player].avatar ||
                  require("../../../images/fallbackProjection.png")
                }
                title={batsmen[player].playerName}
                rightElement={
                  <div
                    style={{
                      fontFamily: "Oswald",
                      fontWeight: 500,
                      fontSize: 30,
                      color: grey_10,
                      display: "flex",
                      alignItems: "flex-end"
                    }}
                  >
                    {score[key]}
                    <span
                      style={{
                        fontFamily: "Oswald",
                        fontWeight: 500,
                        fontSize: 14,
                        margin: "0px 0 8px 5px"
                      }}
                    >
                      ({balls[key]})
                    </span>
                  </div>
                }
              />
              <div
                style={{
                  padding: "10px 15px"
                }}
              >
                <div
                  style={{
                    borderRadius: "10px",
                    backgroundColor: "#ffe8d9"
                  }}
                >
                  <div style={styles.CriclyticsPredicts}>
                    Criclytics predicts {batsmen[player].playerName} to score{" "}
                    {/* {batsmen &&
                      batsmen[player] &&
                      maxBy(batsmen[player].listProjections, "probabilities")
                        .bound}{" "}
                    runs */}
                    {batsmen &&
                      batsmen[player] && findMax(batsmen[player].listProjections)}{" "} runs
                  </div>
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  marginTop: 20
                }}
              >
                <BarGraph
                  kind="batsman"
                  data={batsmen[player].listProjections}
                />
              </div>
              <HrLine />
            </React.Fragment>
          );
        })}
      </div>
    );
  };
  render() {
    const { data, bowlingData } = this.props;

    const batsmen = data && data.playerBattingProbabilities;
    const playerIds = [data && data.strikerId, data && data.nonStrikerId];
    const Score = [data && data.strikerRuns, data && data.nonStrikerRuns];
    const balls = [data && data.strikerBalls, data && data.nonStrikerBalls];
    return (
      <React.Fragment>
        {!this.props.isHide && (
          <Fact
            header="Live Player Projections"
            headerDescription="Criclytics takes you into the future. Look out for your favourite players."
            fact={this.props.currentMatch && this.props.currentMatch.factEngine}
            hideFactEngine
            hideHrLine
          />
        )}
        <div>
          {batsmen && this.generateBatsman(batsmen, playerIds, Score, balls)}
          {bowlingData &&
            this.generateBowlers(bowlingData.bowlingProjections, bowlingData)}
        </div>
      </React.Fragment>
    );
  }
}
const normal = "normal";
const styles = {
  heeaderText: {
    fontFamily: "mont500",
    fontSize: "12px",
    fontWeight: 500,
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: normal,
    letterSpacing: normal,
    color: "#141b2f",
    padding: "13px 16px"
  },
  CriclyticsPredicts: {
    fontFamily: "mont500",
    fontSize: "12px",
    fontWeight: normal,
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: 2,
    letterSpacing: normal,
    color: "#141b2f",
    padding: "2px 0px",
    textAlign: "center"
  }
};

export default LivePlayerPrediction;    
