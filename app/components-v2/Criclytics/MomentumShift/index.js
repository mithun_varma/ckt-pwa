import filter from "lodash/filter";
import maxBy from "lodash/maxBy";
import React, { PureComponent } from "react";
import { grey_6, white } from "../../../styleSheet/globalStyle/color";
import BallCard from "../../commonComponent/BallCard";
import { LineGraph } from "../../commonComponent/DChart";
import HrLine from "../../commonComponent/HrLine";
import OverCard from "../../commonComponent/OverCard";
import Switch from "../../commonComponent/Switch";
import Fact from "../../Fact";
import CommentaryPlayerStatus from "../../screenComponent/scoreCard/CommentaryPlayerStatus";
import ButtonBar from "../../commonComponent/ButtonBar";

export class MomentumShift extends PureComponent {
  state = {
    activeData: undefined,
    diffPoints: undefined,
    activeOver: undefined,
    showOverCard: false,
    inningNo: 1,
    currentOver: [],
    currentTeam: "",
    initial: true,
    isTooltipVisible: false,
    activeItem: "Inn 1"
  };
  componentDidMount() {
    const firstBattingTeam =
      this.props.currentMatch !== undefined &&
      this.props.currentMatch.firstBatting &&
      this.props.currentMatch.firstBatting ===
        this.props.currentMatch.teamA.teamKey
        ? this.props.currentMatch &&
          this.props.currentMatch.teamA &&
          this.props.currentMatch.teamA.shortName
        : this.props.currentMatch &&
          this.props.currentMatch.teamB &&
          this.props.currentMatch.teamB.shortName;

    const topFourDiffPoints = filter(
      this.props.data &&
        this.props.data.overHistoryDetails &&
        this.props.data.topFourDiffPoints,
      ["inningNo", 1]
    );
    const filteredTeam = filter(
      this.props.data && this.props.data.overHistoryDetails,
      ["inningNo", 1]
    );

    this.props.data &&
      this.props.data.topFourDiffPoints &&
      topFourDiffPoints.map((elem, ind) => {
        return filteredTeam.map((ele, index) => {
          if (elem.overNo === ele.overNo) {
            filteredTeam[index]["DiffPoint"] = true;
          }
        });
      });
    this.setState({
      activeData: filteredTeam,
      currentTeam: firstBattingTeam
    });

    this.props.fetchOverSeperator(
      this.props.currentMatch && this.props.currentMatch.matchId
    );
  }

  handleChange = (value, firstBattingTeam, secondBattingTeam) => {
    const topFourDiffPoints = filter(
      this.props.data.overHistoryDetails && this.props.data.topFourDiffPoints,
      ["inningNo", value === firstBattingTeam.shortName ? 1 : 2]
    );

    const data = filter(this.props.data.overHistoryDetails, [
      "inningNo",
      value === firstBattingTeam.shortName ? 1 : 2
    ]);

    topFourDiffPoints.map((elem, ind) => {
      data.map((ele, index) => {
        if (elem.overNo === ele.overNo) {
          data[index]["DiffPoint"] = true;
        }
      });
    });

    value === firstBattingTeam.shortName
      ? this.setState({
          activeData: data,
          diffPoints: filter(this.props.data.topFourDiffPoints, [
            "inningNo",
            1
          ]),
          currentTeam: firstBattingTeam.shortName,
          inningNo: 1,
          isTooltipVisible: false,
          showOverCard: false
        })
      : this.setState({
          activeData: data,
          diffPoints: filter(this.props.data.topFourDiffPoints, [
            "inningNo",
            2
          ]),
          inningNo: 2,
          currentTeam: secondBattingTeam.shortName,
          isTooltipVisible: false,
          showOverCard: false
        });
  };
  changeOverAccordingToMomentum = val => {
    this.setState(
      {
        currentOver:
          this.props.OverData &&
          this.props.OverData[this.state.inningNo - 1] &&
          this.props.OverData[this.state.inningNo - 1][
            this.props.OverData[this.state.inningNo - 1].length -
              val.payload.overNo
          ],
        showOverCard: true,
        activeOver: val.payload.overNo,
        isTooltipVisible: true
      },
      () => {
        this.props.ballByBall({
          matchId: this.props.currentMatch.matchId,
          overNo: val.payload.overNo,
          inningNo: val.payload.inningNo
        });
      }
    );
  };

  getInningNumbers = () => {
    this.props.data.overHistoryDetails;
    if (this.props.data && 
      this.props.data.overHistoryDetails && 
      this.props.data.overHistoryDetails.length > 0) {
        return (
          maxBy(this.props.data.overHistoryDetails, "inningNo").inningNo
        )
    }
  }

  handleActiveInning = item => {
    let inning = item.split(" ")[1];

    const topFourDiffPoints = this.props.data.topFourDiffPoints.filter((ele) => ele.inningNo == inning);

    const data = this.props.data.overHistoryDetails.filter((ele) => ele.inningNo == inning);
    
    topFourDiffPoints.map((elem, ind) => {
      data.map((ele, index) => {
        if (elem.overNo === ele.overNo) {
          data[index]["DiffPoint"] = true;
        }
      });
    });

    this.setState({
      activeData: data,
      diffPoints: topFourDiffPoints,
      // currentTeam: firstBattingTeam.shortName,
      inningNo: inning,
      isTooltipVisible: false,
      showOverCard: false,
      activeItem: item
    });
  };
  
  render() {
    const { currentMatch } = this.props;
    const BUTTON_ITEMS = {
      1: ["Inn 1"],
      2: ["Inn 1", "Inn 2"],
      3: ["Inn 1", "Inn 2", "Inn 3"],
      4: ["Inn 1", "Inn 2", "Inn 3", "Inn 4"]
    }
    const firstBattingTeam =
      currentMatch &&
      currentMatch.firstBatting &&
      currentMatch.firstBatting === currentMatch.teamA.teamKey
        ? currentMatch && currentMatch.teamA
        : currentMatch && currentMatch.teamB;
    const secondBattingTeam =
      currentMatch &&
      currentMatch.firstBatting &&
      currentMatch.firstBatting === currentMatch.teamA.teamKey
        ? currentMatch && currentMatch.teamB
        : currentMatch && currentMatch.teamA;

    return (
      <React.Fragment>
        {!this.props.isHide && (
          <Fact
            header="Momentum Shift"
            headerDescription="When did the match turn in favour of the team? Tap to find the game changing overs."
            fact={
              currentMatch && currentMatch.factEngine && currentMatch.factEngine
            }
          />
        )}
        <div>
          <div
            style={{
              background: white
            }}
          >
            {this.props.isHide && <HrLine />}
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                ...this.props.homeStyle
              }}
            >
              <div
                style={{
                  ...styles.headerText,
                  flex: 1,
                  alignSelf: "center",
                  padding: "0 4.9%",
                  ...this.props.headerTextStyle
                }}
              >
                Momentum Shift
              </div>
              <div
                style={{
                  padding: "8px 16px",
                  ...this.props.switchStyle
                }}
              >
                {/* <Switch
                  value1={
                    currentMatch &&
                    firstBattingTeam &&
                    firstBattingTeam.shortName
                  }
                  value2={
                    currentMatch &&
                    secondBattingTeam &&
                    secondBattingTeam.shortName
                  }
                  callback={value => {
                    this.handleChange(
                      value,
                      firstBattingTeam,
                      secondBattingTeam
                    );
                  }}
                /> */}
                {currentMatch && currentMatch.format == "TEST" ? 
                  <div style={{
                    maxWidth: 202,
                    minWidth: 104
                  }}>

                    <ButtonBar
                      items={BUTTON_ITEMS[this.getInningNumbers()]}
                      activeItem={this.state.activeItem}
                      onClick={this.handleActiveInning}
                      hideHrLine
                      />
                  </div>
                :
                  <Switch
                    value1={
                      currentMatch &&
                      firstBattingTeam &&
                      firstBattingTeam.shortName
                    }
                    value2={
                      currentMatch &&
                      secondBattingTeam &&
                      secondBattingTeam.shortName
                    }
                    callback={value => {
                      this.handleChange(
                        value,
                        firstBattingTeam,
                        secondBattingTeam
                      );
                    }}
                  />
                }
              </div>
            </div>
            <HrLine />
            {this.props.isHide && <HrLine />}
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                marginTop: "6.5%"
              }}
            >
              <LineGraph
                callBack={this.changeOverAccordingToMomentum}
                topDiffPoints={this.state.diffPoints}
                color={"#ea6650"}
                currentTeam={this.state.currentTeam}
                data={this.state.activeData}
                isTooltipVisible={this.state.isTooltipVisible}
              />
            </div>
            <HrLine />
            <center
              style={{
                display: !this.state.showOverCard ? "block" : "none",
                color: grey_6,
                padding: 8,
                fontFamily: "mont400",
                fontSize: 12
              }}
            >
              Tap highlighted overs to view turning points
            </center>
            <div
              style={{
                display: this.state.showOverCard ? "inline" : "none"
              }}
            >
              <div
                style={{
                  display: "flex",
                  padding: "8px 16px",
                  background: white
                }}
              >
                <div
                  style={{
                    flex: 0.14,
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <OverCard
                    data={{ title: this.state.activeOver, caption: "over" }}
                  />
                </div>
                <div
                  style={{
                    flex: 1,
                    display: "flex",
                    overflow: "scroll",
                    padding: "0 4.8%"
                  }}
                >
                  <BallCard balls={this.state.currentOver} />
                </div>
              </div>
              <HrLine />
              {this.props.ballByBallData && (
                <CommentaryPlayerStatus
                  isMomentum
                  teamName={this.state.currentTeam}
                  background={white}
                  playerStats={
                    this.props.ballByBallData && this.props.ballByBallData
                  }
                />
              )}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const normal = "normal";
const styles = {
  headerText: {
    fontFamily: "mont500",
    fontSize: "12px",
    fontWeight: "500",
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: normal,
    letterSpacing: normal,
    color: "#727682",
    textTransform: "uppercase"
  }
};
export default MomentumShift;
