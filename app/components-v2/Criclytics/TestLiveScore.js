import React from "react";
import { NotFound } from "../../containers/CriclyticsSlider";
import {
  grey_6,
  grey_4,
  grey_8,
  grey_10,
  orange_8,
  grey_5,
  grey_5_1,
  green_10,
  red_10,
  cardShadow,
  avatarShadow,
  pink_10,
  white
} from "../../styleSheet/globalStyle/color";
import Fact from "../Fact";
import HrLine from "../commonComponent/HrLine";
import Polling from "../commonComponent/Polling";

const getCurrentPrediction = (inningId, data) => {
  // console.log(data, "livescore");
  switch (inningId) {
    case 1:
      return [
        `${data.predictedScore}/${data.predictedWicket}`,
        `${data.secondPredictedScore}/${data.secondPredictedWicket}`,
        `${data.thirdPredictedScore}/${data.thirdPredictedWicket}`,
        `${data.fourthPredictedScore}/${data.fourthPredictedWicket}`
      ];
    case 2:
      return [
        "--",
        `${data.secondPredictedScore}/${data.secondPredictedWicket}`,
        `${data.thirdPredictedScore}/${data.thirdPredictedWicket}`,
        `${data.fourthPredictedScore}/${data.fourthPredictedWicket}`
      ];
    case 3:
      return [
        "--",
        "--",
        `${data.thirdPredictedScore}/${data.thirdPredictedWicket}`,
        `${data.fourthPredictedScore}/${data.fourthPredictedWicket}`
      ];
    case 4:
      return [
        "--",
        "--",
        "--",
        `${data.fourthPredictedScore}/${data.fourthPredictedWicket}`
      ];
  }
};

const TestLiveScore = ({ data, currentMatch }) => {
  // console.log(data, currentMatch);
  const inningsOrder =
    data.liveScores && data.liveScores.length > 0
      ? data.liveScores[data.liveScores.length - 1].inningIds
      : [];
  const { liveScores } = data;
  const lastObj =
    data.liveScores && data.liveScores.length > 0
      ? data.liveScores[data.liveScores.length - 1]
      : {};
  const teams = [
    currentMatch.currentScoreCard.battingTeam === currentMatch.teamA.teamKey
      ? currentMatch.teamA
      : currentMatch.teamB,
    currentMatch.currentScoreCard.battingTeam !== currentMatch.teamA.teamKey
      ? currentMatch.teamA
      : currentMatch.teamB
  ];
  // console.log(teams[0]);

  return data && data.liveScores && data.liveScores.length > 0 ? (
    <div style={{ padding: 16 }}>
      <div style={{ background: grey_4 }}>
        <Fact
          header="Live Score Predictor"
          headerDescription="Is it going to be a high-scoring affair or a low-scoring bummer?"
          fact={currentMatch && currentMatch.factEngine}
          hideFactEngine
          hideHrLine
        />
        {lastObj &&
          lastObj.winvizView && (
            <React.Fragment>
              <HrLine />
              <div
                style={{
                  padding: "8px 20px",
                  background: white
                }}
              >
                <Polling
                  data={{
                    teamAPercentage: lastObj.winvizView.battingTeamPercent,
                    teamBpercentage: lastObj.winvizView.bowlingTeamPercent,
                    tiePercent: lastObj.winvizView.drawPercent,
                    teamA: teams[0].shortName,
                    teamB: teams[1].shortName
                  }}
                  score={currentMatch}
                />
              </div>
            </React.Fragment>
          )}
        <HrLine />
        <div style={{ marginTop: "2%", background: white }}>
          {inningsOrder &&
            inningsOrder.length > 0 &&
            inningsOrder.map((teamId, index) => (
              <div>
                <div
                  style={{
                    display: "flex",
                    flex: 1,
                    // alignItems: "center",
                    justifyContent: "space-between",
                    padding: "14px 16px"
                  }}
                >
                  {/* 1 */}
                  <div
                    style={{
                      textAlign: "left",
                      flex: 0.2,
                      paddingTop: 5
                    }}
                  >
                    <div
                      style={{
                        fontFamily: "Oswald",
                        fontWeight: 400,
                        fontSize: 20
                      }}
                    >
                      {currentMatch &&
                      currentMatch.inningCollection[
                        `${index < 2 ? `${teamId}_1` : `${teamId}_2`}`
                      ]
                        ? `${
                            currentMatch.inningCollection[
                              `${index < 2 ? `${teamId}_1` : `${teamId}_2`}`
                            ].runs
                          }/${
                            currentMatch.inningCollection[
                              `${index < 2 ? `${teamId}_1` : `${teamId}_2`}`
                            ].wickets
                          }`
                        : "--"}
                    </div>
                    {currentMatch &&
                      currentMatch.inningCollection[
                        `${index < 2 ? `${teamId}_1` : `${teamId}_2`}`
                      ] &&
                      currentMatch.inningCollection[
                        `${index < 2 ? `${teamId}_1` : `${teamId}_2`}`
                      ].overs && (
                        <div
                          style={{
                            fontSize: 12,
                            color: grey_8,
                            minHeight: 18
                          }}
                        >
                          {`(${
                            currentMatch &&
                            currentMatch.inningCollection[
                              `${index < 2 ? `${teamId}_1` : `${teamId}_2`}`
                            ]
                              ? `${
                                  currentMatch.inningCollection[
                                    `${
                                      index < 2 ? `${teamId}_1` : `${teamId}_2`
                                    }`
                                  ].overs
                                }`
                              : ""
                          })`}
                        </div>
                      )}
                  </div>

                  {/* 2 */}
                  <div
                    style={{
                      display: "flex",
                      flex: 0.6,
                      flexDirection: "column",
                      alignItems: "center",
                      position: "relative"
                    }}
                  >
                    <div
                      style={{
                        position: "absolute",
                        display: "flex",
                        left: 0,
                        right: 0,
                        top: "26%",
                        justifyContent: "space-between"
                      }}
                    >
                      <div
                        style={{
                          width: 6,
                          height: 6,
                          borderRadius: "50%",
                          background: orange_8
                        }}
                      />
                      <div
                        style={{
                          width: "90%",
                          borderTop: `1px dashed ${grey_5}`,
                          marginTop: 3
                        }}
                      />
                      <div
                        style={{
                          width: 6,
                          height: 6,
                          borderRadius: "50%",
                          background: green_10
                        }}
                      />
                    </div>
                    <div
                      style={{
                        width: 34,
                        height: 24,
                        marginTop: 7,
                        zIndex: 1
                      }}
                    >
                      <img
                        src={
                          teams[0].teamKey === teamId
                            ? teams[0].avatar
                            : teams[1].avatar
                        }
                        alt=""
                        style={{
                          width: "100%",
                          height: "auto",
                          boxShadow: avatarShadow
                        }}
                      />
                    </div>
                    <div
                      style={{
                        fontWeight: 600,
                        fontSize: 14,
                        fontFamily: "Montserrat",
                        marginTop: 10
                      }}
                    >
                      {teams[0].teamKey === teamId
                        ? teams[0].shortName
                        : teams[1].shortName}
                    </div>
                  </div>

                  {/* 3 */}
                  <div
                    style={{
                      textAlign: "right",
                      flex: 0.2,
                      paddingTop: 5
                    }}
                  >
                    <div
                      style={{
                        fontFamily: "Oswald",
                        fontWeight: 400,
                        fontSize: 20,
                        color: red_10
                      }}
                    >
                      {
                        getCurrentPrediction(
                          liveScores[liveScores.length - 1].inningNo,
                          liveScores[liveScores.length - 1]
                        )[index]
                      }
                    </div>
                    {getCurrentPrediction(
                      liveScores[liveScores.length - 1].inningNo,
                      liveScores[liveScores.length - 1]
                    )[index] !== "--" && (
                      <div
                        style={{
                          fontSize: 12,
                          color: grey_8
                        }}
                      >
                        Runs
                      </div>
                    )}
                  </div>
                </div>
              </div>
            ))}
        </div>
        <HrLine />
        <div
          style={{
            fontFamily: "Montserrat",
            fontSize: "12px",
            fontWeight: 500,
            color: "#141b2f",
            margin: 12,
            textAlign: "center",
            background: "#fecdc8",
            padding: "4px 8px",
            borderRadius: "12px"
            // marginBottom:12
            // display: 'none'
          }}
        >
          <span
            style={{
              fontWeight: 600,
              color: grey_10
            }}
          >
            Projected Result:
          </span>
          <span
            style={{
              paddingLeft: 8,
              color: red_10
            }}
          >
            {lastObj &&
            lastObj.predictvizMarginView &&
            lastObj.predictvizMarginView.result === "tie"
              ? "Match to end in a Draw"
              : `${
                  teams[0].teamKey === lastObj.predictvizMarginView.winnerTeamId
                    ? teams[0].shortName
                    : teams[1].shortName
                } to ${lastObj &&
                  lastObj.predictvizMarginView &&
                  lastObj.predictvizMarginView.result} by ${
                  lastObj &&
                  lastObj.predictvizMarginView &&
                  lastObj.predictvizMarginView.wickets
                    ? `${lastObj.predictvizMarginView.wickets} wickets`
                    : `${lastObj.predictvizMarginView.runs} runs`
                }`}
          </span>
        </div>
        <HrLine />
      </div>
    </div>
  ) : (
    <div>
      <div
        style={{
          padding: "16px 16px"
        }}
      >
        <div
          style={{
            background: grey_4
          }}
        >
          <NotFound message="No Live score Predictor found for the match" />
        </div>
      </div>
    </div>
  );
};

export default TestLiveScore;
