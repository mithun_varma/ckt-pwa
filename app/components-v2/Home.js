import React, { Component } from "react";
import PlayerPerformance from "./commonComponent/PlayerPerformance";
import BackgroundComponent from "./commonComponent/BackgroundComponent";
import InningsGraph from "./commonComponent/InningsGraph";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeAnswer: ""
    };
  }

  selectPollOption = activeAnswer => {
    this.setState({
      activeAnswer
    });
  };

  render() {
    return (
      <div>
        <BackgroundComponent
          rootStyles={{
            height: 128
          }}
          logo
        />
        <InningsGraph />
        <PlayerPerformance />
        {/* <FanFight /> */}
        {/* <FanPoll
          data={pollQues}
          activeAnswer={this.state.activeAnswer}
          onClick={this.selectPollOption}
        /> */}
      </div>
    );
  }
}

export default Home;

const pollQues = {
  question: "Who is the best Indian T20 and ODI batsman of the year?",
  answer: ["Rohit Sharma", "Shikhar Dhawan", "Virat Kohli"]
};
