import React, { Component } from "react";
import {
  white,
  cardShadow,
} from "../../styleSheet/globalStyle/color";

export class FantasyResearchCenterTotalCard extends Component {
  constructor(props) {
    super(props);
  }

  handleSaveTeam(team){
    this.props.history.push('/fantasyTeamCode');

    this.props.saveFanFightTeam(team);
  }

  render() {

    return (
      <div
        style={{
          margin: "10px 16px",
          // padding: 10,
          background: white,
          borderRadius: 3,
          boxShadow: cardShadow
        }}
      >
        <div style={{ display: "flex", flexDirection: "row" }}>
        <div style={{flex: 0.5, padding: "16px"}}>
          <div style={{
            width: "100%",
            fontFamily: "Montserrat",
            fontSize: "12px",
            fontWeight: "500",
            fontStyle: "normal",
            fontStretch: "normal",
            lineHeight: "normal",
            letterSpacing: "0.3px",
            textAlign: "left",
            color: "#141b2f",
            }}>
          Total Projected Points
          </div>
          <div style={{
            width: "100%",
            // fontFamily: "Oswald",
            fontSize: "16px",
            fontWeight: "500",
            fontStyle: "normal",
            fontStretch: "normal",
            lineHeight: "normal",
            letterSpacing: "normal",
            textAlign: "left",
            color: "#141b2f",
            marginTop: "5%"
            }}>
            {this.props.teamMinPoints}-{this.props.teamMaxPoints}
          </div>
        </div>
        <div style={{flex: 0.5, padding: "16px"}}>
            <div style={{
              borderRadius: "3px",
              backgroundImage: "linear-gradient(to right, #ea6550, #fa9441)"
  }}>
  <div style={{
    fontFamily: "Montserrat",
  fontSize: "12px",
  fontWeight: "600",
  fontStyle: "normal",
  fontStretch: "normal",
  lineHeight: "normal",
  letterSpacing: "normal",
  textAlign: "center",
  color: "#ffffff",
  padding: "14px 0"}} onClick={()=>{this.handleSaveTeam( {
    payload: {
      crictecMatchId: "123456",
      ffPlayers: this.props.fanFightList
    }
  }
)}}>
  Save Team
  </div>
            </div>
        </div>
        </div>
      </div>
    );
  }
}

export default FantasyResearchCenterTotalCard;
