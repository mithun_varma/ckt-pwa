import React, { Component } from "react";
import { grey_8, grey_10, black } from "../../styleSheet/globalStyle/color";
import HrLine from "../commonComponent/HrLine";
import AddCircleIcon from "@material-ui/icons/AddCircleOutline";
import RemoveCircle from "@material-ui/icons/RemoveCircle";

export class FantasyResearchCenterPlayerList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: []
    };
  }

  selectPlayer = player => {
    let sr = [...this.state.selected];
    if (sr.indexOf(player.playerFeedID) == -1) {
      this.props.addSelectedPlayer(
        this.props.activePlayerCategory,
        player,
        iserror => {
          if (!iserror) {
            sr.push(player.playerFeedID);
          }
        }
      );
    }
    this.setState({
      selected: [...sr]
    });
  };

  removePlayer = player => {
    let sr = [...this.state.selected];
    if (sr.indexOf(player.playerFeedID) > -1) {
      sr.splice(sr.indexOf(player.playerFeedID), 1);
      this.props.removeSelectedPlayer(this.props.activePlayerCategory, player);
    }
    this.setState({
      selected: [...sr]
    });
  };

  render() {
    // console.log("nova", this.props.data);

    let finalData = [];

    if (this.props.activePlayerCategory === "BAT") {
      finalData = this.props.data.filter(ele => {
        return ele.playerRole === "BATSMAN";
      });
    } else if (this.props.activePlayerCategory === "BOWL") {
      finalData = this.props.data.filter(ele => {
        return ele.playerRole === "BOWLER";
      });
    } else if (this.props.activePlayerCategory === "WK") {
      finalData = this.props.data.filter(ele => {
        return ele.playerRole === "KEEPER";
      });
    } else if (this.props.activePlayerCategory === "AR") {
      finalData = this.props.data.filter(ele => {
        return ele.playerRole === "ALL_ROUNDER";
      });
    }

    return (
      <div style={{ padding: 12 }}>
        {finalData.map((item, index) => (
          <div
            key={item.id}
            style={{
              display: "flex",
              flexDirection: "column",
              fontFamily: "Montserrat",
              borderRadius: 2,
              backgroundColor: "#eeeeee",
              marginBottom: 16
            }}
          >
            <div
              style={{
                backgroundColor: "#eeeeee",
                padding: "10px",
                display: "flex",
                flex: 0.6,
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center"
              }}
              onClick={() => {
                this.state.selected.indexOf(item.playerFeedID) == -1
                  ? this.selectPlayer(item)
                  : this.removePlayer(item);
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",

                  alignItems: "center"
                }}
              >
                <div>
                  <div
                    style={{
                      width: 50,
                      height: 50,
                      marginRight: 8,
                      borderRadius: "50%",
                      background: grey_8,
                      position: "relative",
                      backgroundImage: `url(${item.playerImage ||
                        require("../../images/fallbackProjection.png")})`,
                      backgroundPosition: "top left",
                      backgroundSize: "cover",
                      backgroundRepeat: "no-repeat"
                    }}
                  />
                </div>
                <div
                  style={{
                    fontFamily: "Montserrat",
                    fontSize: 14,
                    fontWeight: "normal",
                    color: "#141b2f",
                    paddingVertical: 6,
                    display: "flex",
                    flexDirection: "column",
                    marginTop: "2px"
                  }}
                >
                  <span
                    style={{
                      color: "#000000",
                      marginBottom: 7,
                      fontFamily: "Montserrat",
                      fontSize: 14,
                      fontWeight: "500",
                      fontStyle: "normal",
                      letterSpacing: 0,
                      color: "#141b2f"
                    }}
                  >
                    {item.playerName}
                  </span>

                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center"
                    }}
                  >
                    {this.state.selected.indexOf(item.playerFeedID) == -1 ? (
                      <React.Fragment>
                        <img
                          style={{ width: 16, height: 16 }}
                          src={item.teamImg}
                        />
                        <span
                          style={{
                            fontFamily: "Montserrat",
                            fontSize: 11,
                            fontWeight: "500",
                            fontStyle: "normal",
                            letterSpacing: 0,
                            color: "#727682",
                            marginLeft: 4
                          }}
                        >
                          {item.playerClubName}
                        </span>
                      </React.Fragment>
                    ) : (
                      <div
                        style={{
                          display: "flex",
                          fontSize: 11,
                          color: "#474b5b"
                        }}
                      >
                        <p>
                          <span>Credits : </span>
                          <span
                            style={{
                              marginLeft: 4,
                              fontWeight: 600,
                              color: "#1e2437"
                            }}
                          >
                            {item.playerCredits}
                          </span>
                        </p>
                      </div>
                    )}
                  </div>
                </div>
              </div>
              {this.state.selected.indexOf(item.playerFeedID) == -1 ? (
                <AddCircleIcon style={{ fontSize: 36, color: "#d44030" }} />
              ) : (
                <RemoveCircle
                  style={{
                    fontSize: 36,
                    color: "#d44030"
                  }}
                />
              )}
            </div>
            <div
              style={{
                // minHeight:
                //   this.state.selected.indexOf(item.id) == -1
                //     ? this.props.activePlayerCategory == "AR"
                //       ? 90
                //       : 70
                //     : 0,
                // height:
                //   this.state.selected.indexOf(item.id) == -1
                //     ? this.props.activePlayerCategory == "AR"
                //       ? 90
                //       : 70
                //     : 0,
                // overflow: "hidden",
                display:
                  this.state.selected.indexOf(item.playerFeedID) == -1
                    ? "block"
                    : "none",
                transition: "all 0.5s ease"
              }}
            >
              <HrLine />
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "space-between",
                  padding: "10px 8px"
                }}
              >
                <span
                  style={{
                    height: 14,
                    fontFamily: "Montserrat",
                    fontSize: 11,
                    fontWeight: "500",
                    fontStyle: "normal",
                    letterSpacing: 0.28,
                    color: "141b2f",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  Last 3 Performances
                </span>

                {item.playerRole === "ALL_ROUNDER" ? (
                  <div>
                    <div
                      style={{
                        fontSize: 12,
                        color: grey_10,
                        fontWeight: 700,
                        // background: '#f1f1f1',
                        display: "flex",
                        flex: 1
                      }}
                    >
                      {item.recentPerformances &&
                        item.recentPerformances.map((stats, index) => (
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              paddingVertical: 16
                            }}
                          >
                            <div
                              style={{
                                fontFamily: "Montserrat",
                                flexDirection: "column"
                              }}
                              key={index}
                            >
                              <div
                                style={{
                                  flexDirection: "column"
                                }}
                              >
                                <div
                                  style={{
                                    fontSize: 12,
                                    color: "#141b2f",
                                    fontFamily: "Montserrat",
                                    fontWeight: "600"
                                  }}
                                >
                                  {stats.score === "/0" ||
                                  stats.score === "0(0)"
                                    ? "DNB"
                                    : stats.score}
                                </div>
                                <div
                                  style={{
                                    fontSize: 12,
                                    color: "#141b2f",
                                    fontFamily: "Montserrat",
                                    fontWeight: "600"
                                  }}
                                >
                                  {stats.wickets === "/0" ||
                                  stats.wickets === "0(0)"
                                    ? "DNB"
                                    : stats.wickets}
                                </div>
                              </div>
                              <div
                                style={{
                                  fontSize: 10,
                                  color: "#eeeeee",
                                  flexDirection: "column"
                                }}
                              />
                            </div>
                            {index !== 2 && (
                              <div
                                style={{
                                  background: black,
                                  opacity: 0.2,
                                  width: 1,
                                  height: "60%",
                                  margin: "0 6px"
                                }}
                              />
                            )}
                          </div>
                        ))}
                    </div>
                  </div>
                ) : (
                  <div
                    style={{
                      fontSize: 12,
                      color: grey_10,
                      fontWeight: 700,
                      display: "flex"
                    }}
                  >
                    {item.recentPerformances &&
                      item.recentPerformances.map((stats, index) => (
                        <div style={{ display: "flex", alignItems: "center" }}>
                          <div
                            style={{
                              fontFamily: "Montserrat"
                            }}
                            key={index}
                          >
                            <div
                              style={{
                                fontSize: 12,
                                color: grey_10,
                                fontWeight: 700
                              }}
                            >
                              {item.playerRole === "BOWLER"
                                ? stats.wickets === "/0" ||
                                  stats.wickets === "0(0)"
                                  ? "DNB"
                                  : stats.wickets
                                : stats.score === "/0" || stats.score === "0(0)"
                                  ? "DNB"
                                  : stats.score}
                            </div>
                          </div>
                          {index !== 2 && (
                            <div
                              style={{
                                background: black,
                                opacity: 0.2,
                                width: 1,
                                height: "60%",
                                margin: "0 6px"
                              }}
                            />
                          )}
                        </div>
                      ))}
                  </div>
                )}
              </div>
              <div
                style={{
                  display: "flex",
                  background: "#e7e6e6",
                  fontSize: 11,
                  padding: "4px 8px",
                  color: "#474b5b"
                }}
              >
                <p
                  style={{
                    padding: "4px",
                    background: "#ffe8d9",
                    borderRadius: 2
                  }}
                >
                  <span>Credits : </span>
                  <span
                    style={{ marginLeft: 4, fontWeight: 600, color: "#1e2437" }}
                  >
                    {item.playerCredits}
                  </span>
                </p>
                {/* <div
                  style={{
                    background: black,
                    opacity: 0.2,
                    width: 1,
                    margin: "0 6px"
                  }}
                /> */}
                <p
                  style={{
                    background: "#353e59",
                    color: "#FFF",
                    padding: 4,
                    marginLeft: 8,
                    borderRadius: 2
                  }}
                >
                  <span>Projected Points : </span>
                  <span
                    style={{ marginLeft: 4, fontWeight: 600, color: "#FFF" }}
                  >
                    {item.projectedPoints}
                  </span>
                </p>
              </div>
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default FantasyResearchCenterPlayerList;
