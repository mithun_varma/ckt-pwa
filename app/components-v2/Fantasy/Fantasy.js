import React, { Component } from "react";
import TopPerformerCard from "./TopPerformerCard";
import FantasyScoreBoard from "./FantasyScoreBoard";
import FantasyResearchCard from "./FantasyResearchCard";
import FantasyPlayerPerformance from "./FantasyPlayerPerformance";
import TabBar from "../commonComponent/TabBar";
import HrLine from "../commonComponent/HrLine";
import {
  gradientGrey,
  grey_8,
  cardShadow,
  white
} from "../../styleSheet/globalStyle/color";
import ScrollButtons from "../Rankings/ScrollButtons";

const TABS = ["Playing XI", "Criclytics", "Fantasy", "Articles"];
const PERFORMANCE_TYPE = [
  {
    label: "Last 4 Score",
    key: "lastPerformance"
  },
  {
    label: "Projected Points & Scores",
    key: "projectedScores"
  }
];

export class Fantasy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "Criclytics",
      activeButton: "lastPerformance"
    };
  }

  handleActiveTab = activeTab => {
    this.setState({
      activeTab
    });
  };

  handleActiveButton = activeButton => {
    this.setState({
      activeButton
    });
  };

  render() {
    return (
      <div
        style={{
          // background: "#dcdcdc",
          // minHeight: "100vh",
          padding: "16px 0"
        }}
      >
        <div
          style={{
            margin: "0 16px",
            // padding: 16,
            background: white,
            borderRadius: 3,
            fontFamily: "Montserrat",
            boxShadow: cardShadow
          }}
        >
          <TabBar
            items={TABS}
            activeItem={this.state.activeTab}
            onClick={this.handleActiveTab}
          />

          {/* Fantasy Research Center Card */}
          <FantasyResearchCard />
          {/* Fantasy Research Center Card closing */}

          {/* Player Performance Title */}
          <div>
            <HrLine />
            <div
              style={{
                background: gradientGrey,
                display: "flex",
                justifyContent: "space-between",
                fontFamily: "Montserrat",
                fontSize: 12,
                padding: "12px 16px",
                color: grey_8
              }}
            >
              <div>Probable Playing XI</div>
              <div style={{ display: "flex" }}>
                India
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                    padding: "0px 8px"
                  }}
                >
                  <img
                    src={require("../../images/arrow_up.svg")}
                    alt=""
                    style={{
                      width: 7,
                      height: 7
                    }}
                  />
                  <img
                    src={require("../../images/arrow_down.svg")}
                    alt=""
                    style={{
                      width: 7,
                      height: 7
                    }}
                  />
                </div>
              </div>
            </div>
            <HrLine />
          </div>
          {/* Player Performance Title closing */}

          {/* player Performance Scroll button */}
          <ScrollButtons
            items={PERFORMANCE_TYPE}
            // items={["PERFORMANCE_TYPE", "test"]}
            activeItem={this.state.activeButton}
            onClick={this.handleActiveButton}
          />
          {/* player Performance Scroll button closing */}

          <FantasyPlayerPerformance
            type={this.state.activeButton}
            data={fantasyPerformanceData[this.state.activeButton]}
          />
        </div>
        {/* Fantasy ScoreBoard Table section */}
        <FantasyScoreBoard title="Fantasy Scoreboard" subtitle="Final Tally" />
        {/* Fantasy ScoreBoard Table section closing */}
        {/* Top 5 Fantasy Performers card */}
        <TopPerformerCard title={"Top 5 Fantasy Performers"} />
        <TopPerformerCard title={"Top 5 Captains Picked"} />
        {/* Top 5 Fantasy Performers card closing */}
      </div>
    );
  }
}

export default Fantasy;

const fantasyPerformanceData = {
  projectedScores: [
    {
      name: "Shikar Dhawan",
      projectedScore: "70 - 80",
      projectedPoints: "13 - 18"
    },
    {
      name: "Shikar Dhawan",
      projectedScore: "70 - 80",
      projectedPoints: "13 - 18"
    },
    {
      name: "Shikar Dhawan",
      projectedScore: "70 - 80",
      projectedPoints: "13 - 18"
    }
  ],
  lastPerformance: [
    {
      name: "M S Dhoni",
      stats: [
        {
          runs: "55(35)",
          against: "AUS"
        },
        {
          runs: "55(35)",
          against: "AUS"
        },
        {
          runs: "55(35)",
          against: "AUS"
        }
      ]
    },
    {
      name: "M S Dhoni",
      stats: [
        {
          runs: "55(35)",
          against: "AUS"
        },
        {
          runs: "55(35)",
          against: "AUS"
        },
        {
          runs: "55(35)",
          against: "AUS"
        }
      ]
    },
    {
      name: "M S Dhoni",
      stats: [
        {
          runs: "55(35)",
          against: "AUS"
        },
        {
          runs: "55(35)",
          against: "AUS"
        },
        {
          runs: "55(35)",
          against: "AUS"
        }
      ]
    }
  ]
};
