import React, { Component } from "react";
import TopPerformerCard from "./TopPerformerCard";
import { grey_8, gradientGrey } from "../../styleSheet/globalStyle/color";
import HrLine from "../commonComponent/HrLine";
import KeyboardArrowDown from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUp from "@material-ui/icons/KeyboardArrowUp";
import Reveal from "react-reveal/Reveal";
import EmptyState from "../commonComponent/EmptyState";

const AllPerformerCard = props => {
  // console.log(props);
  return (
    <div style={{ fontFamily: "Montserrat" }}>
      <div
        style={{
          display: "flex",
          // fontFamily: "Montserrat",
          fontWeight: 400,
          color: grey_8,
          fontSize: 10,
          justifyContent: "space-between",
          background: gradientGrey,
          // background: "grey",
          padding: "12px 10px",
          alignItems: "center",
          textAlign: "center",
          textTransform: "uppercase"
        }}
      >
        <div style={{ flex: 0.1 }}>Rank</div>
        <div style={{ flex: 0.4 }}>Players</div>
        <div
          style={{
            flex: 0.3,
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
          }}
          onClick={() => props.sortPlayer("selection")}
        >
          Selected by{" "}
          {props.selectionSort ? (
            <KeyboardArrowUp style={{ fontSize: 18 }} />
          ) : (
            <KeyboardArrowDown style={{ fontSize: 18 }} />
          )}
        </div>
        <div
          style={{
            flex: 0.2,
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
          }}
          onClick={() => props.sortPlayer("points")}
        >
          points
          {props.pointSort ? (
            <KeyboardArrowUp style={{ fontSize: 18 }} />
          ) : (
            <KeyboardArrowDown style={{ fontSize: 18 }} />
          )}
        </div>
      </div>
      <HrLine />
      {props.players &&
        props.players.map((player, index) => (
          <Reveal effect={"fadeIn"} duration={1000}>
            <div key={player.rank}>
              <div
                style={{
                  display: "flex",
                  fontFamily: "Montserrat",
                  fontWeight: 400,
                  // color: grey_8,
                  fontSize: 12,
                  justifyContent: "space-between",
                  // background: gradientGrey,
                  // background: "grey",
                  padding: "12px 10px",
                  alignItems: "center",
                  textAlign: "center"
                  // textTransform: "uppercase"
                }}
              >
                <div style={{ flex: 0.1 }}>{player.rank}</div>
                <div style={{ flex: 0.4 }}>{player.playerName}</div>
                <div style={{ flex: 0.3 }}>{`${
                  player.playerSelectionPercentage
                }%`}</div>
                <div style={{ flex: 0.2 }}>{player.playerMatchPoints}</div>
              </div>
              <HrLine />
            </div>
          </Reveal>
        ))}
      <div
        style={{
          padding: 16,
          color: "#d44030",
          textAlign: "center",
          fontSize: 12,
          fontWeight: 600
        }}
        onClick={props.toggleTopPlayers}
      >
        Back to Top 5 fantasy performers
      </div>
    </div>
  );
};

export class Running extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isTop5: true,
      isSort: false,
      selectionSort: false,
      pointSort: false,
      topPerformers: [],
      top5Performers: []
    };
  }

  handleMatchSelect = val => {
    this.setState({ activePill: val });
  };

  handleActiveButton = activeButton => {
    this.setState({
      activeButton
    });
  };

  static getDerivedStateFromProps = (nextProps, state) => {
    // console.log(nextProps.scoreDetails.fanFightList);
    if (nextProps.scoreDetails.fanFightList && !state.isSort) {
      const { fanFightList } = nextProps.scoreDetails;
      const playerArray = [
        ...fanFightList.matchSquad[0].playersArray.map(pl => ({
          teamImg: fanFightList.matchTeamHomeFlag,
          ...pl
        })),
        ...fanFightList.matchSquad[1].playersArray.map(pl => ({
          teamImg: fanFightList.matchTeamAwayFlag,
          ...pl
        }))
      ];
      const topPerformers =
        playerArray &&
        playerArray.length > 0 &&
        playerArray
          .sort((a, b) => b.playerMatchPoints - a.playerMatchPoints)
          .map((pl, idx) => ({
            rank: idx < 9 ? `0${idx + 1}` : idx + 1,
            ...pl
          }));
      const top5Performers = topPerformers.filter(pl => +pl.rank < 6);
      return {
        ...state,
        isSort: true,
        topPerformers,
        top5Performers
      };
    }
  };

  sortPlayer = type => {
    // console.log(type);
    const { topPerformers } = this.state;
    if (type === "selection") {
      if (this.state.selectionSort) {
        const sortedPlayer =
          topPerformers &&
          topPerformers.length > 0 &&
          topPerformers.sort(
            (a, b) => b.playerSelectionPercentage - a.playerSelectionPercentage
          );
        this.setState({
          topPerformers: sortedPlayer,
          selectionSort: false
        });
      }
      if (!this.state.selectionSort) {
        const sortedPlayer =
          topPerformers &&
          topPerformers.length > 0 &&
          topPerformers.sort(
            (a, b) => a.playerSelectionPercentage - b.playerSelectionPercentage
          );
        this.setState({
          topPerformers: sortedPlayer,
          selectionSort: true
        });
      }
    }
    if (type === "points") {
      if (this.state.pointSort) {
        const sortedPlayer =
          topPerformers &&
          topPerformers.length > 0 &&
          topPerformers.sort((a, b) => b.playerMatchPoints - a.playerMatchPoints);
        this.setState({
          topPerformers: sortedPlayer,
          pointSort: false
        });
      }
      if (!this.state.pointSort) {
        const sortedPlayer =
          topPerformers &&
          topPerformers.length > 0 &&
          topPerformers.sort((a, b) => a.playerMatchPoints - b.playerMatchPoints);
        this.setState({
          topPerformers: sortedPlayer,
          pointSort: true
        });
      }
    }
  };

  toggleCreateTeam = value => {
    this.setState({
      createTeam: !value
    });
  };

  toggleTopPlayers = () => {
    const { isTop5 } = this.state;
    this.setState({
      isTop5: !isTop5
    });
  };

  render() {
    const { fanFightList } = this.props.scoreDetails;
    if (!fanFightList)
      return (
        <div
          style={{
            fontFamily: "Montserrat",
            textAlign: "center",
            minHeight: "40vh"
          }}
        >
          <EmptyState msg={"no fantasy data available for this match"} />
        </div>
      );
    const { topPerformers, top5Performers } = this.state;
    return (
      <React.Fragment>
        <div
          style={{
            minHeight: "40vh"
          }}
        >
          {this.state.isTop5 ? (
            <TopPerformerCard
              title={"Top 5 Fantasy Performers"}
              // {...this.props}
              players={top5Performers}
              toggleTopPlayers={this.toggleTopPlayers}
            />
          ) : (
            <AllPerformerCard
              {...this.props}
              players={topPerformers}
              {...this.state}
              sortPlayer={this.sortPlayer}
              toggleTopPlayers={this.toggleTopPlayers}
            />
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default Running;
