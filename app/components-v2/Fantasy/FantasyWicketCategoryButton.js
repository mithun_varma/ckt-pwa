import React, { Component } from 'react';

export class FantasyWicketCategoryButton extends Component {

  render() {
    return (
      <div>
        <div style={{
          width: "46px",
          height: "106px",
          objectFit: "contain",
          borderRadius: "3px",
          border: "solid 0.5px #dbdbdb"
        }}
        >

          <div style={{
            width: "98%",
            height: "30%",
            marginTop: "1%",
            objectFit: "contain",
            borderRadius: "3px 3px 0 0",
            background: "#eeeeee",
            textAlign: "center"
          }}>
            <img
              src={wk_icon}
              alt=""
              style={{
                width: "70%",
                marginTop: "10%"
              }}
            />

          </div>

          <div style={{
            width: "100%",
            marginTop: "45%",
            fontFamily: "Oswald",
            fontSize: "21px",
            fontWeight: "500",
            lineHeight: "0.81",
            letterSpacing: "0.5px",
            textAlign: "center",
            color: "#a1a4ac"

          }}>
            1
          </div>
          <div style={{
            width: "100%",
            marginTop: "20%",
            fontFamily: "Montserrat",
            fontSize: "11px",
            fontWeight: "500",
            lineHeight: "1.55",
            letterSpacing: "0.3px",
            textAlign: "center",
            color: "#a1a4ac"
          }}>
            WK
          </div>
        </div>
      </div>
    )
  }
}

export default FantasyWicketCategoryButton;