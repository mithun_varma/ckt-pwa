import React, { Component } from "react";
import ScoreDetailsHeader from "../../components-v2/screenComponent/scoreCard/ScoreDetailsHeader";
import {
  cardShadow,
  white
} from "../../styleSheet/globalStyle/color";


export default class FantasyPlayers extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <div
        style={{
          // background: "#dcdcdc",
          // minHeight: "100vh",
          padding: "16px 0"
        }}
      >

          {/* Header */}
          <ScoreDetailsHeader
          history={history}
          leftIconOnClick={() => {
            history.goBack();
          }}
          isHeaderBackground={false}
      
        />

      <div style={{
        fontSize: 22,
        color: "#141b2f",
        fontWeight: "bold",
        padding: 16
      }}>
        Team Code
      </div>
        <div
          style={{
            margin: "0 16px",
            // padding: 16,
            background: white,
            borderRadius: 3,
            fontFamily: "Montserrat",
            boxShadow: cardShadow
          }}
        >
        <div style={{
          borderRadius: "3px",
          backgroundColor: "#141b2f",
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          color: white,
          padding: 16
        }}>
            <img></img> FANFIGHT
        </div>
        <div style={{
          padding: 16,
          paddingBottom: 0
        }}>
          <ul style={{
            padding: 16,
            lineHeight: 1.75,
            color: "#6f768a"
          }}>
            <li style={{
              marginBottom: 16,
              listStyle: "disc",
            }}>
            To activate your fanfight fantasy team copy your unique team code.
            </li>
            <li style={{
              marginBottom: 16,
              listStyle: "disc",
            }}>
            Open fanfight on your browser/App and apply team code for the respective match.
            </li>
          </ul>
        </div>

        <div style={{
          padding: 16,
          paddingBottom: 0,
          display: "flex",
          flexDirection: "column",
          justifyContent: "center"
        }}>
          <div style={{
            flex: 1,
            fontSize: 15,
            color: "#141b2f",
            textAlign: "center"
          }}>
            VOUCHER CODE
          </div>
          <div style={{
            flex: 1,
            display: "flex",
            justifyContent: "center",
            margin: 16
          }}>
            <div style={{
              fontSize: 24,
              color: "#ff6700",
              padding: 16,
              borderRadius: "2px",
              border: "dashed 1px #444444",
            }}>
              67XDQNLAX328
            </div>
          </div>
        </div>
        <div style={{
          padding: 16
        }}>
          <div style={{
            textAlign: "center",
            borderRadius: "2px",
            backgroundImage: "linear-gradient(to left, #d44030, #9b000d)",
            color: white,
            padding: 16
          }}>
            Copy Code
          </div>
        </div>
        
        </div>
        
      </div>
    );
  }
}