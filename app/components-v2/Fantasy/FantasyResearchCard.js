import React, { Component } from "react";
import { gradientBlack, white } from "../../styleSheet/globalStyle/color";
import CardGradientTitle from "../commonComponent/CardGradientTitle";
export class FantasyResearchCard extends Component {
  render() {
    return (
      <div>
        <CardGradientTitle title="Fantasy Research Center" />
        {/* Black Card Question */}
        {/* <div
              style={{
                background: gradientBlack,
                borderRadius: 3,
                position: "relative",
                padding: 10,
                color: white,
                fontFamily: "Montserrat",
                letterSpacing: 0.3,
                marginTop: "12px",
                lineHeight: "19px"
              }}
            >
              <img
                src={require("../../images/medal.png")}
                alt=""
                style={{
                  width: 18,
                  height: 26,
                  top: -2,
                  right: 10,
                  position: "absolute"
                }}
              />
              <div
                style={{
                  // fontSize: 12,
                  // fontWeight: 700,
                  // paddingBottom: 10,
                  // borderBottom: "1px solid #777777"

                    width: '177px',
                    height: '34px',
                    fontFamily: 'Montserrat',
                    fontSize: '12px',
                    fontWeight: '600',
                    fontStyle: 'normal',
                    fontStretch: 'normal',
                    lineHeight: '1.42',
                    letterSpacing: '0.3px',
                    color: '#ffffff'
                }}
              >
                Do you know how Fantasy cricket works ?
              </div>
              <div
                style={{
                  fontSize: 10,
                  paddingTop: 10
                }}
              >
                Create a virtual team of real cricket players and score points depending on how your chosen players perform in real live matches
              </div>
            </div> */}
        {/* Black Card Question closing */}
      </div>
    );
  }
}

export default FantasyResearchCard;
