import React, { Component } from 'react'
import {
  gradientBlack, white,
} from "../../styleSheet/globalStyle/color";
export class FantasyResearchCenterBoard extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   activePlayerCategory: this.props.activePlayerCategory
    // };
  }

  render() {
    return (
      <div style={{ padding: "16px", paddingTop: 0 }}>

        <div
          style={{
            background: gradientBlack,
            borderRadius: 3,
            position: "relative",
            padding: 10,
            color: white,
            fontFamily: "Montserrat",
            letterSpacing: 0.3,
            marginTop: "12px",
            lineHeight: "19px"
          }}
        >

          <div>
            <div
              style={{
                display: "flex",
                fontFamily: "Montserrat",
                fontSize: 12,
                fontWeight: 600,
                color: white,
                alignItems: "center",
                padding: "14px 10px",
                borderBottom: '1px solid #777777'
              }}
            >
            <div style={{ flex: 0.1 }}>
            <img
                  src={require("../../images/profile_icon.png")}
                  alt=""
                  style={{
                    width: "80%",
                    display: "flex"
                  }}
                />
            </div>
              <div style={{ flex: 0.6, fontWeight: "bold" }}>
                Players
              </div>
              <div
                style={{
                  flex: 0.3,
                  display: "flex",
                  justifyContent: "right",
                  textAlign: "right",
                  color: white
                }}
              >
                <div style={{
                  width: "100%",
                  textAlign: "right"
                }}>{this.props.totalNumOfSelectedPlayers} / {this.props.matchConfig.maxPlayers}</div>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                fontFamily: "Montserrat",
                fontSize: 12,
                fontWeight: 600,
                color: white,
                alignItems: "center",
                padding: "14px 10px"
                // borderBottom: '1px solid #777777'
              }}
            >
            <div style={{ flex: 0.1 }}>
            <img
                  src={require("../../images/star_orange.png")}
                  alt=""
                  style={{
                    width: "80%",
                    display: "flex"
                  }}
                />
            </div>
              <div style={{ flex: 0.6, fontWeight: "bold" }}>
                Credits Left
              </div>
              <div
                style={{
                  flex: 0.3,
                  display: "flex",
                  justifyContent: "right",
                  textAlign: "right",
                  color: white
                }}
              >
                <div style={{
                  width: "100%",
                  textAlign: "right"
                }}>{this.props.totalCreditsConsumed} / {this.props.matchConfig.maxCredits}</div>
              </div>
            </div>
          </div>
        </div>



      </div>
    )
  }
}

export default FantasyResearchCenterBoard;
