import React, { Component } from "react";
import FantasyResearchCard from "./FantasyResearchCard";
import FantasyResearchCenter from "../../components-v2/Fantasy/FantasyResearchCenter";
// import FantasyResearchCenterTotalCard from "../../components-v2/Fantasy/FantasyResearchCenterTotalCard";
import { white } from "../../styleSheet/globalStyle/color";
import HrLine from "../commonComponent/HrLine";
import ErrorOutline from "@material-ui/icons/ErrorOutline";
import Reveal from "react-reveal/Reveal";
import isEmpty from "lodash/isEmpty";
import playerIco from "../../images/players.svg";
import creditIco from "../../images/credits.svg";

const PointView = propsData => {
  // console.log(data);
  const { data } = propsData;
  return (
    <div
      style={{
        // position: "sticky",
        position: "fixed",
        left: 0,
        right: 0,
        width: "100vw",
        // marginLeft: -16,
        background: "#fff",
        bottom: 0,
        // marginBottom: -16,
        fontFamily: "Montserrat",
        fontSize: 12,
        fontWeight: 500,
        boxShadow: "0px -2px 4px #c2c2c2"
      }}
    >
      {data.isError && (
        <Reveal effect={"fadeIn"} duration={1000}>
          <p
            style={{
              display: "flex",
              padding: 12,
              color: "#fff",
              background: "#f5a023",
              alignItems: "center"
            }}
          >
            <ErrorOutline />
            <span style={{ marginLeft: 8 }}>{data.error}</span>
          </p>
        </Reveal>
      )}
      <p
        style={{
          display: "flex",
          padding: 16,
          justifyContent: "space-between",
          alignItems: "center"
        }}
      >
        <span style={{ display: "flex", alignItems: "center" }}>
          <img src={playerIco} /> <span style={{ marginLeft: 4 }}>Players</span>
        </span>
        <span
          style={{
            fontSize: 14,
            fontFamily: "Oswald"
          }}
        >
          {data.totalNumOfSelectedPlayers}/11
        </span>
      </p>
      <HrLine />
      <p
        style={{
          display: "flex",
          padding: 16,
          justifyContent: "space-between",
          alignItems: "center"
        }}
      >
        <span style={{ display: "flex", alignItems: "center" }}>
          <img src={creditIco} />
          <span style={{ marginLeft: 4 }}>Credits Left</span>
        </span>
        <span
          style={{
            fontSize: 14,
            fontFamily: "Oswald"
          }}
        >
          {100 - parseFloat(data.totalCreditsConsumed)}/100
        </span>
      </p>
      <div
        style={{
          padding: 12,
          fontSize: 12,
          textAlign: "center",
          fontWeight: 600,
          color: data.passed ? "#fff" : "#838383",
          background: data.passed
            ? "linear-gradient(to right,#9b000d, #d44030)"
            : "#dddddd"
        }}
      >
        {data.passed ? (
          <a
            style={{
              color: "inherit",
              display: "flex",
              width: "100%",
              justifyContent: "center"
            }}
            target="_blank"
            // https://fanfight.com/cricket/createteam/?match-id=43669&players=123,123,123,123,123,123
            href={`https://fanfight.com/cricket/createteam/?match-id=${
              data.matchId
            }&players=${data.playerIds.toString()}`}
          >
            Add team to FanFight
          </a>
        ) : (
          "Add team to FanFight"
        )}
      </div>
    </div>
  );
};

export class FantasyTeamCreation extends Component {
  constructor(props) {
    super(props);
    this.fullNames = {
      WK: "Wicket-keeper",
      AR: "All-rounder",
      BAT: "Batsman",
      BOWL: "Bowler"
    };
    this.state = {
      matchId: "",
      playerIds: [],
      matchConfig: {
        maxCredits: 100.0,
        maxPlayers: 11,
        WK_max: 1,
        WK_min: 1,
        AR_max: 3,
        AR_min: 1,
        BOWL_max: 5,
        BOWL_min: 3,
        BAT_max: 5,
        BAT_min: 3,
        playerCategories
      },
      teamPlayerCount: {},
      error: "",
      isError: false,
      activePlayerCategory: "BAT",
      totalNumOfSelectedPlayers: 0,
      totalCreditsConsumed: 0.0,
      teamMinPoints: 0,
      teamMaxPoints: 0,
      selectedPlayers: {
        WK: [],
        AR: [],
        BOWL: [],
        BAT: []
      },
      passed: false
    };
  }

  toFunc = () =>
    setTimeout(() => {
      this.setState({
        isError: false,
        error: ""
      });
    }, 3000);

  static getDerivedStateFromProps = (nextProps, state) => {
    // console.log(nextProps);
    if (nextProps.teams.length > 0 && isEmpty(state.teamPlayerCount)) {
      const teamCnt = {};
      teamCnt[nextProps.teams[0]] = 0;
      teamCnt[nextProps.teams[1]] = 0;
      return {
        ...state,
        matchId: nextProps.fanFightList.matchFeedID,
        teamPlayerCount: { ...teamCnt }
      };
    }
    return null;
  };

  minCountCheck = player => {
    const { selectedPlayers, teamPlayerCount } = this.state;
    if (+teamPlayerCount[`${player.playerClubName}`] === 7) {
      this.setState({
        isError: true,
        error: "Maximum 7 players from each team allowed"
      });
      this.toFunc();
      return false;
    }
    if (
      parseFloat(this.state.totalCreditsConsumed) +
        parseFloat(player.playerCredits) >
      parseFloat(this.state.matchConfig.maxCredits)
    ) {
      this.setState({
        isError: true,
        error: "Total credits cannot exceed 100"
      });
      this.toFunc();
      return false;
    }
    if (
      selectedPlayers["BAT"].length + selectedPlayers["AR"].length === 7 &&
      !(player.playerRole === "KEEPER")
    ) {
      if (
        selectedPlayers["BOWL"].length < 3 &&
        !(player.playerRole === "BOWLER")
      ) {
        this.setState({
          isError: true,
          error: "Please select at least 3 Bowlers",
          activePlayerCategory: "BOWL"
        });
        this.toFunc();
        return false;
      }
    }
    if (
      selectedPlayers["BOWL"].length + selectedPlayers["AR"].length === 7 &&
      !(player.playerRole === "KEEPER")
    ) {
      if (
        selectedPlayers["BAT"].length < 3 &&
        !(player.playerRole === "BATSMAN")
      ) {
        this.setState({
          isError: true,
          error: "Please select at least 3 Batsmen",
          activePlayerCategory: "BAT"
        });
        this.toFunc();
        return false;
      }
    }
    if (
      selectedPlayers["BOWL"].length + selectedPlayers["BAT"].length === 9 &&
      selectedPlayers["BAT"].length >= 3 &&
      selectedPlayers["BOWL"].length >= 3 &&
      !(player.playerRole === "KEEPER")
    ) {
      if (
        selectedPlayers["AR"].length < 1 &&
        !(player.playerRole === "ALL_ROUNDER")
      ) {
        this.setState({
          isError: true,
          error: "Please select at least 1 All Rounder",
          activePlayerCategory: "AR"
        });
        this.toFunc();
        return false;
      }
    }
    if (
      selectedPlayers["BOWL"].length +
        selectedPlayers["BAT"].length +
        selectedPlayers["AR"].length ===
        10 &&
      selectedPlayers["BAT"].length >= 3 &&
      selectedPlayers["BOWL"].length >= 3 &&
      selectedPlayers["AR"].length > 0
    ) {
      if (
        selectedPlayers["WK"].length < 1 &&
        !(player.playerRole === "KEEPER")
      ) {
        this.setState({
          isError: true,
          error: "Please select at least 1 Wicket keeper",
          activePlayerCategory: "WK"
        });
        this.toFunc();
        return false;
      }
    }
    return true;
  };

  addSelectedPlayer = (playerCategory, player, cb) => {
    // console.log(player);
    if (
      this.state.matchConfig.maxPlayers >=
      this.state.totalNumOfSelectedPlayers + 1
    ) {
      let { selectedPlayers, teamPlayerCount, playerIds } = this.state;
      // console.log(selectedPlayers);
      if (
        selectedPlayers[playerCategory].length + 1 >
        this.state.matchConfig[`${playerCategory}_max`]
      ) {
        cb(true);
        this.setState({
          isError: true,
          error: `select maximum ${selectedPlayers[playerCategory].length} ${
            this.fullNames[playerCategory]
          }${playerCategory !== "WK" ? "'s" : ""} only`
        });
        this.errorTimeout = setTimeout(() => {
          this.setState({
            isError: false,
            error: ""
          });
        }, 3000);
      } else {
        if (this.minCountCheck(player)) {
          cb(false);
          selectedPlayers[playerCategory].push(player);
          playerIds.push(player.playerFeedID);
          teamPlayerCount[`${player.playerClubName}`] += 1;
          this.setState({
            selectedPlayers,
            teamPlayerCount,
            playerIds
          });
          this.calculateCreditsAndPoints();
        }
      }
    } else {
      this.setState({
        isError: true,
        error: `select 11 players only`
      });
      this.errorTimeout = setTimeout(() => {
        this.setState({
          isError: false,
          error: ""
        });
      }, 3000);
    }
  };

  removeSelectedPlayer = (playerCategory, player) => {
    let selectedPlayerList = this.state.selectedPlayers;
    let teamPlayerCount = this.state.teamPlayerCount;
    let newplayerIds = this.state.playerIds;
    // let playerIndex = selectedPlayerList[playerCategory].indexOf(player);
    selectedPlayerList[playerCategory] = selectedPlayerList[
      playerCategory
    ].filter(p => {
      return p.playerFeedID !== player.playerFeedID;
    });
    let playerIds = newplayerIds.filter(id => id != player.playerFeedID);
    teamPlayerCount[`${player.playerClubName}`] -= 1;
    // selectedPlayerList[playerCategory] = selectedPlayerList[playerCategory].slice(0, playerIndex).concat(selectedPlayerList[playerCategory].slice(playerIndex + 1, selectedPlayerList[playerCategory].length))
    this.setState({
      selectedPlayers: selectedPlayerList,
      teamPlayerCount,
      playerIds
    });
    this.calculateCreditsAndPoints();
  };

  isPlayerAlreadySelected = (playerCategory, player) => {
    return this.state.selectedPlayers[playerCategory].find(playerItem => {
      return playerItem.id === player.id;
    });
  };

  handleActivePlayerCategory = activePlayerCategory => {
    this.setState({
      activePlayerCategory
    });
  };

  componentDidMount = () => {};

  calculateCreditsAndPoints = () => {
    let totalPlayers = 0;
    let totalCredits = 0.0;
    let teamMinPoints = 0;
    let teamMaxPoints = 0;

    for (var selectedPlayerCategoryType in this.state.selectedPlayers) {
      totalPlayers += this.state.selectedPlayers[selectedPlayerCategoryType]
        .length;
      this.state.selectedPlayers[selectedPlayerCategoryType].map(player => {
        totalCredits += parseFloat(player.playerCredits);
        teamMinPoints += player.minPoints;
        teamMaxPoints += player.maxPoints;
      });
    }
    this.setState({
      totalNumOfSelectedPlayers: totalPlayers,
      passed: +totalPlayers === 11,
      totalCreditsConsumed: totalCredits,
      teamMinPoints,
      teamMaxPoints
    });
  };

  render() {
    return (
      <div>
        <div
          style={{
            background: white,
            marginBottom: +this.state.totalCreditsConsumed > 0 ? 160 : 10
          }}
        >
          <FantasyResearchCard />
          <FantasyResearchCenter
            title="Fantasy Research Center"
            matchConfig={this.state.matchConfig}
            isError={this.state.isError}
            activePlayerCategory={this.state.activePlayerCategory}
            totalNumOfSelectedPlayers={this.state.totalNumOfSelectedPlayers}
            totalCreditsConsumed={this.state.totalCreditsConsumed}
            selectedPlayers={this.state.selectedPlayers}
            addSelectedPlayer={this.addSelectedPlayer}
            removeSelectedPlayer={this.removeSelectedPlayer}
            handleActivePlayerCategory={this.handleActivePlayerCategory}
            isPlayerAlreadySelected={this.isPlayerAlreadySelected}
            data={this.props.data}
            handleback={this.props.handleBack}
          />
        </div>
        {+this.state.totalCreditsConsumed > 0 && (
          <PointView data={this.state} />
        )}
      </div>
    );
  }
}

export default FantasyTeamCreation;

const playerCategories = ["WK", "BAT", "AR", "BOWL"];
