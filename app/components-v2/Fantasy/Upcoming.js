import React, { Component } from "react";
import FantasyResearchCard from "./FantasyResearchCard";
// import ReactMomentCountDown from 'react-moment-countdown';

import moment from "moment";
import Countdown from 'react-countdown-moment';
import HrLine from "../commonComponent/HrLine";
import {
  gradientGrey,
  grey_8,
  white
} from "../../styleSheet/globalStyle/color";
import PillTabs from "../../components/Common/PillTabs";

export class Upcoming extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeButton: "lastPerformance",
      activePill: "",
      createTeam: true
    };

  }

  handleMatchSelect = val => {
    this.setState({ activePill: val });
  };

  handleActiveButton = activeButton => {
    this.setState({
      activeButton
    });
  };

  handleSelectTeam(playerList){
    this.props.his
  }
  componentDidMount = () => {
    TEAMS = this.props.scoreDetails.teams
   
    
    if(this.state.activePill.length === 0){
      this.setState({ activePill: TEAMS[0] });
    }

  };

  toggleCreateTeam = (value) => {
    this.setState({
      createTeam: !value
    });
  }

  render() {

    let projectedScores = this.props.scoreDetails.fanFightList.filter(p=>{
      return p.teamId === this.state.activePill
    }).map(p=> {
        let obj ={};
        obj.name = p.shortName;
        obj.projectedScore = `${p.minRuns} - ${p.maxRuns}`;
        obj.projectedPoints =`${p.minPoints} - ${p.maxPoins}`;;
        return obj
    });

    
    let lastPerformance = this.props.scoreDetails.fanFightList && this.props.scoreDetails.fanFightList.filter(p=>{
      return p.teamId === this.state.activePill
    }).map(p=> {
      let obj ={};
      obj.name = p.shortName;
      
      if(p.playerRole === 'Batsman'){
        
        obj.stats = p.inningsHistory.map(s =>{
          let o ={};
          o.runs = s.score;
          o.against = s.opposition;
          return o;
        })
      }
      
      else if(p.playerRole === 'Bowler'){
        obj.stats = p.inningsHistory.map(s =>{
          let o ={};
          o.runs = s.wickets;
          o.against = s.opposition;
          return o;
        })
        
      }
      
      else if(p.playerRole === 'Keeper'){
        obj.stats = p.inningsHistory.map(s =>{
          let o ={};
          o.runs = s.score;
          o.against = s.opposition;
          return o;
        })
        
      }
      else if(p.playerRole === 'AllRounder'){
        
        obj.stats = p.inningsHistory.map(s =>{
          let o ={};
          o.runs = s.score;
          o.against = s.opposition;
          return o;
        })
      } 

      return obj
    });



const fantasyPerformanceData = {
};




fantasyPerformanceData.projectedScores = projectedScores;
fantasyPerformanceData.lastPerformance = lastPerformance;

    return (
      <React.Fragment>
      <div
        style={{
          // background: "#dcdcdc",
          // minHeight: "100vh",
          // padding: "16px 0"
        }}
      >

      <div
          style={{
            // margin: "0 16px",
            // padding: 16,
            background: white,
            // borderRadius: 3,
            fontFamily: "Montserrat",
            // boxShadow: cardShadow
          }}
        >

          {/* Fantasy Research Center Card */}
          <FantasyResearchCard />
          {/* Fantasy Research Center Card closing */}

          {/* Player Performance Title */}
          <div>
            <HrLine />
            <div
              style={{
                background: gradientGrey,
                display: "flex",
                justifyContent: "space-between",
                fontFamily: "Montserrat",
                fontSize: 12,
                padding: "12px 16px",
                color: grey_8
              }}
            >
              <div style={{ 
                flex: "0.5",
                display: "flex",
                flexDirection: "column",
                justifyContent: "center"
                }}>
                <div style={{

                  width: '38px',
                  height: '13px',
                  fontFamily: 'Montserrat',
                  fontSize: '10px',
                  fontWeight: 'normal',
                  fontStyle: 'normal',
                  fontStretch: 'normal',
                  lineHeight: 'normal',
                  letterSpacing: '0.5px',
                  color: '#727682'

                }}>
                TEAMS
                </div>
              </div>
              <div style={{ flex: "0.5" }}>
              <PillTabs
                items={TEAMS}
                activeItem={this.state.activePill}
                onTabSelect={this.handleMatchSelect}
              />
              </div>
            </div>
            <HrLine />
          </div>

          <div style={{}}>
              <p style={{ fontFamily: "Montserrat", fontSize : '12px' , padding: '20px' ,  textAlign: 'center'}}>
                 Watch this space when the match starts in <br></br> 
                 <p style={{ fontFamily: "Montserrat", fontSize : '16px' , textAlign: 'center' , color: '#3bc306'}}>

                 <Countdown endDate={moment(this.props.scoreDetails.scoreCards[this.props.match.params.matchId].startDate)} />

                 </p>
              </p>
             
          </div>
        
        
        </div>
        
       
      </div>

</React.Fragment>
    );
  }
}

export default Upcoming;

// const fantasyPerformanceData1 = {
//   projectedScores: [
//     {
//       name: "Shikar Dhawan",
//       projectedScore: "70 - 80",
//       projectedPoints: "13 - 18"
//     },
//     {
//       name: "Shikar Dhawan",
//       projectedScore: "70 - 80",
//       projectedPoints: "13 - 18"
//     },
//     {
//       name: "Shikar Dhawan",
//       projectedScore: "70 - 80",
//       projectedPoints: "13 - 18"
//     }
//   ],
//   lastPerformance: [
//     {
//       name: "M S Dhoni",
//       stats: [
//         {
//           runs: "55(35)",
//           against: "AUS"
//         },
//         {
//           runs: "55(35)",
//           against: "AUS"
//         },
//         {
//           runs: "55(35)",
//           against: "AUS"
//         }
//       ]
//     },
//     {
//       name: "M S Dhoni",
//       stats: [
//         {
//           runs: "55(35)",
//           against: "AUS"
//         },
//         {
//           runs: "55(35)",
//           against: "AUS"
//         },
//         {
//           runs: "55(35)",
//           against: "AUS"
//         }
//       ]
//     },
//     {
//       name: "M S Dhoni",
//       stats: [
//         {
//           runs: "55(35)",
//           against: "AUS"
//         },
//         {
//           runs: "55(35)",
//           against: "AUS"
//         },
//         {
//           runs: "55(35)",
//           against: "AUS"
//         }
//       ]
//     }
//   ]
// };
