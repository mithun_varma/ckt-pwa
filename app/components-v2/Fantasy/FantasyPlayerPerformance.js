import React, { Component } from "react";
import {
  gradientBlack,
  grey_8,
  grey_2,
  grey_10,
  white,
  grey_1,
  black,
  grey_6
} from "../../styleSheet/globalStyle/color";
import HrLine from "../commonComponent/HrLine";

import batIcon from "../../images/batS.svg";

export class FantasyPlayerPerformance extends Component {
  render() {
    return (
      <div style={{ padding: "0 16px 10px" }}>
        {/* Scroll button will go here */}

        {/* List Section */}
        {this.props.type == "projectedScores" &&
          this.props.data.map((item, index) => (
            <div key={index}>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                  fontFamily: "Montserrat",
                  padding: "6px 0"
                }}
              >
                <div style={{ display: "flex" }}>
                  <div>
                    <div
                      style={{
                        width: 50,
                        height: 50,
                        borderRadius: "50%",
                        background: grey_8,
                        position: "relative",
                        backgroundImage: `url(${item.avatar ||
                          require("../../images/no_highlights.png")})`,
                        backgroundPosition: "top left",
                        backgroundSize: "cover",
                        backgroundRepeat: "no-repeat"
                      }}
                    >
                      <div
                        style={{
                          position: "absolute",
                          width: 22,
                          height: 22,
                          borderRadius: "50%",
                          // background: "#ededed",
                          background: grey_1,
                          top: 0,
                          right: -6,
                          backgroundImage: `url(${item.subIcon ||
                            require("../../images/batS@3x.png")})`,
                          backgroundPosition: "center",
                          backgroundSize: "16px 16px",
                          padding: "8px",
                          backgroundRepeat: "no-repeat"
                        }}
                      />
                    </div>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      marginLeft: 14
                    }}
                  >
                    <div
                      style={{
                        paddingBottom: 2,
                        fontFamily: "Montserrat",
                        fontSize: "14px",
                        fontWeight: "normal",
                        fontStyle: "normal",
                        fontStretch: "normal",
                        lineHeight: "normal",
                        letterSpacing: "normal",
                        color: "#141b2f"
                      }}
                    >
                      {item.name}
                    </div>
                    {item.role === "AllRounder" ? (
                      <div style={{ display: "flex", flexDirection: "row"}}>
                        <div
                          style={{
                            fontSize: 12,
                            color: grey_10,
                            fontWeight: 700
                          }}
                        >
                          {item.projectedScore}
                          <span
                            style={{
                              color: grey_2,
                              fontWeight: 500,
                              paddingLeft: 6
                            }}
                          >
                            Runs
                          </span>
                        </div>

                        <div
                          style={{
                            background: black,
                            opacity: 0.2,
                            width: 1,
                            height: "60%",
                            margin: "0 15px"
                          }}
                        />

                        <div
                          style={{
                            fontSize: 12,
                            color: grey_10,
                            fontWeight: 700
                          }}
                        >
                          {item.projectedWickets}
                          <span
                            style={{
                              color: grey_2,
                              fontWeight: 500,
                              paddingLeft: 6
                            }}
                          >
                            Wickets
                          </span>
                        </div>
                      </div>
                    ) : (
                      <div>
                        {item.role === "Keeper" ? (
                          <div
                            style={{
                              fontSize: 12,
                              color: grey_10,
                              fontWeight: 700
                            }}
                          >
                            {item.projectedScore}
                            <span
                              style={{
                                color: grey_2,
                                fontWeight: 500,
                                paddingLeft: 6
                              }}
                            >
                              Runs
                            </span>
                          </div>
                        ) : (
                          <div
                            style={{
                              fontSize: 12,
                              color: grey_10,
                              fontWeight: 700
                            }}
                          >
                         
                            {item.projectedWickets || item.projectedScore}
                            <span
                              style={{
                                color: grey_2,
                                fontWeight: 500,
                                paddingLeft: 6
                              }}
                            >
                              {item.projectedScore && "Runs"}
                              {item.projectedWickets &&
                                item.role !== "Keeper" &&
                                "Wickets"}
                            </span>
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                </div>
                <div>
                  <div
                    style={{
                      // background: "linear-gradient(to right, #353e59, #1e2437)",
                      background: gradientBlack,
                      borderRadius: 3,
                      color: white,
                      padding: 8,
                      fontFamily: "Montserrat",
                      fontSize: 10,
                      textAlign: "center",
                      width: "67px"
                    }}
                  >
                    <div>Points</div>
                    <div
                      style={{
                        marginTop: 2,
                        fontSize: 14,
                        fontWeight: 700
                      }}
                    >
                      {item.projectedPoints}
                    </div>
                  </div>
                </div>
              </div>
              {this.props.data.length - 1 !== index && <HrLine />}
            </div>
          ))}
        {/* List Section closing */}
        {/* List Section type 2 */}
        {this.props.type == "lastPerformance" &&
          this.props.data.map((item, index) => (
            <div key={index}>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  fontFamily: "Montserrat",
                  padding: "9px 0"
                  // borderBottom: "1px solid #777777"
                }}
              >
                <div>
                  <div
                    style={{
                      width: 50,
                      height: 50,
                      borderRadius: "50%",
                      background: grey_8,
                      position: "relative",
                      backgroundImage: `url(${item.avatar ||
                        require("../../images/no_highlights.png")})`,
                      backgroundPosition: "top left",
                      backgroundSize: "cover",
                      backgroundRepeat: "no-repeat"
                    }}
                  >
                    <div
                      style={{
                        position: "absolute",
                        width: 22,
                        height: 22,
                        borderRadius: "50%",
                        background: grey_1,
                        top: 0,
                        right: -6,
                        backgroundImage: `url(${item.subIcon ||
                          require("../../images/no_highlights.png")})`,
                        backgroundPosition: "center",
                        backgroundSize: "16px 16px",
                        padding: "8px",
                        backgroundRepeat: "no-repeat"
                      }}
                    />
                  </div>
                </div>
                <div
                  style={{
                    display: "flex",
                    flex: 1,
                    flexDirection: "column",
                    justifyContent: "center",
                    marginLeft: 14
                  }}
                >
                  <div
                    style={{
                      paddingBottom: 2,
                      fontFamily: "Montserrat",
                      fontSize: "14px",
                      fontWeight: "normal",
                      fontStyle: "normal",
                      fontStretch: "normal",
                      lineHeight: "normal",
                      letterSpacing: "normal",
                      color: "#141b2f",
                      paddingBottom: 2
                    }}
                  >
                    {item.name}
                  </div>

                  {item.role === "AllRounder" ? (
                    <div>
                      <div
                        style={{
                          fontSize: 12,
                          color: grey_10,
                          fontWeight: 700,
                          // background: '#f1f1f1',
                          display: "flex",
                          flex: 1
                        }}
                      >
                        {item.stats &&
                          item.stats.map((stats, index) => (
                            <div
                              style={{ display: "flex", alignItems: "center" }}
                            >
                              <div
                                style={{
                                  // width: 56,
                                  fontFamily: "Montserrat"
                                  // borderRight: '1px solid #b7b7b7',
                                  // marginRight: 10,
                                }}
                                key={index}
                              >
                                <div
                                  style={{
                                    fontSize: 12,
                                    color: grey_10,
                                    fontWeight: 700
                                  }}
                                >
                                  {stats.runs}
                                  <br />
                                  {stats.wickets === "/0"
                                    ? "DNB"
                                    : stats.wickets}
                                </div>
                                <div
                                  style={{
                                    fontSize: 10,
                                    color: grey_6,
                                    fontWeight: 600
                                  }}
                                >
                                  {stats.against}
                                </div>
                              </div>
                              {index !== 2 && (
                                <div
                                  style={{
                                    background: black,
                                    opacity: 0.2,
                                    width: 1,
                                    height: "60%",
                                    margin: "0 15px"
                                  }}
                                />
                              )}
                            </div>
                          ))}
                      </div>
                    </div>
                  ) : (
                    <div
                      style={{
                        fontSize: 12,
                        color: grey_10,
                        fontWeight: 700,
                        // background: '#f1f1f1',
                        display: "flex",
                        flex: 1
                      }}
                    >
                      {item.stats &&
                        item.stats.map((stats, index) => (
                          <div
                            style={{ display: "flex", alignItems: "center" }}
                          >
                            <div
                              style={{
                                // width: 56,
                                fontFamily: "Montserrat"
                                // borderRight: '1px solid #b7b7b7',
                                // marginRight: 10,
                              }}
                              key={index}
                            >
                              <div
                                style={{
                                  fontSize: 12,
                                  color: grey_10,
                                  fontWeight: 700
                                }}
                              >
                                {stats.runs}
                              </div>
                              <div
                                style={{
                                  fontSize: 10,
                                  color: grey_6,
                                  fontWeight: 600
                                }}
                              >
                                {stats.against}
                              </div>
                            </div>
                            {index !== 2 && (
                              <div
                                style={{
                                  background: black,
                                  opacity: 0.2,
                                  width: 1,
                                  height: "60%",
                                  margin: "0 15px"
                                }}
                              />
                            )}
                          </div>
                        ))}
                    </div>
                  )}
                </div>
              </div>
              {this.props.data.length - 1 !== index && <HrLine />}
            </div>
          ))}
        {/* List Section type 2 closing */}
      </div>
    );
  }
}

export default FantasyPlayerPerformance;
