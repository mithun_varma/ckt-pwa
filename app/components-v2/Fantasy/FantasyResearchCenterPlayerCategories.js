import React, { Component } from "react";

import wk_icon from "../../images/wk_icon.png";
import selected_wk_icon from "../../images/selected_wk_icon.png";
import bat_icon from "../../images/bat-inactive-fntsy.svg";
import selected_bat_icon from "../../images/selected_bat_icon.png";
import ar_icon from "../../images/ar_icon.png";
import selected_ar_icon from "../../images/selected_ar_icon.png";
import bowl_icon from "../../images/bowl_icon.png";
import selected_bowl_icon from "../../images/selected_bowl_icon.png";
import HrLine from "../commonComponent/HrLine";

export class FantasyResearchCenterPlayerCategories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activePlayerCategory: this.props.activePlayerCategory
    };
  }

  render() {
    return (
      <React.Fragment>
        <div
          style={{
            padding: "16px",
            paddingTop: 0,
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          {/* <FantasyWicketCategoryButton category={}/> */}
          {this.props.matchConfig.playerCategories.map(
            (playerCategory, index) => (
              <div
                key={index}
                style={{
                  width: "46px",
                  height: "89px",
                  overflow: "hidden",
                  objectFit: "contain",
                  borderRadius: "3px",
                  border:
                    playerCategory === this.props.activePlayerCategory
                      ? "solid 0.5px #d44030"
                      : "solid 0.5px #dbdbdb"
                }}
                onClick={() =>
                  this.props.handleActivePlayerCategory(playerCategory)
                }
              >
                <div
                  style={{
                    width: "100%",
                    height: "30%",
                    marginTop: "1%",
                    objectFit: "contain",
                    borderRadius: "3px 3px 0 0",
                    background:
                      playerCategory === this.props.activePlayerCategory
                        ? "#d44030"
                        : "#eeeeee",
                    textAlign: "center"
                  }}
                >
                  {playerCategory == "WK" &&
                    (playerCategory === this.props.activePlayerCategory ? (
                      <img
                        src={selected_wk_icon}
                        alt=""
                        style={{
                          width: "70%",
                          marginTop: "10%"
                        }}
                      />
                    ) : (
                      <img
                        src={wk_icon}
                        alt=""
                        style={{
                          width: "70%",
                          marginTop: "10%"
                        }}
                      />
                    ))}
                  {playerCategory == "AR" &&
                    (playerCategory === this.props.activePlayerCategory ? (
                      <img
                        src={selected_ar_icon}
                        alt=""
                        style={{
                          width: "70%",
                          marginTop: "10%"
                        }}
                      />
                    ) : (
                      <img
                        src={ar_icon}
                        alt=""
                        style={{
                          width: "70%",
                          marginTop: "10%"
                        }}
                      />
                    ))}
                  {playerCategory == "BOWL" &&
                    (playerCategory === this.props.activePlayerCategory ? (
                      <img
                        src={selected_bowl_icon}
                        alt=""
                        style={{
                          width: "70%",
                          marginTop: "10%"
                        }}
                      />
                    ) : (
                      <img
                        src={bowl_icon}
                        alt=""
                        style={{
                          width: "70%",
                          marginTop: "10%"
                        }}
                      />
                    ))}
                  {playerCategory == "BAT" &&
                    (playerCategory === this.props.activePlayerCategory ? (
                      <img
                        src={selected_bat_icon}
                        alt=""
                        style={{
                          width: "70%",
                          marginTop: "10%"
                        }}
                      />
                    ) : (
                      <img
                        src={bat_icon}
                        alt=""
                        style={{
                          width: "70%",
                          marginTop: "10%"
                        }}
                      />
                    ))}
                </div>

                <div
                  style={{
                    width: "100%",
                    marginTop: "45%",
                    fontFamily: "Oswald",
                    fontSize: "21px",
                    fontWeight: "500",
                    lineHeight: "0.81",
                    letterSpacing: "0.5px",
                    textAlign: "center",
                    color:
                      playerCategory === this.props.activePlayerCategory
                        ? "#141b2f"
                        : "#a1a4ac"
                  }}
                />
                <div
                  style={{
                    width: "100%",
                    marginTop: "20%",
                    fontFamily: "Montserrat",
                    fontSize: "11px",
                    fontWeight: "500",
                    lineHeight: "1.55",
                    letterSpacing: "0.3px",
                    textAlign: "center",
                    color:
                      playerCategory === this.props.activePlayerCategory
                        ? "#141b2f"
                        : "#a1a4ac"
                  }}
                >
                  {playerCategory}
                  <p>
                    {this.props.selectedPlayers[playerCategory].length > 0
                      ? this.props.selectedPlayers[playerCategory].length
                      : ""}
                  </p>
                </div>
              </div>
            )
          )}
        </div>
        <HrLine />
        <div
          style={{
            fontFamily: "Montserrat",
            fontSize: 11,
            textAlign: "center",
            padding: 8,
            color: "#d44030"
          }}
        >
          {this.props.activePlayerCategory == "WK" &&
            `You can select one Wicket-keeper`}
          {this.props.activePlayerCategory == "BAT" &&
            `You can select  3-5 Batsman`}
          {this.props.activePlayerCategory == "AR" &&
            `You can select 1-3 All-rounders`}
          {this.props.activePlayerCategory == "BOWL" &&
            `You can select 3-5 Bowlers `}
        </div>
        <HrLine />
      </React.Fragment>
    );
  }
}

export default FantasyResearchCenterPlayerCategories;
