import React, { Component } from "react";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import FantasyResearchCard from "./FantasyResearchCard";
import FantasyPlayerPerformance from "./FantasyPlayerPerformance";
import FantasyTeamCreation from "./FantasyTeamCreation";
import HrLine from "../commonComponent/HrLine";
import {
  gradientGrey,
  grey_8,
  white,
  grey_10
} from "../../styleSheet/globalStyle/color";
import ScrollButtons from "../Rankings/ScrollButtons";
import PillTabs from "../../components/Common/PillTabs";

import FanFight from "../commonComponent/FanFight";

import fanFightLogo from "../../images/Fan_FightLLogo.svg";

import batIcon from "../../images/batS@3x.png";
import ballIcon from "../../images/ballS@3x.png";
import wkIcon from "../../images/wk@3x.png";
import all from "../../images/all@3x.png";
import Modal from "react-responsive-modal";
import Error from "@material-ui/icons/Error";
import EmptyState from "../commonComponent/EmptyState";
const TABS = ["Playing XI", "Criclytics", "Fantasy", "Articles"];
let TEAMS = [];
const PERFORMANCE_TYPE = [
  {
    label: "Projected Scores & Points",
    key: "projectedScores"
  },
  {
    label: "Last 3 Performances",
    key: "lastPerformance"
  }
];

export class FantasyTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeButton: "lastPerformance",
      activePill: "",
      createTeam: true,
      modalOpen: false
    };
  }

  handleMatchSelect = val => {
    this.setState({ activePill: val });
  };

  handleActiveButton = activeButton => {
    this.setState({
      activeButton
    });
  };

  componentDidMount = () => {
    const visited = localStorage.getItem("isVisited");
    if (!visited) {
      this.setState({
        modalOpen: true
      });
    }
  };

  _handleCloseModal = () => {
    this.setState({
      modalOpen: false
    });
    localStorage.setItem("isVisited", true);
  };

  toggleCreateTeam = value => {
    this.setState({
      createTeam: !value
    });
  };

  render() {
    const { fanFightList } = this.props.scoreDetails;
    // console.log(fanFightList);
    if (!fanFightList)
      return (
        <div
          style={{
            fontFamily: "Montserrat",
            textAlign: "center",
            minHeight: "40vh"
          }}
        >
          <EmptyState msg={"no fantasy data available for this match"} />
        </div>
      );
    const playerArray = [
      ...fanFightList.matchSquad[0].playersArray.map(pl => ({
        teamImg: fanFightList.matchTeamHomeFlag,
        ...pl
      })),
      ...fanFightList.matchSquad[1].playersArray.map(pl => ({
        teamImg: fanFightList.matchTeamAwayFlag,
        ...pl
      }))
    ];
    const teams = [
      fanFightList.matchTeamHomeShort,
      fanFightList.matchTeamAwayShort
    ];

    return (
      <React.Fragment>
        <div
          style={
            {
              // background: "#dcdcdc",
              // minHeight: "100vh",
              // padding: "16px 0"
            }
          }
        >
          <FantasyTeamCreation
            history={this.props.history}
            handleBack={this.toggleCreateTeam}
            saveFanFightTeam={this.props.saveFanFightTeam}
            fanFightList={this.props.scoreDetails.fanFightList}
            teams={teams}
            data={playerArray}
          />
        </div>
        <Modal
          open={this.state.modalOpen}
          closeIconSize={20}
          showCloseIcon={true}
          onClose={this._handleCloseModal}
          closeOnOverlayClick={true}
          styles={{
            modal: {
              padding: 0,
              minWidth: "95%"
            },
            closeButton: {
              top: 11,
              right: 8
            },
            closeIcon: {
              fill: "#8b94ae"
            }
          }}
          center
        >
          <div style={{ fontFamily: "Montserrat", fontSize: 12 }}>
            <div
              style={{
                display: "flex",
                flex: 1,
                justifyContent: "space-between",
                background: gradientGrey,
                alignItems: "center",
                padding: "12px 8px 12px 16px",
                color: grey_10,
                fontSize: 12
              }}
            >
              <div
                style={{
                  // fontSize: 12,
                  fontFamily: "Montserrat",
                  fontWeight: 500
                  // flex: 0.74,
                  // ...this.props.titleStyles
                }}
              >
                <span style={{ display: "flex", alignItems: "center" }}>
                  <Error style={{ color: "#e66456" }} /> How we calculate
                  Projected Points
                </span>
              </div>
            </div>
            <HrLine />
            <p style={{ color: "#727682", padding: "16px 32px" }}>
              Historical data, strength of the opposition, recent performances,
              player’s strengths/weaknesses across venues… These are just a few
              of the factors that we apply, to determine a player’s performance,
              for an upcoming game.
            </p>
            <p style={{ color: "#727682", padding: "0px 32px 16px" }}>
              Having said that, the very nature of cricket rests on levels of
              unpredictability, which add to the beauty of the sport. Thus,
              whatever we offer, are merely what our machine learning algorithms
              have projected to be the most likely outcome, and are in no way a
              certainty.
            </p>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export default FantasyTab;
