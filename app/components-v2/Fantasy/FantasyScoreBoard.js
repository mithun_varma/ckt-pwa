import React, { Component } from "react";
import ExpandMore from "@material-ui/icons/ExpandMore";
import CardGradientTitle from "../commonComponent/CardGradientTitle";
import {
  white,
  orange_10,
  gradientGrey,
  cardShadow,
  grey_8,
  grey_10,
  green_8
} from "../../styleSheet/globalStyle/color";
import HrLine from "../commonComponent/HrLine";

export class FantasyScoreBoard extends Component {
  render() {
    return (
      <div
        style={{
          margin: "10px 16px",
          // padding: 10,
          background: white,
          borderRadius: 3,
          boxShadow: cardShadow
        }}
      >
        {/* Title goes here */}
        <CardGradientTitle
          title={this.props.title}
          rootStyles={{ background: white }}
          subtitle={this.props.subtitle}
          subtitleStyles={{
            color: orange_10,
            marginRight: 10
          }}
        />

        <div
          style={{
            display: "flex",
            background: gradientGrey,
            fontFamily: "Montserrat",
            fontSize: 12,
            color: grey_8,
            alignItems: "center",
            padding: 10
            // borderBottom: '1px solid #777777',
          }}
        >
          <div style={{ flex: 0.4 }}>Players</div>
          <div style={{ flex: 0.3, display: "flex", justifyContent: "center" }}>
            Selected By <ExpandMore style={{ fontSize: 20 }} />
          </div>
          <div style={{ flex: 0.3, display: "flex", justifyContent: "center" }}>
            Final Points<ExpandMore style={{ fontSize: 20 }} />
          </div>
        </div>
        <HrLine />
        {/* list item */}
        {[1, 2, 3, 4].map(item => (
          <div>
            <div
              style={{
                display: "flex",
                fontFamily: "Montserrat",
                fontSize: 12,
                fontWeight: 600,
                color: grey_10,
                alignItems: "center",
                padding: "14px 10px"
                // borderBottom: '1px solid #777777',
              }}
            >
              <div style={{ flex: 0.4 }}>Virat Kohli</div>
              <div
                style={{ flex: 0.3, display: "flex", justifyContent: "center" }}
              >
                85.42%
              </div>
              <div
                style={{
                  flex: 0.3,
                  display: "flex",
                  justifyContent: "center",
                  color: green_8
                }}
              >
                45
              </div>
            </div>
            <HrLine />
          </div>
        ))}
      </div>
    );
  }
}

export default FantasyScoreBoard;
