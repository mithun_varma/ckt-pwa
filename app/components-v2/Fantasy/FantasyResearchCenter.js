import React, { Component } from "react";
import { white } from "../../styleSheet/globalStyle/color";
// import FantasyResearchCenterBoard from "../../components-v2/Fantasy/FantasyResearchCenterBoard";
import FantasyResearchCenterPlayerCategories from "../../components-v2/Fantasy/FantasyResearchCenterPlayerCategories";
import FantasyResearchCenterPlayerList from "../../components-v2/Fantasy/FantasyResearchCenterPlayerList";
// import left_orange_arrow_icon from '../../images/left_orange_arrow_icon.png';

export class FantasyResearchCenter extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div
          style={{
            background: white,
            display: "flex",
            justifyContent: "space-between",
            fontFamily: "Montserrat",
            fontSize: 12,
            padding: "12px 16px",
            paddingBottom: 0
          }}
        />
        <FantasyResearchCenterPlayerCategories
          title="Fantasy Team Stat"
          {...this.props}
        />
        <FantasyResearchCenterPlayerList
          data={this.props.data}
          {...this.props}
        />
      </div>
    );
  }
}

export default FantasyResearchCenter;
