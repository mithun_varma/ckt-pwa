import React, { Component } from "react";
import {
  cardShadow,
  white,
  grey_10,
  orange_1,
  grey_8
} from "../../styleSheet/globalStyle/color";
import CardGradientTitle from "../commonComponent/CardGradientTitle";
import indianFlag_New from "../../images/indianFlag.svg";
import HrLine from "../commonComponent/HrLine";
import Reveal from "react-reveal/Reveal";

export class TopPerformerCard extends Component {
  render() {
    const { players } = this.props;
    return (
      <div
        style={{
          fontFamily: "Montserrat"
          // paddingBottom: 12
          // margin: "10px 0 10px 16px",
          // background: white,
          // borderRadius: 3,
          // boxShadow: cardShadow
        }}
      >
        {/* Card Gradient Title goes here */}
        <CardGradientTitle
          title={this.props.title}
          rootStyles={{ background: white, fontWeight: 500 }}
        />
        {players &&
          players.map((player, index) => (
            <Reveal effect={"fadeIn"} duration={1000}>
              <div
                style={{
                  // width: 150,
                  // padding: "4px 0",
                  fontFamily: "Montserrat",
                  backgroundColor: "#f4f1f1",
                  margin: "12px 12px 0px 12px",
                  borderRadius: 2
                  // marginRight: 12
                }}
              >
                <div
                  style={{
                    padding: "10px",
                    // marginBottom: "30px",
                    display: "flex",
                    flex: 0.6,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center"
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",

                      alignItems: "center"
                    }}
                  >
                    <div>
                      <div
                        style={{
                          width: 50,
                          height: 50,
                          marginRight: 8,
                          borderRadius: "50%",
                          background: grey_8,
                          position: "relative",
                          backgroundImage: `url(${player.playerImage ||
                            require("../../images/fallbackProjection.png")})`,
                          backgroundPosition: "top left",
                          backgroundSize: "cover",
                          backgroundRepeat: "no-repeat"
                        }}
                      />
                    </div>
                    <div
                      style={{
                        fontFamily: "Montserrat",
                        fontSize: 14,
                        fontWeight: "normal",
                        color: "#141b2f",
                        paddingVertical: 6,
                        display: "flex",
                        flexDirection: "column",
                        marginTop: "2px"
                      }}
                    >
                      <span
                        style={{
                          color: "#000000",
                          marginBottom: 7,
                          fontFamily: "Montserrat",
                          fontSize: 14,
                          fontWeight: "500",
                          fontStyle: "normal",
                          letterSpacing: 0,
                          color: "#141b2f"
                        }}
                      >
                        {player.playerName}
                      </span>

                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center"
                        }}
                      >
                        <React.Fragment>
                          <img
                            style={{ width: 14, height: 12 }}
                            src={player.teamImg}
                          />
                          <span
                            style={{
                              fontFamily: "Montserrat",
                              fontSize: 11,
                              fontWeight: "500",
                              fontStyle: "normal",
                              letterSpacing: 0,
                              color: "#727682",
                              marginLeft: 4
                            }}
                          >
                            {player.playerClubName}
                          </span>
                        </React.Fragment>
                      </div>
                    </div>
                  </div>
                  <div style={{ textAlign: "end" }}>
                    <p
                      style={{
                        fontFamily: "Oswald",
                        fontSize: 21,
                        fontWeight: 500
                      }}
                    >
                      {player.playerMatchPoints}
                    </p>
                    <p style={{ fontSize: 11, color: "#727682" }}>points</p>
                  </div>
                </div>
                <HrLine />
                <div
                  style={{
                    display: "flex",
                    flex: 1,
                    alignItems: "center",
                    padding: "12px 8px"
                  }}
                >
                  <p style={{ flex: 0.6, fontSize: 11, fontWeight: 500 }}>
                    Selected By :{" "}
                    <span style={{ color: "#d44030", fontWeight: 600 }}>
                      {`${player.playerSelectionPercentage}%`}
                    </span>{" "}
                    <span style={{ fontWeight: 400, color: "#474b5b" }}>
                      People
                    </span>
                  </p>
                  <div
                    style={{
                      flex: 0.5,
                      // width: "80%",
                      background: "#ece2e2",
                      borderRadius: 4
                    }}
                  >
                    <div
                      style={{
                        width: `${player.playerSelectionPercentage}%`,
                        height: 8,
                        background:
                          player.playerSelectionPercentage > 50
                            ? "#35a863"
                            : player.playerSelectionPercentage == 50
                              ? "#727682"
                              : "#d64b4b", //#d64b4b -red, #727682 -grey
                        borderRadius: "4px 0px 0px 4px"
                      }}
                    />
                  </div>
                </div>
              </div>
            </Reveal>
          ))}
        <div
          onClick={this.props.toggleTopPlayers}
          style={{
            padding: 16,
            color: "#d44030",
            textAlign: "center",
            fontSize: 12,
            fontWeight: 600
          }}
        >
          View All Players
        </div>
      </div>
    );
  }
}

export default TopPerformerCard;
