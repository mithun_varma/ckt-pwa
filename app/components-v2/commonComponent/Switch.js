import React, { PureComponent } from "react";
import { white } from "../../styleSheet/globalStyle/color";

export class Switch extends PureComponent {
  state = {
    checkedStatus1: true,
    checkedStatus2: false
  };
  toggle = value => {
    this.setState(
      {
        checkedStatus1: !this.state.checkedStatus1,
        checkedStatus2: !this.state.checkedStatus2
      },
      () => {
        this.state.checkedStatus1
          ? this.props.callback(this.props.value1)
          : this.props.callback(this.props.value2);
      }
    );
  };
  render() {
    const { value1, value2 } = this.props;
    return (
      <div
        style={{
          display: "flex"
        }}
      >
        <div
          onClick={this.toggle}
          style={{
            borderRadius: "3px 0px 0px 3px",
            backgroundColor: this.state.checkedStatus1 ? "#d44030" : "#fafafa",
            flex: 0.5,
            border: this.state.checkedStatus1
              ? "solid 0.5px transparent"
              : "solid 0.5px #979797",
            borderRight: this.state.checkedStatus1 ? "none" : "",
            alignSelf: "center",
            padding: "4px 10px",
            color: this.state.checkedStatus1 ? white : "#9b9b9b",
            fontFamily: this.state.checkedStatus1 ? "mont500" : "mont400",
            fontSize: "12px",
            minWidth: 45,
            textAlign: "center"
            // fontWeight: this.state.checkedStatus1 ? "bold" : 500
          }}
        >
          {value1}
        </div>
        <div
          onClick={this.toggle}
          style={{
            borderRadius: "0px 3px 3px 0px",
            backgroundColor: this.state.checkedStatus2 ? "#d44030" : "#fafafa",
            flex: 0.5,
            border: this.state.checkedStatus2
              ? "solid 0.5px transparent"
              : "solid 0.5px #979797",
            borderLeft: this.state.checkedStatus2 ? "none" : "",
            textAlign: "center",
            padding: "4px 10px",
            color: this.state.checkedStatus2 ? white : "#9b9b9b",
            fontFamily: this.state.checkedStatus2 ? "mont500" : "mont400",
            fontSize: "12px",
            // fontWeight: this.state.checkedStatus2 ? "bold" : 500,
            alignSelf: "center",
            minWidth: 45,
            textAlign: "center"
          }}
        >
          {value2}
        </div>
      </div>
    );
  }
}

export default Switch;
