import React, { PureComponent } from "react";
import { white, grey_8 } from "../../styleSheet/globalStyle/color";
const factImage = require("../../images/criclytics.svg");
export class FactEngiene extends PureComponent {
  render() {
    const { fact, bgColor, noHeader, padding, changeText } = this.props;

    return (
      <div
        style={
          {
            // paddingBottom: 16
          }
        }
      >
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            backgroundColor: bgColor || white,
            height: "auto",
            width: "auto",
            padding: "18px 7px"
          }}
        >
          <div
            style={{
              padding: padding ? "10px 2px" : "10px 15px"
            }}
          >
            <img
              src={factImage}
              style={{
                // alignSelf: "center",
                height: "42px",
                width: "42px"
              }}
            />
          </div>

          <div
            style={{
              flex: 1,
              alignSelf: "center"
              // padding: "10px 15px"
            }}
          >
            {!noHeader && (
              <div
                style={{
                  fontFamily: "Montserrat",
                  fontSize: "12px",
                  fontWeight: 600,
                  fontStyle: "normal",
                  fontStretch: "normal",
                  lineHeight: 1.5,
                  letterSpacing: "normal",
                  color: "#141b2f",
                  paddingTop: "5px"
                }}
              >
                Fact Engine
              </div>
            )}
            <div
              style={{
                fontFamily: "Montserrat",
                fontSize: "12px",
                fontWeight: 400,
                lineHeight: changeText ? 1.42 : 1.5,
                color: grey_8,
                // opacity: 0.7,
                wordWrap: "break-word !important",
                // padding: "2px 16px",
                ...this.props.valueStyles
              }}
            >
              {fact}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FactEngiene;
