import React, { Component } from "react";
import {
  white,
  cardShadow,
  grey_10,
  grey_8,
  red_10,
  grey_6
} from "../../styleSheet/globalStyle/color";
import HrLine from "./HrLine";
import OverCard from "../commonComponent/OverCard";
import BallCard from "../commonComponent/BallCard";

const inningsPrefix = ["st", "nd", "rd", "th"];
class InningsGraph extends Component {
  constructor(props) {
    super(props);
    this.state = {
      innings: 1
    };
  }

  // temporary Function
  selectMatchInnings = operation => {
    this.setState({
      innings:
        operation == "decrement"
          ? this.state.innings == 1
            ? 1
            : this.state.innings - 1
          : this.state.innings == 4
            ? 4
            : this.state.innings + 1
    });
  };

  render() {
    return (
      <div
        style={{
          background: white,
          margin: "16px 16px",
          borderRadius: 3,
          boxShadow: cardShadow
        }}
      >
        {/* title */}
        <div
          style={{
            display: "flex",
            flex: 1,
            // justifyContent: "space-between",
            alignItems: "center",
            padding: "8px 16px"
          }}
        >
          <div style={{ flex: 0.1 }}>
            <img
              src={require("../../images/analytics.svg")}
              alt="arrow"
              style={{
                width: 20
              }}
            />
          </div>
          <div
            style={{
              color: grey_10,
              fontSize: 12,
              fontFamily: "Montserrat",
              fontWeight: 500,
              // marginLeft: 20,
              flex: 0.6
            }}
          >
            Projected Player Performance
          </div>
          <div
            style={{
              flex: 0.3,
              display: "flex",
              justifyContent: "flex-end"
            }}
          >
            <div
              style={{
                borderRadius: 3,
                border: "1px solid #e0e1e4",
                background: "#fbfbfc",
                display: "flex"
                // flex: .34
              }}
            >
              <div
                style={{
                  fontSize: 11,
                  fontFamily: "Montserrat",
                  fontWeight: 400,
                  color: grey_8,
                  padding: "8px 12px",
                  borderWidth: "0 1px 0 0",
                  borderStyle: "solid",
                  borderColor: "#e0e1e4",
                  letterSpacing: "0.3px",
                  minWidth: 92
                }}
              >
                <span
                  style={{
                    padding: "0 3px"
                  }}
                >
                  {`${this.state.innings}${
                    inningsPrefix[this.state.innings - 1]
                  } innings`}
                </span>
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  padding: "0 8px"
                }}
              >
                <img
                  // src={require("../../images/arrow_up_small.svg")}
                  src={require("../../images/arrow_up.svg")}
                  alt=""
                  style={{
                    width: 8,
                    height: 8,
                    margin: "1px 0"
                  }}
                  onClick={() => this.selectMatchInnings("decrement")}
                />
                <img
                  // src={require("../../images/arrow_up_small.svg")}
                  src={require("../../images/arrow_down.svg")}
                  alt=""
                  style={{
                    width: 8,
                    height: 8,
                    // transform: "rotate(180deg)",
                    margin: "1px 0"
                  }}
                  onClick={() => this.selectMatchInnings("increment")}
                />
              </div>
            </div>
          </div>
        </div>
        <HrLine />
        {/* title closing */}

        {/* Body Section  */}
        <div>
          <div>Chart Section</div>
          <div
            style={{
              position: "relative"
            }}
          >
            <HrLine />
            <div
              style={{
                position: "absolute",
                bottom: -6,
                right: 6,
                fontFamily: "Montserrat",
                fontWeight: 500,
                fontSize: 10,
                color: grey_8,
                opacity: 0.6,
                letterSpacing: 1,
                background: white
              }}
            >
              Overs
            </div>
          </div>
          {/* balls summary section */}
          <div
            style={{
              display: "flex",
              flex: 1,
              padding: "8px 0"
            }}
          >
            <div
              style={{
                flex: 0.14,
                // background: "lightgrey",
                display: "flex",
                justifyContent: "flex-end",
                alignItems: "center"
              }}
            >
              <OverCard data={{ title: 30, caption: "over" }} />
            </div>
            <div
              style={{
                flex: 0.86,
                display: "flex"
              }}
            >
              <BallCard balls={[0, 1, 2, "W", 4, 6]} />
            </div>
          </div>
          <HrLine />
          {/* players current STATUS */}
          <div
            style={{
              display: "flex",
              flex: 1,
              padding: "8px 16px",
              flexWrap: "wrap"
            }}
          >
            {[1, 2, 3, 4].map((item, i) => (
              <div key={i}
                style={{
                  display: "flex",
                  width: "50%",
                  fontSize: 12,
                  color: grey_10,
                  background: "#FFF",
                  padding: "4px 0"
                }}
              >
                <div
                  style={{
                    fontFamily: "Montserrat",
                    fontWeight: 400,
                    flex: 0.55
                  }}
                >
                  T Paine
                </div>
                <div
                  style={{
                    flex: 0.45,
                    fontFamily: "Montserrat",
                    fontWeight: 700
                  }}
                >
                  7 (40 balls)
                </div>
              </div>
            ))}
          </div>
          <HrLine />

          {/* players Wicket current STATUS */}
          <div
            style={{
              display: "flex",
              flex: 1,
              padding: "8px 16px",
              flexWrap: "wrap"
            }}
          >
            <OverCard
              rootStyles={{
                background: red_10
              }}
              data={{ title: "W", caption: "29.3" }}
            />
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                padding: "0 16px"
              }}
            >
              <div
                style={{
                  fontFamily: "Montserrat",
                  fontWeight: 500,
                  fontSize: 10,
                  color: grey_6
                }}
              >
                Ishanth Sharma to Tim Pain
              </div>
              <div
                style={{
                  fontFamily: "Montserrat",
                  fontWeight: 500,
                  fontSize: 12,
                  color: grey_10
                }}
              >
                That’s out!
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default InningsGraph;
