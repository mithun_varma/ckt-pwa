import React, {Component} from 'react';
import { screenWidth } from '../../styleSheet/screenSize/ScreenDetails';
import { gradientGrey } from '../../styleSheet/globalStyle/color';

const dataBowler=[
    {
        'name':'Kuldeep Yadav',
        'over':'4',
        'median':'1',
        'run':'27',
        'wicket':'2',
        'economy':'4.8',
    },
    {
        'name':'Kuldeep Yadav',
        'over':'4',
        'median':'1',
        'run':'27',
        'wicket':'2',
        'economy':'4.8',
    },
    {
        'name':'Kuldeep Yadav',
        'over':'4',
        'median':'1',
        'run':'27',
        'wicket':'2',
        'economy':'4.8',
    },
    {
        'name':'Kuldeep Yadav',
        'over':'4',
        'median':'1',
        'run':'27',
        'wicket':'2',
        'economy':'4.8',
    },
    {
        'name':'Kuldeep Yadav',
        'over':'4',
        'median':'1',
        'run':'27',
        'wicket':'2',
        'economy':'4.8',
    },
    {
        'name':'Kuldeep Yadav',
        'over':'4',
        'median':'1',
        'run':'27',
        'wicket':'2',
        'economy':'4.8',
    },
]

export default class BowlerScoreBoard extends Component {
    constructor(props){
        super(props);
        this.state={
            dataBowler:[]

        };
    }
    componentDidMount(){
        const bowlerScore = this.props.bowlerScore;
        this.setState({dataBowler:bowlerScore});

    }
    render(){
        return(
            <div style={pageStyle.bowlerScoreContainer}>
                <div style={pageStyle.bowlerScoreBoardHeading}>
                    <div style={pageStyle.bowlerNameField}> 
                        BOWLER
                    </div>
                    <div style={pageStyle.bowlerOtherField}>
                        O
                    </div>
                    <div style={pageStyle.bowlerOtherField}>
                        M
                    </div>
                    <div style={pageStyle.bowlerOtherField}>
                        R
                    </div>
                    <div style={pageStyle.bowlerOtherField}>
                        W
                    </div>
                    <div style={pageStyle.bowlerOtherField}>
                        ECO
                    </div>
                </div>
                {
                    (this.state.dataBowler).map((data)=>{
                        const name = data.playerName;
                        const bowler = data['bowlingStatistics'][1];
                        const over = bowler.overs;
                        const runs = bowler.runs;
                        const maidenOvers =bowler.maidenOvers;
                        const wickets = bowler.wickets;
                        const economy = bowler.economy;
                        return(
                        over &&
                            <div style={pageStyle.bowlerScoreData}>
                                <div style={pageStyle.bowlerNameValue}>
                                    {name}
        
                                </div>
                                <div style={pageStyle.bowlerOverValue}>
                                    {over}
        
                                </div>
                                <div style={pageStyle.bowlerOtherValue}>
                                    {maidenOvers}
        
                                    
                                </div>
                                <div style={pageStyle.bowlerOtherValue}>
                                    {runs}
        
                                </div>
                                <div style={pageStyle.bowlerOtherValue}>
                                    {wickets}
        
                                </div>
                                <div style={pageStyle.bowlerOtherValue}>
                                    {economy}
                                </div>
                            </div>
                         
                        );

                    })
                 
                }
            </div>
        );
    }

}

const pageStyle ={
    bowlerOtherValue:{
        display:'flex',
        flex:0.15,
        justifyContent:'flex-start',
        marginTop:'7px',
        marginBotom:'10px',
        fontSize:'12px',
        color:"#727682",
        fontFamily:'Montserrat-Medium',

    },
    bowlerOverValue:{
        display:'flex',
        flex:0.15,
        justifyContent:'flex-start',
        marginTop:'7px',
        marginBotom:'10px',
        fontSize:'12px',
        color:'#141b2f',
        fontWeight:'700',
        fontFamily:'Montserrat-Medium',

    },
    bowlerNameValue:{
        display:'flex',
        flex:0.50,
        justifyContent:'flex-start',
        marginTop:'7px',
        marginBotom:'10px',
        fontSize:'12px',
        fontWeight:'700',
        color:'#141b2f',
        fontFamily:'Montserrat-Medium',
    },
    bowlerScoreData:{
        display:'flex',
        flexDirection:'row',
        flex:1,
    },
    bowlerOtherField:{
        display:'flex',
        flex:0.15,
        justifyContent:'flex-start',
        fontSize:'12px',
        color:"#727682",
        fontFamily:'Montserrat-Medium',
    },
    bowlerNameField:{
        display:'flex',
        flex:0.50,
        justifyContent:'flex-start',
        fontSize:'12px',
        color:"#727682",
        fontFamily:'Montserrat-Medium',
    },
    bowlerScoreBoardHeading:{
        display:'flex',
        flexDirection:'row',
        justifyContent:'flex-start',
        backgroundImage:gradientGrey,
        color:'#727682',
        fontSize:'10px',
        paddingTop:'12px',
        paddingBottom:'12px',
        flex:1,

    },
    bowlerScoreContainer:{
        display:'flex',
        flexDirection:'column',
        width:screenWidth * 0.911,
        flex:1,
        boxShadow:'0px 3px 6px 0px #13000000',
        backgroundColor:'#ffffff',
        paddingLeft:'12px',
    }
}