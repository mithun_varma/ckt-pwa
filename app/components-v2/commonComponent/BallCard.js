import React, { Component } from "react";
import {
  grey_8,
  white,
  red_10,
  blue_10,
  grey_10,
  green_10,
  grey_7
} from "../../styleSheet/globalStyle/color";
import sortBy from "lodash/sortBy";

class BallCard extends Component {
  render() {
    return sortBy(this.props.balls, d => d.ballNo).map((item, key) => (
      <div
        key={`${key + 1}`}
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          // flex: 0.166,
          // width: "16%"
          margin: "0 2.3%"
        }}
      >
        <div
          style={{
            width: 28,
            height: 28,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            borderRadius: "50%",
            border: `1px solid ${
              item.runs == 6
                ? blue_10
                : item.runs == 4
                  ? green_10
                  : item.wicketcount > 0
                    ? red_10
                    : grey_7
            }`,
            color:
              item.runs == 4 || item.runs == 6 || item.wicketcount > 0
                ? white
                : grey_10,
            background:
              item.runs == 6
                ? blue_10
                : item.runs == 4
                  ? green_10
                  : item.wicketcount > 0
                    ? red_10
                    : white,
            fontFamily: "Montserrat",
            fontWeight: 700,
            fontSize: item.ballStr ? 10 : 14
          }}
        >
          {item.wicketcount > 0 ? "w" : item.ballStr ? item.ballStr : item.runs}
        </div>
        <div
          style={{
            fontFamily: "Montserrat",
            fontWeight: 500,
            fontSize: 10,
            opacity: 0.5,
            color: grey_8,
            textAlign: "center",
            marginTop: 4
          }}
        >
          {/* 29.1 */}
          {item.overStr}
        </div>
      </div>
    ));
  }
}

export default BallCard;
