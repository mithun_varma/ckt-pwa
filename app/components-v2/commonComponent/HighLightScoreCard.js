import React, {Component} from 'react';
import { screenWidth } from '../../styleSheet/screenSize/ScreenDetails';

export default class HighlightScore extends Component{
    constructor(props){
        super(props);
        this.state={

        }
        this._handelToggel =this._handelToggel.bind(this);

    }
    componentDidMount(){

    }
    render(){
        return(
            <div style={pageStyle.container}>
                <div>
                    {this.toggelActionBar()}
                </div>
                <div>
                    {this.fours()}
                </div>
                <div>
                    {this.sixes()}
                </div>
                <div>
                    {this.wicket()}
                </div>

            </div>
        );
    }
    
    fours(){
        return(
            <div>
                {this.boxDyanamicFour(32.5)}
            </div>
        );
    }
    sixes(){
        return(
            <div>
                {this.boxDyanamicSix(33.5)}

            </div>
        );

    }
    wicket(){
        return(
            <div>
                {this.boxDyanamicWicket(24.6)}
            </div>
        );

    }
    boxDyanamicFour(over){
        return(
            <div style={pageStyle.boxContainerFour}>
               <div style={pageStyle.topHalf}>
                    {"4"}
                </div>
                <div style={{display:'flex',width:screenWidth*0.08,marginLeft:screenWidth*0.018,backgroundColor:'#fff',height:1}}>
                {null}
                </div>
                <div style={pageStyle.lowerHalf}>
                    {over}
                </div>
            </div>
        );
    }
    boxDyanamicSix(over){
        return(
            <div style={pageStyle.boxContainerSix}>
                <div style={pageStyle.topHalf}>
                    {"6"}
                </div>
                <div style={{display:'flex',width:screenWidth*0.08,marginLeft:screenWidth*0.018,backgroundColor:'#fff',height:1}}>
                {null}
                </div>
                <div style={pageStyle.lowerHalf}>
                    {over}
                </div>
            </div>
        );
    }
    boxDyanamicWicket(over){
        return(
            <div style={pageStyle.boxContainerWicket}>
                <div style={pageStyle.topHalf}>
                    {"W"}
                </div>
                <div style={{display:'flex',width:screenWidth*0.08,marginLeft:screenWidth*0.018,backgroundColor:'#fff',height:1}}>
                {null}
                </div>
                <div style={pageStyle.lowerHalf}>
                    {over}
                </div>
            </div>
        );
    }
    toggelActionBar(){
        return(
            <div style={pageStyle.actionBarContainer}>
                <div style={{display:'flex',flex:0.10}}>
                    {null}
                </div>
                <div style={pageStyle.activeButton}>
                    All
                </div>
                <div style={{display:'flex',flex:0.10}}>
                    {null}
                </div>
                <div style={pageStyle.unactiveButton}>
                    Fours
                </div>
                <div style={{display:'flex',flex:0.10}}>
                    {null}
                </div>
                <div style={pageStyle.activeButton}>
                    Sixes
                </div>
                <div style={{display:'flex',flex:0.10}}>
                    {null}
                </div>
                <div style={pageStyle.unactiveButton}>
                    Wickets
                </div>
                <div style={{display:'flex',flex:0.10}}>
                    {null}
                </div>
            </div>
        );
    }
    
    _handelToggel(event){

    }

}

const pageStyle={
    topHalf:{
        display:'flex',
        paddingTop:'3px',
        paddingBottom:'3px',
        paddingLeft:'5px',
        paddingRight:'5px',
        justifyContent:'center',
        fontSize:'21px',
    },
    lowerHalf:{
        display:'flex',
        paddingBottom:'2px',
        paddingTop:'2px',
        paddingRight:'5px',
        paddingLeft:'5px',
        justifyContent:'center',
        fontSize:'10px'
    },
    boxContainerFour:{
        display:'flex',
        flexDirection:'column',
        flex:1,
        borderRadius:'3px',
        boxShadow:'0px 3px 6px 0px #13000000',
        backgroundColor:'#35a863',
        color:'#fff',
        width:screenWidth*0.116,
    },
    boxContainerSix:{
        display:'flex',
        flexDirection:'column',
        flex:1,
        borderRadius:'3px',
        boxShadow:'0px 3px 6px 0px #13000000',
        backgroundColor:'#4a90e2',
        color:'#fff',
        width:screenWidth*0.116,
    },
    boxContainerWicket:{
        display:'flex',
        flexDirection:'column',
        flex:1,
        borderRadius:'3px',
        boxShadow:'0px 3px 6px 0px #13000000',
        backgroundColor:'#d64b4b',
        color:'#fff',
        width:screenWidth*0.116,
    },
    container:{
        display:'flex',
        flexDirection:'column',
        flex:1,
        width:screenWidth*0.911,
        marginLeft:screenWidth*0.025,

    },
    actionBarContainer:{
        display:'flex',
        flex:1,
        flexDirection:'row',
        padding:'4px',
        justifyContent:'center'

    },
    activeButton:{
        display:'flex',
        flex:0.15,
        backgroundColor:'#f76b1c',
        boxShadow:'0px 3px 6px 0px #13000000',
        justifyContent:'center',
        paddingLeft:'6.5px',
        paddingRight:'6.5px',
        paddingTop:'2px',
        paddingBottom:'2px',
        fontSize:'12px',
        letterSpacing:'0.6px',
        color:'#fff',
        borderRadius:'3px',
        letterSpacing:'0.6px',
    },
    unactiveButton:{
        flex:0.15,
        display:'flex',
        color:'#9b9b9b',
        display:'flex',
        paddingLeft:'6.5px',
        paddingRight:'6.5px',
        paddingTop:'2px',
        paddingBottom:'2px',
        backgroundColor:'#fafafa',
        boxShadow:'0px 3px 6px 0px #13000000',
        justifyContent:'center',
        fontSize:'12px',
        letterSpacing:'0.6px',
        border:'0.5px solid #979797',
        borderRadius:'3px',
        letterSpacing:'0.6px'
    }
}