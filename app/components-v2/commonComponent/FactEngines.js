import React from "react";
import {
  white,
  grey_8
} from "../../styleSheet/globalStyle/color";
class FactEngines extends React.Component {
  render() {
      const { data, activeClass } = this.props;
    //   console.log(data);
    return (
      <div
        style={{
          background: white,
          // margin: "12px 16px 0px 16px",
          margin: "0px 16px 0px 16px",
          borderRadius: 3,
          // boxShadow: cardShadow,
          position: "relative",
          padding: "16px",
          transition: 'all 0.5s',
          ...this.props.rootStyles
        }}
      >
        <div
          style={{
            display: "flex",
            // alignItems: "center",
            marginBottom: "12px"
          }}
        >
          <div
            style={{
              height: 45,
              width: 45,
              display: "flex",
              justifyContent: "center",
              borderRadius: "50%",
              // border: `solid 1px ${grey_8}`,
              marginRight: "5%"
            }}
          >
            <img src={require("../../images/group-22.svg")} />
          </div>
          <div
            style={{
              fontFamily: "Montserrat",
              fontSize: "12px",
              flex: "1",
              fontWeight: "400"
            }}
          >
            <p style={{ fontWeight: "600" }}>Fact Engine</p>
            <p style={{ color: grey_8 }} className={activeClass}>
              {data}
            </p>
          </div>
        </div>
        {/* <div style={hrLineStyle} />
        <div
          style={{
            display: "flex",
            fontFamily: "Montserrat",
            fontWeight: 400,
            color: grey_8,
            fontSize: 10,
            justifyContent: "space-between",
            background: gradientGrey,
            // background: "grey",
            padding: "10px 0",
            alignItems: "center",
            textAlign: "center",
            textTransform: "uppercase"
          }}
        >
          Team Score Projections
        </div>
        <div style={hrLineStyle} /> */}
      </div>
    );
  }
}

export default FactEngines;
