import React from "react";
import {
  white,
  grey_10,
  grey_8,
  red_Orange
} from "../../styleSheet/globalStyle/color";
import ChevronRight from "@material-ui/icons/ChevronRight";
import map from "lodash/map";
class TeamScoreProjection extends React.Component {
  render() {
    const { data, activeClass } = this.props;
    return (
      <div
        style={{
          background: white,
          margin: "0px 16px 0px 16px",
          borderRadius: 3,
          // boxShadow: cardShadow,
          position: "relative",
          padding: "16px",
          transition: "all 0.5s",
          ...this.props.rootStyles
        }}
        onClick={this.props.redirectTo}
      >
        <div
          style={{
            display: "flex"
            // alignItems: "center",
            // marginBottom: "12px"
          }}
        >
          <div
            style={{
              height: 45,
              width: 45,
              display: "flex",
              justifyContent: "center",
              borderRadius: "50%",
              // border: `solid 1px ${grey_8}`,
              marginRight: "5%"
            }}
          >
            <img src={require("../../images/group-22.svg")} />
          </div>
          <div
            style={{
              fontFamily: "Montserrat",
              fontSize: "12px",
              flex: "1",
              fontWeight: "400"
            }}
          >
            <p style={{ fontWeight: "600" }}>1st Batting Score Projector</p>
            <div
              style={{ color: grey_8, background: "lightgreys" }}
              className={activeClass}
            >
              <div style={{ marginTop: 6 }}>
                {/* {[1, 2].map(list => ( */}
                {map(this.props.scoreProjection, (team, index) => (
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      padding: "6px 0"
                    }}
                  >
                    <img
                      src={
                        team && team.teamAvatar
                          ? team.teamAvatar
                          : require("../../images/flag_empty.svg")
                      }
                      alt=""
                      style={{
                        width: 26,
                        height: 19,
                        borderRadius: 2
                      }}
                    />
                    <span
                      style={{
                        color: grey_8,
                        fontSize: 12,
                        margin: "0 16px",
                        width: 36
                      }}
                    >
                      {team && team.teamName
                        ? team.teamName.substring(0, 4).toUpperCase()
                        : ""}
                    </span>
                    <span
                      style={{
                        color: grey_10,
                        fontSize: 12,
                        fontWeight: 700
                      }}
                    >
                      {team && team.teamScore ? team.teamScore : ""} runs
                    </span>
                  </div>
                ))}
              </div>
            </div>
          </div>
          <div>
            <ChevronRight
              style={{
                color: red_Orange
              }}
            />
          </div>
        </div>
        {/* <div style={hrLineStyle} />
        <div
          style={{
            display: "flex",
            fontFamily: "Montserrat",
            fontWeight: 400,
            color: grey_8,
            fontSize: 10,
            justifyContent: "space-between",
            background: gradientGrey,
            // background: "grey",
            padding: "10px 0",
            alignItems: "center",
            textAlign: "center",
            textTransform: "uppercase"
          }}
        >
          Team Score Projections
        </div>
        <div style={hrLineStyle} /> */}
      </div>
    );
  }
}

export default TeamScoreProjection;
