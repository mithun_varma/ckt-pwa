import React, { Component } from "react";
import { grey_10, blue_grey } from "../../styleSheet/globalStyle/color";
import HrLine from "./HrLine";
import ChevronRight from "@material-ui/icons/ChevronRight";

class ListItems extends Component {
  render() {
    const normal = "normal";
    return (
      <div>
        <div
          className={this.props.isAnimated && "fade-out-left"}
          style={{
            display: "flex",
            flex: 1,
            padding: "16px",
            alignItems: "center",
            color: grey_10,
            ...this.props.rootStyles
          }}
          onClick={e => {
            e.preventDefault();

            this.props.param && this.props.onClick(this.props.param);
          }}
        >
          <div
            style={{
              flex: 0.74,
              display: "flex",
              alignItems: "center"
            }}
          >
            {this.props.isAvatar && (
              <div
                style={{
                  display: "flex",
                  marginRight: 12,
                  ...this.props.avatarWrapperStyles
                }}
              >
                <img
                  src={
                    // this.props.ranking && this.props.ranking.image ||
                    this.props.avatar || require("../../images/flagAvatar.png")
                  }
                  alt=""
                  style={
                    this.props.avatarStyle || {
                      width: 30,
                      height: 30,
                      borderRadius: "50%"
                    }
                  }
                />
              </div>
            )}
            <div
              style={{
                fontFamily: "Montserrat",
                fontWeight: 500,
                fontSize: 14,
                ...this.props.titleStyle
              }}
            >
              {/* {this.props.ranking && this.props.ranking.name} */}
              {this.props.title}

              {this.props.belowTitle && (
                <div
                  style={{
                    fontFamily: "Montserrat",
                    fontWeight: 500,
                    fontSize: 12,
                    fontStyle: normal,
                    fontStretch: normal,
                    lineHeight: normal,
                    letterSpacing: normal,
                    color: "#727682"
                  }}
                >
                  {this.props.belowTitle}
                </div>
              )}
            </div>
          </div>
          <div
            style={{
              flex: 0.26,
              display: "flex",
              alignItems: "center",
              justifyContent: "flex-end"
            }}
          >
            {this.props.subtitle && (
              <div
                style={{
                  textAlign: "right",
                  paddingRight: 14,
                  fontFamily: "Montserrat",
                  fontWeight: 500,
                  fontSize: 14
                }}
              >
                {/* {this.props.ranking && this.props.ranking.rating} */}
                {this.props.subtitle}
              </div>
            )}

            <div
              style={{
                display: "flex",
                justifyContent: "flex-end"
              }}
            >
              {this.props.rightElement ? (
                this.props.rightElement
              ) : (
                <ChevronRight
                  style={{ color: this.props.iconColor || blue_grey }}
                />
              )}
            </div>
          </div>
        </div>
        {!this.props.isHR && <HrLine color={this.props.customColorHR} />}
      </div>
    );
  }
}

export default ListItems;
