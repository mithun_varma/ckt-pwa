import React, { Component } from "react";
import { grey_10, white } from "../../styleSheet/globalStyle/color";
import moment from "moment";

class ScheduleDateCard extends Component {
  render() {
    return (
      <div
        style={{
          background: grey_10,
          borderRadius: 3,
          padding: "2px 6px 3px",
          color: white,
          width: 50,
          textAlign: "center",
          ...this.props.rootStyles
        }}
      >
        <div
          style={{
            fontFamily: "Montserrat",
            opacity: 0.9,
            fontSize: 10,
            fontWeight: 500,
            // padding: "3px 4px",
            padding: "3px 0px",
            borderWidth: "0 0 1px 0",
            borderStyle: "solid",
            textTransform: "uppercase",
            borderColor: "rgba(255,255,255, 0.4)",
            ...this.props.titleStyles
          }}
        >
          {moment(this.props.data.startDate).format("MMM")}
          {/* {this.props.data.month} */}
        </div>
        <div
          style={{
            fontFamily: "Montserrat",
            fontWeight: 600,
            fontSize: 18,
            textAlign: "center",
            padding: "2px 0"
          }}
        >
          {moment(this.props.data.startDate).format("DD")}
        </div>
      </div>
    );
  }
}

export default ScheduleDateCard;
