import React, { Component } from "react";
import { white, red_Orange } from "../../styleSheet/globalStyle/color";
import HrLine from "./HrLine";

class TabBar extends Component {
  handleScroll = (e, val) => {
    let decider = document.getElementById(val).offsetLeft;

    let pos = e.target.parentNode.getBoundingClientRect().x;
    let xStart = e.target.parentNode.parentNode.getBoundingClientRect().x;
    let xEnd = e.target.parentNode.parentNode.getBoundingClientRect().width;
    let xWidth = xEnd - xStart;
    let diff1 = Math.abs(xEnd - pos);
    let diff2 = Math.abs(xStart - pos);
    let diff = diff1 > diff2 ? "left" : "right";
    if (diff === "left") {
      e.target.parentNode.parentNode.scrollLeft -=
        e.target.parentNode.getBoundingClientRect().width + 30;
    } else if (diff === "right") {
      e.target.parentNode.parentNode.scrollLeft +=
        e.target.parentNode.getBoundingClientRect().width + 30;
    }
    e.preventDefault();
    if (val !== this.props.activeItem) {
      this.props.onClick(val);
    }
  };
  render() {
    const { activeItem } = this.props;
    return (
      <div className={"tabsSection"}>
        <ul
          style={{
            width: "100%",
            background: white,
            display: "flex",
            flexWrap: "nowrap",
            overflowX: "auto",
            alignItems: "center",
            boxSizing: "border-box",
            margin: 0,
            padding: 0
          }}
        >
          {this.props.items.map(val => (
            <li
              role="presentation"
              // className={val === this.props.activeItem ? "active" : "inactive"}
              key={val}
              id={val}
              onClick={e => {
                this.handleScroll(e, val);
              }}
              style={{
                position: "relative",
                height: "36px",
                display: "flex",
                flex: "1 0 auto",
                alignItems: "center",
                justifyContent: "center",
                padding: "0 10px",
                margin: "0px 5px",
                cursor: "pointer"
              }}
            >
              <span
                style={{
                  color: "#999999",
                  textAlign: "center",
                  textTransform: "capitalize",
                  fontSize: "12px",
                  fontWeight: "normal",
                  fontStyle: "normal",
                  fontStretch: "normal",
                  lineHeight: "normal",
                  letterSpacing: "0.6px",
                  color: "#727682",
                  ...this.props.valueStyles
                }}
              >
                {val}
              </span>
              <div
                style={{
                  height: 2,
                  width: "100%",
                  borderRadius: "2px",
                  // background: gradientOrange,
                  background:
                    val === this.props.activeItem ? red_Orange : "#FFF",
                  position: "absolute",
                  bottom: 0
                }}
              />
            </li>
          ))}
        </ul>
        <HrLine />
      </div>
    );
  }
}

export default TabBar;
