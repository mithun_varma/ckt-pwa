import React, { Component } from "react";
import {
  grey_10,
  white,
  grey_6,
  gradientGrey
} from "../../styleSheet/globalStyle/color";
import { hrLineStyle } from "../../styleSheet/globalStyle/styleConfig";

const caption = "test caption";
// const caption = "";

class ListModal extends Component {
  render() {
    return (
      // main wrapper
      <div
        style={{
          background: "rgba(0, 0, 0, 0.4)",
          // opacity: 0.4,
          display: "flex",
          flex: 1,
          height: "100vh",
          alignItems: "center",
          justifyContent: "center",
          padding: "0 16px"
        }}
      >
        {/* inner main wrapper */}
        <div
          style={{
            width: "100%",
            background: white
          }}
        >
          {/* title */}
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              color: grey_10,
              padding: "12px 16px",
              background: gradientGrey
            }}
          >
            <div
              style={{
                color: grey_10,
                fontFamily: "Montserrat",
                fontWeight: 500,
                fontSize: 12
              }}
            >
              Trophy Cabinet
            </div>
            <div>
              <img
                src={require("../../images/close.png")}
                alt=""
                style={{
                  width: 12
                }}
              />
            </div>
          </div>
          <div style={hrLineStyle} />
          {/* body */}
          {/* Large Avatar */}
          <div
            style={{
              maxHeight: "80vh",
              overflow: "scroll"
            }}
          >
            Large Avatar Example
            {[1, 2].map(item => (
              <div>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    flex: 1,
                    padding: "18px 12px"
                  }}
                >
                  <div
                    style={{
                      flex: 0.16
                    }}
                  >
                    <img
                      src={require("../../images/trophy.png")}
                      alt="avatar"
                      style={{ width: 41 }}
                    />
                  </div>
                  <div
                    style={{
                      flex: 0.84,
                      padding: "0 12px",
                      color: grey_10,
                      fontSize: 12,
                      fontFamily: "Montserrat",
                      fontWeight: 500
                    }}
                  >
                    <div>ICC World Cup</div>
                    <div
                      style={{
                        fontFamily: "Montserrat",
                        fontWeight: 400
                      }}
                    >
                      2011, 2007
                    </div>
                  </div>
                </div>
                <div style={hrLineStyle} />
              </div>
            ))}
            {/* Small Avatar */}
            Small Avatar Example
            {[1, 2].map(item => (
              <div>
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    flex: 1,
                    padding: "12px 12px"
                  }}
                >
                  <div
                    style={{
                      flex: 0.16
                    }}
                  >
                    <img
                      src={require("../../images/flag_avatar.png")}
                      alt="avatar"
                      style={{ width: 21 }}
                    />
                  </div>
                  <div
                    style={{
                      flex: 0.84,
                      padding: "0 12px",
                      color: grey_10,
                      fontSize: 12,
                      fontFamily: "Montserrat",
                      fontWeight: 500
                    }}
                  >
                    <div>Indian Stadiums</div>
                    {caption && (
                      <div
                        style={{
                          fontFamily: "Montserrat",
                          fontWeight: 600,
                          color: grey_6
                        }}
                      >
                        Kolkata
                      </div>
                    )}
                  </div>
                </div>
                <div style={hrLineStyle} />
              </div>
            ))}
          </div>
          {/* Body Closing */}
        </div>
      </div>
    );
  }
}

export default ListModal;
