import React from "react";
import * as d3 from "d3";
import * as shape from "d3-shape";
import * as d3Axis from "d3-axis";
import { select as d3Select } from "d3-selection";

import dropRightWhile from "lodash/dropRightWhile";

export class PredictChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static getDerivedStateFromProps = (nextProps, state) => {
    // console.log(nextProps, state);
    return null;
  };

  //   componentDidUpdate = (props) => {
  //     console.log(props);
  //   }
  render() {
    const margin = { top: 20, right: 20, bottom: 40, left: 50 };
    const width = 350 - margin.left - margin.right;
    const height = 240 - margin.top - margin.bottom;
    const { data } = this.props;

    let filteredData = dropRightWhile(
      data,
      dt => +dt.overNo !== this.props.currentOver
    );

    if (this.props.isSecond) {
      filteredData = dropRightWhile(
        data,
        dt => +dt.overNo !== this.props.currentOver
      );
    }
    const x = d3
      .scaleLinear()
      .domain([0, this.props.maxXvalue])
      .range([0, width]);
    let y = d3
      .scaleLinear()
      .domain([0, d3.max(this.props.data, d => +d.predictedScore)])
      .range([height, 0]);
    if (this.props.isSecond) {
      y = d3
        .scaleLinear()
        .domain([0, d3.max(this.props.innings2Data, d => +d.currentScore)])
        .range([height, 0]);
    }

    const xAxis = d3Axis
      .axisBottom()
      .scale(x)
      .ticks(5)
      .tickSize(0)
      .tickPadding([12]);

    const yAxis = d3Axis
      .axisLeft()
      .scale(y)
      .ticks(5)
      .tickSize(-width)
      .tickPadding([12]);

    const line = shape
      .line()
      .curve(d3.curveNatural)
      .x(d => x(d.overNo))
      .y(d => y(d.currentScore));
    const linePredict = shape
      .line()
      .x(d => x(d.overNo))
      .y(d => y(d.predictedScore));
    d3Select(this.axisElement)
      .call(xAxis)
      .selectAll(".tick line")
      .attr("style", "visibility:hidden");
    d3Select(this.axisYElement)
      .call(yAxis)
      .selectAll(".tick line")
      .attr("stroke-dasharray", "3,2")
      .attr("stroke", "#e3e4e6");

    const MaxData = {
      color: "#d6d6d6",
      text: "High",
      data: [
        {
          overNo: this.props.currentOver,
          predictedScore: filteredData[filteredData.length - 1].currentScore,
          currentScore: filteredData[filteredData.length - 1].currentScore
        },
        {
          overNo: filteredData[filteredData.length - 1].predictedOver,
          predictedScore: filteredData[filteredData.length - 1].predictedScore,
          currentScore: filteredData[filteredData.length - 1].currentScore
        }
      ]
    };
    const markArr = [MaxData];
    // const { data } = this.props;
    // console.log(filteredData);
    return (
      <div className="chart-container">
        <svg height={height + 50} width={width + 80}>
          <defs>
            <linearGradient id="MyGradient" x1="0" y1="0" x2="0" y2="1">
              <stop offset="5%" stopColor="#E82849" stopOpacity={0.4} />
              <stop offset="98%" stopColor="#E82849" stopOpacity={0.2} />
            </linearGradient>
          </defs>

          <g id="xAxis" transform="translate(50,10)">
            {/* <path
              id="area"
              d={area(filteredData)}
              fill="url(#MyGradient)"
              // stroke="#E82849"
              // strokeDasharray="3 3"
              // strokeWidth="2"
              style={{ opacity: 0.8 }}
            /> */}
            <path
              id="line"
              d={line(filteredData)}
              fill="none"
              stroke={this.props.lineColor}
              //   strokeDasharray="3 3"
              strokeWidth="2"
              // style={{ opacity: 0.8 }}
            />
            {this.props.innings2Data && (
              <g>
                <path
                  id="line"
                  d={line(this.props.innings2Data)}
                  fill="none"
                  stroke={this.props.secondInnColor}
                  // strokeDasharray="3 3"
                  strokeWidth="2"
                  // style={{ opacity: 0.8 }}
                />
                {/* <circle
                  className=".dot"
                  cx={x(
                    this.props.innings2Data[this.props.innings2Data.length - 1]
                      .overNo
                  )}
                  cy={y(
                    this.props.innings2Data[this.props.innings2Data.length - 1]
                      .currentScore
                  )}
                  r={4}
                  fill="#fff"
                  stroke="#192354"
                  strokeWidth="2"
                /> */}
              </g>
            )}
            {/* <g>
              <path
                id="vLine"
                d={line(vData)}
                fill="none"
                stroke="#192354"
                // strokeDasharray="3 3"
                strokeWidth="2"
                // style={{ opacity: 0.8 }}
              />
            </g> */}

            {markArr.map(mk => (
              <React.Fragment key={mk.text}>
                <path
                  id="vLine"
                  d={linePredict(mk.data)}
                  fill="none"
                  stroke={mk.color}
                  //   strokeDasharray="3 2"
                  strokeWidth="1"
                  // style={{ opacity: 0.8 }}
                />
                <text
                  className="customeTicks"
                  x={width}
                  y={y(mk.data[1].predictedScore - 10)}
                  fill="#a70e13"
                  style={{ fontFamily: "mont500", fontSize: "10px" }}
                >
                  {`${mk.data[1].predictedScore}`}
                </text>
              </React.Fragment>
            ))}
            {/* <filter id="dropshadow" height="130%">
              <feGaussianBlur in="SourceAlpha" stdDeviation="3" />
              <feOffset dx="2" dy="2" result="offsetblur" />
              <feComponentTransfer>
                <feFuncA type="linear" slope="0.5" />
              </feComponentTransfer>
              <feMerge>
                <feMergeNode />
                <feMergeNode in="SourceGraphic" />
              </feMerge>
            </filter> */}
            {/* <circle
              className=".dot"
              //   style={{filter:"url(#dropshadow)"}}
              cx={x(this.props.currentOver)}
              cy={y(filteredData[filteredData.length - 1].currentScore)}
              r={5}
              fill={this.props.lineColor}
              stroke="#fff"
              strokeWidth="4"
            /> */}
            <defs>
              <circle
                id="largeC"
                cx={x(this.props.currentOver)}
                cy={y(filteredData[filteredData.length - 1].currentScore)}
                r="8"
              />
              <filter
                id="a"
                width="163.6%"
                height="163.6%"
                x="-31.8%"
                y="-22.7%"
                filterUnits="objectBoundingBox"
              >
                <feOffset dy="2" in="SourceAlpha" result="shadowOffsetOuter1" />
                <feGaussianBlur
                  in="shadowOffsetOuter1"
                  result="shadowBlurOuter1"
                  stdDeviation="2"
                />
                <feColorMatrix
                  in="shadowBlurOuter1"
                  values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25206635 0"
                />
              </filter>
              <linearGradient id="grad" x1="100%" x2="0%" y1="50%" y2="50%">
                <stop offset="0%" stopColor={this.props.lineColor} />
                <stop offset="100%" stopColor={this.props.lineColor} />
              </linearGradient>
            </defs>
            <g fill="none" fillRule="nonzero">
              <use fill="#000" filter="url(#a)" xlinkHref="#largeC" />
              <use fill="#FFF" xlinkHref="#largeC" />
              <circle
                cx={x(this.props.currentOver)}
                cy={y(filteredData[filteredData.length - 1].currentScore)}
                r="4"
                fill="url(#grad)"
              />
            </g>
            <g
              ref={el => {
                this.axisElement = el;
              }}
              id="xAxisleft"
              fill="#000"
              transform={`translate(0,${height})`}
            />
            <g
              ref={el => {
                this.axisYElement = el;
              }}
              id="yAxis"
              fill="#000"
              transform="translate(0,0)"
            />
          </g>
          <text
            style={{
              fontFamily: "mont600",
              fontSize: "10px",
              fill: "#5d5d5d"
            }}
            x={width}
            y={height + margin.bottom}
            textAnchor="middle"
          >
            OVERS
          </text>
          <text
            style={{
              fontFamily: "mont600",
              fontSize: "10px",
              fill: "#5d5d5d"
            }}
            x={height / 2}
            y={-10}
            transform="rotate(90)"
            textAnchor="middle"
          >
            RUNS
          </text>
        </svg>
      </div>
    );
  }
}

export default PredictChart;
