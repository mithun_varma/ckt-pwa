import React from "react";
import { cardShadow, white, grey_10 } from "../../styleSheet/globalStyle/color";
import ChevronRight from "@material-ui/icons/ChevronRight";
class QuickBytes extends React.Component {
  render() {
    const { data, activeClass, disableClick } = this.props;
    return (
      <div
        style={{
          background: white,
          margin: "12px 16px",
          borderRadius: 3,
          boxShadow: cardShadow,
          position: "relative",
          // padding: "38px 12px 16px",
          padding: "38px 16px 16px 12px",
          // boxShadow: cardShadow,
          transition: "all 0.5s",
          ...this.props.rootStyles
        }}
      >
        <div
          style={{
            position: "absolute",
            top: 0,
            // left: 24,
            left: 12,
            background: grey_10,
            fontFamily: "Montserrat",
            fontWeight: 500,
            fontSize: 10,
            color: white,
            padding: "4px 16px",
            letterSpacing: "1.5px",
            borderRadius: "0 0 3px 3px",
            textTransform: "uppercase"
          }}
        >
          Quick Bytes
        </div>
        <div
          style={{
            color: grey_10,
            fontFamily: "Montserrat",
            fontWeight: 400,
            fontSize: 12,
            // padding: "12px 28px 24px 14px",
            minHeight: "55px",
            letterSpacing: "0.3px",
            cursor: "pointer"
          }}
          className={activeClass}
        >
          {data &&
            data.link && (
              <ChevronRight
                style={{
                  color: grey_10,
                  position: "absolute",
                  right: "16px",
                  marginTop: "-3px",
                  marginRight: -6
                }}
              />
            )}
          <div style={{ width: "94%" }}>
            {data &&
              (data.link ? (
                !disableClick ? (
                  <a
                    href={data.link ? data.link : ""}
                    // target="_blank"
                    style={{
                      color: grey_10
                    }}
                  >
                    {data.quickByte}
                  </a>
                ) : (
                  data.quickByte
                )
              ) : (
                data.quickByte
              ))}
          </div>
        </div>
      </div>
    );
  }
}

export default QuickBytes;
