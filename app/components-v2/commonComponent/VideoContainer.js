import React, { Component } from "react";

import EmptyState from "components-v2/commonComponent/EmptyState";
import Loader from "components-v2/commonComponent/Loader";
import { cardShadow } from "../../styleSheet/globalStyle/color";
// import "../../../node_modules/video-react/dist/video-react.css";
import ReactPlayer from "react-player";
import Eden from "../../images/eden.jpg";

class VideoContainer extends Component {
  constructor(props) {
    super(props);
    this._handelPlayerClick = this._handelPlayerClick.bind(this);
  }
  _handelPlayerClick(event) {
    const id = event.target.alt;
    this.props.history.push("/players/:" + id);
  }
  componentDidMount() {
    // window.addEventListener('scroll', this.handleScroll,true);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll(event) {
    var element = document.getElementById("image");
    var topPos = element.getBoundingClientRect().top + window.scrollX;
  }

  render() {
    return (
      <div style={{ ...pageStyle.container, ...this.props.rootStyles }}>
        <div style={pageStyle.headerWrapper}>
          <div style={pageStyle.header}>{this.props.title}</div>
          {this.props.poweredStadium && (
            <div style={pageStyle.featuredHeader}>
              <div>Powered By</div>
              <div>
                Criclytics
                <span> &trade;</span>
              </div>
            </div>
          )}
        </div>
        <div style={pageStyle.hrLine} />

        {this.props.isBig ? (
          <div style={pageStyle.imageWrapper}>
            {this.props.loading ? (
              <Loader styles={{ height: "175px" }} noOfLoaders={1} />
            ) : this.props.data && this.props.data.length ? (
              this.props.data.map(item => (
                <div
                  style={pageStyle.VideoContainer}
                  onClick={() => this.props.showVid(item)}
                  name="image"
                  key={item.name}
                >
                  <div
                    style={{
                      width: 290,
                      height: 130,
                      position: "relative"
                    }}
                  >
                    <ReactPlayer
                      url={
                        item.path ? item.path : "https://youtu.be/eG9zcpY74q4"
                      }
                      style={pageStyle.reactPlayer}
                      controls={true}
                      light={true}
                      width="100%"
                      height="100%"
                    />
                    <div
                      style={{
                        position: "absolute",
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        background: "none",
                        opacity: 0.9,
                        ...this.props.backgroundStyles
                      }}
                    />
                  </div>
                  <div style={pageStyle.imageName}>{item.name}</div>
                </div>
              ))
            ) : (
              <EmptyState
                msg={`No ${this.props.title}`}
                rootStyles={{
                  height: 200
                }}
              />
            )}
          </div>
        ) : (
          <div style={pageStyle.imageWrapper}>
            {this.props.loading ? (
              <Loader styles={{ height: "175px" }} noOfLoaders={1} />
            ) : this.props.data && this.props.data.length ? (
              this.props.data.map(item => (
                <div
                  style={pageStyle.VideoContainer}
                  onClick={() => this.props.showVid(item)}
                  name="image"
                  key={item.name}
                >
                  <div
                    style={{
                      width: 190,
                      height: 130,
                      position: "relative"
                    }}
                  >
                    <ReactPlayer
                      url={
                        item.path ? item.path : "https://youtu.be/eG9zcpY74q4"
                      }
                      style={pageStyle.reactPlayer}
                      controls={true}
                      width="100%"
                      height="100%"
                      position="fixed"
                    />
                    <div
                      style={{
                        position: "absolute",
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        background: "none",
                        opacity: 0.9,
                        ...this.props.backgroundStyles
                      }}
                    />
                  </div>
                  <div style={pageStyle.imageName}>{item.name}</div>
                </div>
              ))
            ) : (
              <EmptyState
                msg={`No ${this.props.title}`}
                rootStyles={{
                  height: 200
                }}
              />
            )}
          </div>
        )}
      </div>
    );
  }
}

// https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png
export default VideoContainer;

const pageStyle = {
  container: {
    display: "flex",
    flexDirection: "column",
    margin: "0 0 0 16px",
    marginTop: -150,
    borderRadius: 3,
    background: "#FFFFFF",
    boxShadow: cardShadow
  },
  headerWrapper: {
    // padding: "12px 16px",
    fontFamily: "Montserrat",
    fontWeight: 600,
    fontSize: 10,
    display: "flex",
    flex: 1,
    justifyContent: "space-between"
  },
  header: {
    margin: "12px 0px 12px 16px"
  },
  featuredHeader: {
    background: "#edeff4",
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "center",
    padding: "0px 22px"
  },
  hrLine: {
    height: 1,
    background: "linear-gradient(to right, #ffffff, #e3e4e6, #e3e4e6, #ffffff)"
  },
  avatarWrapper: {
    overflowX: "scroll",
    whiteSpace: "nowrap",
    display: "flex",
    background: "#FFF",
    borderRadius: 3
  },
  avatarContainer: {
    display: "inline-block",
    padding: "20px 18px 18px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatarName: {
    fontFamily: "Montserrat",
    fontWeight: 400,
    fontSize: 10,
    color: "#141b2f",
    textAlign: "center",
    whiteSpace: "initial"
  },
  imageWrapper: {
    overflowX: "scroll",
    whiteSpace: "nowrap",
    display: "flex"
  },
  VideoContainer: {
    position: "relative",
    padding: "12px 0px 12px 16px"
  },
  imageStyle: {
    width: 296,
    height: 130
  },
  imageName: {
    // position: "absolute",
    bottom: -22,
    left: 22,
    right: 22,
    color: "#000000",
    fontSize: 10,
    fontFamily: "Montserrat",
    fontWeight: 600,
    whiteSpace: "initial",
    paddingTop: 10
  },
  reactPlayer: {
    position: "absolute",
    top: 0,
    left: 0
  }
};
