import ArrowDownward from "@material-ui/icons/ArrowDownward";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import React, { Component } from "react";
import { CartesianGrid, Line, LineChart, XAxis } from "recharts";
import { grey_10, grey_8, white } from "../../styleSheet/globalStyle/color";

export class LineGraph extends Component {
  constructor(props) {
    super(props);
    this.state = {
      low: "",
      high: "",
      drop: ""
    };
  }

  componentDidMount = () => {
    this.tooltip.style.display = "none";
  };

  UNSAFE_componentWillReceiveProps = nextProps => {
    if (!nextProps.isTooltipVisible) {
      this.tooltip.style.display = "none";
    }
  };

  showToolTip = val => {
    let topHeight = val.index % 2 === 0 ? 35 : -35;

    this.props.callBack(val);
    let x = Math.round(val.cx);
    let y = Math.round(val.cy);

    this.tooltip.style.display = "block";
    this.tooltip.style.position = "absolute";
    this.tooltip.style.top = `${+val.cy + topHeight}px`;
    this.tooltip.style.left = `${
      +val.width - +val.cx < 113 ? +val.width - 113 : +val.cx - 50
    }px`;
    this.setState({
      high: val.payload.perc,
      low: val.payload.diff,
      drop: val.payload.neg
    });
  };

  returnCustomLabel = val => {
    return val.payload.DiffPoint ? (
      <svg
        onClick={() => this.showToolTip(val)}
        key={Math.random()}
        x={val.cx - 10}
        y={val.cy - 10}
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        width="30"
        height="30"
        viewBox="0 0 30 30"
      >
        <defs>
          <circle id="b" cx="11" cy="11" r="11" />
          <filter
            id="a"
            width="163.6%"
            height="163.6%"
            x="-31.8%"
            y="-22.7%"
            filterUnits="objectBoundingBox"
          >
            <feOffset dy="2" in="SourceAlpha" result="shadowOffsetOuter1" />
            <feGaussianBlur
              in="shadowOffsetOuter1"
              result="shadowBlurOuter1"
              stdDeviation="2"
            />
            <feColorMatrix
              in="shadowBlurOuter1"
              values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25206635 0"
            />
          </filter>
          <linearGradient id="c" x1="100%" x2="0%" y1="50%" y2="50%">
            <stop offset="0%" stopColor="#EA6550" />
            <stop offset="100%" stopColor="#FA9441" />
          </linearGradient>
        </defs>
        <g fill="none" fillRule="nonzero" transform="translate(4 1.84)">
          <use fill="#000" filter="url(#a)" xlinkHref="#b" />
          <use fill="#FFF" xlinkHref="#b" />
          <circle cx="11" cy="11" r="5" fill="url(#c)" />
        </g>
      </svg>
    ) : null;
  };
  render() {

    let domain = [0,20];
    if(this.props.data) {
    
      // console.log("MOMENTUM SHIFT fix", this.props.data.length)
      domain = this.props.data.length >20?50:20;
    
    }
    return (
      <div style={{ position: "relative" }}>
        <LineChart
          width={300}
          height={300}
          margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
          data={this.props.data}
        >
          <Line
            dot={val => this.returnCustomLabel(val)}
            type="linear"
            dataKey={"perc"}
            stroke={this.props.color || "#8884d8"}
          />

          <CartesianGrid
            vertical={false}
            stroke="#d3d3d3"
            strokeDasharray="1 1"
          />
          <XAxis
            interval={"preserveStartEnd"}
            type={"number"}
            tickCount={5}
            stroke="#727682"
            dataKey={"overNo"}
            tickLine={false}
            axisLine={false}
            domain={domain}
          />
        </LineChart>
        <div className="ui-chart-tooltip" ref={ref => (this.tooltip = ref)}>
          <div
            className="ui-chart-tooltip-content"
            style={{
              display: "flex",
              background: "#fff",
              alignItems: "center"
            }}
          >
            <div
              style={{
                padding: "4px",
                background: grey_10,
                color: white,
                fontFamily: "mont500",
                border: `1px solid ${grey_10}`,
                fontSize: "12px",
                borderBottomLeftRadius: "3px",
                borderTopLeftRadius: "3px"
              }}
            >
              <p>{this.state.high}%</p>
            </div>
            <div
              style={{
                padding: "4px",
                border: `1px solid ${grey_8}`,
                borderLeft: "none",
                color: grey_8,
                background: white,
                fontFamily: "mont500",
                fontSize: "12px",
                borderBottomRightRadius: "3px",
                borderTopRightRadius: "3px"
              }}
            >
              <p>
                <span style={{ marginRight: 2 }}>{this.state.low}%</span>
                {this.state.drop ? (
                  <ArrowDownward
                    style={{ fontSize: "12px", verticalAlign: "middle" }}
                  />
                ) : (
                  <ArrowUpward
                    style={{ fontSize: "12px", verticalAlign: "middle" }}
                  />
                )}
                <span style={{ marginLeft: 2 }}>{this.props.currentTeam}</span>
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LineGraph;
