import React, { Component } from "react";
import { white, grey_10, cardShadow } from "../../styleSheet/globalStyle/color";
import HrLine from "./HrLine";

class NormalCard extends Component {
  render() {
    return (
      <div
        style={{
          background: white,
          margin: "0 16px 12px",
          borderRadius: 3,
          boxShadow: cardShadow
        }}
      >
        {/* title */}
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            padding: "12px 16px"
          }}
        >
          <div
            style={{
              color: grey_10,
              fontSize: 12,
              fontFamily: "Montserrat",
              fontWeight: 500
            }}
          >
            {this.props.title}
          </div>
          <div>
            <img
              src={require("../../images/right_arrow.svg")}
              alt="arrow"
              style={{
                width: 8
              }}
            />
          </div>
        </div>
        <HrLine />
        {/* title closing */}

        {/* Polling Section */}
        {/* {this.props.type == "polling" && <Polling />} */}
        {this.props.type == "text" && (
          <div
            style={{
              fontFamily: "Montserrat",
              fontWeight: 400,
              fontSize: 12,
              color: grey_10,
              padding: "12px 16px",
              lineHeight: 2
            }}
          >
            {this.props.content}
          </div>
        )}
      </div>
    );
  }
}

export default NormalCard;
