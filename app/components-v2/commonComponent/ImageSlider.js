import React, { PureComponent } from "react";
import Lightbox from "react-images";
const kohli = require("../../images/kohli.png");
const lyon = require("../../images/lyon_profile.png");
export class ImageSlider extends PureComponent {
  state = {
    isOpen: true
  };
  closeLightbox = () => this.setState({ isOpen: false });
  render() {
    return (
      <Lightbox
        images={[{ src: kohli }, { src: lyon }]}
        isOpen={this.state.lightboxIsOpen}
        onClickPrev={this.gotoPrevious}
        onClickNext={this.gotoNext}
        onClose={this.closeLightbox}
      />
    );
  }
}

export default ImageSlider;
