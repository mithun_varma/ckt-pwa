import React from "react";
import { Bar, BarChart, Cell, XAxis, YAxis } from "recharts";
const findMax = arr => {
  let max = arr[0].probabilities;
  arr.map((ele, ind) => {
    if (+arr[ind].probabilities > +max) {
      max = ele.probabilities;
    }
  });

  return max;
};

const labelRenderer = props => {
  const y = props.value > 85 ? -50 : 5;
  return (
    <text
      x={props.x + 25}
      y={props.y - y}
      fill="#141b2f"
      textAnchor={"central"}
      fontSize={15}
      fontFamily="Oswald"
      letterSpacing={0}
    >
      {`${props.value}%`}
    </text>
  );
};

export const BarGraph = props => {
  if (props.data === null) {
    return <div>No Data</div>;
  }
  return (
    <React.Fragment>
      <BarChart width={250} height={150} data={props.data}>
        <XAxis
          stroke="#727682"
          tickLine={false}
          axisLine={false}
          dataKey={"bound"}
        />

        <YAxis hide domain={[1, 100]} />
        <defs>
          <linearGradient id="splitColor" x1="0" y1="0" x2="0" y2="1">
            <stop offset={"0%"} stopColor="#d44030" stopOpacity={1} />
            <stop offset={"100%"} stopColor="#9b000d" stopOpacity={1} />
          </linearGradient>
        </defs>

        <Bar
          data={props.data}
          label={labelRenderer}
          barSize={150}
          background="#edeff4"
          dataKey={"probabilities"}
        >
          {props.data &&
            props.data.map((entry, index) => {
              return (
                <Cell
                  key={index}
                  fill={
                    entry.probabilities === findMax(props.data)
                      ? "url(#splitColor)"
                      : "#cccdd1"
                  }
                />
              );
            })}
        </Bar>
      </BarChart>
    </React.Fragment>
  );
};
