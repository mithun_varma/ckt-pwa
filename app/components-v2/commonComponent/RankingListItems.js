import React, { Component } from "react";
import {
  grey_8,
  gradientGrey,
  blue_grey,
  white,
  cardShadow
} from "../../styleSheet/globalStyle/color";
import HrLine from "./HrLine";
import ChevronRight from "@material-ui/icons/ChevronRight";

class RankingListItems extends Component {
  render() {
    const {
      rank,
      title,
      type,
      ranking,
      isHeader,
      isRightIcon,
      teamName
    } = this.props;
    return (
      <div>
        <div
          style={{
            display: "flex",
            flex: 1,
            padding: "16px 0 16px 10px",
            alignItems: "center",
            fontSize: 10,
            fontFamily: "Montserrat",
            color: grey_8,
            background: isHeader ? gradientGrey : white,
            maxHeight: 54
          }}
          onClick={e => {
            e.preventDefault();
            this.props.param && this.props.onClick(this.props.param);
          }}
        >
          <div style={{ flex: 0.1 }}>{rank}</div>
          <div
            style={{
              flex: type == "TEAMS" ? 0.68 : 0.56,
              display: "flex",
              alignItems: "center",
              paddingLeft: 6
            }}
          >
            {this.props.isAvatar && (
              <div
                style={{
                  display: "flex",
                  marginRight: 12,
                  boxShadow: cardShadow
                }}
              >
                <img
                  src={
                    // this.props.ranking && this.props.ranking.image ||
                    this.props.avatar || "http://via.placeholder.com/68x60.png?"
                  }
                  alt=""
                  style={{
                    width: 30,
                    height: 20
                    // borderRadius: "50%"
                  }}
                />
              </div>
            )}
            <div
              style={{
                fontFamily: "Montserrat",
                fontWeight: 500
                // fontSize: 14
              }}
            >
              {/* {this.props.ranking && this.props.ranking.name} */}
              {title}
            </div>
          </div>
          {type != "TEAMS" &&
            !isHeader && (
              <div
                style={{
                  flex: 0.12,
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center"
                }}
              >
                {/* {type} */}
                <div
                  style={{
                    boxShadow: cardShadow
                  }}
                >
                  <img
                    src={
                      // this.props.ranking && this.props.ranking.image ||
                      this.props.avatar || "https://via.placeholder.com/150"
                    }
                    alt=""
                    style={{
                      width: 30,
                      height: 20
                      // borderRadius: "50%"
                    }}
                  />
                </div>
                <div>{teamName}</div>
              </div>
            )}
          {type != "TEAMS" &&
            isHeader && (
              <div
                style={{
                  flex: 0.12,
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center"
                }}
              >
                {"TEAMS"}
              </div>
            )}
          <div style={{ flex: 0.13 }}>{ranking}</div>

          <div style={{ flex: 0.09, display: "flex" }}>
            {!isHeader && <ChevronRight style={{ color: blue_grey }} />}
          </div>
          {false && (
            <div
              style={{
                flex: 0.26,
                display: "flex",
                alignItems: "center",
                justifyContent: "flex-end"
              }}
            >
              {this.props.subtitle && (
                <div
                  style={{
                    textAlign: "right",
                    paddingRight: 14,
                    fontFamily: "Montserrat",
                    fontWeight: 500,
                    fontSize: 14
                  }}
                >
                  {/* {this.props.ranking && this.props.ranking.rating} */}
                  {this.props.subtitle}
                </div>
              )}

              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end"
                }}
              >
                {this.props.rightElement ? (
                  this.props.rightElement
                ) : (
                  <ChevronRight style={{ color: grey_8 }} />
                )}
              </div>
            </div>
          )}
        </div>
        <HrLine />
      </div>
    );
  }
}

export default RankingListItems;
