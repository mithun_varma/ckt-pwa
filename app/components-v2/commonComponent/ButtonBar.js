import React, { Component } from "react";
import {
  white,
  grey_2,
  grey_6,
  grey_1,
  red_Orange
} from "../../styleSheet/globalStyle/color";
import HrLine from "./HrLine";

class ButtonBar extends Component {
  render() {
    const { items, activeItem, isTrimValue, inningNumberArr } = this.props;
    return (
      <div
        style={{
          display: "flex",
          padding: "24px 0",
          position: "relative",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        {!this.props.hideHrLine &&
          <div
          style={{
            width: "100%"
          }}
          >
            <HrLine />
          </div>
        }

        <div
          className="buttonTab"
          style={{
            position: "absolute",
            // display: "inline-block",
            // margin: "0 auto",
            border: "1px solid #e3e4e6",
            // padding: "4px 0p",
            fontFamily: "Montserrat",
            fontWeight: 400,
            fontSize: 10,
            color: grey_6,
            background: grey_2,
            borderRadius: 2
          }}
        >
          {items.map((val, index) => (
            <div
              key={val}
              style={{
                display: "inline-block",
                padding:
                  this.props.increaseLastItemWidth && items.length - 1 == index
                    ? "8px 38px"
                    : "8px 12px",
                textTransform: "uppercase",
                borderRight: items.length - 1 > index && "1px solid #e3e4e6",
                background: val === activeItem ? white : grey_1,
                color: val === activeItem ? red_Orange : grey_6,
                fontWeight: val === activeItem ? "600" : "400",
                ...this.props.itemStyles
                // color: "red"
              }}
              onClick={e => {
                e.preventDefault();
                if (val !== activeItem) {
                  this.props.onClick(val);
                }
              }}
            >
              {/* {isTrimValue ? val.substring(0, isTrimValue).toUpperCase() : val} */}
              {isTrimValue
                ? val
                    .substring(0, val.length - 1)
                    .substring(0, isTrimValue)
                    .toUpperCase()
                : val}
              {`${this.props.showIndex ? " " + inningNumberArr[index] : ""}`}
              <span
                style={{
                  textTransform: "lowercase"
                }}
              >
                {`${
                  this.props.showIndex
                    ? inningNumberArr[index] == 1
                      ? "st"
                      : inningNumberArr[index] == 2
                        ? "nd"
                        : ""
                    : ""
                }`}
              </span>
            </div>
          ))}

          {/* <div
            style={{
              display: "inline-block",
              padding: "4px 20px",
              borderRight: "1px solid #e3e4e6"
            }}
          >
            TEST
          </div>
          <div
            style={{
              display: "inline-block",
              padding: "4px 20px",
              borderRight: "1px solid #e3e4e6"
            }}
          >
            ODI
          </div>
          <div style={{ display: "inline-block", padding: "4px 20px" }}>
            T20
          </div> */}
        </div>
      </div>
    );
  }
}

export default ButtonBar;
