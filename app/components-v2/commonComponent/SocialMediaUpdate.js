/* eslint-disable prettier/prettier */
/* eslint-disable no-undef */
import React, { Component } from 'react';
import { screenWidth } from '../../styleSheet/screenSize/ScreenDetails';

const widthScreen = window.innerWidth;
const heightScreen = window.innerHeight;

export default class PlayerSocial extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };

    }
    componentDidMount() {

    }
    render() {
        return (
            <div style={pageStyle.conatiner}>
                <div style={{ display: 'flex', flexDirection: 'row', flex: 1 }}>
                    <div style={pageStyle.socialHeading}>
                        Virat on Social
                    </div>
                    <div style={pageStyle.socialMenu}>
                        fb
                    </div>
                    <div style={pageStyle.socialMenu}>
                        tw
                    </div>
                    <div style={pageStyle.socialMenu}>
                        In
                    </div>

                </div>
                <div style={pageStyle.socialView}>
                    Data from social
                </div>

            </div>
        );
    }

}

const pageStyle = {
    conatiner: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        width:screenWidth*0.911,
        backgroundColor:'#fff',
        marginLeft:screenWidth*0.044,
        marginBottom:'50px',
    },
    socialHeading: {
        display: 'flex',
        flex: 0.60,
        justifyContent: 'flex-start',
        color: '#141b2f',
        fontSize: '12px',
        fontWeight: '700',
    },
    socialMenu: {
        display: 'flex',
        flex: 0.20,
        justifyContent: 'center',
        width: widthScreen * 0.18,
    },
    socialView: {
        display: 'flex',
        flex: 1,
        width: widthScreen * 0.90,
        height: heightScreen * 0.50,
    },

}