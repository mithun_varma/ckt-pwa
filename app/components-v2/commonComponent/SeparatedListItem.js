import React, { Component } from "react";
import { grey_10, grey_8 } from "../../styleSheet/globalStyle/color";

class SeparatedListItem extends Component {
  render() {
    return (
      <div style={{ ...pageStyle.listItemSingle, ...this.props.rootStyles }}>
        <div
          style={{
            ...pageStyle.listItemSingleLable,
            ...this.props.labelStyles
          }}
        >
          {this.props.lable}
        </div>
        <div style={pageStyle.listItemSingleValue}>{this.props.value}</div>
      </div>
    );
  }
}

export default SeparatedListItem;

const pageStyle = {
  listItemSingle: {
    display: "flex",
    flex: 1,
    padding: "12px 0",
    fontSize: 12,
    fontFamily: "Montserrat",
    fontWeight: 500
  },
  listItemSingleLable: {
    display: "flex",
    flex: 0.5,
    color: grey_8
  },
  listItemSingleValue: {
    display: "flex",
    flex: 0.5,
    paddingLeft: 10,
    color: grey_10
  }
};
