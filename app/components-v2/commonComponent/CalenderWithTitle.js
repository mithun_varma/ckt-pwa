/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import leftArrowOrange from './../../styleSheet/svg/leftArrowOrange.svg';
import rightArrowOrange from './../../styleSheet/svg/rightArrowOrange.svg';
import HomeScoreCard from './../screenComponent/teamProfile/HomeScoreCardTeam';
import Modal from 'react-responsive-modal';
import {BASE_APP_URL} from './../../utils/requestUrls';
let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  ClevertapReact.initialize("W88-4KR-845Z");
}

// const  BASE_APP_URL ='https://qaapi.crictec.com/api'
const widthScreen = "320";
const screenWidth = widthScreen;
const heightScreen = "640";

const dateArray = [31, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 23, 24, 25, 26, 27, 28, 29, 30, 31, 1, 2];
// defining the day
const day = new Array();
    day[1] = 'Sunday';
    day[2] = "Monday";
    day[3] = 'Tuesday';
    day[4] = 'Wednesday';
    day[5] = 'Thursday';
    day[6] = 'Friday';
    day[7] = 'Saturday';
// defining the day ends
const  month = new Array();
    month[1] = "JAN";
    month[2] = "FEB";
    month[3] = "MAR";
    month[4] = "APR"; 
    month[5] = "MAY";
    month[6] = "JUN";
    month[7] = "JUL";
    month[8] = "AUG";
    month[9] = "SEP";
    month[10] = "OCT";
    month[11] = "NOV";
    month[12] = "DEC";
const daysInMonthNonLeap =[
    {
        1:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
        2:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28],
        3:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
        4:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
        5:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
        6:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
        7:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
        8:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
        9:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
        10:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
        11:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
        12:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
    }
];
const daysInMonthLeap=[
    {
        1:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
        2:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,29],
        3:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
        4:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
        5:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
        6:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
        7:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
        8:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
        9:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
        10:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
        11:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
        12:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
    }
]
export default class Calender extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dateArray: [],
            currentYear:'',
            currentMonth:'',
            currentMonthDigit:'',
            todayDate:'',
            todayDay:'',
            todayDateDigit:'',
            firstDayOfCurrentMonth:'',
            odiMatchDetails:[],
            month:'',
            year:'',
            //show mini score card
            showMiniScoreCard:false,
            scoreCardData:[],
             //this is used to highlight the today date
             todayDateToHighLight:"",
             todayMonthToHighLight:"",
             todayYearToHighLight:"",
        }

        this._handelLeftClick = this._handelLeftClick.bind(this);
        this._handelRightClick = this._handelRightClick.bind(this);

        this._handleCloseModal=this._handleCloseModal.bind(this);
    

    }
    
    _handelCalender(){
        var mm = this.state.currentMonthDigit;
        var yyyy = this.state.currentYear;
        this.setState({
        month:mm,
        year:yyyy,
        }, ()=>{
            // console.log(this.state.year,this.state.month);
            var year = this.state.year;
            var  month = "0"+this.state.month;
            var date =`${year}-${month}-01`;
            // console.log("calender date -----",date);
            this.props.fetchTeamSchedule(date);
            
        }
        );
        
    }
       
    componentDidMount() {
        // console.log(BASE_APP_URL,'BASE_APP_URL------------')
        var currentDate = new Date(); // reading today date

        const todayDay = day[currentDate.getDay()+1];
        this.setState({todayDay:todayDay});
        var todayDate = currentDate.getDate();
        this.setState({todayDate});
        this.setState({todayDateToHighLight:todayDate})

        // console.log("---today day---- today date----",todayDay,todayDate);
      
        const currentYear = currentDate.getFullYear();
        this.setState({todayYearToHighLight:currentYear});

        const currentMonth = month[currentDate.getMonth()+1];
        this.setState({todayMonthToHighLight:currentMonth}); //highlight month
        const currentMonthDigit = currentDate.getMonth()+1;

        this.setState(
            {
                currentMonthDigit,
                currentMonth,
                currentYear,

            },
            ()=>{
                const firstDayOfCurrentMonth = new Date(this.state.currentYear, this.state.currentMonthDigit-1, 1).getDay();
                // console.log(firstDayOfCurrentMonth,'cudsbfajkkv----saCMAB');
                this.setState({firstDayOfCurrentMonth},()=>{
                    this.setDateForCurrentMonth();
                    this._handelCalender();
                }); 
                
            }
        );
    }
    setDateForCurrentMonth(){
        const year = this.state.currentYear;
        // non leap year 
        if(year%4 != 0){
            const firstDayOfCurrentMonth = new Date(this.state.currentYear, this.state.currentMonthDigit-1, 1).getDay();
            this.setState({firstDayOfCurrentMonth},
                ()=>{
                    //starting setting date
                    const currentMonthDigit = this.state.currentMonthDigit;
                    var resultArray =[];
                    if(this.state.firstDayOfCurrentMonth>0){
                        var previousMonth="";
                        if(currentMonthDigit == 1){
                            previousMonth = daysInMonthNonLeap[0][12];
                        }
                        else{
                            previousMonth = daysInMonthNonLeap[0][currentMonthDigit-1];
    
                        }
                        for(let i = this.state.firstDayOfCurrentMonth ; i >= 2 ;i-- ){
                            resultArray.push(previousMonth[previousMonth.length-i+1])
                        }
                    }
                    // console.log('current month digit------------',currentMonthDigit)
                    const currentArray = daysInMonthNonLeap[0][currentMonthDigit];
                    for(let j = 0; j<currentArray.length;j++){
                        // console.log(currentArray[j]);
                        resultArray.push(currentArray[j]);
                    }
                    this.setState({dateArray:resultArray});
                    //ending setting date
                }
            );
        }
        else{
            const firstDayOfCurrentMonth = new Date(this.state.currentYear, this.state.currentMonthDigit-1, 1).getDay();
            this.setState({firstDayOfCurrentMonth},
                ()=>{
                    //starting setting date
                    const currentMonthDigit = this.state.currentMonthDigit;
                    var resultArray =[];
                    if(this.state.firstDayOfCurrentMonth>0){
                        var previousMonth="";
                        if(currentMonthDigit == 1){
                            previousMonth = daysInMonthLeap[0][12];
                        }
                        else{
                            previousMonth = daysInMonthLeap[0][currentMonthDigit-1];
    
                        }
                        for(let i = this.state.firstDayOfCurrentMonth ; i >= 2 ;i-- ){
                            resultArray.push(previousMonth[previousMonth.length-i+1])
                        }
                    }
                    // console.log('current month digit------------',currentMonthDigit)
                    const currentArray = daysInMonthLeap[0][currentMonthDigit];
                    for(let j = 0; j<currentArray.length;j++){
                        // console.log(currentArray[j]);
                        resultArray.push(currentArray[j]);
                    }
                    this.setState({dateArray:resultArray});
                    //ending setting date
                }
            );
        }
       
    }
    _handelLeftClick(){
        // console.log('previous month button clicked');
        let currentYear = this.state.currentYear;
        currentYear =  parseInt(currentYear, 10);
        let currentMonthDigit = this.state.currentMonthDigit;
        currentMonthDigit = parseInt(currentMonthDigit,10);
        

        if(this.state.currentMonthDigit == 1 ){
            this.setState({
                currentMonthDigit:12,
                currentYear :currentYear-1
            },
            ()=>{
                // console.log(this.state.currentMonthDigit,this.state.currentYear);
                const currentMonth = month[this.state.currentMonthDigit];
                this.setState({currentMonth});
                this.setDateForCurrentMonth();
                this._handelCalender();
            })
        }
        else{
            this.setState({currentMonthDigit: currentMonthDigit-1}
                ,()=>{
                    // console.log("came at hert of left click",this.state.currentMonthDigit);
                    const currentMonth = month[this.state.currentMonthDigit];
                    this.setState({currentMonth});
                    this.setDateForCurrentMonth();
                    this._handelCalender();

                   
                }
            )
        }

    }
    _handelRightClick(){
        // console.log('next month button clicked');

        if(this.state.currentMonthDigit == '11'){
            this.setState({
                currentMonthDigit:1,
                currentYear :this.state.currentYear+1
            },()=>{
                this.setDateForCurrentMonth(); 
                const currentMonth = month[this.state.currentMonthDigit];
                this.setState({currentMonth});
                this.setDateForCurrentMonth();
                this._handelCalender();
            })
        }
        else{
            this.setState({currentMonthDigit: this.state.currentMonthDigit+1},
                ()=>{
                    const currentMonth = month[this.state.currentMonthDigit];
                    this.setState({currentMonth});
                    this.setDateForCurrentMonth();
                    this._handelCalender();
                })
        }
    }
    render() {
        return (
            <div style={{ marginLeft: widthScreen * 0.04 }}>
                <div>
                    {this.calenderTitle()}
                </div>
                <div>
                    {
                        (this.state.showMiniScoreCard) ?
                        <div>
                            {this.showMiniScoreCard()}
                        </div>
                        :
                        <div>
                            {null}
                        </div>
                    }
                    
                </div>
                
            </div>
        );
    }
    showMiniScoreCard(){
        const rawData=this.state.scoreCardData;
        if(rawData && rawData.data && rawData.data['fullScorecard'] ){
            const data = rawData.data['fullScorecard']['matchMiniScorecard'];
            return(
                <div style={{zIndex:'9999'}} onClick={() => {
                    data

                    ClevertapReact.event("Teams", {
                      source: "Teams",
                      matchId: data.matchId
                    });

                    this.props.history.push(`/score/${data.matchId}`)
                    this._handleCloseModal()
                }}>
                    <Modal open={true} closeIconSize ={'20'} showCloseIcon={true} onClose={this._handleCloseModal} center style={pageStyle.trophyCabinetContainer}>
                        <HomeScoreCard score={data} />
                    </Modal>
                </div>
            );
        }
        else{
            return(null
                // <div style={{zIndex:'9999'}}>
                //     NO data found for Team
                // </div>
            );
        }
    }
    _handleCloseModal(event){
        this.setState({showMiniScoreCard:false});
        event.stopPropagation();
    }
    calenderTitle() {
    var matchDetails =[];
    if(this.props.teamSchedule.matches != null){
            var matches = this.props.teamSchedule.matches;
        
        for(let i=0 ;i <matches.length;i++){
            if(matches[i].format == 'ODI'){
                // reading the T and trying to fing out the date
                const rawDate = matches[i].startDate;

                const posT = rawDate.indexOf('T');
                const posLast = rawDate.lastIndexOf("-");
                const dateString = rawDate.substring(posLast+1,posT);
                const statusStr = matches[i].statusStr;
                const matchId = matches[i].matchId;
                matchDetails.push({'type':'ODI','startDate':dateString, 'seriesName':matches[i].seriesName,'statusStr':statusStr,'matchId':matchId});
            }
            else if (matches[i].format == 'TEST'){
                const rawDate = matches[i].startDate;
                const posT = rawDate.indexOf('T');
                const posLast = rawDate.lastIndexOf("-");
                const dateString = rawDate.substring(posLast+1,posT);
                
                const statusStr = matches[i].statusStr;
                const matchId = matches[i].matchId;
                
                matchDetails.push({'type':'TEST','startDate':dateString, 'seriesName':matches[i].seriesName,'statusStr':statusStr,'matchId':matchId});

            }
            else if ( matches[i].format == 'T20'){
                const rawDate = matches[i].startDate;
                const posT = rawDate.indexOf('T');
                const posLast = rawDate.lastIndexOf("-");
                const dateString = rawDate.substring(posLast+1,posT);
            
                const statusStr = matches[i].statusStr;
                const matchId = matches[i].matchId;
                matchDetails.push({'type':'T20','startDate':dateString, 'seriesName':matches[i].seriesName,'statusStr':statusStr,'matchId':matchId});

            }

            // console.log("matchDetails------------>",matchDetails)
            
        }


        if(this.state.dateArray != null ){
            return (
                <div style={pageStyle.calenderContainer}>
                    <div style={pageStyle.calenderTitle}>
                        <div style={pageStyle.calenderTypeName}>
                            SCHEDULE
                        </div>
                        <div style={pageStyle.calenderTypeDate}>
                            <div style={{ display: 'flex', flexDirection: 'row', flex: 1 }}>
                                <div style={{ display: 'flex',  justifyContent:'flex-start',marginRight:'10px', color: '#ea6550',cursor:'pointer'}}>
                                    <img src={leftArrowOrange} alt="leftArrow"  onClick={this._handelLeftClick}/>
                                </div>
                                <div style={{ display: 'flex', flex: 0.96, color: '#141b2f',justifyContent:'flex-start' }} />
                                    {this.state.currentMonth+ ' - '+ this.state.currentYear}
                                </div>
                                <div style={{ display: 'flex' ,justifyContent:'flex-start',marginLeft:'10px', color: '#ea6550',cursor:'pointer'}}>
                                    <img src={rightArrowOrange} alt="rightArrow" onClick={this._handelRightClick}/>
                                </div>
                            </div>
                        </div>
                    <div style={pageStyle.calenderTitleBottomBorder}>
                    </div>
                    <div style={pageStyle.calenderDaysContainer}>
                        <div style={pageStyle.calenderDays}>
                            MON
                        </div>
                        <div style={pageStyle.calenderDays}>
                            TUE
                        </div>
                        <div style={pageStyle.calenderDays}>
                            WED
                        </div>
                        <div style={pageStyle.calenderDays}>
                            THU
                        </div>
                        <div style={pageStyle.calenderDays}>
                            FRI
                        </div>
                        <div style={pageStyle.calenderDays}>
                            SAT
                        </div>
                        <div style={pageStyle.calenderDays}>
                            SUN
                        </div>
                    </div>
                    <div style={pageStyle.calenderDays}>
                        {
                            (this.state.dateArray).map((date,i) => {
                                
                                    {  

                                         //setting for highlighting dates
                                         const dateHighlight = this.state.todayDateToHighLight;
                                         const monthHighlight = this.state.todayMonthToHighLight;
                                         const yearHighlight = this.state.todayYearToHighLight;
                                         //
                                         const currentMonth =this.state.currentMonth;
                                         const currentYear = this.state.currentYear;

                                        var letterStyle ={fontSize:'8px'}
                                        var calenderBlockStyle = pageStyle.calenderDaysBlockNormal;

                        
                                        if(currentYear == yearHighlight && currentMonth == monthHighlight && date ==dateHighlight){
                                            calenderBlockStyle  = pageStyle.calenderDaysBlockNormalHighlight;
                                            //  console.log(currentYear,yearHighlight,currentMonth,monthHighlight,date,dateHighlight,"================")
                                        }
                                        for(let i = 0; i<matchDetails.length ; i++){
                                            if(matchDetails[i].startDate == date){
                                                if(matchDetails[i].type === 'ODI'){
                                                    calenderBlockStyle = pageStyle.calenderDaysBlockODI;
                                                    letterStyle={fontSize:'8px', color:'#d64b4b'}
                                                    if(currentYear == yearHighlight && currentMonth == monthHighlight && date ==dateHighlight){
                                                        // console.log(currentYear,yearHighlight,currentMonth,monthHighlight,date,dateHighlight,"================")
                                                        calenderBlockStyle  = pageStyle.calenderDaysBlockODIHighlight;
                                                    }
                                                }
                                                else if( matchDetails[i].type === 'TEST'){
                                                    calenderBlockStyle = pageStyle.calenderDaysBlockTest;
                                                    letterStyle={fontSize:'8px', color:'#4a90e2'}
                                                    if(currentYear == yearHighlight && currentMonth == monthHighlight && date ==dateHighlight){
                                                        calenderBlockStyle  = pageStyle.calenderDaysBlockTestHighlight;
                                                    }

                                                }
                                                else if( matchDetails[i].type === 'T20'){
                                                    calenderBlockStyle = pageStyle.calenderDaysBlockT20;
                                                    letterStyle={fontSize:'8px',color:'#35a863'}
                                                    if(currentYear == yearHighlight && currentMonth == monthHighlight && date ==dateHighlight){
                                                        calenderBlockStyle  = pageStyle.calenderDaysBlockT20Highlight;
                                                    }

                                                }
                                            }
                                            var vsString ="";
                                            var matchId="";
                                            for(let i =0 ; i<matchDetails.length ;i++){
                                                const dateFromMatchDetails = matchDetails[i].startDate;
                                                if(date == dateFromMatchDetails){
                                                    vsString = matchDetails[i].statusStr
                                                    matchId = matchDetails[i].matchId;
                                                }
                                            }
                                        }

                                        return(
                                            <div  id={matchId} onClick={(event)=>{this._handelDayClick(event)}}>
                                                {
                                                    (i+1<this.state.firstDayOfCurrentMonth)?
                                                    <div style={pageStyle.calenderDaysBlockNormal}>
                                                        <div style={{color:'#a1a4ac'}}>
                                                            {date}
                                                        </div>
                                                    </div>
                                                    :
                                                    <div style={calenderBlockStyle}>
                                                        <div >
                                                            <div id={matchId}>
                                                                {date}
                                                            </div>
                                                            <div id={matchId}>
                                                            {
                                                                (vsString)?
                                                                <div style={letterStyle} id={matchId}>
                                                                    {vsString}
                                                                </div>
                                                                :
                                                                <div>
                                                                    {null}
                                                                </div>
                                                            }
                                                            </div>
                                                        </div>
                                                    </div>

                                                }
                                            </div>
                                        );
                                    }

                                })
                        }
                    </div>
                    <div style={{ height: '1px', display: 'flex' }}>

                    </div>
                    <div style={pageStyle.calenderRepresentation}>
                        <div style={{ display: 'flex', flex: 0.10 }}>
                        </div>
                        <div style={pageStyle.calenderRepresentationType}>
                            <div style={{ marginBottom: '4px', display: 'flex', borderRadius: '3px', height: '6px', width: '6px', border: '1px solid #4a90e2', backgroundColor: '#4a90e2' }}>
                                {null}
                            </div>
                            <div style={{ display: 'flex', paddingLeft: '10px' }}>
                                TEST
                            </div>
                        </div>
                        <div style={pageStyle.calenderRepresentationType}>
                            <div style={{ marginBottom: '4px', display: 'flex', borderRadius: '3px', height: '6px', width: '6px', border: '1px solid #d64b4b', backgroundColor: '#d64b4b' }}>

                            </div>
                            <div style={{ display: 'flex', paddingLeft: '10px' }}>
                                ODIs
                            </div>
                        </div>
                        <div style={pageStyle.calenderRepresentationType}>
                            <div style={{ marginBottom: '4px', display: 'flex', borderRadius: '3px', height: '6px', width: '6px', border: '1px solid #35a863', backgroundColor: '#35a863' }}>

                            </div>
                            <div style={{ display: 'flex', paddingLeft: '10px' }}>
                                T20
                            </div>
                        </div>
                        <div style={{ display: 'flex', flex: 0.10 }}>
                        </div>
                    </div>
                </div >
            );
        }
        else{
            return(null);
        }
    }
    else{
        return(null);
    }
    
    }
    _handelDayClick(event){
        const matchId = event.target.id;
        if(matchId) {
            this.apiCallFullScoreCard(matchId);
        }
       
    }
    apiCallFullScoreCard(matchId){
        const self =this;
        const url = `${BASE_APP_URL}scorecards/${matchId}?cardType=Full`;
        //console.log(BASE_APP_URL)
    
              return fetch(url, {
                  method: "GET",
                  headers: {
                      "Content-Type": "application/json",
                  }
              })
              .then(data => data.json())
              .then(data =>{
                self.setState({scoreCardData:data},
                    ()=>{
                        self.setState({showMiniScoreCard:true});
                    }
                );
              }
              )
            .catch((err)=>{});
          


    }

}

const pageStyle = {
    calenderRepresentationType: {
        display: 'flex',
        flexDirection: 'row',
        flex: 0.15,
        color: '#727682',
        justifyContent: 'center',
        fontSize: '10px',
        alignItems: 'flex-end',

    },
    calenderRepresentation: {
        display: 'flex',
        flexDirection: 'row',
        height: heightScreen * 0.03,
        justifyContent: 'space-around',
        alignItems: 'center',
        // marginTop: heightScreen * 0.36,
        paddingTop:'12px',
        paddingBottom:'12px'

    },
    calenderDaysBlockNormal: {
        display: 'flex',
        height: widthScreen * 0.145,
        width: widthScreen * 0.145,
        padding: '4px',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        color: '#141b2f',
        borderRight: '1px solid #e3e4e6',
        borderBottom: '1px solid #e3e4e6',
        fontSize: '12px',
        fontWeight:'300',
    },
    calenderDaysBlockNormalHighlight: {
        display: 'flex',
        height: widthScreen * 0.145,
        width: widthScreen * 0.145,
        padding: '4px',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        color: '#141b2f',
        borderRight: '1px solid #e3e4e6',
        borderBottom: '1px solid #e3e4e6',
        fontSize: '12px',
        fontWeight:'700',
    },
    calenderDaysBlockODI: {
        display: 'flex',
        height: widthScreen * 0.145,
        width: widthScreen * 0.145,
        padding: '4px',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        color: '#141b2f',
        borderRight: '1px solid #e3e4e6',
        borderBottom: '1px solid #e3e4e6',
        fontSize: '12px',
        backgroundColor:'#f5d4d4',
        fontWeight:'300',

    },
    calenderDaysBlockODIHighlight: {
        display: 'flex',
        height: widthScreen * 0.145,
        width: widthScreen * 0.145,
        padding: '4px',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        color: '#141b2f',
        borderRight: '1px solid #e3e4e6',
        borderBottom: '1px solid #e3e4e6',
        backgroundColor:'#f5d4d4',
        fontSize: '12px',
        fontWeight:'700',

    },
    calenderDaysBlockTest: {
        display: 'flex',
        height: widthScreen * 0.145,
        width: widthScreen * 0.145,
        padding: '4px',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        color: '#141b2f',
        borderRight: '1px solid #e3e4e6',
        borderBottom: '1px solid #e3e4e6',
        fontSize: '12px',
        backgroundColor:'#d4e5f7',
        fontWeight:'300',
    },
    calenderDaysBlockTestHighlight: {
        display: 'flex',
        height: widthScreen * 0.145,
        width: widthScreen * 0.145,
        padding: '4px',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        color: '#141b2f',
        borderRight: '1px solid #e3e4e6',
        borderBottom: '1px solid #e3e4e6',
        fontSize: '12px',
        backgroundColor:'#d4e5f7',
        fontWeight:'700',
    },
    calenderDaysBlockT20: {
        display: 'flex',
        height: widthScreen * 0.145,
        width: widthScreen * 0.145,
        padding: '4px',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        color: '#141b2f',
        borderRight: '1px solid #e3e4e6',
        borderBottom: '1px solid #e3e4e6',
        fontSize: '12px',
        backgroundColor:'#cfeada',
        fontWeight:'300',
    },
    calenderDaysBlockT20Highlight: {
        display: 'flex',
        height: widthScreen * 0.145,
        width: widthScreen * 0.145,
        padding: '4px',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        color: '#141b2f',
        borderRight: '1px solid #e3e4e6',
        borderBottom: '1px solid #e3e4e6',
        fontSize: '12px',
        backgroundColor:'#cfeada',
        fontWeight:'700',
    },
    calenderDays: {
        display: 'flex',
        flex: 0.16,
        justifyContent: 'flex-start',
        color: '#727682',
        fontSize: '8px',
        alignItems: 'center',
        flexWrap: 'wrap',
        fontFamily:'Montserrat',
        fontWeight:'300',

    },
    calenderDaysContainer: {
        display: 'flex',
        flexDirection: 'row',
        backgroundImage: 'linear-gradient(to right, #ffffff, #f0f1f2, #f0f1f2, #f0f1f2, #ffffff',
        height: heightScreen * 0.05,
        paddingLeft:'5px',
    },
    calenderTypeName: {
        fontWeight: '500',
        display: 'flex',
        flexDirection: 'row',
        flex: 0.65,
        color: '#141b2f',
        justifyContent: 'flex-start',
        fontSize: '12px',
    },
    calenderTypeDate: {
        display: 'flex',
        flexDirection: 'row',
        flex: 0.36,
        justifyContent: 'flex-start',
        fontSize: '12px',
        letterSpacing: '0.6px',
        alignItems:'center',
    },
    calenderTitle: {
        display: 'flex',
        flexDirection: 'row',
        // height: heightScreen * .07,
        backgroundColor: '#ffffff',
        padding:'8px 16px 8px 16px',
        borderRadius: "3px 3px 0 0"
    },
    calenderTitleBottomBorder: {
        display: 'flex',
        backgroundImage: 'linear-gradient(to right, #ffffff,#e3e4e6, #e3e4e6,#e3e4e6, #e3e4e6,#ffffff)',
        height: '1px',
    },
    calenderContainer: {
        // width: widthScreen * 0.911,
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        boxShadow: '0px 3px 6px 0px #13000000',
        backgroundColor: '#ffffff',
        borderRadius: '3px',
        fontFamily: 'Montserrat'
    },
    trophyCabinetContainer:{
        display:'flex',
        flex:1,
        flexDirection:'column',
        // width:screenWidth*0.911,
        backgroundColor:'#fff',
    },


}
