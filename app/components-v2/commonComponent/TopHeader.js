//manas
import React,  { Component } from 'react';
import { screenWidth, screenHeight } from '../../styleSheet/screenSize/ScreenDetails';
import { gradientOrange } from './../../styleSheet/globalStyle/color';
import  backIconBlack  from './../../styleSheet/svg/backIconBlack.svg';
import backIconWhite  from './../../styleSheet/svg/backIconWhite.svg';
import searchGlassWhite from './../../styleSheet/svg/searchGlassWhite.svg';
import threeDot from './../../styleSheet/svg/threeDot.svg';

export default  class TopHeader extends Component{
    constructor(props){
        super(props);
        this.state={
            modifyHeader:false,

        }
        this._handelBackClick = this._handelBackClick.bind(this);
        this.handleScroll = this.handleScroll.bind(this);
    }
    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll,true);
    }
    
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll(event) {
        var element = document.getElementById('header');
        var topPos = element.getBoundingClientRect().top + window.scrollY;
        var elementNew = document.getElementById('headerPseudo');
        var topPos1 = elementNew.getBoundingClientRect().top + window.scrollY;
        if(topPos<=0){
            this.setState({modifyHeader:true});
        }
        else if(topPos1==0){
           this.setState({modifyHeader:false},()=>{this.forceUpdate()});
        }
        
    }

    render(){
        const history = this.props;
        const type =this.props.type;
        return(
            <div onScroll={this._handelScroll} >
                <div>
                {
                    (type==='black')?
                        <div>
                            {
                                (this.state.modifyHeader)?
                                <div>
                                    {this.blackBackButtonTitle()}
                                </div>
                                :
                                <div>
                                    {this.blackBackButton()}
                                </div>
                            }
                        </div>
                        :
                        <div>
                            {
                                (this.state.modifyHeader)?
                                <div>
                                    {this.whiteBackButtonTitle()}
                                </div>
                                :
                                <div>
                                    {this.whiteBackButton()}
                                </div>
                                
                                
                            }
                        </div>
                }
                </div>
                <div id={"headerPseudo"}>

                </div>
               

            </div>
        );
    }
    blackBackButton(){
        return(
            <div style={pageStyle.TopHeaderContainerBlack}>
                <div style={pageStyle.iconsStyleConatiner} id={"header"}>
                    <div style={{display:'flex', flex:0.50, justifyContent:'flex-start'}}>
                        <img src={backIconBlack} width="16px" height="16px"  onClick={this._handelBackClick}/>
                    </div>
                    <div style={{display:'flex', flex:0.50, justifyContent:'flex-end', marginRight:screenWidth*0.044}}>
                        <img src={threeDot} width="17.5px" height="17.5px" />
                    </div>
                </div>
                <div  style={pageStyle.tittleStyleConatinerBlack} >
                    {this.props.title}
                </div>

            </div>
        );
    }
    blackBackButtonTitle(){
        return(
        <div style={pageStyle.TopHeaderContainerBlackTitle}>
            <div style={pageStyle.iconsStyleConatinerTitle} >
                <div style={{display:'flex', flex:0.05, justifyContent:'flex-start'}}>
                    <img src={backIconBlack} width="16px" height="16px"  onClick={this._handelBackClick}/>
                </div>
                <div  style={pageStyle.tittleStyleConatinerBlackCenter} id={"header"}>
                    {this.props.title}
                 </div>
                <div style={{display:'flex', flex:0.05, justifyContent:'flex-end', marginRight:screenWidth*0.044}}>
                    <img src={threeDot} width="17.5px" height="17.5px" />
                </div>
            </div>
           
        </div>
        )

    }
    whiteBackButton(){
        return(
            <div style={pageStyle.TopHeaderContainer}>
                <div style={pageStyle.iconsStyleConatiner} id={"header"} >
                    <div style={{display:'flex', flex:0.50, justifyContent:'flex-start'}}>
                        <img src={backIconWhite} width="16px" height="16px"  onClick={this._handelBackClick}/>
                    </div>
                    <div style={{display:'flex', flex:0.50, justifyContent:'flex-end', marginRight:screenWidth*0.044}}>
                        <img src={searchGlassWhite} width="17.5px" height="17.5px" />
                    </div>
                </div>
                <div style={pageStyle.tittleStyleConatiner}>
                    {this.props.title}
                </div>

            </div>
        );
    }
    whiteBackButtonTitle(){
        return(
            <div style={pageStyle.TopHeaderContainerTitle}>
                <div style={pageStyle.iconsStyleConatinerTitle} >
                    <div style={{display:'flex', flex:0.05, justifyContent:'flex-start'}}>
                        <img src={backIconWhite} width="16px" height="16px"  onClick={this._handelBackClick}/>
                    </div>
                    <div style={pageStyle.tittleStyleConatinerCenter} id={"header"} >
                        {this.props.title}
                    </div>
                    <div style={{display:'flex', flex:0.05, justifyContent:'flex-end', marginRight:screenWidth*0.044}}>
                        <img src={searchGlassWhite} width="17.5px" height="17.5px" />
                    </div>
                </div>
               

            </div>
        );
    }
    _handelBackClick(event){
        this.props.history.goBack();

    }
}
const pageStyle={
    tittleStyleConatinerCenter:{
        display:'flex',
        flex:0.90,
        flexDirection:'row',
        color:'#ffffff',
        fontSize:'22px',
        fontFamily:'Montserrat-Bold,sans-serif',
        margin:'-8px  0px 16px 16px',

    },
    TopHeaderContainerTitle:{
        position:'absolute',
        zIndex:'999',
        display:'flex',
        flex:1,
        flexDirection:'column',
        width:screenWidth,
        height:screenHeight * 0.078,
        backgroundImage: gradientOrange,  
        paddingTop:'15px',

    },
    TopHeaderContainerBlackTitle:{
        position:'absolute',
        zIndex:'999',
        display:'flex',
        flex:1,
        flexDirection:'column',
        width:screenWidth,
        height:screenHeight * 0.078,
        backgroundColor:'#edeff4', 
        paddingTop:'15px',

    },
    tittleStyleConatiner:{
        display:'flex',
        flex:1,
        flexDirection:'row',
        margin:'-50px 0px 16px 16px',
        color:'#ffffff',
        fontSize:'22px',
        fontFamily:'Montserrat-Bold,sans-serif'

    },
    tittleStyleConatinerBlack:{
        display:'flex',
        flex:1,
        flexDirection:'row',
        margin:'-50px 0px 16px 16px',
        color:'#141b2f',
        fontSize:'22px',
        fontFamily:'Montserrat-Bold,sans-serif'

    },
    tittleStyleConatinerBlackCenter:{
        display:'flex',
        flex:0.90,
        justifyContent:'flex-start',
        flexDirection:'row',
        margin:'-8px  0px 16px 16px',
        color:'#141b2f',
        fontSize:'22px',
        fontFamily:'Montserrat-Bold,sans-serif'

    },
    iconsStyleConatiner:{
        display:'flex',
        flex:1,
        flexDirection:'row',
        margin:'16px 0px 0px 16px',
    },
    iconsStyleConatinerTitle:{
        display:'flex',
        flex:1,
        flexDirection:'row',
        margin:'0px 0px 0px 16px',

    },
    TopHeaderContainer:{
        display:'flex',
        flex:1,
        flexDirection:'column',
        width:screenWidth,
        height:screenHeight * 0.225,
        backgroundImage: gradientOrange,       
    },
    TopHeaderContainerBlack:{
       
        display:'flex',
        flex:1,
        flexDirection:'column',
        width:screenWidth,
        height:screenHeight * 0.225,
        backgroundColor:'#edeff4',
        // backgroundImage: gradientGrey,       
    },

}
