import React, { Component } from "react";
import {
  white,
  grey_10,
  cardShadow,
  grey_8,
  gradientOrangeToBottom,
  grey_6
} from "../../styleSheet/globalStyle/color";
import HrLine from "./HrLine";

class PlayerPerformance extends Component {
  render() {
    return (
      <div
        style={{
          background: white,
          margin: "16px 16px",
          borderRadius: 3,
          boxShadow: cardShadow
        }}
      >
        {/* title */}
        <div
          style={{
            display: "flex",
            flex: 1,
            // justifyContent: "space-between",
            alignItems: "center",
            padding: "12px 16px"
          }}
        >
          <div>
            <img
              src={require("../../images/analytics.svg")}
              alt="arrow"
              style={{
                width: 20
              }}
            />
          </div>
          <div
            style={{
              color: grey_10,
              fontSize: 12,
              fontFamily: "Montserrat",
              fontWeight: 500,
              marginLeft: 20
            }}
          >
            Projected Player Performance
          </div>
        </div>
        <HrLine />
        {/* title closing */}
        {/* body */}

        <div
          style={{
            padding: "20px 14px 0px"
          }}
        >
          {/* status */}
          <div
            style={{
              display: "flex",
              alignItems: "center"
            }}
          >
            <div
              style={{
                flex: 0.16
              }}
            >
              <div
                style={{
                  width: 33,
                  height: 33,
                  borderRadius: 8,
                  border: `1px solid ${grey_6}`,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <img
                  src={require("../../images/ball_orange.png")}
                  alt=""
                  style={{
                    width: 16,
                    height: 16
                  }}
                />
              </div>
            </div>
            <div
              style={{
                flex: 0.5
              }}
            >
              <div
                style={{
                  fontFamily: "Montserrat",
                  fontWeight: 500,
                  fontSize: 14,
                  color: grey_10
                }}
              >
                Ravindra Jadeja
              </div>
              <div
                style={{
                  color: grey_8,
                  fontFamily: "Montserrat",
                  fontWeight: 400,
                  fontSize: 12
                }}
              >
                India
              </div>
            </div>
            <div
              style={{
                flex: 0.34
              }}
            >
              <div
                style={{
                  fontFamily: "Montserrat",
                  fontWeight: 500,
                  fontSize: 14,
                  color: grey_10
                }}
              >
                4/35
              </div>
              <div
                style={{
                  color: grey_8,
                  fontFamily: "Montserrat",
                  fontWeight: 400,
                  fontSize: 12
                }}
              >
                (5.3)
              </div>
            </div>
          </div>

          {/* Chart */}
          <div
            style={{
              display: "flex",
              height: 164,
              alignItems: "flex-end",
              textAlign: "center"
            }}
          >
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                width: "33.3%",
                padding: "0 8px"
              }}
            >
              <div
                style={{
                  height: "20px",
                  background: "#b7b5b8",
                  width: "100%",
                  borderTopRightRadius: 2,
                  borderTopLeftRadius: 2
                }}
              />
              <div
                style={{
                  color: grey_10,
                  opacity: 0.3,
                  fontSize: 12,
                  padding: "8px 0",
                  letterSpacing: 4
                }}
              >{`<2W`}</div>
            </div>

            <div
              style={{
                display: "flex",
                flexDirection: "column",
                width: "33.3%",
                padding: "0 8px"
              }}
            >
              <div
                style={{
                  display: "flex",
                  width: "100%",
                  background: "#b7b5b8",
                  height: "100px",
                  borderTopRightRadius: 2,
                  borderTopLeftRadius: 2
                }}
              />
              <div
                style={{
                  color: grey_10,
                  opacity: 0.3,
                  fontSize: 12,
                  padding: "8px 0",
                  letterSpacing: 4
                }}
              >{`3-5w`}</div>
            </div>

            <div
              style={{
                display: "flex",
                flexDirection: "column",
                width: "33.3%",
                padding: "0 8px"
              }}
            >
              <div
                style={{
                  display: "flex",
                  width: "100%",
                  background: gradientOrangeToBottom,
                  height: "120px",
                  borderTopRightRadius: 2,
                  borderTopLeftRadius: 2
                }}
              />
              <div
                style={{
                  color: grey_10,
                  opacity: 0.3,
                  fontSize: 12,
                  padding: "8px 0",
                  letterSpacing: 4
                }}
              >{`5+w`}</div>
            </div>
          </div>
          <HrLine />
          {/* avatar section */}
          <div
            style={{
              display: "flex",
              justifyContent: "space-around",
              padding: "14px 0",
              alignItems: "center"
            }}
          >
            <div
              style={{
                width: 24,
                height: 24,
                borderRadius: "50%",
                border: "2px solid grey",
                display: "flex",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <img
                src={require("../../images/flag_avatar.png")}
                alt=""
                style={{
                  width: 20
                }}
              />
            </div>
            <div
              style={{
                width: 24,
                height: 24,
                // background: "grey",
                borderRadius: "50%",
                border: "2px solid grey",
                display: "flex",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <img
                src={require("../../images/flag_avatar.png")}
                alt=""
                style={{
                  width: 20
                }}
              />
            </div>
            <div
              style={{
                width: 40,
                height: 40,
                borderRadius: "50%",
                border: "2px solid #ea6650",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                // boxShadow: cardShadow
                boxShadow: "0px 0px 2px #ea6650, 0 0px 2px 2px #f5a623"
              }}
            >
              <img
                src={require("../../images/flag_avatar.png")}
                alt=""
                style={{
                  width: 40
                }}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PlayerPerformance;

// https://png.pngtree.com/element_pic/17/03/12/54a211270a10b8716533919ff0aaa2b0.jpg
