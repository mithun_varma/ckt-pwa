import React, { Component } from "react";
import {
  white,
  gradientRedOrange
} from "../../styleSheet/globalStyle/color";

class BackgroundComponent extends Component {
  render() {
    if (this.props.show !== undefined) {
      // console.log('THIS PROPS IS PRESENT');
      return (
        <React.Fragment>
          {!this.props.show ? null : (
            <div
              style={{ ...pageStyle.backgroundStyle, ...this.props.rootStyles }}
            >
              {this.props.logo ? (
                <img
                  src={require("../../images/cdcLogo.png")}
                  alt=""
                  style={{
                    width: 150,
                    // height: 30
                  }}
                />
              ) : (
                // </div>
                <div
                  style={{ ...pageStyle.titleStyle, ...this.props.titleStyles }}
                >
                  {this.props.title}
                </div>
              )}
            </div>
          )}
        </React.Fragment>
      );
    } else {
      // console.log('THIS PROPS IS NOT PRESENT');

      return (
        <React.Fragment>
          <div
            style={{ ...pageStyle.backgroundStyle, ...this.props.rootStyles }}
          >
            {this.props.logo ? (
              <img
                src={require("../../images/cdc-final-logo@3x.png")}
                alt=""
                style={{
                  width: 150,
                  // height: 30
                }}
              />
            ) : (
              // </div>
              <div
                style={{ ...pageStyle.titleStyle, ...this.props.titleStyles }}
              >
                {this.props.title}
              </div>
            )}
          </div>
        </React.Fragment>
      );
    }
  }
}

export default BackgroundComponent;

const pageStyle = {
  backgroundStyle: {
    // height: 258,
    background: gradientRedOrange,
    padding: "16px 16px"
  },
  titleStyle: {
    fontFamily: "Montserrat",
    fontWeight: 700,
    fontSize: 22,
    color: white,
    marginTop: 40
  }
};
