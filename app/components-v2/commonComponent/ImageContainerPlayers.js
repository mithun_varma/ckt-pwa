import React, { Component } from "react";

let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  ClevertapReact.initialize("W88-4KR-845Z");
}
class ImageContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      callApi: true
    };
    this._handelPlayerClick = this._handelPlayerClick.bind(this);
  }
  _handelPlayerClick(id) {
    // const id = event.target.alt;
    this.props.history.push(`/players/${id}`);
  }

  render() {
    return (
      <div style={{ ...pageStyle.container, ...this.props.rootStyles }}>
        <div style={pageStyle.headerWrapper}>
          <div style={pageStyle.header}>{this.props.title}</div>
          {this.props.poweredStadium && (
            <div style={pageStyle.featuredHeader}>
              <div>Powered By</div>
              <div>
                Criclytics
                <span> &trade;</span>
              </div>
            </div>
          )}
        </div>
        <div style={pageStyle.hrLine} />

        {this.props.isAvatar ? (
          <div style={pageStyle.avatarWrapper}>
            {this.props.data.map(item => (
              <div
                style={pageStyle.avatarContainer}
                id={item.crictecId}
                onClick={() => {

                  ClevertapReact.event("ImagePlayer", {
                    source: "ImagePlayer",
                    crictecId: item.crictecId
                  });
                  this._handelPlayerClick(item.crictecId)}
                }
              >
                <div
                  style={{
                    // background: `url(${item.avatar
                    //   ? item.avatar
                    //   : require("../../images/virat-kohli-nike-removebg@2x.png")})`,
                    // backgroundSize: "cover",
                    // backgroundPosition: "top center",
                    width: 80,
                    height: 80,
                    borderRadius: "50%",
                    background: "#616469",
                    overflow: "hidden"
                    // borderBottomLeftRadius: '64px',
                    // borderBottomRightRadius: '64px'
                  }}
                >
                  <img
                    src={
                      item.avatar
                        ? item.avatar
                        : require("../../images/fallbackProjection.png")
                    }
                    alt={item.crictecId}
                    style={{
                      objectFit: "contain",
                      width: 80,
                      marginLeft: "10px",
                      marginTop: "10px"
                    }}
                  />
                </div>
                <div style={pageStyle.avatarName}>{item.displayName}</div>
              </div>
            ))}
          </div>
        ) : (
          <div style={pageStyle.imageWrapper}>
            {this.props.data.map((item, index) => (
              <div
                style={{
                  ...pageStyle.imageContainer,
                  minWidth: "40%",
                  height: "120px",
                  // backgroundBlendMode: "lighten",
                  // backgroundSize: "contain",
                  background: `${this.props.gradient},url(${
                    item.avatar
                      ? item.avatar
                      : require("../../images/fallbackProjection.png")
                  })`,
                  backgroundSize: "cover",
                  backgroundPosition: "top center"
                  // backgroundImage: ''
                }}
                id={item.crictecId}
                onClick={() => {
                  
                  ClevertapReact.event("ImagePlayer", {
                    source: "ImagePlayer",
                    crictecId: item.crictecId
                  });
                  this._handelPlayerClick(item.crictecId)}
                }
              >
                <div style={pageStyle.imageName}>{item.displayName}</div>
              </div>
            ))}
          </div>
        )}
      </div>
    );
  }
}

// https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png
export default ImageContainer;

const pageStyle = {
  container: {
    display: "flex",
    flexDirection: "column",
    margin: "0 0 0 16px",
    marginTop: -194,
    borderRadius: 3,
    background: "#FFFFFF"
  },
  headerWrapper: {
    // padding: "12px 16px",
    fontFamily: "Montserrat",
    fontWeight: 500,
    fontSize: 12,
    display: "flex",
    flex: 1,
    justifyContent: "space-between"
  },
  header: {
    margin: "12px 0px 12px 16px",
    textTransform: "capitalize"
  },
  featuredHeader: {
    background: "#edeff4",
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "center",
    padding: "0px 22px"
  },
  hrLine: {
    height: 1,
    background: "linear-gradient(to right, #ffffff, #e3e4e6, #e3e4e6, #ffffff)"
  },
  avatarWrapper: {
    overflowX: "scroll",
    whiteSpace: "nowrap",
    display: "flex",
    background: "#FFF",
    borderRadius: 3
  },
  avatarContainer: {
    display: "inline-block",
    padding: "20px 30px 20px 18px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatarName: {
    fontFamily: "Montserrat",
    fontWeight: 400,
    fontSize: 12,
    color: "#141b2f",
    textAlign: "center",
    marginTop: "8px",
    whiteSpace: "initial"
  },
  imageWrapper: {
    overflowX: "scroll",
    whiteSpace: "nowrap",
    display: "flex",
    paddingLeft: "4px"
  },
  imageContainer: {
    position: "relative",
    margin: "16px 0px 16px 12px"
  },
  imageStyle: {
    width: 150,
    height: "100%"
  },
  imageName: {
    position: "absolute",
    bottom: 12,
    left: 12,
    right: 12,
    color: "#FFFFFF",
    fontSize: 12,
    fontFamily: "Montserrat",
    fontWeight: 600,
    whiteSpace: "initial",
    textTransform: "capitalize"
  }
};
