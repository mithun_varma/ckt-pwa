import React, { Component } from "react";
import {
  grey_10,
  white,
  gradientGrey
} from "../../styleSheet/globalStyle/color";
import { hrLineStyle } from "../../styleSheet/globalStyle/styleConfig";
import Slider from "react-slick";

const arr = [1, 2, 3, 4, 5, 6];
class JerseyModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.settings = {
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      className: "jersey-slider",
      cssEase: "linear",
      appendDots: dots => (
        <div>
          <ul className="slider-dots-wrapper">{dots}</ul>
        </div>
      )
    };
  }

  render() {
    return (
      <div style={{background: "rgba(0, 0, 0, 0.4)",display: "flex",flex: 1,height: "100vh",alignItems: "center",justifyContent: "center",padding: "0 16px" }}>
        {/* inner main wrapper */}
        <div style={{ width: "100%", background: white }} >
          {/* title */}
          <div style={{ display: "flex", justifyContent: "space-between", color: grey_10, padding: "12px 16px", background: gradientGrey}}>
            <div style={{ color: grey_10,fontFamily: "Montserrat",fontWeight: 500, fontSize: 12 }} >
              Trophy Cabinet
            </div>
            <div>
              <img src={require("../../images/close.png")} alt="" onClick={this._handelClose} style={{width: 12}} />
            </div>
          </div>
          <div style={hrLineStyle} />
          {/* body */}
          <div style={{ overflow: "scroll"}}>
            {/* test wrap */}
            <Slider {...this.settings}>
              {[1, 2, 3, 4,1,1,1,1,1].map(item => (
                <div style={{ display: "flex", flex: 1, flexWrap: "wrap",width: "50%", justifyContent: "center", alignItems: "center",flexDirection: "column",padding: "16px 0 8px",position: "relative" }} className="jerseySliderContainer">
                  {arr.map((item, key) => (
                    <div style={{ display: "flex", width: "50%", justifyContent: "center", alignItems: "center", flexDirection: "column",padding: "16px 0 8px",position: "relative"}}>
                      <div>
                        <img
                          src={require("../../images/teamJersey.png")}
                          alt=""
                          style={{
                            width: 96,
                            height: 96
                          }}
                        />
                      </div>
                      <div
                        style={{
                          marginTop: 4
                        }}
                      >
                        1986
                      </div>
                      <div
                        style={{
                          position: "absolute",
                          bottom: 0,
                          bottom: 0,
                          left: 0,
                          right: 0,
                          background: `linear-gradient(to ${
                            key % 2 == 0 ? "right" : "left"
                          }, #ffffff, #e3e4e6)`,
                          height: 1
                        }}
                      />
                    </div>
                  ))}
                </div>
              ))}
            </Slider>
            {/* test wrap closing */}
          </div>
          {/* Body Closing */}
        </div>
      </div>
    );
  }
}

export default JerseyModal;

// {/* test wrap */}
// {/* <div
//               style={{
//                 display: "flex",
//                 flex: 1,
//                 // padding: "20px 0 12px",
//                 flexWrap: "wrap"
//               }}
//             >
//               {arr.map((item, key) => (
//                 <div
//                   style={{
//                     display: "flex",
//                     // flex: 0.5,
//                     width: "50%",
//                     justifyContent: "center",
//                     alignItems: "center",
//                     flexDirection: "column",
//                     padding: "16px 0 8px",
//                     // borderBottom: "1px solid #e3e4e6",
//                     position: "relative"
//                     // flexGrow: 1
//                   }}
//                 >
//                   <div>
//                     <img
//                       // src={require("../../images/jersey.png")}
//                       src={require("../../images/teamJersey.png")}
//                       alt=""
//                       style={{
//                         width: 96,
//                         height: 96
//                       }}
//                     />
//                   </div>
//                   <div
//                     style={{
//                       marginTop: 4
//                     }}
//                   >
//                     1986
//                   </div>
//                   <div
//                     style={{
//                       position: "absolute",
//                       bottom: 0,
//                       bottom: 0,
//                       left: 0,
//                       right: 0,
//                       background: `linear-gradient(to ${
//                         key % 2 == 0 ? "right" : "left"
//                       }, #ffffff, #e3e4e6)`,
//                       height: 1
//                     }}
//                   />
//                 </div>
//               ))}
//             </div> */}

// {/* test wrap closing */}
