import React, { Component } from "react";
import { PathLine } from "react-svg-pathline";

export class WagonWheel extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const wagonPos = {
      x: 0,
      y: -180
    };
    let { wagon, selectedWagonPoint } = this.props;
    if (wagon == null) wagon = wagonPos;
    return (
      <div
        style={{
          // background: "white",
          background:
            "linear-gradient(to left, #ffffff, #f8f8f9 18%, #f8f8f9 85%, #ffffff)",
          borderRadius: 3,
          padding: "14px 0",
          display: "flex",
          justifyContent: "center"
          // position: "relative"
        }}
      >
        {this.props.showWicketImage ? (
          <div
            style={{
              width: 203,
              height: 203,
              borderRadius: "50%",
              overflow: "hidden"
            }}
          >
            <img
              src={require("../../images/groundImageWicket.png")}
              style={{
                width: 203,
                height: 203
              }}
            />
          </div>
        ) : (
          <div
            style={{
              width: 203,
              height: 203,
              position: "relative",
              // display: "flex",
              background: "lightgrey",
              borderRadius: "50%",
              overflow: "hidden"
            }}
          >
            <img src={require("../../images/ground.png")} />
            <svg style={pageStyle.lineStyle}>
              <React.Fragment>
                {wagon.map(
                  (item, key) =>
                    // ))}
                    // {wagon.y > 0 ? (
                    item.y > 0 ? (
                      <PathLine
                        points={[
                          { x: 100, y: 85.6 },
                          {
                            x: (item.x / 170.0) * 100 + 100,
                            y: 85.6 - (item.y / 160) * 85.6
                          }
                        ]}
                        stroke={item.runs == "six" ? "#4a90e2" : "#417505"}
                        strokeWidth={selectedWagonPoint == item.ballId
                          ? 2
                          : 1}
                        fill="none"
                        style={{
                          opacity: !selectedWagonPoint
                            ? 1
                            : selectedWagonPoint == item.ballId
                              ? 1
                              : 0.3
                        }}
                        // r={10}
                      />
                    ) : (
                      <PathLine
                        points={[
                          { x: 99, y: 85 },
                          {
                            x: (item.x / 170.0) * 100 + 100,
                            y: 85.6 - (item.y / 180) * 114
                          }
                        ]}
                        stroke={item.runs == "six" ? "#4a90e2" : "#417505"}
                        strokeWidth={selectedWagonPoint == item.ballId
                          ? 2
                          : 1}
                        fill="none"
                        style={{
                          opacity: !selectedWagonPoint
                            ? 1
                            : selectedWagonPoint == item.ballId
                              ? 1
                              : 0.3
                        }}
                        // r={10}
                      />
                      // )}
                    )
                )}
              </React.Fragment>
            </svg>
            <div
              style={{
                width: 4,
                height: 4,
                borderRadius: "50%",
                border: "1px solid white",
                background: "red",
                position: "absolute",
                top: "43%",
                // left: "49.3%"
                left: "49.2%"
              }}
            />
          </div>
        )}
      </div>
    );
  }
}

export default WagonWheel;

const pageStyle = {
  lineStyle: {
    position: "absolute",
    left: "1%", //"50%",
    top: "2%", //"43%",
    height: "inherit",
    width: "inherit"
  }
};
