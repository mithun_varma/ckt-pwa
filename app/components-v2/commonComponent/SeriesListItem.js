import ChevronRight from "@material-ui/icons/ChevronRight";
import React, { Component } from "react";
import {
  grey_10,
  grey_8,
  white,
  blue_grey
} from "../../styleSheet/globalStyle/color";
import HrLine from "./HrLine";
import moment from "moment";
import sortBy from "lodash/sortBy";
export default class SeriesLanding extends Component {
  convertMilliSecondsToDate = (from, to) => {
    return `${moment(from).format("MMM DD")}-${moment(to).format("MMM DD")}`;
  };
  displayMatchStats = seriesData => {
    // return seriesData.map((ele, key) => {
    return sortBy(seriesData, d => -d.totalMatches).map((ele, key) => {
      return ele.totalMatches > 0 ? (
        <span key={key}>
          {key > 0 && ele.totalMatches ? (
            <span
              style={{
                justifyContent: "space-between",
                verticalAlign: "middle",
                padding: "0 4px",
                fontSize: 12
              }}
            >
              {/* &bull; */}
              •
            </span>
          ) : null}

          <span
            style={{
              color: grey_10,
              fontSize: "12px",
              fontFamily: "Montserrat"
            }}
          >
            {ele.totalMatches}
          </span>
          <span
            style={{
              color: grey_8,
              display: "inline",
              fontSize: "12px",
              paddingLeft: "2.5px",
              fontFamily: "Montserrat"
            }}
          >
            {ele.kind}
          </span>
          {/* {key !== seriesData.length - 1 ? ( */}
          {false && key > 0 && ele.totalMatches ? (
            <span
              style={{
                justifyContent: "space-between",
                verticalAlign: "middle",
                padding: "0 4px",
                fontSize: 12
              }}
            >
              {/* &bull; */}
              •
            </span>
          ) : null}
        </span>
      ) : null;
    });
  };
  componentDidMount() {}
  render() {
    const { seriesName, startDate, matchStats, onClick, endDate } = this.props;

    const bull = ".";
    return (
      <React.Fragment>
        <div
          onClick={onClick}
          style={{
            margin: "10px 16px 14px",
            backgroundColor: white,
            cursor: "pointer"
          }}
        >
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <div
              style={{
                color: grey_10,
                fontSize: "12px",
                fontFamily: "Montserrat",
                fontWeight: 500
              }}
            >
              {seriesName}
            </div>
            <div
              style={{
                display: "flex"
              }}
            >
              <ChevronRight
                style={{
                  fontSize: 22,
                  color: blue_grey
                }}
              />
            </div>
          </div>
          {/* <span style={{ paddingLeft: 2 }}> */}
          {this.displayMatchStats(matchStats)}
          {/* </span> */}
          <div
            style={{
              color: grey_8,
              fontSize: "12px",
              fontFamily: "Montserrat",
              marginTop: 6
            }}
          >
            <span
              style={{
                background: "#ffe8d9",
                borderRadius: 10,
                padding: "2px 10px"
              }}
            >
              {this.convertMilliSecondsToDate(startDate, endDate)}
            </span>
          </div>
        </div>

        <HrLine />
      </React.Fragment>
    );
  }
}
