import React, { Component } from "react";
import Modal from 'react-responsive-modal';
import {
  gradientGrey
} from "../../styleSheet/globalStyle/color";
import { screenWidth, screenHeight } from "../../styleSheet/screenSize/ScreenDetails";
import HrLine from "./HrLine";



export default class TeamModalJersey extends Component{
    constructor(props){
        super(props);
        this.state={
            showModal:false,
        };
        this._handleCloseModal = this._handleCloseModal.bind(this);

    }
    componentDidMount(){

    }

    
    _handleCloseModal(){
        this.props.closeJersey();

    }
    render(){
        return (
            <div>
                <Modal open={true} closeIconSize ={'20'} showCloseIcon={true} onClose={this._handleCloseModal} center style={pageStyle.jerseyCabinetContainer}>
                    {this.slider()}
                </Modal>
            </div>
        );
    }

    slider(){
    const jerseyArray = this.props.jerseyData;

    if(jerseyArray.length>6){
        return(null);

    }
    else{
        return(
            <div>
                {this.singleScreen(jerseyArray)}
            </div>
        );
    }
    //  var settings = {
    //         dots: true,
    //         infinite: true,
    //         speed: 500,
    //         slidesToShow: 1,
    //         slidesToScroll: 1
    //     };
    //     var arrayOfArrays=[];
    //     for(let i = 0 ; i< jerseyArray.length; i+=6){
    //         arrayOfArrays.push(jerseyArray.slice(i,i+6));
    //     }
    //     return(
    //         <Slider {...settings} >
    //             <div>
    //                 {
    //                     arrayOfArrays.map((array,i)=>{
    //                         <div id={i} key={i}>
    //                             {this.singleScreen(array)}
    //                         </div>
    //                     })
    //                 }
    //             </div>
    //         </Slider>
    //     );
    }

    singleScreen(array){
        return(
            <div style={pageStyle.singleScreen}>
                <div style={pageStyle.title}>
                    {"Team Jerseys"}
                </div>
                <div>
                    { 
                        array[0]&&array[1]&&
                        <div style={pageStyle.singleRow}>
                            <div style={{flex:0.10, display:'flex'}}>
                                {null}
                            </div>
                            <div style={pageStyle.singleRowImage}>
                                
                                <img src={array[0].image} alt="jersey image" height="110px" width="110px" style={pageStyle.images} />
                                
                                <div style={pageStyle.year}>
                                    {array[0].year}
                                </div>
                            </div>
                            <div style={{flex:0.10, display:'flex'}}>
                                {null}
                            </div>
                            <div style={pageStyle.singleRowImage}>
                                
                                    <img src={array[1].image} alt="jersey image" height="110px" width="110px"  style={pageStyle.images} />
                                
                                <div style={pageStyle.year}>
                                    {array[1].year}
                                </div>
                            </div>
                            <div style={{flex:0.10, display:'flex'}}>
                                {null}
                            </div>
                        </div>  
                    }
                </div>
                <HrLine />
                <div>
                    { 
                        array[2]&&array[3]&&
                        <div style={pageStyle.singleRow}>
                            <div style={{flex:0.10, display:'flex'}}>
                                {null}
                            </div>
                            <div style={pageStyle.singleRowImage}>
                              
                                <img src={array[2].image} alt="jersey image" height="110px" width="110px" style={pageStyle.images}  />
                               
                                <div style={pageStyle.year}>
                                    {array[2].year}
                                </div>
                            </div>
                            <div style={{flex:0.10, display:'flex'}}>
                                {null}
                            </div>
                            <div style={pageStyle.singleRowImage}>
                               
                                <img src={array[3].image} alt="jersey image" height="110px" width="110px" style={pageStyle.images} />
                              
                                <div style={pageStyle.year}>
                                    {array[3].year}
                                </div>
                            </div>
                            <div style={{flex:0.10, display:'flex'}}>
                                {null}
                            </div>
                        </div>  
                    }
                </div>
                <HrLine />
                <div>
                    { 
                        array[4]&&array[5]&&
                        <div style={pageStyle.singleRow}>
                            <div style={{flex:0.10, display:'flex'}}>
                                {null}
                            </div>
                            <div style={pageStyle.singleRowImage}>
                              
                                <img src={array[4].image} alt="jersey image" height="110px" width="110px" style={pageStyle.images} />
                               
                                <div style={pageStyle.year}>
                                    {array[4].year}
                                </div>
                            </div>
                            <div style={{flex:0.10, display:'flex'}}>
                                {null}
                            </div>
                            <div style={pageStyle.singleRowImage}>
                               
                                    <img src={array[5].image} alt="jersey image" height="110px" width="110px"  style={pageStyle.images} />
                             
                                <div style={pageStyle.year}>
                                    {array[5].year}
                                </div>
                            </div>
                            <div style={{flex:0.10, display:'flex'}}>
                                {null}
                            </div>
                        </div>  
                    }
                </div>
                <HrLine />
            </div>
        );
    }

}

const pageStyle = {
    jerseyCabinetContainer:{
        display:'flex',
        flex:1,
        flexDirection:'column',
        width:screenWidth*0.911,
        backgroundColor:'#fff',
    },
    images:{
        display:'flex',
        flex:1,
        border:'1px solid #cdcdcd',
        borderRadius:'50%',
        backgroundColor:'#cdcdcd'
    },
    year:{
        display:'flex',
        flex:1,
    },
    singleRowImage:{
        display:'flex',
        flex:0.35,
        flexDirection:'column',
        color:'#141b2f',
        fontSize:'14px',
        justifyContent:'center'

    },
    singleRow:{
        display:'flex',
        flex:1,
        flexDirection:'row',
        width:screenWidth*0.911,
        marginTop:'10px',
    },
    title:{
        display:'flex',
        height:screenHeight*0.06,
        alignItems:'center',
        paddingLeft:'16px',
        justifyContent:'flex-start',
        flex:1,
        background:gradientGrey,
        marginTop:'-15px',

    },
    singleScreen:{
        display:'flex',
        flex:1,
        width:screenWidth*0.80,
        backgroundColor:'#fff',
        flexDirection:'column',
    },
    modalContainer:{
        display:'flex',
        flex:1,
        
    },

}