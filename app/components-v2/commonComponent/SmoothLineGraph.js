import React, { PureComponent } from "react";
import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
} from "recharts";
import { grey_1 } from "../../styleSheet/globalStyle/color";
export class SmoothLineGraph extends PureComponent {
  state = {
    data: undefined
  };

  render() {

    return (
      <LineChart
        width={300}
        height={300}
        data={this.props.data}
        // margin={{ top: 5, right: 40, bottom: 5, left: 40 }}
      >
        {this.props.data &&
          this.props.data.map(ele => {
            return ele.Projected ? (
              <Line type="natural" dataKey="currentScore" stroke={"black"} />
            ) : (
              <Line type="natural" dataKey="currentScore" stroke={"#8884d8"} />
            );
          })}

        {/* <Line
          points={[{ x: 4, y: 300, value: 1 }, { x: 5, y: 255, value: 2 }]}
          type="linear"
          dataKey={"Projected"}
          stroke="#111111"
        /> */}

        <CartesianGrid vertical={false} stroke="#727682" strokeDasharray="0" />
        <XAxis
          // interval={5}
          // domain={["1", "50"]}
          stroke="#727682"
          dataKey="overNo"
        />
        {/* <Tooltip /> */}
      </LineChart>
    );
  }
}

export default SmoothLineGraph;
