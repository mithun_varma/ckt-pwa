import React, { PureComponent } from "react";

export class FactHeader extends PureComponent {
  render() {
    const { header, description } = this.props;
    return (
      <div
        style={{
          borderRadius: "3px",
          backgroundColor: "#edeff4"
        }}
      >
        <div
          style={{
            fontFamily: "Montserrat",
            fontSize: "20px",
            fontWeight: "bold",
            color: "#141b2f",
            paddingBottom: "15px",
            padding: "0px 22px",
            paddingTop: "24px"
          }}
        >
          {header}
        </div>
        {description && (
          <div
            style={{
              fontFamily: "Montserrat",
              fontSize: "14px",
              color: "#727682",
              padding: "0px 22px",
              marginTop: 7.5,
              paddingBottom: "33px"
            }}
          >
            {description}
          </div>
        )}
      </div>
    );
  }
}

export default FactHeader;
