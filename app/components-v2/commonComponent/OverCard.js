import React, { Component } from "react";
import { grey_8, white } from "../../styleSheet/globalStyle/color";

class OverCard extends Component {
  render() {
    return (
      <div
        style={{
          background: grey_8,
          borderRadius: 3,
          padding: "2px 6px 3px",
          color: white,
          width: 50,
          textAlign: "center",
          ...this.props.rootStyles
        }}
      >
        <div
          style={{
            fontFamily: "Montserrat",
            fontWeight: 700,
            fontSize: 21,
            fontWeight: "bold",
            // padding: "3px 4px",
            padding: "3px 0px",
            borderWidth: "0 0 1px 0",
            borderStyle: "solid",
            borderColor: "rgba(255,255,255, 0.4)",
            ...this.props.titleStyles
          }}
        >
          {this.props.data.title}
        </div>
        <div
          style={{
            fontFamily: "Montserrat",
            fontWeight: 500,
            fontSize: 10,
            textAlign: "center",
            padding: "2px 0",
            ...this.props.captionStyles
          }}
        >
          {this.props.data.caption}
        </div>
      </div>
    );
  }
}

export default OverCard;
