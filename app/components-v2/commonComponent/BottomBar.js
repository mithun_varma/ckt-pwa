import React, { Component } from "react";
import { grey_8, white } from "../../styleSheet/globalStyle/color";

export class BottomBar extends Component {
  redirectTo = path => {
    this.props.push(`/${path === "/" ? "" : path}`);
  };
  render() {
    const { tabs } = this.props;
    return (
      <div style={pageStyle.bottomWrapper}>
        <div
          style={{
            ...pageStyle.itemStyle
          }}
          onClick={() => {
            this.redirectTo(tabs[0].route);
          }}
        >
          <img
            src={
              this.props.location.pathname === "/"
                ? require("../../images/homeActive.svg")
                : require("../../images/home.svg")
            }
            alt=""
            style={{
              width: 22,
              height: 22
            }}
          />
          <div
            style={
              this.props.location.pathname === "/"
                ? pageStyle.active
                : pageStyle.inactive
            }
          >
            {tabs[0].key}
          </div>
        </div>
        <div
          style={{ ...pageStyle.itemStyle }}
          onClick={() => {
            this.redirectTo(tabs[1].route);
          }}
        >
          <img
            src={
              this.props.location.pathname === "/schedule"
                ? require("../../images/scheduleActive.svg")
                : require("../../images/schedule.svg")
            }
            alt=""
            style={{
              width: 22,
              height: 22
            }}
          />
          <div
            style={
              this.props.location.pathname === "/schedule"
                ? pageStyle.active
                : pageStyle.inactive
            }
          >
            {tabs[1].key}
          </div>
        </div>
        <div
          style={{ ...pageStyle.itemStyle, position: "relative" }}
          onClick={() => {
            this.redirectTo(tabs[2].route);
          }}
        >
          <div
            style={{
              position: "absolute",
              // top: -31,
              bottom: -10,
              background: white,
              padding: 5,
              borderRadius: "50%",
              display: "flex",
              boxShadow: "0 -5px 3px -2px rgba(0, 0, 0, 0.12)"
              // boxShadow: "0px -6px 9px -4px rgba(0,0,0,0.75)"
            }}
          >
            <img
              src={
                this.props.location.pathname === "/criclytics"
                  ? require("../../images/criclytics.svg")
                  : require("../../images/criclytics.svg")
              }
              alt=""
              style={{
                width: 45,
                height: 45
              }}
            />
          </div>

          <div style={{ width: "74px", marginLeft: "5px" }}>
            <div
              style={
                this.props.location.pathname === "/criclytics"
                  ? pageStyle.activeCric
                  : pageStyle.inactiveCric
              }
              // style ={{width: '74px', marginTop: "4px"}}
            >
              {tabs[2].key}
              <span style={{ fontSize: "7px", verticalAlign: "top" }}> TM</span>
            </div>
          </div>
        </div>
        <div
          style={{ ...pageStyle.itemStyle }}
          onClick={() => {
            this.redirectTo(tabs[3].route);
          }}
        >
          <img
            src={
              this.props.location.pathname === "/articles"
                ? require("../../images/newsActive.svg")
                : require("../../images/news.svg")
            }
            alt=""
            style={{
              width: 22,
              height: 22
            }}
          />
          <div
            style={
              this.props.location.pathname === "/articles"
                ? pageStyle.active
                : pageStyle.inactive
            }
          >
            {tabs[3].key}
          </div>
        </div>
        <div
          style={{ ...pageStyle.itemStyle }}
          onClick={() => {
            this.redirectTo(tabs[4].route);
          }}
        >
          <img
            src={
              this.props.location.pathname === "/more"
                ? require("../../images/moreActive.svg")
                : require("../../images/more.svg")
            }
            alt=""
            style={{
              width: 22,
              height: 22
            }}
          />
          <div
            style={
              this.props.location.pathname === "/more"
                ? pageStyle.active
                : pageStyle.inactive
            }
          >
            {tabs[4].key}
          </div>
        </div>
      </div>
    );
  }
}

export default BottomBar;

const pageStyle = {
  bottomWrapper: {
    position: "fixed",
    bottom: 0,
    background: white,
    // height: 20,
    width: "100%",
    display: "flex",
    flex: 1,
    justifyContent: "space-evenly",
    alignItems: "center",
    fontFamily: "Montserrat",
    fontSize: 10,
    // color: grey_8,
    height: 56,
    textTransform: "uppercase",
    // boxShadow: "0 0 8px rgba(0, 0, 0, 0.4)",
    boxShadow: "0 -3px 3px 0 rgba(0, 0, 0, 0.12)",
    zIndex: 999
  },
  itemStyle: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    flex: 0.2
  },
  labelStyle: {
    marginTop: 2
  },
  active: {
    marginTop: 4,
    // color: orange_10
    color: "#d44030"
  },
  inactive: {
    marginTop: 4,
    color: grey_8
  },
  activeCric: {
    // marginTop: 2,
    // color: orange_10,
    color: "#d44030",
    top: 6,
    position: "absolute"
  },
  inactiveCric: {
    // marginTop: 2,
    color: grey_8,
    top: 6,
    position: "absolute"
  }
};
