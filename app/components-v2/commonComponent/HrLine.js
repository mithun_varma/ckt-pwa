import React, { Component } from "react";
import { hrLineStyle } from "../../styleSheet/globalStyle/styleConfig";

class HrLine extends Component {
  render() {
    return <div style={{ ...hrLineStyle, ...this.props.style }} />;
  }
}

export default HrLine;
