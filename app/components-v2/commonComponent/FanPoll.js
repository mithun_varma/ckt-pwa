import React, { Component } from "react";
import {
  cardShadow,
  white,
  grey_10,
  lightGreen
} from "../../styleSheet/globalStyle/color";
import CheckBoxOutlineBlank from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBox from "@material-ui/icons/CheckBox";
import Loader from "./Loader";

let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  ClevertapReact.initialize("W88-4KR-845Z");
}
class FanPoll extends Component {
  getHighest = data => {
    return Math.max(
      +data.option1Percentage,
      +data.option2Percentage,
      +data.option3Percentage
    );
  };

  handlePollClick = (id, option) => {
    if (!this.props.isResult) {
      this.props.handlePoll(id, option);
    }
  };

  render() {
    const {
      pollData,
      isResult,
      pollResult,
      loading,
      selectedOption
    } = this.props;
    const highest = this.getHighest(pollResult);
    // console.log(highest);
    return (
      <div
        style={{
          background: white,
          margin: "0px 16px 12px",
          borderRadius: 3,
          boxShadow: cardShadow,
          position: "relative",
          padding: "38px 12px 16px",
          ...this.props.rootStyles
        }}
      >
        <div
          style={{
            position: "absolute",
            top: 0,
            // left: 24,
            left: 12,
            background: grey_10,
            fontFamily: "Montserrat",
            fontWeight: 500,
            fontSize: 10,
            color: white,
            padding: "4px 16px",
            letterSpacing: "1.5px",
            borderRadius: "0 0 3px 3px"
          }}
        >
          FAN POLL
        </div>
        <span
          style={{
            position: "absolute",
            right: 16,
            top: 4,
            fontSize: 10,
            fontWeight: 600,
            color: "#d74730"
          }}
        >
          {`${isResult ? pollResult.totalPolls : pollData.totalPolls} votes`}
        </span>
        <div
          style={{
            color: grey_10,
            fontFamily: "Montserrat",
            fontWeight: 500,
            fontSize: 12
          }}
        >
          {pollData && pollData.question}
        </div>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              margin: "16px 0"
            }}
          >
            {pollData && pollData.option3 ? (
              <React.Fragment>
                <Loader styles={{ height: "45px", width: "30%" }} />
                <Loader styles={{ height: "45px", width: "30%" }} />
                <Loader styles={{ height: "45px", width: "30%" }} />{" "}
              </React.Fragment>
            ) : (
              <React.Fragment>
                <Loader styles={{ height: "45px", width: "45%" }} />
                <Loader styles={{ height: "45px", width: "45%" }} />
              </React.Fragment>
            )}
          </div>
        ) : (
          <div
            style={{
              margin: "16px 0",
              fontFamily: "Montserrat",
              display: "flex"
            }}
          >
            <div
              className={
                pollData && pollData.option3 ? "fan-option" : "fan-option w-48"
              }
              style={
                isResult
                  ? { borderLeft: "none" }
                  : pollData && pollData.option3
                    ? {}
                    : { minWidth: "48%" }
              }
              onClick={() => {
                ClevertapReact.event("FanPoll", {
                  source: "FanPoll",
                  pollId: pollData.pollId
                });
                this.handlePollClick(pollData.pollId, "option1")}}
            >
              {isResult && (
                <div
                  className="rotate-div"
                  style={
                    highest === +pollResult.option1Percentage
                      ? { background: lightGreen }
                      : { background: grey_10 }
                  }
                >
                  {pollResult.selectedOption === "option1" ||
                  selectedOption === "option1" ? (
                    <CheckBox
                      style={{
                        fontSize: 14,
                        color: "#fff",
                        transform: "rotate(90deg) translateY(1px)",
                        verticalAlign: "middle"
                      }}
                    />
                  ) : (
                    <CheckBoxOutlineBlank
                      style={{
                        fontSize: 14,
                        color: "#fff",
                        transform: "translateX(-1px)",
                        verticalAlign: "middle"
                      }}
                    />
                  )}
                  <span>{`${+pollResult.option1Percentage}%`}</span>
                </div>
              )}
              {pollData && pollData.option1}
            </div>
            <div
              className={
                pollData && pollData.option3 ? "fan-option" : "fan-option w-48"
              }
              style={
                isResult
                  ? { borderLeft: "none" }
                  : pollData && pollData.option3
                    ? {}
                    : { minWidth: "48%" }
              }
              onClick={() => {

                ClevertapReact.event("FanPoll", {
                  source: "FanPoll",
                  pollId: pollData.pollId
                });
                this.handlePollClick(pollData.pollId, "option2")}}
            >
              {isResult && (
                <div
                  className="rotate-div"
                  style={
                    highest === +pollResult.option2Percentage
                      ? { background: lightGreen }
                      : { background: grey_10 }
                  }
                >
                  {pollResult.selectedOption === "option2" ||
                  selectedOption === "option2" ? (
                    <CheckBox
                      style={{
                        fontSize: 14,
                        color: "#fff",
                        transform: "rotate(90deg) translateY(1px)",
                        verticalAlign: "middle"
                      }}
                    />
                  ) : (
                    <CheckBoxOutlineBlank
                      style={{
                        fontSize: 14,
                        color: "#fff",
                        transform: "translateX(-1px)",
                        verticalAlign: "middle"
                      }}
                    />
                  )}
                  <span>{`${+pollResult.option2Percentage}%`}</span>
                </div>
              )}
              {pollData && pollData.option2}
            </div>
            {pollData &&
              pollData.option3 && (
                <div
                  className="fan-option"
                  style={isResult ? { borderLeft: "none" } : {}}
                  onClick={() =>{

                    ClevertapReact.event("FanPoll", {
                      source: "FanPoll",
                      pollId: pollData.pollId
                    });
                    this.handlePollClick(pollData.pollId, "option3")
                  }
                  }
                >
                  {isResult && (
                    <div
                      className="rotate-div"
                      style={
                        highest === +pollResult.option3Percentage
                          ? { background: lightGreen }
                          : { background: grey_10 }
                      }
                    >
                      {pollResult.selectedOption === "option3" ||
                      selectedOption === "option3" ? (
                        <CheckBox
                          style={{
                            fontSize: 14,
                            color: "#fff",
                            transform: "rotate(90deg) translateY(1px)",
                            verticalAlign: "middle"
                          }}
                        />
                      ) : (
                        <CheckBoxOutlineBlank
                          style={{
                            fontSize: 14,
                            color: "#fff",
                            transform: "translateX(-1px)",
                            verticalAlign: "middle"
                          }}
                        />
                      )}
                      <span>{`${+pollResult.option3Percentage}%`}</span>
                    </div>
                  )}
                  {pollData && pollData.option3}
                </div>
              )}
          </div>
        )}
      </div>
    );
  }
}

export default FanPoll;
