import React, { Component } from "react";
import {
  grey_8,
  white,
  red_10,
  blue_10,
  grey_10,
  green_10,
  grey_7,
  blue_grey
} from "../../styleSheet/globalStyle/color";

class BallComponent extends React.PureComponent {
  render() {
    const { showSeparator, type, balls } = this.props;
    return balls.map((item, key) => (
      <div
        key={`${key + 1}`}
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          ...this.props.rootStyles
        }}
      >
        {showSeparator &&
          key > 0 &&
          // key < 9 &&
          item.overNo != balls[key - 1].overNo && (
            <div
              style={{
                height: "136%",
                width: 1,
                background: grey_10,
                margin: "0 6px 0 0"
              }}
            />
          )}
        <div
          style={{
            width: 21,
            height: 21,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            borderRadius: "50%",

            color: white,
            // background: item == "L" ? red_10 : green_10,
            background:
              type == "match"
                ? item == "L"
                  ? red_10
                  : item == "W"
                    ? green_10
                    : blue_grey
                : item.runs == 6
                  ? blue_10
                  : item.runs == 4
                    ? green_10
                    : item.wicketCount > 0
                      ? red_10
                      : blue_grey,
            fontFamily: "Montserrat",
            fontWeight: 700,
            fontSize: 7
          }}
        >
          {type == "match"
            ? item
            : item.wicketCount > 0
              ? "w"
              : item.extras > 0
                ? item.extraType
                : item.runs || 0}
        </div>
      </div>
    ));
  }
}

export default BallComponent;
