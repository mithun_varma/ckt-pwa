import React, { Component } from "react";
import {
  grey_10,
  gradientGrey,
  blue_grey
} from "../../styleSheet/globalStyle/color";
import HrLine from "./HrLine";
import ChevronRight from "@material-ui/icons/ChevronRight";

class CardGradientTitle extends Component {
  render() {
    return (
      <div>
        <div
          style={{
            display: "flex",
            flex: 1,
            justifyContent: "space-between",
            background: gradientGrey,
            alignItems: "center",
            padding: "12px 8px 12px 16px",
            color: grey_10,
            fontSize: 12,
            ...this.props.rootStyles
          }}
          onClick={this.props.handleCardClick}
        >
          <div
            style={{
              // fontSize: 12,
              fontFamily: "Montserrat",
              fontWeight: 500,
              // flex: 0.74,
              ...this.props.titleStyles
            }}
          >
            <span>{this.props.title}</span>
          </div>
          {this.props.isRightIcon && (
            <div
              style={{
                display: "flex"
              }}
            >
              <ChevronRight style={{ color: blue_grey }} />
            </div>
          )}
          {this.props.rightElement}
          {this.props.subtitle && (
            <div
              style={{
                // fontSize: 12,
                fontFamily: "Montserrat",
                fontWeight: 500,
                ...this.props.subtitleStyles
              }}
              onClick={e => {
                e.preventDefault();
                this.props.isSubtitleClickable
                  // ? this.props.redirectTo(`/articles/${this.props.cardType}`)
                  ? this.props.redirectTo(`/articles?type=${this.props.cardType}`)
                  : this.props.redirectTo();
              }}
            >
              {this.props.subtitle}
            </div>
          )}
        </div>
        <HrLine />
      </div>
    );
  }
}

export default CardGradientTitle;
