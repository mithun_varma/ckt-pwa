import React, { Component } from "react";
import {
  screenWidth
} from "../../styleSheet/screenSize/ScreenDetails";
import {
  orange_10,
  grey_6,
  grey_4
} from "../../styleSheet/globalStyle/color";
import HrLine from "./HrLine";
import EmptyState from "./EmptyState";
import moment from "moment";


let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  ClevertapReact.initialize("W88-4KR-845Z");
}

export default class PlayerNews extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   cardTitle: "News Title",
    //   newsData: []
    // };
  }
  componentDidMount() {
    // this.setState({ cardTitle: cardTitle });
    // this.setState({ newsData: news });
  }

  render() {
    // console.log(this.props.playerNews);
    const { playerNews } = this.props;
    return (
      <div style={pageStyle.playerNewsContainer}>
        <div style={pageStyle.playerNewsHead}>
          <span>You may also like</span>
          {this.props.playerNews.length > 0 && (
            <span
              style={{ color: "#d44030" }}
              onClick={() => this.props.history.push("/articles/news")}
            >
              View All
            </span>
          )}
        </div>
        <HrLine />
        <div style={pageStyle.newsContainer}>
          {playerNews && playerNews.length > 0 ? (
            playerNews.map(data => {
              return (
                <div
                  style={pageStyle.playerNewsView}
                  onClick={() => {

                    ClevertapReact.event("Article", {
                      source: "Article",
                      articleId: data._id
                    });

                    this.props.history.push(
                      `/articles/${data.type}/${data.title.replace(
                        /[ ,]/g,
                        "-"
                        )}/${data._id}`
                        )
                    }
                  }
                >
                  <div
                    style={{
                      ...pageStyle.playerNewsViewImage,
                      background: `url(${
                        data.imageData.image
                      }) top center no-repeat`,
                      backgroundSize: "cover"
                    }}
                  />
                  <div style={{ padding: "12px", background: grey_4 }}>
                    <div style={pageStyle.playerNewsViewHeading}>
                      {data.title}
                    </div>
                    <div style={pageStyle.playerNewsViewDescription}>
                      {data.description}
                    </div>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        width: screenWidth * 0.8,
                        marginTop: "12px"
                      }}
                    >
                      <div style={pageStyle.playerNewsViewAuthor}>
                        {data.author}
                      </div>
                      <div style={pageStyle.playerNewsViewDate}>
                        <span>
                          {moment(data.publishedAt).format("DD MMM,YYYY")}
                        </span>
                        <span style={{ padding: "0 6px" }}> &bull;</span>
                        <span style={{ color: orange_10 }}>
                          {data.readTime ? data.readTime : ""}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })
          ) : (
            <EmptyState msg="no news found" />
          )}
        </div>
        {/* <div style={pageStyle.playerNewsMoreNews}>
                    Related News
                </div> */}
      </div>
    );
  }
}

const pageStyle = {
  newsContainer: {
    display: "flex",
    flexDirection: "row",
    backgroundColor: "#fff",
    overflowY: "scroll",
    marginTop: "12px"
  },
  playerNewsContainer: {
    display: "flex",
    flexDirection: "column",
    overflow: "hidden",
    backgroundColor: "#fff",
    padding: "12px 16px",
    fontFamily: "Montserrat"
  },
  playerNewsHead: {
    display: "flex",
    justifyContent: "space-between",
    color: "#141b2f",
    fontSize: "12px",
    paddingBottom: "14px",
    fontWeight: "500"
  },
  playerNewsView: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#fff",
    width: 200,
    marginRight: "5%",
    boxShadow: "0 3px 6px 0 #13000000"
  },
  playerNewsViewImage: {
    display: "flex",
    height: 100,
    width: "100%"
    // width: screenWidth * 0.611
  },
  playerNewsViewTime: {
    display: "flex",
    width: screenWidth * 0.35,
    padding: "5px",
    color: "#ffffff",
    justifyContent: "center",
    alignItems: "center",
    backgroundImage: "linear-gradient(to right, #ea6550, #fa9441)",
    marginTop: "-20px"
  },
  playerNewsViewHeading: {
    display: "flex",
    // padding: '5px',
    color: "#141b2f",
    fontSize: "14px",
    fontWeight: "600"
    // fontFamily: 'Montserrat-Medium',
  },
  playerNewsViewDescription: {
    display: "flex",
    color: "#141b2f",
    fontSize: "12px",
    marginTop: "8px",
    // fontFamily: 'Montserrat-Medium',
    // backgroundColor:'#f1f1f1',
    padiing: "5px"
    // width: screenWidth * 0.80,
  },
  playerNewsViewAuthor: {
    display: "flex",
    // flex: 0.50,
    justifyContent: "flex-start",
    color: grey_6,
    fontSize: "10px",
    fontStyle: "italic"
    // fontFamily: 'Montserrat-Medium',
    // backgroundColor:'#f1f1f1',
    // padding:'15px 5px 5px 5px',
  },
  playerNewsViewDate: {
    display: "flex",
    // flex: 0.50,
    fontWeight: "500",
    // justifyContent: 'flex-end',
    color: "#141b2f",
    fontSize: "10px"
    // fontFamily: 'Montserrat-Medium',
    // backgroundColor:'#f1f1f1',
    // padding:'15px 5px 5px 5px',
  },
  playerNewsMoreNews: {
    display: "flex",
    flex: 1,
    padding: "5px 5px 10px 0",
    color: "#f76b1c",
    fontSize: "14px",
    // fontFamily: 'Montserrat-Medium',
    justifyContent: "flex-end"
  }
};
