/* eslint-disable react/no-array-index-key */
import React from 'react';
import PropTypes from 'prop-types';
import * as scale from 'd3-scale';
import * as shape from 'd3-shape';
import Theme from './Theme';
const d3 = {
  scale,
  shape,
};
// import { scaleBand, scaleLinear } from "d3-scale";

class PreviewChart extends React.Component {
  constructor(props) {
    super(props);
    this.value = this.value.bind(this);
    this.label = this.label.bind(this);
    this.color = this.color.bind(this);
  }
  value(item) {
    return item.value;
  }

  label(item) {
    return item.label;
  }

  color(index) {
    return Theme.colors[index];
  }
  render() {
    const { content } = this.props;
    const arcs = d3.shape.pie().value(this.value)(this.props.content);
    const arc = d3.shape
      .arc()
      .innerRadius(0)
      .outerRadius(this.props.pieWidth / 2)
      .padAngle(0);
    // console.log(arc(arcs[0]));
    // const arcData = arcs[index];
    // const path = arc(arcData);
    return (
      <div className="katap-PreviewChart">
        {!content || content.length === 0 ? (
          <div>
            <p>Please Provide valid details</p>
          </div>
        ) : (
          <React.Fragment>
            <svg height={this.props.pieHeight} width={this.props.pieWidth}>
              <g id="xAxis" transform="translate(100,100)">
                {this.props.content.map((item, index) => (
                  <g className="pie_class" key={`pie_shape_' + ${index}`}>
                    <path id="area" d={arc(arcs[index])} fill={this.color(index)} />
                    <text
                      className="chart-text"
                      textAnchor="middle"
                      transform={`translate(${arc.centroid(arcs[index])})`}
                    >
                      {item.value}
                    </text>
                  </g>
                ))}
              </g>
            </svg>
            <svg
              height={this.props.legendHeight}
              width={this.props.legendWidth}
              transform="translate(0,0)"
              className="legend_svg"
            >
              {this.props.content.map((item, index) => (
                <g
                  className="pie_legend"
                  key={`pie_leg_' + ${index}`}
                  transform={`translate(0,${index * 20})`}
                >
                  <rect x="0" y="0" width="10" height="10" fill={this.color(index)} />
                  <text className="legend-text" textAnchor="start" x="20" y="10">
                    {item.label}
                  </text>
                </g>
              ))}
            </svg>
          </React.Fragment>
        )}
      </div>
    );
  }
}
PreviewChart.propTypes = {
  content: PropTypes.any,
  pieWidth: PropTypes.number,
  pieHeight: PropTypes.number,
  legendWidth: PropTypes.number,
  legendHeight: PropTypes.number,
};

export default PreviewChart;
