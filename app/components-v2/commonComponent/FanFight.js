import React, { Component } from "react";
import { grey_10, white, green_10 } from "../../styleSheet/globalStyle/color";

class FanFight extends Component {
  render() {
    return (
      <div>
        <a
          href={this.props.url && this.props.url}
          download
          style={{
            textDecoration: "none"
          }}
          target="_blank"
        >
          <div
            style={{
              background: grey_10,
              margin: "0 16px",
              display: "flex",
              padding: "8px 10px",
              borderRadius: 3,
              alignItems: "center",
              ...this.props.rootStyles
            }}
          >
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
                color: white,
                padding: "12px 10px",
                borderWidth: "0 1px 0 0",
                borderColor: "#545454",
                borderStyle: "solid"
              }}
            >
              <div>
                <img
                  src={require("../../images/fanfight_logo.png")}
                  alt=""
                  style={{
                    width: 57,
                    height: 25
                  }}
                />
              </div>
              <div
                style={{
                  fontSize: 8,
                  letterSpacing: "3.5px",
                  fontFamily: "DINCondensed-Bold",
                  marginTop: 6
                }}
              >
                FANFIGHT
              </div>
            </div>

            <div style={{ padding: "12px" }}>
              <div
                style={{
                  color: white,
                  fontSize: 12,
                  fontFamily: "Montserrat",
                  fontWeight: 400
                }}
              >
                India’s Premier Fantasy Platform
              </div>
              <div
                style={{
                  color: green_10,
                  fontSize: 15,
                  fontFamily: "Montserrat",
                  fontWeight: 600,
                  marginTop: 6
                }}
              >
                Start playing today
              </div>
            </div>
          </div>
        </a>
      </div>
    );
  }
}

export default FanFight;
