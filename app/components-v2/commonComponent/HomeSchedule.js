import React from "react";
import CardGradientTitle from "./CardGradientTitle";
import HrLine from "./HrLine";
import OverCard from "./OverCard";
import ScheduleDateCard from "./ScheduleDateCard";
import {
  white,
  red_Orange,
  grey_10,
  grey_8
} from "../../styleSheet/globalStyle/color";
import moment from "moment";
import Loader from "../../components/Common/Loader";

let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  ClevertapReact.initialize("W88-4KR-845Z");
}
export class HomeSchedule extends React.PureComponent {
  render() {
    // console.log(this.props.matches);
    return (
      <div>
        <CardGradientTitle
          title="WORLD CUP SCHEDULE"
          // subtitle="View All"
          handleCardClick={() =>
            this.props.history.push(`/series/odi-world-cup/opta:1`)
          }
          isRightIcon
          rootStyles={{
            color: "#727682",
            fontSize: 10,
            padding: "10px 8px"
          }}
          // redirectTo={() => {
          //   this.props.history.push(`/series/odi-world-cup/opta:1`);
          // }}
          // subtitleStyles={{
          //   color: red_Orange,
          //   marginRight: 10,
          //   fontSize: 10
          // }}
        />
        {/* <HrLine /> */}
        {this.props.loading ? (
          <Loader noOfLoaders={3} />
        ) : (
          this.props.matches &&
          this.props.matches.length > 0 &&
          this.props.matches.map((match, i) => (
            <React.Fragment key={i}>
              <div
                style={{
                  display: "flex",
                  padding: "14px 16px",
                  background: white
                }}
                onClick={() => {
                  ClevertapReact.event("HomeSchedule", {
                    source: "HomeSchedule",
                    matchId: match.matchId
                  });
                  this.props.history.push(`/score/${match.matchId}`);
                }}
              >
                <ScheduleDateCard data={match} />
                {/* <div> */}
                <div
                  style={{
                    marginLeft: 12,
                    fontFamily: "Montserrat",
                    fontWeight: 500,
                    fontSize: "12px",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "space-around"
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center"
                    }}
                  >
                    <img
                      style={{
                        width: 24,
                        height: 15,
                        boxShadow:
                          "rgba(0, 0, 0, 0.2) 0px 0px 2px 0px, rgba(0, 0, 0, 0.3) 0px 1px 3px 0px"
                      }}
                      src={
                        match.teamA.avatar ||
                        ((match.teamA.shortName == "tbc" ||
                          match.teamA.shortName == "TBC") &&
                          require("../../images/TBC-Flag.png")) ||
                        require("../../images/flag_empty.svg")
                      }
                    />
                    <span
                      style={{
                        marginLeft: 6,
                        minWidth: 28,
                        color: "#141b2f"
                      }}
                    >
                      {match.teamA.shortName &&
                        match.teamA.shortName.toUpperCase()}
                    </span>
                    <span
                      style={{
                        marginLeft: 20,
                        marginRight: 20,
                        color: "#727682"
                      }}
                    >
                      vs
                    </span>
                    <img
                      style={{
                        width: 24,
                        height: 15,
                        boxShadow:
                          "rgba(0, 0, 0, 0.2) 0px 0px 2px 0px, rgba(0, 0, 0, 0.3) 0px 1px 3px 0px"
                      }}
                      src={
                        match.teamB.avatar ||
                        ((match.teamB.shortName == "tbc" ||
                          match.teamB.shortName == "TBC") &&
                          require("../../images/TBC-Flag.png")) ||
                        require("../../images/flag_empty.svg")
                      }
                    />
                    <span
                      style={{
                        marginLeft: 6,
                        color: "#141b2f"
                      }}
                    >
                      {match.teamB.shortName &&
                        match.teamB.shortName.toUpperCase()}
                    </span>
                  </div>
                  <div
                    style={{
                      marginTop: 14,
                      display: "flex",
                      alignItems: "center"
                    }}
                  >
                    <div
                      style={{
                        display: "flex"
                      }}
                    >
                      <img
                        //   style={{ width: 11, height: 11 }}
                        src={require("../../images/colck-sh.svg")}
                      />
                      <span
                        style={{
                          marginLeft: 5,
                          color: "#141b2f"
                        }}
                      >
                        {moment(match.startDate).format("hh:mm A")}
                      </span>
                    </div>

                    <div
                      style={{
                        marginLeft: 28,
                        display: "flex"
                      }}
                    >
                      <img src={require("../../images/venue-sh.svg")} />
                      <span
                        style={{
                          marginLeft: 5,
                          color: "#141b2f"
                        }}
                      >
                        {match.venue}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <HrLine />
            </React.Fragment>
          ))
        )}
        {/* </div> */}
      </div>
    );
  }
}

export default HomeSchedule;
