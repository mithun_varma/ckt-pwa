import React, { Component } from "react";
import {
  gradientGrey,
  white,
  grey_6,
  cardShadow,
  grey_8,
  grey_10,
  gradientRedNewLeft
} from "../../styleSheet/globalStyle/color";
import { hrLineStyle } from "../../styleSheet/globalStyle/styleConfig";
import ChevronRight from "@material-ui/icons/ChevronRight";
class PointsTable extends Component {
  render() {
    const { table } = this.props;
    return (
      <div
        style={{
          borderRadius: 3,
          padding: "0 0",
          // margin: "0 16px 12px",
          boxShadow: cardShadow
        }}
      >
        {/* Title */}
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            background: gradientRedNewLeft,
            alignItems: "center",
            padding: "12px 16px",
            borderTopLeftRadius: 3,
            borderTopRightRadius: 3
          }}
          onClick={this.props.onClick}
        >
          <div
            style={{
              color: white,
              fontSize: 12,
              fontFamily: "Montserrat",
              fontWeight: 500
            }}
          >
            {this.props.title}
          </div>
          <ChevronRight style={{ color: white }} />
        </div>
        {/* title closing */}

        {/* body */}
        <div>
          {/* header */}
          <div
            style={{
              display: "flex",
              fontFamily: "Montserrat",
              fontWeight: 400,
              color: grey_8,
              fontSize: 10,
              justifyContent: "space-between",
              background: gradientGrey,
              // background: "grey",
              padding: "10px",
              alignItems: "center",
              textAlign: "center",
              textTransform: "uppercase"
            }}
          >
            <div style={{ flex: 0.24, textAlign: "left" }}>Teams</div>
            <div style={{ flex: 0.24 }}>Matches</div>
            <div style={{ flex: 0.24 }}>Points</div>
            <div style={{ flex: 0.28 }}>QUALIFICATION PROBABILITY</div>
          </div>
          <div style={hrLineStyle} />
          {/* header Closing */}

          {/* content */}
          {table.map((list, key) => (
            <div key={list.crictecTeamId}>
              <div
                style={{
                  display: "flex",
                  fontFamily: "Montserrat",
                  fontWeight: 400,
                  color: grey_6,
                  fontSize: 12,
                  background: white,
                  padding: "16px 0px",
                  alignItems: "center",
                  textAlign: "center"
                }}
              >
                <div
                  style={{ flex: 0.24, display: "flex", alignItems: "center" }}
                >
                  <div
                    style={{
                      flex: 0.5,
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <img
                      src={list.teamAvatar || "https://via.placeholder.com/150"}
                      alt=""
                      style={{
                        width: 24,
                        height: 19
                      }}
                    />
                  </div>
                  <div
                    style={{
                      flex: 0.5,
                      textAlign: "left",
                      fontFamily: "Montserrat",
                      fontWeight: 500,
                      color: grey_10
                    }}
                  >
                    {list.teamName}
                  </div>
                </div>
                <div
                  style={{
                    flex: 0.24,
                    fontFamily: "oswald400",
                    // fontWeight: 500,
                    color: grey_10
                  }}
                >
                  {list.matchesPlayed}
                </div>
                <div
                  style={{
                    flex: 0.24,
                    fontFamily: "oswald300",
                    fontWeight: 400,
                    color: grey_10
                  }}
                >
                  {list.points}
                </div>
                <div
                  style={{
                    flex: 0.28,
                    fontFamily: "oswald300",
                    fontWeight: 400,
                    color: grey_10
                  }}
                >
                  {list.qualificationProbability}
                </div>
              </div>
              <div style={hrLineStyle} />
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default PointsTable;
