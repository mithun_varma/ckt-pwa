import React, { PureComponent } from "react";
import { LazyLoadImage } from 'react-lazy-load-image-component';
import Slider from "react-slick";
export class HorizontalList extends PureComponent {
  constructor(props) {
    super(props);
    this.centerRef = {};
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.slides !== this.props.slides) {
      this.slider.slickGoTo(0);
    }
  }

  render() {
    const { callBack } = this.props;
   // this.props.slides.sort(function(a,b) {return (a.image !== null && b.image === null) ? -1 : ((b.image !== null &&  a.image === null) ? 1 : 0);} );
    // console.log(temp,"sort check");
    let settings = {
      centerMode: true,
      infinite: false,
      swipeToSlide: false,
      slidesToShow: 4,
      speed: 380,
      draggable: true,
      swipe: false,
      arrows: false,
      className: "horizontal-list-slider"
    };
    return (
      <React.Fragment>
        <div style={{ width: "100%" }}>
          <Slider ref={slider => (this.slider = slider)} {...settings}>
            {this.props.slides.length > 0 &&
              this.props.slides.map((ele, ind) => (
                <div
                  style={{
                    position: "relative",
                    overflowX: "scroll",
                    whiteSpace: "nowrap"
                  }}
                  key={ind}
                  className={ele.active ? "card-active" : "card"}
                  onClick={e => {  
                    this.slider.slickGoTo(ind);
                    callBack(ind);
                  }}
                >
                  <LazyLoadImage
                    src={ele.image !== null
                      ? ele.image
                      : require("../../images/fallbackProjection.png")} // use normal <img> attributes as props
                    width={"100%"} />
                
                </div>
              ))}
            <div />
            <div />
            <div />
          </Slider>
        </div>
      </React.Fragment>
    );
  }
}

export default HorizontalList;
