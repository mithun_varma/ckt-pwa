import React, { Component } from "react";
import { cardShadow, white } from "../../styleSheet/globalStyle/color";
import HrLine from "./HrLine";

let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  ClevertapReact.initialize("W88-4KR-845Z");
}

class FantasyBanner extends Component {
  render() {
    const { data } = this.props;
    return (
      <div
        style={{
          display: "flex",
          backgroundImage: "linear-gradient(118deg, #353e59, #1e2437)",
          margin: "12px 16px",
          borderRadius: 3,
          boxShadow: cardShadow,
          position: "relative",
          // padding: "38px 12px 16px",
          padding: "16px 12px",
          paddingBottom: 0,
          // boxShadow: cardShadow,
          transition: "all 0.5s"
        }}
      >
        <img
          style={{ position: "absolute", top: -4, right: "24px" }}
          src={require("../../images/fantasyBadge.svg")}
        />
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            width: "85%",
            backgroundImage: `url(${require("../../images/fantasybg.svg")})`,
            backgroundPosition: "right bottom",
            backgroundRepeat: "no-repeat"
          }}
        >
          <p
            style={{
              fontFamily: "Montserrat",
              fontWeight: "500",
              fontSize: 12,
              color: white,
              marginBottom: 8
            }}
          >
            {data.matchShortName}
          </p>
          <HrLine style={{ opacity: "0.2" }} />
          <p
            style={{
              fontFamily: "Montserrat",
              fontWeight: "300",
              fontSize: 12,
              color: white,
              marginTop: 8
            }}
          >
            Have you picked your Fantasy Team yet? Checkout our Fantasy Research
            Center to pick the Best team.
          </p>
          <div
            style={{
              padding: "16px 0px",
              width: "100%"
            }}
          >
            <button
              style={{
                background: "#d44030",
                border: "none",
                color: white,
                fontFamily: "Montserrat",
                fontWeight: "500",
                fontSize: 12,
                borderRadius: 18,
                padding: "6px 15px"
              }}
              className="fantasy-btn"
              onClick={() =>{
                ClevertapReact.event("Fantasy", {
                  source: "Fantasy",
                  matchId: data.matchId
                });

                this.props.history.push(`/score/${data.matchId}/fantasy`)
              }
              }
            >
              Go to Fantasy Research Center
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default FantasyBanner;
