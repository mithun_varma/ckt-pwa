import React, { Component } from "react";
import CardGradientTitle from "components-v2/commonComponent/CardGradientTitle";
import {
  white,
  cardShadow,
  red_Orange,
  gradientRedNewLeft
} from "../../styleSheet/globalStyle/color";
import ChevronRight from "@material-ui/icons/ChevronRight";
import EmptyState from "../../components-v2/commonComponent/EmptyState";
import Loader from "../../components-v2/commonComponent/Loader";

let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  ClevertapReact.initialize("W88-4KR-845Z");
}

export class ImageScrollerCArd extends Component {
  render() {
    // const {
    //   featuredPlayers,
    //   featuredPlayersLoading,
    //   similarPlayers,
    //   isNotFeatured
    // } = this.props;
    // console.log(this.props);
    return (
      <div
        style={{
          background: white,
          borderRadius: 3,
          boxShadow: cardShadow,
          marginBottom: 10
        }}
      >
        <CardGradientTitle
          title={this.props.title}
          isRightIcon={this.props.isRightIcon}
          // subtitle={this.props.hasViewAll ? "View All" : ""}

          handleCardClick={() =>
            this.props.isRightIcon ? this.props.history.push(`/players`) : ""
          }
          rootStyles={{
            background: white,
            borderRadius: 3,
            fontSize: 12
          }}
          // subtitleStyles={{
          //   color: red_Orange,
          //   marginRight: 10
          // }}
        />

        <div
          style={{
            padding: 12,
            overflowX: "scroll",
            display: "flex"
          }}
        >
          {this.props.isNotFeatured ? (
            <React.Fragment>
              {this.props.similarPlayers &&
              this.props.similarPlayers.length > 0 ? (
                this.props.similarPlayers.map((list, key) => (
                  <div
                    style={{
                      width: 190,
                      padding: "4px 0",
                      marginRight: 12
                    }}
                    key={`${key + 1}`}
                    onClick={() =>{

                  ClevertapReact.event("ImageScroller", {
                    source: "ImageScroller",
                    playerId: list.playerId
                  });
                  this.props.history.push(`/players/${list.playerId}`)
                }
                    }
                  >
                    <div
                      style={{
                        width: 190,
                        height: 180,
                        background: "#eeeeee",
                        // avatar not coming from API
                        backgroundImage: `url(${
                          list.avatar
                            ? list.avatar
                            : require("../../images/kohli.png")
                        })`,
                        backgroundPosition: "top center",
                        backgroundSize: "cover",
                        backgroundRepeat: "no-repeat",
                        borderRadius: "3px 3px 0 0"
                      }}
                    />
                    <div
                      style={{
                        background: gradientRedNewLeft,
                        padding: 8,
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "space-between",
                        borderRadius: "0 0 3px 3px"
                      }}
                    >
                      <div
                        style={{
                          fontFamily: "Montserrat",
                          fontWeight: 600,
                          color: white,
                          fontSize: 11,
                          maxWidth: "18ch",
                          whiteSpace: "nowrap"
                        }}
                      >
                        <p>{list && list.name}</p>
                        {list.country && (
                          <p style={{ fontWeight: "300" }}>India | Hard Code</p>
                        )}
                      </div>
                      <div
                        style={{
                          // background: white,
                          display: "flex"
                          // borderRadius: "50%",
                          // padding: 3
                        }}
                      >
                        <ChevronRight
                          style={{
                            color: white,
                            fontSize: 26
                          }}
                        />
                      </div>
                    </div>
                  </div>
                ))
              ) : (
                <EmptyState msg="No similar Players Found" />
              )}
            </React.Fragment>
          ) : this.props.featuredPlayersLoading &&
          this.props.featuredPlayersLoading ? (
            <Loader styles={{ height: "140px" }} noOfLoaders={1} />
          ) : this.props.featuredPlayers &&
          this.props.featuredPlayers.length > 0 ? (
            this.props.featuredPlayers.map((list, key) => (
              <div
                style={{
                  width: 190,
                  padding: "4px 0",
                  marginRight: 12
                }}
                key={`${key + 1}`}
                onClick={() => {
                  ClevertapReact.event("ImageScroller", {
                    source: "ImageScroller",
                    crictecId: list.crictecId
                  });
                  this.props.history.push(`/players/${list.crictecId}`)
                }
                }
              >
                <div
                  style={{
                    width: 190,
                    height: 180,
                    background: "#eeeeee",
                    // avatar not coming from API
                    backgroundImage: `url(${
                      list.avatar
                        ? list.avatar
                        : require("../../images/fallbackProjection.png")
                    })`,
                    backgroundPosition: "top left",
                    backgroundSize: "cover",
                    backgroundRepeat: "no-repeat",
                    borderRadius: "3px 3px 0 0"
                  }}
                />
                <div
                  style={{
                    background: gradientRedNewLeft,
                    padding: 8,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between",
                    borderRadius: "0 0 3px 3px"
                  }}
                >
                  <div
                    style={{
                      fontFamily: "Montserrat",
                      fontWeight: 600,
                      color: white,
                      fontSize: 11,
                      maxWidth: "18ch",
                      whiteSpace: "nowrap"
                    }}
                  >
                    <p>{list && list.displayName}</p>
                    {/* <p style={{ fontWeight: "300" }}>India | Hard Code</p> */}
                  </div>
                  <div
                    style={{
                      // background: white,
                      display: "flex"
                      // borderRadius: "50%",
                      // padding: 3
                    }}
                  >
                    <ChevronRight
                      style={{
                        color: white,
                        fontSize: 26
                      }}
                    />
                  </div>
                </div>
              </div>
            ))
          ) : (
            <EmptyState
              msg="No featured player found"
              rootStyles={{
                height: 170
              }}
            />
          )}
        </div>
      </div>
    );
  }
}

export default ImageScrollerCArd;
