import React, { Component } from "react";

import EmptyState from "components-v2/commonComponent/EmptyState";
import Loader from "components-v2/commonComponent/Loader";
import { cardShadow } from "../../styleSheet/globalStyle/color";


import kolkata from "../../images/3x/kolkata@3x.png"
import cuttack from "../../images/3x/cuttack@3x.png"
import dehradun from "../../images/3x/dehradun@3x.png"
import delhi from "../../images/3x/delhi@3x.png"
import dharamshala from "../../images/3x/dharamshala@3x.png"
import guwahati from "../../images/3x/guwahati@3x.png"
import hyderabad from "../../images/3x/hyderabad@3x.png"
import indore from "../../images/3x/indore@3x.png"
import kanpur from "../../images/3x/kanpur@3x.png"
import lucknow from "../../images/3x/lucknow@3x.png"
import mohali from "../../images/3x/mohali@3x.png"
import mumbai from "../../images/3x/mumbai@3x.png"
import nagpur from "../../images/3x/nagpur@3x.png"
import noida from "../../images/3x/noida@3x.png"
import rajkot from "../../images/3x/rajkot@3x.png"
import ranchi from "../../images/3x/ranchi@3x.png"
import thiruvananthapuram from "../../images/3x/thiruvananthapuram@3x.png"
import vishakapatnam from "../../images/3x/vishakapatnam@3x.png"

class ImageContainer extends Component {
  constructor(props) {
    super(props);
    this._handelPlayerClick = this._handelPlayerClick.bind(this);
  }
  _handelPlayerClick(event) {
    const id = event.target.alt;
    this.props.history.push("/players/:" + id);
  }
  componentDidMount() {
    // window.addEventListener('scroll', this.handleScroll,true);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll(event) {
    var element = document.getElementById("image");
    var topPos = element.getBoundingClientRect().top + window.scrollX;
  }


  mapAvatars = () =>{
    (this.props.isAvatar === true ) 
    {
    } 
  }

  render() {

    this.mapAvatars();
    return (
      <div style={{ ...pageStyle.container, ...this.props.rootStyles }}>
        <div style={pageStyle.headerWrapper}>
          <div style={pageStyle.header}>{this.props.title}</div>
          {this.props.poweredStadium && (
            <div style={pageStyle.featuredHeader}>
              <div>Powered By</div>
              <div>
                Criclytics 
                <span> &trade;</span>
              </div>
            </div>
          )}
        </div>
        <div style={pageStyle.hrLine} />

        {this.props.isAvatar ? (
          <div style={pageStyle.avatarWrapper}>
            {this.props.data.map(item => (
              <div
                style={pageStyle.avatarContainer}
                name="image"
                id={item.crictecPlayerId}
                key={item.crictecPlayerId}
                onClick={this._handelPlayerClick}
              >
                <div>
                  <img
                    src={require("../../images/place_holder_1.png")}
                    alt={item.crictecPlayerId}
                    width="86"
                    height="110"
                  />
                </div>
                <div style={pageStyle.avatarName}>{item.name}</div>
              </div>
            ))}
          </div>
        ) : (
          <div style={pageStyle.imageWrapper}>
            {this.props.loading ? (
              <Loader styles={{ height: "175px" }} noOfLoaders={1} />
            ) : this.props.data && this.props.data.length ? (
              this.props.data.map(item => (
                <div
                  style={pageStyle.imageContainer}
                  onClick={() => this.props.getGroundDetails(item)}
                  name="image"
                  key={item.name}
                >
                  <div
                    style={{
                      width: 120,
                      height: 120,
                      position: "relative"
                    }}
                  >
                    <img
                      src={
                        item.avatar
                          ? item.avatar
                          : "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
                      }
                      alt="Snow"
                      style={pageStyle.imageStyle}
                    />
                    <div
                      style={{
                        position: "absolute",
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        background:
                          "linear-gradient(30deg, #eb684f, rgba(114, 118, 130, 0.09))",
                        opacity: 0.9,
                        ...this.props.backgroundStyles
                      }}
                    />
                  </div>
                  <div style={pageStyle.imageName}>{item.name}</div>
                </div>
              ))
            ) : (
              <EmptyState
                msg={`No ${this.props.title}`}
                rootStyles={{
                  height: 200
                }}
              />
            )}
          </div>
        )}
      </div>
    );
  }
}

// https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png
export default ImageContainer;

const pageStyle = {
  container: {
    display: "flex",
    flexDirection: "column",
    margin: "0 0 0 16px",
    marginTop: -150,
    borderRadius: 3,
    background: "#FFFFFF",
    boxShadow: cardShadow
  },
  headerWrapper: {
    // padding: "12px 16px",
    fontFamily: "Montserrat",
    fontWeight: 600,
    fontSize: 10,
    display: "flex",
    flex: 1,
    justifyContent: "space-between"
  },
  header: {
    margin: "12px 0px 12px 16px"
  },
  featuredHeader: {
    background: "#edeff4",
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "center",
    padding: "0px 22px"
  },
  hrLine: {
    height: 1,
    background: "linear-gradient(to right, #ffffff, #e3e4e6, #e3e4e6, #ffffff)"
  },
  avatarWrapper: {
    overflowX: "scroll",
    whiteSpace: "nowrap",
    display: "flex",
    background: "#FFF",
    borderRadius: 3
  },
  avatarContainer: {
    display: "inline-block",
    padding: "20px 18px 18px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatarName: {
    fontFamily: "Montserrat",
    fontWeight: 400,
    fontSize: 10,
    color: "#141b2f",
    textAlign: "center",
    whiteSpace: "initial"
  },
  imageWrapper: {
    overflowX: "scroll",
    whiteSpace: "nowrap",
    display: "flex"
  },
  imageContainer: {
    position: "relative",
    padding: "12px 0px 12px 16px"
  },
  imageStyle: {
    width: 120,
    height: 120
  },
  imageName: {
    position: "absolute",
    bottom: 22,
    left: 22,
    right: 22,
    color: "#FFFFFF",
    fontSize: 10,
    fontFamily: "Montserrat",
    fontWeight: 600,
    whiteSpace: "initial"
  }
};
