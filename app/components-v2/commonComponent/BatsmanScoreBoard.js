import React, { Component } from 'react';
import { screenWidth, screenHeight } from '../../styleSheet/screenSize/ScreenDetails';
import { gradientGrey } from '../../styleSheet/globalStyle/color';
import HrLine from './HrLine';

const dataBatsman =[
    {
        'name':'Aron Finch',
        'run':'78',
        'ball':'45',
        'fours':'9',
        'sixes':'1',
        'strikeRate':'1.89',
        'outBy':'manas',
        'catchBy':'kartik',
    },
    {
        'name':'Aron Finch',
        'run':'78',
        'ball':'45',
        'fours':'9',
        'sixes':'1',
        'strikeRate':'1.89',
        'outBy':'manas',
        'catchBy':'kartik',
    },
    {
        'name':'Aron Finch',
        'run':'78',
        'ball':'45',
        'fours':'9',
        'sixes':'1',
        'strikeRate':'1.89',
        'outBy':'manas',
        'catchBy':'kartik',
    },
    {
        'name':'Aron Finch',
        'run':'78',
        'ball':'45',
        'fours':'9',
        'sixes':'1',
        'strikeRate':'1.89',
        'outBy':'manas',
        'catchBy':'kartik',
    },
    {
        'name':'Aron Finch',
        'run':'78',
        'ball':'45',
        'fours':'9',
        'sixes':'1',
        'strikeRate':'1.89',
        'outBy':'manas',
        'catchBy':'kartik',
    }
]

export default class BatsmanScoreCard extends Component{
    constructor(props){
        super(props);
        this.state={
            dataBatsman:[]
        }

    }
    componentDidMount(){
        const batsmanScore = this.props.batsmanScore;
        this.setState({dataBatsman:batsmanScore});

    }
    total(){
        const {extraField} = this.props
        return(
            <div style={{paddingLeft:"12px", color:'#fff',alignItems:'center',backgroundImage:'linear-gradient(to right, #727682, #141b2f)',display:'flex', flexDirection:'row', flex:1, width:screenWidth*0.911, height:screenHeight*0.07}}>
              <div style={{display:'flex', flex:0.20,justifyContent:'flex-start'}}>
                Total
              </div>
              <div style={{display:'flex', flex:0.60,justifyContent:'center'}}>
                {null}
              </div>
              <div style={{display:'flex', flex:0.20,justifyContent:'center'}}>
              {  extraField[1].runs+" / "+extraField[1].wickets}
              </div>
            </div>
        );

    }
    extra(){
        const {extraField} = this.props;
        return(
            <div style={{paddingLeft:"12px",color:'#141b2f',alignItems:'center',display:'flex', flexDirection:'row', flex:1,width:screenWidth*0.911, height:screenHeight*0.07}}>
                <div style={{display:'flex', flex:0.40, justifyContent:'flex-start'}}>
                    Extras
                </div>
                <div style={{display:'flex', flex:0.01, justifyContent:'center'}}>
                    {extraField[1].extras}
                </div>
                <div style={{display:'flex', flex:0.59, justifyContent:'flex-start'}}>
                  { " ("+" B: "+ extraField[1].byes+","+ " LB: "+extraField[1].legByes+","+" W: "+extraField[1].wide+","+" NB: "+ extraField[1].noballs+")"}
                </div>

            </div>
        );

    }
    render(){
        return(
            <div style={pageStyle.batsmanContainer}>
                <div style={pageStyle.batsmanScoreHeadingContainer}>
                    <div style={pageStyle.batsmanHeadingBatsman}>
                        BATSMAN

                    </div>
                    <div style={pageStyle.batsmanHeadingOther}>
                        R

                    </div>
                    <div style={pageStyle.batsmanHeadingOther}>
                        B

                    </div>
                    <div style={pageStyle.batsmanHeadingOther}>
                        4s

    `               </div>
                    <div style={pageStyle.batsmanHeadingOther}>
                        6s

                    </div>
                    <div style={pageStyle.batsmanHeadingOther}>
                        SR
                    </div>
                </div>
                <div>
                {
                    (this.state.dataBatsman).map((data)=>{
                        const batsmanName = data.playerName;
                        const batsman = data['battingStatistics'][1];
                        const balls = batsman.balls;
                        const runs = batsman.runs;
                        const fours = batsman.fours;
                        const sixes = batsman.sixes;
                        const strikeRate =batsman.strikeRate;
                        const isDismissed =batsman.dismissed;
                        const strDismissed = batsman.outStr;

                        return(
                            <div style={pageStyle.batsmanScoreConatiner}>
                                <div style={pageStyle.batsmanScoreDetails}>
                                    <div style={pageStyle.batsmanScoreName}>
                                        {batsmanName}
        
                                    </div>
                                    <div style={pageStyle.batsmanScoreRun}>
                                        {runs}
        
                                    </div>
                                    <div style={pageStyle.batsmanScoreOther}>
                                        {balls}
        
                                    </div>
                                    <div style={pageStyle.batsmanScoreOther}>
                                        {fours}
        
                                    </div>
                                    <div style={pageStyle.batsmanScoreOther}>
                                        {sixes}
        
                                    </div>
                                    <div style={pageStyle.batsmanScoreOther}>
                                        {strikeRate}
                                    </div>
        
                                </div>
                                {   (isDismissed)?
                                    <div style={pageStyle.batsmanOutDetails}>
                                        <div style={pageStyle.batsmanOutFirstDetails}>
                                                {strDismissed}
                                        </div>
                                        <div style={pageStyle.batsmanOutSecondDetails}>
                                                {null}
                                        </div>
            
                                    </div>
                                    :
                                    <div>
                                        {null}
                                    </div>
                                }
                                <div style={{display:'flex',flex:1}}>
                                   <HrLine />
                                </div>
                            </div>
                        );
                    })

                   
                }
                </div>
                <div >
                    {
                        (this.props.extra==='NA') ?
                        <div>
                            {null}
                        </div>
                        :
                        <div>
                            {this.extra()}
                        </div>
                    
                    }
                </div>
                <div >
                    
                         {
                            (this.props.total==='NA') ?
                            <div>
                                {null}
                            </div>
                            :
                            <div>
                                {this.total()}
                            </div>
                        
                        }
                    
                    
                </div>
            </div>
        );
    }

}

const pageStyle ={
    batsmanScoreOther:{
        display:'flex',
        flex:0.10,
        color:'#141b2f',
        fontFamily:'Montserrat-Medium',
        fontSize:'12px',
        justifyContent:'flex-start',

    },
    batsmanScoreRun:{
        display:'flex',
        flex:0.10,
        color:'#141b2f',
        fontFamily:'Montserrat-Medium',
        fontSize:'12px',
        justifyContent:'flex-start',
        fontWeight:'700',
    },
    batsmanScoreName:{
        display:'flex',
        flex:0.50,
        color:'#141b2f',
        fontFamily:'Montserrat-Medium',
        fontSize:'12px',
        justifyContent:'flex-start',
        fontWeight:'700',
    },
    batsmanScoreDetails:{
        display:'flex',
        flexDirection:'row',
        paddingTop:'10px',
        fontSize:'10px',
        flex:1,

    },
    batsmanOutSecondDetails:{
        display:'flex',
        flex:0.20,
        justifyContent:'flex-start'

    },
    batsmanOutFirstDetails:{
        display:'flex',
        flex:0.80,
        justifyContent:'flex-start',
    },
    batsmanOutDetails:{
        display:'flex',
        flexDirection:'row',
        flex:1,
        paddingBottom:'10px',
        paddingTop:'10px',
        opacity:0.70,
        color:'#141b2f',
        fontSize:'11px',
        fontFamily:'Montserrat-Regular',

    },
    batsmanScoreConatiner:{
        display:'flex',
        flexDirection:'column',
        flex:1,
        paddingLeft:'12px',
    },
    batsmanHeadingOther:{
        display:'flex',
        flex:0.10,
        color:'#727682',
        fontSize:'10px',
        justifyContent:'flex-start',

    },
    batsmanHeadingBatsman:{
        display:'flex',
        flex:0.50,
        color:'#727682',
        fontSize:'10px',
        justifyContent:'flex-start',

    },
    batsmanScoreHeadingContainer:{
        display:'flex',
        flexDirection:'row',
        flex:1,
        paddingTop:'10px',
        paddingBottom:'10px',
        paddingLeft:'12px',
        backgroundImage:gradientGrey,
        color:'#727682'
        
    },
    batsmanContainer:{
        display:'flex',
        flexDirection:'column',
        flex:1,
        width:screenWidth*0.911,
        backgroundColor:"#fff",
    },


}