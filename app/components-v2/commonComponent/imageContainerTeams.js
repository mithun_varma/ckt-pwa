import React, { Component } from "react";
import EmptyState from "components-v2/commonComponent/EmptyState";
import { cardShadow } from "../../styleSheet/globalStyle/color";
import { screenHeight, screenWidth } from "../../styleSheet/screenSize/ScreenDetails";

// import InfiniteScroll from "react-infinite-scroll-component
class ImageContainer extends Component {
  constructor(props){
    super(props);
    this.state={
      loading:true,
      callApi:true,
    }
    this._handelPlayerClick = this._handelPlayerClick.bind(this);
    this.fetchMoreData = this.fetchMoreData.bind(this);
    this.handleScroll = this.handleScroll.bind(this);

  }
  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll,true);
  }

  componentWillUnmount() {
      window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll(event) {
      var element = event.target;
      const leftPosition = element.scrollLeft;
      // window.setTimeout(
      //   ()=>{
      //     this.setState({callApi:true});
      //   }, 
      //   3000
      // );

      if(leftPosition>1100 && this.state.callApi){
        this.setState({callApi:false},()=>{this.state.callApi});
        this.callNextData(element.id);
        
      }
      
  }
  callNextData(type){
    this.props.getMoreTeamDetails(type);
    
  }
  _handelPlayerClick(event){
    const id = event.target.id;
    this.props.history.push("/teams/"+id);
  }
  fetchMoreData(){
    // console.log("fetch more");
  }

  render() {
    var rawData = this.props.data;
    const data = rawData.filter(function (el) {
      return el != null;
    });

    return (
      <div style={{ ...pageStyle.container, ...this.props.rootStyles }} id={this.props.title} >
        <div style={pageStyle.headerWrapper}>
          <div style={pageStyle.header}>{this.props.title}</div>
        </div>
        <div style={pageStyle.hrLine} />
        {this.props.isAvatar ? (
          <div style={pageStyle.avatarWrapper}>
            {data.map(item => 
            (
              <div style={pageStyle.avatarContainer} key={item.teamId} alt={item.teamId} id={item.teamId} onClick={this._handelPlayerClick}> 
                  <img
                    src={
                      item.avatar
                        ? item.avatar
                        : require("../../styleSheet/svg/AustralianFlag.svg")
                    }
                    alt={item.teamId}
                    width="100px"
                    height="65px"
                    alt={item.teamId} id={item.teamId}
                    style={{boxShadow:'1px 1px 3px 0px #999999'}}
                  />
                <div alt={item.teamId} id={item.teamId} style={pageStyle.avatarName}>{item.name}</div>
              </div>
            )
            )}
          </div>
        ) : (
          <div style={pageStyle.imageWrapper}>
            {
            data && data.length ? (
              data.map((item,i) => {
                if(!this.props.isBlack){
                  return(
                    <div style={pageStyle.imageContainer} onClick={this._handelPlayerClick} key={item.teamId} alt={item.teamId} id={item.teamId}>
                      <div style={{ width: '120px', height: '120px', position: "relative" }} >
                        <img
                          src={
                            item.image
                              ? item.image
                              : "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
                          }
                          alt={item.teamId}
                          style={pageStyle.imageStyle}
                        />
                        <div alt={item.teamId} id={item.teamId}
                          style={{position: "absolute",top: 0,bottom: 0,left: 0,right: 0,background:"linear-gradient(30deg, #141b2f, rgba(114, 118, 130, 0.09))",opacity: 0.9,height:'120px',width:'120px', borderRadius:'2px',}}/>
                      </div>
                      <div style={pageStyle.imageName} alt={item.teamId}>{item.name}</div>
                    </div>
                  )
                }
                else{
                  return(
                    <div style={pageStyle.imageContainer} onClick={this._handelPlayerClick} key={item.teamId} alt={item.teamId} id={item.teamId}>
                      <div style={{ width: '120px', height: '120px', position: "relative" }} >
                        <img
                          src={
                            item.image
                              ? item.image
                              : "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
                          }
                          alt={item.teamId}
                          style={pageStyle.imageStyle}
                        />
                        <div alt={item.teamId} id={item.teamId}
                          style={{position: "absolute",top: 0,bottom: 0,left: 0,right: 0,background:"linear-gradient(30deg, #eb684f, rgba(114, 118, 130, 0.09))",opacity: 0.9,height:'120px',width:'120px', borderRadius:'2px',}}/>
                      </div>
                      <div style={pageStyle.imageName}>{item.name}</div>
                    </div>
                  )
                }
              })
            ) : (
              <EmptyState
                msg={`No ${this.props.title}`}
                rootStyles={{
                  height: 200
                }}
              />
            )}
          </div>
        )}
      </div>

    
    );
  }
}

// https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png
export default ImageContainer;

const pageStyle = {
  container: {
    display:'flex',
    flexDirection:'column',
    marginLeft:'16px',
    marginRight: '16px',
    marginTop: 0,
    borderRadius: 3,
    background: "#FFFFFF",
    boxShadow: cardShadow,
    // width:screenWidth*0.911,
  },
  headerWrapper: {
    // padding: "12px 16px",
    fontFamily: "Montserrat",
    fontWeight: 600,
    fontSize: 12,
    display: "flex",
    flex: 1,
    justifyContent: "space-between"
  },
  header: {
    padding: "12px 0px 12px 16px",
    fontFamily:'Montserrat',
    fontSize:'12px',
    fontWeight:'500',
    color:'#141b2f',
  },
  featuredHeader: {
    background: "#edeff4",
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "center",
    padding: "0px 22px"
  },
  hrLine: {
    height: 1,
    background: "linear-gradient(to right, #ffffff, #e3e4e6, #e3e4e6, #ffffff)"
  },
  avatarWrapper: {
    overflowX: "scroll",
    display: "flex",
    background: "#FFF",
    borderRadius: 3
  },
  avatarContainer: {
    display: "inline-block",
    padding: "20px 18px 0px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    height:screenHeight*0.19,
    // overflow:'scroll'
  },
  avatarName: {
    fontFamily: "Montserrat",
    fontWeight: 500,
    fontSize: 12,
    color: "#141b2f",
    textAlign: "center",
    whiteSpace: "initial"
  },
  imageWrapper: {
    overflowX: "scroll",
    whiteSpace: "nowrap",
    display: "flex",
    marginLeft:'2px',
   
  },
  imageContainer: {
    position: "relative",
    paddingTop:'12px',
    paddingBottom:'12px',
    paddingLeft:'12px',
   
   
  },
  imageStyle: {
    width: '120px',
    height: '120px',
    borderRadius:'2px',
  },
  imageName: {
    position: "absolute",
    bottom: "24px",
    left: "24px",
    right: "24px",
    width:'100px',
    color: "#FFFFFF",
    fontSize: '12px',
    fontFamily: "Montserrat",
    whiteSpace: "initial"
  }
};