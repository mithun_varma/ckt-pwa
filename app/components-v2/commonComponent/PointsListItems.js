import React, { Component } from "react";
import {
  grey_10,
  grey_8,
  white,
  avatarShadow,
  green_10
} from "../../styleSheet/globalStyle/color";

export class PointsListItems extends Component {
  render() {
    const {
      name,
      match,
      win,
      loss,
      tie,
      noResult,
      points,
      netRunRate,
      avatar,
      isHeader,
      qualified
    } = this.props;
    return (
      <div
        style={{
          display: "flex",
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center",
          color: isHeader ? grey_8 : grey_10,
          fontSize: isHeader ? 10 : 12,
          fontFamily: "Montserrat",
          padding: "16px 0",
          fontWeight: isHeader ? 400 : 500,
          ...this.props.rootStyles
        }}
      >
        <div
          style={{
            flex: 0.32,
            display: "flex",
            paddingLeft: 10,
            alignItems: "center"
          }}
        >
          <div
            style={{
              width: 16,
              height: 15,
              background: qualified ? green_10 : "transparent",
              color: white,
              fontFamily: "Oswald",
              fontWeight: 600,
              marginRight: 4,
              fontSize: 11,
              display: "flex",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            {qualified ? "Q" : ""}
          </div>
          {!isHeader && (
            <img
              src={avatar}
              alt=""
              style={{
                width: 20,
                height: 14,
                boxShadow: avatarShadow,
                marginRight: 6
              }}
            />
          )}
          <span>{name}</span>
        </div>
        <div style={{ flex: 0.09, fontWeight: isHeader ? 400 : 600 }}>
          {match}
        </div>
        <div style={{ flex: 0.09 }}>{win}</div>
        <div style={{ flex: 0.09 }}>{loss}</div>
        <div style={{ flex: 0.09 }}>{tie}</div>
        <div style={{ flex: 0.09 }}>{noResult}</div>
        <div style={{ flex: 0.09 }}>{points}</div>
        <div style={{ flex: 0.14 }}>{netRunRate}</div>
      </div>
    );
  }
}

export default PointsListItems;
