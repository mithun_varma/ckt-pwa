import React, { Component } from "react";
import Modal from 'react-responsive-modal';
import {
  gradientGrey,
} from "../../styleSheet/globalStyle/color";
import { screenWidth, screenHeight } from "../../styleSheet/screenSize/ScreenDetails";
import aus_log from './../../styleSheet/images/aus_logo.png';
import HrLine from "./HrLine";
import EmptyState from "./EmptyState";




export default class TeamModalTrophy extends Component{
    constructor(props){
        super(props);
        this.state={
            showModal:false,
            trophyArray:[],


        };
        this._handelOpenModal = this._handelOpenModal.bind(this);
        this._handleCloseModal = this._handleCloseModal.bind(this);


    }
    componentDidMount(){
        const {trophyData} = this.props;
        this.setState({trophyArray:trophyData});

    }

    _handelOpenModal(){
       

    }
    _handleCloseModal(){
        this.props.closeTrophy();
        

    }
    render(){
        return (
            <div style={pageStyle.modalContainer}>
                <Modal open={true} closeIconSize ={'20'} showCloseIcon={true} onClose={this._handleCloseModal} center style={pageStyle.trophyCabinetContainer}>
                    {this.trophyCabinet()}
                </Modal>
            </div>
        );
    }
    trophyCabinet(){
        return(
            <div>
                <div style={pageStyle.title}>
                    Trophy Cabinet 
                </div>
                <div style={pageStyle.trophyList}>
                    { this.trophyList()}
                </div>
            </div>
        );
    }
    trophyList(){
        const {trophyArray} = this.state;
        if(trophyArray != null){
            return(
                <div style={pageStyle.trophyListContainer}>
                    {
                        trophyArray.map((data)=>{
                            const image = data.avatar || aus_log;
                            return(
                                <div>
                                    <div style={pageStyle.trophyListRow}>
                                        <div style={pageStyle.trophy}>
                                            <img src={image} alt = "trophy image" height="41px" width= "41px" /> 
                                        </div>
                                        <div style ={pageStyle.trophyText}>
                                            <div style={pageStyle.trophyTextName}>
                                                {data.name}
                                            </div>
                                            <div  style ={pageStyle.trophyTextYear}>
                                                {data.year}
                                            </div>
                                        </div>
                                    </div>
                                    <HrLine/>
                                </div>
                            )
                        })

                    }
                </div>
            );
        }
        else{
            return(
                <div>
                    <EmptyState msg="No trophy found for this team" />
                </div>
            );
        }
    }

}

const pageStyle = {

    trophyList:{
        display:'flex',
        flex:1,
    },
    title:{
        display:'flex',
        flex:1,
        background:gradientGrey,
        alignItems:'center',
        justifyContent:'flex-start',
        color:'#141b2f',
        height:screenHeight*0.0609,
        marginTop:'-15px',
        fontSize:'12px',

    },
    modalContainer:{
        display:'flex',
        flex:1, 
        zIndex:'9999',
        position:'absolute',
        fontFamily:'Montserrat',
    },
    trophyCabinetContainer:{
        display:'flex',
        flex:1,
        flexDirection:'column',
        width:screenWidth*0.911,
        backgroundColor:'#fff',
    },
    trophyListContainer:{
        display:'flex',
        flexDirection:'column',
        flex:1,
        width:screenWidth*0.80,
    },
    trophyListRow:{
        display:'flex',
        flexDirection:'row',
        flex:1,
        alignItems:'center',
        width:screenWidth*0.80,
        height:screenHeight*0.123,

    },
    tophyField:{
        display:'flex',
        flex:1,
        flexDirection:'row',
        height:screenHeight*0.1218,
        width:screenWidth*0.911,
        backgroundColor:"#fff",
    },
    trophy:{
        display:'flex',
        flex:0.10,
        alignItems:'center',
        justifyContent:'flex-start',
        border:'1px solid #fa9441',
        borderRadius:'50%',
   
    },
    trophyText:{
        display:'flex',
        justifyContent:'flex-start',
        alignItems:"center",
        flexDirection:'column',
        paddingLeft:'16px',
    },
    trophyTextName:{
        display:'flex',
        justifyContent:'flex-start',
        alignItems:"center",
        color:'#141b2f',
        fontSize:'12px',

    },
    trophyTextYear:{
        display:'flex',
        justifyContent:'flex-start',
        alignItems:"center",
        color:'#141b2f',
        fontSize:'12px',
        letterSpacing:'0.33px',
        fontWeight:'700',
 }
}