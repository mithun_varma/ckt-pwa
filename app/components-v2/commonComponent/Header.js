import React, { Component } from "react";
import ArrowBack from "@material-ui/icons/ArrowBack";
import Share from "@material-ui/icons/Share";
import { white, gradientOrange } from "../../styleSheet/globalStyle/color";

export class Header extends Component {
  render() {
    const {
      title,
      isHeaderBackground,
      leftArrowBack,
      textColor,
      isShare,
      backgroundColor
    } = this.props;
    return (
      <div
        style={{
          position: "fixed",
          top: 0,
          left: 0,
          right: 0,
          // background: "#ea6550",
          background: isHeaderBackground ? backgroundColor : "transparent",
          // background: isHeaderBackground ? gradientOrange : "transparent",
          display: "flex",
          flex: 1,
          alignItems: "center",
          zIndex: 999,
          textTransform: "capitalize",
          // minHeight: 56,
          minHeight: 42,
          marginBottom: 16,
          padding: "0 14px",
          justifyContent: "space-between",
          color: textColor
          // transform: "translateZ(0)",
          // transition: "all .3s"
          // transitionDelay: "2s",
          // opacity: 1
        }}
      >
        <div
          style={{
            display: "flex",
            width: "94%",
            alignItems: "center"
          }}
        >
          {leftArrowBack && (
            <div
              style={{
                display: "flex"
              }}
              onClick={e => {
                e.preventDefault();
                this.props.leftIconOnClick();
              }}
            >
              <ArrowBack />
            </div>
          )}

          {this.props.title !== "Criclytics" ? (
            <div
              style={{
                whiteSpace: "nowrap",
                overflow: "hidden",
                textOverflow: "ellipsis",
                width: "90%",
                paddingLeft: 8,
                fontFamily: "Montserrat",
                fontSize: this.props.titleFont || 22,
                fontWeight: 600,
                ...this.props.titleStyles
              }}
            >
              {this.props.title}
            </div>
          ) : (
            <div
              style={{
                whiteSpace: "nowrap",
                overflow: "hidden",
                textOverflow: "ellipsis",
                width: "90%",
                paddingLeft: 8,
                fontFamily: "Montserrat",
                fontSize: this.props.titleFont || 22,
                fontWeight: 600,
                ...this.props.titleStyles
              }}
            >
              {this.props.title}
              <span style={{ fontSize: "11px", verticalAlign: "top" }}>
                TM
              </span>
            </div>
          )}
        </div>
        {isShare && (
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end"
            }}
          >
            <Share />
          </div>
        )}
      </div>
    );
  }
}

export default Header;
