import React from "react";
import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  Dot,
  YAxis,
  Label,
  Tooltip
} from "recharts";

const LabelTop = props => {
  return (
    <svg
      x={props.coordinate.x + 1000}
      y={props.coordinate.y + 10}
      xmlns="http://www.w3.org/2000/svg"
      width="149"
      height="40"
      viewBox="0 0 149 40"
    >
      <g fill="none" fillRule="evenodd">
        <path
          stroke="#A1A4AC"
          d="M37 .5v29h108.5A2.5 2.5 0 0 0 148 27V3a2.5 2.5 0 0 0-2.5-2.5H37z"
        />
        <path
          fill="#141B2F"
          d="M15.693 30l5.59 10 6.379-10H42.5V0h-39a3 3 0 0 0-3 3v24a3 3 0 0 0 3 3h12.193z"
        />
        <text
          fill="#FFF"
          fontFamily="Montserrat-SemiBold, Montserrat"
          fontSize="14"
          fontWeight="500"
          transform="translate(.5)"
        >
          <tspan x="7" y="20">
            62%
          </tspan>
        </text>
        <text
          fill="#727682"
          fontFamily="Montserrat-Medium, Montserrat"
          fontSize="14"
          fontWeight="400"
          transform="translate(.5)"
        >
          <tspan x="48" y="20">
            4% INDIA
          </tspan>
        </text>
        <path
          fill="#727682"
          d="M82.688 12.612l3.85 3.85.962-.962L82 10l-5.5 5.5.962.963 3.85-3.85V20h1.376z"
        />
      </g>
    </svg>
  );
};

const returnCustomLabel = props => {
  return props.payload.DiffPoint ? (
    <svg
      // onClick={console.log("hello")}
      key={Math.random()}
      x={props.cx - 10}
      y={props.cy - 10}
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width="30"
      height="30"
      viewBox="0 0 30 30"
    >
      <defs>
        <circle id="b" cx="11" cy="11" r="11" />
        <filter
          id="a"
          width="163.6%"
          height="163.6%"
          x="-31.8%"
          y="-22.7%"
          filterUnits="objectBoundingBox"
        >
          <feOffset dy="2" in="SourceAlpha" result="shadowOffsetOuter1" />
          <feGaussianBlur
            in="shadowOffsetOuter1"
            result="shadowBlurOuter1"
            stdDeviation="2"
          />
          <feColorMatrix
            in="shadowBlurOuter1"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25206635 0"
          />
        </filter>
        <linearGradient id="c" x1="100%" x2="0%" y1="50%" y2="50%">
          <stop offset="0%" stopColor="#EA6550" />
          <stop offset="100%" stopColor="#FA9441" />
        </linearGradient>
      </defs>
      <g fill="none" fillRule="nonzero" transform="translate(4 1.84)">
        <use fill="#000" filter="url(#a)" xlinkHref="#b" />
        <use fill="#FFF" xlinkHref="#b" />
        <circle cx="11" cy="11" r="5" fill="url(#c)" />
      </g>
    </svg>
  ) : null;
};

export const LineGraph = props => {
  return (
    <LineChart width={300} height={300} data={props.data}>
      <Line
        dot={returnCustomLabel}
        type="linear"
        dataKey={"perc"}
        stroke={props.color || "#8884d8"}
      />

      <CartesianGrid vertical={false} stroke="#d3d3d3" strokeDasharray="0" />
      <XAxis tickCount={5} stroke="#727682" dataKey={"overNo"} />
    </LineChart>
  );
};
