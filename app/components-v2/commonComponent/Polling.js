import React, { Component } from "react";
import {
  green_6,
  red_4,
  grey_8,
  grey_2,
  red_10
} from "../../styleSheet/globalStyle/color";

class Polling extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {}
  render() {
    const { data, score } = this.props;
    // console.log(data, 'polling')
    const teamA = data && data.teamA;
    const teamAPercentageValue =
      data && data.teamAPercentage ? Number(data.teamAPercentage) : 0;
    const teamBPercentageValue =
      data && data.teamBpercentage ? Number(data.teamBpercentage) : 0;
    const teamAName =
      score && score.status == "UPCOMING"
        ? score.teamA && score.teamA.teamKey == teamA
          ? score.teamA.shortName
          : score.teamB.shortName
        : "";
    const teamAPercentage = data ? data.teamAPercentage + "%" : "0%";
    const teamB = data && data.teamB;
    const teamBName =
      score && score.status == "UPCOMING"
        ? score.teamB && score.teamB.teamKey == teamB
          ? score.teamB.shortName
          : score.teamA.shortName
        : "";
    const teamBPercentage = data ? data.teamBpercentage + "%" : "0%";
    const tie = score && score.format == "TEST" ? "Draw" : "Tie";
    const tiePercent =
      data && data.tiePercent && data.tiePercent != "0" ? data.tiePercent : "";
    const tiePercentage =
      data && data.tiePercent ? data.tiePercent + "%" : "0%";
    return (
      <div style={{ ...this.props.style }}>
        {teamA && teamB ? (
          <div
            style={{
              display: "flex",
              flex: 1,
              position: "relative",
              padding: "2px 0px",
              minHeight: 47
            }}
          >
            {tiePercent && (
              <div
                style={{
                  position: "absolute",
                  top: 2,
                  left: "50%",
                  fontSize: 12,
                  fontWeight: 600,
                  fontFamily: "Montserrat",
                  color: grey_8,
                  display: "flex",
                  flex: 1,
                  flexDirection: "column",
                  justifyContent: "space-between",
                  minHeight: 42
                }}
              >
                <div>{tiePercentage}</div>
                <div
                  style={{
                    fontWeight: 500,
                    fontSize: 10
                  }}
                >
                  {tie}
                </div>
              </div>
            )}
            <div style={{ width: teamAPercentage }}>
              <div
                style={{
                  color:
                    teamAPercentageValue > teamBPercentageValue
                      ? green_6
                      : red_4,
                  fontFamily: "Montserrat",
                  fontWeight: 600,
                  fontSize: 12
                }}
              >
                {teamAPercentage}
              </div>
              <div
                style={{
                  width: "100%",
                  // background: green_6,
                  background:
                    teamAPercentageValue > teamBPercentageValue
                      ? green_6
                      : red_10,
                  height: 5,
                  borderBottomLeftRadius: 50,
                  borderTopLeftRadius: 50,
                  margin: "3px 0 2px"
                }}
              />
              {false && score.status == "UPCOMING" ? (
                <div
                  style={{
                    color: grey_8,
                    fontFamily: "Montserrat",
                    fontWeight: 500,
                    fontSize: 10
                  }}
                >
                  {teamAName ? teamAName.substring(0, 3).toUpperCase() : ""}
                </div>
              ) : (
                <div
                  style={{
                    color: grey_8,
                    fontFamily: "Montserrat",
                    fontWeight: 500,
                    fontSize: 10
                  }}
                >
                  {teamA ? teamA.substring(0, 3).toUpperCase() : ""}
                </div>
              )}
            </div>

            {tiePercent && (
              <div
                style={{
                  width: tiePercentage,
                  color: "transparent"
                }}
              >
                <div
                  style={{
                    // color: grey_8,
                    // color: "transparent",
                    fontFamily: "Montserrat",
                    fontWeight: 600,
                    fontSize: 12
                  }}
                >
                  {/* {tiePercentage} */}
                  {tiePercent}
                </div>
                <div
                  style={{
                    width: "100%",
                    background: grey_2,
                    height: 5,
                    margin: "3px 0 2px"
                    // borderTopRightRadius: 50,
                    // borderBottomRightRadius: 50
                  }}
                />
                <div
                  style={{
                    // color: grey_8,
                    fontFamily: "Montserrat",
                    fontWeight: 500,
                    fontSize: 10
                  }}
                >
                  {/* {tie} */}
                </div>
              </div>
            )}

            <div
              style={{
                width: teamBPercentage,
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-end"
              }}
            >
              <div
                style={{
                  // color: red_4,
                  color:
                    teamAPercentageValue > teamBPercentageValue
                      ? red_4
                      : green_6,
                  fontFamily: "Montserrat",
                  fontWeight: 600,
                  fontSize: 12
                }}
              >
                {teamBPercentage}
              </div>
              <div
                style={{
                  width: "100%",
                  // background: red_10,
                  background:
                    teamAPercentageValue > teamBPercentageValue
                      ? red_10
                      : green_6,
                  height: 5,
                  borderTopRightRadius: 50,
                  borderBottomRightRadius: 50,
                  // borderRadius: 50,
                  margin: "3px 0 2px"
                }}
              />
              {false && score.status == "UPCOMING" ? (
                <div
                  style={{
                    color: grey_8,
                    fontFamily: "Montserrat",
                    fontWeight: 500,
                    fontSize: 10
                  }}
                >
                  {teamBName ? teamBName.substring(0, 3).toUpperCase() : ""}
                </div>
              ) : (
                <div
                  style={{
                    color: grey_8,
                    fontFamily: "Montserrat",
                    fontWeight: 500,
                    fontSize: 10
                  }}
                >
                  {teamB ? teamB.substring(0, 3).toUpperCase() : ""}
                </div>
              )}
            </div>
          </div>
        ) : (
          <div
            className="prediction-loader"
            style={{
              display: "flex",
              alignItems: "center",
              fontFamily: "Montserrat",
              fontWeight: 500,
              fontSize: 12,
              minHeight: 47,
              padding: "4px 0px"
            }}
          >
            {/* <div class="lds-ring"><div></div><div></div><div></div><div></div></div> */}
            <div
              style={{
                height: 38,
                width: 38,
                overflow: "hidden",
                borderRadius: "50%",
                marginRight: 12
              }}
            >
              <img
                src={require("../../images/prediction-loader.png")}
                alt=""
                style={{
                  width: "100%",
                  objectFit: "cover"
                }}
              />
            </div>
            <div
              style={{
                fontWeight: 400
              }}
            >
              <div>Win projections to be updated soon</div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default Polling;
