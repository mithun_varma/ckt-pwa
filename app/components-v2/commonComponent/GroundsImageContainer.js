import React, { Component } from "react";
import { hrLineStyle } from "config/styleConfig";

class GroundsImageContainer extends Component {
  render() {
    return (
      <div
        style={{
          background: "#FFF",
          margin: "10px 0 0 16px",
          borderRadius: 3,
          padding: "0 16px"
        }}
      >
        <div
          style={{
            padding: "12px 0px",
            background: "#FFF",
            fontFamily: "Montserrat",
            fontWeight: 500,
            fontSize: 12,
            color: "#141b2f"
          }}
        >
          {this.props.header}
        </div>
        <div style={hrLineStyle} />
        <div>
          <div style={pageStyle.imageWrapper}>
            {!this.props.gallery &&
              ["this.props.data.content", 2, 3, 4].map(item => (
                <div style={pageStyle.imageContainer}>
                  <div
                    style={{
                      position: "absolute",
                      textTransform: "uppercase",
                      display: "flex",
                      alignItems: "center",
                      right: 22,
                      top: 20,
                      color: "#FFF",
                      fontFamily: "Montserrat",
                      fontWeight: 500,
                      fontSize: 10
                    }}
                  >
                    {this.props.isCompare && (
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <span
                          style={{
                            marginRight: 8
                          }}
                        >
                          Compare
                        </span>
                        <img
                          src={require("../../images/right_arrow_orange.svg")}
                          alt=""
                          style={{
                            width: 30,
                            height: 30
                          }}
                        />
                      </div>
                    )}
                    {this.props.isMoments && (
                      <div
                        style={{
                          background:
                            "linear-gradient(to right, #141b2f, #727682)",
                          padding: "4px 8px",
                          border: "1px solid #fa9441",
                          opacity: 0.9
                        }}
                      >
                        {[1, 2].map(item => (
                          <div
                            style={{
                              display: "flex",
                              flex: 1,
                              fontSize: 12,
                              alignItems: "center",
                              padding: "2px 0"
                            }}
                          >
                            <div>
                              <img
                                src={
                                  "https://www.indiacelebrating.com/wp-content/uploads/Flag-of-India.jpg"
                                }
                                alt=""
                                style={{
                                  width: 20,
                                  height: 20,
                                  borderRadius: "50%",
                                  border: "1px solid #FFF"
                                }}
                              />
                            </div>
                            <div
                              style={{
                                color: "#fadd8d",
                                fontFamily: "Montserrat",
                                fontWeight: 600,
                                fontSize: 12,
                                textTransform: "uppercase",
                                padding: "0 6px"
                              }}
                            >
                              IND
                            </div>
                            <div
                              style={{
                                fontFamily: "Montserrat",
                                fontWeight: 400,
                                padding: "0 6px"
                              }}
                            >
                              400/7
                            </div>
                          </div>
                        ))}
                      </div>
                    )}
                  </div>
                  <div>
                    <div
                      style={{
                        width: 280,
                        height: 207,
                        backgroundColor: "grey",
                        // backgroundImage: `url(${require("../../images/dhoni.png")})`,
                        backgroundImage: `url(https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png)`,
                        backgroundSize: "cover",
                        backgroundPosition: "center",
                        backgroundRepeat: "no-repeat"
                      }}
                    />
                    {/* <img
                      src="https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
                      alt="Snow"
                      // style={pageStyle.imageStyle}
                      style={{
                        width: "100%",
                        height: "100%"
                      }}
                    /> */}
                    <div
                      style={{
                        display: "flex",
                        // flex: 1,
                        justifyContent: "space-between",
                        padding: "10px 14px",
                        alignItems: "center",
                        background:
                          "linear-gradient(to right, #ea6550, #fa9441)"
                      }}
                    >
                      <div
                        style={{
                          color: "#FFF",
                          fontSize: 12,
                          fontFamily: "Montserrat",
                          fontWeight: 500
                        }}
                      >
                        <div>MCG, Sydney</div>
                        <div
                          style={{
                            fontFamily: "Montserrat",
                            fontWeight: 400
                          }}
                        >
                          East Melbourne, Australia
                        </div>
                      </div>
                      <div
                        style={
                          {
                            // background: "#FFF",
                          }
                        }
                      >
                        <img
                          src={require(" ../../images/right_arrow_orange.svg")}
                          alt=""
                          style={{
                            width: 35,
                            height: 35
                          }}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            {this.props.gallery && (
              <div
                style={{
                  padding: "12px 1px"
                }}
              >
                {[1, 2, 3].map(item => (
                  <img
                    src="https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
                    alt="Snow"
                    style={{ width: 240, height: 138, marginRight: 2 }}
                  />
                ))}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default GroundsImageContainer;

const pageStyle = {
  imageWrapper: {
    overflowX: "scroll",
    whiteSpace: "nowrap",
    display: "flex"
  },
  imageContainer: {
    position: "relative",
    padding: "12px 12px 12px 0px"
  },
  imageStyle: {
    width: 280,
    height: 207
  }
};
