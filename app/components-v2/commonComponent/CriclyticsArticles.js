import React, { Component } from "react";
import { white, grey_8 } from "../../styleSheet/globalStyle/color";
import CardGradientTitle from "./CardGradientTitle";
import FeaturedArticles from "../Articles/FeatureArticleCard";
import ArticleCard from "../Articles/ArticleCard";
import HrLine from "./HrLine";

export default class CriclyticsArticles extends Component {
  cardOnClick = (path, type) => {
    this.props.history.push(path);
  };

  render() {
    return (
      <div
        style={{
          margin: "0 16px",
          background: white
        }}
      >
        <CardGradientTitle
          title="Post Match Analysis"
          titleStyles={{
            color: grey_8,
            textTransform: "uppercase",
            fontSize: 11
          }}
        />
        {this.props.articles &&
          this.props.articles.length > 0 && (
            <div>
              {this.props.articles.map(
                (article, key) =>
                  article.type === "analysis" && (
                    <FeaturedArticles
                      key={key}
                      article={article}
                      cardOnClick={path => this.cardOnClick(path, "article")}
                      cardType={"analysis"}
                    />
                  )
              )}
            </div>
          )}
        {this.props.articles &&
          this.props.articles.length > 0 && (
            <div>
              {this.props.articles.map(
                article =>
                  article.type === "video" && (
                    <React.Fragment>
                      <HrLine />
                      <ArticleCard
                        article={this.props.articles[1]}
                        isVideo
                        cardOnClick={path => this.cardOnClick(path, "video")}
                      />
                    </React.Fragment>
                  )
              )}
            </div>
          )}
      </div>
    );
  }
}
