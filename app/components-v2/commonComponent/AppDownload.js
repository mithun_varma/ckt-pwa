import React, { Component } from "react";
import { gradientRedNewLeft, white } from "../../styleSheet/globalStyle/color";
import ChevronRight from "@material-ui/icons/ChevronRight";

class AppDownload extends Component {
  componentDidMount() {
    document
      .getElementById("download-app")
      .addEventListener("click", this.handleClick);
  }

  handleClick = () => {
    let parent = document.getElementById("download-app");
    let link = document.createElement("a");
    link.style.display = "none";
    parent.appendChild(link);
    link.setAttribute("target", "_blank");
    link.setAttribute("download", "download");
    link.setAttribute("href", this.props.url);
    // link.href = this.props.url;
    link.click();
    parent.removeChild(link);
  };

  render() {
    return (
      <div
        style={{
          background: gradientRedNewLeft,
          margin: "0 16px 20px",
          padding: "14px",
          borderRadius: 3,
          ...this.props.rootStyles
        }}
        id="download-app"
      >
        {/* <a
          // href="https://play.google.com/store/apps/details?id=com.crictec.cricket"
          // href={this.props.url}
          // download
          style={{
            textDecoration: "none"
          }}
        > */}
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            ...this.props.innerStyle
          }}
        >
          <div style={{ display: "flex", alignItems: "center" }}>
            {!this.props.noIcon && (
              <img
                src={
                  this.props.isAndroid
                    ? require("../../images/android.svg")
                    : require("../../images/i-os-icon.svg")
                }
                style={{ width: 20, height: 20 }}
                alt=""
              />
            )}
            <span
              style={{
                marginLeft: 14,
                fontFamily: "Montserrat",
                fontWeight: 600,
                color: white,
                fontSize: 12
              }}
            >
              DOWNLOAD APP
            </span>
          </div>
          {/* <div style={{display: "flex"}}> */}
          {!this.props.noIcon && (
            <ChevronRight
              style={{
                color: white
              }}
            />
          )}
          {/* </div> */}
        </div>
        {/* </a> */}
      </div>
    );
  }
}

export default AppDownload;
