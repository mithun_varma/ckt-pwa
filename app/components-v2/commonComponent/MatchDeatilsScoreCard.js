import React, { Component } from 'react';
import { screenWidth } from '../../styleSheet/screenSize/ScreenDetails';
import { gradientGrey } from '../../styleSheet/globalStyle/color';
// import {greyGradientLine} from './HrLine';

const dataMatchDetails =[
    {
        'field':'Venue',
        'value':'Adelaide Oval, Australia',
    },
    {
        'field':'Series',
        'value':'India tour of Australia 2018'
    },
    {
        'field':'Match',
        'value':'India vs Australia'
    },
    {
        'field':'Toss',
        'value':'India elected to bat'
    }

];

export default class MatchDetailsScoreCard extends Component {
constructor(props){
    super(props);
    this.state={
        dataMatchDetails:[],
    };

}
componentDidMount(){
    this.setState({
        dataMatchDetails
    })

}
render(){
    const venueDetails = this.props.venueDetails[0];
    // console.log(venueDetails);
    return(
        <div style={pageStyle.matchDetailsConatiner}>
            <div>
                {greyGradientLine}
            </div>
            <div style={pageStyle.matchDetailsHeading}>
                MATCH DETAILS
            </div>
            <div>
                {greyGradientLine}
            </div>
            <div>

                <div style={pageStyle.matchDetailsDataContainer}>
                    <div style={pageStyle.matchDetailsValue1}>
                        {"Venue"}
                    </div>
                    <div style={pageStyle.matchDetailsValue2}>
                        {venueDetails.venue}
                    </div>
                </div>
                <div style={pageStyle.matchDetailsDataContainer}>
                    <div style={pageStyle.matchDetailsValue1}>
                        {"Match Name"}
                    </div>
                    <div style={pageStyle.matchDetailsValue2}>
                        {venueDetails.matchName}
                    </div>
                </div>
                <div style={pageStyle.matchDetailsDataContainer}>
                    <div style={pageStyle.matchDetailsValue1}>
                        {"Series Name"}
                    </div>
                    <div style={pageStyle.matchDetailsValue2}>
                        {venueDetails.seriesName}
                    </div>
                </div>
                <div style={pageStyle.matchDetailsDataContainer}>
                    <div style={pageStyle.matchDetailsValue1}>
                        {"Toss Result"}
                    </div>
                    <div style={pageStyle.matchDetailsValue2}>
                        {venueDetails.toss}
                    </div>
                </div>
            </div>
        </div>
    );
}



}
const pageStyle ={
    matchDetailsValue2:{
        display:'flex',
        flex:0.60,
        justifyContent:'flex-start',
        paddingRight:'16px',
        marginTop:'14px',
        fontSize:'12px',
        color:'#141b2f',
        fontWeight:'700',
    },
    matchDetailsValue1:{
        display:'flex',
        flex:0.40,
        justifyContent:'flex-start',
        marginTop:'14px',
        fontSize:'12px',
        color:'#141b2f',
        fontWeight:'500',
    },
    matchDetailsDataContainer:{
        display:'flex',
        flexDirection:'row',
        flex:1,
    },
    matchDetailsHeading:{
        display:'flex',
        justifyContent:'flex-start',
        padding:'12px',
        backgroundImage:gradientGrey,
        color:'#727682',
        fontSize:'10px',
        flex:1,
    },
    matchDetailsConatiner:{
        display:'flex',
        flexDirection:'column',
        width:screenWidth * 0.911,
        flex:1,
        boxShadow:'0px 3px 6px 0px #13000000',
        backgroundColor:'#ffffff',
        paddingLeft:'12px',

    },

}