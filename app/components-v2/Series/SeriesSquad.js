import React, { Component } from "react";
import {
  white,
  blue_grey
} from "../../styleSheet/globalStyle/color";
import EmptyState from "components/Common/EmptyState";
import Filter from "lodash/filter";
import ListItems from "../../components-v2/commonComponent/ListItems";
import ButtonBar from "../../components-v2/commonComponent/ButtonBar";
import CardGradientTitle from "../commonComponent/CardGradientTitle";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
const bowler = require("../../images/Bowler.svg");
const batsman = require("../../images/Batsman.png");
const all_rounder = require("../../images/AllRounder.svg");
const wicket_keeper = require("../../images/Keeper.svg");
export class SeriesSquad extends Component {
  state = {
    activeButton: null,
    buttons: [],
    display: false
  };

  generatePlayerIcon = playerKind => {
    return playerKind.indexOf("Batsman") != -1
      ? batsman
      : playerKind.indexOf("Bowler") != -1
        ? bowler
        : playerKind.indexOf("Keeper") != -1
          ? wicket_keeper
          : playerKind.indexOf("AllRounder") != -1
            ? all_rounder
            : "Not Found";
  };
  generateTeamList = team => {
    return (
      team &&
      team.map((player, index) => {
        return (
          <div key={index}>
            <ListItems
              key={index}
              title={player.shortName || "-"}
              rootStyles={{
                padding: "10px 16px"
              }}
              titleStyle={{
                fontWeight: 400
              }}
              rightElement={
                player.role &&
                ["AllRounder", "Bowler", "Batsman", "Keeper"].indexOf(
                  player.role
                ) > -1 ? (
                  <img
                    style={{ height: 26, width: 24 }}
                    src={this.generatePlayerIcon(player.role)}
                  />
                ) : (
                  <div />
                )
              }
            />
          </div>
        );
      })
    );
  };
  componentWillMount() {
    this.setState({ activeButton: this.props.format[0] });
  }

  generateSquad = data => {
    const filtered = Filter(data, ["format", this.state.activeButton]);

    return filtered.length != 0 ? (
      filtered.map((ele, ind) => {
        return (
          <div className="toggleClass">
            <div>
              <ListItems
                param={ind + 1}
                onClick={value => {
                  this.refs[`List${value - 1}`].className === "show"
                    ? (this.refs[`List${value - 1}`].className = "hide")
                    : (this.refs[`List${value - 1}`].className = "show");
                }}
                rightElement={<ExpandMoreIcon style={{ color: blue_grey }} />}
                title={ele.name}
                rootStyles={{
                  padding: "12px 16px"
                }}
                titleStyle={{
                  fontSize: 12,
                  fontWeight: 600
                }}
              />
            </div>
            <div className="hide" ref={`List${ind}`}>
              {this.generateTeamList(ele.players)}
            </div>
          </div>
        );
      })
    ) : (
      <EmptyState msg={`No ${this.state.activeButton} squads found`} />
    );
  };

  render() {
    const { squads } = this.props;
    return squads ? (
      <div style={{ backgroundColor: white }}>
        <ButtonBar
          key
          items={this.props.format}
          activeItem={this.state.activeButton}
          onClick={val => {
            this.setState({ activeButton: val });
          }}
        />
        <CardGradientTitle title="Teams" />
        {this.generateSquad(squads)}
      </div>
    ) : (
      <div>No Teams Found</div>
    );
  }
}

export default SeriesSquad;
