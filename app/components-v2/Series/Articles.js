import React, { PureComponent } from "react";
import ArticleCard from "../../components-v2/Articles/ArticleCard";
import Loader from "../../components/Common/Loader";
import IsEmpty from "lodash/isEmpty";
import EmptyState from "components/Common/EmptyState";
import { white } from "../../styleSheet/globalStyle/color";
import HrLine from "../commonComponent/HrLine";
export class componentName extends PureComponent {
  state = {
    data: undefined
  };
  componentDidMount() {
    this.props.request({ seriesId: this.props.id });
    // Requesting the data here
    // const data = await Axios.get(
    //   "http://13.232.186.174/api/articles?types[]=analysis&limit=3&skip=0"
    // );
  }
  cardOnClick = data => {
    this.props.history.push(data);
  };

  render() {
    const { news, newsLoading } = this.props;
    return (
      <div
        style={{
          background: white
        }}
      >
        {newsLoading ? (
          <Loader styles={{ height: "80px" }} noOfLoaders={4} />
        ) : news && news.length > 0 ? (
          news.map((ele, key) => (
            <div>
              <ArticleCard
                key={key}
                article={ele}
                cardType={ele.type}
                // articleHost={this.state.data.host}
                cardOnClick={this.cardOnClick}
                iterationId={key}
              />
              <HrLine />
            </div>
          ))
        ) : (
          <EmptyState
            msg="No news found"
            // rootStyles={{
            //   height: 170
            // }}
          />
        )}
      </div>
    );
  }
}

export default componentName;
