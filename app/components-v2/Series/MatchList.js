import React, { PureComponent } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import Loader from "../../components/Common/Loader";
import Reveal from "react-reveal";
import EmptyState from "components/Common/EmptyState";
import animate from "../../utils/animationConfig";
import moment from "moment";
import HomeScoreCard from "../HomeScore/HomeScoreCard";
import CardGradientTitle from "../commonComponent/CardGradientTitle";
let counter = 0;
let kind = [];

let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  ClevertapReact.initialize("W88-4KR-845Z");
}

export default class MatchList extends PureComponent {
  state = {
    matchNumber: 0
  };
  ordinal_suffix_of(i) {
    let j = i % 10,
      k = i % 100;
    if (j == 1 && k != 11) {
      return i + "st";
    }
    if (j == 2 && k != 12) {
      return i + "nd";
    }
    if (j == 3 && k != 13) {
      return i + "rd";
    }
    return i + "th";
  }
  generateMatchStats = (teamName, score, over) => {
    return (
      <div style={{ display: "flex" }}>
        <div>{teamName}</div>
        {score &&
          over && (
            <div>
              <span> {score} </span>
              <span>{`(${over})`}</span>
            </div>
          )}
      </div>
    );
  };
  formatMatchType = matchKind => {
    kind.push(matchKind);
    if (kind[kind.length - 1] === kind[kind.length - 2]) {
      counter += 1;
      return `${this.ordinal_suffix_of(counter)} ${matchKind.toLowerCase()}`;
    } else {
      counter = 0;
      return `${this.ordinal_suffix_of(
        counter + 1
      )} ${matchKind.toLowerCase()}`;
    }
  };
  render() {
    const { series, matches } = this.props;
    return (
      <React.Fragment>
        <div className="wrapperContainerRemove">
          {series.seriesDetailsLoading ? (
            <Loader styles={{ height: "75px" }} noOfLoaders={4} />
          ) : series.seriesMatches.length > 0 ? (
            <InfiniteScroll
              style={{ overflow: "unset" }}
              dataLength={series.seriesMatches.length}
              next={this.loadMoreItems}
              loader={
                series.seriesMatches.length >= 6 && (
                  <Loader styles={{ height: "150px" }} noOfLoaders={2} />
                )
              }
              hasMore={false}
            >
              {series.seriesMatches.map(
                (match, matchKey) =>
                  match ? (
                    <section
                      className="sectionWrapper__matchList"
                      key={`${matchKey + 1}`}
                    >
                      <div className="seriesDateRemove">
                        {match.startDate && (
                          <CardGradientTitle
                            title={moment(match.startDate).format(
                              "Do MMMM, YYYY"
                            )}
                          />
                          // <h5>
                          //   {moment(match.startDate).format("Do MMMM, YYYY")}
                          // </h5>
                        )}
                      </div>
                      <div
                        onClick={() =>
                          // this.props.Click(match.matchId)
                          {

                            ClevertapReact.event("Series", {
                              source: "Series",
                              matchId: match && match.matchId || ""
                            });

                            if (
                              match &&
                              match.teamA &&
                              match.teamA.name != "tbc"
                            ) {
                              this.props.Click(match.matchId);
                            }
                          }
                        }
                        className="sectionWrapper__card sectionWhite"
                      >
                        {/* <ScheduleCard score={match} status={"upcoming"} /> */}
                        {/* <Sched uleCardNew score={match} status={"upcoming"} /> */}
                        <HomeScoreCard
                          score={match}
                          isSeries
                          cardStyles={{ width: "100%" }}
                        />
                      </div>
                    </section>
                  ) : null
              )}
            </InfiniteScroll>
          ) : (
            <Reveal effect={animate.emptyState.effect}>
              <EmptyState msg="No matches found" />
            </Reveal>
          )}
        </div>
      </React.Fragment>
    );
  }
}
