import React, { PureComponent } from "react";
import FactEngiene from "../commonComponent/FactEngiene";
import FactHeader from "../commonComponent/FactHeader";
import HrLine from "../commonComponent/HrLine";

export class Fact extends PureComponent {
  render() {
    const { header, headerDescription, fact, hideFactEngine, hideHrLine } = this.props;
    return (
      <div>
        <div style={{}}>
          <FactHeader header={header} description={headerDescription} />
          {/* {!hideFactEngine && <FactEngiene fact={fact} />}
          {!hideHrLine && <HrLine /> } */}
        </div>
      </div>
    );
  }
}

export default Fact;
