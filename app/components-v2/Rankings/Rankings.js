import React, { Component } from "react";
import { grey_10, white } from "../../styleSheet/globalStyle/color";
import TabBar from "../commonComponent/TabBar";
import HrLine from "../commonComponent/HrLine";
import ScrollButtons from "./ScrollButtons";
import CardGradientTitle from "../commonComponent/CardGradientTitle";
import ListItems from "../commonComponent/ListItems";

const MATCH_TYPE = [
  "odi",
  "t20",
  "test",
  "women's odi",
  "women's t20",
  "women's test"
];

const RANK_TYPE = ["team", "batting", "bowling", "all-rounder"];

class Rankings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "team",
      activeButton: "odi"
    };
  }

  handleActiveTab = activeTab => {
    this.setState({
      activeTab
    });
  };

  handleActiveButton = activeButton => {
    this.setState({
      activeButton
    });
  };

  render() {
    return (
      <div
        style={{
          padding: 16,
          // minHeight: "100vh"
        }}
      >
        {/* Ranking Title */}
        <div
          style={{
            fontFamily: "Montserrat",
            fontWeight: 700,
            fontSize: 22,
            color: grey_10,
            marginBottom: 12
          }}
        >
          Rankings
        </div>

        {/* Rankiong Card Section */}
        <div
          style={{
            background: white,
            borderRadius: 3
          }}
        >
          <TabBar
            items={RANK_TYPE}
            activeItem={this.state.activeTab}
            onClick={this.handleActiveTab}
          />
          {/* <HrLine /> */}

          {/* Button Bar */}
          <ScrollButtons
            items={MATCH_TYPE}
            activeItem={this.state.activeButton}
            onClick={this.handleActiveButton}
          />
          <HrLine />

          <CardGradientTitle
            title="TEAMS"
            subtitle="RATING"
            subtitleStyles={{ width: "22%" }}
          />

          {/* Ranking List */}
          {ListData.map(item => (
            <ListItems title={item.title} subtitle={item.subtitle} isAvatar />
          ))}
          {/* Ranking List Closing */}
        </div>
        {/* Rankiong Card Section closing */}
      </div>
    );
  }
}

export default Rankings;

const ListData = [
  {
    title: "India",
    subtitle: 998
  },
  {
    title: "Australia",
    subtitle: 99
  },
  {
    title: "India",
    subtitle: 998
  },
  {
    title: "India",
    subtitle: 998
  }
];
