import React, { Component } from "react";
import {
  white,
  grey_1,
  red_Orange,
  grey_7_1,
  grey_7
} from "../../styleSheet/globalStyle/color";
class ScrollButtons extends Component {
  render() {
    const { activeItem, items } = this.props;
    return (
      <div
        style={{
          display: "flex",
          padding: 12,
          overflow: "scroll",
          // flexDirection: "row",
          // justifyContent: "space-around",
          ...this.props.rootStyles
        }}
      >
        {items.map(val => (
          <div
            style={{
              // flex: 0.4,
              padding: "6px 8px",
              borderRadius: 3,
              // background: val.key == activeItem ? gradientOrange : grey_1,
              background: val.key == activeItem ? red_Orange : grey_1,
              color: val.key == activeItem ? white : grey_7,
              margin: "0 8px",
              border: `0.6px solid ${
                val.key == activeItem ? red_Orange : grey_7_1
              }`,
              fontFamily: "Montserrat",
              fontWeight: 500,
              fontSize: 10,
              whiteSpace: "nowrap",
              textTransform: "capitalize",
              fontStyle: 'normal',
                fontStretch: 'normal',
                lineHeight: 'normal',
                letterSpacing: 'normal'


            
            }}
            onClick={e => {
              if (val.key !== activeItem) {
                this.props.onClick(val.key, val.filters);
              }
            }}
          >
            {val.label}
          </div>
        ))}
      </div>
    );
  }
}

export default ScrollButtons;
