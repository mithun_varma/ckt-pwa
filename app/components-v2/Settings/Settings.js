import React, { Component } from "react";
import { grey_10, white, cardShadow } from "../../styleSheet/globalStyle/color";
import Title from "./Title";
import Options from "./Options";
import HrLine from "../commonComponent/HrLine";
import Headers from './../commonComponent/TopHeader';
import { screenWidth } from "../../styleSheet/screenSize/ScreenDetails";

let lastScrollY = 0;
let ticking = false;
class Settings extends Component {
  constructor(props){
    super(props);
    this.state={
      modifyHeader:false,
    }
  }
  render() {

    return (
      <div>
        <Headers title={'Settings'} type={'black'}  id='header' history={this.props.history}/>
        <div
          style={{
            minHeight: "100vh",
            overflowY: 'auto',
            width:screenWidth*0.911,
            marginLeft:screenWidth*0.044,
          }}
        >
        
        {/* Settings Title */}
        <div
          style={{
            fontFamily: "Montserrat",
            fontWeight: 700,
            fontSize: 22,
            color: grey_10,
            marginBottom: 12
          }}
        >
          Settings
        </div>

        {/* Theme */}
        <div
          style={{
            background: white,
            borderRadius: 3,
            boxShadow: cardShadow,
            marginBottom: 12
          }}
        >
          {/* Title */}
          <Title title="Theme" />
          <Options
            title="Dark Theme"
            subtitle="Light text against dark background"
            isSwitch
          />
          <HrLine />
          {/* Title */}
          <Title title="Data Saver Options" />
          <Options
            title="Automatic data fresh"
            subtitle="Light text against dark background"
            isSwitch
          />
        </div>

        {/* Load Images */}
        <div
          style={{
            background: white,
            borderRadius: 3,
            boxShadow: cardShadow,
            marginBottom: 12
          }}
        >
          <Options title="Load images" subtitle="Effects data usage" isSwitch />
        </div>

        {/* Notifications */}
        <div
          style={{
            background: white,
            borderRadius: 3,
            boxShadow: cardShadow,
            marginBottom: 12
          }}
        >
          {/* Title */}
          <Title title="Notifications" />
          <Options title="Allow Notifications" isSwitch />
          <HrLine />
          <Options title="Sound" isSwitch />
          <HrLine />
          <Options title="Vibration" isSwitch />
        </div>

        {/* Do not Disturb */}
        <div
          style={{
            background: white,
            borderRadius: 3,
            boxShadow: cardShadow,
            marginBottom: 12
          }}
        >
          {/* Title */}
          <Title title="Do not Disturb" />
          <Options
            title="Do not Disturb"
            subtitle="Notifications will not make sound or vibrate"
            isSwitch
          />
          <HrLine />
          <Options title="From" subtitle="3:00" />
          <HrLine />
          <Options title="To" subtitle="07:00" />
        </div>

        {/* Primary Notifications */}
        <div
          style={{
            background: white,
            borderRadius: 3,
            boxShadow: cardShadow,
            marginBottom: 12
          }}
        >
          {/* Title */}
          <Title title="Primary Notifications" />
          <Options
            title="Cricket updates"
            subtitle="Notify breaking or interesting stories"
            isSwitch
          />
          <HrLine />
          <Options
            title="Notify breaking or interesting stories"
            subtitle="Notify the results after the match"
          />
          <HrLine />
          <Options
            title="T20 league match results"
            subtitle="Notify the results after the match"
          />
        </div>

        {/* Opt-in Notifications */}
        <div
          style={{
            background: white,
            borderRadius: 3,
            boxShadow: cardShadow,
            marginBottom: 12
          }}
        >
          {/* Title */}
          <Title title="Opt-in Notifications" />
          <Options title="Matches" />
          <HrLine />
          <Options title="Series" />
          <HrLine />
          <Options title="Teams" />
        </div>
      </div>
    
      </div>
   );
  }
}

export default Settings;
