import React, { Component } from "react";
import { grey_10 } from "../../styleSheet/globalStyle/color";
import Switch from "@material-ui/core/Switch";
class Options extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedSwitch: true
    };
  }

  toggleSwitch = () => {
    this.setState({
      checkedSwitch: !this.state.checkedSwitch
    });
  };

  render() {
    return (
      <div
        style={{
          padding: "18px 12px"
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <div
            style={{
              color: grey_10,
              fontSize: 14,
              fontFamily: "Montserrat",
              fontWeight: 500
            }}
          >
            {this.props.title}
          </div>
          {this.props.isSwitch && (
            <Switch
              classes={{
                switchBase: "switchBaseStyle",
                checked: "switchChecked",
                bar: this.state.checkedSwitch
                  ? "switchBarActive"
                  : "switchBarInactive"
              }}
              disableRipple
              checked={this.state.checkedSwitch}
              onChange={this.toggleSwitch}
            />
          )}
        </div>
        <div
          style={{
            color: grey_10,
            fontSize: 10,
            fontFamily: "Montserrat",
            fontWeight: 400
          }}
        >
          {this.props.subtitle}
        </div>
      </div>
    );
  }
}

export default Options;
