import React, { Component } from "react";
import HrLine from "../commonComponent/HrLine";
import { gradientGrey, grey_8 } from "../../styleSheet/globalStyle/color";

class Title extends Component {
  render() {
    return (
      <div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            background: gradientGrey,
            alignItems: "center",
            padding: "16px 8px 16px 10px",
            color: grey_8,
            fontSize: 10,
            fontFamily: "Montserrat",
            fontWeight: 400,
            letterSpacing: "0.5px",
            textTransform: "uppercase",
            borderRadius: 3
          }}
        >
          {this.props.title}
        </div>
        <HrLine />
      </div>
    );
  }
}

export default Title;
