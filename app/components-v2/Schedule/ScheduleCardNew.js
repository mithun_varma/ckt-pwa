import React, { Component } from "react";
import {
  grey_10,
  orange_2,
  avatarShadow,
  grey_8
} from "../../styleSheet/globalStyle/color";
import moment from "moment";

class ScheduleCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hrString: "",
      initial: true
    };
    this.timeout = 0;
  }
  handleCardClick = score => {
    this.props.history.push(`/score/${score.matchId}`);
  };

  componentDidMount = () => {
    if (
      this.props.score &&
      this.props.score.status === "UPCOMING" &&
      this.props.score.startDate
    ) {
      this.handleCountdownTimer();
    }
  };

  // static getDerivedStateFromProps = (nextProps, state) => {
  //   if (nextProps.score && nextProps.score.startDate && state.initial) {
  //     // this.handleCountdownTimer();
  //     return {
  //       ...state,
  //       initial: false
  //     };
  //   }
  // };

  handleCountdownTimer = () => {
    const startDate = this.props.score && this.props.score.startDate;
    let countDownDate = new Date(startDate).getTime() - 1800000;
    // console.log("inside : ", countDownDate);
    let now = new Date().getTime();
    // Find the distance between now and the count down date
    let distance = countDownDate - now;
    // Time calculations for days, hours, minutes and seconds
    // let days = Math.floor(distance / (1000 * 60 * 60 * 24));
    let hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((distance % (1000 * 60)) / 1000);
    let result =
      (hours > 0 ? hours + (hours > 1 ? " hrs " : " hr ") : "") +
      minutes +
      (minutes > 1 ? " mins " : " min ") +
      " to toss";
    // let result = hours + " hrs " + minutes + " min for toss";
    // Output the result in an element with id="time-section"
    // If the count down is over, write some text
    if (distance < 0) {
      clearInterval(this.timeout);
      // console.log("NO: ", result);
      this.setState({
        hrString: "0 hr 0 min to toss"
      });
      // return "0 hr 0 min to toss";
      // return "EXPIRED";
    } else {
      // console.log("yes: ", result);
      this.setState({
        hrString: result
      });
      // return result;
      // document.getElementById("time-section").innerHTML = result || "testing";
    }
    // console.log("called : ", startDate);
    this.timeout = setInterval(() => {
      // Get today's date and time
      let countDownDate = new Date(startDate).getTime() - 1800000;
      // console.log("inside : ", countDownDate);
      let now = new Date().getTime();
      // Find the distance between now and the count down date
      let distance = countDownDate - now;
      // Time calculations for days, hours, minutes and seconds
      // let days = Math.floor(distance / (1000 * 60 * 60 * 24));
      let hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((distance % (1000 * 60)) / 1000);
      let result =
        (hours > 0 ? hours + (hours > 1 ? " hrs " : " hr ") : "") +
        minutes +
        (minutes > 1 ? " mins " : " min ") +
        " to toss";
      // let result = hours + " hrs " + minutes + " min for toss";
      // Output the result in an element with id="time-section"
      // If the count down is over, write some text
      if (distance < 0) {
        clearInterval(this.timeout);
        // console.log("NO: ", result);
        this.setState({
          hrString: "0 hr 0 min to toss"
        });
        // return "0 hr 0 min to toss";
        // return "EXPIRED";
      } else {
        // console.log("yes: ", result);
        this.setState({
          hrString: result
        });
        // return result;
        // document.getElementById("time-section").innerHTML = result || "testing";
      }
    }, 1000);
  };

  componentWillUnmount = () => {
    clearInterval(this.timeout);
  };

  render() {
    const { score } = this.props;
    // const { inningCollection } = score;
    // let inningsArray = [];
    
    const Id = `match_${score.matchId}`;

    let teamCollection = [
      {
        teamName:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.fullName
            : score.teamB.fullName,
        shortName:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.shortName
            : score.teamB.shortName,
        teamKey:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.teamKey
            : score.teamB.teamKey,
        runs: [],
        wickets: [],
        overs: [],
        declared: [],
        followOn: [],
        avatar:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.avatar
            : score.teamB.avatar
      },
      {
        teamName:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.fullName
            : score.teamB.fullName,
        shortName:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.shortName
            : score.teamB.shortName,
        teamKey:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.teamKey
            : score.teamB.teamKey,
        runs: [],
        wickets: [],
        overs: [],
        declared: [],
        followOn: [],
        avatar:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.avatar
            : score.teamB.avatar
      }
    ];
    if (score.status !== "UPCOMING") {
      for (const order of score.inningOrder) {
        let team = order.split("_")[0];
        if (team === score.firstBatting) {
          (teamCollection[0].runs = [
            ...teamCollection[0].runs,
            score.inningCollection[order].runs
          ]),
            (teamCollection[0].wickets = [
              ...teamCollection[0].wickets,
              score.inningCollection[order].wickets
            ]),
            (teamCollection[0].overs = [
              ...teamCollection[0].overs,
              score.inningCollection[order].overs
            ]),
            (teamCollection[0].declared = [
              ...teamCollection[0].declared,
              score.inningCollection[order].declared
            ]),
            (teamCollection[0].followOn = [
              ...teamCollection[0].followOn,
              score.inningCollection[order].followOn
            ]);
        } else {
          (teamCollection[1].runs = [
            ...teamCollection[1].runs,
            score.inningCollection[order].runs
          ]),
            (teamCollection[1].wickets = [
              ...teamCollection[1].wickets,
              score.inningCollection[order].wickets
            ]),
            (teamCollection[1].overs = [
              ...teamCollection[1].overs,
              score.inningCollection[order].overs
            ]),
            (teamCollection[1].declared = [
              ...teamCollection[1].declared,
              score.inningCollection[order].declared
            ]),
            (teamCollection[1].followOn = [
              ...teamCollection[1].followOn,
              score.inningCollection[order].followOn
            ]);
        }
      }
    } else if (score.status == "UPCOMING") {
      for (let i = 0; i < teamCollection.length; i++) {
        (teamCollection[i].teamName =
          score[i == 0 ? "teamA" : "teamB"].fullName),
          (teamCollection[i].shortName =
            score[i == 0 ? "teamA" : "teamB"].shortName),
          (teamCollection[i].teamKey =
            score[i == 0 ? "teamA" : "teamB"].teamKey);
        (teamCollection[i].runs = []),
          (teamCollection[i].wickets = []),
          (teamCollection[i].overs = []),
          (teamCollection[i].declared = []),
          (teamCollection[i].followOn = []),
          (teamCollection[i].avatar = score[i == 0 ? "teamA" : "teamB"].avatar);
      }
    }

    return (
      <div
        style={{
          // backgroundColor: "#edeff5",
          position: "relative",
          padding: "28"
        }}
      >
        <div
          className="scoreCard"
          style={{
            padding: "16px",
            boxShadow: "none",
            ...this.props.cardStyles
          }}
          id={Id}
          onClick={() => {
            if (score && score.teamA && score.teamA.name != "tbc") {
              this.props.handleCardClick(score, Id);
            }
          }}
        >
          <div
            style={{
              fontFamily: "Montserrat",
              fontWeight: 500,
              fontSize: 12,
              color: grey_10,
              display: "flex",
              alignItems: "center",
              marginBottom: 12,
              whiteSpace: "nowrap",
              overflow: "hidden"
            }}
          >
            {score &&
              score.status === "RUNNING" &&
              score.firstBallStatus === "RUNNING" && (
                // score.statusOverView === "RUNNING" &&
                <span
                  style={{
                    border: "solid 0.6px #979797",
                    borderRadius: "7.5px",
                    padding: "1px 8px",
                    display: "inline-block",
                    fontSize: "8px",
                    marginRight: 12,
                    // marginLeft: "10px",
                    textTransform: "uppercase"
                  }}
                >
                  <span
                    style={{
                      width: "5px",
                      height: "5px",
                      borderRadius: "50%",
                      marginLeft: "-2px",
                      marginRight: "4px",
                      backgroundColor: "green",
                      display: "inline-block"
                    }}
                  />
                  live
                </span>
              )}
            <span>{score && score.relatedName ? score.relatedName : ""}</span>
            {score &&
              score.relatedName && <span style={{ padding: "0 6px" }}>•</span>}
            <span>{score && score.venue ? score.venue : "--"}</span>
          </div>

          <div
            style={{
              flex: 1,
              flexDirection: "column",
              // width: 300,
              backgroundColor: "#ffffff",
              // height: 92,
              justifyContent: "space-between"
              // marginTop: 63,
              // position: "absolute"
            }}
          >
            <div
              style={{
                flexDirection: "column",
                justifyContent: "space-between",
                position: "relative",
                fontFamily: "Montserrat",
                fontSize: 14,
                fontWeight: "500"
              }}
              onClick={this.props.handleScoreCard}
            >
              {teamCollection &&
                teamCollection.map((team, teamIndex) => (
                  <div key={team.teamKey} style={{ display: "flex" }}>
                    <div
                      style={{
                        display: "flex",
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "space-between",
                        flexDirection: "row"
                        // marginBottom: "6px"
                      }}
                    >
                      <div
                        style={{
                          flex: score.status === "UPCOMING" ? null : 0.4,
                          display: "flex",
                          alignItems: "center",
                          marginBottom: 12
                        }}
                      >
                        <img
                          src={
                            team.avatar
                              ? team.avatar
                              : team.teamName == "tbc" || team.teamName == "TBC"
                                ? require("../../images/TBC-Flag.png")
                                : require("../../images/flag_empty.svg")
                          }
                          alt=""
                          style={{
                            width: 30,
                            height: 21,
                            boxShadow: avatarShadow,
                            borderRadius: 2,
                            // marginLeft: 13,
                            marginRight: 12
                          }}
                        />
                        {/* {score.status !== "UPCOMING" ? ( */}
                        <span style={{ fontWeight: 400 }}>
                          {team.shortName &&
                            (score.status === "UPCOMING"
                              ? team.teamName == "tbc" || team.teamName == "TBC"
                                ? team.teamName.toUpperCase()
                                : team.teamName
                              : team.shortName.length > 4
                                ? team.shortName.substring(0, 3).toUpperCase()
                                : team.shortName.toUpperCase())}
                        </span>
                        {/* ) : (
                          <span style={{ fontWeight: 400, fontSize: 14 }}>
                            {(score &&
                              score.teamA.shortName
                                .substring(0, 3)
                                .toUpperCase()) ||
                              "-"}
                          </span>
                        )} */}
                      </div>
                      <div
                        style={{
                          flex: 0.6,
                          flexDirection: "row",
                          marginBottom: 12
                        }}
                      >
                        {score.status !== "UPCOMING" ? (
                          <span
                            style={{
                              fontSize: 14
                            }}
                          >
                            <span
                              style={{
                                fontWeight: `${
                                  score.status === "RUNNING" &&
                                  team.teamKey === score.currentScoreCard &&
                                  score.currentScoreCard.battingTeam &&
                                  team.runs.length === 1
                                    ? "600"
                                    : "400"
                                }`
                              }}
                            >
                              <div style={{
                                display: "inline-block",
                                minWidth: score.format == "TEST" ? 58 : "auto"
                              }}>
                              {`${
                                team.runs[0] !== undefined
                                  ? `${team.runs[0]}`
                                  : ""
                              }${
                                team.wickets[0] !== undefined &&
                                team.wickets[0] < 10
                                  ? `/${team.wickets[0]}`
                                  : ""
                              }`}
                              {
                                team.declared[0]
                                  ? <span style={{
                                    color: "#d64b4b",
                                    fontWeight: 400
                                  }}>{` d`}</span>
                                  : ""
                              }
                              </div>
                              <span style={{ fontWeight: 400, marginLeft: 8 }}>
                                {/* {score.format !== "TEST"
                                  ? team.overs.length === 1
                                    ? team.overs[0] && `(${team.overs[0]})`
                                    : ""
                                  : team.overs[0] && `(${team.overs[0]})`} */}
                                  {(score.format !== "TEST" ||
                                    score.status === "RUNNING" &&
                                    score.currentScoreCard &&
                                    team.teamKey === score.currentScoreCard.teamKey &&
                                    team.runs.length < 2
                                  ) &&
                                    team.overs[0] &&
                                    `(${team.overs[0]})`}
                              </span>
                            </span>
                            <span
                              style={{
                                fontWeight: `${
                                  score.status === "RUNNING" &&
                                  team.teamKey === score.currentScoreCard &&
                                  score.currentScoreCard.battingTeam &&
                                  team.runs.length > 1
                                    ? "600"
                                    : "400"
                                }`
                              }}
                            >
                              {team.runs.length > 1 &&
                                ` & ${
                                  team.runs[1] !== undefined ? team.runs[1] : ""
                                }${
                                  team.wickets[1] !== undefined &&
                                  team.wickets[1] < 10
                                    ? "/" + team.wickets[1]
                                    : ""
                                }`}
                                {
                                  team.declared.length > 1 && team.declared[1]
                                    ? <span style={{
                                      color: "#d64b4b",
                                      fontWeight: 400
                                    }}>{` d`}</span>
                                    : ""
                                }
                                {
                                  team.followOn.length > 1 && team.followOn[1]
                                    ? <span style={{
                                      color: "#d64b4b",
                                      fontWeight: 400
                                    }}>{` f/o`}</span>
                                    : ""
                                }
                              <span style={{ fontWeight: 400, marginLeft: 8 }}>
                                {team.runs.length > 1 &&
                                  (score.format !== "TEST" ||
                                  score.status === "RUNNING" &&
                                  score.currentScoreCard &&
                                  team.teamKey === score.currentScoreCard.teamKey &&
                                  team.runs.length > 1) &&
                                  `(${team.overs[1]})`}
                              </span>

                              {/* To show DLS method text */}
                              {score &&
                                score.dlApplied &&
                                teamIndex == 1 && (
                                  <span
                                    style={{
                                      color: grey_8,
                                      fontSize: 12
                                    }}
                                  >
                                    {" "}
                                    (DLS)
                                  </span>
                                )}
                            </span>
                          </span>
                        ) : null}
                      </div>
                    </div>
                    {/* <div style={{ marginLeft: 10 }} /> */}
                  </div>
                ))}
            </div>

            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                marginTop: 8,
                marginBottom: 8
              }}
              onClick={this.props.handleScoreCard}
            >
              <div style={{ display: "flex", paddingRight: "6px" }}>
                <img
                  src={require("../../images/capsuleLeftWing.png")}
                  alt=""
                  style={{ width: 40, height: 2 }}
                />
              </div>
              <div
                className="line_limit"
                style={{
                  background: orange_2,
                  fontFamily: "Montserrat",
                  fontSize: 11,
                  color: grey_10,
                  padding: "2px 4px",
                  borderRadius: "12px",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  textAlign: "center",
                  minWidth: "75%",
                  maxWidth: "95%",
                  whiteSpace: "initial"
                }}
              >
                {score.status !== "UPCOMING" ? (
                  (score.status && score.status === "RUNNING"
                    ? score.toss
                    : score.statusStr) ||
                  `${moment(score.startDate).format(
                    "Do MMM YYYY, h:mm A "
                  )} ${window
                    .moment()
                    .tz(window.moment.tz.guess(true))
                    .format("z")}`
                ) : score.toss && score.status == "UPCOMING" ? (
                  <div>{score.toss}</div>
                ) : new Date(score.startDate).getTime() - new Date().getTime() >
                88200000 ? (
                  `${moment(score.startDate).format(
                    "Do MMM YYYY, h:mm A "
                  )} ${window
                    .moment()
                    .tz(window.moment.tz.guess(true))
                    .format("z")}`
                ) : (
                  this.state.hrString
                )}
              </div>
              <div style={{ display: "flex", paddingLeft: "6px" }}>
                <img
                  src={require("../../images/capsuleRightWing.png")}
                  alt=""
                  style={{ width: 40, height: 2 }}
                />
              </div>
            </div>
          </div>
          {score.status === "COMPLETED" &&
            score.manOfTheMatch &&
            score.manOfTheMatch[0] && (
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  // background: gradientGrey,
                  alignItems: "center",
                  fontFamily: "Montserrat",
                  padding: "6px 0",
                  //   marginLeft: "10px"
                  // marginTop: 158,
                  // width: 299,
                  // height: 59,
                  // position: "absolute",
                  color: grey_10,
                  ...this.props.rootStyles
                }}
                onClick={this.props.handleScoreCard}
              >
                <div
                  style={{
                    height: 38,
                    width: 38,
                    overflow: "hidden",
                    borderRadius: "50%",
                    marginRight: 12
                  }}
                >
                  <img
                    src={
                      (score.manOfTheMatch &&
                        score.manOfTheMatch[0] &&
                        score.manOfTheMatch[0].avatar &&
                        score.manOfTheMatch[0].avatar) ||
                      require("../../images/Man of the match-09.svg")
                    }
                    alt=""
                    style={{
                      width: "100%",
                      objectFit: "cover"
                    }}
                  />
                </div>
                <div
                  style={{
                    justifyContent: "space-between",
                    // flex: 0.95,
                    flexDirection: "column"
                  }}
                >
                  <div style={{ fontSize: 13, fontWeight: 500 }}>
                    {score.manOfTheMatch &&
                      score.manOfTheMatch[0] &&
                      score.manOfTheMatch[0].name}
                  </div>
                  <div style={{ fontSize: 12, fontWeight: 400 }}>
                    Player of the Match
                  </div>
                </div>
              </div>
            )}
        </div>
      </div>
    );
  }
}

export default ScheduleCard;
