import React, { Component } from "react";
import {
  grey_10,
  white,
  cardShadow,
} from "../../styleSheet/globalStyle/color";
import ButtonBar from "../commonComponent/ButtonBar";
import TabBar from "../commonComponent/TabBar";
import ScheduleCard from "./ScheduleCard";
// import MatchTitle from "./MatchTitle";
import CardGradientTitle from "../commonComponent/CardGradientTitle";

const FORMAT_TABS = [
  "all",
  "international",
  "domestic",
  "odi",
  "leagues",
  "women",
  "t20",
  "test"
];
const MATCH_STATUS = ["upcoming", "ongoing", "completed"];
class Schedule extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "international",
      activeButton: "ongoing"
    };
  }

  handleActiveButton = activeButton => {
    this.setState({
      activeButton
    });
  };

  handleActiveTab = activeTab => {
    this.setState({
      activeTab
    });
  };

  render() {
    return (
      <div
        style={{
          padding: "16px"
        }}
      >
        {/* Title */}
        <div
          style={{
            color: grey_10,
            fontFamily: "Montserrat",
            fontWeight: 700,
            fontSize: 22,
            marginBottom: 8
          }}
        >
          Schedule
        </div>

        {/* Schedule body Card */}
        <div
          style={{
            background: white,
            padding: "10px 0 0",
            borderRadius: 3,
            boxShadow: cardShadow
          }}
        >
          <ButtonBar
            items={MATCH_STATUS}
            activeItem={this.state.activeButton}
            onClick={this.handleActiveButton}
          />

          <div
            style={{
              marginTop: 4
            }}
          >
            <TabBar
              items={FORMAT_TABS}
              activeItem={this.state.activeTab}
              onClick={this.handleActiveTab}
            />
          </div>
          {/* <HrLine /> */}

          {/* Schedule Card */}
          <div>
            {/* Match Title */}
            <CardGradientTitle
              title="India’s Tour of Australia 2018"
              isRightIcon
            />
            {/* Match Body */}
            <ScheduleCard isSchedule={true} />
          </div>
          {/* Schedule Card closing */}
        </div>
        {/* Schedule Score Card */}
      </div>
    );
  }
}

export default Schedule;
