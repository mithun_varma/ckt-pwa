import React, { Component } from "react";
import {
  grey_10,
  orange_2,
  avatarShadow,
  grey_2,
} from "../../styleSheet/globalStyle/color";
import moment from "moment";
import HrLine from "../commonComponent/HrLine";
import map from "lodash/map";

class ScheduleCard extends Component {
  cardOnClick = path => {
    this.props.history.push(path);
  };
  render() {
    const { score, isSchedule } = this.props;
    const inningCollection = score.inningCollection;
    const inningOrder = score.inningOrder;
    var resultScoreCard = [];
    //checking number of inning
    //CASE-1
    var inning1 = [];
    var inning2 = [];
    var inning3 = [];
    var inning4 = [];
    var fullScoreCard = [];
    if (inningOrder && inningOrder.length < 2) {
      const keyOfInning1 = inningOrder[0];
      const inning1 = inningCollection && inningCollection[keyOfInning1];
      resultScoreCard.push({
        1: inning1
      });
    } else if (inningOrder && inningOrder.length < 3) {
      const keyOfInning1 = inningOrder[0];
      const keyOfInning2 = inningOrder[1];
      const inning1 = inningCollection && inningCollection[keyOfInning1];
      const inning2 = inningCollection && inningCollection[keyOfInning2];
      resultScoreCard.push({
        1: inning1,
        2: inning2
      });
    } else if (inningOrder && inningOrder.length < 4) {
      const keyOfInning1 = inningOrder[0];
      const keyOfInning2 = inningOrder[1];
      const keyOfInning3 = inningOrder[2];
      const inning1 = inningCollection && inningCollection[keyOfInning1];
      const inning2 = inningCollection && inningCollection[keyOfInning2];
      const inning3 = inningCollection && inningCollection[keyOfInning3];
      resultScoreCard.push({
        1: inning1,
        2: inning2,
        3: inning3
      });
    } else if (inningOrder && inningOrder.length < 5) {
      const keyOfInning1 = inningOrder[0];
      const keyOfInning2 = inningOrder[1];
      const keyOfInning3 = inningOrder[2];
      const keyOfInning4 = inningOrder[3];
      const inning1 = inningCollection && inningCollection[keyOfInning1];
      const inning2 = inningCollection && inningCollection[keyOfInning2];
      const inning3 = inningCollection && inningCollection[keyOfInning3];
      const inning4 = inningCollection && inningCollection[keyOfInning4];
      resultScoreCard.push({
        1: inning1,
        2: inning2,
        3: inning3,
        4: inning4
      });
    }

    let updatedScoreCard = resultScoreCard[0];
    let inningTeams = [];
    let updatedTeams = [];
    let multiInningRuns = [];
    let multiInningWickets = [];
    let multiInningOvers = [];

    for (var key in updatedScoreCard) {
      inningTeams.push(updatedScoreCard[key] && updatedScoreCard[key].teamKey);
    }

    function removeDups(names) {
      let unique = {};
      names.forEach(function(i) {
        if (!unique[i]) {
          unique[i] = true;
        }
      });
      return Object.keys(unique);
    }
    updatedTeams = removeDups(inningTeams);

    for (var key in updatedScoreCard) {
      multiInningRuns.push(updatedScoreCard[key] && updatedScoreCard[key].runs);
    }

    for (var key in updatedScoreCard) {
      multiInningWickets.push(
        updatedScoreCard[key] && updatedScoreCard[key].wickets
      );
    }

    for (var key in updatedScoreCard) {
      multiInningOvers.push(
        updatedScoreCard[key] && updatedScoreCard[key].overs
      );
    }
    // if(inningCollection &&  inningCollection.length>0){
    return (
      <div>
        <div
          style={{ padding: 16, ...this.props.rootStyles }}
          onClick={e => {
            e.preventDefault();
            this.props.history.push(`/score/${score.matchId}`);
          }}
        >
          <div
            style={{
              fontFamily: "Montserrat",
              fontWeight: 500,
              fontSize: 12,
              color: grey_10,
              paddingBottom: 6,
              display: "flex",
              alignItems: "center",
              overflow: "hidden"
            }}
          >
            {score.matchStatus == "RUNNING" && (
              <span
                style={{
                  border: "solid 0.6px #979797",
                  borderRadius: "7.5px",
                  padding: "1px 8px",
                  display: "inline-block",
                  fontSize: "8px",
                  marginRight: "6px",
                  // marginLeft: "10px",
                  textTransform: "uppercase"
                }}
              >
                <span
                  style={{
                    width: "5px",
                    height: "5px",
                    borderRadius: "50%",
                    marginLeft: "-2px",
                    marginRight: "4px",
                    backgroundColor: "green",
                    display: "inline-block"
                  }}
                />
                live
              </span>
            )}
            {/* <span>-- </span> */}
            {score.relatedName && (
              <React.Fragment>
                <span>{score.relatedName}</span>
                <span style={{ padding: "0 6px" }}>•</span>
              </React.Fragment>
            )}
            <span
              style={{
                maxWidth: "26ch",
                whiteSpace: "nowrap",
                overflow: "hidden"
              }}
            >
              {/* {score.venue && score.venue} */}
              {score.seriesName && score.seriesName}
            </span>
          </div>
          {/* New Card Section */}
          <div
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              fontFamily: "Montserrat",
              fontWeight: 500
            }}
          >
            {/* 1 */}

            {score.status === "COMPLETED" || score.status === "RUNNING" ? (
              resultScoreCard[0] && resultScoreCard[0].length < 3 ? (
                map(resultScoreCard[0], (item, key) => (
                  <div
                    style={{
                      display: "flex",
                      flex: 1,
                      alignItems: "center",
                      paddingBottom: 6,
                      fontFamily: "Montserrat"
                    }}
                  >
                    <div style={{ display: "flex", flex: 0.25 }}>
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <img
                          src={require("../../images/indianFlag.svg")}
                          alt=""
                          style={{
                            width: 24,
                            height: 16,
                            boxShadow: avatarShadow
                          }}
                        />
                      </div>
                      <div style={{ marginLeft: 10 }}>
                        {item
                          ? item.teamKey === score.teamA.name
                            ? score.teamA.shortName || score.teamA.name
                            : score.teamB.name
                          : ""}
                      </div>
                    </div>
                    {score.status !== "upcoming" && item && item.runs ? (
                      <div style={{ display: "flex", flex: 0.2 }}>
                        <span>
                          {item && item.runs}/{item && item.wickets}
                        </span>
                      </div>
                    ) : (
                      ""
                    )}
                    {score.status !== "upcoming" && item && item.runs ? (
                      <div style={{ display: "flex", flex: 0.25 }}>
                        <span> ({item && item.overs})</span>
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                ))
              ) : (
                <div>
                  <div
                    style={{
                      display: "flex",
                      flex: 1,
                      alignItems: "center",
                      paddingBottom: 6,
                      fontSize: 14
                      // justifyContent: 'space-between'
                    }}
                  >
                    <div style={{ display: "flex", flex: 0.4 }}>
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <img
                          src={require("../../images/indianFlag.svg")}
                          alt=""
                          style={{
                            width: 24,
                            height: 16,
                            boxShadow: avatarShadow
                          }}
                        />
                      </div>
                      <div style={{ marginLeft: 10 }}>
                        {updatedTeams[0] === score.teamA.teamKey ||
                        updatedTeams[0] === score.teamA.name
                          ? score.teamA.name.substring(0, 3).toUpperCase()
                          : updatedTeams[0] === score.teamB.teamKey ||
                            updatedTeams[0] === score.teamB.name
                            ? score.teamB.name.substring(0, 3).toUpperCase()
                            : score.teamA.name.substring(0, 3).toUpperCase()}
                      </div>
                    </div>
                    <div style={{ flex: 0.6, display: "flex" }}>
                      {score.status !== "upcoming" &&
                      multiInningOvers.length > 2 ? (
                        <div
                          style={{
                            display: "flex"
                          }}
                        >
                          <span style={{ marginRight: 6 }}>{`${
                            multiInningRuns[0] ? multiInningRuns[0] : ""
                          }${
                            multiInningWickets[0] !== undefined
                              ? "/" + multiInningWickets[0]
                              : ""
                          }`}</span>
                          <span style={{ marginRight: 6 }}>
                            {multiInningRuns[1] && "&"}
                          </span>
                          <span style={{ marginRight: 6 }}>{`${
                            multiInningRuns[1] ? multiInningRuns[1] : ""
                          }${
                            multiInningWickets[1] !== undefined
                              ? "/" + multiInningWickets[1]
                              : ""
                          }`}</span>
                        </div>
                      ) : (
                        <div
                          style={{
                            display: "flex"
                          }}
                        >
                          <span style={{ marginRight: 6 }}>{`${
                            multiInningRuns[0] ? multiInningRuns[0] : ""
                          }${
                            multiInningWickets[0] !== undefined
                              ? "/" + multiInningWickets[0]
                              : ""
                          }`}</span>
                        </div>
                      )}
                      {score.format !== "TEST" && (
                        <div
                          style={{
                            display: "flex",
                            alignItems: "center",
                            fontSize: 12
                          }}
                        >
                          {score.status !== "upcoming" &&
                          multiInningOvers.length > 2 ? (
                            <span>{`(${multiInningOvers[1]})`}</span>
                          ) : (
                            <span>{`(${multiInningOvers[0]})`}</span>
                          )}
                        </div>
                      )}
                    </div>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flex: 1,
                      alignItems: "center",
                      paddingBottom: 6,
                      justifyContent: "space-between",
                      fontSize: 14
                    }}
                  >
                    <div style={{ display: "flex", flex: 0.4 }}>
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <img
                          src={require("../../images/indianFlag.svg")}
                          alt=""
                          style={{
                            width: 24,
                            height: 16,
                            boxShadow: avatarShadow
                          }}
                        />
                      </div>
                      <div style={{ marginLeft: 10 }}>
                        {updatedTeams[1] === score.teamA.teamKey ||
                        updatedTeams[1] === score.teamA.name
                          ? score.teamA.name.substring(0, 3).toUpperCase()
                          : updatedTeams[1] === score.teamB.teamKey ||
                            updatedTeams[1] === score.teamB.name
                            ? score.teamB.name.substring(0, 3).toUpperCase()
                            : score.teamB.name.substring(0, 3).toUpperCase()}
                      </div>
                    </div>
                    <div style={{ display: "flex", flex: 0.6 }}>
                      {score.status !== "upcoming" &&
                      multiInningOvers.length > 2 ? (
                        <div
                          style={{
                            display: "flex",
                            alignSelf: "flex-start"
                          }}
                        >
                          <span style={{ marginRight: 6 }}>{`${
                            multiInningRuns[2] ? multiInningRuns[2] : ""
                          }${
                            multiInningWickets[2] !== undefined
                              ? "/" + multiInningWickets[2]
                              : ""
                          }`}</span>
                          <span style={{ marginRight: 6 }}>
                            {multiInningRuns[3] && "&"}
                          </span>
                          <span style={{ marginRight: 6 }}>{`${
                            multiInningRuns[3] ? multiInningRuns[3] : ""
                          }${
                            multiInningWickets[3] !== undefined
                              ? "/" + multiInningWickets[3]
                              : ""
                          }`}</span>
                        </div>
                      ) : (
                        <div
                          style={{
                            display: "flex",
                            // flex: 0.50,
                            alignSelf: "flex-start"
                          }}
                        >
                          <span style={{ marginRight: 6 }}>{`${
                            multiInningRuns[1] ? multiInningRuns[1] : ""
                          }${
                            multiInningWickets[1] !== undefined
                              ? "/" + multiInningWickets[1]
                              : ""
                          }`}</span>
                        </div>
                      )}
                      {!score.format !== "TEST" && (
                        <div
                          style={{
                            display: "flex",
                            fontSize: 12,
                            alignItems: "center"
                          }}
                        >
                          {score.status !== "upcoming" &&
                          multiInningOvers.length > 2 ? (
                            <span>
                              {multiInningOvers[3]
                                ? `(${multiInningOvers[3]})`
                                : multiInningOvers[2]
                                  ? `(${multiInningOvers[2]})`
                                  : ""}
                            </span>
                          ) : (
                            <span>
                              {multiInningOvers[1]
                                ? `(${multiInningOvers[1]})`
                                : multiInningOvers[2]
                                  ? `${multiInningOvers[2]}`
                                  : ""}
                            </span>
                          )}
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              )
            ) : (
              <div
                style={{
                  display: "flex",
                  flex: 1,
                  alignItems: "flex-start",
                  paddingBottom: 6,
                  flexDirection: "column",
                  fontFamily: "Montserrat",
                  fontWeight: 500
                }}
              >
                <div style={{ display: "flex", flex: 0.5, marginBottom: 10 }}>
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <img
                      src={require("../../images/indianFlag.svg")}
                      alt=""
                      style={{ width: 24, height: 16, boxShadow: avatarShadow }}
                    />
                  </div>
                  <div style={{ marginLeft: 10, fontSize: 14 }}>
                    {score &&
                      score.teamA &&
                      score.teamA.name.substring(0, 3).toUpperCase()}
                  </div>
                </div>
                <div style={{ display: "flex", flex: 0.5 }}>
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <img
                      src={require("../../images/indianFlag.svg")}
                      alt=""
                      style={{ width: 24, height: 16, boxShadow: avatarShadow }}
                    />
                  </div>
                  <div style={{ marginLeft: 10, fontSize: 14 }}>
                    {score &&
                      score.teamB &&
                      score.teamB.name.substring(0, 3).toUpperCase()}
                  </div>
                </div>
              </div>
            )}
          </div>

          {/* Time Capsule */}
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              marginTop: 10
            }}
          >
            <div style={{ display: "flex", padding: "0 8px" }}>
              <img
                src={require("../../images/capsuleLeftWing.png")}
                alt=""
                style={{ width: 40, height: 2 }}
              />
            </div>
            <div
              style={{
                background: orange_2,
                fontFamily: "Montserrat",
                fontSize: 11,
                color: grey_10,
                padding: "2px 16px",
                borderRadius: "12px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                textAlign: "center",
                maxWidth: "85%",
                whiteSpace: "initial"
              }}
            >
              {score.matchStatus == "COMPLETED" ||
              score.matchStatus === "RUNNING"
                ? score.statusStr || score.toss
                : `${moment(score.startDate).format(
                    "Do MMM YYYY, h:mm A "
                  )} ${window.moment().tz(window.moment.tz.guess(true)).format('z')}`}
            </div>
            <div style={{ display: "flex", padding: "0 8px" }}>
              <img
                src={require("../../images/capsuleRightWing.png")}
                alt=""
                style={{ width: 40, height: 2 }}
              />
            </div>
          </div>
          {/* Time Capsule Closing */}
          {/* New Card Section Closing */}
        </div>

        {/* MAN of the Match */}
        {score.matchStatus == "COMPLETED" &&
          score.manOfTheMatch && (
            <div>
              <HrLine />
              <div
                style={{ padding: 16, display: "flex", alignItems: "center" }}
              >
                <div style={{ display: "flex", flex: 0.16 }}>
                  <div
                    style={{
                      width: 35,
                      height: 33,
                      borderRadius: "50%",
                      background: grey_2,
                      backgroundImage: `url(${require("../../images/dhoni.png")})`,
                      backgroundSize: "cover",
                      backgroundPosition: "center",
                      backgroundRepeat: "no-repeat"
                    }}
                  />
                </div>
                <div style={{ flex: 0.84 }}>
                  <div
                    style={{
                      display: "flex",
                      fontFamily: "Montserrat",
                      fontWeight: 600,
                      fontSize: 12,
                      color: grey_10
                    }}
                  >
                    <div style={{ flex: 0.75 }}>
                      {score.manOfTheMatch
                        ? score.manOfTheMatch[0] && score.manOfTheMatch[0].name
                          ? score.manOfTheMatch[0].name
                          : ""
                        : ""}
                    </div>
                    {/* <div>5/37 (7.3)</div> */}
                  </div>
                  <div
                    style={{
                      fontFamily: "Montserrat",
                      fontSize: 11,
                      color: grey_10
                    }}
                  >
                    Player of the Match
                  </div>
                </div>
              </div>
            </div>
          )}
        {/* MAN of the Match Closing */}
      </div>
    );
    // }
    // else{
    //   return(null);
    // }
  }
}

export default ScheduleCard;
