import React, { Component } from "react";
import {
  grey_10,
  orange_2,
  avatarShadow,
  grey_8
} from "../../styleSheet/globalStyle/color";
import moment from "moment";
// import "./style.css";
import Polling from "../commonComponent/Polling";
import NotificationsNone from "@material-ui/icons/NotificationsNone";
import Notifications from "@material-ui/icons/Notifications";
import { isAndroid } from "react-device-detect";

class HomeScoreCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hrString: "",
      initial: true
    };
    this.timeout = 0;
  }
  handleNotification = () => {
    if (isAndroid) {
      document
        .getElementById("notification-bell")
        .animate(
          [
            { transform: "rotate(35deg)" },
            { transform: "rotate(-30deg)" },
            { transform: "rotate(25deg)" },
            { transform: "rotate(-20deg)" },
            { transform: "rotate(15deg)" },
            { transform: "rotate(-10deg)" },
            { transform: "rotate(5deg)" },
            { transform: "rotate(0deg)" }
          ],
          {
            duration: 800,
            iterations: 1
          }
        );
    }

    setTimeout(() => {
      this.props.handleNotification();
    }, 800);
  };

  componentDidMount = () => {
    if (
      this.props.score &&
      this.props.score.status === "UPCOMING" &&
      this.props.score.startDate
    ) {
      this.handleCountdownTimer();
    }
  };
  handleCountdownTimer = () => {
    const startDate = this.props.score && this.props.score.startDate;
    let countDownDate = new Date(startDate).getTime() - 1800000;
    // console.log("inside : ", countDownDate);
    let now = new Date().getTime();
    // Find the distance between now and the count down date
    let distance = countDownDate - now;
    // Time calculations for days, hours, minutes and seconds
    // let days = Math.floor(distance / (1000 * 60 * 60 * 24));
    let hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((distance % (1000 * 60)) / 1000);
    let result =
      (hours > 0 ? hours + (hours > 1 ? " hrs " : " hr ") : "") +
      minutes +
      (minutes > 1 ? " mins " : " min ") +
      " to toss";
    // let result = hours + " hrs " + minutes + " min for toss";
    // Output the result in an element with id="time-section"
    // If the count down is over, write some text
    if (distance < 0) {
      clearInterval(this.timeout);
      // console.log("NO: ", result);
      this.setState({
        hrString: "0 hr 0 min to toss"
      });
      // return "0 hr 0 min to toss";
      // return "EXPIRED";
    } else {
      // console.log("yes: ", result);
      this.setState({
        hrString: result
      });
      // return result;
      // document.getElementById("time-section").innerHTML = result || "testing";
    }
    // console.log("called : ", startDate);
    this.timeout = setInterval(() => {
      // Get today's date and time
      let countDownDate = new Date(startDate).getTime() - 1800000;
      // console.log("inside : ", countDownDate);
      let now = new Date().getTime();
      // Find the distance between now and the count down date
      let distance = countDownDate - now;
      // Time calculations for days, hours, minutes and seconds
      // let days = Math.floor(distance / (1000 * 60 * 60 * 24));
      let hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((distance % (1000 * 60)) / 1000);
      let result =
        (hours > 0 ? hours + (hours > 1 ? " hrs " : " hr ") : "") +
        minutes +
        (minutes > 1 ? " mins " : " min ") +
        " to toss";
      // let result = hours + " hrs " + minutes + " min for toss";
      // Output the result in an element with id="time-section"
      // If the count down is over, write some text
      if (distance < 0) {
        clearInterval(this.timeout);
        // console.log("NO: ", result);
        this.setState({
          hrString: "0 hr 0 min to toss"
        });
        // return "0 hr 0 min to toss";
        // return "EXPIRED";
      } else {
        // console.log("yes: ", result);
        this.setState({
          hrString: result
        });
        // return result;
        // document.getElementById("time-section").innerHTML = result || "testing";
      }
    }, 1000);
  };

  componentWillUnmount = () => {
    clearInterval(this.timeout);
  };

  render() {
    const { score } = this.props;
    // const { inningCollection } = score;
    // let inningsArray = [];
    let teamCollection = [
      {
        teamName:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.fullName
            : score.teamB.fullName,
        shortName:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.shortName
            : score.teamB.shortName,
        teamKey:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.teamKey
            : score.teamB.teamKey,
        runs: [],
        wickets: [],
        overs: [],
        declared: [],
        followOn: [],
        avatar:
          score.teamA.teamKey === score.firstBatting
            ? score.teamA.avatar
            : score.teamB.avatar
      },
      {
        teamName:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.fullName
            : score.teamB.fullName,
        shortName:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.shortName
            : score.teamB.shortName,
        teamKey:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.teamKey
            : score.teamB.teamKey,
        runs: [],
        wickets: [],
        overs: [],
        declared: [],
        followOn: [],
        avatar:
          score.teamA.teamKey !== score.firstBatting
            ? score.teamA.avatar
            : score.teamB.avatar
      }
    ];
    if (score.status !== "UPCOMING") {
      for (const order of score.inningOrder) {
        let team = order.split("_")[0];
        if (team === score.firstBatting) {
          (teamCollection[0].runs = [
            ...teamCollection[0].runs,
            score.inningCollection[order].runs
          ]),
            (teamCollection[0].wickets = [
              ...teamCollection[0].wickets,
              score.inningCollection[order].wickets
            ]),
            (teamCollection[0].overs = [
              ...teamCollection[0].overs,
              score.inningCollection[order].overs
            ]),
            (teamCollection[0].declared = [
              ...teamCollection[0].declared,
              score.inningCollection[order].declared
            ]),
            (teamCollection[0].followOn = [
              ...teamCollection[0].followOn,
              score.inningCollection[order].followOn
            ]);
        } else {
          (teamCollection[1].runs = [
            ...teamCollection[1].runs,
            score.inningCollection[order].runs
          ]),
            (teamCollection[1].wickets = [
              ...teamCollection[1].wickets,
              score.inningCollection[order].wickets
            ]),
            (teamCollection[1].overs = [
              ...teamCollection[1].overs,
              score.inningCollection[order].overs
            ]),
            (teamCollection[1].declared = [
              ...teamCollection[1].declared,
              score.inningCollection[order].declared
            ]),
            (teamCollection[1].followOn = [
              ...teamCollection[1].followOn,
              score.inningCollection[order].followOn
            ]);
        }
      }
    } else if (score.status == "UPCOMING") {
      for (let i = 0; i < teamCollection.length; i++) {
        (teamCollection[i].teamName =
          score[i == 0 ? "teamA" : "teamB"].fullName),
          (teamCollection[i].shortName =
            score[i == 0 ? "teamA" : "teamB"].shortName),
          (teamCollection[i].teamKey =
            score[i == 0 ? "teamA" : "teamB"].teamKey);
        (teamCollection[i].runs = []),
          (teamCollection[i].wickets = []),
          (teamCollection[i].overs = []),
          (teamCollection[i].declared = []),
          (teamCollection[i].followOn = []),
          (teamCollection[i].avatar = score[i == 0 ? "teamA" : "teamB"].avatar);
      }
    }

    // console.log(teamCollection);

    // console.log("Score: ", teamCollection)
    return (
      <div>
        <div
          style={{
            // backgroundColor: "#edeff5",
            position: "relative",
            width: "96%"
            // padding: "28"
          }}
        >
          {score.status !== "COMPLETED" &&
            !this.props.isSeries && (
              <div className={`notification-wrapper ${this.props.className}`}>
                <div
                  className="outer-wrapper"
                  // onClick={this.props.handleNotification}
                  onClick={this.handleNotification}
                >
                  {this.props.notificationActive ? (
                    <Notifications
                      id="notification-bell"
                      style={{
                        fill: "#a70f14",
                        marginTop: 10,
                        marginLeft: 24,
                        fontSize: 24
                      }}
                    />
                  ) : (
                    <NotificationsNone
                      id="notification-bell"
                      style={{
                        marginTop: 10,
                        marginLeft: 24,
                        fontSize: 24,
                        color: grey_8
                      }}
                    />
                  )}
                </div>
              </div>
            )}
          <div
            className={
              score.status === "COMPLETED" && !this.props.isSeries
                ? "scoreCard"
                : "scoreCard card-clip"
            }
            style={{
              padding: "8px 16px",
              width: "98%",
              ...this.props.cardStyles
            }}
          >
            <div
              style={{
                fontFamily: "Montserrat",
                fontWeight: 500,
                fontSize: 14,
                color: grey_10,
                display: "flex",
                alignItems: "center",
                marginBottom: 4,
                whiteSpace: "nowrap",
                overflow: "hidden"
              }}
            >
              <span>{score && score.relatedName ? score.relatedName : ""}</span>
              {score &&
                score.relatedName && (
                  <span style={{ padding: "0 6px" }}>•</span>
                )}
              <span
                style={
                  score.status !== "COMPLETED"
                    ? {
                        maxWidth: "17ch"
                      }
                    : { maxWidth: "20ch" }
                }
                className="series-text"
              >
                {score && score.seriesName
                  ? score.seriesName.substring(0, 25)
                  : "--"}
              </span>
            </div>
            <div
              style={{
                fontFamily: "Montserrat",
                fontWeight: 500,
                fontSize: 12,
                color: "#727682",
                display: "flex",
                alignItems: "center",
                marginBottom: "8px",
                boxShadow: "0 16px 10px -17px cardShadow"
              }}
            >
              {score &&
                score.status === "RUNNING" &&
                score.firstBallStatus === "RUNNING" && (
                  // score.statusOverView === "RUNNING" &&
                  <span
                    style={{
                      border: "solid 0.6px #979797",
                      borderRadius: "7.5px",
                      padding: "1px 8px",
                      display: "inline-block",
                      fontSize: "8px",
                      marginRight: "10px",
                      textTransform: "uppercase"
                    }}
                  >
                    <span
                      style={{
                        width: "5px",
                        height: "5px",
                        borderRadius: "50%",
                        marginLeft: "-2px",
                        marginRight: "4px",
                        backgroundColor: "#35a863",
                        display: "inline-block"
                      }}
                    />
                    live
                  </span>
                )}
              <img
                src={require("../../images/location-icon.svg")}
                alt=""
                style={{
                  width: 9,
                  height: 12,
                  right: 10,
                  top: 10
                }}
              />
              <span
                style={{
                  marginLeft: 5,
                  whiteSpace: "nowrap",
                  maxWidth: "25ch",
                  overflow: "hidden"
                }}
              >
                {(score && score.venue && score.venue) || "Adelaid"}
              </span>
            </div>

            <div
              style={{
                flex: 1,
                flexDirection: "column",
                // width: 300,
                backgroundColor: "#ffffff",
                // height: 92,
                justifyContent: "space-between"
                // marginTop: 63,
                // position: "absolute"
              }}
            >
              <div
                style={{
                  flexDirection: "column",
                  justifyContent: "space-between",
                  position: "relative",
                  fontFamily: "Montserrat"
                }}
                onClick={this.props.handleScoreCard}
              >
                {teamCollection &&
                  teamCollection.map((team, index) => (
                    <div key={team.teamKey} style={{ display: "flex" }}>
                      <div
                        style={{
                          display: "flex",
                          flex: 1,
                          alignItems: "center",
                          justifyContent: "space-between",
                          flexDirection: "row",
                          marginBottom: `${
                            index === 0
                              ? "6px"
                              : score.status === "UPCOMING"
                                ? "6px"
                                : "0px"
                          }`
                        }}
                      >
                        <div
                          style={{
                            flex: 0.4,
                            display: "flex",
                            alignItems: "center"
                          }}
                        >
                          <img
                            src={
                              team.avatar
                                ? team.avatar
                                : team.teamName == "tbc" ||
                                  team.teamName == "TBC"
                                  ? require("../../images/TBC-Flag.png")
                                  : require("../../images/flag_empty.svg")
                            }
                            alt=""
                            style={{
                              width: 24,
                              height: 16,
                              boxShadow: avatarShadow,
                              // marginLeft: 13,
                              marginRight: 14
                            }}
                          />
                          {/* {score.status !== "UPCOMING" ? ( */}
                          <span style={{ fontWeight: 400, fontSize: 14 }}>
                            {(team.shortName && team.shortName.length > 4
                              ? team.shortName.substring(0, 4).toUpperCase()
                              : team.shortName.toUpperCase()) || "-"}
                          </span>
                          {/* ) : (
                          <span style={{ fontWeight: 400, fontSize: 14 }}>
                            {(score &&
                              score.teamA.shortName
                                .substring(0, 3)
                                .toUpperCase()) ||
                              "-"}
                          </span>
                        )} */}
                        </div>
                        <div style={{ flex: 0.6, flexDirection: "row" }}>
                          {score.status !== "UPCOMING" ? (
                            <span
                              style={{
                                // fontWeight: `${
                                //   score.status === "RUNNING" &&
                                //   team.teamKey ===
                                //     (score.currentScoreCard &&
                                //       score.currentScoreCard.battingTeam)
                                //     ? "700"
                                //     : "400"
                                // }`,
                                fontSize: 14
                              }}
                            >
                              <span style={{
                                  fontWeight: `${
                                    score.status === "RUNNING" &&
                                    score.currentScoreCard &&
                                    team.teamKey === score.currentScoreCard.teamKey &&
                                    team.runs.length < 2
                                      ? "600"
                                      : "400"
                                  }`
                                }}>
                              <div style={{
                                display: "inline-block",
                                minWidth: score.format == "TEST" ? 58 : "auto"
                              }}>
                              {`${
                                team.runs[0] !== undefined
                                  ? `${team.runs[0]}`
                                  : ""
                              }${
                                team.wickets[0] !== undefined &&
                                team.wickets[0] < 10
                                  ? `/${team.wickets[0]}`
                                  : ""
                              }`}
                              {
                                team.declared[0]
                                  ? <span style={{
                                    color: "#d64b4b",
                                    fontWeight: 400
                                  }}>{` d`}</span>
                                  : ""
                              }
                              </div>
                                {(score.format !== "TEST" ||
                                    score.status === "RUNNING" &&
                                    score.currentScoreCard &&
                                    team.teamKey === score.currentScoreCard.teamKey &&
                                    team.runs.length < 2
                                  ) &&
                                  team.overs[0] &&
                                  <span style={{ fontWeight: 400, marginLeft: 8 }}>
                                    ({team.overs[0]})
                                  </span>
                                }
                              </span>
                              <span style={{
                                  fontWeight: `${
                                    score.status === "RUNNING" &&
                                    score.currentScoreCard &&
                                    team.teamKey === score.currentScoreCard.teamKey &&
                                    team.runs.length > 1
                                      ? "600"
                                      : "400"
                                  }`
                                }}>
                              {team.runs.length > 1 &&
                                ` & ${
                                  team.runs[1] !== undefined ? team.runs[1] : ""
                                }${
                                  team.wickets[1] !== undefined &&
                                  team.wickets[1] < 10
                                    ? "/" + team.wickets[1]
                                    : ""
                                }`}
                                {
                                  team.declared.length > 1 && team.declared[1]
                                    ? <span style={{
                                      color: "#d64b4b",
                                      fontWeight: 400
                                    }}>{` d`}</span>
                                    : ""
                                }
                                {
                                  team.followOn.length > 1 && team.followOn[1]
                                    ? <span style={{
                                      color: "#d64b4b",
                                      fontWeight: 400
                                    }}>{` f/o`}</span>
                                    : ""
                                }
                                {team.runs.length > 1 &&
                                  (score.format !== "TEST" ||
                                  score.status === "RUNNING" &&
                                  score.currentScoreCard &&
                                  team.teamKey === score.currentScoreCard.teamKey &&
                                  team.runs.length > 1) &&
                                  <span style={{ fontWeight: 400, marginLeft: 8 }}>
                                    ({team.overs[1]})
                                  </span>
                                }

                              {/* To show DLS method text */}
                              {score &&
                                score.dlApplied &&
                                index == 1 && (
                                  <span
                                    style={{
                                      color: grey_8,
                                      fontSize: 12
                                    }}
                                  >
                                    {" "}
                                    (DLS)
                                  </span>
                                )}
                                </span>
                            </span>
                          ) : null}
                        </div>
                      </div>
                      {/* <div style={{ marginLeft: 10 }} /> */}
                    </div>
                  ))}
                {score &&
                  score.status &&
                  score.status === "UPCOMING" && (
                    <div
                      style={{
                        position: "absolute",
                        top: 2,
                        left: "40%",
                        // background: "#f1f1f1",
                        borderLeft: "1px solid #e2e2e2",
                        fontFamily: "Montserrat",
                        fontSize: 14,
                        fontWeight: 400,
                        color: grey_10,
                        padding: "0 18px"
                      }}
                    >
                      <div>
                        {score && moment(score.startDate).format("Do MMMM ")}
                      </div>
                      <div style={{ marginTop: 5 }}>
                        {score &&
                          moment(score.startDate).format("h:mm A ") +
                            window
                              .moment()
                              .tz(window.moment.tz.guess(true))
                              .format("z")}
                      </div>
                    </div>
                  )}
              </div>

              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop: 8,
                  marginBottom: 8
                }}
                onClick={this.props.handleScoreCard}
              >
                <div style={{ display: "flex", padding: "0 6px 0 0" }}>
                  <img
                    src={require("../../images/capsuleLeftWing.png")}
                    alt=""
                    style={{ width: 40, height: 2 }}
                  />
                </div>
                <div
                  className="line_limit"
                  style={{
                    background: orange_2,
                    fontFamily: "Montserrat",
                    fontSize: 11,
                    color: grey_10,
                    padding: "2px 4px",
                    borderRadius: "12px",
                    display: "flex",
                    justifyContent: "center",
                    // alignItems: "center",
                    // textAlign: "center",
                    minWidth: "75%",
                    maxWidth: "95%",
                    whiteSpace: "initial",
                    textAlign: "center"
                  }}
                >
                  {score.status !== "UPCOMING" ? (
                    (score.status && score.status === "RUNNING"
                      ? score.toss
                      : score.statusStr) ||
                    `${moment(score.startDate).format(
                      "Do MMM YYYY, h:mm A "
                    )} ${window
                      .moment()
                      .tz(window.moment.tz.guess(true))
                      .format("z")}`
                  ) : score.toss && score.status == "UPCOMING" ? (
                    <div>{score.toss}</div>
                  ) : new Date(score.startDate).getTime() -
                    new Date().getTime() >
                  88200000 ? (
                    `${moment(score.startDate).format(
                      "Do MMM YYYY, h:mm A "
                    )} ${window
                      .moment()
                      .tz(window.moment.tz.guess(true))
                      .format("z")}`
                  ) : (
                    this.state.hrString
                  )}
                </div>
                <div style={{ display: "flex", padding: "0 0 0 6px" }}>
                  <img
                    src={require("../../images/capsuleRightWing.png")}
                    alt=""
                    style={{ width: 40, height: 2 }}
                  />
                </div>
              </div>
            </div>
            {score.status === "COMPLETED" &&
            score.manOfTheMatch &&
            score.manOfTheMatch[0] ? (
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  // background: gradientGrey,
                  alignItems: "center",
                  fontFamily: "Montserrat",
                  padding: "2px",
                  minHeight: 47,
                  // marginLeft: "10px",
                  // marginTop: 158,
                  // width: 299,
                  // height: 59,
                  // position: "absolute",
                  color: grey_10
                  // ...this.props.rootStyles
                }}
                onClick={this.props.handleScoreCard}
              >
                <div
                  style={{
                    height: 38,
                    width: 38,
                    overflow: "hidden",
                    borderRadius: "50%",
                    marginRight: 12
                  }}
                >
                  <img
                    src={
                      (score.manOfTheMatch &&
                        score.manOfTheMatch[0] &&
                        score.manOfTheMatch[0].avatar &&
                        score.manOfTheMatch[0].avatar) ||
                      require("../../images/Man of the match-09.svg")
                    }
                    alt=""
                    style={{
                      width: "100%",
                      objectFit: "cover"
                      // marginLeft: 30,
                      // maxWidth: 34.2,
                      // height: 38,
                      // width: 38,
                      // alignSelf: "center",
                      // marginLeft: 80.4,
                      // borderRadius: "50%",
                      // marginRight: "12px",
                      // resize: "contain"
                    }}
                  />
                </div>

                <div
                  style={{
                    justifyContent: "space-between",
                    // flex: 0.95,
                    flexDirection: "column"
                  }}
                >
                  <div
                    style={{
                      fontSize: 13,
                      fontWeight: 500,
                      whiteSpace: "nowrap",
                      overflow: "hidden",
                      textOverflow: "ellipsis"
                    }}
                  >
                    {score.manOfTheMatch &&
                      score.manOfTheMatch[0] &&
                      score.manOfTheMatch[0].name}
                  </div>
                  <div style={{ fontSize: 12, fontWeight: 400 }}>
                    Player of the Match
                  </div>
                </div>
              </div>
            ) : score.status === "RUNNING" || score.status === "UPCOMING" ? (
              !this.props.isSeries ? (
                <div
                  style={{
                    alignSelf: "center",
                    // marginLeft: 10,
                    // background: gradientGrey,
                    position: "relative"
                    // marginRight: 10,
                    // top: 165,
                    // height: 49
                  }}
                  onClick={this.props.handleScoreCard}
                >
                  <Polling
                    // style={{ padding: "2px 15px" }}
                    data={score && score.prediction && score.prediction}
                    score={score}
                  />
                </div>
              ) : null
            ) : (
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "flex-start",
                  // background: gradientGrey,
                  alignItems: "center",
                  fontFamily: "Montserrat",
                  padding: "4px",
                  // marginLeft: "10px",
                  // marginTop: 158,
                  // width: 299,
                  // height: 59,
                  // position: "absolute",
                  color: grey_10,
                  ...this.props.rootStyles
                }}
                onClick={this.props.handleScoreCard}
              >
                <div
                  style={{
                    height: 38,
                    width: 38,
                    overflow: "hidden",
                    borderRadius: "50%",
                    marginRight: 12
                  }}
                >
                  <img
                    src={
                      score.matchCancelled
                        ? require("../../images/Abandoned_Icon.png")
                        : require("../../images/Man of the match-09.svg")
                    }
                    alt=""
                    style={{
                      width: "100%",
                      objectFit: "cover"
                      // marginLeft: 30,
                      // maxWidth: 34.2,
                      // height: 38,
                      // width: 38,
                      // alignSelf: "center",
                      // marginLeft: 80.4,
                      // borderRadius: "50%",
                      // marginRight: "12px",
                      // resize: "contain"
                    }}
                  />
                </div>

                <div
                  style={{
                    justifyContent: "space-between",
                    // flex: 0.95,
                    flexDirection: "column"
                  }}
                >
                  <div
                    style={{
                      fontSize: 13,
                      fontWeight: 500,
                      whiteSpace: "nowrap",
                      overflow: "hidden",
                      textOverflow: "ellipsis"
                    }}
                  >
                    {score.matchCancelled ? "Match Abandoned" : "TBA"}
                  </div>
                  {!score.matchCancelled ? (
                    <div style={{ fontSize: 12, fontWeight: 400 }}>
                      Player of the Match
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default HomeScoreCard;
