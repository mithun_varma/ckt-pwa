import React, { Component } from "react";
import NormalCard from "./commonComponent/NormalCard";

class UIComponents extends Component {
  render() {
    return (
      <div
        style={{
          padding: "12px 0"
        }}
      >
        {/* points table section */}
        {/* <PointsTable /> */}

        {/* Progress bar component */}
        {/* <NormalCard title={"Who's winning ?"} type={"polling"} /> */}
        {/* Progress bar component  closing */}

        {/* Quick Bytes component */}
        <NormalCard
          title={"Quick Bytes"}
          type="text"
          content="Ross Taylor is a better batsman than Virat Kohli Ross Taylor is a
          better batsman than Virat Kohli Ross Taylor is a better batsman than
          Virat Kohli Ross Taylor is a better batsman than Virat Kohli"
        />
        {/* Quick Bytes closing */}
      </div>
    );
  }
}

export default UIComponents;
