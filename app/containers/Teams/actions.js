/*
 *
 * Teams actions
 *
 */
import { showToast } from 'utils/misc';
import {
  FETCH_TEAM_LIST,
  FETCH_TEAM_LIST_SUCCESS,
  FETCH_TEAM_LIST_FAILURE,
  FETCH_TEAM_DETAILS,
  FETCH_TEAM_DETAILS_SUCCESS,
  FETCH_TEAM_DETAILS_FAILURE,
  FETCH_TEAM_SCHEDULE,
  FETCH_TEAM_SCHEDULE_SUCCESS,
  FETCH_TEAM_SCHEDULE_FAILURE,
  FETCH_TEAM_RESULTS,
  FETCH_TEAM_RESULTS_SUCCESS,
  FETCH_TEAM_RESULTS_FAILURE,
  FETCH_TEAM_SQUADS,
  FETCH_TEAM_SQUADS_SUCCESS,
  FETCH_TEAM_SQUADS_FAILURE,
  FETCH_TEAM_NEWS,
  FETCH_TEAM_NEWS_SUCCESS,
  FETCH_TEAM_NEWS_FAILURE,
  //
  FETCH_MORE_TEAM_LIST_BY_CATEGORY,
  FETCH_MORE_TEAM_LIST_BY_CATEGORY_SUCCESS,
  FETCH_MORE_TEAM_LIST_BY_CATEGORY_FAILURE,

  //
  FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS,
  FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS_SUCCESS,
  FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS_FAILURE,

  //
  FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC,
  FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC_FAILURE,
  FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC_SUCCESS,
} from './constants';

const ACTIVE_TOAST = {};

//adding Magic moments
export const fetchTeamMagicMoment = payload =>({
  type:FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS,
  payload
})
export const fetchTeamMagicMomentSuccess = payload =>({
  type:FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS_SUCCESS,
  payload
})
export const fetchTeamMagicMomentFailure = payload =>({
  type:FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS_FAILURE,
  payload
})
//end of magioc moments
//adding NOSTALGIC moments
export const fetchTeamNostalgicMoment = payload =>({
  type:FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC,
  payload
})
export const fetchTeamNostalgicMomentSuccess = payload =>({
  type:FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC_SUCCESS,
  payload
})
export const fetchTeamNostalgicMomentFailure = payload =>({
  type:FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC_FAILURE,
  payload
})
//end of NOSTALGIC moments

// adding now
export const fetchMoreTeamListByCategory = payload => ({
  type: FETCH_MORE_TEAM_LIST_BY_CATEGORY,
  payload
})

export const fetchMoreTeamListByCategorySuccess = payload => ({
  type: FETCH_MORE_TEAM_LIST_BY_CATEGORY_SUCCESS,
  payload
})

export const fetchMoreTeamListByCategoryFailure = payload => ({
  type: FETCH_MORE_TEAM_LIST_BY_CATEGORY_FAILURE,
  payload
})

// Team List actions
export const fetchTeamList = payload => ({
  type: FETCH_TEAM_LIST,
  payload,
});

export const fetchTeamListSuccess = payload => ({
  type: FETCH_TEAM_LIST_SUCCESS,
  payload,
});

export const fetchTeamListFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchTeamList', 'Unable to fetch team list');
  return {
    type: FETCH_TEAM_LIST_FAILURE,
    payload: err,
  };
};

// Team Details actions
export const fetchTeamDetails = payload => ({
  type: FETCH_TEAM_DETAILS,
  payload,
});

export const fetchTeamDetailsSuccess = payload => ({
  type: FETCH_TEAM_DETAILS_SUCCESS,
  payload,
});

export const fetchTeamDetailsFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchTeamDetails', 'Unable to fetch team details');
  return {
    type: FETCH_TEAM_DETAILS_FAILURE,
    payload: err,
  };
};

// Team Schedule actions
export const fetchTeamSchedule = payload => ({
  type: FETCH_TEAM_SCHEDULE,
  payload,
});

export const fetchTeamScheduleSuccess = payload => ({
  type: FETCH_TEAM_SCHEDULE_SUCCESS,
  payload,
});

export const fetchTeamScheduleFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchTeamSchedule', 'Unable to fetch team schedule');
  return {
    type: FETCH_TEAM_SCHEDULE_FAILURE,
    payload: err,
  };
};

// Team Results actions
export const fetchTeamResults = payload => ({
  type: FETCH_TEAM_RESULTS,
  payload,
});

export const fetchTeamResultsSuccess = payload => ({
  type: FETCH_TEAM_RESULTS_SUCCESS,
  payload,
});

export const fetchTeamResultsFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchTeamResults', 'Unable to fetch team result');
  return {
    type: FETCH_TEAM_RESULTS_FAILURE,
    payload: err,
  };
};

// Team Squads actions
export const fetchTeamSquads = payload => ({
  type: FETCH_TEAM_SQUADS,
  payload,
});

export const fetchTeamSquadsSuccess = payload => ({
  type: FETCH_TEAM_SQUADS_SUCCESS,
  payload,
});

export const fetchTeamSquadsFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchTeamSquads', 'Unable to fetch team squads');
  return {
    type: FETCH_TEAM_SQUADS_FAILURE,
    payload: err,
  };
};

// Team News actions
export const fetchTeamNews = payload => ({
  type: FETCH_TEAM_NEWS,
  payload,
});

export const fetchTeamNewsSuccess = payload => ({
  type: FETCH_TEAM_NEWS_SUCCESS,
  payload,
});

export const fetchTeamNewsFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchTeamNews', 'Unable to fetch team news');
  return {
    type: FETCH_TEAM_NEWS_FAILURE,
    payload: err,
  };
};
