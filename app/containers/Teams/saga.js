/* eslint no-param-reassign: 0 */
import { call, put, takeLatest } from 'redux-saga/effects';

import {
  API_TEAMS_LIST,
  API_TEAM_SCHEDULE,
  API_TEAM_RESULTS,
  API_TEAM_SQUADS,
  API_TEAM_ARTICLES,
  API_TEAM_DETAILS,
  API_GET_MORE_TEAM_LIST_BY_CATEGORY,
  API_GET_TEAM_MAGIC_MOMENT,
  API_GET_TEAM_NOSTALGIC_MOMENT,
} from 'utils/requestUrls';
import request from 'utils/request';

import * as teamConstants from './constants';
import * as teamActions from './actions';

export function* getTeamMagicMoments(payload){
  const url = API_GET_TEAM_MAGIC_MOMENT(payload.payload.teamId);
  const config ={
    ...url,
    params:{
      ...url.params,
      categoryId: payload.payload.teamId,
    },
  };
  try{
    const options = yield call(request,config);
    yield put(teamActions.fetchTeamMagicMomentSuccess(options.data.data));

  }
  catch(err){
    yield put(teamActions.fetchTeamMagicMomentFailure(options.data.data));

  }

}
export function* getTeamNostalgicMoments(payload){
  const url = API_GET_TEAM_NOSTALGIC_MOMENT(payload.payload.teamId);
  const config ={
    ...url,
    params:{
      ...url.params,
      categoryId: payload.payload.teamId,
    },
  };
  try{
    const options = yield call(request,config);
    yield put(teamActions.fetchTeamNostalgicMomentSuccess(options.data.data));

  }
  catch(err){
    yield put(teamActions.fetchTeamNostalgicMomentFailure(options.data.data));

  }

}

export function* getTeamList(payload) {
  const config = {
    ...API_TEAMS_LIST,
    params: {
      ...API_TEAMS_LIST.params,
      ...payload.payload,
    },
  };
  try {
    // const options = yield call(request, API_TEAMS_LIST);
    const options = yield call(request, config);
    yield put(teamActions.fetchTeamListSuccess(options.data.data));
  } catch (err) {
    yield put(teamActions.fetchTeamListFailure(err));
  }
}

export function* getTeamDetails(payload) {
  const requestUrl = API_TEAM_DETAILS(payload.payload.teamId);
  delete payload.payload.teamId;
  const config = { ...requestUrl, params: { ...requestUrl.params, ...payload.payload } };
  try {
    const options = yield call(request, config);
    yield put(teamActions.fetchTeamDetailsSuccess(options.data.data));
  } catch (err) {
    yield put(teamActions.fetchTeamDetailsFailure(err));
  }
}

export function* getTeamSchedule(payload) {
  const requestUrl = API_TEAM_SCHEDULE(payload.payload.teamId);
  delete payload.payload.teamId;
  const config = {
    ...requestUrl,
    params: {
      ...requestUrl.params,
      ...payload.payload,
    },
  };
  try {
    const options = yield call(request, config);
    yield put(teamActions.fetchTeamScheduleSuccess(options.data.data));
  } catch (err) {
    yield put(teamActions.fetchTeamScheduleFailure(err));
  }
}

export function* getTeamResults(payload) {
  const requestUrl = API_TEAM_RESULTS(payload.payload.teamId);
  delete payload.payload.teamId;
  const config = {
    ...requestUrl,
    params: {
      ...requestUrl.params,
      ...payload.payload,
    },
  };
  try {
    const options = yield call(request, config);
    yield put(teamActions.fetchTeamResultsSuccess(options.data.data));
  } catch (err) {
    yield put(teamActions.fetchTeamResultsFailure(err));
  }
}

export function* getTeamSquads(payload) {
  try {
    const options = yield call(request, API_TEAM_SQUADS(payload.payload.teamId));
    yield put(
      teamActions.fetchTeamSquadsSuccess(
        options.data.data[0].additionalInfo ? options.data.data[0].additionalInfo.players : [],
      ),
    );
  } catch (err) {
    yield put(teamActions.fetchTeamSquadsFailure(err));
  }
}

export function* getTeamNews(payload) {
  const url = API_TEAM_ARTICLES(payload.payload.teamId);
  const config = {
    ...url,
    params: {
      ...url.params,
      ...payload.payload,
    },
  };
  try {
    const options = yield call(request, config);
    yield put(
      teamActions.fetchTeamNewsSuccess({
        news: options.data.data,
        host: options.data.host,
        skip: payload.payload.skip,
      }),
    );
  } catch (err) {
    yield put(teamActions.fetchTeamNewsFailure(err));
  }
}
export function* getMoreTeamListByCategory(payload) {
  const config = {
    ...API_GET_MORE_TEAM_LIST_BY_CATEGORY,
    params: {
      ...API_GET_MORE_TEAM_LIST_BY_CATEGORY.params,
      ...payload.payload
    }
  };

  try {
    const options = yield call(request, config)
    yield put(teamActions.fetchMoreTeamListByCategorySuccess(options.data.data));
  }
  catch (error) {
    yield put(teamActions.fetchMoreTeamListByCategoryFailure(error));
  }
}

export default function* defaultSaga() {
  yield takeLatest(teamConstants.FETCH_TEAM_LIST, getTeamList);
  yield takeLatest(teamConstants.FETCH_TEAM_SCHEDULE, getTeamSchedule);
  yield takeLatest(teamConstants.FETCH_TEAM_RESULTS, getTeamResults);
  yield takeLatest(teamConstants.FETCH_TEAM_SQUADS, getTeamSquads);
  yield takeLatest(teamConstants.FETCH_TEAM_NEWS, getTeamNews);
  yield takeLatest(teamConstants.FETCH_TEAM_DETAILS, getTeamDetails);
  yield takeLatest(teamConstants.FETCH_MORE_TEAM_LIST_BY_CATEGORY, getMoreTeamListByCategory);
  yield takeLatest(teamConstants.FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS,getTeamMagicMoments);
  yield takeLatest(teamConstants.FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC,getTeamNostalgicMoments);
}
