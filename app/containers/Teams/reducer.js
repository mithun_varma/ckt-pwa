/*
 *
 * Teams reducer
 *
 */
/* eslint no-fallthrough: 0 */
/* eslint no-else-return: 0 */
/* eslint no-underscore-dangle: 0 */

// import partition from 'lodash/partition';
import {
  FETCH_TEAM_LIST,
  FETCH_TEAM_LIST_SUCCESS,
  FETCH_TEAM_LIST_FAILURE,
  FETCH_TEAM_DETAILS,
  FETCH_TEAM_DETAILS_SUCCESS,
  FETCH_TEAM_DETAILS_FAILURE,
  FETCH_TEAM_SCHEDULE,
  FETCH_TEAM_SCHEDULE_SUCCESS,
  FETCH_TEAM_SCHEDULE_FAILURE,
  FETCH_TEAM_RESULTS,
  FETCH_TEAM_RESULTS_SUCCESS,
  FETCH_TEAM_RESULTS_FAILURE,
  FETCH_TEAM_SQUADS,
  FETCH_TEAM_SQUADS_SUCCESS,
  FETCH_TEAM_SQUADS_FAILURE,
  FETCH_TEAM_NEWS,
  FETCH_TEAM_NEWS_SUCCESS,
  FETCH_TEAM_NEWS_FAILURE,
  FETCH_MORE_TEAM_LIST,
  FETCH_MORE_TEAM_LIST_BY_CATEGORY_SUCCESS,
  FETCH_MORE_TEAM_LIST_BY_CATEGORY_FAILURE,
  FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS,
  FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS_SUCCESS,
  FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS_FAILURE,
  FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC,
  FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC_FAILURE,
  FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC_SUCCESS
} from "./constants";

export const initialState = {
  teamListLoading: false,
  teamList: [],
  teamDetailsLoading: false,
  teamDetails: {},
  teamScheduleLoading: false,
  teamSchedule: {},
  teamResultsLoading: false,
  teamResults: {},
  teamSquadsLoading: false,
  teamSquads: [],
  teamNewsLoading: false,
  teamNews: [],
  teamNewsOldLength: 0,
  teamNewsFeatured: [],
  articleHost: "",
  //
  teamMagicMoments: [],
  teamMagicMomentsLoading: false,
  //
  teamNostalgicMoments: [],
  teamNostalgicMomentsLoading: false
};

function teamsReducer(state = initialState, action) {
  switch (action.type) {
    // starting the magic moments
    case FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS:
      return {
        ...state,
        teamMagicMomentsLoading: true
      };
    case FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS_SUCCESS:

      const teamMagicMoments = state.teamMagicMoments;
      return {
        ...state,
        teamMagicMoments: [action.payload]
      };
    case FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS_FAILURE: {
      return {
        ...state,
        teamMagicMomentsLoading: false
      };
    }
    //ending the moments
    // Team List Case
    case FETCH_TEAM_LIST:
      return {
        ...state,
        teamListLoading: true
      };
    case FETCH_TEAM_LIST_SUCCESS:
      return {
        ...state,
        teamListLoading: false,
        teamList: action.payload
      };
    case FETCH_TEAM_LIST_FAILURE:
      return {
        ...state,
        teamListLoading: false
      };
    // Team Details Case
    case FETCH_TEAM_DETAILS:
      return {
        ...state,
        teamDetailsLoading: true
        // teamDetails: {},
      };
    case FETCH_TEAM_DETAILS_SUCCESS:
      return {
        ...state,
        teamDetailsLoading: false,
        // teamDetails: action.payload,
        teamDetails: {
          ...state.teamDetails,
          [action.payload.crictecTeamId]: action.payload
        }
      };
    case FETCH_TEAM_DETAILS_FAILURE:
      return {
        ...state,
        teamDetailsLoading: false
      };
    // Team Schedule Case
    case FETCH_TEAM_SCHEDULE:
      return {
        ...state,
        teamScheduleLoading: true
      };
    case FETCH_TEAM_SCHEDULE_SUCCESS:
      return {
        ...state,
        teamScheduleLoading: false,
        teamSchedule: action.payload
      };
    case FETCH_TEAM_SCHEDULE_FAILURE:
      return {
        ...state,
        teamScheduleLoading: false
      };
    // Team Results Case
    case FETCH_TEAM_RESULTS:
      return {
        ...state,
        teamResultsLoading: true
      };
    case FETCH_TEAM_RESULTS_SUCCESS:
      return {
        ...state,
        teamResultsLoading: false,
        teamResults: action.payload
      };
    case FETCH_TEAM_RESULTS_FAILURE:
      return {
        ...state,
        teamResultsLoading: false
      };
    // Team Squads Case
    case FETCH_TEAM_SQUADS:
      return {
        ...state,
        teamSquadsLoading: true
      };
    case FETCH_TEAM_SQUADS_SUCCESS:
      return {
        ...state,
        teamSquadsLoading: false,
        teamSquads: action.payload
      };
    case FETCH_TEAM_SQUADS_FAILURE:
      return {
        ...state,
        teamSquadsLoading: false
      };
    // Team News Case
    case FETCH_TEAM_NEWS:
      return {
        ...state,
        teamNewsLoading: true
      };
    case FETCH_TEAM_NEWS_SUCCESS:
      if (!action.payload.skip) {
        // const newsGrouped = partition(action.payload.news, d => d.filters.featured);
        // const [teamNewsFeatured, teamNews] = newsGrouped;
        return {
          ...state,
          teamNewsLoading: false,
          teamNews: action.payload.news,
          // teamNewsFeatured,
          articleHost: action.payload.host,
          teamNewsOldLength: 0
        };
      } else {
        const teamNews = [...state.teamNews];
        return {
          ...state,
          teamNewsLoading: false,
          teamNews: action.payload.news.length
            ? teamNews.concat(action.payload.news)
            : teamNews,
          articleHost: action.payload.host,
          teamNewsOldLength: teamNews
        };
      }
    case FETCH_TEAM_NEWS_FAILURE:
      return {
        ...state,
        teamNewsLoading: false
      };

    case FETCH_MORE_TEAM_LIST_BY_CATEGORY_SUCCESS: {
      let teamList = state.teamList;
      let teamsListByCategory;
      let newTeamsListByCatetory;
      let index = 0;
      for (index = 0; index < teamList.length; index++) {
        teamsListByCategory = teamList[index];
        if (teamsListByCategory.type === action.payload.type) {
          newTeamsListByCatetory = Object.assign({}, teamsListByCategory, {
            teams: [...teamsListByCategory.teams, ...action.payload.teams]
          });
          break;
        }
      }
      teamList.splice(index, 1);
      teamList.splice(index, 0, newTeamsListByCatetory);
      return {
        ...state,
        teamList: [...teamList]
      };
    }

    case FETCH_MORE_TEAM_LIST_BY_CATEGORY_FAILURE: {
      return {
        ...state
      };
    }

    case FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC:
      return {
        ...state,
        teamNostalgicMomentsLoading: true
      };
    case FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC_SUCCESS:
      const teamNostalgicMoments = state.teamNostalgicMoments;
      return {
        ...state,
        teamNostalgicMoments: [action.payload]
      };
    case FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC_FAILURE: {
      return {
        ...state,
        teamNostalgicMomentsLoading: false
      };
    }
    //ending the NOstalgic
    default:
      return state;
  }
}

export default teamsReducer;
