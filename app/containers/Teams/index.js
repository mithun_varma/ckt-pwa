import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import { Helmet } from "react-helmet";
import makeSelectTeams from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import ImageContainer from "./../../components-v2/commonComponent/imageContainerTeams";
import * as teamActions from "./actions";
import {
  screenWidth,
  screenHeight
} from "../../styleSheet/screenSize/ScreenDetails";
import Header from "./../../components-v2/commonComponent/Header";
import {
  gradientRedOrangeLeft,
  white
} from "./../../styleSheet/globalStyle/color";
import ReactGA from "react-ga";

/* eslint-disable react/prefer-stateless-function */
export class Teams extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "international",
      title: "",
      isHeaderBackground: true,
      titlePlaced: false
    };
  }
  componentDidMount() {
    // ReactGA.initialize("UA-136526387-1");
    // ReactGA.pageview(
    //   "https://www.cricket.com" +
    //     window.location.pathname +
    //     window.location.search
    // );
    this.props.fetchTeamList({});
  }

  componentWillUnmount() {}
  loadMoreTeamsList = type => {
    let allTeamsList = this.props.teams.teamList;
    let teamsListByCategory;
    for (let index = 0; index < allTeamsList.length; index++) {
      teamsListByCategory = allTeamsList[index];
      if (teamsListByCategory.type === type) {
        this.props.fetchMoreTeamListByCategory({
          type: type,
          skip: teamsListByCategory.teams.length,
          limit: 5
        });
        break;
      }
    }
  };

  teamDiscovery(teamList) {
    if (teamList != null) {
      return (
        <div style={pageStyle.conatiner}>
          <div
            style={{
              fontFamily: "Montserrat",
              fontWeight: 700,
              fontSize: 22,
              color: white,
              marginBottom: 12,
              minHeight: "90px",
              paddingLeft: "16px",
              textTransform: "capitalize",
              background: gradientRedOrangeLeft
            }}
          >
            {null}
          </div>
          <Header
            title={"Teams"}
            isHeaderBackground={this.state.isHeaderBackground}
            leftArrowBack
            textColor={white}
            backgroundColor={gradientRedOrangeLeft}
            history={this.props.history}
            leftIconOnClick={() => {
              this.props.history.goBack();
            }}
          />
          <div style={{ marginTop: -screenHeight * 0.11 }}>
            {teamList.map((team, i) => {
              return (
                <div style={pageStyle.other} id={team.type}>
                  {team.type === "Popular Teams" ? (
                    <ImageContainer
                      isAvatar
                      history={this.props.history}
                      data={team.teams}
                      rootStyles={{ marginTop: "12px", overflow: "scroll" }}
                      title={team.type}
                      getMoreTeamDetails={this.loadMoreTeamsList}
                    />
                  ) : i % 2 == 0 ? (
                    <ImageContainer
                      isBlack
                      history={this.props.history}
                      data={team.teams}
                      rootStyles={{ marginTop: "12px", overflow: "scroll" }}
                      title={team.type}
                      getMoreTeamDetails={this.loadMoreTeamsList}
                    />
                  ) : (
                    <ImageContainer
                      history={this.props.history}
                      data={team.teams}
                      rootStyles={{ marginTop: "12px", overflow: "scroll" }}
                      title={team.type}
                      getMoreTeamDetails={this.loadMoreTeamsList}
                    />
                  )}
                </div>
              );
            })}
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
  render() {
    const { teams, history } = this.props;
    const teamList = teams.teamList;
    const teamListLoading = teams.teamListLoading;
    if (teamList.length > 1 || !teamListLoading) {
      return (
        <div style={pageStyle.teamContainer}>
          <Helmet titleTemplate="%s | cricket.com">
            <title>
              Cricket Teams | IPL Teams | International and Domestic Cricket
              Teams
            </title>
            <meta
              name="description"
              content="List of international and domestic cricket team profiles. Find out the complete cricket news of your favourte cricket teams on cricket.com."
            />
            <meta
              name="keywords"
              content="best cricket teams, cricket teams, ipl teams, test teams, domestic cricket teams"
            />
            <link
              rel="canonical"
              href={`www.cricket.com${history &&
                history.location &&
                history.location.pathname}`}
            />
          </Helmet>
          <div style={pageStyle.teamDiscovery}>
            {this.teamDiscovery(teamList)}
          </div>
        </div>
      );
    } else {
      return null;
    }
  }

  handleActiveTab = activeTab => {
    this.setState(
      {
        activeTab
      },
      () => {
        this.props.fetchTeamList({ tags: this.state.activeTab.split() });
      }
    );
  };

  handleDetailRedirection = (e, id) => {
    e.preventDefault();
    this.props.history.push(`/teams/${id}`);
  };
}

const pageStyle = {
  teamContainer: {
    // display: "flex",
    // width: screenWidth * 1.0
  },
  teamDiscovery: {
    // width: screenWidth * 1.0
  },
  teamDetails: {
    display: "flex",
    width: screenWidth * 1.0
  },
  conatiner: {
    display: "flex",
    flexDirection: "column",
    padding: "40px 0",
    minHeight: "120vh"
  },
  TopHeader: {
    display: "flex"
  },
  avatar: {
    display: "flex",
    flexDirection: "row"
  },
  other: {
    // display: "flex",
    // flex: 1,
    // width: screenWidth * 0.96
  }
};

Teams.propTypes = {
  fetchTeamList: PropTypes.func.isRequired,
  teams: PropTypes.object,
  history: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  teams: makeSelectTeams()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchTeamList: payload => dispatch(teamActions.fetchTeamList(payload)),
    fetchMoreTeamListByCategory: payload =>
      dispatch(teamActions.fetchMoreTeamListByCategory(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "teams", reducer });
const withSaga = injectSaga({ key: "teams", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(Teams);
