/*
 *
 * Teams constants
 *
 */
export const FETCH_TEAM_LIST = 'app/Teams/FETCH_TEAM_LIST';
export const FETCH_TEAM_LIST_SUCCESS = 'app/Teams/FETCH_TEAM_LIST_SUCCESS';
export const FETCH_TEAM_LIST_FAILURE = 'app/Teams/FETCH_TEAM_LIST_FAILURE';

export const FETCH_TEAM_DETAILS = 'app/Teams/FETCH_TEAM_DETAILS';
export const FETCH_TEAM_DETAILS_SUCCESS = 'app/Teams/FETCH_TEAM_DETAILS_SUCCESS';
export const FETCH_TEAM_DETAILS_FAILURE = 'app/Teams/FETCH_TEAM_DETAILS_FAILURE';

export const FETCH_TEAM_SCHEDULE = 'app/Teams/FETCH_TEAM_SCHEDULE';
export const FETCH_TEAM_SCHEDULE_SUCCESS = 'app/Teams/FETCH_TEAM_SCHEDULE_SUCCESS';
export const FETCH_TEAM_SCHEDULE_FAILURE = 'app/Teams/FETCH_TEAM_SCHEDULE_FAILURE';

export const FETCH_TEAM_RESULTS = 'app/Teams/FETCH_TEAM_RESULTS';
export const FETCH_TEAM_RESULTS_SUCCESS = 'app/Teams/FETCH_TEAM_RESULTS_SUCCESS';
export const FETCH_TEAM_RESULTS_FAILURE = 'app/Teams/FETCH_TEAM_RESULTS_FAILURE';

export const FETCH_TEAM_SQUADS = 'app/Teams/FETCH_TEAM_SQUADS';
export const FETCH_TEAM_SQUADS_SUCCESS = 'app/Teams/FETCH_TEAM_SQUADS_SUCCESS';
export const FETCH_TEAM_SQUADS_FAILURE = 'app/Teams/FETCH_TEAM_SQUADS_FAILURE';

export const FETCH_TEAM_NEWS = 'app/Teams/FETCH_TEAM_NEWS';
export const FETCH_TEAM_NEWS_SUCCESS = 'app/Teams/FETCH_TEAM_NEWS_SUCCESS';
export const FETCH_TEAM_NEWS_FAILURE = 'app/Teams/FETCH_TEAM_NEWS_FAILURE';


export const FETCH_MORE_TEAM_LIST_BY_CATEGORY = "app/Teams/FETCH_MORE_TEAM_LIST_BY_CATEGORY";
export const FETCH_MORE_TEAM_LIST_BY_CATEGORY_SUCCESS = "app/Teams/FETCH_MORE_TEAM_LIST_BY_CATEGORY_SUCCESS";
export const FETCH_MORE_TEAM_LIST_BY_CATEGORY_FAILURE = "app/Teams/FETCH_MORE_TEAM_LIST_BY_CATEGORY_FAILURE";

export const FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS ="app/Teams/FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS";
export const FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS_SUCCESS ="app/Teams/FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS_SUCCESS";
export const FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS_FAILURE ="app/Teams/FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_MOMENTS_FAILURE";


//nostalgic
export const FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC = "app/Teams/FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC";
export const FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC_SUCCESS = "app/Teams/FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC_SUCCESS";
export const FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC_FAILURE = "app/Teams/FETCH_TEAM_IMAGE_LIST_BY_CATEGORY_NOSTALGIC_FAILURE";
