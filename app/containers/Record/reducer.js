/*
 *
 * Record reducer
 *
 */

// import { fromJS } from "immutable";
import * as recordConstants from "./constants";

export const initialState = {
  pointsTableLoading: false,
  pointsTable: [],
  recordsLoading: false,
  records: {},
  homePoll: {},
  pollResult: {},
  isPollResult: false,
  pollLoading: false,
  postPollLoading: false,
  cardIndex: ""
};

function recordReducer(state = initialState, action) {
  switch (action.type) {
    case recordConstants.SAVE_CARD_POSITION:
      return {
        ...state,
        cardIndex: action.payload
      };
    case recordConstants.POINTS_TABLE:
      return {
        ...state,
        pointsTableLoading: true
      };
    case recordConstants.POINTS_TABLE_SUCCESS:
      return {
        ...state,
        pointsTableLoading: false,
        pointsTable: action.payload.data || []
      };
    case recordConstants.POINTS_TABLE_FAILURE:
      return {
        ...state,
        pointsTableLoading: false
      };

    // RECORDS case
    case recordConstants.RECORDS:
      return {
        ...state,
        recordsLoading: true
      };
    case recordConstants.RECORDS_SUCCESS:
      return {
        ...state,
        recordsLoading: false,
        records: action.payload.data ? action.payload.data : {}
      };
    case recordConstants.RECORDS_FAILURE:
      return {
        ...state,
        recordsLoading: false
      };
    case recordConstants.GET_HOME_FAN_POLL:
      return {
        ...state,
        pollLoading: true
      };
    case recordConstants.GET_HOME_FAN_POLL_SUCCESS:
      return {
        ...state,
        pollLoading: false,
        homePoll: action.payload.data ? action.payload.data : {}
      };
    case recordConstants.GET_HOME_FAN_POLL_FAILURE:
      return {
        ...state,
        pollLoading: false
      };
    case recordConstants.POST_FAN_POLL:
      return {
        ...state,
        isPollResult: false,
        postPollLoading: true
      };
    case recordConstants.POST_FAN_POLL_SUCCESS:
      return {
        ...state,
        postPollLoading: false,
        isPollResult: true,
        pollResult: action.payload.data ? action.payload.data : {}
      };
    case recordConstants.POST_FAN_POLL_FAILURE:
      return {
        ...state,
        isPollResult: false,
        postPollLoading: false
      };
    case recordConstants.FAN_POLL_RESET:
      return {
        ...state,
        postPollLoading: false,
        isPollResult: false
      };
    case recordConstants.GET_POLL_BY_ID:
      return {
        ...state,
        postPollLoading: true
      };
    case recordConstants.GET_POLL_BY_ID_SUCCESS:
      return {
        ...state,
        postPollLoading: false,
        isPollResult: true,
        pollResult: action.payload.data ? action.payload.data : {}
      };
    case recordConstants.GET_POLL_BY_ID_FAILURE:
      return {
        ...state,
        postPollLoading: false
      };
    default:
      return state;
  }
}

export default recordReducer;
