/**
 *
 * Record
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import map from "lodash/map";

import {
  grey_10,
  white,
  grey_4,
  black
} from "../../styleSheet/globalStyle/color";
import TabBar from "components-v2/commonComponent/TabBar";
import ListItems from "components-v2/commonComponent/ListItems";
import ButtonBar from "components-v2/commonComponent/ButtonBar";
import RecordDetails from "components-v2/Records/RecordDetails";
import Header from "components-v2/commonComponent/Header";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectRecord from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import * as recordActions from "./actions";
import ReactGA from "react-ga";
import { Helmet } from 'react-helmet';

/* eslint-disable react/prefer-stateless-function */

const FORMAT_DATA = ["batting", "bowling"];
const MATCH_TYPE = ["test", "odi", "t20"];
let scrollTop = null;

let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  // ClevertapReact.initialize("TEST-Z88-4KR-845Z"); // Old Key
  ClevertapReact.initialize("W88-4KR-845Z");
  // initializeFirebase();
  // askForPermissioToReceiveNotifications();
}

export class Record extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeButton: "batting",
      activeTab: "test",
      title: "",
      isHeaderBackground: false
    };
  }

  componentDidMount() {
    // ReactGA.initialize("UA-136526387-1");
    // ReactGA.pageview(
    //   "https://www.cricket.com" +
    //     window.location.pathname +
    //     window.location.search
    // );
    // this.props.fetchPointsTable();
    window.addEventListener("scroll", this.handleScroll);
    if (this.props.match.params.recordType) {
      this.props.fetchRecords({
        format: this.state.activeTab,
        recordType:
          recordList[this.state.activeButton][
            this.props.match.params.recordType
          ].key,
        playingType: this.state.activeButton
      });
    }
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = event => {
    ({ scrollTop } = event.target.scrollingElement);
    // console.log("object: ", scrollTop);
    if (scrollTop > 26) {
      this.setState({
        // title: "Records",
        title: this.props.match.params.recordType
          ? recordList[this.state.activeButton][
              this.props.match.params.recordType
            ].label
          : "Records",
        isHeaderBackground: true
      });
    } else if (scrollTop < 27) {
      this.setState({
        title: "",
        isHeaderBackground: false
      });
    }
  };

  handleActiveButton = activeButton => {
    this.setState({
      activeButton
    });
  };

  handleActiveTab = activeTab => {
    this.setState({
      activeTab
    });
  };

  handleRecords = key => {
    ClevertapReact.event("Records", {
      source: "Records"
    });
    this.props.history.push(`/records/${key}`);
    this.props.fetchRecords({
      format: this.state.activeTab,
      recordType: recordList[this.state.activeButton][key].key,
      playingType: this.state.activeButton
    });
  };

  render() {
    const { record, history } = this.props;
    const title = this.props.match.params.recordType
      ? recordList[this.state.activeButton][this.props.match.params.recordType]
          .label
      : "Records";
    // console.log("object:", this.props, title);
    return (
      <React.Fragment>
        <Helmet titleTemplate="%s | cricket.com">
          <title>Cricket Records | ODI, T20 and Test Matches</title>
          <meta
            name="description"
            content="Check out the list of all cricket records including batting, bowling, odi cricket matches, t20 cricket matches and test cricket matches at cricket.com."
          />
          <meta
            name="keywords"
            content="cricket records, odi cricket, test cricket matches, t20 cricket"
          />
          <link
            rel="canonical"
            href={`www.cricket.com${history &&
              history.location &&
              history.location.pathname}`}
          />
          {/* <link rel="apple-touch-icon" href="http://mysite.com/img/apple-touch-icon-57x57.png" /> */}
        </Helmet>
        <div
          style={{
            padding: "54px 16px 16px",
            // minHeight: "120vh"
          }}
        >
          {/* <RecordDetails /> */}
          {/* Records Title */}
          {false && (
            <div
              style={{
                fontFamily: "Montserrat",
                fontWeight: 700,
                fontSize: 22,
                color: grey_10,
                marginBottom: 12
              }}
            >
              {title}
            </div>
          )}
          <Header
            // title={this.state.title}
            title={title}
            // isHeaderBackground={this.state.isHeaderBackground}
            isHeaderBackground={true}
            leftArrowBack
            textColor={black}
            backgroundColor={grey_4}
            history={history}
            leftIconOnClick={() => {
              history.goBack();
            }}
          />

          {this.props.match.params.recordType ? (
            <RecordDetails
              keys={
                recordKeys[this.state.activeButton][
                  this.props.match.params.recordType
                ]
              }
              type={
                this.state.activeButton == "batting" ? "BATSMEN" : "BOWLERS"
              }
              data={
                record.records && record.records[this.state.activeButton]
                  ? record.records[this.state.activeButton]
                  : []
              }
              loading={record.recordsLoading}
            />
          ) : (
            // {/* Records Card Section */}
            <div
              style={{
                background: white,
                borderRadius: 3,
                paddingTop: 8
              }}
            >
              <ButtonBar
                items={FORMAT_DATA}
                activeItem={this.state.activeButton}
                onClick={this.handleActiveButton}
              />

              <TabBar
                items={MATCH_TYPE}
                activeItem={this.state.activeTab}
                onClick={this.handleActiveTab}
                valueStyles={{
                  textTransform: "uppercase"
                }}
              />
              {/* <HrLine /> */}

              {map(recordList[this.state.activeButton], (list, index) => (
                <ListItems
                  title={list.label}
                  onClick={this.handleRecords}
                  param={index}
                />
              ))}
            </div>
          )}
        </div>
      </React.Fragment>
    );
  }
}

Record.propTypes = {
  fetchPointsTable: PropTypes.func,
  fetchRecords: PropTypes.func,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  record: makeSelectRecord()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchPointsTable: payload =>
      dispatch(recordActions.fetchPointsTable(payload)),
    fetchRecords: payload => dispatch(recordActions.fetchRecords(payload)),
    fetchHomePoll: payload => dispatch(recordActions.getPollHome(payload)),
    saveCardPosition: payload =>
      dispatch(recordActions.saveCardPosition(payload)),
    postPollOption: payload => dispatch(recordActions.postPoll(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "record", reducer });
const withSaga = injectSaga({ key: "record", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(Record);

const recordList = {
  batting: {
    "Most-Runs": { label: "Most Runs", key: "runs" },
    // "Highest-Scores": { label: "Highest Scores", key: "highestScore" },
    "Best-Batting-Average": { label: "Best Batting Average", key: "average" },
    "Best-Batting-Strike-Rate": {
      label: "Best Batting Strike Rate",
      key: "strikeRate"
    },
    "Most-Hundreds": { label: "Most Hundreds", key: "hundreds" },
    "Most-Fifties": { label: "Most Fifties", key: "fifties" }
    // "Most-Fours": { label: "Most Fours", key: "fours" },
    // "Most-Sixes": { label: "Most Sixes", key: "sixes" }
  },
  bowling: {
    "Most-Wickets": { label: "Most Wickets", key: "wickets" },
    "Best-Bowling-Average": {
      label: "Best Bowling Average",
      key: "bowlingAverage"
    },
    // "Best-Bowling": { label: "Best Bowling", key: "bestBowling" },
    "Most-5-Wickets-Haul": { label: "Most 5 Wickets Haul", key: "fiveWickets" },
    "Best-Economy": { label: "Best Economy", key: "economy" },
    "Best-Bowling-Strike-Rate": {
      label: "Best Bowling Strike Rate",
      key: "bowlingStrikeRate"
    }
  }
};

const recordKeys = {
  batting: {
    "Most-Runs": [
      { label: "M", key: "matches" },
      { label: "I", key: "innings" },
      { label: "R", key: "runs" },
      { label: "AVG", key: "average" }
    ],
    "Highest-Scores": [
      { label: "M", key: "matches" },
      { label: "I", key: "innings" },
      { label: "R", key: "runs" },
      { label: "SCORE", key: "highestScore" }
    ],
    "Best-Batting-Average": [
      { label: "M", key: "matches" },
      { label: "I", key: "innings" },
      { label: "R", key: "runs" },
      { label: "AVG", key: "average" }
    ],
    "Best-Batting-Strike-Rate": [
      { label: "M", key: "matches" },
      { label: "I", key: "innings" },
      { label: "R", key: "runs" },
      { label: "SR", key: "strikeRate" }
    ],
    "Most-Hundreds": [
      { label: "M", key: "matches" },
      { label: "I", key: "innings" },
      { label: "R", key: "runs" },
      { label: "100s", key: "mostHundreds" }
    ],
    "Most-Fifties": [
      { label: "M", key: "matches" },
      { label: "I", key: "innings" },
      { label: "R", key: "runs" },
      { label: "50s", key: "mostFifties" }
    ],
    "Most-Fours": [
      { label: "M", key: "matches" },
      { label: "I", key: "innings" },
      { label: "R", key: "runs" },
      { label: "4s", key: "fours" }
    ],
    "Most-Sixes": [
      { label: "M", key: "matches" },
      { label: "I", key: "innings" },
      { label: "R", key: "runs" },
      { label: "6s", key: "sixes" }
    ]
  },
  bowling: {
    "Most-Wickets": [
      { label: "M", key: "matches" },
      { label: "O", key: "overs" },
      { label: "W", key: "wickets" },
      { label: "AVG", key: "bowlingAverage" }
    ],
    "Best-Bowling-Average": [
      { label: "M", key: "matches" },
      { label: "O", key: "overs" },
      { label: "W", key: "wickets" },
      { label: "AVG", key: "bowlingAverage" }
    ],
    // "Best-Bowling": [
    //   { label: "M", key: "matches" },
    //   { label: "O", key: "overs" },
    //   { label: "W", key: "wickets" },
    //   { label: "ECO", key: "economy" }
    // ],
    "Most-5-Wickets-Haul": [
      { label: "M", key: "matches" },
      { label: "O", key: "overs" },
      { label: "W", key: "wickets" },
      { label: "5W", key: "fiveWickets" }
    ],
    "Best-Economy": [
      { label: "M", key: "matches" },
      { label: "O", key: "overs" },
      { label: "W", key: "wickets" },
      { label: "ECO", key: "economy" }
    ],
    "Best-Bowling-Strike-Rate": [
      { label: "M", key: "matches" },
      { label: "O", key: "overs" },
      { label: "W", key: "wickets" },
      { label: "SR", key: "bowlingStrikeRate" }
    ]
  }
};
