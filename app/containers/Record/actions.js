/*
 *
 * Record actions
 *
 */
import { showToast } from "utils/misc";

import * as recordConstants from "./constants";

const ACTIVE_TOAST = {};

export const fetchPointsTable = payload => ({
  type: recordConstants.POINTS_TABLE,
  payload
});

export const fetchPointsTableSuccess = payload => ({
  type: recordConstants.POINTS_TABLE_SUCCESS,
  payload
});

export const fetchPointsTableFailure = err => {
  showToast(ACTIVE_TOAST, "fetchPointsTable", "Unable to get points table");
  return {
    type: recordConstants.POINTS_TABLE_FAILURE,
    payload: err
  };
};

export const fetchRecords = payload => ({
  type: recordConstants.RECORDS,
  payload
});

export const fetchRecordsSuccess = payload => ({
  type: recordConstants.RECORDS_SUCCESS,
  payload
});

export const fetchRecordsFailure = err => {
  showToast(ACTIVE_TOAST, "fetchRecords", "Unable to get points table");
  return {
    type: recordConstants.RECORDS_FAILURE,
    payload: err
  };
};

export const getPollHome = payload => ({
  type: recordConstants.GET_HOME_FAN_POLL,
  payload
});

export const getPollHomeSuccess = payload => ({
  type: recordConstants.GET_HOME_FAN_POLL_SUCCESS,
  payload
});

export const getPollHomeFailure = err => {
  return {
    type: recordConstants.GET_HOME_FAN_POLL_FAILURE,
    payload: err
  };
};

export const postPoll = payload => ({
  type: recordConstants.POST_FAN_POLL,
  payload
});

export const postPollSuccess = payload => ({
  type: recordConstants.POST_FAN_POLL_SUCCESS,
  payload
});

export const postPollFailure = err => {
  return {
    type: recordConstants.POST_FAN_POLL_FAILURE,
    payload: err
  };
};

export const getPollById = payload => ({
  type: recordConstants.GET_POLL_BY_MATCH_ID,
  payload
});

export const getPollByIdSuccess = payload => ({
  type: recordConstants.GET_POLL_BY_MATCH_ID_SUCCESS,
  payload
});

export const getPollByIdFailure = err => {
  return {
    type: recordConstants.GET_POLL_BY_MATCH_ID_FAILURE,
    payload: err
  };
};

export const getPollByPollId = payload => ({
  type: recordConstants.GET_POLL_BY_ID,
  payload
});

export const getPollByPollIdSuccess = payload => ({
  type: recordConstants.GET_POLL_BY_ID_SUCCESS,
  payload
});

export const saveCardPosition = payload => ({
  type: recordConstants.SAVE_CARD_POSITION,
  payload
});

export const pollReset = payload => ({
  type: recordConstants.FAN_POLL_RESET,
  payload
});

export const getPollByPollIdFailure = err => {
  return {
    type: recordConstants.GET_POLL_BY_ID_FAILURE,
    payload: err
  };
};
