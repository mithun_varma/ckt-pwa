import { call, put, takeLatest } from "redux-saga/effects";
import Cookies from "universal-cookie";

const cookies = new Cookies();
import {
  API_GET_POINTS_TABLE,
  API_GET_RECORDS,
  API_GET_POLL_HOME,
  API_GET_POLL_BY_MATCH_ID,
  API_GET_POLL_BY_POLL_ID,
  API_POST_POLL
} from "utils/requestUrls";

import request from "utils/request";

import * as recordConstants from "./constants";
import * as recordActions from "./actions";

export function* getPointsTable(payload) {
  const config = {
    ...API_GET_POINTS_TABLE,
    params: {
      ...payload.payload
    }
  };
  try {
    const options = yield call(request, config);
    yield put(recordActions.fetchPointsTableSuccess(options.data));
  } catch (err) {
    yield put(recordActions.fetchPointsTableFailure(err));
  }
}

export function* getRecords(payload) {
  const config = {
    ...API_GET_RECORDS,
    params: {
      ...API_GET_RECORDS.params,
      ...payload.payload
    }
  };

  try {
    const options = yield call(request, config);
    yield put(recordActions.fetchRecordsSuccess(options.data));
  } catch (err) {
    yield put(recordActions.fetchRecordsFailure(err));
  }
}

export function* postFanPoll(payload) {
  const config = {
    ...API_POST_POLL,
    params: {
      ...API_POST_POLL.params,
      ...payload.payload.param
    }
  };
  try {
    const options = yield call(request, config);
    if (payload.payload.type == "homescreen") {
      cookies.set("resultIds", payload.payload.param.id);
      cookies.set("selectedOption", payload.payload.param.option);
    }
    if (payload.payload.type == "scorecard") {
      const scoreResultIds = cookies.get("scoreResultIds");
      const scoreSelectedOptions = cookies.get("scoreSelectedOptions");
      cookies.set(
        "scoreResultIds",
        scoreResultIds !== undefined
          ? [...scoreResultIds, payload.payload.param.id]
          : [payload.payload.param.id]
      );
      cookies.set(
        "scoreSelectedOptions",
        scoreSelectedOptions !== undefined
          ? {
              ...scoreSelectedOptions,
              [`${payload.payload.param.id}`]: payload.payload.param.option
            }
          : { [`${payload.payload.param.id}`]: payload.payload.param.option }
      );
    }
    yield put(recordActions.postPollSuccess(options.data));
  } catch (err) {
    yield put(recordActions.postPollFailure(err));
  }
}

export function* getPollByPollId(payload) {
  const config = {
    ...API_GET_POLL_BY_POLL_ID,
    params: {
      ...API_GET_POLL_BY_POLL_ID.params,
      ...payload.payload
    }
  };

  try {
    const options = yield call(request, config);
    yield put(recordActions.getPollByPollIdSuccess(options.data));
    // const resultIds = cookies.get("resultIds");
    // cookies.set(
    //   "resultIds",
    //   resultIds !== undefined
    //     ? [...resultIds, payload.payload.id]
    //     : [payload.payload.id]
    // );
  } catch (err) {
    yield put(recordActions.getPollByPollIdFailure(err));
  }
}

export function* getHomePoll() {
  try {
    const options = yield call(request, API_GET_POLL_HOME);
    yield put(recordActions.getPollHomeSuccess(options.data));
  } catch (err) {
    yield put(recordActions.getPollHomeFailure(err));
  }
}

export default function* defaultSaga() {
  yield takeLatest(recordConstants.POINTS_TABLE, getPointsTable);
  yield takeLatest(recordConstants.RECORDS, getRecords);
  yield takeLatest(recordConstants.GET_HOME_FAN_POLL, getHomePoll);
  yield takeLatest(recordConstants.POST_FAN_POLL, postFanPoll);
  yield takeLatest(recordConstants.GET_POLL_BY_ID, getPollByPollId);
}
