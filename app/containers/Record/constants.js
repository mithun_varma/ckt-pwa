/*
 *
 * Record constants
 *
 */

export const POINTS_TABLE = "app/Record/POINTS_TABLE";
export const POINTS_TABLE_SUCCESS = "app/Record/POINTS_TABLE_SUCCESS";
export const POINTS_TABLE_FAILURE = "app/Record/POINTS_TABLE_FAILURE";

export const RECORDS = "app/Record/RECORDS";
export const RECORDS_SUCCESS = "app/Record/RECORDS_SUCCESS";
export const RECORDS_FAILURE = "app/Record/RECORDS_FAILURE";

export const GET_HOME_FAN_POLL = "app/Record/GET_HOME_FAN_POLL";
export const FAN_POLL_RESET = "app/Record/FAN_POLL_RESET";
export const GET_HOME_FAN_POLL_SUCCESS = "app/Record/GET_HOME_FAN_POLL_SUCCESS";
export const GET_HOME_FAN_POLL_FAILURE = "app/Record/GET_HOME_FAN_POLL_FAILURE";

export const POST_FAN_POLL = "app/Record/POST_FAN_POLL";
export const POST_FAN_POLL_SUCCESS = "app/Record/POST_FAN_POLL_SUCCESS";
export const POST_FAN_POLL_FAILURE = "app/Record/POST_FAN_POLL_FAILURE";

export const GET_POLL_BY_MATCH_ID = "app/Record/GET_POLL_BY_MATCH_ID";
export const GET_POLL_BY_MATCH_ID_SUCCESS =
  "app/Record/GET_POLL_BY_MATCH_ID_SUCCESS";
export const GET_POLL_BY_MATCH_ID_FAILURE =
  "app/Record/GET_POLL_BY_MATCH_ID_FAILURE";

export const GET_POLL_BY_ID = "app/Record/GET_POLL_BY_ID";
export const GET_POLL_BY_ID_SUCCESS = "app/Record/GET_POLL_BY_ID_SUCCESS";
export const GET_POLL_BY_ID_FAILURE = "app/Record/GET_POLL_BY_ID_FAILURE";

export const SAVE_CARD_POSITION = "app/Record/SAVE_CARD_POSITION";
