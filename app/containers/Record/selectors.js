import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the record state domain
 */

const selectRecordDomain = state => state.record || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Record
 */

const makeSelectRecord = () =>
  createSelector(selectRecordDomain, substate => substate);

export default makeSelectRecord;
export { selectRecordDomain };
