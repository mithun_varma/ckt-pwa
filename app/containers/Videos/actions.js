/*
 *
 * Videos actions
 *
 */

import { showToast } from "utils/misc";
import * as videoConstants from "./constants";

const ACTIVE_TOAST = {};

export const fetchVideoGallery = payload => ({
  type: videoConstants.FETCH_VIDEO_GALLERY,
  payload
});

export const fetchVideoGallerySuccess = payload => ({
  type: videoConstants.FETCH_VIDEO_GALLERY_SUCCESS,
  payload
});

export const fetchVideoGalleryFailure = err => {
  return {
    type: videoConstants.FETCH_VIDEO_GALLERY_FAILURE,
    payload: err
  };
};

// <====================FEATURED VIDEO=========================>

export const fetchFeaturedVideos = payload => ({
  type: videoConstants.FETCH_FEATURED_VIDEOS,
  payload
});

export const fetchFeaturedVideosSuccess = payload => ({
  type: videoConstants.FETCH_FEATURED_VIDEOS_SUCCESS,
  payload
});

export const fetchFeaturedVideosFailure = err => {
  showToast(
    ACTIVE_TOAST,
    "fetchFeaturedVideos",
    "Unable to fetch Featured Videos"
  );
  return {
    type: videoConstants.FETCH_FEATURED_VIDEOS_FAILURE,
    payload: err
  };
};

// <====================LATEST VIDEO=========================>

export const fetchLatestVideos = payload => ({
  type: videoConstants.FETCH_LATEST_VIDEOS,
  payload
});

export const fetchLatestVideosSuccess = payload => ({
  type: videoConstants.FETCH_LATEST_VIDEOS_SUCCESS,
  payload
});

export const fetchLatestVideosFailure = err => {
  showToast(ACTIVE_TOAST, "fetchLatestVideos", "Unable to fetch Latest Videos");
  return {
    type: videoConstants.FETCH_LATEST_VIDEOS_FAILURE,
    payload: err
  };
};
// <====================ALL VIDEOS=========================>

export const fetchVideos = payload => {
  return {
    type: videoConstants.FETCH_VIDEOS,
    payload
  };
};

export const fetchVideosSuccess = payload => {
  return {
    type: videoConstants.FETCH_VIDEOS_SUCCESS,
    payload
  };
};

export const fetchVideosFailure = err => {
  showToast(ACTIVE_TOAST, "fetchVideos", "Unable to fetch  Videos");
  return {
    type: videoConstants.FETCH_VIDEOS_FAILURE,
    payload: err
  };
};

// <====================OTHER VIDEO=========================>

export const fetchOtherVideos = payload => ({
  type: videoConstants.FETCH_OTHER_VIDEOS,
  payload
});

export const fetchOtherVideosSuccess = payload => ({
  type: videoConstants.FETCH_OTHER_VIDEOS_SUCCESS,
  payload
});

export const fetchOtherVideosFailure = err => {
  showToast(ACTIVE_TOAST, "fetchOtherVideos", "Unable to fetch Latest Videos");
  return {
    type: videoConstants.FETCH_OTHER_VIDEOS_FAILURE,
    payload: err
  };
};

export const fetchVideoDetails = payload => ({
  type: videoConstants.FETCH_VIDEO_DETAIL,
  payload
});

export const fetchVideoDetailsSuccess = payload => ({
  type: videoConstants.FETCH_VIDEO_DETAIL_SUCCESS,
  payload
});

export const fetchVideoDetailsFailure = err => {
  showToast(
    ACTIVE_TOAST,
    "fetchFeaturedPhotos",
    "Unable to fetch Featured Photos"
  );
  return {
    type: videoConstants.FETCH_VIDEO_DETAIL_FAILURE,
    payload: err
  };
};
