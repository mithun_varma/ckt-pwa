/*
 *
 * Videos reducer
 *
 */

import * as videosActions from "./constants";

export const initialState = {
  featuredVideoLoading: false,
  latestVideosLoading: false,
  otherVideosLoading: false,
  isFeaturedVideoExist: true,

  featuredVideo: [],
  latestVideos: [],
  otherVideos: [],
  videos: [],
  videosLoading: false,

  videoHost: "",
  videoOldLength: 0,
  videoDetailLoading: false,
  videoDetails: {},

  videoGalleryLoading: false,
  vieoGallery: []
};

function videosReducer(state = initialState, action) {
  switch (action.type) {
    // ALL VIDEOS
    case videosActions.FETCH_VIDEOS:
      return {
        ...state,
        videosLoading: true
      };
    case videosActions.FETCH_VIDEOS_SUCCESS: {
      return {
        ...state,
        videosLoading: false,
        videos: action.payload
      };
    }
    case videosActions.FETCH_VIDEOS_FAILURE: {
      return {
        ...state,
        videosLoading: false
      };
    }

    case videosActions.FETCH_VIDEO_GALLERY:
      return {
        ...state,
        videoGalleryLoading: true
      };
    case videosActions.FETCH_VIDEO_GALLERY_SUCCESS:
      return {
        ...state,
        videoGalleryLoading: false,
        vieoGallery: action.payload
      };
    case videosActions.FETCH_VIDEO_GALLERY_FAILURE:
      return {
        ...state,
        videoGalleryLoading: false
      };

    // <====================FEATURED VIDEO=========================>
    case videosActions.FETCH_FEATURED_VIDEOS:
      return {
        ...state,
        featuredVideoLoading: true,
        featuredVideo: []
      };

    case videosActions.FETCH_FEATURED_VIDEOS_SUCCESS: {
      const video = [...state.featuredVideo];
      return {
        ...state,
        featuredVideoLoading: false,
        isFeaturedVideoExist: action.payload.data.length > 0 && true,
        featuredVideo: action.payload.data,
        videoHost: action.payload.host,
        videoOldLength: action.payload.skip ? video.length : 0
      };
    }

    case videosActions.FETCH_FEATURED_VIDEOS_FAILURE:
      return {
        ...state,
        featuredVideoLoading: false,
        featuredVideo: []
      };

    // <====================LATEST VIDEO=========================>
    case videosActions.FETCH_LATEST_VIDEOS:
      return Object.assign({}, state, {
        latestVideosLoading: true,
        latestVideos: action.payload.skip ? [...state.latestVideos] : []
      });

    case videosActions.FETCH_LATEST_VIDEOS_SUCCESS: {
      const videos = [...state.latestVideos];
      return {
        ...state,
        latestVideosLoading: false,
        latestVideos: action.payload.skip
          ? videos.concat(action.payload.data)
          : action.payload.data,
        videoHost: action.payload.host,
        videoOldLength: action.payload.skip ? videos.length : 0
      };
    }

    case videosActions.FETCH_LATEST_VIDEOS_FAILURE:
      return Object.assign({}, state, { latestVideosLoading: false });

    // <====================OTHER VIDEO=========================>
    case videosActions.FETCH_OTHER_VIDEOS:
      return Object.assign({}, state, {
        otherVideosLoading: true,
        otherVideos: action.payload.skip ? [...state.otherVideos] : []
      });
    case videosActions.FETCH_OTHER_VIDEOS_SUCCESS: {
      const videos = [...state.otherVideos];
      return {
        ...state,
        otherVideosLoading: false,
        otherVideos: action.payload.skip
          ? videos.concat(action.payload.data)
          : action.payload.data,
        videoHost: action.payload.host,
        videoOldLength: action.payload.skip ? videos.length : 0
      };
    }
    case videosActions.FETCH_OTHER_VIDEOS_FAILURE:
      return Object.assign({}, state, { otherVideosLoading: false });
    // === Video Details
    case videosActions.FETCH_VIDEO_DETAIL:
      return {
        ...state,
        videoDetailLoading: true
      };
    case videosActions.FETCH_VIDEO_DETAIL_SUCCESS: {
      return {
        ...state,
        videoDetailLoading: false,
        videoDetails: action.payload.data
      };
    }
    case videosActions.FETCH_VIDEO_DETAIL_FAILURE:
      return {
        ...state,
        videoDetailLoading: false
      };
    default:
      return state;
  }
}

export default videosReducer;
