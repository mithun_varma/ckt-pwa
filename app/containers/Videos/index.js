/**
 *
 * Videos
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { Helmet } from "react-helmet";

import Header from "../../components-v2/commonComponent/Header";
import { black, white } from "../../styleSheet/globalStyle/color";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";

import makeSelectArticles from "../Articles/selectors";
import articlesReducer from "../Articles/reducer";
import * as articlesActions from "../Articles/actions";
import articlesSaga from "../Articles/saga";

import makeSelectVideos from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import VideoContainer from "../../components-v2/commonComponent/VideoContainer";
import ReactPlayer from "react-player";
import * as videosActions from "./actions";

import close from "../../images/closeFloat.png";
import { screenHeight } from "../../styleSheet/screenSize/ScreenDetails";
import ReactGA from "react-ga";
import VideoCard from "../../components-v2/Articles/VideoCard";
import EmptyState from "../../components-v2/commonComponent/EmptyState";

/* eslint-disable react/prefer-stateless-function */
export class Videos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "Videos",
      videoPlay: false,
      currentVideoPlayUrl: ""
    };
  }

  handleCloseVid = () => {
    this.setState(
      {
        videoPlay: false,
        currentVideoPlayUrl: ""
      },
      () => {
        return true;
      }
    );
  };

  showVid = url => {
    if (url) {
      this.setState(
        {
          videoPlay: true,
          currentVideoPlayUrl: url.path
        },
        () => {
          return true;
        }
      );
    } else {
      return null;
    }
  };

  componentDidMount = () => {
    // ReactGA.initialize("UA-136526387-1");
    // ReactGA.pageview(
    //   "https://www.cricket.com" +
    //     window.location.pathname +
    //     window.location.search
    // );
    this.props.fetchVideos();
    this.props.fetchFeaturedVideos({
      filters: ["featured"]
    });

    this.props.fetchLatestVideos({
      filters: ["recent"]
    });

    this.props.fetchOtherVideos({
      filters: ["-recent", "-featured"]
    });
  };

  render() {
    const { history } = this.props;

    return (
      <React.Fragment
      // style={{
      //   background: "linear-gradient(30deg, #141b2f, rgba(114, 118, 130, 0))"
      // }}
      >
        <Helmet titleTemplate="%s | cricket.com">
          <title>Cricket Video | Cricket Updates and Highlights</title>
          <meta
            name="description"
            content="Watch latest cricket videos and clips, highlights, interviews, blogs and more at cricket.com. Get the latest cricket updates only at cricket.com. "
          />
          <meta
            name="keywords"
            content="live cricket video, live cricket streaming, cricket updates, cricket news, cricket news ipl"
          />
          <link
            rel="canonical"
            href={`www.cricket.com${history &&
              history.location &&
              history.location.pathname}`}
          />
        </Helmet>

        {/* EXP */}

        {/* {this.state.videoPlay && (
          <div
            style={{
              height: "100vh",
              width: "100vw",
              position: "fixed",
              top: 0,
              zIndex: "99999",
              background: "black"
            }}
          >
            <ReactPlayer
              url={this.state.currentVideoPlayUrl}
              width="100%"
              height="100%"
              controls="true"
              config={{
                youtube: {
                  playerVars: { showinfo: 1 }
                }
              }}
            />
            <div
              onClick={() => this.handleCloseVid()}
              style={{
                height: "30px",
                width: "30px",
                padding: "1px",
                background: "transparent",
                borderRadius: "50%",
                position: "fixed",
                top: 100,
                right: "6%",
                zIndex: "99999"
              }}
            >
              {" "}
              <img src={close} style={{ height: "100%", width: "100%" }} />{" "}
            </div>
          </div>
        )} */}
        {/* END OF EXP */}
        <Header
          title={"Videos"}
          isHeaderBackground={true}
          leftArrowBack
          textColor={black}
          backgroundColor={"#edeff4"}
          history={this.props.history}
          showHeader={true}
          leftIconOnClick={() => {
            this.props.history.goBack();
          }}
          titleStyles={{ fontSize: 18 }}
        />

        <div style={{ paddingTop: 52 }}>
          {/* <div style={{marginLeft:20,
               
                  width: '79px',
                  height: '27px',
                  fontFamily: 'Montserrat',
                  fontSize: '22px',
                  fontWeight: 'bold',
                  fontStyle: 'normal',
                  fontStretch: 'normal',
                  lineHeight: 'normal',
                  letterSpacing: 'normal',
                  color: '#141b2f'
            
                
                }}> Videos </div> */}
          {/* {vidData &&
            vidData.map((data, key) => {
              if (key === 0) {
                return (
                  <VideoContainer
                    rootStyles={{
                      marginTop: 12,
                      width: "90%"
                    }}
                    data={data.vids}
                    isBig={true}
                    loading={false}
                    title={data.type}
                    backgroundStyles={{
                      background:
                        "linear-gradient(30deg, #141b2f, rgba(114, 118, 130, 0))"
                    }}
                    key={data.type}
                    showVid={this.showVid}
                  />
                );
              } else {
                return (
                  <VideoContainer
                    rootStyles={{
                      marginTop: 12,
                      width: "90%"
                    }}
                    data={data.vids}
                    isBig={false}
                    loading={false}
                    title={data.type}
                    backgroundStyles={{
                      background:
                        "linear-gradient(30deg, #141b2f, rgba(114, 118, 130, 0))"
                    }}
                    key={data.type}
                    showVid={this.showVid}
                  />
                );
              }
            })} */}
          {this.props.videos.featuredVideosLoading ? (
            <div
              style={{
                // width: "90%",
                // padding:'16px',
                minHeight: 200,
                background: "#fff",
                margin: "16px"
              }}
            >
              <img
                src={require("../../images/CricketLoader.gif")}
                style={{
                  width: "100px",
                  marginLeft: "30%",
                  marginTop: "35%",
                  // height: 200,
                  background: "#fff"
                }}
              />
            </div>
          ) : this.props.videos.featuredVideo.length > 0 ? (
            <div
              style={{
                margin: "0 16px 10px 16px"
              }}
            >
              <VideoCard
                loading={this.props.videos.featuredVideosLoading}
                videos={this.props.videos.featuredVideo}
                title="Trending Videos"
                history={history}
              />
            </div>
          ) : (
            <EmptyState msg="no trending videos found" />
          )}
          {this.props.videos.latestVideosLoading ? (
            <div
              style={{
                // width: "90%",
                // padding:'16px',
                minHeight: 200,
                background: "#fff",
                margin: "16px"
              }}
            >
              <img
                src={require("../../images/CricketLoader.gif")}
                style={{
                  width: "100px",
                  marginLeft: "30%",
                  marginTop: "35%",
                  // height: 200,
                  background: "#fff"
                }}
              />
            </div>
          ) : this.props.videos.latestVideos.length > 0 ? (
            <div
              style={{
                margin: "0 16px 10px 16px"
              }}
            >
              <VideoCard
                loading={this.props.videos.latestVideosLoading}
                videos={this.props.videos.latestVideos}
                title="Latest Videos"
                history={history}
              />
            </div>
          ) : (
            <EmptyState
              rootStyles={{
                height: 200,
                fontFamily: "mont400",
                background: white,
                margin: 16
              }}
              msg="no latest videos found"
            />
          )}
          {this.props.videos.otherVideosLoading ? (
            <div
              style={{
                // width: "90%",
                // padding:'16px',
                minHeight: 200,
                background: "#fff",
                margin: "16px"
              }}
            >
              <img
                src={require("../../images/CricketLoader.gif")}
                style={{
                  width: "100px",
                  marginLeft: "30%",
                  marginTop: "35%",
                  // height: 200,
                  background: "#fff"
                }}
              />
            </div>
          ) : this.props.videos.otherVideos.length > 0 ? (
            <div
              style={{
                margin: "0 16px 10px 16px"
              }}
            >
              <VideoCard
                loading={this.props.videos.otherVideosLoading}
                videos={this.props.videos.otherVideos}
                title="Other Videos"
                history={history}
              />
            </div>
          ) : (
            <EmptyState
              rootStyles={{
                height: 200,
                fontFamily: "mont400",
                background: white,
                margin: 16
              }}
              msg="no other videos found"
            />
          )}
        </div>
      </React.Fragment>
    );
  }
}

Videos.propTypes = {
  fetchNewsOpinion: PropTypes.func,
  fetchFeaturedVideos: PropTypes.func,
  fetchLatestVideos: PropTypes.func,
  fetchVideos: PropTypes.func,
  fetchOtherVideos: PropTypes.func,
  articles: PropTypes.object,
  videos: PropTypes.object,
  history: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  videos: makeSelectVideos(),
  articles: makeSelectArticles()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchNewsOpinion: payload =>
      dispatch(articlesActions.fetchNewsOpinion(payload)),
    fetchFeaturedVideos: payload =>
      dispatch(videosActions.fetchFeaturedVideos(payload)),
    fetchVideos: payload => dispatch(videosActions.fetchVideos()),
    fetchLatestVideos: payload =>
      dispatch(videosActions.fetchLatestVideos(payload)),
    fetchOtherVideos: payload =>
      dispatch(videosActions.fetchOtherVideos(payload)),
    fetchVideoGallery: payload =>
      dispatch(videosActions.fetchVideoGallery(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withArticlesReducer = injectReducer({
  key: "articles",
  reducer: articlesReducer
});
const withArticlesSaga = injectSaga({ key: "articles", saga: articlesSaga });
const withVideosReducer = injectReducer({ key: "videos", reducer });
const withVideosSaga = injectSaga({ key: "videos", saga });

const videosList = [
  {
    type: "Trending",
    vids: [
      {
        name: "dummy ",
        vidId: "cdc-62-eden-gardens",
        path: "https://youtu.be/uSGCAJS6qWg"
      },
      {
        name: "dummy vid",
        vidId: "cdc-94-wankhede-stadium",
        path: "https://youtu.be/uSGCAJS6qWg"
      },
      {
        name: "dummy vid",
        vidId: "cdc-251-rajiv-gandhi-international-cricket-stadium",
        path: "https://youtu.be/uSGCAJS6qWg"
      }
    ]
  },
  {
    type: "Latest Videos",
    vids: [
      {
        name: "dummy vid 2",
        vidId: "cdc-116-feroz-shah-kotla",
        path: "https://youtu.be/uSGCAJS6qWg"
      },
      {
        name: "dummy , Bangalore",
        vidId: "cdc-1-m-chinnaswamy-stadium",
        path: "https://youtu.be/uSGCAJS6qWg"
      },
      {
        name: "Dummy 5",
        vidId: "cdc-251-rajiv-gandhi-international-cricket-stadium",
        path: "https://youtu.be/uSGCAJS6qWg"
      }
    ]
  },
  {
    type: "Other videos",
    vids: [
      {
        name: "India vs Pak",
        vidId: "cdc-249-lord's-cricket-ground",
        path: "https://youtu.be/uSGCAJS6qWg"
      },
      {
        name: "India vs Pak",
        vidId: "cdc-259-melbourne-cricket-ground",
        path: "https://youtu.be/uSGCAJS6qWg"
      }
    ]
  }
];

export default compose(
  withArticlesReducer,
  withArticlesSaga,
  withVideosReducer,
  withVideosSaga,
  withConnect
)(Videos);
