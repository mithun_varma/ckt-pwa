/* eslint-disable no-param-reassign */
import { call, put, takeLatest } from "redux-saga/effects";

import {
  API_GET_VIDEOS,
  API_GET_VIDEOS_BY_TYPE,
  API_GET_VIDEOS_BY_ID,
  API_GET_VIDEO_GALLERY
} from "utils/requestUrls";
import request from "utils/request";

import * as videosConstants from "./constants";
import * as videosActions from "./actions";

export function* getFeaturedVideos(payload) {
  const config = {
    ...API_GET_VIDEOS_BY_TYPE,
    params: {
      ...API_GET_VIDEOS_BY_TYPE.params,
      ...payload.payload
    }
  };
  try {
    const options = yield call(request, config);
    yield put(
      videosActions.fetchFeaturedVideosSuccess({
        data: options.data.data,
        host: options.data.host,
        skip: payload.payload.skip
      })
    );
  } catch (err) {
    yield put(videosActions.fetchFeaturedVideosFailure(err));
  }
}

export function* getVideoGallery(payload) {
  const config = {
    ...API_GET_VIDEO_GALLERY,
    params: {
      ...API_GET_VIDEO_GALLERY.params,
      ...payload.payload
    }
  };
  try {
    const options = yield call(request, config);
    yield put(
      videosActions.fetchVideoGallerySuccess({
        data: options.data.data,
        host: options.data.host,
        skip: payload.payload.skip
      })
    );
  } catch (err) {
    yield put(videosActions.fetchVideoGalleryFailure(err));
  }
}

export function* getLatestVideos(payload) {
  const config = {
    ...API_GET_VIDEOS_BY_TYPE,
    params: {
      ...API_GET_VIDEOS_BY_TYPE.params,
      ...payload.payload
    }
  };
  try {
    const options = yield call(request, config);
    // console.log(options);
    yield put(
      videosActions.fetchLatestVideosSuccess({
        data: options.data.data,
        host: options.data.host,
        skip: payload.payload.skip
      })
    );
  } catch (err) {
    yield put(videosActions.fetchLatestVideosFailure(err));
  }
}

export function* getOtherVideos(payload) {
  const config = {
    ...API_GET_VIDEOS_BY_TYPE,
    params: {
      ...API_GET_VIDEOS_BY_TYPE.params,
      ...payload.payload
    }
  };
  try {
    const options = yield call(request, config);
    // console.log(options);
    yield put(
      videosActions.fetchOtherVideosSuccess({
        data: options.data.data,
        host: options.data.host,
        skip: payload.payload.skip
      })
    );
  } catch (err) {
    yield put(videosActions.fetchOtherVideosFailure(err));
  }
}

// === Video Details
export function* getVideoDetails(payload) {
  const requestUrl = API_GET_VIDEOS_BY_ID(payload.payload.id);
  delete payload.payload.id;
  const config = {
    ...requestUrl,
    params: {
      ...requestUrl.params,
      ...payload.payload
    }
  };
  try {
    const options = yield call(request, config);
    yield put(
      videosActions.fetchVideoDetailsSuccess({
        data: options.data.data,
        host: options.data.host
      })
    );
  } catch (err) {
    yield put(videosActions.fetchVideoDetailsFailure(err));
  }
}

// === All Videos
export function* getAllVideos(payload) {
  const requestUrl = API_GET_VIDEOS();
  const config = {
    ...requestUrl,
    params: {
      ...requestUrl.params
    }
  };
  try {
    const options = yield call(request, config);
    yield put(
      videosActions.fetchVideosSuccess({
        data: options.data.data,
        host: options.data.host
      })
    );
  } catch (err) {
    yield put(videosActions.fetchVideosFailure(err));
  }
}

export default function* defaultSaga() {
  yield takeLatest(videosConstants.FETCH_FEATURED_VIDEOS, getFeaturedVideos);
  yield takeLatest(videosConstants.FETCH_LATEST_VIDEOS, getLatestVideos);
  yield takeLatest(videosConstants.FETCH_VIDEOS, getAllVideos);
  yield takeLatest(videosConstants.FETCH_OTHER_VIDEOS, getOtherVideos);
  yield takeLatest(videosConstants.FETCH_VIDEO_DETAIL, getVideoDetails);
  yield takeLatest(videosConstants.FETCH_VIDEO_GALLERY, getVideoGallery);
}
