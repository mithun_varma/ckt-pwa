
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import Header from 'components/Common/Header';
import Loader from 'components/Common/Loader';
import DisplayPhoto from 'components/Photos/DisplayPhoto';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import makeSelectPhotos from '../Photos/selectors';
import reducer from '../Photos/reducer';
import saga from '../Photos/saga';
import * as photosActions from '../Photos/actions';

/* eslint-disable react/prefer-stateless-function */
export class PhotosList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showPhoto: false,
      activeImage: 0,
    };
  }

  componentDidMount = () => {
    this.props.fetchGalleryById({ id: this.props.match.params.id });
  };

  togglePhoto = (flag, key = 0) => {
    this.setState({
      showPhoto: flag,
      activeImage: key,
    });
  };

  render() {
    const { photos, history } = this.props;
    return this.state.showPhoto || this.props.match.params.showgallery ? (
      <DisplayPhoto
        togglePhoto={this.togglePhoto}
        photos={photos}
        history={history}
        match={this.props.match}
        activeImage={this.state.activeImage}
      />
    ) : (
      <div className="wrapperContainer">
        <Header
          title={this.props.match.params.title}
          leftIcon="arrowBack"
          leftIconOnClick={() => history.goBack()}
          history={history}
        />
        <main>
          <div className="photos__wrapper">
            <div className="photos__list">
              {photos.photoGalleryloading ? (
                <Loader styles={{ height: '120px' }} noOfLoaders={4} />
              ) : (
                photos.photoGallery.map((list, key) => (
                  <div className="photos__item" onClick={() => this.togglePhoto(true, key)}>
                    <img src={photos.photoHost + list.imageData.image} alt="" />
                  </div>
                ))
              )}
            </div>
          </div>
        </main>
      </div>
    );
  }
}

PhotosList.propTypes = {
  fetchGalleryById: PropTypes.func,
  photos: PropTypes.object,
  history: PropTypes.object,
  match: PropTypes.object,
};

function mapDispatchToProps(dispatch) {
  return {
    fetchGalleryById: payload => dispatch(photosActions.fetchGalleryById(payload)),
  };
}

const mapStateToProps = createStructuredSelector({
  photos: makeSelectPhotos(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'photos', reducer });
const withSaga = injectSaga({ key: 'photos', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(PhotosList);
