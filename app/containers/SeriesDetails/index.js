/**
 *
 * Series
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import EmptyState from "../../components-v2/commonComponent/EmptyState";
import { createStructuredSelector } from "reselect";
import Size from "lodash/size";
import animate from "../../utils/animationConfig";

import { Helmet } from "react-helmet";

import Header from "../../components-v2/commonComponent/Header";
import MatchList from "../../components-v2/Series/MatchList";
import SeriesSquad from "../../components-v2/Series/SeriesSquad";
import Articles from "../../components-v2/Series/Articles";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";

import makeSelectSeries from "../Series/selectors";
import seriesReducer from "../Series/reducer";
import * as seriesActions from "../Series/actions";
import seriesSaga from "../Series/saga";

import makeSelectRecord from "../Record/selectors";
import recordReducer from "../Record/reducer";
import * as recordActions from "../Record/actions";
import recordSaga from "../Record/saga";

import {
  grey_4,
  black,
  white,
  grey_8,
  gradientGrey
} from "../../styleSheet/globalStyle/color";
import TabBar from "../../components-v2/commonComponent/TabBar";
import { Reveal } from "react-reveal";
import CardGradientTitle from "../../components-v2/commonComponent/CardGradientTitle";

import ListItems from "../../components-v2/commonComponent/ListItems";
import Loader from "../../components/Common/Loader";
import HrLine from "../../components-v2/commonComponent/HrLine";
import PointsListItems from "../../components-v2/commonComponent/PointsListItems";
import ReactGA from "react-ga";

/* eslint-disable react/prefer-stateless-function */

const scheduleTabs = ["MATCHES", "SQUADS", "POINTS TABLE", "NEWS", "VENUES"];
const scheduleTabsWithoutPointsTable = ["MATCHES", "SQUADS", "NEWS", "VENUES"];

let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  // ClevertapReact.initialize("TEST-Z88-4KR-845Z"); // Old Key
  ClevertapReact.initialize("W88-4KR-845Z");
  // initializeFirebase();
  // askForPermissioToReceiveNotifications();
}
export class SeriesDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "MATCHES",
      title: "",
      isHeaderBackground: true,
      titlePlaced: false
    };
    this.handelActive = this.handelActive.bind(this);
  }

  redirectTo = id => {
    ClevertapReact.event("Stadium", {
      source: "Stadium",
      groundId: id
    });

    this.props.history.push(`/grounds/${id}`);
  };

  handelActive(activeTab) {
    this.setState({ activeTab });
  }
  componentDidMount() {
    // ReactGA.initialize("UA-136526387-1");
    // ReactGA.pageview(
    //   "https://www.cricket.com" +
    //     window.location.pathname +
    //     window.location.search
    // );
    // window.addEventListener("scroll", this.handleScroll);
    this.props.fetchSeriesDetails({
      seriesId: this.props.match.params.seriesid
    });
    if (this.props.match.params.type == "points") {
      this.props.fetchPointsTable({
        seriesId: this.props.match.params.seriesid
      });
      this.setState({
        activeTab: "POINTS TABLE"
      });
      // } else if (this.props.match.params.seriesname == "ipl") {
      // } else if (this.props.match.params.seriesname.indexOf("world-cup") != -1) {
      //   this.props.fetchPointsTable({
      //     seriesId: this.props.match.params.seriesid
      //   });
    }
  }

  componentWillUnmount() {
    // window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = event => {
    const scrollTop = event.target.scrollingElement.scrollTop;
    // console.log("object: ", scrollTop);
    if (scrollTop > 38 && !this.state.titlePlaced) {
      this.setState({
        title:
          this.props.series.seriesDetails &&
          this.props.series.seriesDetails.name
            ? this.props.series.seriesDetails.name
            : "Series",
        titlePlaced: true,
        isHeaderBackground: true
      });
    } else if (scrollTop < 39) {
      this.setState({
        title: "",
        titlePlaced: false,
        isHeaderBackground: true
      });
    }
  };

  handleActiveTab = activeTab => {
    this.setState(
      {
        activeTab
      },
      () => {
        if (activeTab == "POINTS TABLE") {
          this.props.fetchPointsTable({
            seriesId: this.props.match.params.seriesid
          });
        }
      }
    );
  };

  render() {
    const {
      series,
      record,
      fetchSeriesDetails,
      match,
      history,
      fetchSeriesSquads,
      fetchSeriesNews,
      fetchSeriesVenues
    } = this.props;
    return Size(series.seriesDetails) <= 0 ? (
      // <Loader styles={{ height: "150px" }} noOfLoaders={5} />
      <div
        style={{
          height: "100vh",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          textAlign: "center"
        }}
      >
        <img src={require("../../images/CricketLoader.gif")} alt="" />
      </div>
    ) : series.seriesDetails && series.seriesDetails.name ? (
      <div
        style={{
          padding: "54px 16px 16px"
        }}
      >
        <Helmet titleTemplate="%s | cricket.com">
          <title>{`${series.seriesDetails &&
            series.seriesDetails
              .name} Squads, Scorecards, Match Preview, News Updates`}</title>
          <meta
            name="description"
            content="Squads, Scorecards, Match Preview, News Updates"
          />
          {/* <meta
            name="keywords"
            content={(article.articleDetails && article.articleDetails.seoKeywords) || ''}
          /> */}
          <link
            rel="canonical"
            href={`www.cricket.com${history &&
              history.location &&
              history.location.pathname}`}
          />
        </Helmet>
        {false && (
          <div
            style={{
              fontFamily: "Montserrat",
              fontWeight: 700,
              fontSize: 22,
              color: black,
              marginBottom: 12
            }}
          >
            {series.seriesDetails && series.seriesDetails.name
              ? series.seriesDetails.name
              : "Series"}
          </div>
        )}
        <Header
          // title={this.state.title}
          title={
            series.seriesDetails && series.seriesDetails.name
              ? series.seriesDetails.name
              : "Series"
          }
          // isHeaderBackground={this.state.isHeaderBackground}
          isHeaderBackground={true}
          leftArrowBack
          textColor={black}
          backgroundColor={grey_4}
          history={history}
          leftIconOnClick={() => {
            history.goBack();
          }}
          titleStyles={{
            fontSize: 16
          }}
        />

        <div style={{}}>
          <Reveal effect={animate.series.tags.effect}>
            <TabBar
              items={
                series.seriesDetails &&
                !series.seriesDetails.isPointsTableExists
                  ? scheduleTabsWithoutPointsTable
                  : scheduleTabs
              }
              // onClick={val => {
              //   this.setState({ activeTab: val });
              // }}
              onClick={this.handleActiveTab}
              activeItem={this.state.activeTab}
            />
          </Reveal>
          {series.seriesDetails.matchMiniScoreCards && (
            <div className="wrapper-container-Remove">
              {this.state.activeTab === "MATCHES" && (
                <MatchList
                  Click={matchId => {
                    history.push(`/score/${matchId}`);
                  }}
                  matches={series.seriesDetails.matchMiniScoreCards}
                  series={series}
                />
              )}
              {this.state.activeTab === "SQUADS" && (
                <SeriesSquad
                  squads={series.seriesDetails.squads}
                  format={series.seriesDetails.format}
                />
              )}
              {this.state.activeTab === "NEWS" && (
                <Articles
                  id={series.seriesDetails.id}
                  news={series.seriesNews}
                  newsLoading={series.seriesNewsLoading}
                  request={this.props.fetchSeriesNews}
                  history={history}
                />
              )}
              <div
                style={{
                  background: white
                }}
              >
                {this.state.activeTab === "VENUES" &&
                  series.seriesDetails.venues.map((ele, key) => {
                    return (
                      <ListItems
                        param={ele.venueId}
                        key={key}
                        title={ele.displayName}
                        onClick={this.redirectTo}
                      />
                    );
                  })}
              </div>
              <div
                style={{
                  background: white
                }}
              >
                {this.state.activeTab === "POINTS TABLE" &&
                (this.props.match.params.type == "points" ||
                  series.seriesDetails.isPointsTableExists) ? (
                  // this.props.match.params.seriesname.indexOf("world-cup") != -1
                  // ||
                  //   this.props.series.seriesDetails.name == "IPL"
                  // this.props.match.params.seriesname == "ipl" ||
                  // this.props.match.params.seriesname == "IPL"
                  <div>
                    {series.seriesDetails &&
                      series.seriesDetails.name && (
                        <CardGradientTitle
                          title={`${series.seriesDetails.name} POINTS TABLE`}
                          rootStyles={{
                            color: grey_8,
                            fontSize: 10,
                            textTransform: "uppercase"
                          }}
                        />
                      )}
                    <div>
                      <PointsListItems
                        name="TEAMS"
                        match="M"
                        win="W"
                        loss="L"
                        tie="T"
                        noResult="NR"
                        points="PTS"
                        netRunRate="NRR"
                        isHeader
                        rootStyles={{
                          // background: gradientGrey,
                          padding: "12px 0"
                        }}
                      />
                      <HrLine />
                      {record.pointsTable && record.pointsTable.length > 0 ? (
                        record.pointsTable.map((list, key) => (
                          <div>
                            <PointsListItems
                              name={list && list.teamName ? list.teamName : "-"}
                              match={
                                list && list.matchesPlayed
                                  ? list.matchesPlayed
                                  : "0"
                              }
                              win={
                                list && list.matchesWon ? list.matchesWon : "0"
                              }
                              loss={
                                list && list.matchesLost
                                  ? list.matchesLost
                                  : "0"
                              }
                              tie={
                                list && list.matchesDraw
                                  ? list.matchesDraw
                                  : "0"
                              }
                              noResult={
                                list && list.noResult ? list.noResult : "0"
                              }
                              points={list && list.points ? list.points : "0"}
                              netRunRate={
                                list && list.netRunRate ? list.netRunRate : "0"
                              }
                              avatar={
                                list && list.teamAvatar
                                  ? list.teamAvatar
                                  : "http://via.placeholder.com/58x30.png?"
                              }
                              qualified={
                                list &&
                                list.qualificationProbability &&
                                list.qualificationProbability == "100%"
                              }
                            />
                            <HrLine />
                          </div>
                        ))
                      ) : (
                        <EmptyState msg="Points table not found" />
                      )}
                    </div>
                  </div>
                ) : (
                  this.state.activeTab === "POINTS TABLE" && (
                    <EmptyState msg="Points table not found" />
                  )
                )}
              </div>
            </div>
          )}
        </div>
      </div>
    ) : (
      <React.Fragment>
        <div
          style={{
            fontFamily: "Montserrat",
            fontWeight: 700,
            fontSize: 22,
            color: black,
            marginBottom: 12
          }}
        >
          Series
        </div>
        <Header
          title={""}
          isHeaderBackground={false}
          leftArrowBack
          textColor={black}
          backgroundColor={grey_4}
          history={history}
          leftIconOnClick={() => {
            history.goBack();
          }}
        />
        <EmptyState msg="Could not find data for the series" />
      </React.Fragment>
    );
  }
}

SeriesDetails.propTypes = {
  fetchSeriesDetails: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  series: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  fetchSeriesSquads: PropTypes.func.isRequired,
  fetchSeriesVenues: PropTypes.func,
  fetchSeriesNews: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  series: makeSelectSeries(),
  record: makeSelectRecord()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchSeriesDetails: payload =>
      dispatch(seriesActions.fetchSeriesDetails(payload)),
    fetchSeriesSquads: payload =>
      dispatch(seriesActions.fetchSeriesSquads(payload)),
    fetchSeriesVenues: payload =>
      dispatch(seriesActions.fetchSeriesVenues(payload)),
    fetchSeriesNews: payload =>
      dispatch(seriesActions.fetchSeriesNews(payload)),
    fetchPointsTable: payload =>
      dispatch(recordActions.fetchPointsTable(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withSeriesReducer = injectReducer({
  key: "series",
  reducer: seriesReducer
});
const withSeriesSaga = injectSaga({ key: "series", saga: seriesSaga });
// ============
const withRecordReducer = injectReducer({
  key: "record",
  reducer: recordReducer
});
const withRecordSaga = injectSaga({ key: "record", saga: recordSaga });
// ============

export default compose(
  withSeriesReducer,
  withSeriesSaga,
  withRecordReducer,
  withRecordSaga,
  withConnect
)(SeriesDetails);
