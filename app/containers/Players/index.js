import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectPlayers from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import * as playersActions from "./actions";
import PlayerProfile from "./../../components-v2/screenComponent/playerProfile/PlayerProfile";
import ReactGA from "react-ga";
import EmptyState from "../../components-v2/commonComponent/EmptyState";

/* eslint-disable react/prefer-stateless-function */
export class Players extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      playerId: ""
    };
    this.playerId = this.props.match.params.playerId;
  }
  componentDidMount() {
    // ReactGA.initialize("UA-136526387-1");
    // ReactGA.pageview(
    //   "https://www.cricket.com" +
    //     window.location.pathname +
    //     window.location.search
    // );
    const playerId = this.props.match.params.playerId;
    // console.log(playerId);
    this.props.fetchPlayerDetails({ playerId });
    this.props.getPlayerMoments({ playerId });
    this.props.fetchPlayerNews({ playerId });
  }

  static getDerivedStateFromProps(nextProps, state) {
    if (nextProps.match.params.playerId !== state.playerId) {
      nextProps.fetchPlayerDetails({
        playerId: nextProps.match.params.playerId
      });
      nextProps.fetchPlayerNews({ playerId: nextProps.match.params.playerId });
      nextProps.getPlayerMoments({ playerId: nextProps.match.params.playerId });
      return {
        playerId: nextProps.match.params.playerId
      };
    }
    return null;
  }
  render() {
    const {
      players,
      history,
      featuredPlayers,
      fetchPlayerDetails,
      fetchPlayerNews
    } = this.props;
    const playerDetails = players.playerDetails[this.playerId];
    // console.log(this.state.playerId);
    // console.log(playerDetails);
    if (playerDetails != null) {
      return (
        <div>
          <PlayerProfile
            playerDetails={playerDetails}
            playerNews={players.playerNews}
            history={history}
            featuredPlayers={players}
            playerMoments={players.playerMoments}
          />
        </div>
      );
    } else {
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "100vh"
          }}
        >
          {/* <img
            src={require("../../images/CricketLoader.gif")}
            // style={{ margin: "40% auto" }}
          /> */}
          <EmptyState msg="no player details found" />
        </div>
      );
    }
  }
}

Players.propTypes = {
  players: PropTypes.object,
  fetchPlayersList: PropTypes.func,
  fetchPlayerDetails: PropTypes.func,
  history: PropTypes.object,
  match: PropTypes.object,
  fetchPlayerNews: PropTypes.func
};

const mapStateToProps = createStructuredSelector({
  players: makeSelectPlayers()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchPlayersList: payload =>
      dispatch(playersActions.fetchPlayersList(payload)),
    fetchPlayerDetails: payload =>
      dispatch(playersActions.fetchPlayerDetails(payload)),
    getPlayerMoments: payload =>
      dispatch(playersActions.fetchPlayerMoments(payload)),
    fetchPlayerNews: payload =>
      dispatch(playersActions.fetchPlayerNews(payload)),
    fetchFeaturedPlayers: payload =>
      dispatch(playersActions.fetchFeaturedPlayers(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "players", reducer });
const withSaga = injectSaga({ key: "players", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(Players);
