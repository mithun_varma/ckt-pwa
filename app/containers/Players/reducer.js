/*
 *
 * Players reducer
 *
 */
/* eslint no-fallthrough: 0 */
/* eslint no-else-return: 0 */
/* eslint no-underscore-dangle: 0 */
// import partition from 'lodash/partition';
import {
  PLAYERS_LIST,
  PLAYERS_LIST_SUCCESS,
  PLAYERS_LIST_FAILURE,
  PLAYERS_DETAILS,
  PLAYERS_DETAILS_SUCCESS,
  PLAYERS_DETAILS_FAILURE,
  PLAYER_NEWS,
  PLAYER_NEWS_SUCCESS,
  PLAYER_NEWS_FAILURE,
  FEATURED_PLAYER,
  FEATURED_PLAYER_SUCCESS,
  FEATURED_PLAYER_FAILURE,
  PLAYER_MOMENTS,
  PLAYER_MOMENTS_SUCCESS,
  PLAYER_MOMENTS_FAILURE
} from "./constants";

export const initialState = {
  playersListLoading: false,
  playersList: [],
  featuredPlayersLoading: false,
  featuredPlayers: [],
  playerDetailsLoading: false,
  playerDetails: {},
  playerMomentsLoading: false,
  playerMoments: [],
  playerNewsLoading: false,
  playerNews: [],
  playerNewsOldLength: 0,
  playerNewsFeatured: [],
  articleHost: "",
  lastFetched: "",
  oldListLength: 0 // Don't Remove, it is used for InfinteScroll
};

function playersReducer(state = initialState, action) {
  switch (action.type) {
    case PLAYERS_LIST:
      return Object.assign({}, state, { playersListLoading: true });
    case PLAYERS_LIST_SUCCESS: {
      const playersList = [...state.playersList];
      return {
        ...state,
        playersListLoading: false,
        oldListLength: action.payload.skip ? playersList.length : 0,
        // playersList: action.payload.length ? playersList.concat(action.payload) : playersList,
        playersList: action.payload.skip
          ? playersList.concat(action.payload.data)
          : action.payload.data,
        lastFetched: new Date().getTime().toString()
      };
    }
    case PLAYERS_LIST_FAILURE:
      return Object.assign({}, state, { playersListLoading: false });

    // Featured Players List Case
    case FEATURED_PLAYER:
      return Object.assign({}, state, { featuredPlayersLoading: true });
    case FEATURED_PLAYER_SUCCESS: {
      // const featuredPlayers = [...state.featuredPlayers];
      return {
        ...state,
        featuredPlayersLoading: false,
        featuredPlayers: action.payload.data
      };
    }
    case FEATURED_PLAYER_FAILURE:
      return Object.assign({}, state, { featuredPlayersLoading: false });

    case PLAYERS_DETAILS:
      return Object.assign({}, state, { playerDetailsLoading: true });
    case PLAYERS_DETAILS_SUCCESS:
    // debugger
      return {
        ...state,
        playerDetailsLoading: false,
        playerDetails: {
          ...state.playerDetails,
          [action.payload.crictecPlayerId]: action.payload
        }
      };
    case PLAYERS_DETAILS_FAILURE:
      return Object.assign({}, state, { playerDetailsLoading: false });

    case PLAYER_MOMENTS:
      return Object.assign({}, state, { playerMomentsLoading: true });
    case PLAYER_MOMENTS_SUCCESS:
      return {
        ...state,
        playerMomentsLoading: false,
        playerMoments: action.payload,
      };
    case PLAYER_MOMENTS_FAILURE:
      return Object.assign({}, state, { playerMomentsLoading: false });
    case PLAYER_NEWS:
      return {
        ...state,
        playerNewsLoading: true
      };
    case PLAYER_NEWS_SUCCESS:
      if (!action.payload.skip) {
        // const newsGrouped = partition(action.payload.news, d => d.filters.featured);
        // const [playerNewsFeatured, playerNews] = newsGrouped;
        return {
          ...state,
          playerNewsLoading: false,
          playerNews: action.payload.news,
          // playerNewsFeatured,
          articleHost: action.payload.host,
          playerNewsOldLength: 0
        };
      } else {
        const playerNews = [...state.playerNews];
        return {
          ...state,
          playerNewsLoading: false,
          playerNews: action.payload.news.length
            ? playerNews.concat(action.payload.news)
            : playerNews,
          articleHost: action.payload.host,
          playerNewsOldLength: playerNews
        };
      }
    case PLAYER_NEWS_FAILURE:
      return {
        ...state,
        playerNewsLoading: false
      };
    default:
      return state;
  }
}

export default playersReducer;
