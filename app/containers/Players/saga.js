import { call, put, takeLatest } from "redux-saga/effects";

import {
  API_GET_PLAYERS_LIST,
  API_GET_PLAYER_DETAILS,
  API_PLAYER_NEWS,
  API_GET_FEATURED_PLAYERS_LIST,
  API_GET_PLAYER_MOMENTS
} from "utils/requestUrls";
import request from "utils/request";

import * as playersConstants from "./constants";
import * as playersActions from "./actions";

export function* getPlayersList(payload) {
  const config = {
    ...API_GET_PLAYERS_LIST
    // params: {
    //   ...API_GET_PLAYERS_LIST.params,
    //   ...payload.payload
    // }
  };
  try {
    // const options = yield call(request, API_GET_PLAYERS_LIST);
    const options = yield call(request, config);

    // yield put(playersActions.fetchPlayersListSuccess(options.data.data));
    yield put(
      playersActions.fetchPlayersListSuccess({
        data: options.data.data,
        skip: payload.payload.skip
      })
    );
  } catch (err) {
    yield put(playersActions.fetchPlayersListFailure(err));
  }
}

export function* getFeaturedPlayers(payload) {
  const config = {
    ...API_GET_FEATURED_PLAYERS_LIST,
    params: {
      ...API_GET_FEATURED_PLAYERS_LIST.params,
      ...payload.payload
    }
  };
  try {
    // const options = yield call(request, API_GET_FEATURED_PLAYERS_LIST);
    const options = yield call(request, config);
    // yield put(playersActions.fetchPlayersListSuccess(options.data.data));
    yield put(playersActions.fetchFeaturedPlayersSuccess(options.data));
  } catch (err) {
    yield put(playersActions.fetchFeaturedPlayersFailure(err));
  }
}

export function* getPlayerDetails(payload) {
  try {
    const options = yield call(
      request,
      API_GET_PLAYER_DETAILS(payload.payload.playerId)
    );
    yield put(playersActions.fetchPlayerDetailsSuccess(options.data.data));
  } catch (err) {
    yield put(playersActions.fetchPlayerDetailsFailure(err));
  }
}

export function* getPlayerMoments(payload) {
  const url = API_GET_PLAYER_MOMENTS(payload.payload.playerId);
  const config = {
    ...url,
    params: {
      ...url.params,
      categoryId: payload.payload.playerId
    }
  };
  try {
    const options = yield call(request, config);
    yield put(playersActions.fetchPlayerMomentsSuccess(options.data.data));
  } catch (err) {
    yield put(playersActions.fetchPlayerMomentsFailure(err));
  }
}

export function* getPlayerNews(payload) {
  const url = API_PLAYER_NEWS(payload.payload.playerId);
  const config = {
    ...url,
    params: {
      ...url.params,
      ...payload.payload
    }
  };
  try {
    const options = yield call(request, config);
    yield put(
      playersActions.fetchPlayerNewsSuccess({
        news: options.data.data,
        host: options.data.host,
        skip: payload.payload.skip
      })
    );
  } catch (err) {
    yield put(playersActions.fetchPlayerNewsFailure(err));
  }
}

export default function* defaultSaga() {
  yield takeLatest(playersConstants.PLAYERS_LIST, getPlayersList);
  yield takeLatest(playersConstants.PLAYERS_DETAILS, getPlayerDetails);
  yield takeLatest(playersConstants.PLAYER_MOMENTS, getPlayerMoments);
  yield takeLatest(playersConstants.PLAYER_NEWS, getPlayerNews);
  yield takeLatest(playersConstants.FEATURED_PLAYER, getFeaturedPlayers);
}
