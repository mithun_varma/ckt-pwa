import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the players state domain
 */

// const selectPlayersDomain = state => state.get('players', initialState);
const selectPlayersDomain = state => state.players || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Players
 */

const makeSelectPlayers = () => createSelector(selectPlayersDomain, substate => substate);

export default makeSelectPlayers;
export { selectPlayersDomain };
