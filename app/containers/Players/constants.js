/*
 *
 * Players constants
 *
 */

// export const DEFAULT_ACTION = 'app/Players/DEFAULT_ACTION';
export const PLAYERS_LIST = "app/Players/PLAYERS_LIST";
export const PLAYERS_LIST_SUCCESS = "app/Players/PLAYERS_LIST_SUCCESS";
export const PLAYERS_LIST_FAILURE = "app/Players/PLAYERS_LIST_FAILURE";

export const PLAYERS_DETAILS = "app/Players/PLAYERS_DETAILS";
export const PLAYERS_DETAILS_SUCCESS = "app/Players/PLAYERS_DETAILS_SUCCESS";
export const PLAYERS_DETAILS_FAILURE = "app/Players/PLAYERS_DETAILS_FAILURE";

export const PLAYER_MOMENTS = "app/Players/PLAYER_MOMENTS";
export const PLAYER_MOMENTS_SUCCESS = "app/Players/PLAYER_MOMENTS_SUCCESS";
export const PLAYER_MOMENTS_FAILURE = "app/Players/PLAYER_MOMENTS_FAILURE";

export const PLAYER_NEWS = "app/Players/PLAYER_NEWS";
export const PLAYER_NEWS_SUCCESS = "app/Players/PLAYER_NEWS_SUCCESS";
export const PLAYER_NEWS_FAILURE = "app/Players/PLAYER_NEWS_FAILURE";

export const FEATURED_PLAYER = "app/Players/FEATURED_PLAYER";
export const FEATURED_PLAYER_SUCCESS = "app/Players/FEATURED_PLAYER_SUCCESS";
export const FEATURED_PLAYER_FAILURE = "app/Players/FEATURED_PLAYER_FAILURE";
