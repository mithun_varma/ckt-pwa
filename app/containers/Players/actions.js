/*
 *
 * Players actions
 *
 */
import { showToast } from "utils/misc";

import {
  PLAYERS_LIST,
  PLAYERS_LIST_SUCCESS,
  PLAYERS_LIST_FAILURE,
  PLAYERS_DETAILS,
  PLAYERS_DETAILS_SUCCESS,
  PLAYERS_DETAILS_FAILURE,
  PLAYER_NEWS,
  PLAYER_NEWS_SUCCESS,
  PLAYER_NEWS_FAILURE,
  FEATURED_PLAYER,
  FEATURED_PLAYER_SUCCESS,
  FEATURED_PLAYER_FAILURE,
  PLAYER_MOMENTS,
  PLAYER_MOMENTS_FAILURE,
  PLAYER_MOMENTS_SUCCESS
} from "./constants";
const ACTIVE_TOAST = {};

/* Players List actions */
export const fetchPlayersList = payload => ({
  type: PLAYERS_LIST,
  payload
});

export const fetchPlayersListSuccess = payload => ({
  type: PLAYERS_LIST_SUCCESS,
  payload
});

export const fetchPlayersListFailure = err => {
  showToast(ACTIVE_TOAST, "fetchPlayersList", "Unable to get players list");
  return {
    type: PLAYERS_LIST_FAILURE,
    payload: err
  };
};

/* Featured Players List actions */
export const fetchFeaturedPlayers = payload => ({
  type: FEATURED_PLAYER,
  payload
});

export const fetchFeaturedPlayersSuccess = payload => ({
  type: FEATURED_PLAYER_SUCCESS,
  payload
});

export const fetchFeaturedPlayersFailure = err => {
  showToast(ACTIVE_TOAST, "fetchFeaturedPlayers", "Unable to get players list");
  return {
    type: FEATURED_PLAYER_FAILURE,
    payload: err
  };
};

/* Players Details actions */
export const fetchPlayerDetails = payload => ({
  type: PLAYERS_DETAILS,
  payload
});

export const fetchPlayerDetailsSuccess = payload => ({
  type: PLAYERS_DETAILS_SUCCESS,
  payload
});

export const fetchPlayerDetailsFailure = err => {
  showToast(
    ACTIVE_TOAST,
    "fetchPlayerDetails",
    "Unable to get players details"
  );
  return {
    type: PLAYERS_DETAILS_FAILURE,
    payload: err
  };
};

/* Players Moments actions */
export const fetchPlayerMoments = payload => ({
  type: PLAYER_MOMENTS,
  payload
});

export const fetchPlayerMomentsSuccess = payload => ({
  type: PLAYER_MOMENTS_SUCCESS,
  payload
});

export const fetchPlayerMomentsFailure = err => {
  return {
    type: PLAYER_MOMENTS_FAILURE,
    payload: err
  };
};

/* Player News actions */
export const fetchPlayerNews = payload => ({
  type: PLAYER_NEWS,
  payload
});

export const fetchPlayerNewsSuccess = payload => ({
  type: PLAYER_NEWS_SUCCESS,
  payload
});

export const fetchPlayerNewsFailure = err => {
  showToast(ACTIVE_TOAST, "fetchPlayerNews", "Unable to get player news");
  return {
    type: PLAYER_NEWS_FAILURE,
    payload: err
  };
};
