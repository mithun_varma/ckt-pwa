import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import Slider from "react-slick";
import map from "lodash/map";
import isEmpty from "lodash/isEmpty";
import { Helmet } from "react-helmet";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import DeviceUUID from "device-uuid";
import Cookies from "universal-cookie";
import FanPoll from "components-v2/commonComponent/FanPoll";
import Loader from "components-v2/commonComponent/Loader";
import EmptyState from "components-v2/commonComponent/EmptyState";
import FeatureArticleCard from "components-v2/Articles/FeatureArticleCard";
import ArticleCard from "components-v2/Articles/ArticleCard";
// import ButtonBar from "components-v2/commonComponent/ButtonBar";
import HrLine from "components-v2/commonComponent/HrLine";
import CardGradientTitle from "components-v2/commonComponent/CardGradientTitle";
import {
  white,
  cardShadow,
  red_Orange,
  gradientRedNew,
  gradientGrey,
  grey_10
} from "../../styleSheet/globalStyle/color";
import ChevronRight from "@material-ui/icons/ChevronRight";
import ImageScollerCard from "../../components-v2/commonComponent/ImageScollerCard";
import VideoCard from "../../components-v2/Articles/VideoCard";
import PointsTable from "../../components-v2/Worldcup/PointsTable";
import BottomBar from "../../components-v2/commonComponent/BottomBar";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";

import makeSelectArticles from "../Articles/selectors";
import articlesReducer from "../Articles/reducer";
import * as articlesActions from "../Articles/actions";
import articlesSaga from "../Articles/saga";

import makeSelectSchedule from "../Schedule/selectors";
import scheduleReducer from "../Schedule/reducer";
import * as scheduleActions from "../Schedule/actions";
import scheduleSaga from "../Schedule/saga";

import makeSelectPlayers from "../Players/selectors";
import playersReducer from "../Players/reducer";
import * as playersActions from "../Players/actions";
import playersSaga from "../Players/saga";

import makeSelectRecord from "../Record/selectors";
import recordReducer from "../Record/reducer";
import * as recordActions from "../Record/actions";
import recordSaga from "../Record/saga";

import makeSelectCriclytics from "../Criclytics/selectors";
import criclyticsReducer from "../Criclytics/reducer";
import * as criclyticsActions from "../Criclytics/actions";
import criclyticsSaga from "../Criclytics/saga";

import makeSelectScoreDetails from "../ScoreDetails/selectors";
import scoreDetailsReducer from "../ScoreDetails/reducer";
import * as scoreDetailsActions from "../ScoreDetails/actions";
import scoreDetailsSaga from "../ScoreDetails/saga";
import HomeScoreCard from "../../components-v2/HomeScore/HomeScoreCard";
import BackgroundComponent from "../../components-v2/commonComponent/BackgroundComponent";
import QuickBytes from "../../components-v2/commonComponent/QuickBytes";
import FactEngines from "../../components-v2/commonComponent/FactEngines";
import TeamScoreProjection from "../../components-v2/commonComponent/TeamScoreProjection";
import MomentumShift from "../../components-v2/Criclytics/MomentumShift";
import PlayerProjection from "../../components-v2/Criclytics/PlayerProjection";
import LivePlayerPrediction from "../../components-v2/Criclytics/LivePlayerProjection";
import TImeMachine from "../../components-v2/Criclytics/TImeMachine ";
import DeathOverSimulator from "../../components-v2/Criclytics/DeathOverSimulator";
// import ReactGA from "react-ga";
import CriclyticsArticles from "../../components-v2/commonComponent/CriclyticsArticles";
import LiveScorePredictor from "../../components-v2/Criclytics/NewLiveScorePredictor";
import { isAndroid } from "react-device-detect";
// import SplashScreen from "../../components-v2/commonComponent/SplashScreen";
// import {
//   initializeFirebase,
//   askForPermissioToReceiveNotifications,
//   subscribeToMatch,
//   unSubscribeToMatch
// } from "../../push-notification";
import FantasyBanner from "../../components-v2/commonComponent/FantasyBanner";
// import moment from "moment";
import AppDownload from "../../components-v2/commonComponent/AppDownload";
import HomeSchedule from "../../components-v2/commonComponent/HomeSchedule";
import ImageCards from "../../components-v2/Articles/ImageCards";
import PhotoView from "../../components-v2/screenComponent/playerProfile/PhotoView";
import CompletedWC from "../../components-v2/Worldcup/CompletedWC";
import InningsBreak from "../../components-v2/Worldcup/InningsBreak";
import CriclyticsLiveHome from "../../components-v2/Criclytics/CriclyticsLiveHome";
import CriclyticsScheduleHome from "../../components-v2/Criclytics/CriclyticsScheduleHome";

// import moment = require("moment");

let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  // ClevertapReact.initialize("TEST-Z88-4KR-845Z"); // Old Key
  ClevertapReact.initialize("W88-4KR-845Z");
  // initializeFirebase();
  // askForPermissioToReceiveNotifications();
}

// if (typeof window !== "undefined") {
//   navigator.serviceWorker
//     .register("../../utils/service-worker.js")
//     .then(function(registration) {
//       console.log("Registration successful, scope is:", registration.scope);
//     })
//     .catch(function(err) {
//       console.log("Service worker registration failed, error:", err);
//     });
// }

const cookies = new Cookies();
/* eslint-disable react/prefer-stateless-function */
export class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      launchFirstTime: true,
      matchIds: [],
      isFade: false,
      factEngine: "",
      quickByte: "",
      activeClass: "fact-active",
      type: null,
      cricData: null,
      currentMatchData: null,
      currentIndex: 0,
      showPhoto: false,
      initialSlide: 0,
      phaseCalled: false,
      summaryCalled: false
    };

    this.slider = React.createRef();
    this.settings = {
      dots: false,
      infinite: true,
      speed: 500,
      autoPlay: false,
      autoPlaySpeed: 500,
      // centerMode: true,
      centerPadding: "10px",
      useTransform: true,
      slidesToShow: 1.12,
      slidesToScroll: 1,
      cssEase: "ease-out",
      className: "homeScoreCard-slider",
      // linear: true,
      arrows: false
    };
    this.cardOnClick = this.cardOnClick.bind(this);
  }

  static getDerivedStateFromProps(nextProps, state) {
    if (state.launchFirstTime) {
      if (nextProps.scoreDetails.scoreCardIds.length > 0) {
        const id =
          nextProps.scoreDetails.scoreCardIds[
            nextProps.record.cardIndex ? nextProps.record.cardIndex : 0
          ];
        nextProps.getHomeCricData({
          matchId: id,
          stopPolling: false
        });
        if (nextProps.scoreDetails.scoreCards[id].status === "COMPLETED") {
          nextProps.getScoreSummary({
            matchId: id,
            format: nextProps.scoreDetails.scoreCards[id].format
          });
          // nextProps.getInningsPhase({
          //   matchId: id,
          //   inningId: 1
          // });
          nextProps.fetchHighlight({
            matchId: id,
            limit: 1,
            filters: ["live"]
          });
        }
        if (nextProps.scoreDetails.scoreCards[id].status === "RUNNING") {
          nextProps.getRecentScore({
            matchId: id
          });
        }
        if (
          nextProps.scoreDetails.scoreCards[id].status === "RUNNING" &&
          nextProps.criclytics.homeCricData &&
          nextProps.criclytics.homeCricData.type &&
          nextProps.criclytics.homeCricData.type === "liveScorePredictor"
        ) {
          nextProps.scoreDetails.scoreCards[id].format === "TEST"
            ? nextProps.getInningsPhase({
                matchId: id,
                day: nextProps.scoreDetails.scoreCards[id].day
              })
            : nextProps.getInningsPhase({
                matchId: id,
                inningId: 1
              });
        }
        return {
          factEngine: nextProps.scoreDetails.scoreCards[id].factEngine,
          quickByte: nextProps.scoreDetails.scoreCards[id].quickByte,
          currentMatchId: id,
          currentMatchData: nextProps.scoreDetails.scoreCards[id],
          launchFirstTime: false,
          pollResultId: cookies.get("resultIds")
        };
      }
    }
    if (!state.launchFirstTime) {
      if (
        state.currentMatchData.status !== "RUNNING" &&
        !nextProps.criclytics.homeCricLoading
      ) {
        nextProps.getHomeCricData({
          stopPolling: true
        });
      }
      if (
        state.currentMatchData.status === "RUNNING" &&
        nextProps.criclytics.homeCricData &&
        nextProps.criclytics.homeCricData.type &&
        nextProps.criclytics.homeCricData.type === "liveScorePredictor" &&
        !state.phaseCalled
      ) {
        state.currentMatchData.format === "TEST"
          ? nextProps.getInningsPhase({
              matchId: state.currentMatchId,
              day: state.currentMatchData.day
            })
          : nextProps.getInningsPhase({
              matchId: state.currentMatchId,
              inningId: 1
            });
        return {
          ...state,
          phaseCalled: true,
          pollResultId: cookies.get("resultIds")
        };
      }
      if (
        state.currentMatchData.status === "COMPLETED" &&
        !state.summaryCalled
      ) {
        nextProps.getScoreSummary({
          matchId: state.currentMatchId,
          format: state.currentMatchData.format
        });
        nextProps.fetchHighlight({
          matchId: state.currentMatchId,
          limit: 1,
          filters: ["live"]
        });
        return {
          ...state,
          summaryCalled: true
        };
      }
      return {
        ...state,
        pollResultId: cookies.get("resultIds")
      };
    }
    return null;
  }

  handleSlideChange = nextId => {
    const id = this.props.scoreDetails.scoreCardIds[nextId];
    this.props.getHomeCricData({
      stopPolling: true
    });
    this.props.stopRecentRunPoll();
    this.props.getHomeCricData({
      matchId: id,
      stopPolling: false
    });
    if (
      this.props.scoreDetails.scoreCards[id] &&
      this.props.scoreDetails.scoreCards[id].status === "COMPLETED"
    ) {
      this.props.getScoreSummary({
        matchId: id,
        format: this.props.scoreDetails.scoreCards[id].format
      });
      this.props.fetchHighlight({
        matchId: id,
        limit: 1,
        filters: ["live"]
      });
    }
    if (
      this.props.scoreDetails.scoreCards[id] &&
      this.props.scoreDetails.scoreCards[id].status === "RUNNING"
    ) {
      this.props.getRecentScore({
        matchId: id
      });
    }
    this.setState(
      {
        summaryCalled: false,
        phaseCalled: false,
        quickByte:
          this.props.scoreDetails &&
          this.props.scoreDetails.scoreCards &&
          this.props.scoreDetails.scoreCards[id] &&
          this.props.scoreDetails.scoreCards[id].quickByte,
        factEngine:
          this.props.scoreDetails &&
          this.props.scoreDetails.scoreCards &&
          this.props.scoreDetails.scoreCards[id] &&
          this.props.scoreDetails.scoreCards[id].factEngine,
        activeClass: "fact-active",
        currentMatchId: id,
        currentIndex: nextId,
        currentMatchData:
          this.props.scoreDetails &&
          this.props.scoreDetails.scoreCards &&
          this.props.scoreDetails.scoreCards[id] &&
          this.props.scoreDetails.scoreCards[id]
      },
      () => {
        // this.props.getHomeCricData({
        //   matchId: id,
        //   stopPolling: false
        // });
        // if (
        //   this.props.scoreDetails.scoreCards[id] &&
        //   this.props.scoreDetails.scoreCards[id].status === "COMPLETED"
        // ) {
        //   this.props.getScoreSummary({
        //     matchId: id
        //   });
        //   this.props.fetchHighlight({
        //     matchId: id
        //   });
        // }
        // if (
        //   this.props.scoreDetails.scoreCards[id] &&
        //   this.props.scoreDetails.scoreCards[id].status === "RUNNING"
        // ) {
        //   this.props.getRecentScore({
        //     matchId: id
        //   });
        // }
      }
    );
  };

  componentDidMount() {
    // ReactGA.initialize("UA-136526387-1");
    // ReactGA.pageview(
    //   "https://www.cricket.com" +
    //     window.location.pathname +
    //     window.location.search
    // );
    ClevertapReact.event("home");
    this.props.pollFeaturedScores();
    this.props.fetchHomePoll();
    // console.log(this.props.scoreDetails.scoreCardIds[0] && this.props.scoreDetails.scoreCardIds[0])
    // this.props.getHomeCricData({
    //   matchId: this.props.scoreDetails.scoreCardIds[0],
    //   stopPolling: false
    // });
    const lastPollId = cookies.get("resultIds");
    if (lastPollId && lastPollId.constructor !== Array) {
      this.props.getPollById({
        pollId: lastPollId
      });
    }
    this.props.fetchPointsTable({ seriesId: "opta:1" });

    // this.props.getRecentScore({
    //   matchId: "2019174-4-england-vs-pakistan"
    // });

    this.props.fetchSeriesSchedule({
      seriesId: "opta:1",
      skip: 0,
      limit: 3
    });

    this.props.fetchSeriesNews({
      seriesId: "opta:1"
    });
    this.props.fetchFantasyAnalysis({
      limit: 3,
      skip: 0
    });
    this.props.fetchNewsOpinion({
      limit: 10,
      skip: 0
    });
    this.props.fetchSuggestion({
      limit: 3,
      skip: 0
    });
    this.props.fetchPreAnalysis({
      limit: 5,
      skip: 0
    });
    this.props.fetchFeaturedPlayers();
    this.props.fetchImages({
      skip: 0,
      limit: 10
    });
    this.props.fetchFeaturedVideos({
      filters: ["featured"]
    });
  }

  componentWillUnmount() {
    this.props.pollFeaturedScores(true);
    this.props.getHomeCricData({ stopPolling: true });
    this.props.stopRecentRunPoll();
  }

  cardOnClick(path) {
    this.props.history.push(path);
  }

  handleValidateArticlesLength = key => {
    if (this.props.articles[key].length > 0) {
      return true;
    }
    return false;
  };

  redirectTo = path => {
    this.props.saveCardPosition(this.state.currentIndex);
    this.props.history.push(path);
  };

  handleNotification = matchId => {
    let a = [...this.state.matchIds];
    if (a.indexOf(matchId) === -1) {
      // subscribeToMatch(matchId);
      a.push(matchId);
    } else {
      // unSubscribeToMatch(matchId);
      a.splice(a.indexOf(matchId), 1);
    }
    this.setState({
      matchIds: a
    });
  };

  handleScoreCardRedirect = id => {
    const index = this.props.scoreDetails.scoreCardIds.indexOf(id);
    this.props.saveCardPosition(index);
    this.props.history.push(`/score/${id}`);
  };

  togglePhotoView = key => {
    this.setState({
      showPhoto: true,
      initialSlide: key
    });
  };

  handleImageClick = val => {
    this.setState({
      showPhoto: false
    });
  };

  handlePollPost = (id, option) => {
    this.props.postPollOption({
      param: {
        id,
        option,
        uniqueId: DeviceUUID.DeviceUUID().get()
      },
      type: "homescreen"
    });
  };

  handleFantasy = () => {
    this.props.saveCardPosition(this.state.currentIndex);
    this.props.history.push(`/score/${this.state.currentMatchId}/fantasy`);
  };

  render() {
    const { articles, players, scoreDetails, record, history } = this.props;
    // const lastPollId = cookies.get("resultIds");
    const selectedOption = cookies.get("selectedOption");

    return this.state.showPhoto ? (
      <PhotoView
        toggleView={val => this.handleImageClick(val)}
        initialSlide={this.state.initialSlide}
        title="Data Digest"
        data={articles.homeImages}
      />
    ) : (
      <React.Fragment>
        <Helmet titleTemplate="%s | Cricket.com">
          <title>
            Cricket News | Live Scores | Cricket Schedule | Match Predictions |
            Cricket.com
          </title>
          <meta
            name="description"
            content="Get the latest cricket updates, fixtures, scorecard and match analysis. Watch ball by ball commentary and get match predictions from cricket.com."
          />
          <meta
            name="keywords"
            content="cricket news, live cricket score, cricket, latest cricket scores, todays match"
          />
          <link
            rel="canonical"
            href={`https://www.cricket.com${
              this.props.history.location.pathname
            }`}
          />
        </Helmet>
        <div
          style={{
            display: "flex",
            flex: 1,
            flexDirection: "column",
            paddingBottom: 60
          }}
        >
          <BackgroundComponent
            // title={"headerTitle"}
            logo
            rootStyles={{
              height: 56,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              background: white,
              boxShadow: "0px 3px 3px rgba(0,0,0,0.12)",
              zIndex: 222
            }}
          />
          <div
            style={{
              width: "100%",
              marginLeft: "auto",
              background: gradientRedNew,
              height: "256px"
            }}
          >
            {scoreDetails.scoreCardLoading ? (
              <div
                style={{
                  width: "90%",
                  height: "200px",
                  margin: "16px auto",
                  background: "#fff",
                  textAlign: "center"
                }}
              >
                <img
                  style={{ width: 100, marginTop: 55 }}
                  src={require("../../images/CricketLoader.gif")}
                />
              </div>
            ) : isEmpty(scoreDetails.scoreCards) ? (
              <div style={{ background: "#fff", margin: 16 }}>
                <EmptyState
                  msg="no scorecards found"
                  rootStyles={{ height: 200 }}
                />
              </div>
            ) : (
              <Slider
                {...this.settings}
                initialSlide={
                  this.props.record.cardIndex ? this.props.record.cardIndex : 0
                }
                beforeChange={(current, next) => this.handleSlideChange(next)}
              >
                {scoreDetails &&
                  scoreDetails.scoreCards &&
                  map(
                    scoreDetails.scoreCards,
                    (match, index) =>
                      match.status && (
                        <HomeScoreCard
                          handleScoreCard={() =>
                            this.handleScoreCardRedirect(match.matchId)
                          }
                          key={index}
                          score={match}
                          handleNotification={
                            () => this.handleNotification(match.matchId)
                            // console.log("")
                          }
                          notificationActive={
                            this.state.matchIds.indexOf(match.matchId) !== -1
                          }
                        />
                      )
                  )}
              </Slider>
            )}
          </div>
          {/* worldcup criclytics */}
          <div style={{ marginTop: "-16px" }}>
            <div
              style={{
                margin: "0 16px 12px",
                fontFamily: "Montserrat",
                fontSize: 14,
                color: grey_10,
                fontWeight: 500,
                background: white,
                boxShadow: cardShadow,
                borderRadius: 3
              }}
            >
              {this.props.criclytics.homeCricLoading ? (
                <div
                  style={{
                    // width: "90%",
                    // padding:'16px',
                    minHeight: 400,
                    background: "#fff",
                    margin: "16px",
                    marginTop: 0,
                    position: "relative"
                  }}
                >
                  <div
                    style={{
                      position: "absolute",
                      width: 0,
                      height: 0,
                      borderLeft: "14px solid transparent",
                      borderRight: "14px solid transparent",
                      borderBottom: "14px solid #fff",
                      top: -13,
                      left: 0
                    }}
                  />
                  <img
                    src={require("../../images/CricketLoader.gif")}
                    style={{
                      width: "100px",
                      marginLeft: "30%",
                      marginTop: "35%",
                      // height: 200,
                      background: "#fff"
                    }}
                  />
                </div>
              ) : isEmpty(this.props.criclytics.homeCricData) ? (
                <div>{/* <EmptyState msg="no criclytics found" /> */}</div>
              ) : (
                <React.Fragment>
                  {this.props.criclytics.homeCricData.type &&
                    this.props.criclytics.homeCricData.type ===
                      "battingAndBowlingProjectionsLive" && (
                      <CriclyticsLiveHome
                        history={history}
                        criclyticsData={this.props.criclytics.homeCricData}
                        currentScoreData={
                          scoreDetails.scoreCards[this.state.currentMatchId]
                        }
                        redirecToCriclyticsSlider={() => {
                          this.props.saveCardPosition(this.state.currentIndex);
                          this.props.history.push(
                            `/criclytics-slider/${this.state.currentMatchId}`
                          );
                        }}
                        redirecToScoreDetails={() => {
                          this.props.saveCardPosition(this.state.currentIndex);
                          this.props.history.push(
                            `/score/${this.state.currentMatchId}`
                          );
                        }}
                        last10balls={this.props.scoreDetails.last10Balls}
                      />
                      // <InningsBreak
                      //   history={history}
                      //   isTest={
                      //     scoreDetails.scoreCards[this.state.currentMatchId]
                      //       .format === "TEST"
                      //   }
                      //   redirecToScoreDetails={() => {
                      //     this.props.saveCardPosition(this.state.currentIndex);
                      //     this.props.history.push(
                      //       `/score/${this.state.currentMatchId}`
                      //     );
                      //   }}
                      //   redirecToCriclyticsSlider={() => {
                      //     this.props.saveCardPosition(this.state.currentIndex);
                      //     this.props.history.push(
                      //       `/criclytics-slider/${this.state.currentMatchId}`
                      //     );
                      //   }}
                      //   currentScoreData={
                      //     scoreDetails.scoreCards[this.state.currentMatchId]
                      //   }
                      //   inningsData={this.props.scoreDetails.inningsPhase}
                      //   criclyticsData={this.props.criclytics.homeCricData}
                      // />
                    )}
                  {this.props.criclytics.homeCricData.type &&
                    this.props.criclytics.homeCricData.type ===
                      "preMatchPlayerProbsFinal" && (
                      <CriclyticsScheduleHome
                        history={history}
                        redirecToCriclyticsSlider={() => {
                          this.props.saveCardPosition(this.state.currentIndex);
                          this.props.history.push(
                            `/criclytics-slider/${this.state.currentMatchId}`
                          );
                        }}
                        criclyticsData={this.props.criclytics.homeCricData}
                        currentScoreData={
                          scoreDetails.scoreCards[this.state.currentMatchId]
                        }
                        handleFantasy={this.handleFantasy}
                      />
                    )}
                  {this.props.criclytics.homeCricData.type &&
                    this.props.criclytics.homeCricData.type ===
                      "liveScorePredictor" &&
                    this.props.scoreDetails.inningsPhase && (
                      <InningsBreak
                        history={history}
                        isTest={
                          scoreDetails.scoreCards[this.state.currentMatchId]
                            .format === "TEST"
                        }
                        redirecToScoreDetails={() => {
                          this.props.saveCardPosition(this.state.currentIndex);
                          this.props.history.push(
                            `/score/${this.state.currentMatchId}`
                          );
                        }}
                        redirecToCriclyticsSlider={() => {
                          this.props.saveCardPosition(this.state.currentIndex);
                          this.props.history.push(
                            `/criclytics-slider/${this.state.currentMatchId}`
                          );
                        }}
                        currentScoreData={
                          scoreDetails.scoreCards[this.state.currentMatchId]
                        }
                        inningsData={this.props.scoreDetails.inningsPhase}
                        criclyticsData={this.props.criclytics.homeCricData}
                      />
                    )}
                  {this.state.currentMatchData.status === "COMPLETED" ||
                  (this.props.criclytics.homeCricData.type &&
                    (this.props.criclytics.homeCricData.type ===
                      "topFourMomentumShiftPoints" ||
                      this.props.criclytics.homeCricData.type === "articles") &&
                    this.props.scoreDetails.scoreSummary &&
                    !this.props.scoreDetails.scoreSummary.abondoned) ? (
                    <CompletedWC
                      videoRedirect={this.redirectTo}
                      loading={this.props.scoreDetails.summaryLoading}
                      scoreData={this.props.scoreDetails.scoreSummary}
                      criclyticsData={this.props.criclytics.homeCricData}
                      history={this.props.history}
                      highlight={this.props.articles.highlight}
                      matchName={this.state.currentMatchData.matchShortName}
                      handleSummaryRedirect={() => {
                        this.props.saveCardPosition(this.state.currentIndex);
                        this.props.history.push(
                          `/score/${this.state.currentMatchId}`
                        );
                      }}
                    />
                  ) : (
                    <div />
                  )}
                </React.Fragment>
              )}
              {/* <InningsBreak scorecard={this.state.currentMatchData} /> */}
            </div>
          </div>
          {/* Worldcup schedule starts */}
          {false && (
            <div style={{ marginTop: "-20px" }}>
              <div>
                {this.props.criclytics &&
                  this.props.criclytics.homeCricData &&
                  !this.props.criclytics.homeCricData
                    .preMatchTeamsProjection && (
                    <FactEngines
                      activeClass={this.state.activeClass}
                      data={this.state.factEngine}
                    />
                  )}
                {/* {false && (
                <CriclyticsArticles
                  history={history}
                  articles={articles.newsOpinions}
                />
              )} */}
                {this.props.criclytics &&
                  this.props.criclytics.homeCricData &&
                  this.props.criclytics.homeCricData
                    .preMatchTeamsProjection && (
                    <TeamScoreProjection
                      redirectTo={() => {
                        this.props.saveCardPosition(this.state.currentIndex);
                        this.props.history.push(
                          `/criclytics-slider/${this.state.currentMatchId}`
                        );
                      }}
                      scoreProjection={
                        this.props.criclytics &&
                        this.props.criclytics.homeCricData &&
                        this.props.criclytics.homeCricData
                          .preMatchTeamsProjection
                          ? this.props.criclytics.homeCricData
                              .preMatchTeamsProjection
                          : {}
                      }
                    />
                  )}
                {this.props.criclytics.homeCricLoading ? (
                  <div
                    style={{
                      // width: "90%",
                      // padding:'16px',
                      minHeight: 400,
                      background: "#fff",
                      margin: "16px"
                    }}
                  >
                    <img
                      src={require("../../images/CricketLoader.gif")}
                      style={{
                        width: "100px",
                        marginLeft: "30%",
                        marginTop: "35%",
                        // height: 200,
                        background: "#fff"
                      }}
                    />
                  </div>
                ) : (
                  <React.Fragment>
                    {this.props.criclytics.homeCricData.type &&
                      this.props.criclytics.homeCricData.type ===
                        "topFourMomentumShiftPoints" &&
                      this.props.criclytics.homeCricData[
                        "topFourMomentumShiftPoints"
                      ] && (
                        <div style={{ padding: "0 16px" }}>
                          <MomentumShift
                            ballByBallData={this.props.criclytics.ballByBall}
                            ballByBall={this.props.fetchBallByBall}
                            OverData={
                              this.props.criclytics.overSeparatorMomentum
                            }
                            data={
                              this.props.criclytics.homeCricData[
                                "topFourMomentumShiftPoints"
                              ]
                            }
                            isHide
                            loading={this.props.criclytics.homeCricLoading}
                            homeStyle={{ background: gradientGrey }}
                            currentMatch={
                              this.state.currentMatchData
                                ? this.state.currentMatchData
                                : {}
                            }
                            headerTextStyle={{
                              fontSize: 11
                            }}
                            switchStyle={{ padding: "5px 16px" }}
                            fetchOverSeperator={this.props.fetchOverSeperator}
                          />
                        </div>
                      )}
                    {this.props.criclytics.homeCricData.type &&
                      this.props.criclytics.homeCricData.type === "articles" &&
                      this.props.criclytics.homeCricData["articles"] && (
                        <CriclyticsArticles
                          history={history}
                          articles={
                            this.props.criclytics.homeCricData["articles"]
                          }
                        />
                      )}
                    {this.props.criclytics.homeCricData.type &&
                      this.props.criclytics.homeCricData.type ===
                        "preMatchPlayerProbsFinal" &&
                      this.props.criclytics.homeCricData[
                        "preMatchPlayerProbsFinal"
                      ] && (
                        <div style={{ padding: "0 16px" }}>
                          <PlayerProjection
                            data={
                              this.props.criclytics.homeCricData[
                                "preMatchPlayerProbsFinal"
                              ]
                            }
                            currentMatch={
                              this.state.currentMatchData
                                ? this.state.currentMatchData
                                : {}
                            }
                            homeStyle={{ background: gradientGrey }}
                            loading={this.props.criclytics.homeCricLoading}
                            isHide
                            isHideFanFightBanner
                          />
                        </div>
                      )}
                    {this.props.criclytics.homeCricData.type &&
                      this.props.criclytics.homeCricData.type ===
                        "battingAndBowlingProjectionsLive" &&
                      this.props.criclytics.homeCricData[
                        "battingAndBowlingProjectionsLive"
                      ] && (
                        <div style={{ padding: "0 16px" }}>
                          <CardGradientTitle
                            title="Live Player Projections"
                            titleStyles={{
                              textTransform: "uppercase",
                              color: "#727682",
                              fontSize: 11
                            }}
                          />
                          <LivePlayerPrediction
                            data={
                              this.props.criclytics.homeCricData[
                                "battingAndBowlingProjectionsLive"
                              ].battingProbabilitiesWrapper
                            }
                            bowlingData={
                              this.props.criclytics.homeCricData[
                                "battingAndBowlingProjectionsLive"
                              ].liveBowlingProjections
                            }
                            loading={this.props.criclytics.homeCricLoading}
                            homeStyle={{ background: gradientGrey }}
                            isHide
                          />
                        </div>
                      )}
                    {this.props.criclytics.homeCricData.type &&
                      this.props.criclytics.homeCricData.type ===
                        "similarMatches" &&
                      this.props.criclytics.homeCricData["similarMatches"] && (
                        <div style={{ padding: "0 16px" }}>
                          <CardGradientTitle
                            title="Time Machine"
                            titleStyles={{
                              textTransform: "uppercase",
                              color: "#727682",
                              fontSize: 11
                            }}
                          />
                          <TImeMachine
                            currentMatch={
                              this.state.currentMatchData
                                ? this.state.currentMatchData
                                : {}
                            }
                            data={
                              this.props.criclytics.homeCricData[
                                "similarMatches"
                              ]
                            }
                            isHide
                          />
                        </div>
                      )}
                    {this.props.criclytics.homeCricData.type &&
                      this.props.criclytics.homeCricData.type ===
                        "liveScorePredictor" &&
                      this.props.criclytics.homeCricData[
                        "liveScorePredictor"
                      ] && (
                        <div style={{ padding: "0 16px" }}>
                          <CardGradientTitle
                            title="Live Score Predictor"
                            titleStyles={{
                              textTransform: "uppercase",
                              color: "#727682",
                              fontSize: 11
                            }}
                          />
                          <LiveScorePredictor
                            currentMatch={
                              this.state.currentMatchData
                                ? this.state.currentMatchData
                                : {}
                            }
                            data={
                              this.props.criclytics.homeCricData[
                                "liveScorePredictor"
                              ]
                            }
                            isHide
                          />
                        </div>
                      )}
                    {this.props.criclytics.homeCricData.type &&
                      this.props.criclytics.homeCricData.type ===
                        "playerProb" &&
                      this.props.criclytics.homeCricData["playerProb"] && (
                        <div style={{ padding: "0 16px" }}>
                          <DeathOverSimulator
                            currentMatch={
                              this.state.currentMatchData
                                ? this.state.currentMatchData
                                : {}
                            }
                            postDeathBowlers={this.props.postDeathOversBowlers}
                            data={
                              this.props.criclytics.homeCricData["playerProb"]
                            }
                            isHide
                          />
                        </div>
                      )}
                    {this.props.criclytics.homeCricData.type &&
                      this.props.criclytics.homeCricData[
                        this.props.criclytics.homeCricData.type
                      ] && (
                        <div
                          style={{
                            background: "#fff",
                            display: "flex",
                            justifyContent: "flex-end",
                            margin: "0 16px",
                            flexDirection: "column"
                          }}
                        >
                          <HrLine />
                          <button
                            style={{
                              background: "#ffe8d9",
                              color: red_Orange,
                              margin: "8px 16px",
                              padding: "0 16px",
                              fontSize: "10px",
                              fontFamily: "mont400",
                              border: "none",
                              borderRadius: 18,
                              minWidth: 160,
                              display: "flex",
                              alignSelf: "flex-end",
                              alignItems: "center"
                            }}
                            onClick={() => {
                              ClevertapReact.event("Criclytics", {
                                source: "homeWordCup",
                                matchId: this.state.currentIndex
                              });
                              this.props.saveCardPosition(
                                this.state.currentIndex
                              );
                              this.props.history.push(
                                `/criclytics-slider/${
                                  this.state.currentMatchId
                                }`
                              );
                            }}
                          >
                            <img
                              src={require("../../images/shape-copy-3.svg")}
                              style={{ marginRight: 8 }}
                            />
                            <div
                              style={{
                                height: "26px",
                                width: "1px",
                                marginRight: "8px",
                                background: "#f9ddcb"
                              }}
                            />
                            view all criclytics
                          </button>
                        </div>
                      )}
                  </React.Fragment>
                )}
                {this.state.quickByte &&
                this.state.currentMatchData.status !== "COMPLETED" ? (
                  <QuickBytes
                    activeClass={this.state.activeClass}
                    data={this.state.quickByte}
                  />
                ) : (
                  <div style={{ marginTop: 16 }} />
                )}
                {this.state.currentMatchData &&
                this.state.currentMatchData.status === "UPCOMING" ? (
                  <FantasyBanner
                    data={this.state.currentMatchData}
                    history={this.props.history}
                  />
                ) : (
                  <div style={{ marginTop: 16 }} />
                )}
              </div>
            </div>
          )}
          {/* =================  START ===================== */}
          {/* ODI World Cup 2019 Should be Removed After World Cup */}
          {false && (
            <div
              style={{
                margin: "0px 16px 16px 16px",
                boxShadow: cardShadow
              }}
            >
              <div
                style={{
                  backgroundImage: `linear-gradient(to bottom, #e8842b, #f9b20c)`,
                  borderRadius: "2px 0px",
                  display: "flex",
                  justifyContent: "space-between",
                  padding: "10px 8px",
                  alignItems: "center"
                }}
                onClick={() => {
                  ClevertapReact.event("Series", {
                    source: "Series",
                    matchId: "opta:1"
                  });
                  this.props.history.push("/series/odi-world-cup/opta:1");
                }}
              >
                {/* <img
                style={{
                  verticalAlign: "middle",
                  width: "60%"
                }}
                src={require("../../images/wc-logo.svg")}
              /> */}
                <div
                  style={{
                    fontSize: 12,
                    color: white,
                    fontFamily: "mont500"
                  }}
                >
                  ODI World Cup 2019
                </div>
                {/* <img src={require("../../images/wc-logo-bg.svg")} /> */}
                <ChevronRight style={{ color: "#fff", alignSelf: "center" }} />
              </div>
              <CardGradientTitle
                title="WORLD CUP NEWS"
                // isRightIcon
                // handleCardClick={() => this.props.history.push(`/articles`)}
                rootStyles={{
                  color: "#727682",
                  fontSize: 10,
                  padding: "10px 8px"
                }}
              />
              <div>
                {articles.seriesNewsLoading ? (
                  <div style={{ padding: 10 }}>
                    <Loader styles={{ height: "120px" }} noOfLoaders={2} />
                  </div>
                ) : (
                  articles.seriesNews.slice(0, 2).map((news, key) => (
                    <div key={key}>
                      <ArticleCard
                        article={news}
                        articleHost={articles.articleHost}
                        cardType="pre-match-analysis"
                        key={`${key + 1}`}
                        cardOnClick={this.cardOnClick}
                        rootStyles={{ padding: "12px 16px", background: white }}
                      />
                      <HrLine />
                    </div>
                  ))
                )}
              </div>
              {this.props.schedule.seriesMatches &&
              this.props.schedule.seriesMatches.length > 0 ? (
                <div>
                  <HomeSchedule
                    history={history}
                    loading={this.props.schedule.seriesMatchesLoading}
                    matches={this.props.schedule.seriesMatches}
                  />
                </div>
              ) : (
                ""
              )}
            </div>
          )}
          {/* ODI World Cup 2019 Should be Removed After World Cup */}
          {/* =================  END ===================== */}
          {/* worldcup schedule ends */}

          {/* =================  START ===================== */}
          {/* World Cup Points Table Should be Removed After World Cup */}
          {/* Points Table */}
          {false && (
            <PointsTable
              loading={record.pointsTableLoading}
              pointsTable={record.pointsTable}
              history={history}
            />
          )}
          {/* World Cup Points Table Should be Removed After World Cup */}
          {/* =================  End ===================== */}

          {/* Section 1 Latest News */}
          <div
            style={{
              background: white,
              borderRadius: 3,
              boxShadow: cardShadow,
              margin: "0 16px 12px"
            }}
          >
            <CardGradientTitle
              title="PRE-MATCH FEATURES"
              isRightIcon
              handleCardClick={() =>
                this.props.history.push(`/articles?type=pre-match-analysis`)
              }
              rootStyles={{
                background: white,
                borderRadius: 3,
                fontSize: 12
              }}
            />
            {/* <CardGradientTitle title="Latest News" /> */}
            {articles.preanalysisLoading ? (
              <div style={{ padding: 10 }}>
                <Loader styles={{ height: "150px" }} noOfLoaders={1} />
              </div>
            ) : (
              articles.prematchAnalysis.length > 0 && (
                <div>
                  <FeatureArticleCard
                    article={articles.prematchAnalysis[0]}
                    articleHost={articles.articleHost}
                    cardType="pre-match-analysis"
                    // key={`${key + 1}`}
                    cardOnClick={this.cardOnClick}
                  />
                  <HrLine />
                </div>
              )
            )}
            {articles.preanalysisLoading ? (
              <div style={{ padding: 10 }}>
                <Loader styles={{ height: "120px" }} noOfLoaders={2} />
              </div>
            ) : (
              articles.prematchAnalysis.slice(1, 3).map((news, key) => (
                <div key={key}>
                  <ArticleCard
                    article={news}
                    articleHost={articles.articleHost}
                    cardType="pre-match-analysis"
                    key={`${key + 1}`}
                    cardOnClick={this.cardOnClick}
                  />
                  <HrLine />
                </div>
              ))
            )}
            {articles.prematchAnalysis.length == 0 && (
              <EmptyState msg="No Pre match analysis found" />
            )}
          </div>

          {/* Data Digest Section Starts */}
          <div
            style={{
              margin: "0 16px 10px 16px"
            }}
          >
            <ImageCards
              loading={this.props.articles.homeImagesLoading}
              images={this.props.articles.homeImages}
              togglePhoto={this.togglePhotoView}
              title="DATA DIGEST"
              history={history}
            />
          </div>
          {/* Data Digest Section Closing */}

          {/* News & Articles Section Starts */}
          {articles.newsOpinions.length > 0 ? (
            <div
              style={{
                background: white,
                borderRadius: 3,
                boxShadow: cardShadow,
                margin: "0 16px 12px"
              }}
            >
              <CardGradientTitle
                title="NEWS AND ARTICLES"
                isRightIcon
                handleCardClick={() =>
                  this.props.history.push(`/articles?type=news`)
                }
                rootStyles={{
                  background: white,
                  borderRadius: 3,
                  fontSize: 12
                }}
              />
              {/* <CardGradientTitle title="Latest News" /> */}
              {articles.newsOpinionLoading ? (
                <div style={{ padding: 10 }}>
                  <Loader styles={{ height: "150px" }} noOfLoaders={1} />
                </div>
              ) : (
                articles.newsOpinions.length > 0 && (
                  <div>
                    <FeatureArticleCard
                      article={articles.newsOpinions[0]}
                      articleHost={articles.articleHost}
                      cardType="news"
                      // key={`${key + 1}`}
                      cardOnClick={this.cardOnClick}
                    />
                    <HrLine />
                  </div>
                )
              )}
              {articles.newsOpinionLoading ? (
                <div style={{ padding: 10 }}>
                  <Loader styles={{ height: "120px" }} noOfLoaders={2} />
                </div>
              ) : (
                articles.newsOpinions.slice(1, 3).map((news, key) => (
                  <div key={key}>
                    <ArticleCard
                      article={news}
                      articleHost={articles.articleHost}
                      cardType="news"
                      key={`${key + 1}`}
                      cardOnClick={this.cardOnClick}
                    />
                    <HrLine />
                  </div>
                ))
              )}
              {articles.newsOpinions.length == 0 && (
                <EmptyState msg="No news and articles found" />
              )}
            </div>
          ) : (
            ""
          )}

          {/* News & Articles Section Closing */}

          {/* =================  START ===================== */}
          {/* News Bulletin Should be Removed After World Cup */}
          {false && (
            <div
              style={{
                background: white,
                borderRadius: 3,
                boxShadow: cardShadow,
                margin: "0 16px 12px"
              }}
            >
              <CardGradientTitle
                title="News Bulletin"
                isRightIcon
                handleCardClick={() =>
                  this.props.history.push(`/articles?type=news`)
                }
                rootStyles={{
                  background: white,
                  borderRadius: 3,
                  fontSize: 12
                }}
              />
              {articles.newsOpinionLoading ? (
                <div style={{ padding: 10 }}>
                  <Loader styles={{ height: "150px" }} noOfLoaders={1} />
                </div>
              ) : articles.newsOpinions.length > 0 ? (
                articles.newsOpinions.slice(0, 5).map((article, key) => (
                  <div key={key}>
                    <h3
                      style={{
                        padding: 16,
                        fontFamily: "mont500",
                        fontSize: 12,
                        display: "flex"
                      }}
                      onClick={() => {
                        ClevertapReact.event("Article", {
                          source: "BulletinHome",
                          matchId: article.matchIds[0],
                          teamId: article.teamIds[0],
                          seriesId: article.seriesIds[0],
                          playerId: article.playerIds[0],
                          articleType: article.type,
                          articleId: article._id || article.id
                        });
                        this.props.history.push(
                          `/articles/news/${
                            article.id
                              ? article.id
                              : // .toLowerCase()
                                // .replace(/ /g, "-")
                                // .replace(/[`~!@#$%^&*()_|+\=?;:'",.]/g, "")
                                article._id
                          }`
                        );
                      }}
                    >
                      <img
                        src={require("../../images/news-icon-inactive-copy.svg")}
                        style={{ marginRight: 8, maxHeight: 24 }}
                      />
                      {article.title}
                    </h3>
                    <HrLine />
                  </div>
                ))
              ) : (
                <EmptyState msg="No news found" />
              )}
            </div>
          )}
          {/* News Bulletin Should be Removed After World Cup */}
          {/* =================  START ===================== */}
          <div
            style={{
              margin: "0 16px 10px 16px"
            }}
          >
            <VideoCard
              loading={this.props.articles.featuredVideosLoading}
              videos={this.props.articles.featuredVideos}
              title="FEATURED VIDEOS"
              isRightIcon
              history={history}
            />
          </div>
          {record && record.pollLoading ? (
            <Loader styles={{ height: "150px" }} />
          ) : (
            !isEmpty(record.homePoll) &&
            record.homePoll.question && (
              // (!record.homePoll.endPoll ||
              //   moment(record.homePoll.endDate).diff(new moment()) > 0) && (
              <FanPoll
                pollData={record.homePoll}
                pollResult={
                  record.homePoll.endPoll
                    ? record.homePoll.pollsView || false // added false because data was not deployed that time and app was crashing
                    : record.pollResult
                }
                isResult={
                  +record.homePoll.pollId === +this.state.pollResultId ||
                  record.homePoll.endPoll
                }
                handlePoll={this.handlePollPost}
                loading={record.postPollLoading}
                selectedOption={selectedOption}
              />
            )
          )}
          {/* Data Digest Old Place */}
          {/* <div
            style={{
              margin: "0 16px 10px 16px"
            }}
          >
            <ImageCards
              loading={this.props.articles.homeImagesLoading}
              images={this.props.articles.homeImages}
              togglePhoto={this.togglePhotoView}
              title="Data Digest"
              history={history}
            />
          </div> */}
          {/* Featured Player */}
          <div
            style={{
              margin: "0 16px 0px 16px"
            }}
          >
            <ImageScollerCard
              featuredPlayers={
                players &&
                players.featuredPlayers &&
                players.featuredPlayers.players
              }
              isRightIcon
              history={history}
              title="TRENDING PLAYERS"
              hasViewAll={false}
            />
          </div>
          {/* Featured Player closing */}
          {/* Section of download App */}
          {isAndroid ? (
            <AppDownload
              url={
                "https://play.google.com/store/apps/details?id=com.crictec.cricket"
              }
              isAndroid
            />
          ) : (
            <AppDownload
              url={"https://itunes.apple.com/us/app/cricket-com/id1460360497"}
            />
          )}
          {/* Section of download App closing */}
        </div>
        <BottomBar
          {...this.props.history}
          tabs={[
            { key: "home", route: "/" },
            { key: "schedule", route: "schedule" },
            { key: "Criclytics", route: "criclytics" },
            { key: "news", route: "articles" },
            { key: "more", route: "more" }
          ]}
        />
      </React.Fragment>
    );
    // }
    // else{
    //   return(
    //     <SplashScreen />
    //   )
    // }
  }
}

Home.propTypes = {
  pollFeaturedScores: PropTypes.func.isRequired,
  fetchFantasyAnalysis: PropTypes.func.isRequired,
  fetchNewsOpinion: PropTypes.func.isRequired,
  fetchMatchReport: PropTypes.func.isRequired,
  fetchFeaturedPlayers: PropTypes.func,
  fetchPointsTable: PropTypes.func,
  articles: PropTypes.object,
  scoreDetails: PropTypes.object,
  players: PropTypes.object,
  criclytics: PropTypes.object,
  record: PropTypes.object,
  history: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  articles: makeSelectArticles(),
  scoreDetails: makeSelectScoreDetails(),
  players: makeSelectPlayers(),
  record: makeSelectRecord(),
  criclytics: makeSelectCriclytics(),
  schedule: makeSelectSchedule()
});

function mapDispatchToProps(dispatch) {
  return {
    pollFeaturedScores: payload =>
      dispatch(scoreDetailsActions.pollFeaturedScores(payload)),
    getScoreSummary: payload =>
      dispatch(scoreDetailsActions.getScoreCardSummary(payload)),
    getInningsPhase: payload =>
      dispatch(scoreDetailsActions.getInnPhase(payload)),
    getRecentScore: payload =>
      dispatch(scoreDetailsActions.getLast10Balls(payload)),
    stopRecentRunPoll: () => dispatch(scoreDetailsActions.stopBallPOll()),
    fetchFantasyAnalysis: payload =>
      dispatch(articlesActions.fetchFantasyAnalysis(payload)),
    fetchNewsOpinion: payload =>
      dispatch(articlesActions.fetchNewsOpinion(payload)),
    fetchMatchReport: payload =>
      dispatch(articlesActions.fetchMatchReport(payload)),
    fetchSuggestion: payload =>
      dispatch(articlesActions.fetchSuggestion(payload)),

    fetchPreAnalysis: payload =>
      dispatch(articlesActions.fetchPreAnalysis(payload)),
    fetchFeaturedPlayers: payload =>
      dispatch(playersActions.fetchFeaturedPlayers(payload)),
    fetchPointsTable: payload =>
      dispatch(recordActions.fetchPointsTable(payload)),
    fetchHomePoll: () => dispatch(recordActions.getPollHome()),
    saveCardPosition: payload =>
      dispatch(recordActions.saveCardPosition(payload)),
    postPollOption: payload => dispatch(recordActions.postPoll(payload)),
    getPollById: payload => dispatch(recordActions.getPollByPollId(payload)),
    postDeathOversBowlers: payload =>
      dispatch(criclyticsActions.postDeathOversBowler(payload)),
    getHomeCricData: payload =>
      dispatch(criclyticsActions.getHomeCric(payload)),
    fetchBallByBall: payload =>
      dispatch(criclyticsActions.getBallByBall(payload)),
    fetchOverSeperator: payload =>
      dispatch(criclyticsActions.getMomentumShiftOverSeparator(payload)),
    fetchFeaturedVideos: payload =>
      dispatch(articlesActions.fetchFeaturedVideos(payload)),
    fetchSeriesNews: payload =>
      dispatch(articlesActions.fetchseriesNews(payload)),
    fetchSeriesSchedule: payload =>
      dispatch(scheduleActions.fetchScheduleBySeries(payload)),
    fetchImages: payload => dispatch(articlesActions.fetchHomeImages(payload)),
    fetchHighlight: payload =>
      dispatch(articlesActions.fetchHighlight(payload)),
    pollReset: payload => dispatch(recordActions.pollReset(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
const withArticlesReducer = injectReducer({
  key: "articles",
  reducer: articlesReducer
});
const withScheduleReducer = injectReducer({
  key: "schedule",
  reducer: scheduleReducer
});
const withScoreDetailsReducer = injectReducer({
  key: "scoreDetails",
  reducer: scoreDetailsReducer
});
const withArticlesSaga = injectSaga({ key: "articles", saga: articlesSaga });
const withScheduleSaga = injectSaga({ key: "schedule", saga: scheduleSaga });

const withScoreDetailsSaga = injectSaga({
  key: "scoreDetails",
  saga: scoreDetailsSaga
});

// ============
const withPlayersReducer = injectReducer({
  key: "players",
  reducer: playersReducer
});
const withPlayersSaga = injectSaga({ key: "players", saga: playersSaga });
// ============

// ============
const withRecordReducer = injectReducer({
  key: "record",
  reducer: recordReducer
});
const withRecordSaga = injectSaga({ key: "record", saga: recordSaga });
// ============
// ============
const withCricReducer = injectReducer({
  key: "criclytics",
  reducer: criclyticsReducer
});
const withCricSaga = injectSaga({ key: "criclytics", saga: criclyticsSaga });
// ============

export default compose(
  withArticlesReducer,
  withScoreDetailsReducer,
  withPlayersReducer,
  withArticlesSaga,
  withScoreDetailsSaga,
  withPlayersSaga,
  withRecordReducer,
  withScheduleReducer,
  withScheduleSaga,
  withRecordSaga,
  withCricReducer,
  withCricSaga,
  withConnect
)(Home);
