/*
 *
 * Series reducer
 *
 */
/* eslint no-case-declarations: 0 */
/* eslint no-else-return: 0 */

import groupBy from "lodash/groupBy";
import moment from "moment";
// import partition from 'lodash/partition';
import sortBy from "lodash/sortBy";
import {
  FETCH_SERIES,
  FETCH_SERIES_SUCCESS,
  FETCH_SERIES_FAILURE,
  FETCH_SERIES_DETAILS,
  FETCH_SERIES_DETAILS_SUCCESS,
  FETCH_SERIES_DETAILS_FAILURE,
  FETCH_SERIES_SQUADS,
  FETCH_SERIES_SQUADS_SUCCESS,
  FETCH_SERIES_SQUADS_FAILURE,
  FETCH_SERIES_VENUES,
  FETCH_SERIES_VENUES_SUCCESS,
  FETCH_SERIES_VENUES_FAILURE,
  FETCH_SERIES_NEWS,
  FETCH_SERIES_NEWS_SUCCESS,
  FETCH_SERIES_NEWS_FAILURE
} from "./constants";
export const initialState = {
  seriesLoading: true,
  seriesList: {},
  seriesFilters: ["international", "domestic", "leagues", "women"],
  seriesDetailsLoading: true,
  seriesDetails: {},
  seriesMatches: [],
  seriesMatchesOldLength: 0,
  seriesSquadsLoading: false,
  seriesSquads: {},
  seriesVenuesLoading: false,
  seriesVenues: [],
  seriesNewsLoading: false,
  seriesNews: [],
  seriesNewsOldLength: 0,
  seriesNewsFeatured: [],
  host: ""
};

function seriesReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_SERIES:
      return { ...state, seriesLoading: true };
    case FETCH_SERIES_SUCCESS: {
      if (action.payload.length > 0) {
        const groupByDate = groupBy(
          sortBy(action.payload, ["startDate"]).reverse(),
          data =>
            moment(data.startDate)
              .startOf("month")
              .format("MMMM YYYY")
        );
        return { ...state, seriesLoading: false, seriesList: groupByDate };
      }
      return { ...state, seriesList: {}, seriesLoading: false };
    }
    case FETCH_SERIES_FAILURE:
      return { ...state, seriesLoading: false };
    case FETCH_SERIES_DETAILS:
      return { ...state, seriesDetailsLoading: true, seriesMatches: [] };
    case FETCH_SERIES_DETAILS_SUCCESS: {
      const seriesMatches = [...state.seriesMatches];
      return {
        ...state,
        seriesDetailsLoading: false,
        seriesDetails: action.payload.data,
        seriesMatches: action.payload.skip
          ? seriesMatches.concat(action.payload.data.matchMiniScoreCards)
          : action.payload.data.matchMiniScoreCards,
        seriesMatchesOldLength: action.payload.skip ? seriesMatches.length : 0
      };
    }
    case FETCH_SERIES_DETAILS_FAILURE:
      return { ...state, seriesDetailsLoading: false };
    case FETCH_SERIES_SQUADS:
      return {
        ...state,
        seriesSquadsLoading: true
      };
    case FETCH_SERIES_SQUADS_SUCCESS:
      return {
        ...state,
        seriesSquadsLoading: false,
        seriesSquads: action.payload
      };
    case FETCH_SERIES_SQUADS_FAILURE:
      return {
        ...state,
        seriesSquadsLoading: false
      };
    case FETCH_SERIES_VENUES:
      return {
        ...state,
        seriesVenuesLoading: true
      };
    case FETCH_SERIES_VENUES_SUCCESS:
      return {
        ...state,
        seriesVenuesLoading: false,
        seriesVenues: action.payload
      };
    case FETCH_SERIES_VENUES_FAILURE:
      return {
        ...state,
        seriesVenuesLoading: false
      };
    case FETCH_SERIES_NEWS:
      return {
        ...state,
        seriesNewsLoading: true
      };
    case FETCH_SERIES_NEWS_SUCCESS:
      if (!action.payload.skip) {
        // const newsGrouped = partition(action.payload.news, d => d.filters.featured);
        // const [seriesNewsFeatured, seriesNews] = newsGrouped;
        return {
          ...state,
          seriesNewsLoading: false,
          seriesNews: action.payload.news,
          // seriesNewsFeatured,
          host: action.payload.host,
          seriesNewsOldLength: 0
        };
      } else {
        const seriesNews = [...state.seriesNews];
        return {
          ...state,
          seriesNewsLoading: false,
          seriesNews: action.payload.news.length
            ? seriesNews.concat(action.payload.news)
            : seriesNews,
          host: action.payload.host,
          seriesNewsOldLength: state.seriesNews.length
        };
      }
    case FETCH_SERIES_NEWS_FAILURE:
      return {
        ...state,
        seriesNewsLoading: false
      };
    default:
      return state;
  }
}

export default seriesReducer;
