/*
 *
 * Series actions
 *
 */

import { showToast } from 'utils/misc';
import {
  FETCH_SERIES,
  FETCH_SERIES_SUCCESS,
  FETCH_SERIES_FAILURE,
  FETCH_SERIES_DETAILS,
  FETCH_SERIES_DETAILS_SUCCESS,
  FETCH_SERIES_DETAILS_FAILURE,
  FETCH_SERIES_SQUADS,
  FETCH_SERIES_SQUADS_SUCCESS,
  FETCH_SERIES_SQUADS_FAILURE,
  FETCH_SERIES_VENUES,
  FETCH_SERIES_VENUES_SUCCESS,
  FETCH_SERIES_VENUES_FAILURE,
  FETCH_SERIES_NEWS,
  FETCH_SERIES_NEWS_SUCCESS,
  FETCH_SERIES_NEWS_FAILURE,
} from './constants';

const ACTIVE_TOAST = {};

/* Fantasy analysis articles actions */
export const fetchSeries = payload => ({
  type: FETCH_SERIES,
  payload,
});

export const fetchSeriesSuccess = payload => ({
  type: FETCH_SERIES_SUCCESS,
  payload,
});

export const fetchSeriesFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchSeries', 'Unable to fetch series');
  return {
    type: FETCH_SERIES_FAILURE,
    payload: err,
  };
};

export const fetchSeriesDetails = payload => ({
  type: FETCH_SERIES_DETAILS,
  payload,
});

export const fetchSeriesDetailsSuccess = payload => ({
  type: FETCH_SERIES_DETAILS_SUCCESS,
  payload,
});

export const fetchSeriesDetailsFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchSeriesDetails', 'Unable to fetch series information');
  return {
    type: FETCH_SERIES_DETAILS_FAILURE,
    payload: err,
  };
};

export const fetchSeriesSquads = payload => ({
  type: FETCH_SERIES_SQUADS,
  payload,
});

export const fetchSeriesSquadsSuccess = payload => ({
  type: FETCH_SERIES_SQUADS_SUCCESS,
  payload,
});

export const fetchSeriesSquadsFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchSeriesSquads', 'Unable to fetch series squads');
  return {
    type: FETCH_SERIES_SQUADS_FAILURE,
    payload: err,
  };
};

export const fetchSeriesVenues = payload => ({
  type: FETCH_SERIES_VENUES,
  payload,
});

export const fetchSeriesVenuesSuccess = payload => ({
  type: FETCH_SERIES_VENUES_SUCCESS,
  payload,
});

export const fetchSeriesVenuesFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchSeriesVenues', 'Unable to fetch series venues');
  return {
    type: FETCH_SERIES_VENUES_FAILURE,
    payload: err,
  };
};

export const fetchSeriesNews = payload => ({
  type: FETCH_SERIES_NEWS,
  payload,
});

export const fetchSeriesNewsSuccess = payload => ({
  type: FETCH_SERIES_NEWS_SUCCESS,
  payload,
});

export const fetchSeriesNewsFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchSeriesNews', 'Unable to fetch series news');
  return {
    type: FETCH_SERIES_NEWS_FAILURE,
    payload: err,
  };
};
