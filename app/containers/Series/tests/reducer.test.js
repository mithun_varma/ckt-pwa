import { fromJS } from 'immutable';
import seriesReducer from '../reducer';

describe('seriesReducer', () => {
  it('returns the initial state', () => {
    expect(seriesReducer(undefined, {})).toEqual(fromJS({}));
  });
});
