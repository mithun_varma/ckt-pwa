import EmptyState from "components/Common/EmptyState";
import Loader from "components/Common/Loader";
import Filter from "lodash/filter";
import identity from "lodash/identity";
import map from "lodash/map";
import PickBy from "lodash/pickBy";
import size from "lodash/size";
// import has from "lodash/has";
import { Helmet } from "react-helmet";
import PropTypes from "prop-types";
import React from "react";
import { connect } from "react-redux";
import Reveal from "react-reveal/Reveal";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import injectReducer from "utils/injectReducer";
import injectSaga from "utils/injectSaga";
import CardGradientTitle from "../../components-v2/commonComponent/CardGradientTitle";
import Header from "../../components-v2/commonComponent/Header";
import Landing from "../../components-v2/commonComponent/SeriesListItem";
import TabBar from "../../components-v2/commonComponent/TabBar";
import { black, grey_4, white } from "../../styleSheet/globalStyle/color";
import animate from "../../utils/animationConfig";
import * as seriesActions from "./actions";
import seriesReducer from "./reducer";
import seriesSaga from "./saga";
import makeSelectSeries from "./selectors";
import ReactGA from "react-ga";

/* eslint-disable react/prefer-stateless-function */
/* eslint-disable no-nested-ternary */
/* eslint-disable dot-notation */

// const TAGS_CONFIG = ["All", "International", "Domestic", "Leauge", "Women"];
const TAGS_CONFIG = ["All", "International", "Domestic"];

let scrollTop = null;

let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  // ClevertapReact.initialize("TEST-Z88-4KR-845Z"); // Old Key
  ClevertapReact.initialize("W88-4KR-845Z");
  // initializeFirebase();
  // askForPermissioToReceiveNotifications();
}
class Series extends React.Component {
  state = {
    activeKey: "All",
    currentFilteredList: [],
    noSeriesFound: false,
    title: "",
    isHeaderBackground: false,
    titlePlaced: false
  };

  componentDidMount = async () => {
    // ReactGA.initialize("UA-136526387-1");
    // ReactGA.pageview(
    //   "https://www.cricket.com" +
    //     window.location.pathname +
    //     window.location.search
    // );
    window.addEventListener("scroll", this.handleScroll);
    await this.props.fetchSeries(this.props.series.seriesFilters);
    await this.setState({ currentFilteredList: this.props.series.seriesList });
  };
  componentWillReceiveProps = () =>
    this.setState({ currentFilteredList: this.props.series.seriesList });

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = event => {
    ({ scrollTop } = event.target.scrollingElement);
    // console.log("point: ", scrollTop);
    if (scrollTop > 26 && !this.state.titlePlaced) {
      this.setState({
        title: "Series",
        isHeaderBackground: true,
        titlePlaced: true
      });
    } else if (scrollTop < 27) {
      this.setState({
        title: "",
        isHeaderBackground: false,
        titlePlaced: false
      });
    }
  };

  handelActive = activeKey => {
    this.props.fetchSeries(
      activeKey === "All"
        ? ["international", "domestic"]
        : [activeKey.toLowerCase()]
    );
    const { series } = this.props;

    this.setState({ activeKey }, () => {
      const allKeys = Object.keys(series.seriesList);
      const filteredData = allKeys.map(ele =>
        Filter(series.seriesList[ele], ["tag", activeKey.toUpperCase()])
      );
      filteredData.forEach((ele, ind) => {
        filteredData[allKeys[ind]] = filteredData[ind];
        delete filteredData[ind];
        // ele[allKeys[ind]] = ele[ind];
      });

      activeKey === "All"
        ? this.setState({ currentFilteredList: series.seriesList })
        : this.setState({
            currentFilteredList: PickBy(filteredData, identity)
          });
    });
  };

  componentDidUpdate() {
    if (this.refs.Null) {
      this.refs.Null.innerHTML = "";
      this.refs.Null.append("No Series Found");
    }
  }
  render() {
    const { series, history } = this.props;
    
    return (
      <div
        style={{
          // padding: "54px 16px 16px"
          padding: "36px 16px 16px"
        }}
      >
        <Helmet titleTemplate="%s | cricket.com">
          <title>Cricket Series - International, Domestic, ODI, IPL and T20 | Cricket Series | Cricket Fixtures</title>
          <meta
            name="description"
            content="Get the list of all ongoing and upcoming cricket match series for international and domestic matches with live score, scorecard, ball by ball commentary and complete cricket news on cricket.com."
          />
          <meta
            name="keywords"
            content="cricket series, ipl series, cricket fixtures, cricket news, ipl series"
          />
          <link
            rel="canonical"
            href={`www.cricket.com${history &&
              history.location &&
              history.location.pathname}`}
          />
        </Helmet>
        {false && (
          <div
            style={{
              fontFamily: "Montserrat",
              fontWeight: 600,
              fontSize: 22,
              color: black,
              // marginBottom: 12
              marginBottom: 8
            }}
          >
            Series
          </div>
        )}
        <Header
          // title={this.state.title}
          title="Series"
          // isHeaderBackground={this.state.isHeaderBackground}
          isHeaderBackground={true}
          leftArrowBack
          textColor={black}
          backgroundColor={grey_4}
          history={history}
          leftIconOnClick={() => {
            history.goBack();
          }}
        />
        <Reveal effect={animate.series.parent.effect}>
          <div style={{ backgroundColor: white }}>
            <Reveal effect={animate.series.tags.effect}>
              <div
                style={{
                  position: "sticky",
                  top: 42,
                  zIndex: 5
                }}
              >
                <TabBar
                  activeItem={this.state.activeKey}
                  items={TAGS_CONFIG}
                  onClick={this.handelActive}
                />
              </div>
            </Reveal>

            <div className="wrapperContainer">
              {series.seriesLoading ? (
                <Loader styles={{ height: "150px" }} noOfLoaders={5} />
              ) : size(this.props.series.seriesList) > 0 ? (
                <React.Fragment>
                  {map(
                    this.props.series.seriesList,
                    (groupedSeries, seriesKey) => {
                      if (groupedSeries) {
                        return (
                          <Reveal
                            key={seriesKey}
                            effect={animate.series.list.effect}
                          >
                            <section>
                              <CardGradientTitle title={seriesKey} />
                              <div className="">
                                {map(groupedSeries, (series, key) => (
                                  <Landing
                                    history={history}
                                    onClick={() => {
                                      ClevertapReact.event("Series", {
                                        source: "Series",
                                        seriesId: series["seriesId"]
                                      });
                                      history.push(
                                        `/series/${
                                          series.name
                                            ? series.name
                                                .toLowerCase()
                                                .replace(/[ /]/g, "-")
                                            : ""
                                        }/${series["seriesId"]}`
                                      );
                                    }}
                                    key={key}
                                    seriesName={series.name}
                                    startDate={series.startDate}
                                    endDate={series.endDate}
                                    matchStats={[
                                      {
                                        totalMatches: series.totalOdi,
                                        kind: series.totalOdi > 1 ? "ODIs" : "ODI"
                                      },
                                      {
                                        totalMatches: series.totalTests,
                                        kind: series.totalTests > 1 ? "Tests" : "Test"
                                      },
                                      {
                                        totalMatches: series.totalT20,
                                        kind: series.totalT20 > 1 ? "T20s" : "T20"
                                      }
                                    ]}
                                  />
                                ))}
                              </div>
                            </section>
                          </Reveal>
                        );
                      }
                    }
                  )}
                </React.Fragment>
              ) : (
                <Reveal effect={animate.emptyState.effect}>
                  <EmptyState
                    img={require("../../images/no_series.png")}
                    msg="No series found"
                  />
                </Reveal>
              )}
            </div>
          </div>
        </Reveal>
      </div>
    );
  }
}

Series.propTypes = {
  fetchSeries: PropTypes.func.isRequired,
  series: PropTypes.object,
  history: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  series: makeSelectSeries()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchSeries: payload => dispatch(seriesActions.fetchSeries(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withSeriesReducer = injectReducer({
  key: "series",
  reducer: seriesReducer
});
const withSeriesSaga = injectSaga({ key: "series", saga: seriesSaga });

export default compose(
  withSeriesReducer,
  withSeriesSaga,
  withConnect
)(Series);
