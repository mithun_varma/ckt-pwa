import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the series state domain
 */

const selectSeriesDomain = state => state.series || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Series
 */

const makeSelectSeries = () => createSelector(selectSeriesDomain, substate => substate);

export default makeSelectSeries;
export { selectSeriesDomain };
