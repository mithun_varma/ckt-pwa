/* eslint no-param-reassign: 0 */
import { takeLatest, call, put } from "redux-saga/effects";
import {
  API_GET_SERIES,
  API_SERIES_DETAILS,
  // API_SERIES_DETAILS_ARTICLES,
  API_SERIES_DETAILS_SQUADS,
  API_SERIES_DETAILS_VENUES,
  API_SERIES_DETAILS_ARTICLES
} from "utils/requestUrls";
import request from "utils/request";
import * as seriesConstants from "./constants";
import * as seriesActions from "./actions";

export function* getSeries(payload) {
  try {
    const options = yield call(request, API_GET_SERIES(payload.payload));
    yield put(seriesActions.fetchSeriesSuccess(options.data.data));
  } catch (err) {
    yield put(seriesActions.fetchSeriesFailure(err));
  }
}

export function* getSeriesDetails(payload) {
  if (payload.payload.seriesId) {
    const requestUrl = API_SERIES_DETAILS(payload.payload.seriesId);
    delete payload.payload.seriesId;
    const config = {
      ...requestUrl
    };

    try {
      // const options = yield call(request, API_SERIES_DETAILS(payload.payload.seriesId));
      const options = yield call(request, config);

      // yield put(seriesActions.fetchSeriesDetailsSuccess(options.data.data));
      yield put(
        seriesActions.fetchSeriesDetailsSuccess({
          data: options.data.data,
          skip: payload.payload.skip
        })
      );
    } catch (err) {
      yield put(seriesActions.fetchSeriesDetailsFailure(err));
    }
  }
}

// COMMENTED OUT - NOT USING ANYWHERE IN APPLICATION
// export function* getSeriesArticles(payload) {
//   try {
//     const options = yield call(request, API_SERIES_DETAILS_ARTICLES(payload.payload.seriesId));
//     yield put(seriesActions.fetchSeriesArticlesSuccess(options.data.data));
//   } catch (err) {
//     yield put(seriesActions.fetchSeriesArticlesFailure(err));
//   }
// }

export function* getSeriesSquads(payload) {
  try {
    const options = yield call(
      request,
      API_SERIES_DETAILS_SQUADS(payload.payload.seriesId)
    );
    yield put(seriesActions.fetchSeriesSquadsSuccess(options.data.data));
  } catch (err) {
    yield put(seriesActions.fetchSeriesSquadsFailure(err));
  }
}

export function* getSeriesVenues(payload) {
  try {
    const options = yield call(
      request,
      API_SERIES_DETAILS_VENUES(payload.payload.seriesId)
    );
    yield put(seriesActions.fetchSeriesVenuesSuccess(options.data.data));
  } catch (err) {
    yield put(seriesActions.fetchSeriesVenuesFailure(err));
  }
}

export function* getSeriesNews(payload) {
  const url = API_SERIES_DETAILS_ARTICLES(payload.payload.seriesId);
  const config = {
    ...url,
    params: {
      ...payload.payload
    }
  };
  try {
    // const options = yield call(request, API_SERIES_DETAILS_ARTICLES(payload.payload.seriesId));
    const options = yield call(request, config);
    yield put(
      seriesActions.fetchSeriesNewsSuccess({
        news: options.data.data,
        host: options.data.host,
        skip: payload.payload.skip
      })
    );
  } catch (err) {
    yield put(seriesActions.fetchSeriesNewsFailure(err));
  }
}

export default function* fetchWatcher() {
  yield takeLatest(seriesConstants.FETCH_SERIES, getSeries);
  yield takeLatest(seriesConstants.FETCH_SERIES_DETAILS, getSeriesDetails);
  yield takeLatest(seriesConstants.FETCH_SERIES_SQUADS, getSeriesSquads);
  yield takeLatest(seriesConstants.FETCH_SERIES_VENUES, getSeriesVenues);
  yield takeLatest(seriesConstants.FETCH_SERIES_NEWS, getSeriesNews);
}
