import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";

import { Helmet } from "react-helmet";

// import sortBy from 'lodash/sortBy';
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import Teams from "./../../components-v2/screenComponent/teamProfile/teamProfile";

import makeSelectSeries from "../Teams/selectors";
import teamReducer from "../Teams/reducer";
import * as teamActions from "../Teams/actions";
import teamSaga from "../Teams/saga";

/* eslint-disable react/prefer-stateless-function */

export class TeamsSchedule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "schedule",
      matchType: "all",
      month: "",
      year: "",
      date: 1
    };
    this._handelDate = this._handelDate.bind(this);
  }

  componentDidMount = () => {
    const teamId = this.props.match.params.teamId;
    this.props.fetchTeamDetails({ teamId: teamId, format: "test" });
    this.props.fetchTeamNews({ teamId: teamId });
    window.scrollTo(0, 0);
    this.props.fetchTeamMagicMoment({ teamId });
    this.props.fetchTeamNostalgicMoment({ teamId });
  };

  _handelDate(date) {
    const teamId = this.props.match.params.teamId;
    this.props.fetchTeamSchedule({ teamId: teamId, date: date });
  }

  render() {
    // this._handelDate();
    const { teams, history } = this.props;
    const teamsDetails = teams;
    //  console.log(teamsDetails.teamNews, '------');
    const teamNews = teamsDetails.teamNews;
    const teamSchedule = teamsDetails.teamSchedule;
    const teamMagicMoments = teams.teamMagicMoments;
    const teamNostalgicMoments = teams.teamNostalgicMoments[0];

    const TEAM_DETAILS = this.props.teams.teamDetails[
      this.props.match.params.teamId
    ];
    return (
      <div>
        <Helmet titleTemplate="%s | cricket.com">
          <title>{`${TEAM_DETAILS &&
            TEAM_DETAILS.displayName} Cricket Team | Team ${TEAM_DETAILS &&
              TEAM_DETAILS.displayName} Squad and Match Fixtures`}</title>
          <meta
            name="description"
            content={`Team ${TEAM_DETAILS && TEAM_DETAILS.displayName} squad, match schedules, scorecard, news and some exclusive moments of the team ${TEAM_DETAILS && TEAM_DETAILS.displayName}. Visit cricket.com for more info.`}
          />
          {/* <meta
            name="keywords"
            content={(article.articleDetails && article.articleDetails.seoKeywords) || ''}
          /> */}
          <link
            rel="canonical"
            href={`www.cricket.com${history &&
              history.location &&
              history.location.pathname}`}
          />
        </Helmet>
        <Teams
          teamNews={teamNews}
          teamNostalgicMoments={teamNostalgicMoments}
          teamMagicMoments={teamMagicMoments}
          fetchTeamSchedule={this._handelDate}
          history={this.props.history}
          teamsDetails={teamsDetails}
          teamId={this.props.match.params.teamId}
          teamSchedule={teamSchedule}
        />
      </div>
    );
  }
}

TeamsSchedule.propTypes = {
  match: PropTypes.object,
  fetchTeamSchedule: PropTypes.func.isRequired,
  history: PropTypes.object,
  teams: PropTypes.object,
  fetchTeamResults: PropTypes.func.isRequired,
  fetchTeamSquads: PropTypes.func.isRequired,
  fetchTeamNews: PropTypes.func.isRequired,
  fetchTeamDetails: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  teams: makeSelectSeries()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchTeamSchedule: payload =>
      dispatch(teamActions.fetchTeamSchedule(payload)),
    fetchTeamResults: payload =>
      dispatch(teamActions.fetchTeamResults(payload)),
    fetchTeamSquads: payload => dispatch(teamActions.fetchTeamSquads(payload)),
    fetchTeamNews: payload => dispatch(teamActions.fetchTeamNews(payload)),
    fetchTeamDetails: payload =>
      dispatch(teamActions.fetchTeamDetails(payload)),
    fetchTeamMagicMoment: payload =>
      dispatch(teamActions.fetchTeamMagicMoment(payload)),
    fetchTeamNostalgicMoment: payload =>
      dispatch(teamActions.fetchTeamNostalgicMoment(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withTeamReducer = injectReducer({ key: "teams", reducer: teamReducer });
const withTeamSaga = injectSaga({ key: "teams", saga: teamSaga });

export default compose(
  withTeamReducer,
  withTeamSaga,
  withConnect
)(TeamsSchedule);
