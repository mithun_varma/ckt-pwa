
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";

import {
  white,
  black
} from "../../styleSheet/globalStyle/color";
import Header from "components-v2/commonComponent/Header";
// import ArticleDetailsComponent from 'components/Articles/ArticleDetails';
import ArticleDetailsComponent from "components-v2/Articles/ArticleDetails";

import makeSelectArticles from "../Articles/selectors";
import articlesReducer from "../Articles/reducer";
import * as articlesActions from "../Articles/actions";
import articlesSaga from "../Articles/saga";
import ReactGA from "react-ga";

/* eslint-disable react/prefer-stateless-function */
let scrollTop = null;
export class ArticleDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      isHeaderBackground: false
    };
  }

  componentDidMount = () => {
    // ReactGA.initialize("UA-136526387-1");
    // ReactGA.pageview(
    //   "https://www.cricket.com" +
    //     window.location.pathname +
    //     window.location.search
    // );
    window.addEventListener("scroll", this.handleScroll);
    // this.props.fetchArticleDetails({ id: this.props.match.params.articleId });
    this.props.fetchArticleDetails({ id: this.props.match.params.title});
  };

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = event => {
    ({ scrollTop } = event.target.scrollingElement);
    if (scrollTop > 220) {
      this.setState({
        // title: "Records",
        title: this.props.articleDetails.articleDetails.title,
        isHeaderBackground: true
      });
    } else if (scrollTop < 220) {
      this.setState({
        title: "",
        isHeaderBackground: false
      });
    }
  };

  render() {
    const { articleDetails, history, fetchArticleDetails } = this.props;
    return (
      <div>
        <Header
          title={this.state.title}
          isHeaderBackground={this.state.isHeaderBackground}
          leftArrowBack
          textColor={this.state.isHeaderBackground ? black : white}
          backgroundColor={white}
          history={history}
          leftIconOnClick={() => {
            history.goBack() || history.push("/")
          }}
          titleStyles={{
            fontSize: 16
          }}
          // isShare
        />
        <ArticleDetailsComponent
          article={articleDetails}
          history={history}
          fetchArticleDetails={fetchArticleDetails}
          match={this.props.match}
        />
      </div>
    );
  }
}

ArticleDetails.propTypes = {
  articleDetails: PropTypes.object,
  fetchArticleDetails: PropTypes.func,
  match: PropTypes.object,
  history: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  articleDetails: makeSelectArticles()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchArticleDetails: payload =>
      dispatch(articlesActions.fetchArticleDetails(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withArticlesReducer = injectReducer({
  key: "articles",
  reducer: articlesReducer
});
const withArticlesSaga = injectSaga({ key: "articles", saga: articlesSaga });

export default compose(
  withArticlesReducer,
  withArticlesSaga,
  withConnect
)(ArticleDetails);
