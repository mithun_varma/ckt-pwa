import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectPlayers from "../Players/selectors";
import reducer from "../Players/reducer";
import saga from "../Players/saga";
import * as playersActions from "../Players/actions";

import PlayerDiscovery from "./../../components-v2/screenComponent/playerProfile/PlayerDiscovery";
import ReactGA from "react-ga";

/* eslint-disable react/prefer-stateless-function */
export class PlayersList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPlayersLoading: false
    };
  }
  componentDidMount() {
    // ReactGA.initialize("UA-136526387-1");
    // ReactGA.pageview(
    //   "https://www.cricket.com" +
    //     window.location.pathname +
    //     window.location.search
    // );
    this.props.fetchPlayersList({ skip: 0, limit: 10 });
  }

  render() {
    const { players, history } = this.props;
    const playerList = players.playersList;
    return (
      <div>
        <PlayerDiscovery
          playerDiscoveryData={playerList}
          loading={players.playersListLoading}
          history={history}
        />
      </div>
    );
  }
}

PlayersList.propTypes = {
  fetchPlayersList: PropTypes.func,
  players: PropTypes.object,
  history: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  players: makeSelectPlayers()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchPlayersList: payload =>
      dispatch(playersActions.fetchPlayersList(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "players", reducer });
const withSaga = injectSaga({ key: "players", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(PlayersList);
