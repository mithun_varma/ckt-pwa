import {
  call,
  put,
  takeLatest,
  take,
  race,
  fork,
  all,
  select,
  cancel
} from "redux-saga/effects";
import { delay } from "redux-saga";
import {
  API_GET_FEATURED_SCORES,
  API_SCORE_DETAILS,
  API_GET_COMMENTARY,
  API_GET_FANTASY_BOARD,
  API_GET_FEATURED_ARTICLES,
  API_GET_COMMENTARY_OVER_SEPARATOR,
  API_GET_COMMENTARY_HIGHLIGHTS,
  API_GET_HIGHLIGHTS_WAGON_STATS,
  API_GET_FANFIGHT_PLAYERS,
  API_SAVE_FANFIGHT_TEAM,
  API_SCORE_SUMMARY,
  API_SCORE_SUMMARY_TEST,
  API_INN_PHASE,
  API_LAST_10_BALLS,
  API_FANFIGHT_GRAPH,
  API_INNING_SUMMARY,
  API_GET_SESSION_PHASE
} from "utils/requestUrls";
import ScorecardParser from "../../utils/parser";
import MiniScorecardParser from "../../utils/miniParser";
import request from "utils/request";
import {
  FEATURED_SCORES_POLLING_DELAY,
  SCORE_DETAILS_FETCH_ERROR__POLLING_DELAY,
  COMMENTARY_DATA_DELAY,
  HIGHLIGHTS_DATA_DELAY
} from "../../config";
import * as scoreDetailsConstants from "./constants";
import * as scoreDetailsActions from "./actions";
// import { SCORE_DETAILS_POLLING_DELAY } from '../../config';
/* eslint-disable no-underscore-dangle */
/* eslint-disable dot-notation */
/* eslint-disable indent */

let ScoreDetailBackgroundTask = null;
let FeaturedBackgroundTask = null;
let CommentaryBackgroundTask = null;

export function* pollFeaturedScores() {
  while (true) {
    try {
      const options = yield call(request, API_GET_FEATURED_SCORES);
      yield put(
        scoreDetailsActions.pollFeaturedScoresSuccess(options.data.data)
      );
      yield call(delay, FEATURED_SCORES_POLLING_DELAY);
    } catch (err) {
      yield call(delay, SCORE_DETAILS_FETCH_ERROR__POLLING_DELAY);
      yield put(scoreDetailsActions.pollFeaturedScoresFailure(err));
    }
  }
}

export function* pollScoreDetails(payload) {
  while (true) {
    try {
      const options = yield call(request, API_SCORE_DETAILS(payload.matchId));
      const data = ScorecardParser(options.data.data);

      // yield put(scoreDetailsActions.pollScoreDetailsSuccess(options.data.data));
      yield put(
        scoreDetailsActions.pollScoreDetailsSuccess({
          parsed: data,
          unParsed: options.data.data
        })
      );
      // if (options.data.data && options.data.data.status !== 'started') break;
      if (data && data.status !== "started") break;
      yield call(delay, FEATURED_SCORES_POLLING_DELAY);
    } catch (err) {
      yield call(delay, SCORE_DETAILS_FETCH_ERROR__POLLING_DELAY);
      yield put(scoreDetailsActions.pollScoreDetailsFailure(err));
    }
  }
}

export function* getFantasyBoard(payload) {
  try {
    const options = yield call(
      request,
      API_GET_FANTASY_BOARD(payload.payload.matchId)
    );
    yield put(scoreDetailsActions.fetchFantasyBoardSuccess(options.data.data));
  } catch (err) {
    yield put(scoreDetailsActions.fetchFantasyBoardFailure(err));
  }
}

export function* getFeaturedArticles(payload) {
  try {
    const options = yield call(
      request,
      API_GET_FEATURED_ARTICLES(payload.payload.matchId)
    );
    yield put(scoreDetailsActions.fetchFeaturedArticlesSuccess(options.data));
  } catch (err) {
    yield put(scoreDetailsActions.fetchFeaturedArticlesFailure(err));
  }
}

export function* fetchWatcher() {
  yield takeLatest(scoreDetailsConstants.FETCH_FANTASY_BOARD, getFantasyBoard);
  yield takeLatest(
    scoreDetailsConstants.FETCH_FEATURED_ARTICLES,
    getFeaturedArticles
  );
}

export function* watchPollFeaturedScoresSaga() {
  while (true) {
    yield take(scoreDetailsConstants.POLL_FEATURED_SCORES);
    yield race([
      call(pollFeaturedScores),
      take(scoreDetailsConstants.STOP_POLL_FEATURED_SCORES)
    ]);
  }
}
export function* watchPollScoresDetailsSaga() {
  while (true) {
    const { payload } = yield take(scoreDetailsConstants.POLL_SCORE_DETAILS);
    yield race([
      call(pollScoreDetails, payload),
      take(scoreDetailsConstants.STOP_POLL_SCORE_DETAILS)
    ]);
  }
}

export function* fetchCommentaryData({ params, isCommentry }) {
  let first = true;
  while (isCommentry ? true : first) {
    try {
      const redux = yield select();
      const key = yield isCommentry ? "commentary" : "summary";
      const paramData = yield !first
        ? {
            ...API_GET_COMMENTARY.params,
            ...params,
            latestId: redux.scoreDetails[key].values[0].key
          }
        : {
            ...API_GET_COMMENTARY.params,
            ...params
          };
      const { data } = yield call(request, {
        ...API_GET_COMMENTARY,
        params: paramData
      });
      if (data.data.length >= 0 || first)
        yield put(
          scoreDetailsActions.getCommentarySuccess({
            values: data.data,
            filters: params,
            isCommentry,
            first
          })
        );
      // yield call(delay, FEATURED_SCORES_POLLING_DELAY);
      // yield call(delay, COMMENTARY_DATA_DELAY);
      // New Condition Added for Delay duration in Commentary and Highlights
      if (isCommentry) {
        yield call(delay, COMMENTARY_DATA_DELAY);
      }
      // else {
      //   yield call(delay, HIGHLIGHTS_DATA_DELAY);
      // }
      first = false;
      if (
        redux.scoreDetails.scoreDetailCards[params.matchId] &&
        redux.scoreDetails.scoreDetailCards[params.matchId].status !== "started"
      )
        break;
    } catch (err) {
      yield put(scoreDetailsActions.getCommentaryFailure(err.response));
      // yield call(delay, FEATURED_SCORES_POLLING_DELAY);
      // yield call(delay, COMMENTARY_DATA_DELAY);
      // New Condition Added for Delay duration in Commentary and Highlights
      if (isCommentry) {
        yield call(delay, COMMENTARY_DATA_DELAY);
      }
      // else {
      //   yield call(delay, HIGHLIGHTS_DATA_DELAY);
      // }
    }
  }
}
export function* pagingCommentaryData({ params, isCommentry }) {
  try {
    const { data } = yield call(request, {
      ...API_GET_COMMENTARY,
      params: {
        ...params
      }
    });

    if (data.data.length >= 0)
      yield put(
        scoreDetailsActions.getCommentaryPagingSuccess({
          values: data.data,
          filters: params,
          isCommentry
        })
      );
  } catch (err) {
    yield put(scoreDetailsActions.getCommentaryFailure(err));
  }
}

export function* watchCommentaryPollSaga() {
  while (true) {
    const { payload } = yield take(scoreDetailsConstants.GET_COMMENTARY_DATA);
    yield race([
      call(fetchCommentaryData, payload),
      take(scoreDetailsConstants.COMMENTARY_DATA_POLLING_STOP)
    ]);
  }
}

export function* watchCommentaryPollSagaNew() {
  while (true) {
    const { payload } = yield take(scoreDetailsConstants.GET_COMMENTARY_DATA);
    if (CommentaryBackgroundTask) yield cancel(CommentaryBackgroundTask);
    CommentaryBackgroundTask = yield fork(fetchCommentaryData, payload);
    yield take(scoreDetailsConstants.COMMENTARY_DATA_POLLING_STOP);
    yield cancel(CommentaryBackgroundTask);
  }
}

export function* commentaryPaging() {
  while (true) {
    const { payload } = yield take(
      scoreDetailsConstants.GET_PAGING_COMMENTARY_DATA
    );
    yield call(pagingCommentaryData, payload);
  }
}

export function* getCommentaryOverSeparator(payload) {
  while (true) {
    const url = API_GET_COMMENTARY_OVER_SEPARATOR(payload.payload.matchId);
    const config = {
      ...url,
      params: {
        ...url.params,
        ...payload.payload
      }
    };
    try {
      const options = yield call(request, config);
      yield put(
        scoreDetailsActions.fetchCommentaryOverSeparatorSuccess(
          options.data.data
        )
      );
      yield call(delay, COMMENTARY_DATA_DELAY);
    } catch (err) {
      yield put(scoreDetailsActions.fetchCommentaryOverSeparatorFailure(err));
      yield call(delay, COMMENTARY_DATA_DELAY);
    }
  }
}

export function* getCommentaryOverSeparatorWatcher() {
  // yield takeLatest(scoreDetailsConstants.FETCH_COMMENTARY_OVER_SEPARATOR, getCommentaryOverSeparator);
  while (true) {
    const payload = yield take(
      scoreDetailsConstants.FETCH_COMMENTARY_OVER_SEPARATOR
    );
    yield race([
      call(getCommentaryOverSeparator, payload),
      take(scoreDetailsConstants.STOP_COMMENTARY_OVER_SEPARATOR)
    ]);
  }
}

export function* getCommentaryHighlights({ params }) {
  try {
    const url = API_GET_COMMENTARY_HIGHLIGHTS(params);
    const { data } = yield call(request, url);
    if (data.data.length >= 0)
      yield put(
        scoreDetailsActions.commentaryHighlightsSuccess({
          values: data.data,
          filters: params
        })
      );
  } catch (err) {
    yield put(scoreDetailsActions.commentaryHighlightsFailure(err.response));
  }
}

export function* commentaryHighlights() {
  while (true) {
    const { payload } = yield take(scoreDetailsConstants.COMMENTARY_HIGHLIGHTS);
    yield call(getCommentaryHighlights, payload);
  }
}

export function* pagingHighlightsData({ params }) {
  try {
    const url = API_GET_COMMENTARY_HIGHLIGHTS(params);
    const { data } = yield call(request, url);
    if (data.data.length >= 0)
      yield put(
        scoreDetailsActions.getHighlightsPagingSuccess({
          values: data.data,
          filters: params
        })
      );
  } catch (err) {
    yield put(scoreDetailsActions.commentaryHighlightsFailure(err));
  }
}

export function* highlightsPaging() {
  while (true) {
    const { payload } = yield take(
      scoreDetailsConstants.GET_PAGING_HIGHLIGHTS_DATA
    );
    yield call(pagingHighlightsData, payload);
  }
}

export function* getHighlightsWagonStats(payload) {
  const url = API_GET_HIGHLIGHTS_WAGON_STATS(payload.payload);
  try {
    const options = yield call(request, url);
    yield put(
      scoreDetailsActions.highlightsWagonStatsSuccess(options.data.data)
    );
  } catch (err) {
    yield put(scoreDetailsActions.highlightsWagonStatsFailure(err));
  }
}
// Dekhiye Bhaiya, ye Fan-Fight ka action hai
export function* getFanFightPlayers(payload) {
  const url = API_FANFIGHT_GRAPH(payload.payload.matchId);
  const config = {
    ...url
    // params: {
    //   ...payload.payload
    // }
  };
  try {
    const options = yield call(request, config);
    yield put(
      scoreDetailsActions.fetchFanFightPlayersSuccess(options.data.data)
    );
  } catch (err) {
    yield put(scoreDetailsActions.fetchFanFightPlayersFailure(err));
  }
}

export function* saveFanFightTeam(payload) {
  const url = API_SAVE_FANFIGHT_TEAM;
  const config = {
    ...url,
    payload: payload.payload.payload
  };
  try {
    const options = yield call(request, config);
    yield put(scoreDetailsActions.saveFanFightTeamSuccess(options.data));
  } catch (err) {
    yield put(scoreDetailsActions.saveFanFightTeamFailure(err));
  }
}

export function* fetchScoreSummary(payload) {
  const url =
    payload.payload.format == "TEST"
      ? API_SCORE_SUMMARY_TEST
      : API_SCORE_SUMMARY;
  const config = {
    ...url,
    params: {
      matchId: payload.payload.matchId
    }
  };
  try {
    const options = yield call(request, config);
    yield put(scoreDetailsActions.getScoreCardSummarySuccess(options.data));
  } catch (err) {
    yield put(scoreDetailsActions.getScoreCardSummaryFailure(err));
  }
}

export function* getInningsPhase(payload) {
  const url = payload.payload.inningId
    ? API_INN_PHASE(payload.payload)
    : API_GET_SESSION_PHASE(payload.payload);
  // const config = {
  //   ...url,
  //   params: {
  //     ...payload.payload
  //   }
  // };
  try {
    const options = yield call(request, url);
    yield put(scoreDetailsActions.getInnPhaseSuccess(options.data));
  } catch (err) {
    yield put(scoreDetailsActions.getInnPhaseFailure(err));
  }
}

export function* pollRecentRuns(payload) {
  const url = API_LAST_10_BALLS;
  const config = {
    ...url,
    params: {
      ...payload
    }
  };
  while (true) {
    try {
      const options = yield call(request, config);
      yield put(scoreDetailsActions.getLast10BallsSuccess(options.data));
      yield call(delay, FEATURED_SCORES_POLLING_DELAY);
    } catch (err) {
      yield call(delay, SCORE_DETAILS_FETCH_ERROR__POLLING_DELAY);
      yield put(scoreDetailsActions.getLast10BallsFailure(err));
    }
  }
}

export function* watchPollLast10BallsSaga() {
  while (true) {
    const { payload } = yield take(scoreDetailsConstants.GET_LAST_10_BALLS);
    yield race([
      call(pollRecentRuns, payload),
      take(scoreDetailsConstants.STOP_POLL)
    ]);
  }
}

export function* fetchInningSummary(payload) {
  const url = API_INNING_SUMMARY(
    payload.payload.matchId,
    payload.payload.inningId,
    payload.payload.teamId
  );
  try {
    const options = yield call(request, url);
    yield put(scoreDetailsActions.getInningSummarySuccess(options.data.data));
  } catch (err) {
    yield put(scoreDetailsActions.getInningSummaryFailure(err));
  }
}

export default function* defaultSaga() {
  yield all(
    [
      fetchWatcher,
      watchPollFeaturedScoresSaga,
      watchPollScoresDetailsSaga,
      watchCommentaryPollSaga,
      watchPollLast10BallsSaga,
      // watchCommentaryPollSagaNew,
      commentaryPaging,
      getCommentaryOverSeparatorWatcher,
      commentaryHighlights,
      highlightsPaging
    ].map(fork)
  );
  yield takeLatest(
    scoreDetailsConstants.HIGHLIGHTS_WAGON_STATS,
    getHighlightsWagonStats
  );
  yield takeLatest(
    scoreDetailsConstants.FETCH_FANFIGHT_PLAYERS,
    getFanFightPlayers
  );
  yield takeLatest(scoreDetailsConstants.SAVE_FANFIGHT_TEAM, saveFanFightTeam);
  yield takeLatest(scoreDetailsConstants.GET_INNINGS_PHASE, getInningsPhase);
  yield takeLatest(
    scoreDetailsConstants.GET_SCORECARD_SUMMARY,
    fetchScoreSummary
  );
  yield takeLatest(
    scoreDetailsConstants.GET_INNING_SUMMARY,
    fetchInningSummary
  );
  // yield takeLatest(scoreDetailsConstants.GET_LAST_10_BALLS, fetchLast10Balls);
}
