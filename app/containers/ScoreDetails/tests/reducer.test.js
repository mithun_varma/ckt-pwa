import { fromJS } from 'immutable';
import scoreDetailsReducer from '../reducer';

describe('scoreDetailsReducer', () => {
  it('returns the initial state', () => {
    expect(scoreDetailsReducer(undefined, {})).toEqual(fromJS({}));
  });
});
