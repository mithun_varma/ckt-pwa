/*
 *
 * ScoreDetails reducer
 *
 */
/* eslint-disable indent */

import {
  POLL_FEATURED_SCORES,
  POLL_FEATURED_SCORES_SUCCESS,
  POLL_FEATURED_SCORES_FAILURE,
  POLL_SCORE_DETAILS,
  POLL_SCORE_DETAILS_SUCCESS,
  POLL_SCORE_DETAILS_FAILURE,
  FETCH_FANTASY_BOARD,
  FETCH_FANTASY_BOARD_SUCCESS,
  FETCH_FANTASY_BOARD_FAILURE,
  FETCH_FEATURED_ARTICLES,
  FETCH_FEATURED_ARTICLES_SUCCESS,
  FETCH_FEATURED_ARTICLES_FAILURE,
  GET_COMMENTARY_DATA,
  COMMENTARY_DATA_FETCH_SUCCESS,
  COMMENTARY_DATA_PAGING_FETCH_SUCCESS,
  FETCH_COMMENTARY_OVER_SEPARATOR,
  FETCH_COMMENTARY_OVER_SEPARATOR_SUCCESS,
  FETCH_COMMENTARY_OVER_SEPARATOR_FAILURE,
  COMMENTARY_HIGHLIGHTS,
  COMMENTARY_HIGHLIGHTS_SUCCESS,
  COMMENTARY_HIGHLIGHTS_FAILURE,
  HIGHLIGHTS_DATA_PAGING_FETCH_SUCCESS,
  HIGHLIGHTS_WAGON_STATS,
  HIGHLIGHTS_WAGON_STATS_SUCCESS,
  HIGHLIGHTS_WAGON_STATS_FAILURE,
  FETCH_FANFIGHT_PLAYERS,
  FETCH_FANFIGHT_PLAYERS_SUCCESS,
  FETCH_FANFIGHT_PLAYERS_FAILURE,
  SAVE_FANFIGHT_TEAM,
  SAVE_FANFIGHT_TEAM_SUCCESS,
  SAVE_FANFIGHT_TEAM_FAILURE,
  GET_INNING_SUMMARY,
  GET_INNING_SUMMARY_SUCCESS,
  GET_INNING_SUMMARY_FAILURE,
} from "./constants";

import * as scoreConstants from "./constants";
// import { stat } from 'fs';

export const initialState = {
  scoreCardLoading: false,
  scoreCards: {},
  scoreDetailsMiniCards: {},
  scoreCardIds: [],
  scoreDetailCards: {},
  scoreDetailCardIds: [],
  fantasyBoardLoading: false,
  fantasyBoard: [],
  featuredArticlesLoading: false,
  featuredArticles: [],
  commentary: {
    values: [],
    filters: {},
    lastFetched: ""
  },
  summary: {
    values: [],
    filters: {
      highlights: ["wicket", "four", "six", "milestone"]
    }
    // lastFetched: '',
  },
  overSeparator: [],
  wagonPointsLoading: false,
  wagonPoints: [],
  articleHost: "",
  fanFightListLoading: false,
  fanFightList: null,
  teams: [],
  saveFanFightResponse: false,
  scoreSummary: null,
  summaryLoading: false,
  inningsPhase: [],
  inningsPhaseLoading: false,
  last10Balls: [],
  liveRunLoading: false,
  inningSummary: null,
  inningSummaryLoading: false,
};

function scoreDetailsReducer(state = initialState, action) {
  switch (action.type) {
    case POLL_FEATURED_SCORES:
      return Object.assign({}, state, { scoreCardLoading: true });
    case POLL_FEATURED_SCORES_SUCCESS: {
      const scoreCardIds = [];
      const newScoreCards = { ...state.scoreCards };
      action.payload.forEach(scoreCard => {
        // scoreCardIds.push(scoreCard.key);
        // scoreCardIds.push(scoreCard.matchId);
        // newScoreCards[scoreCard.key] = scoreCard;
        newScoreCards[scoreCard.matchId] = scoreCard;
      });
      for (let id in newScoreCards) {
        scoreCardIds.push(id);
      }
      return Object.assign({}, state, {
        scoreCardLoading: false,
        scoreCards: newScoreCards,
        scoreCardIds
      });
    }
    case POLL_FEATURED_SCORES_FAILURE:
      return Object.assign({}, state, { scoreCardLoading: false });
    case POLL_SCORE_DETAILS:
      return Object.assign({}, state, { scoreCardLoading: true });
    case POLL_SCORE_DETAILS_SUCCESS: {
      //
      // const scoreCards = { ...state.scoreCards };
      // scoreCards[action.payload.key] = action.payload;
      // scoreCards[action.payload.matchid || action.payload.matchId] = action.payload;
      // return Object.assign({}, state, { scoreCardLoading: false, scoreCards });
      const scoreDetailCards = { ...state.scoreDetailCards };
      const scoreDetailsMiniCards = { ...state.scoreDetailsMiniCards };
      // let scoreCardIds = { ...state.scoreCardIds };
      scoreDetailCards[action.payload.parsed.key] = action.payload.parsed;
      scoreDetailsMiniCards[action.payload.parsed.key] =
        action.payload.unParsed.fullScorecard.matchMiniScorecard;
      // scoreCardIds = [];
      // for (let id in scoreDetailsMiniCards) {
      //   console.log(id);
      //   scoreCardIds.push(id);
      // }
      return Object.assign({}, state, {
        scoreCardLoading: false,
        scoreDetailCards,
        scoreDetailsMiniCards
        // scoreCardIds,
      });
    }
    case POLL_SCORE_DETAILS_FAILURE:
      return Object.assign({}, state, { scoreCardLoading: false });
    case GET_COMMENTARY_DATA:
      return action.payload.filters
        ? {
            ...state,
            summary: {
              ...state.summary,
              filters: action.payload.filters,
              values: []
            }
          }
        : {
            ...state,
            summary: { ...state.summary, values: [] },
            commentary: {
              ...state.commentary,
              values: []
            }
          };
    case COMMENTARY_DATA_FETCH_SUCCESS: {
      const key = action.payload.isCommentry ? "commentary" : "summary";
      return action.payload.first
        ? {
            ...state,
            [key]: {
              ...action.payload,
              lastFetched: new Date().getTime().toString(),
              count: action.payload.values.length
            }
          }
        : {
            ...state,
            [key]: {
              filters: action.payload.filters,
              isCommentry: action.payload.isCommentry,
              lastFetched: new Date().getTime().toString(),
              count: action.payload.values.length,
              values: [...action.payload.values, ...state[key].values]
            }
          };
    }
    case COMMENTARY_DATA_PAGING_FETCH_SUCCESS: {
      const key = action.payload.isCommentry ? "commentary" : "summary";
      return {
        ...state,
        [key]: {
          filters: action.payload.filters,
          isCommentry: action.payload.isCommentry,
          lastFetched: new Date().getTime().toString(),
          count: action.payload.values.length,
          values: state[key].values.concat(action.payload.values)
        }
      };
    }
    case HIGHLIGHTS_DATA_PAGING_FETCH_SUCCESS: {
      return {
        ...state,
        summary: {
          filters: action.payload.filters,
          lastFetched: new Date().getTime().toString(),
          count: action.payload.values.length,
          values: state.summary.values.concat(action.payload.values)
        }
      };
    }
    case FETCH_FANTASY_BOARD: {
      return Object.assign({}, state, {
        fantasyBoardLoading: true
      });
    }
    case FETCH_FANTASY_BOARD_SUCCESS: {
      return Object.assign({}, state, {
        fantasyBoardLoading: false,
        fantasyBoard: action.payload
      });
    }
    case FETCH_FANTASY_BOARD_FAILURE:
      return Object.assign({}, state, {
        fantasyBoardLoading: false
      });
    case FETCH_FEATURED_ARTICLES:
      return Object.assign({}, state, { featuredArticlesLoading: true });
    case FETCH_FEATURED_ARTICLES_SUCCESS:
      return Object.assign({}, state, {
        featuredArticlesLoading: false,
        featuredArticles: action.payload.data,
        articleHost: action.payload.host
      });
    case FETCH_FEATURED_ARTICLES_FAILURE:
      return Object.assign({}, state, { featuredArticlesLoading: false });
    case FETCH_COMMENTARY_OVER_SEPARATOR_SUCCESS:
      return {
        ...state,
        overSeparator: action.payload ? action.payload : []
      };

    case COMMENTARY_HIGHLIGHTS:
      return {
        ...state,
        summary: {
          ...state.summary,
          filters: action.payload.filters,
          values: []
        }
      };
    case COMMENTARY_HIGHLIGHTS_SUCCESS:
      return {
        ...state,
        summary: {
          ...action.payload,
          lastFetched: new Date().getTime().toString(),
          count: action.payload.values.length,
          filters: action.payload.filters
        }
      };
    case COMMENTARY_HIGHLIGHTS_FAILURE:
      return {
        ...state,
        summary: {
          ...state.summary,
          filters: action.payload.filters,
          values: []
        }
      };
    case HIGHLIGHTS_WAGON_STATS:
      return {
        ...state,
        wagonPointsLoading: true,
        wagonPoints: []
      };
    case HIGHLIGHTS_WAGON_STATS_SUCCESS:
      return {
        ...state,
        wagonPointsLoading: false,
        wagonPoints:
          action.payload && action.payload.wagonPoints.length > 0
            ? action.payload.wagonPoints
            : []
      };
    case HIGHLIGHTS_WAGON_STATS_FAILURE:
      return {
        ...state,
        wagonPointsLoading: false,
        wagonPoints: []
      };
    case FETCH_FANFIGHT_PLAYERS:
      return {
        ...state,
        fanFightListLoading: true
      };
    case FETCH_FANFIGHT_PLAYERS_SUCCESS: {
      // let teamsArray = action.payload
      //   ? [action.payload.teamA, action.payload.teamB]
      //   : ["NA", "NA"];
      return {
        ...state,
        fanFightListLoading: false,
        fanFightList:
          action.payload && action.payload.matchDetails
            ? action.payload.matchDetails
            : null
        // teams:
        //   action.payload && action.payload.teamA && action.payload.teamB
        //     ? teamsArray
        //     : []
      };
    }
    case FETCH_FANFIGHT_PLAYERS_FAILURE:
      return {
        ...state,
        fanFightListLoading: false
      };
    case SAVE_FANFIGHT_TEAM:
      return {
        ...state,
        saveFanFightResponse: ""
      };
    case SAVE_FANFIGHT_TEAM_SUCCESS: {
      return {
        ...state,
        saveFanFightResponse: action.payload.status == 200 ? 200 : 400
      };
    }
    case SAVE_FANFIGHT_TEAM_FAILURE:
      return {
        ...state,
        saveFanFightResponse: 400
      };
    case scoreConstants.GET_SCORECARD_SUMMARY:
      return {
        ...state,
        summaryLoading: true
      };
    case scoreConstants.GET_SCORECARD_SUMMARY_SUCCESS:
      return {
        ...state,
        summaryLoading: false,
        scoreSummary: action.payload.data
      };
    case scoreConstants.GET_SCORECARD_SUMMARY_FAILURE:
      return {
        ...state,
        summaryLoading: false
      };
    case scoreConstants.GET_INNINGS_PHASE:
      return {
        ...state,
        inningsPhaseLoading: true
      };
    case scoreConstants.GET_INNINGS_PHASE_SUCCESS:
      return {
        ...state,
        inningsPhaseLoading: false,
        inningsPhase: action.payload.data
      };
    case scoreConstants.GET_INNINGS_PHASE_FAILURE:
      return {
        ...state,
        inningsPhaseLoading: false
      };
    case scoreConstants.GET_LAST_10_BALLS:
      return {
        ...state,
        liveRunLoading: true
      };
    case scoreConstants.GET_LAST_10_BALLS_SUCCESS:
      return {
        ...state,
        last10Balls: action.payload.data,
        liveRunLoading: false
      };
    case scoreConstants.GET_LAST_10_BALLS_FAILURE:
      return {
        ...state,
        liveRunLoading: false
      };
    case scoreConstants.GET_INNING_SUMMARY:
      return {
        ...state,
        inningSummaryLoading: true
      };
    case scoreConstants.GET_INNING_SUMMARY_SUCCESS:
      return {
        ...state,
        inningSummary: action.payload ? action.payload : false,
        inningSummaryLoading: false
      };
    case scoreConstants.GET_INNING_SUMMARY_FAILURE:
      return {
        ...state,
        inningSummaryLoading: false
      };
    default:
      return state;
  }
}

export default scoreDetailsReducer;
