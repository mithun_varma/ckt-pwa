/*
 *
 * ScoreDetails actions
 *
 */
import { showToast } from "utils/misc";

import {
  POLL_FEATURED_SCORES,
  POLL_FEATURED_SCORES_SUCCESS,
  POLL_FEATURED_SCORES_FAILURE,
  STOP_POLL_FEATURED_SCORES,
  POLL_SCORE_DETAILS,
  POLL_SCORE_DETAILS_SUCCESS,
  POLL_SCORE_DETAILS_FAILURE,
  STOP_POLL_SCORE_DETAILS,
  FETCH_FANTASY_BOARD,
  FETCH_FANTASY_BOARD_SUCCESS,
  FETCH_FANTASY_BOARD_FAILURE,
  FETCH_FEATURED_ARTICLES,
  FETCH_FEATURED_ARTICLES_SUCCESS,
  FETCH_FEATURED_ARTICLES_FAILURE,
  GET_COMMENTARY_DATA,
  GET_PAGING_COMMENTARY_DATA,
  COMMENTARY_DATA_FETCH_SUCCESS,
  COMMENTARY_DATA_PAGING_FETCH_SUCCESS,
  COMMENTARY_DATA_POLLING_STOP,
  FETCH_COMMENTARY_OVER_SEPARATOR,
  FETCH_COMMENTARY_OVER_SEPARATOR_SUCCESS,
  FETCH_COMMENTARY_OVER_SEPARATOR_FAILURE,
  STOP_COMMENTARY_OVER_SEPARATOR,
  COMMENTARY_HIGHLIGHTS,
  COMMENTARY_HIGHLIGHTS_SUCCESS,
  COMMENTARY_HIGHLIGHTS_FAILURE,
  GET_PAGING_HIGHLIGHTS_DATA,
  HIGHLIGHTS_DATA_PAGING_FETCH_SUCCESS,
  HIGHLIGHTS_WAGON_STATS,
  HIGHLIGHTS_WAGON_STATS_SUCCESS,
  HIGHLIGHTS_WAGON_STATS_FAILURE,
  FETCH_FANFIGHT_PLAYERS,
  FETCH_FANFIGHT_PLAYERS_SUCCESS,
  FETCH_FANFIGHT_PLAYERS_FAILURE,
  SAVE_FANFIGHT_TEAM,
  SAVE_FANFIGHT_TEAM_SUCCESS,
  SAVE_FANFIGHT_TEAM_FAILURE,
  GET_INNING_SUMMARY,
  GET_INNING_SUMMARY_SUCCESS,
  GET_INNING_SUMMARY_FAILURE,
} from "./constants";
import * as scorecardConstants from "./constants";
const ACTIVE_TOAST = {};

/* Featured Scores actions */
export const pollFeaturedScores = stopPolling => ({
  type: stopPolling ? STOP_POLL_FEATURED_SCORES : POLL_FEATURED_SCORES
});

export const pollFeaturedScoresSuccess = payload => ({
  type: POLL_FEATURED_SCORES_SUCCESS,
  payload
});

export const pollFeaturedScoresFailure = err => {
  showToast(ACTIVE_TOAST, "pollFeaturedScores", "Unable to get scores");
  return {
    type: POLL_FEATURED_SCORES_FAILURE,
    payload: err
  };
};

/* Score details */

export const pollScoreDetails = payload => ({
  type: payload.stopPolling ? STOP_POLL_SCORE_DETAILS : POLL_SCORE_DETAILS,
  payload
});

export const pollScoreDetailsSuccess = payload => ({
  type: POLL_SCORE_DETAILS_SUCCESS,
  payload
});

export const pollScoreDetailsFailure = err => {
  showToast(ACTIVE_TOAST, "pollScoreDetails", "Unable to get score details");
  return {
    type: POLL_SCORE_DETAILS_FAILURE,
    payload: err
  };
};

/* Fantasy Score Board actions */
export const fetchFantasyBoard = payload => ({
  type: FETCH_FANTASY_BOARD,
  payload
});

export const fetchFantasyBoardSuccess = payload => ({
  type: FETCH_FANTASY_BOARD_SUCCESS,
  payload
});

export const fetchFantasyBoardFailure = err => {
  showToast(
    ACTIVE_TOAST,
    "fetchFantasyBoard",
    "Unable to get fantasy score board"
  );
  return {
    type: FETCH_FANTASY_BOARD_FAILURE,
    payload: err
  };
};

/* Featured Articles actions */
export const fetchFeaturedArticles = payload => ({
  type: FETCH_FEATURED_ARTICLES,
  payload
});

export const fetchFeaturedArticlesSuccess = payload => ({
  type: FETCH_FEATURED_ARTICLES_SUCCESS,
  payload
});

export const fetchFeaturedArticlesFailure = err => {
  showToast(
    ACTIVE_TOAST,
    "fetchFeaturedArticles",
    "Unable to get featured articles"
  );
  return {
    type: FETCH_FEATURED_ARTICLES_FAILURE,
    payload: err
  };
};

export const getCommentaryData = payload => ({
  type: GET_COMMENTARY_DATA,
  payload
});
export const getPagingCommentaryData = payload => ({
  type: GET_PAGING_COMMENTARY_DATA,
  payload
});
export const getCommentarySuccess = payload => ({
  type: COMMENTARY_DATA_FETCH_SUCCESS,
  payload
});
export const getCommentaryFailure = () => {
  showToast(ACTIVE_TOAST, "getCommentary", "Unable to fetch commentary");
  return {
    type: FETCH_FEATURED_ARTICLES_FAILURE
  };
};
export const getCommentaryPagingSuccess = payload => ({
  type: COMMENTARY_DATA_PAGING_FETCH_SUCCESS,
  payload
});
export const commentaryStopPollingReq = payload => ({
  type: COMMENTARY_DATA_POLLING_STOP,
  payload
});

/* Get COMMENATRY OVER SEPARATOR All actions */
export const fetchCommentaryOverSeparator = payload => ({
  type: FETCH_COMMENTARY_OVER_SEPARATOR,
  payload
});

export const fetchCommentaryOverSeparatorSuccess = payload => ({
  type: FETCH_COMMENTARY_OVER_SEPARATOR_SUCCESS,
  payload
});

export const fetchCommentaryOverSeparatorFailure = err => {
  showToast(
    ACTIVE_TOAST,
    "fetchCommentaryOverSeparator",
    "Unable to fetch commentary over separator"
  );
  return {
    type: FETCH_COMMENTARY_OVER_SEPARATOR_FAILURE,
    payload: err
  };
};

export const stopCommentaryOverSeparator = payload => ({
  type: STOP_COMMENTARY_OVER_SEPARATOR,
  payload
});

// Commentary Highlights Actions
export const commentaryHighlights = payload => ({
  type: COMMENTARY_HIGHLIGHTS,
  payload
});

export const commentaryHighlightsSuccess = payload => ({
  type: COMMENTARY_HIGHLIGHTS_SUCCESS,
  payload
});

export const commentaryHighlightsFailure = err => {
  showToast(ACTIVE_TOAST, "commentaryHighlights", "Unable to get highlights");
  return {
    type: COMMENTARY_HIGHLIGHTS_FAILURE,
    payload: err
  };
};

export const getPagingHighlightsData = payload => ({
  type: GET_PAGING_HIGHLIGHTS_DATA,
  payload
});

export const getHighlightsPagingSuccess = payload => ({
  type: HIGHLIGHTS_DATA_PAGING_FETCH_SUCCESS,
  payload
});

export const highlightsWagonStats = payload => ({
  type: HIGHLIGHTS_WAGON_STATS,
  payload
});

export const highlightsWagonStatsSuccess = payload => ({
  type: HIGHLIGHTS_WAGON_STATS_SUCCESS,
  payload
});

export const highlightsWagonStatsFailure = err => {
  showToast(
    ACTIVE_TOAST,
    "highlightsWagonStats",
    "Unable to get highlights wagon stats"
  );
  return {
    type: HIGHLIGHTS_WAGON_STATS_FAILURE,
    payload: err
  };
};

// Dekhiye Bhaiya, ye Fan-Fight ka action hai
export const fetchFanFightPlayers = payload => ({
  type: FETCH_FANFIGHT_PLAYERS,
  payload
});

export const fetchFanFightPlayersSuccess = payload => ({
  type: FETCH_FANFIGHT_PLAYERS_SUCCESS,
  payload
});

export const fetchFanFightPlayersFailure = err => {
  showToast(
    ACTIVE_TOAST,
    "fetchFanFightPlayers",
    "Unable to get fan fight players list"
  );
  return {
    type: FETCH_FANFIGHT_PLAYERS_FAILURE,
    payload: err
  };
};

// Save FanFight AAction
export const saveFanFightTeam = payload => ({
  type: SAVE_FANFIGHT_TEAM,
  payload
});

export const saveFanFightTeamSuccess = payload => ({
  type: SAVE_FANFIGHT_TEAM_SUCCESS,
  payload
});

export const saveFanFightTeamFailure = err => {
  showToast(
    ACTIVE_TOAST,
    "saveFanFightTeam",
    "Unable to save fan fight players list"
  );
  return {
    type: SAVE_FANFIGHT_TEAM_FAILURE,
    payload: err
  };
};

//get scorecard summary
export const getScoreCardSummary = payload => ({
  type: scorecardConstants.GET_SCORECARD_SUMMARY,
  payload
});

export const getScoreCardSummarySuccess = payload => ({
  type: scorecardConstants.GET_SCORECARD_SUMMARY_SUCCESS,
  payload
});

export const getScoreCardSummaryFailure = err => {
  return {
    type: scorecardConstants.GET_SCORECARD_SUMMARY_FAILURE,
    payload: err
  };
};

//get last 10 balls data
export const getLast10Balls = payload => ({
  type: scorecardConstants.GET_LAST_10_BALLS,
  payload
});

export const getLast10BallsSuccess = payload => ({
  type: scorecardConstants.GET_LAST_10_BALLS_SUCCESS,
  payload
});

export const getLast10BallsFailure = err => {
  return {
    type: scorecardConstants.GET_LAST_10_BALLS_FAILURE,
    payload: err
  };
};

export const getInnPhase = payload => ({
  type: scorecardConstants.GET_INNINGS_PHASE,
  payload
});

export const getInnPhaseSuccess = payload => ({
  type: scorecardConstants.GET_INNINGS_PHASE_SUCCESS,
  payload
});

export const getInnPhaseFailure = err => {
  return {
    type: scorecardConstants.GET_INNINGS_PHASE_FAILURE,
    payload: err
  };
};

export const stopBallPOll = () => ({
  type: scorecardConstants.STOP_POLL
});

//get INNING summary
export const getInningSummary = payload => ({
  type: scorecardConstants.GET_INNING_SUMMARY,
  payload
});

export const getInningSummarySuccess = payload => ({
  type: scorecardConstants.GET_INNING_SUMMARY_SUCCESS,
  payload
});

export const getInningSummaryFailure = err => {
  return {
    type: scorecardConstants.GET_INNING_SUMMARY_FAILURE,
    payload: err
  };
};
