import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the scoreDetails state domain
 */

const selectScoreDetailsDomain = state => state.scoreDetails || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by ScoreDetails
 */

const makeSelectScoreDetails = () => createSelector(selectScoreDetailsDomain, substate => substate);

export default makeSelectScoreDetails;
export { selectScoreDetailsDomain };
