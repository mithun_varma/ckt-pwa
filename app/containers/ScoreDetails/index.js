/* eslint-disable global-require */
/* eslint no-nested-ternary: 0 */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import * as scoreDetailsActions from "containers/ScoreDetails/actions";
import makeSelectScoreDetails from "./selectors";
import reducer from "./reducer";
import saga from "./saga";

import makeSelectRecord from "../Record/selectors";
import recordReducer from "../Record/reducer";
import * as recordActions from "../Record/actions";
import recordSaga from "../Record/saga";

import { Helmet } from "react-helmet";

//
import DeviceUUID from "device-uuid";
import Cookies from "universal-cookie";

import FanPoll from "components-v2/commonComponent/FanPoll";
import { white, cardShadow, grey_10 } from "../../styleSheet/globalStyle/color";
import TabBar from "components-v2/commonComponent/TabBar";
import HrLine from "components-v2/commonComponent/HrLine";
import LiveScore from "components-v2/screenComponent/scoreCard/LiveScore";
import ButtonBar from "components-v2/commonComponent/ButtonBar";
import Commentary from "../../components-v2/screenComponent/scoreCard/Commentary";
import MatchInfo from "../../components-v2/screenComponent/scoreCard/MatchInfo";
import PlayingXI from "../../components-v2/screenComponent/scoreCard/PlayingXI";
import FeaturedArticles from "../../components-v2/screenComponent/scoreCard/FeaturedArticles";

// import TeamScoreBoard from 'components/ScoreBoard/TeamScoreBoard';
import TeamScoreBoard from "../../components-v2/screenComponent/scoreCard/TeamScoreBoard";
import EmptyState from "../../components-v2/commonComponent/EmptyState";
import ArrowBack from "@material-ui/icons/ArrowBack";
import MiniScoreCard from "../../components-v2/screenComponent/scoreCard/MiniScoreCard";
import ScoreDetailsHeader from "../../components-v2/screenComponent/scoreCard/ScoreDetailsHeader";
import QuickBytes from "../../components-v2/commonComponent/QuickBytes";
import FantasyTab from "../../components-v2/Fantasy/FantasyTab";
import Running from "../../components-v2/Fantasy/Running";
// import FantasyPlayers from "../../components-v2/Fantasy/FantasyPlayers";
import FactEngines from "../../components-v2/commonComponent/FactEngines";
import isEmpty from "lodash/isEmpty";
import BackgroundComponent from "../../components-v2/commonComponent/BackgroundComponent";
import Sticky from "react-stickynode";
import ReactGA from "react-ga";
import FanFight from "../../components-v2/commonComponent/FanFight";

const cookies = new Cookies();
// const scheduleMatchTabs = ["MATCH DETAILS", "FEATURED ARTICLES"];
const liveMatchTabs = [
  "Live",
  "Scorecard",
  "Highlights",
  "Fantasy",
  "Articles",
  "Match Info"
];

const pastMatchTabs = [
  "Scorecard",
  "Commentary",
  "Highlights",
  "Fantasy",
  "Articles",
  "Match Info"
];

const scheduleMatchTabs = [
  "Probable Playing XI",
  "Match Info",
  "Fantasy",
  "Articles"
];

const scheduleMatchTabsWithoutSquad = ["Match Info", "Fantasy", "Articles"];

const scheduleAfterTossMatchTabs = [
  "Playing XI",
  "Match Info",
  "Fantasy",
  "Articles"
];

const COMMENTARY_CONFIG = ["BALL BY BALL", "OVER BY OVER"];
const TAGS_CONFIG = [
  { label: "All", key: "all", filters: ["wicket", "four", "six", "milestone"] },
  { label: "Fours", key: "four", filters: ["four"] },
  { label: "Sixes", key: "six", filters: ["six"] },
  { label: "Wickets", key: "wicket", filters: ["wicket"] }
  // { label: "Milestones", key: "milestone", filters: ["milestone"] }
];
const WAGON_FILTERS = {
  all: ["four", "six", "wicket"],
  four: ["four"],
  six: ["six"],
  wicket: ["wicket"]
};
let scrollTop = null;
export class ScoreDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // activeTab: "Match Info",
      launchFirstTime: true,
      activeTab: "",
      activeKey: "all",
      activeTabAlreadySet: false,
      activeButton: "BALL BY BALL",
      initial: true,
      isHeaderBackground: false,
      showGroundGraph: {},
      ballKey: "",
      titlePlaced: false,
      fanPoll: "",
      // fanPoll: pollDataMock,
      matchId: "",
      isPollResultFetched: false
    };
    this.timeout = "";
  }

  showGroundGraph = key => {
    this.setState({
      showGroundGraph: {
        [key]: !this.state.showGroundGraph[key],
        ballKey: key
      }
    });
  };

  handlePollPost = (id, option) => {
    this.props.postPollOption({
      param: {
        id,
        option,
        uniqueId: DeviceUUID.DeviceUUID().get()
      },
      type: "scorecard"
    });
  };

  componentDidMount() {
    // ReactGA.initialize("UA-136526387-1");
    // ReactGA.pageview(
    //   "https://www.cricket.com" +
    //     window.location.pathname +
    //     window.location.search
    // );
    window.addEventListener("scroll", this.handleScroll);
    this.props.fetchFanFightPlayers({
      matchId: this.props.match.params.matchId
    });
    if (this.state.matchId != this.props.match.params.matchId) {
      this.setState({
        matchId: this.props.match.params.matchId
      });
    }
    this.props.pollScoreDetails({
      matchId: this.props.match.params.matchId,
      stopPolling: false
    });
    this.props.getCommentaryData({
      params: { matchId: this.props.match.params.matchId, skip: 0, limit: 10 },
      isCommentry: true
    });
    // this.props.saveFanFightTeam({
    //   payload: {
    //     crictecMatchId: "123456",
    //     ffPlayers: [
    //       {
    //         ffPlayerId: "ff123",
    //         crictecId: "cc123",
    //         fullName: "virat kohli",
    //         shortName: "virat",
    //         displayName: "kohli",
    //         source: "",
    //         group: "",
    //         playerRole: "bowler",
    //         playerCredits: 10.5,
    //         status: "",
    //         teamId: "",
    //         minScore: "",
    //         maxScore: ""
    //       }
    //     ]
    //   }
    // });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const teamKeys =
      nextProps.scoreDetails.scoreDetailCards[
        this.props.match.params.matchId
      ] &&
      nextProps.scoreDetails.scoreDetailCards[this.props.match.params.matchId]
        .teams
        ? Object.keys(
            nextProps.scoreDetails.scoreDetailCards[
              this.props.match.params.matchId
            ].teams
          )
        : [];
    const firstTeam =
      nextProps.scoreDetails.scoreDetailCards[
        this.props.match.params.matchId
      ] &&
      nextProps.scoreDetails.scoreDetailCards[this.props.match.params.matchId]
        .firstBatting
        ? nextProps.scoreDetails.scoreDetailCards[
            this.props.match.params.matchId
          ].firstBatting
        : teamKeys[0];

    if (this.state.launchFirstTime) {
      // console.log("first time", state);
      if (
        nextProps.scoreDetails.scoreDetailsMiniCards[
          this.props.match.params.matchId
        ]
      ) {
        this.setState({
          fanPoll:
            nextProps.scoreDetails.scoreDetailsMiniCards[
              this.props.match.params.matchId
            ].pollsData,
          // fanPoll: pollDataMock,
          launchFirstTime: false
        });
      }
    }

    if (!this.state.isPollResultFetched && this.state.fanPoll) {
      const lastPollId = cookies.get("scoreResultIds");
      if (lastPollId && lastPollId.indexOf(this.state.fanPoll.pollId) > -1) {
        this.props.getPollById({
          pollId: lastPollId[lastPollId.indexOf(this.state.fanPoll.pollId)]
        });
        this.setState({
          isPollResultFetched: true
        });
      }
    }

    if (
      // false &&
      (!this.state.activeTabAlreadySet &&
        nextProps.scoreDetails &&
        nextProps.scoreDetails.scoreDetailCards[
          nextProps.match.params.matchId
        ] &&
        nextProps.scoreDetails.scoreDetailCards[nextProps.match.params.matchId]
          .status) ||
      (this.props.scoreDetails.scoreDetailCards[
        nextProps.match.params.matchId
      ] &&
        nextProps.scoreDetails.scoreDetailCards[
          nextProps.match.params.matchId
        ] &&
        this.props.scoreDetails.scoreDetailCards[nextProps.match.params.matchId]
          .status !=
          nextProps.scoreDetails.scoreDetailCards[
            nextProps.match.params.matchId
          ].status)
    ) {
      if (
        nextProps.scoreDetails.scoreDetailCards[nextProps.match.params.matchId]
          .status === "started" &&
        nextProps.scoreDetails.scoreDetailCards[nextProps.match.params.matchId]
          .firstBallStatus === "RUNNING"
      ) {
        this.setState({
          activeTabAlreadySet: true,
          activeTab: "Live"
        });
      } else if (
        nextProps.scoreDetails.scoreDetailCards[nextProps.match.params.matchId]
          .status === "notstarted"
      ) {
        this.setState({
          activeTabAlreadySet: true,
          // activeTab: "Match Info"
          activeTab:
            nextProps.scoreDetails.scoreDetailCards[
              nextProps.match.params.matchId
            ].status === "started" &&
            nextProps.scoreDetails.scoreDetailCards[
              nextProps.match.params.matchId
            ].teams &&
            nextProps.scoreDetails.scoreDetailCards[
              nextProps.match.params.matchId
            ].teams[firstTeam] &&
            nextProps.scoreDetails.scoreDetailCards[
              nextProps.match.params.matchId
            ].teams[firstTeam].playingElevenDeclared
              ? "Playing XI"
              : true &&
                nextProps.scoreDetails.scoreDetailCards[
                  nextProps.match.params.matchId
                ] &&
                nextProps.scoreDetails.scoreDetailCards[
                  nextProps.match.params.matchId
                ].teams &&
                nextProps.scoreDetails.scoreDetailCards[
                  nextProps.match.params.matchId
                ].teams[firstTeam] &&
                nextProps.scoreDetails.scoreDetailCards[
                  nextProps.match.params.matchId
                ].teams[firstTeam].probablePlayingXi.length < 1
                ? "Match Info"
                : "Probable Playing XI"
        });
      } else if (
        nextProps.scoreDetails.scoreDetailCards[nextProps.match.params.matchId]
          .status === "started" &&
        nextProps.scoreDetails.scoreDetailCards[nextProps.match.params.matchId]
          .firstBallStatus === "UPCOMING"
      ) {
        this.setState({
          activeTabAlreadySet: true,
          // activeTab: "Match Info"
          activeTab:
            nextProps.scoreDetails.scoreDetailCards[
              nextProps.match.params.matchId
            ].status === "started" &&
            nextProps.scoreDetails.scoreDetailCards[
              nextProps.match.params.matchId
            ].teams &&
            nextProps.scoreDetails.scoreDetailCards[
              nextProps.match.params.matchId
            ].teams[firstTeam] &&
            nextProps.scoreDetails.scoreDetailCards[
              nextProps.match.params.matchId
            ].teams[firstTeam].playingElevenDeclared
              ? "Playing XI"
              : "Probable Playing XI"
        });
      } else {
        this.setState(
          {
            activeTabAlreadySet: true,
            activeTab: "Scorecard"
          },
          () => {
            // this.props.stopCommentaryOverSeparator(); // Stoping OverSeparator calling
          }
        );
      }
    }

    if (
      this.state.launchFirstTime &&
      this.props.match.params.type &&
      (this.props.match.params.type == "articles" ||
        this.props.match.params.type == "fantasy")
    ) {
      this.setState({
        activeTab:
          this.props.match.params.type.charAt(0).toUpperCase() +
          this.props.match.params.type.slice(1)
      });
    }
  }

  componentWillUnmount() {
    this.props.pollScoreDetails({ stopPolling: true });
    this.props.commentaryStopPollingReq(); // Stoping current polling
    this.props.stopCommentaryOverSeparator(); // Stoping OverSeparator calling
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = event => {
    ({ scrollTop } = event.target.scrollingElement);
    // console.log("object: ", scrollTop);
    if (scrollTop > 172 && !this.state.titlePlaced) {
      this.setState({
        isHeaderBackground: true,
        titlePlaced: true
      });
    } else if (scrollTop < 173) {
      this.setState({
        isHeaderBackground: false,
        titlePlaced: false
      });
    }
  };

  handleActiveTab = activeTab => {
    this.setState(
      {
        activeTab,
        activeButton:
          activeTab == "Commentary" || activeTab == "Live"
            ? "BALL BY BALL"
            : this.state.activeButton
      },
      () => {
        if (activeTab != "Live") {
          this.props.commentaryStopPollingReq(); // Stoping current polling
          // this.props.stopCommentaryOverSeparator(); // Stoping OverSeparator calling
        }

        if (activeTab == "Fantasy" || activeTab == "Articles") {
          this.props.history.replace(
            `/score/${
              this.props.match.params.matchId
            }/${activeTab.toLowerCase()}`
          );
        }
        //  else {
        //   this.props.history.push(`/score/${this.props.match.params.matchId}`);
        // }

        if (this.timeout) clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
          if (activeTab === "Commentary" || activeTab == "Live") {
            setTimeout(() => {
              this.props.getCommentaryData({
                params: { matchId: this.props.match.params.matchId, limit: 10 },
                isCommentry: true
              });
            }, 10);
            // this.props.commentaryStopPollingReq(); // Stoping current polling
            // this.props.stopCommentaryOverSeparator(); // Stoping OverSeparator calling
          } else if (activeTab === "Highlights") {
            setTimeout(() => {
              // this.props.getCommentaryData({
              //   params: {
              //     matchId: this.props.match.params.matchId,
              //     // highlights: this.state.initial
              //     //   ? []
              //     //   : this.props.scoreDetails.summary.filters.highlights,
              //     highlights: TAGS_CONFIG[0].filters
              //   }
              // });
              // this.props.scoreDetails.scoreDetailCards[this.props.match.params.matchId].firstBatting
              this.props.scoreDetails.scoreDetailCards[
                this.props.match.params.matchId
              ] &&
                this.props.scoreDetails.scoreDetailCards[
                  this.props.match.params.matchId
                ].now &&
                this.props.scoreDetails.scoreDetailCards[
                  this.props.match.params.matchId
                ].now.battingTeam &&
                this.props.scoreDetails.scoreDetailCards[
                  this.props.match.params.matchId
                ].battingOrder &&
                this.props.commentaryHighlights({
                  params: {
                    matchId: this.props.match.params.matchId,
                    skip: 0,
                    limit: 10,
                    // teamKey: this.props.scoreDetails.scoreDetailCards[
                    //   this.props.match.params.matchId
                    // ].firstBatting,
                    teamKey: this.props.scoreDetails.scoreDetailCards[
                      this.props.match.params.matchId
                    ].now.battingTeam,
                    // inningNumber: 1,
                    inningNumber: this.props.scoreDetails.scoreDetailCards[
                      this.props.match.params.matchId
                    ].battingOrder.length,
                    highlights: TAGS_CONFIG[0].filters
                  }
                });

              this.props.highlightsWagonStats({
                matchId: this.props.match.params.matchId,
                // teamKey: this.props.scoreDetails.scoreDetailCards[
                //   this.props.match.params.matchId
                // ].firstBatting,
                teamKey: this.props.scoreDetails.scoreDetailCards[
                  this.props.match.params.matchId
                ].now.battingTeam,
                // inningNumber: 1,
                inningNumber: this.props.scoreDetails.scoreDetailCards[
                  this.props.match.params.matchId
                ].battingOrder.length,
                type: WAGON_FILTERS.all
              });

              this.setState({
                initial: false
              });
            }, 10);
            this.props.commentaryStopPollingReq(); // Stoping current polling
            // this.props.stopCommentaryOverSeparator(); // Stoping OverSeparator calling
          } else if (activeTab === "Fantasy") {
            // this.props.commentaryStopPollingReq(); // Stoping current polling
            // this.props.stopCommentaryOverSeparator(); // Stoping OverSeparator calling
            // this.props.fetchFantasyBoard({
            //   matchId: this.props.match.params.matchId
            // });
          }
        });
      }
    );
  };

  handleActiveButton = activeButton => {
    this.setState({
      activeButton
    });
  };

  render() {
    const {
      history,
      scoreDetails,
      record,
      match,
      fetchFeaturedArticles,
      getCommentaryData,
      commentaryStopPollingReq,
      fetchCommentaryOverSeparator,
      commentaryHighlights,
      highlightsWagonStats
    } = this.props;
    let teamorder = null;
    // console.log("sc: ", this.props, window);
    const lastPollId = cookies.get("scoreResultIds");
    const score =
      scoreDetails.scoreDetailsMiniCards[this.props.match.params.matchId];
    const selectedOption = cookies.get("scoreSelectedOptions");
    const { activeTab } = this.state;
    const title =
      scoreDetails.scoreDetailsMiniCards[this.props.match.params.matchId] &&
      scoreDetails.scoreDetailsMiniCards[this.props.match.params.matchId]
        .matchShortName
        ? scoreDetails.scoreDetailsMiniCards[this.props.match.params.matchId]
            .matchShortName
        : "-";

    const matchNumber =
      scoreDetails.scoreDetailsMiniCards[this.props.match.params.matchId] &&
      scoreDetails.scoreDetailsMiniCards[this.props.match.params.matchId]
        .relatedName
        ? `, ${
            scoreDetails.scoreDetailsMiniCards[this.props.match.params.matchId]
              .relatedName
          }`
        : "";
    const teamKeys =
      scoreDetails.scoreDetailCards[this.props.match.params.matchId] &&
      scoreDetails.scoreDetailCards[this.props.match.params.matchId].teams
        ? Object.keys(
            scoreDetails.scoreDetailCards[this.props.match.params.matchId].teams
          )
        : [];
    const firstTeam =
      scoreDetails.scoreDetailCards[this.props.match.params.matchId] &&
      scoreDetails.scoreDetailCards[this.props.match.params.matchId]
        .firstBatting
        ? scoreDetails.scoreDetailCards[this.props.match.params.matchId]
            .firstBatting
        : teamKeys[0];
    if (
      !scoreDetails.scoreDetailsMiniCards[this.props.match.params.matchId] ||
      isEmpty(
        scoreDetails.scoreDetailsMiniCards[this.props.match.params.matchId]
      )
    ) {
      return (
        <div
          style={{
            display: "flex",
            height: "89vh",
            alignItems: "center"
          }}
        >
          <div />
          {false && (
            <EmptyState
              img={require("../../images/emptyState.svg")}
              imageStyle={{
                width: "60%",
                height: "auto"
              }}
              msg="Oops! something went wrong"
              msgStyle={{
                marginTop: 10
              }}
            />
          )}
        </div>
      );
    }

    return (
      <div>
        <Helmet titleTemplate="%s | cricket.com">
          <title>{`${score && score.matchShortName} | ${score &&
            score.teamA.name} vs ${score &&
            score.teamB.name} | match on ${score &&
            score.startDate &&
            moment(score.startDate).format("DD-MMM-YYYY")}`}</title>
          <meta
            name="description"
            content={` Get ball by ball commentary, live score, match info, scorecard, and full highlights of ${score &&
              score.matchShortName}, ${score && score.seriesName} - ${score &&
              score.relatedName}. Check out the complete cricket news of the match on cricket.com.`}
          />
          <meta
            name="keywords"
            content={`${score && score.teamA.name},${score &&
              score.teamB.name},${score && score.matchShortName},${score &&
              score.matchShortName} live score, ${score &&
              score.matchShortName} scorecard`}
          />
          <link
            rel="canonical"
            href={`www.cricket.com${history &&
              history.location &&
              history.location.pathname}`}
          />
          {/* <link rel="apple-touch-icon" href="http://mysite.com/img/apple-touch-icon-57x57.png" /> */}
        </Helmet>
        <BackgroundComponent
          rootStyles={{
            height: 164
          }}
          titleStyles={{
            marginTop: 34
          }}
        />
        <div
          style={{
            padding: "0px 16px 16px"
          }}
        >
          {/* Header */}
          {this.state.isHeaderBackground && (
            <ScoreDetailsHeader
              history={history}
              leftIconOnClick={() => {
                history.goBack();
              }}
              isHeaderBackground={this.state.isHeaderBackground}
              score={
                scoreDetails.scoreDetailsMiniCards[
                  this.props.match.params.matchId
                ]
              }
              redirectToCriclytics={() => {
                history.push(
                  `/criclytics-slider/${this.props.match.params.matchId}`
                );
              }}
            />
          )}

          {false && (
            <div
              style={{
                fontFamily: "Montserrat",
                fontWeight: 700,
                fontSize: 22,
                color: grey_10,
                marginBottom: 12
              }}
            >
              {title + matchNumber}
            </div>
          )}

          {/* Header closing */}
          <div
            style={{
              marginBottom: 12,
              marginTop: -150
            }}
          >
            <div
              style={{
                fontFamily: "Montserrat",
                fontWeight: 600,
                fontSize: 20,
                color: white,
                marginBottom: 12,
                display: "flex",
                alignItems: "flex-start"
              }}
            >
              <ArrowBack
                style={{ color: white, margin: "3px 8px 0 -4px" }}
                onClick={() => {
                  history.goBack();
                }}
              />
              <span
                style={{
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                  whiteSpace: "nowrap"
                }}
              >
                {title + matchNumber}
              </span>
            </div>

            {scoreDetails.scoreDetailsMiniCards[match.params.matchId] && (
              <MiniScoreCard
                score={scoreDetails.scoreDetailsMiniCards[match.params.matchId]}
                history={history}
                matchId={this.props.match.params.matchId}
              />
            )}
          </div>

          <div
            style={{
              background: white,
              boxShadow: cardShadow,
              borderRadius: 3
            }}
          >
            {scoreDetails.scoreDetailCards[this.props.match.params.matchId] && (
              <Sticky
                enabled
                top={56}
                // bottomBoundary={3000}
                innerZ={999}
                activeClass="bottomBoxSch"
              >
                <TabBar
                  itemsOld={
                    scoreDetails.scoreDetailCards[
                      this.props.match.params.matchId
                    ].status === "notstarted"
                      ? scheduleMatchTabs
                      : scoreDetails.scoreDetailCards[
                          this.props.match.params.matchId
                        ].status === "started"
                        ? liveMatchTabs
                        : pastMatchTabs
                  }
                  items={
                    scoreDetails.scoreDetailCards[
                      this.props.match.params.matchId
                    ].status === "notstarted" &&
                    scoreDetails.scoreDetailCards[
                      this.props.match.params.matchId
                    ].teams[firstTeam].probablePlayingXi.length < 1
                      ? scheduleMatchTabsWithoutSquad
                      : scoreDetails.scoreDetailCards[
                          this.props.match.params.matchId
                        ].status === "notstarted" &&
                        scoreDetails.scoreDetailCards[
                          this.props.match.params.matchId
                        ].teams[firstTeam].probablePlayingXi.length > 1
                        ? scheduleMatchTabs
                        : scoreDetails.scoreDetailCards[
                            this.props.match.params.matchId
                          ].status === "started" &&
                          scoreDetails.scoreDetailCards[
                            this.props.match.params.matchId
                          ].firstBallStatus === "RUNNING"
                          ? liveMatchTabs
                          : scoreDetails.scoreDetailCards[
                              this.props.match.params.matchId
                            ].status === "started" &&
                            scoreDetails.scoreDetailCards[
                              this.props.match.params.matchId
                            ].teams &&
                            scoreDetails.scoreDetailCards[
                              this.props.match.params.matchId
                            ].teams[firstTeam] &&
                            scoreDetails.scoreDetailCards[
                              this.props.match.params.matchId
                            ].teams[firstTeam].playingElevenDeclared
                            ? scheduleAfterTossMatchTabs
                            : scoreDetails.scoreDetailCards[
                                this.props.match.params.matchId
                              ].status === "started" &&
                              scoreDetails.scoreDetailCards[
                                this.props.match.params.matchId
                              ].teams &&
                              scoreDetails.scoreDetailCards[
                                this.props.match.params.matchId
                              ].teams[firstTeam] &&
                              !scoreDetails.scoreDetailCards[
                                this.props.match.params.matchId
                              ].teams[firstTeam].playingElevenDeclared
                              ? scheduleMatchTabs
                              : pastMatchTabs
                  }
                  activeItem={this.state.activeTab}
                  onClick={this.handleActiveTab}
                />
              </Sticky>
            )}
            {activeTab == "Live" && (
              <LiveScore
                history={history}
                score={
                  scoreDetails.scoreDetailCards[this.props.match.params.matchId]
                }
              />
            )}
            {activeTab == "Match Info" && (
              <div>
                <MatchInfo
                  history={history}
                  score={
                    scoreDetails.scoreDetailCards[
                      this.props.match.params.matchId
                    ] || {}
                  }
                />
              </div>
            )}

            {(activeTab == "Probable Playing XI" ||
              activeTab == "Playing XI") && (
              <div>
                <PlayingXI
                  history={history}
                  score={
                    scoreDetails.scoreDetailCards[
                      this.props.match.params.matchId
                    ] || {}
                  }
                />
                <div>
                  <FanFight
                    rootStyles={{
                      margin: "10px 0"
                    }}
                    url={
                      activeTab == "Probable Playing XI"
                        ? "https://fanfight.com/?utm_source=cdc&utm_medium=P11pretoss&utm_campaign=CDC_P11pretoss/"
                        : activeTab == "Playing XI"
                          ? "https://fanfight.com/?utm_source=cdc&utm_medium=P11_post_toss&utm_campaign=CDC_P11_post_toss/"
                          : ""
                    }
                  />
                </div>
                {false && (
                  <MatchInfo
                    history={history}
                    score={
                      scoreDetails.scoreDetailCards[
                        this.props.match.params.matchId
                      ] || {}
                    }
                    onlyPlayingXI
                  />
                )}
              </div>
            )}

            {activeTab == "Scorecard" && (
              <div>
                <TeamScoreBoard
                  history={history}
                  matchId={match.params.matchId}
                  score={scoreDetails.scoreDetailCards[match.params.matchId]}
                />
              </div>
            )}

            {activeTab == "Articles" && (
              <div>
                <FeaturedArticles
                  history={this.props.history}
                  matchId={match.params.matchId}
                  articles={scoreDetails.featuredArticles}
                  fetchFeaturedArticles={fetchFeaturedArticles}
                  loading={scoreDetails.featuredArticlesLoading}
                  articleHost={scoreDetails.articleHost}
                />
              </div>
            )}

            {activeTab == "Fantasy" &&
              // .matchStatus === "UPCOMING" && (
              scoreDetails.scoreDetailsMiniCards[match.params.matchId]
                .firstBallStatus != "RUNNING" &&
              scoreDetails.scoreDetailsMiniCards[match.params.matchId]
                .firstBallStatus != "COMPLETED" &&
              scoreDetails.scoreDetailsMiniCards[match.params.matchId]
                .firstBallStatus != "BETWEEN_INNINGS" && (
                <div>
                  <FantasyTab
                    msg="Fantasy not found"
                    history={this.props.history}
                    saveFanFightTeam={this.props.saveFanFightTeam}
                    scoreDetails={this.props.scoreDetails}
                  />
                </div>
              )}
            {false &&
              activeTab == "Fantasy" &&
              scoreDetails.scoreDetailsMiniCards[match.params.matchId]
                .matchStatus === "COMPLETED" && (
                <div>
                  <FantasyTab
                    msg="Fantasy not found"
                    history={this.props.history}
                    saveFanFightTeam={this.props.saveFanFightTeam}
                    scoreDetails={this.props.scoreDetails}
                  />
                  {/* <Running
                    msg="Fantasy not found"
                    history={this.props.history}
                    saveFanFightTeam={this.props.saveFanFightTeam}
                    scoreDetails={this.props.scoreDetails}
                    {...this.props}
                  /> */}
                </div>
              )}
            {activeTab == "Fantasy" && // .matchStatus == "RUNNING" && (
              (scoreDetails.scoreDetailsMiniCards[match.params.matchId]
                .firstBallStatus == "RUNNING" ||
                scoreDetails.scoreDetailsMiniCards[match.params.matchId]
                  .matchStatus == "COMPLETED" ||
                scoreDetails.scoreDetailsMiniCards[match.params.matchId]
                  .firstBallStatus == "BETWEEN_INNINGS") && (
                <div>
                  <Running
                    msg="Fantasy not found"
                    history={this.props.history}
                    saveFanFightTeam={this.props.saveFanFightTeam}
                    scoreDetails={this.props.scoreDetails}
                  />
                    {/* <FantasyTab
                    msg="Fantasy not found"
                    history={this.props.history}
                    saveFanFightTeam={this.props.saveFanFightTeam}
                    scoreDetails={this.props.scoreDetails}
                  /> */}
                </div>
              )}
          </div>

          {activeTab == "Live" && (
            <div>
              {scoreDetails.scoreDetailsMiniCards[match.params.matchId] &&
                scoreDetails.scoreDetailsMiniCards[match.params.matchId]
                  .quickByte && (
                  <div
                    style={{
                      marginTop: 10,
                      boxShadow: cardShadow
                    }}
                  >
                    <QuickBytes
                      // data={scoreDetails.scoreDetailsMiniCards[match.params.matchId].toss}
                      data={
                        scoreDetails.scoreDetailsMiniCards[match.params.matchId]
                          .quickByte
                      }
                      disableClick={
                        window.location.href ==
                        scoreDetails.scoreDetailsMiniCards[match.params.matchId]
                          .quickByte.link
                      }
                      rootStyles={{ margin: 0 }}
                    />
                  </div>
                )}
              {false &&
                scoreDetails.scoreDetailsMiniCards[match.params.matchId] &&
                scoreDetails.scoreDetailsMiniCards[match.params.matchId]
                  .pollsData && (
                  <div
                    style={{
                      marginTop: 10,
                      boxShadow: cardShadow
                    }}
                  >
                    <FanPoll
                      pollData={
                        scoreDetails.scoreDetailsMiniCards[match.params.matchId]
                          .pollsData
                      }
                      pollResult={record.pollResult}
                      isResult={
                        lastPollId
                          ? lastPollId.indexOf(this.state.fanPoll.pollId) > -1
                          : false || record.isPollResult
                      }
                      handlePoll={this.handlePollPost}
                      loading={record.postPollLoading}
                      selectedOption={
                        selectedOption
                          ? selectedOption[`${this.state.fanPoll.pollId}`]
                          : ""
                      }
                      rootStyles={{
                        margin: 0
                      }}
                    />
                  </div>
                )}
              {false &&
                scoreDetails.scoreDetailsMiniCards[match.params.matchId] &&
                scoreDetails.scoreDetailsMiniCards[match.params.matchId]
                  .factEngine && (
                  <div
                    style={{
                      marginTop: 10,
                      boxShadow: cardShadow
                    }}
                  >
                    <FactEngines
                      data={
                        scoreDetails.scoreDetailsMiniCards[match.params.matchId]
                          .factEngine
                      }
                      rootStyles={{
                        margin: 0
                      }}
                    />
                  </div>
                )}
            </div>
          )}

          {/* Commentry Card Section */}
          {(activeTab == "Live" || activeTab == "Commentary") && (
            <div
              style={{
                background: white,
                borderRadius: activeTab == "Commentary" ? "none" : 3,
                boxShadow:
                  activeTab == "Commentary"
                    ? "0px 3px 2px 0px rgba(19, 0, 0, 0.1)"
                    : cardShadow,
                marginTop: activeTab == "Commentary" ? 0 : 12
              }}
            >
              {false &&
                activeTab == "Commentary" &&
                scoreDetails.scoreDetailsMiniCards[match.params.matchId] &&
                scoreDetails.scoreDetailsMiniCards[match.params.matchId]
                  .quickByte && (
                  <div
                    style={{
                      background: "#edeff4",
                      padding: " 10px 0"
                    }}
                  >
                    <QuickBytes
                      // data={scoreDetails.scoreDetailsMiniCards[match.params.matchId].toss}
                      data={
                        scoreDetails.scoreDetailsMiniCards[match.params.matchId]
                          .quickByte
                      }
                      disableClick={
                        window.location.href ==
                        scoreDetails.scoreDetailsMiniCards[match.params.matchId]
                          .quickByte.link
                      }
                      rootStyles={{ margin: 0 }}
                    />
                  </div>
                )}
              <div style={{ padding: "8px 0 16px" }}>
                <ButtonBar
                  items={COMMENTARY_CONFIG}
                  activeItem={this.state.activeButton}
                  onClick={this.handleActiveButton}
                />
              </div>
              <HrLine />
              {/* Over Summary */}
              <Commentary
                data={
                  this.state.activeTab === "Highlights"
                    ? scoreDetails.summary
                    : scoreDetails.commentary
                }
                // score={scoreDetails.scoreDetailCards}
                score={scoreDetails.scoreDetailCards[match.params.matchId] || false}
                overSeparator={scoreDetails.overSeparator}
                fetchCommentaryOverSeparator={fetchCommentaryOverSeparator}
                dispatch={this.props.dispatch}
                match={this.props.match}
                getCommentaryData={getCommentaryData}
                activeTab={"Commentary"}
                commentaryFilter={this.state.activeButton}
                matchId={this.props.match.params.matchId}
                commentaryType={this.state.activeButton}
                commentaryStopPollingReq={commentaryStopPollingReq}
                showGroundGraph={this.showGroundGraph}
                showGroundGraphFlag={this.state.showGroundGraph}
                getInningSummary={this.props.getInningSummary}
                inningSummary={this.props.scoreDetails.inningSummary}
              />
            </div>
          )}
          {/* Commentry Card Section Closing */}
          {activeTab == "Highlights" && (
            <div
              style={{
                background: white,
                borderRadius: 3,
                boxShadow: cardShadow,
                marginTop: 12
              }}
            >
              {/* Over Summary */}
              <Commentary
                data={
                  this.state.activeTab === "Highlights"
                    ? scoreDetails.summary
                    : scoreDetails.commentary
                }
                score={
                  scoreDetails.scoreDetailCards[match.params.matchId] || false
                }
                wagonPoints={scoreDetails.wagonPoints}
                wagonPointsLoading={scoreDetails.wagonPointsLoading}
                overSeparator={scoreDetails.overSeparator}
                activeTab={this.state.activeTab}
                dispatch={this.props.dispatch}
                match={this.props.match}
                getCommentaryData={getCommentaryData}
                commentaryHighlights={commentaryHighlights}
                highlightsWagonStats={highlightsWagonStats}
                activeTab={"Highlights"}
                commentaryFilter={this.state.activeButton}
                matchId={this.props.match.params.matchId}
                commentaryType={this.state.activeButton}
                commentaryStopPollingReq={commentaryStopPollingReq}
                showGroundGraph={this.showGroundGraph}
                showGroundGraphFlag={this.state.showGroundGraph}
                getInningSummary={this.props.getInningSummary}
                inningSummary={this.props.scoreDetails.inningSummary}
              />
            </div>
          )}
        </div>
      </div>
    );
  }
}

ScoreDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  scoreDetails: PropTypes.object.isRequired,
  pollScoreDetails: PropTypes.func.isRequired,
  fetchFantasyBoard: PropTypes.func.isRequired,
  fetchFeaturedArticles: PropTypes.func.isRequired,
  getCommentaryData: PropTypes.func.isRequired,
  commentaryStopPollingReq: PropTypes.func.isRequired,
  fetchCommentaryOverSeparator: PropTypes.func,
  highlightsWagonStats: PropTypes.func,
  match: PropTypes.object.isRequired
};

const mapStateToProps = createStructuredSelector({
  scoreDetails: makeSelectScoreDetails(),
  record: makeSelectRecord()
});

function mapDispatchToProps(dispatch) {
  return {
    pollScoreDetails: payload =>
      dispatch(scoreDetailsActions.pollScoreDetails(payload)),
    fetchFantasyBoard: payload =>
      dispatch(scoreDetailsActions.fetchFantasyBoard(payload)),
    fetchFeaturedArticles: payload =>
      dispatch(scoreDetailsActions.fetchFeaturedArticles(payload)),
    getCommentaryData: (payload, filters) =>
      dispatch(scoreDetailsActions.getCommentaryData(payload, filters)),
    commentaryStopPollingReq: payload =>
      dispatch(scoreDetailsActions.commentaryStopPollingReq(payload)),
    fetchCommentaryOverSeparator: payload =>
      dispatch(scoreDetailsActions.fetchCommentaryOverSeparator(payload)),
    stopCommentaryOverSeparator: payload =>
      dispatch(scoreDetailsActions.stopCommentaryOverSeparator(payload)),
    commentaryHighlights: payload =>
      dispatch(scoreDetailsActions.commentaryHighlights(payload)),
    highlightsWagonStats: payload =>
      dispatch(scoreDetailsActions.highlightsWagonStats(payload)),
    fetchFanFightPlayers: payload =>
      dispatch(scoreDetailsActions.fetchFanFightPlayers(payload)),
    saveFanFightTeam: payload =>
      dispatch(scoreDetailsActions.saveFanFightTeam(payload)),
    postPollOption: payload => dispatch(recordActions.postPoll(payload)),
    getPollById: payload => dispatch(recordActions.getPollByPollId(payload)),
    getScoreSummary: payload =>
      dispatch(scoreDetailsActions.getScoreCardSummary(payload)),
    getInningSummary: payload =>
      dispatch(scoreDetailsActions.getInningSummary(payload)),
    dispatch
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "scoreDetails", reducer });
const withSaga = injectSaga({ key: "scoreDetails", saga });
// ============
const withRecordReducer = injectReducer({
  key: "record",
  reducer: recordReducer
});
const withRecordSaga = injectSaga({ key: "record", saga: recordSaga });
// ============

export default compose(
  withReducer,
  withSaga,
  withRecordReducer,
  withRecordSaga,
  withConnect
)(ScoreDetails);

const pollDataMock = {
  pollId: 10,
  crictecMatchId:
    "2019232-1-chennai-super-kings-vs-royal-challengers-bangalore",
  name:
    "Chennai Super Kings vs Royal Challengers Bangalore Sat Mar 23 14:30:00 GMT 2019",
  startDate: "2019-03-23T16:12:57.804+0000",
  endDate: "2019-03-24T16:12:57.804+0000",
  display: "both",
  question: "How many balls will CSK take to wrap up this match?",
  option1: "40-50",
  option2: "51-75",
  option3: "75-plus",
  endPoll: false,
  option1Count: 1,
  option2Count: 4,
  option3Count: 3,
  matchName: null,
  totalPolls: 8,
  uniqueIds: [
    "cd07b9b7-61e9-473a-b12e-9d8f5dadbd71",
    "8e5604af-99db-47a5-b5e9-465040486690",
    "df9ab08a-44a4-41dd-bf9e-ffd6adeb77eb",
    "f227ed1a-3ddf-4f42-bbea-606440e1ccb8",
    "6692de98-d63f-446a-b920-b56f1cfe6933",
    "c6759800-3626-43ad-b239-860305011841",
    "a16e6255-17c3-431b-b047-3f66d24c286f",
    "9f8f1a47-f6eb-4845-b042-f6da7c25f0fb"
  ]
};
