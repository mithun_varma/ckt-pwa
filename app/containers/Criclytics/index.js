import React, { PureComponent } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import Loader from "../../components-v2/commonComponent/Loader";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import injectReducer from "utils/injectReducer";
import injectSaga from "utils/injectSaga";
import FactEngiene from "../../components-v2/commonComponent/FactEngiene";
import Header from "../../components-v2/commonComponent/Header";
import HrLine from "../../components-v2/commonComponent/HrLine";
import ListItem from "../../components-v2/commonComponent/ListItems";
import CriclyticsCard from "../../components-v2/Criclytics/HomeCard";
import { white } from "../../styleSheet/globalStyle/color";
import makeSelectScoreDetails from "../ScoreDetails/selectors";
import * as CriclyticsAction from "./actions";
import reducer from "./reducer";
import saga from "./saga";
import makeSelectCriclytics from "./selectors";

let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  // ClevertapReact.initialize("TEST-Z88-4KR-845Z"); // Old Key
  ClevertapReact.initialize("W88-4KR-845Z");
  // initializeFirebase();
  // askForPermissioToReceiveNotifications();
}
export class Criclytics extends PureComponent {
  state = {
    title: "",
    isHeaderBackground: false,
    displayHeader: "inline" // used only once
  };
  componentWillUnmount() {}
  componentDidMount() {
    this.props.fetchMatches();
  }

  render() {
    const { criclytics, history } = this.props; // criclytics variable is deconstructed but never used
    const matches =
      this.props.criclytics.mathces && this.props.criclytics.mathces; // this logic is totally redundant :/
    //  ^^ ( use the variable criclytics directly that has been deconstructed)

    return (
      <React.Fragment>
        <Helmet titleTemplate="%s | cricket.com">
          <title>
            Worldcup Predictions | Final 4 Projections of 2019 Worldcup Matches | Cricket
            Analytics
          </title>
          <meta
            name="description"
            content="Check worldcup qualification probability, final 4 predictions, worldcup points table and live cricket score ball by ball of all the matches. Get all the latest cricket updates only at Cricket.com."
          />
          <meta
            name="keywords"
            content="worldcup qualification probability, worldcup points table, worldcup predictions, final 4 projections 2019, cricket analytics"
          />

          <link
            rel="canonical"
            href={`www.cricket.com${history &&
              history.location &&
              history.location.pathname}`}
          />
        </Helmet>
        <div
          style={{
            background: "linear-gradient(176deg, #0b1533, #050b1b)",
            overflowY: "scroll",
            minHeight: "102vh"
          }}
        >
          <div style={{ padding: "16px 16px 16px" }}>
            <Header
              backgroundColor="#0b1533"
              textColor="#fff"
              isHeaderBackground={true}
              title={"Criclytics"}
              leftArrowBack
              leftIconOnClick={() => history.goBack()}
              history={history}
            />
          </div>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div style={{ alignContent: "flex-start", width: "100%" }}>
              <div
                style={{
                  paddingTop: 7,
                  padding: "0px 16px"
                }}
              >
                <FactEngiene
                  changeText
                  padding
                  noHeader
                  bgColor="transparent"
                  fact="The worlds most powerful predictive cricket algorithm" //binding fact is yet to be done
                  valueStyles={{
                    color: white,
                    opacity: 0.7,
                    padding: "2px 16px"
                  }}
                />
              </div>
              <div
                style={{
                  paddingTop: 21
                }}
              >
                {/* Removed World Cup Qualification Probability */}
                {/* <HrLine
                  style={{
                    background:
                      "linear-gradient(to left, rgba(255, 255, 255, 0.15), #ffffff 17%, #ffffff 87%, rgba(255, 255, 255, 0.15))",
                    opacity: 0.3
                  }}
                />
                <ListItem
                  titleStyle={{
                    color: white
                  }}
                  param={1} // no idea why 1 is needed ?? have a look , guess this can be removed or binded to some data
                  onClick={() => {
                    this.props.history.push("/playoffsProjection");
                  }}
                  isHR
                  title="Worldcup Final 4 Projections"
                  rootStyles={{
                    padding: "12px 12px 12px 16px"
                  }}
                  iconColor={white}
                  avatarStyle={{
                    width: "30px",
                    height: "30px",
                    objectFit: "contain",
                    borderRadius: "50%"
                  }}
                  // isAvatar
                  // avatar={require("./../../images/wc_logo.svg")}
                /> */}
                {/* Removed World Cup Qualification Probability closing */}
                <HrLine
                  style={{
                    background:
                      "linear-gradient(to left, rgba(255, 255, 255, 0.15), #ffffff 17%, #ffffff 87%, rgba(255, 255, 255, 0.15))",
                    opacity: 0.3
                  }}
                />
                {matches.length <= 0 && (
                  <Loader styles={{ height: "50px" }} noOfLoaders={5} /> // c an we make the no. of loader a variable , a better code structuring choice
                )}
                {matches.filter(x => x !== null).map((ele, key) => {
                  return (
                    <CriclyticsCard
                      key={key}
                      matchId={ele.matchId}
                      history={this.props.history}
                      score={ele}
                    />
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  criclytics: makeSelectCriclytics(),
  scoreDetails: makeSelectScoreDetails()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchMatches: () => dispatch(CriclyticsAction.getMatchesForCriclytics()),

    getHomeCric: payload => dispatch(CriclyticsAction.getHomeCric(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "criclytics", reducer });
const withSaga = injectSaga({ key: "criclytics", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(Criclytics);

const normal = "normal";
const style = {
  headerText: {
    fontFamily: "Montserrat",
    fontSize: "22px",
    fontWeight: 600,
    fontStyle: normal,
    fontStretch: normal,
    lineHeight: normal,
    letterSpacing: normal,
    color: "#ffffff"
  }
};
