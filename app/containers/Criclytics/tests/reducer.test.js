import { fromJS } from 'immutable';
import criclyticsReducer from '../reducer';

describe('criclyticsReducer', () => {
  it('returns the initial state', () => {
    expect(criclyticsReducer(undefined, {})).toEqual(fromJS({}));
  });
});
