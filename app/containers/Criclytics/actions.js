import {
  CLEAR_STORE,
  GET_BALL_BY_BALL,
  GET_BALL_BY_BALL_FALIURE,
  GET_BALL_BY_BALL_SUCCESS,
  GET_DEATH_OVER_BOWLERS,
  GET_DEATH_OVER_BOWLERS_FALIURE,
  GET_DEATH_OVER_BOWLERS_SUCCESS,
  GET_HOME_CRICLYTICS,
  GET_HOME_CRICLYTICS_FAILURE,
  GET_HOME_CRICLYTICS_STOP,
  GET_HOME_CRICLYTICS_SUCCESS,
  GET_LIVE_BOWLER_PROJECTIONS,
  GET_LIVE_BOWLER_PROJECTIONS_FALIURE,
  GET_LIVE_BOWLER_PROJECTIONS_SUCCESS,
  GET_LIVE_MATCHES_PROJECTION,
  GET_LIVE_MATCHES_PROJECTION_FALIURE,
  GET_LIVE_MATCHES_PROJECTION_SUCCESS,
  GET_LIVE_PLAYERS,
  GET_LIVE_PLAYERS_FALIURE,
  GET_LIVE_PLAYERS_SUCCESS,
  GET_LIVE_PLAYER_PORJECTIONS,
  GET_LIVE_PLAYER_PORJECTIONS_FALIURE,
  GET_LIVE_PLAYER_PORJECTIONS_SUCCESS,
  GET_MATCHES_FOR_CRICLYTICS,
  GET_MATCHES_FOR_CRICLYTICS_FALIURE,
  GET_MATCHES_FOR_CRICLYTICS_SUCCESS,
  GET_MATCH_UPS,
  GET_MATCH_UPS_FALIURE,
  GET_MATCH_UPS_SUCCESS,
  GET_MOMENTUM,
  GET_MOMENTUM_FALIURE,
  GET_MOMENTUM_SUCCESS,
  GET_OVER_SEPARATOR_MOMENTUM_SHIFT,
  GET_OVER_SEPARATOR_MOMENTUM_SHIFT_FALIURE,
  GET_OVER_SEPARATOR_MOMENTUM_SHIFT_SUCCESS,
  GET_PLAYER_PORJECTIONS,
  GET_PLAYER_PORJECTIONS_FALIURE,
  GET_PLAYER_PORJECTIONS_SUCCESS,
  GET_PRE_MATCH_PLAYERS,
  GET_PRE_MATCH_PLAYERS_FALIURE,
  GET_PRE_MATCH_PLAYERS_SUCCCESS,
  GET_QUALIFICATION_TABLE,
  GET_QUALIFICATION_TABLE_FAILURE,
  GET_QUALIFICATION_TABLE_SUCCESS,
  GET_SCORECARD_FOR_CRICLYTICS,
  GET_SCORECARD_FOR_CRICLYTICS_FALIURE,
  GET_SCORECARD_FOR_CRICLYTICS_SUCCESS,
  GET_SCREENS_FOR_CRICLYTICS,
  GET_SCREENS_FOR_CRICLYTICS_FALIURE,
  GET_SCREENS_FOR_CRICLYTICS_SUCCESS,
  GET_TIME_MACHINE,
  GET_TIME_MACHINE_FALIURE,
  GET_TIME_MACHINE_SUCCESS,
  POST_DEATH_OVERS_SELECTED,
  POST_DEATH_OVERS_SELECTED_FALIURE,
  POST_DEATH_OVERS_SELECTED_SUCCESS,
  GET_BATTING_PROJECTIONS,
  GET_BATTING_PROJECTIONS_SUCCESS,
  GET_BATTING_PROJECTIONS_FALIURE,
  GET_MATCH_UPS_PLAYER_ONE,
  GET_MATCH_UPS_PLAYER_ONE_SUCCESS,
  GET_MATCH_UPS_PLAYER_ONE_FAILURE,
  GET_MATCH_UPS_FOR_SELECTED_PLAYER,
  GET_MATCH_UPS_FOR_SELECTED_PLAYER_SUCCESS,
  GET_MATCH_UPS_FOR_SELECTED_PLAYER_FAILURE,
  GET_KEY_STATS,
  GET_KEY_STATS_SUCCESS,
  GET_KEY_STATS_FAILURE,
  RESET_DEATH_OVER_PROJECTED_STORE,
  GET_DEATH_BOWLERS_UPDATE,
  GET_DEATH_BOWLERS_UPDATE_SUCCESS,
  GET_DEATH_BOWLERS_UPDATE_FAILURE
} from "./constants";

/*
 *
 * Criclytics actions
 *
 */
//home screen cryclitics

export const getQualificationTable = payload => ({
  type: GET_QUALIFICATION_TABLE,
  payload
});

export const getQualificationTableSuccess = payload => ({
  type: GET_QUALIFICATION_TABLE_SUCCESS,
  payload
});

export const getQualificationTableFailure = payload => ({
  type: GET_QUALIFICATION_TABLE_FAILURE,
  payload
});

export const getHomeCric = payload => ({
  type: payload.stopPolling ? GET_HOME_CRICLYTICS_STOP : GET_HOME_CRICLYTICS,
  payload
});

export const getHomeCricSuccess = payload => ({
  type: GET_HOME_CRICLYTICS_SUCCESS,
  payload
});

export const getHomeCricFailure = payload => ({
  type: GET_HOME_CRICLYTICS_FAILURE,
  payload
});

export const getMatchUps = payload => ({
  type: GET_MATCH_UPS,
  payload
});

export const getMatchUpsSuccess = payload => ({
  type: GET_MATCH_UPS_SUCCESS,
  payload
});

export const getMatchUpsFaliure = payload => ({
  type: GET_MATCH_UPS_FALIURE,
  payload
});

export const getLivePlayerProjections = payload => ({
  type: GET_LIVE_PLAYER_PORJECTIONS,
  payload
});

export const getLivePlayerProjectionsSuccess = payload => ({
  type: GET_LIVE_PLAYER_PORJECTIONS_SUCCESS,
  payload
});

export const getLivePlayerProjectionsFaliure = payload => ({
  type: GET_LIVE_PLAYER_PORJECTIONS_FALIURE,
  payload
});
export const getMatchesForCriclytics = payload => ({
  type: GET_MATCHES_FOR_CRICLYTICS,
  payload
});

export const getMatchesForCriclyticsSuccess = payload => ({
  type: GET_MATCHES_FOR_CRICLYTICS_SUCCESS,
  payload
});
export const getMatchesForCriclyticsFaliure = payload => ({
  type: GET_MATCHES_FOR_CRICLYTICS_FALIURE,
  payload
});

export const getMomentumShift = payload => ({
  type: GET_MOMENTUM,
  payload
});

export const getMomentumShiftSuccess = payload => ({
  type: GET_MOMENTUM_SUCCESS,
  payload
});

export const getMomentumShiftFaliure = payload => ({
  type: GET_MOMENTUM_FALIURE,
  payload
});

export const getPreMatchProbability = payload => ({
  type: GET_PLAYER_PORJECTIONS,
  payload
});

export const getPreMatchProbabilitySuccess = payload => ({
  type: GET_PLAYER_PORJECTIONS_SUCCESS,
  payload
});

export const getPreMatchProbabilityFaliure = payload => ({
  type: GET_PLAYER_PORJECTIONS_FALIURE,
  payload
});

export const getDeathOverBowlers = payload => ({
  type: GET_DEATH_OVER_BOWLERS,
  payload
});

export const getDeathOverBowlersSuccess = payload => ({
  type: GET_DEATH_OVER_BOWLERS_SUCCESS,
  payload
});

export const getDeathOverBowlersFailure = payload => ({
  type: GET_DEATH_OVER_BOWLERS_FALIURE,
  payload
});


export const getDeathBowlersUpdate = payload => ({
  type: GET_DEATH_BOWLERS_UPDATE,
  payload
})

export const getDeathBowlersUpdateSuccess = payload => ({
  type: GET_DEATH_BOWLERS_UPDATE_SUCCESS,
  payload
})

export const getDeathBowlersUpdateFailure = payload => ({
  type: GET_DEATH_BOWLERS_UPDATE_FAILURE,
  payload
})


export const getScoreCardForCriclytics = payload => ({
  type: GET_SCORECARD_FOR_CRICLYTICS,
  payload
});

export const getScoreCardForCriclyticsSucceess = payload => ({
  type: GET_SCORECARD_FOR_CRICLYTICS_SUCCESS,
  payload
});

export const getScoreCardForCriclyticsFailure = payload => ({
  type: GET_SCORECARD_FOR_CRICLYTICS_FALIURE,
  payload
});

export const getMomentumShiftOverSeparator = payload => ({
  type: GET_OVER_SEPARATOR_MOMENTUM_SHIFT,
  payload
});
export const getFirstBattingProbability = payload => ({
  type: GET_BATTING_PROJECTIONS,
  payload
});

export const getFirstBattingProbabilitySuccess = payload => ({
  type: GET_BATTING_PROJECTIONS_SUCCESS,
  payload
});
export const getFirstBattingProbabilityFaliure = payload => ({
  type: GET_BATTING_PROJECTIONS_FALIURE,
  payload
});

export const getBallByBall = payload => ({
  type: GET_BALL_BY_BALL,
  payload
});
export const getBallByBallSuccess = payload => ({
  type: GET_BALL_BY_BALL_SUCCESS,
  payload
});
export const getBallByBallFaliure = payload => ({
  type: GET_BALL_BY_BALL_FALIURE,
  payload
});

export const getMomentumShiftOverSeparatorSuccess = payload => ({
  type: GET_OVER_SEPARATOR_MOMENTUM_SHIFT_SUCCESS,
  payload
});
export const getMomentumShiftOverSeparatorFaliure = payload => ({
  type: GET_OVER_SEPARATOR_MOMENTUM_SHIFT_FALIURE,
  payload
});
export const postDeathOversBowler = payload => ({
  type: POST_DEATH_OVERS_SELECTED,
  payload
});
export const postDeathOversBowlerSuccess = payload => ({
  type: POST_DEATH_OVERS_SELECTED_SUCCESS,
  payload
});
export const postDeathOversBowlerFaliure = payload => ({
  type: POST_DEATH_OVERS_SELECTED_FALIURE,
  payload
});

export const getTimeMachine = payload => ({
  type: GET_TIME_MACHINE,
  payload
});

export const getTimeMachineSuccess = payload => ({
  type: GET_TIME_MACHINE_SUCCESS,
  payload
});

export const getTimeMachineFaliure = payload => ({
  type: GET_TIME_MACHINE_FALIURE,
  payload
});

export const getLiveMatchPrediction = payload => {
  return {
    type: GET_LIVE_MATCHES_PROJECTION,
    payload
  };
};

export const getLiveMatchPredictionSuccess = payload => ({
  type: GET_LIVE_MATCHES_PROJECTION_SUCCESS,
  payload
});

export const getLiveMatchPredictionFaliure = payload => ({
  type: GET_LIVE_MATCHES_PROJECTION_FALIURE,
  payload
});

export const getLiveBowlerPrediction = payload => ({
  type: GET_LIVE_BOWLER_PROJECTIONS,
  payload
});

export const getLiveBowlerPredictionSuccess = payload => ({
  type: GET_LIVE_BOWLER_PROJECTIONS_SUCCESS,
  payload
});

export const getLiveBowlerPredictionFaliure = payload => ({
  type: GET_LIVE_BOWLER_PROJECTIONS_FALIURE,
  payload
});

export const getPreMatchPlayers = payload => ({
  type: GET_PRE_MATCH_PLAYERS,
  payload
});

export const getPreMatchPlayersSuccess = payload => ({
  type: GET_PRE_MATCH_PLAYERS_SUCCCESS,
  payload
});

export const getPreMatchPlayersFaliure = payload => ({
  type: GET_PRE_MATCH_PLAYERS_FALIURE,
  payload
});

export const getLiveMatchPlayers = payload => ({
  type: GET_LIVE_PLAYERS,
  payload
});

export const getLiveMatchPlayersSuccess = payload => ({
  type: GET_LIVE_PLAYERS_SUCCESS,
  payload
});

export const getLiveMatchPlayersFaliure = payload => ({
  type: GET_LIVE_PLAYERS_FALIURE,
  payload
});

export const getScreensForCriclytics = payload => ({
  type: GET_SCREENS_FOR_CRICLYTICS,
  payload
});
export const getScreensForCriclyticsSuccess = payload => ({
  type: GET_SCREENS_FOR_CRICLYTICS_SUCCESS,
  payload
});
export const getScreensForCriclyticsFaliure = payload => ({
  type: GET_SCREENS_FOR_CRICLYTICS_FALIURE,
  payload
});

export const clearStore = payload => ({
  type: CLEAR_STORE,
  payload
});

export const resetdeathOverProjectedStore = payload => ({
  type: RESET_DEATH_OVER_PROJECTED_STORE,
  payload
});

// export const getLivePlayerProjections= payload => ( {
//   type: GET_LIVE
// })


export const getMatchUpsForPlayerOne = payload => ({
  type: GET_MATCH_UPS_PLAYER_ONE,
  payload
})


export const getMatchUpsForPlayerOneSuccess = payload => ({
  type: GET_MATCH_UPS_PLAYER_ONE_SUCCESS,
  payload
})

export const getMatchUpsForPlayerOneFailure = payload => ({
  type: GET_MATCH_UPS_PLAYER_ONE_FAILURE,
  payload
})

export const getMatchUpsForSelectedPlayer = payload => ({
  type: GET_MATCH_UPS_FOR_SELECTED_PLAYER,
  payload
})


export const getMatchUpsForSelectedPlayerSuccess = payload => ({
  type: GET_MATCH_UPS_FOR_SELECTED_PLAYER_SUCCESS,
  payload
})

export const getMatchUpsForSelectedPlayerFailure = payload => ({
  type: GET_MATCH_UPS_FOR_SELECTED_PLAYER_FAILURE,
  payload
})

export const getKeyStats = payload => ({
  type: GET_KEY_STATS,
  payload
});

export const getKeyStatsSuccess = payload => ({
  type: GET_KEY_STATS_SUCCESS,
  payload
})

export const getKeyStatsFailure = payload => (
  {
    type: GET_KEY_STATS_FAILURE,
    payload
  }
)