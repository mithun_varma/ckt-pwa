/*
 *
 * Criclytics reducer
 *
 */

import {
  GET_BATTING_PROJECTIONS,
  GET_BATTING_PROJECTIONS_FALIURE,
  GET_BATTING_PROJECTIONS_SUCCESS,
  GET_LIVE_PLAYER_PORJECTIONS,
  GET_LIVE_PLAYER_PORJECTIONS_FALIURE,
  GET_LIVE_PLAYER_PORJECTIONS_SUCCESS,
  GET_MATCHES_FOR_CRICLYTICS,
  GET_MATCHES_FOR_CRICLYTICS_FALIURE,
  GET_MATCHES_FOR_CRICLYTICS_SUCCESS,
  GET_MATCH_UPS,
  GET_MATCH_UPS_FALIURE,
  GET_MATCH_UPS_SUCCESS,
  GET_MOMENTUM,
  GET_MOMENTUM_FALIURE,
  GET_MOMENTUM_SUCCESS,
  GET_PLAYER_PORJECTIONS,
  GET_PLAYER_PORJECTIONS_SUCCESS,
  GET_DEATH_OVER_BOWLERS,
  GET_DEATH_OVER_BOWLERS_SUCCESS,
  GET_DEATH_OVER_BOWLERS_FALIURE,
  POST_DEATH_OVERS_SELECTED,
  POST_DEATH_OVERS_SELECTED_SUCCESS,
  POST_DEATH_OVERS_SELECTED_FALIURE,
  GET_TIME_MACHINE,
  GET_TIME_MACHINE_SUCCESS,
  GET_TIME_MACHINE_FALIURE,
  GET_LIVE_MATCHES_PROJECTION,
  GET_LIVE_MATCHES_PROJECTION_SUCCESS,
  GET_LIVE_MATCHES_PROJECTION_FALIURE,
  GET_HOME_CRICLYTICS,
  GET_HOME_CRICLYTICS_FAILURE,
  GET_HOME_CRICLYTICS_SUCCESS,
  GET_LIVE_BOWLER_PROJECTIONS_FALIURE,
  GET_LIVE_BOWLER_PROJECTIONS,
  GET_LIVE_BOWLER_PROJECTIONS_SUCCESS,
  GET_PRE_MATCH_PLAYERS,
  GET_PRE_MATCH_PLAYERS_SUCCCESS,
  GET_PRE_MATCH_PLAYERS_FALIURE,
  GET_LIVE_PLAYERS,
  GET_LIVE_PLAYERS_SUCCESS,
  GET_LIVE_PLAYERS_FALIURE,
  GET_SCREENS_FOR_CRICLYTICS,
  GET_SCREENS_FOR_CRICLYTICS_SUCCESS,
  GET_SCREENS_FOR_CRICLYTICS_FALIURE,
  GET_OVER_SEPARATOR_MOMENTUM_SHIFT,
  GET_OVER_SEPARATOR_MOMENTUM_SHIFT_SUCCESS,
  GET_OVER_SEPARATOR_MOMENTUM_SHIFT_FALIURE,
  GET_BALL_BY_BALL,
  GET_BALL_BY_BALL_SUCCESS,
  GET_BALL_BY_BALL_FALIURE,
  GET_QUALIFICATION_TABLE,
  GET_QUALIFICATION_TABLE_SUCCESS,
  GET_QUALIFICATION_TABLE_FAILURE,
  GET_SCORECARD_FOR_CRICLYTICS,
  GET_SCORECARD_FOR_CRICLYTICS_FALIURE,
  GET_SCORECARD_FOR_CRICLYTICS_SUCCESS,
  GET_MATCH_UPS_PLAYER_ONE,
  GET_MATCH_UPS_PLAYER_ONE_SUCCESS,
  GET_MATCH_UPS_PLAYER_ONE_FAILURE,
  GET_MATCH_UPS_FOR_SELECTED_PLAYER,
  GET_MATCH_UPS_FOR_SELECTED_PLAYER_SUCCESS,
  GET_MATCH_UPS_FOR_SELECTED_PLAYER_FAILURE,
  GET_KEY_STATS,
  GET_KEY_STATS_SUCCESS,
  GET_KEY_STATS_FAILURE,
  CLEAR_STORE,
  RESET_DEATH_OVER_PROJECTED_STORE,
  GET_DEATH_BOWLERS_UPDATE,
  GET_DEATH_BOWLERS_UPDATE_SUCCESS,
  GET_DEATH_BOWLERS_UPDATE_FAILURE,
} from "./constants";

export const initialState = {
  matchesLoading: undefined,
  mathces: [],
  matchUps: [],
  matchUpsLoading: undefined,
  momentumShift: [],
  momentumShiftLoading: undefined,
  livePlayerProjections: [],
  livePlayerProjectionsLoading: undefined,

  preMatchPlayerProjections: [],
  preMatchPlayerProjectionsLoading: undefined,
  deathBowlers: [],
  deathBowlersLoading: undefined,
  error: undefined,
  deathOverProjected: [],
  deathOverProjectedLoading: undefined,
  timeMachine: [],
  timeMachineLoading: undefined,
  liveMatchPrediction: [],
  liveMatchPredictionLoading: undefined,
  homeCricLoading: false,
  homeCricData: {},
  liveBowlerPredictionLoading: undefined,
  liveBowlerPrediction: [],
  liveProjectionPlayers: [],
  liveProjectionPlayersLoading: undefined,
  preMatchPlayers: [],
  preMatchPlayersLoading: undefined,
  criclyticsScreen: [],
  criclyticsScreenLoading: undefined,
  overSeparatorMomentum: undefined,
  overSeparatorMomentumLoading: undefined,
  ballByBall: [],
  ballByBallLoading: undefined,
  qualificationTable: [],
  qualificationTableLoading: false,
  firstBattingTeamProbability: [],
  firstBattingTeamProbabilityLoading: undefined,
  matchUpsPlayerOneLoading: false,
  matchUpsPlayerOneData: [],
  matchUpsForSelectedPlayerLoading: false,
  matchUpsForSelectedPlayer: [],
  keyStats: {},
  keyStatsLoading: false,
  matchMiniScoreCards: {},
  deathBowlersUpdateLoading: false
};

export function criclyticsReducer(state = initialState, action) {
  switch (action.type) {
    case GET_QUALIFICATION_TABLE:
      return {
        ...state,
        qualificationTableLoading: true
      };
    case GET_QUALIFICATION_TABLE_SUCCESS:
      // console.log(action.payload);
      return {
        ...state,
        qualificationTableLoading: false,
        qualificationTable: action.payload
      };
    case GET_QUALIFICATION_TABLE_FAILURE:
      return {
        ...state,
        qualificationTableLoading: false
      };

    case GET_HOME_CRICLYTICS:
      return {
        ...state,
        homeCricData: {},
        homeCricLoading: true
      };
    case GET_HOME_CRICLYTICS_SUCCESS:
      // console.log(action.payload);
      return {
        ...state,
        homeCricLoading: false,
        homeCricData: action.payload
      };
    case GET_HOME_CRICLYTICS_FAILURE:
      return {
        ...state,
        homeCricLoading: false
      };

    case GET_MATCH_UPS:
      return { ...state, matchUpsLoading: true };
    case GET_MATCH_UPS_SUCCESS:
      return {
        ...state,
        matchUpsLoading: false,
        matchUps: action.payload
      };
    case GET_MATCH_UPS_FALIURE:
      return {
        ...state,
        matchUpsLoading: true,
        error: action.payload
      };
    case GET_MATCHES_FOR_CRICLYTICS:
      return {
        ...state,
        matchesLoading: true
      };
    case GET_MATCHES_FOR_CRICLYTICS_SUCCESS:
      return {
        ...state,
        matchesLoading: false,
        mathces: action.payload
      };
    case GET_MATCHES_FOR_CRICLYTICS_FALIURE:
      return {
        ...state,
        matchesLoading: true,
        error: action.payload
      };

    case GET_MOMENTUM:
      return {
        ...state,
        momentumShiftLoading: true
      };
    case GET_MOMENTUM_SUCCESS:
      return {
        ...state,
        momentumShift: action.payload,
        momentumShiftLoading: false
      };
    case GET_MOMENTUM_FALIURE:
      return {
        ...state,
        error: action.payload,
        momentumShiftLoading: true
      };
    case GET_LIVE_PLAYER_PORJECTIONS:
      return {
        ...state,
        livePlayerProjectionsLoading: true
      };
    case GET_LIVE_PLAYER_PORJECTIONS_SUCCESS:
      return {
        ...state,
        livePlayerProjections: action.payload,
        livePlayerProjectionsLoading: false
      };
    case GET_LIVE_PLAYER_PORJECTIONS_FALIURE:
      return {
        ...state,
        livePlayerProjectionsLoading: true,
        error: action.payload
      };

    case GET_PLAYER_PORJECTIONS:
      return {
        ...state,
        preMatchPlayerProjectionsLoading: true
      };
    case GET_PLAYER_PORJECTIONS_SUCCESS:
      return {
        ...state,
        preMatchPlayerProjections: action.payload,
        preMatchPlayerProjectionsLoading: false
      };
    case GET_BATTING_PROJECTIONS_FALIURE: {
      return {
        ...state,
        preMatchPlayerProjectionsLoading: true,
        error: action.payload
      };
    }
    case GET_DEATH_OVER_BOWLERS: {
      return {
        ...state,
        deathBowlersLoading: true
      };
    }

    case GET_DEATH_OVER_BOWLERS_SUCCESS: {
      return {
        ...state,
        deathBowlersLoading: false,
        deathBowlers: action.payload
      };
    }
    case GET_DEATH_OVER_BOWLERS_FALIURE: {
      return {
        ...state,
        deathBowlersLoading: true,
        error: action.payload
      };
    }

    case GET_DEATH_BOWLERS_UPDATE: {
      return {
        ...state,
        deathBowlersUpdateLoading: true,
        deathBowlers: {},
        deathOverProjected: []
      }
    }

    case GET_DEATH_BOWLERS_UPDATE_SUCCESS: {
      return {
        ...state,
        deathBowlers: action.payload,
        deathBowlersUpdateLoading: false
      }
    }

    case GET_DEATH_BOWLERS_UPDATE_FAILURE: {
      return {
        ...state,
        deathBowlersUpdateLoading: true
      }
    }

    case POST_DEATH_OVERS_SELECTED: {
      return {
        ...state,
        deathOverProjectedLoading: true
      };
    }
    case POST_DEATH_OVERS_SELECTED_SUCCESS: {
      return {
        ...state,
        deathOverProjectedLoading: false,
        deathOverProjected: action.payload
      };
    }
    case POST_DEATH_OVERS_SELECTED_FALIURE: {
      return {
        ...state,
        deathOverProjectedLoading: true,
        error: action.payload
      };
    }

    case RESET_DEATH_OVER_PROJECTED_STORE: {
      return {
        ...state,
        deathOverProjectedLoading: false,
        deathOverProjected: []
      };
    }

    case GET_TIME_MACHINE: {
      return {
        ...state,
        timeMachineLoading: true
      };
    }

    case GET_TIME_MACHINE_SUCCESS: {
      return {
        ...state,
        timeMachineLoading: false,
        timeMachine: action.payload
      };
    }

    case GET_TIME_MACHINE_FALIURE: {
      return {
        ...state,
        timeMachineLoading: true,
        error: action.payload
      };
    }
    case GET_LIVE_MATCHES_PROJECTION: {
      return {
        ...state,
        liveMatchPredictionLoading: true
      };
    }
    case GET_LIVE_MATCHES_PROJECTION_SUCCESS: {
      return {
        ...state,
        liveMatchPredictionLoading: false,
        liveMatchPrediction: action.payload
      };
    }
    case GET_LIVE_MATCHES_PROJECTION_FALIURE: {
      return {
        ...state,
        liveMatchPredictionLoading: true,
        error: action.payload
      };
    }
    case GET_LIVE_BOWLER_PROJECTIONS: {
      return {
        ...state,
        liveBowlerPredictionLoading: true
      };
    }
    case GET_LIVE_BOWLER_PROJECTIONS_SUCCESS: {
      return {
        ...state,
        liveBowlerPredictionLoading: false,
        liveBowlerPrediction: action.payload
      };
    }
    case GET_LIVE_BOWLER_PROJECTIONS_FALIURE: {
      return {
        ...state,
        liveBowlerPredictionLoading: true,
        error: action.payload
      };
    }
    case GET_PRE_MATCH_PLAYERS: {
      return {
        ...state,
        preMatchPlayersLoading: true
      };
    }
    case GET_PRE_MATCH_PLAYERS_SUCCCESS: {
      return {
        ...state,
        preMatchPlayerProjectionsLoading: false,
        preMatchPlayers: action.payload
      };
    }
    case GET_PRE_MATCH_PLAYERS_FALIURE: {
      return {
        ...state,
        preMatchPlayerProjectionsLoading: true,
        error: action.payload
      };
    }
    case GET_LIVE_PLAYERS: {
      return {
        ...state,
        liveProjectionPlayersLoading: true
      };
    }
    case GET_LIVE_PLAYERS_SUCCESS: {
      return {
        ...state,
        liveProjectionPlayersLoading: false,
        liveProjectionPlayers: action.payload
      };
    }
    case GET_LIVE_PLAYERS_FALIURE: {
      return {
        ...state,
        liveProjectionPlayersLoading: true,
        error: action.payload
      };
    }
    case GET_SCREENS_FOR_CRICLYTICS: {
      return {
        ...state,
        criclyticsScreenLoading: true
      };
    }

    case GET_BATTING_PROJECTIONS:
      return {
        ...state,
        firstBattingTeamProbabilityLoading: true
      };
    case GET_BATTING_PROJECTIONS_SUCCESS:
      return {
        ...state,
        firstBattingTeamProbability: action.payload,
        firstBattingTeamProbabilityLoading: false
      };
    case GET_BATTING_PROJECTIONS_FALIURE:
      return {
        ...state,
        firstBattingTeamProbabilityLoading: true,
        error: action.payload
      };
    case GET_SCREENS_FOR_CRICLYTICS_SUCCESS: {
      return {
        ...state,
        criclyticsScreenLoading: false,
        criclyticsScreen: action.payload
      };
    }
    case GET_SCREENS_FOR_CRICLYTICS_FALIURE: {
      return {
        ...state,
        criclyticsScreenLoading: true,
        error: action.payload
      };
    }
    case GET_OVER_SEPARATOR_MOMENTUM_SHIFT: {
      return {
        ...state,
        overSeparatorMomentumLoading: true
      };
    }
    case GET_OVER_SEPARATOR_MOMENTUM_SHIFT_SUCCESS: {
      return {
        ...state,
        overSeparatorMomentumLoading: false,
        overSeparatorMomentum: action.payload
      };
    }
    case GET_OVER_SEPARATOR_MOMENTUM_SHIFT_FALIURE: {
      return {
        ...state,
        overSeparatorMomentumLoading: true,
        error: action.payload
      };
    }
    case GET_BALL_BY_BALL: {
      return {
        ...state,
        ballByBallLoading: true
      };
    }
    case GET_BALL_BY_BALL_SUCCESS: {
      return {
        ...state,
        ballByBall: action.payload,
        ballByBallLoading: false
      };
    }
    case GET_BALL_BY_BALL_FALIURE: {
      return {
        ...state,
        ballByBallLoading: true,
        error: action.payload
      };
    }
    case GET_SCORECARD_FOR_CRICLYTICS: {
      return {
        ...state,
        matchMiniScoreCardsLoading: true
      };
    }
    case GET_SCORECARD_FOR_CRICLYTICS_SUCCESS: {
      return {
        ...state,
        matchMiniScoreCards: action.payload,
        matchMiniScoreCardsLoading: false
      };
    }
    case GET_SCORECARD_FOR_CRICLYTICS_FALIURE: {
      return {
        ...state,
        matchMiniScoreCardsLoading: true,
        error: action.payload
      };
    }

    case GET_MATCH_UPS_PLAYER_ONE: {
      return {
        ...state,
        matchUpsPlayerOneLoading: true
      };
    }

    case GET_MATCH_UPS_PLAYER_ONE_SUCCESS: {
      return {
        ...state,
        matchUpsPlayerOneLoading: false,
        matchUpsPlayerOneData:
          action.payload &&
          action.payload.matchUpData &&
          action.payload.matchUpData.length > 0
            ? action.payload.matchUpData
            : []
      };
    }

    case GET_MATCH_UPS_PLAYER_ONE_FAILURE: {
      return {
        ...state,
        matchUpsPlayerOneLoading: false
      };
    }

    case GET_MATCH_UPS_FOR_SELECTED_PLAYER: {
      return {
        ...state,
        matchUpsForSelectedPlayerLoading: true
      };
    }
    case GET_MATCH_UPS_FOR_SELECTED_PLAYER_SUCCESS: {
      const obj = {
        bowler: "matchUpsPlayerOneData",
        batsman: "matchUpsForSelectedPlayer"

      }
      return {
        ...state,
        matchUpsForSelectedPlayerLoading: false,
        // matchUpsForSelectedPlayer:
        [obj[action.payload.role]]:
          action.payload.data &&
          action.payload.data.matchUpData &&
          action.payload.data.matchUpData.length > 0
            ? action.payload.data.matchUpData
            : []
      };
    }

    case GET_MATCH_UPS_FOR_SELECTED_PLAYER_FAILURE: {
      return {
        ...state,
        matchUpsForSelectedPlayerLoading: false
      };
    }

    case GET_KEY_STATS: {
      return {
        ...state,
        keyStatsLoading: true
      };
    }

    case GET_KEY_STATS_SUCCESS: {
      return {
        ...state,
        keyStatsLoading: false,
        keyStats:
          action.payload ? action.payload : {}
      };
    }

    case GET_KEY_STATS_FAILURE: {
      return {
        ...state,
        keyStatsLoading: false
      };
    }
    case CLEAR_STORE: {
      return {
        ...state,
        matchesLoading: undefined,
        mathces: [],
        matchUps: [],
        matchUpsLoading: undefined,
        momentumShift: [],
        momentumShiftLoading: undefined,
        livePlayerProjections: [],
        livePlayerProjectionsLoading: undefined,
        firstBattingTeamProbability: [],
        firstBattingTeamProbabilityLoading: undefined,
        preMatchPlayerProjections: [],
        preMatchPlayerProjectionsLoading: undefined,
        deathBowlers: [],
        deathBowlersLoading: undefined,
        error: undefined,
        deathOverProjected: [],
        deathOverProjectedLoading: undefined,
        timeMachine: [],
        timeMachineLoading: undefined,
        liveMatchPrediction: [],
        liveMatchPredictionLoading: undefined,
        liveBowlerPredictionLoading: undefined,
        liveBowlerPrediction: [],
        liveProjectionPlayers: [],
        liveProjectionPlayersLoading: undefined,
        preMatchPlayers: [],
        preMatchPlayersLoading: undefined,
        criclyticsScreen: [],
        criclyticsScreenLoading: undefined,
        overSeparatorMomentum: undefined,
        overSeparatorMomentumLoading: undefined,
        ballByBall: [],
        ballByBallLoading: undefined,
        qualificationTable: [],
        qualificationTableLoading: false,
        matchUpsPlayerOneLoading: false,
        matchUpsPlayerOneData: [],
        matchUpsForSelectedPlayerLoading: false,
        matchUpsForSelectedPlayer: []
      };
    }
    default:
      return state;
  }
}

//TODO Make the store for all the endpoints once they are abvailable

export default criclyticsReducer;
