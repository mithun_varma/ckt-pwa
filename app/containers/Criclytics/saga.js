import {
  API_GET_MATCH_UPS,
  API_GET_FEATURED_SCORES,
  API_GET_MOMENTUM_SHIFT,
  API_GET_LIVE_BATTING_PROBABILITY,
  API_GET_FIRST_BATTING_SCORE,
  API_GET_PRE_MATCH_PROBABILITY,
  API_GET_DEATH_OVER_BOWLERS,
  API_POST_DEATH_BOWLERS,
  API_GET_TIME_MACHINE,
  API_GET_LIVE_MATCH_PREDICTION,
  API_GET_HOME_CRIC,
  API_GET_LIVE_BOWLER_PREDICITON,
  API_GET_PLAYER_DETAILS,
  API_GET_CRICLYTICS_SCREEN,
  API_GET_MATCHES_FOR_CRICLYTICS_LANDED,
  API_GET_COMMENTARY_OVER_SEPARATOR,
  API_GET_BALL_BY_BALL_MONMENTUM,
  API_GET_PRE_MATCH_PLAYER_PROJECTIONS,
  API_GET_QUALIFICATION_TABLE,
  API_SCORE_DETAILS,
  API_GET_MATCH_UPS_PLAYER_ONE,
  API_GET_MATCH_UPS_FOR_SELECTED_PLAYER,
  API_GET_KEY_STATS
} from "../../utils/requestUrls";
import { delay } from "redux-saga";
import * as CriclyticsAction from "./actions";
import * as CriclyticsConstant from "./constants";
import {
  takeLatest,
  call,
  put,
  fork,
  race,
  all,
  take
} from "redux-saga/effects";
import request from "utils/request";
import { Criclytics } from ".";
// import { take, call, put, select } from 'redux-saga/effects';

// Individual exports for testing

export function* fetchBallByBalls(payload) {
  try {
    const requestURL = API_GET_BALL_BY_BALL_MONMENTUM(
      payload.payload.matchId,
      payload.payload.overNo,
      payload.payload.inningNo
    );
    const options = yield call(request, requestURL);
    yield put(CriclyticsAction.getBallByBallSuccess(options.data.data));
  } catch (err) {
    yield put(CriclyticsAction.getBallByBallFaliure(err));
  }
}
export function* fetchQualificationTable(payload) {
  const config = {
    ...API_GET_QUALIFICATION_TABLE,
    params: {
      ...payload.payload
    }
  };
  try {
    const options = yield call(request, config);
    yield put(CriclyticsAction.getQualificationTableSuccess(options.data.data));
  } catch (err) {
    yield put(CriclyticsAction.getQualificationTableFailure(err));
  }
}

export function* fetchFirstBattingProjection(payload) {
  try {
    const requestURL = API_GET_FIRST_BATTING_SCORE(payload.payload);

    const options = yield call(request, requestURL);

    yield put(
      CriclyticsAction.getFirstBattingProbabilitySuccess(options.data.data)
    );
  } catch (err) {
    yield put(CriclyticsAction.getFirstBattingProbabilityFaliure(err));
  }
}

export function* fetchScreensForCriclytics(payload) {
  try {
    const requestURL = API_GET_CRICLYTICS_SCREEN(payload.payload);
    const data = yield call(request, requestURL);

    yield put(CriclyticsAction.getScreensForCriclyticsSuccess(data.data.data));
  } catch (err) {
    yield put(CriclyticsAction.getScreensForCriclyticsFaliure(err));
  }
}

export function* fetchMatches(payload) {
  try {
    const requestURL = API_GET_MATCHES_FOR_CRICLYTICS_LANDED;
    const options = yield call(request, requestURL);

    yield put(
      CriclyticsAction.getMatchesForCriclyticsSuccess(options.data.data)
    );
  } catch (err) {
    yield put(CriclyticsAction.getMatchesForCriclyticsFaliure(err));
  }
}
export function* getLiveMatchProjections(payload) {
  try {
    const requestURL = API_GET_LIVE_MATCH_PREDICTION(payload.payload);

    const options = yield call(request, requestURL);
    yield put(
      CriclyticsAction.getLiveMatchPredictionSuccess(options.data.data)
    );
  } catch (err) {
    yield put(CriclyticsAction.getLiveMatchPredictionFaliure(err));
  }
}
export function* getTimeMachine(payload) {
  try {
    const requestURL = API_GET_TIME_MACHINE(
      payload.payload.matchId,
      payload.payload.inningNo,
      payload.payload.overNo
    );

    const options = yield call(request, requestURL);

    yield put(CriclyticsAction.getTimeMachineSuccess(options.data.data));
  } catch (err) {
    yield put(CriclyticsAction.getTimeMachineFaliure(err));
  }
}

export function* fetchPreMatchStats(payload) {
  try {
    const requestURL = API_GET_PRE_MATCH_PLAYER_PROJECTIONS(payload.payload);

    const options = yield call(request, requestURL);
    yield put(
      CriclyticsAction.getPreMatchProbabilitySuccess(options.data.data)
    );
  } catch (error) {
    yield put(CriclyticsAction.getPreMatchProbabilityFaliure(error));
  }
}

export function* fetchDeathOver(payload) {
  try {
    const requestURL = API_GET_DEATH_OVER_BOWLERS(payload.payload);
    const options = yield call(request, requestURL);
    yield put(CriclyticsAction.getDeathOverBowlersSuccess(options.data));
  } catch (err) {
    yield put(CriclyticsAction.getDeathOverBowlersFailure(err));
  }
}

export function* fetchDeathBowlersUpdate(payload) {
  try {
    const requestURL = API_GET_DEATH_OVER_BOWLERS(payload.payload);
    const options = yield call(request, requestURL);
    yield put(CriclyticsAction.getDeathBowlersUpdateSuccess(options.data));
  } catch (err) {
    yield put(CriclyticsAction.getDeathBowlersUpdateFailure(err));
  }
}

export function* fetchMomentumShift(payload) {
  try {
    const requestURL = API_GET_MOMENTUM_SHIFT(payload.payload);
    const config = {
      ...requestURL
    };

    const options = yield call(request, config);

    yield put(CriclyticsAction.getMomentumShiftSuccess(options.data.data));
  } catch (err) {
    yield put(CriclyticsAction.getMomentumShiftFaliure(err));
  }
}
export function* fetchOverForMomentum(payload) {
  try {
    const requestURL = API_GET_COMMENTARY_OVER_SEPARATOR(payload.payload);

    const options = yield call(request, requestURL);
    yield put(
      CriclyticsAction.getMomentumShiftOverSeparatorSuccess(options.data.data)
    );
  } catch (err) {
    yield put(CriclyticsAction.getMomentumShiftOverSeparatorFaliure(err));
  }
}
export function* postDataForDeathOver(payload) {
  try {
    const requestURL = API_POST_DEATH_BOWLERS;
    const config = {
      ...requestURL,
      data: payload.payload
    };
    const options = yield call(request, config);
    // const options = postMockOptions;
    yield put(CriclyticsAction.postDeathOversBowlerSuccess(options.data));
  } catch (error) {
    yield put(CriclyticsAction.postDeathOversBowlerFaliure(error));
  }
}

export function* getLiveMatchPlayers(payload) {
  try {
    const reqURL = API_GET_PLAYER_DETAILS(payload);
    const options = yield call(request, reqURL);
    yield put(CriclyticsAction.getLiveMatchPlayersSuccess(options.data));
  } catch (err) {
    yield put(CriclyticsAction.getLiveMatchPlayersFaliure(err));
  }
}

export function* getPreMatchPlayers(payload) {
  try {
    const reqURL = API_GET_PLAYER_DETAILS(payload);

    const options = yield call(request, reqURL);
    yield put(CriclyticsAction.getPreMatchPlayersSuccess(options.data));
  } catch (err) {
    yield put(CriclyticsAction.getPreMatchPlayersFaliure(err));
  }
}
export function* getMatchMiniScoreCard(payload) {
  try {
    const reqUrl = API_SCORE_DETAILS(payload.payload);
    const options = yield call(request, reqUrl);
    yield put(
      CriclyticsAction.getScoreCardForCriclyticsSucceess(options.data.data)
    );
  } catch (err) {
    yield put(CriclyticsAction.getScoreCardForCriclyticsFailure(err));
  }
}

export function* getHomeCricData(payload) {
  // console.log(payload);
  while (true) {
    try {
      // console.warn(payload);
      const requestURL = API_GET_HOME_CRIC(payload.matchId);
      const config = {
        ...requestURL
      };
      const options = yield call(request, config);
      yield put(CriclyticsAction.getHomeCricSuccess(options.data.data));
      yield call(delay, 20000);
    } catch (err) {
      yield call(delay, 20000);
      yield put(CriclyticsAction.getHomeCricFailure(err));
    }
  }
}
export function* fetchKLiveBowlers(payload) {
  while (true) {
    try {
      const requestURL = API_GET_LIVE_BOWLER_PREDICITON(payload.payload);

      const options = yield call(request, requestURL);
      yield put(
        CriclyticsAction.getLiveBowlerPredictionSuccess(options.data.data)
      );
      yield call(delay, 80000000);
    } catch (err) {
      yield call(delay, 80000000);
      yield put(CriclyticsAction.getLiveBowlerPredictionFaliure(err));
    }
  }
}
export function* fetchLiveScorePredictions(payload) {
  try {
    const requestURL = API_GET_LIVE_BATTING_PROBABILITY(payload.payload);

    const options = yield call(request, requestURL);
    yield put(
      CriclyticsAction.getLivePlayerProjectionsSuccess(options.data.data)
    );
    yield call(delay, 8000000);
  } catch (error) {
    yield call(delay, 8000000);
    yield put(CriclyticsAction.getLivePlayerProjectionsFaliure(error));
  }
}

export function* fetchMatchUps(payload) {
  try {
    const requestURL = API_GET_MATCH_UPS(payload.payload);
    const config = {
      ...requestURL
    };

    const options = yield call(request, config);

    yield put(CriclyticsAction.getMatchUpsSuccess(options.data.data));
    yield call(delay, 10000);
  } catch (err) {
    yield put(CriclyticsAction.getMatchUpsFaliure(err));
    yield call(delay, 10000);
  }
}
export function* watchMatchUps() {
  while (true) {
    const { payload } = yield take(CriclyticsConstant.GET_MATCH_UPS);

    yield race([
      call(fetchLiveScorePredictions, payload),
      take(CriclyticsConstant.STOP_MATCH_UPS)
    ]);
  }
}
export function* watchPollLiveBatsmanPrediciton() {
  while (true) {
    const { payload } = yield take(
      CriclyticsConstant.GET_LIVE_PLAYER_PORJECTIONS
    );

    yield race([
      call(fetchLiveScorePredictions, payload),
      take(CriclyticsConstant.GET_LIVE_MATCH_PROJECTIONS_STOP)
    ]);
  }
}
export function* watchPollLivePlayerPrediciton() {
  while (true) {
    const { payload } = yield take(
      CriclyticsConstant.GET_LIVE_BOWLER_PROJECTIONS
    );

    yield race([
      call(getHomeCricData, payload),
      take(CriclyticsConstant.GET_LIVE_BOWLER_PROJECTIONS_STOP)
    ]);
  }
}

export function* watchPollHomeCricSaga() {
  while (true) {
    const { payload } = yield take(CriclyticsConstant.GET_HOME_CRICLYTICS);
    yield race([
      call(getHomeCricData, payload),
      take(CriclyticsConstant.GET_HOME_CRICLYTICS_STOP)
    ]);
  }
}

export function* getMatchUpsForPlayerOne(payload) {
  try {
    const options = yield call(request, API_GET_MATCH_UPS_PLAYER_ONE(payload));
    yield put(
      CriclyticsAction.getMatchUpsForPlayerOneSuccess(options.data.data)
    );
  } catch (error) {
    yield put(CriclyticsAction.getMatchUpsForPlayerOneFailure(error));
  }
}

export function* getMatchUpsForSelectedPlayer(payload) {
  try {
    const options = yield call(
      request,
      API_GET_MATCH_UPS_FOR_SELECTED_PLAYER(payload)
    );
    yield put(
      CriclyticsAction.getMatchUpsForSelectedPlayerSuccess({data: options.data.data, role: payload.payload.role})
    );
  } catch (error) {
    yield put(CriclyticsAction.getMatchUpsForSelectedPlayerFailure(error));
  }
}

export function* getKeyStats(payload) {
  try {
    const options = yield call(request, API_GET_KEY_STATS(payload));
    yield put(CriclyticsAction.getKeyStatsSuccess(options.data.data));
  } catch (error) {
    yield put(CriclyticsAction.getKeyStatsFailure(error));
  }
}

export default function* fetchWatcher() {
  // yield all([watchPollLivePlayerPrediciton].map(fork));
  yield all([watchPollHomeCricSaga].map(fork));
  // yield all([watchMatchUps].map(fork));
  // yield all([watchPollLiveBatsmanPrediciton].map(fork));
  yield takeLatest(CriclyticsConstant.GET_MATCH_UPS, fetchMatchUps);
  yield takeLatest(CriclyticsConstant.GET_MATCHES_FOR_CRICLYTICS, fetchMatches);
  yield takeLatest(CriclyticsConstant.GET_MOMENTUM, fetchMomentumShift);
  yield takeLatest(
    CriclyticsConstant.GET_LIVE_PLAYER_PORJECTIONS,
    fetchLiveScorePredictions
  );
  yield takeLatest(
    CriclyticsConstant.GET_QUALIFICATION_TABLE,
    fetchQualificationTable
  );

  yield takeLatest(
    CriclyticsConstant.GET_PLAYER_PORJECTIONS,
    fetchPreMatchStats
  );
  yield takeLatest(CriclyticsConstant.GET_DEATH_OVER_BOWLERS, fetchDeathOver);
  yield takeLatest(
    CriclyticsConstant.POST_DEATH_OVERS_SELECTED,
    postDataForDeathOver
  );
  yield takeLatest(CriclyticsConstant.GET_TIME_MACHINE, getTimeMachine);
  yield takeLatest(
    CriclyticsConstant.GET_LIVE_MATCHES_PROJECTION,
    getLiveMatchProjections
  );
  // yield takeLatest(CriclyticsConstant.GET_HOME_CRICLYTICS, getHomeCricData);
  yield takeLatest(
    CriclyticsConstant.GET_LIVE_BOWLER_PROJECTIONS,
    fetchKLiveBowlers
  );
  yield takeLatest(
    CriclyticsConstant.GET_PRE_MATCH_PLAYERS,
    getPreMatchPlayers
  );
  yield takeLatest(CriclyticsConstant.GET_LIVE_PLAYERS, getLiveMatchPlayers);
  yield takeLatest(
    CriclyticsConstant.GET_SCREENS_FOR_CRICLYTICS,
    fetchScreensForCriclytics
  );
  yield takeLatest(
    CriclyticsConstant.GET_OVER_SEPARATOR_MOMENTUM_SHIFT,
    fetchOverForMomentum
  );
  yield takeLatest(CriclyticsConstant.GET_BALL_BY_BALL, fetchBallByBalls);
  yield takeLatest(
    CriclyticsConstant.GET_SCORECARD_FOR_CRICLYTICS,
    getMatchMiniScoreCard
  );
  yield takeLatest(
    CriclyticsConstant.GET_BATTING_PROJECTIONS,
    fetchFirstBattingProjection
  );

  yield takeLatest(
    CriclyticsConstant.GET_MATCH_UPS_PLAYER_ONE,
    getMatchUpsForPlayerOne
  );

  yield takeLatest(
    CriclyticsConstant.GET_MATCH_UPS_FOR_SELECTED_PLAYER,
    getMatchUpsForSelectedPlayer
  );

  yield takeLatest(CriclyticsConstant.GET_KEY_STATS, getKeyStats);

  yield takeLatest(CriclyticsConstant.GET_DEATH_BOWLERS_UPDATE, fetchDeathBowlersUpdate);
}


const postMockOptions = {
  "data": {
    "playerIds": [
      {
        "name": "LT Ngidi",
        "avatar": "https://s3.ap-south-1.amazonaws.com/crictecdev/Players/Headshot/cdc-3989-lt-ngidi-headshot.png",
        "overs": "47",
        "maiden": null,
        "runs": "6",
        "wickets": "0",
        "playerId": "cdc-3989-lt-ngidi"
      },
      {
        "name": "Kagiso Rabada",
        "avatar": "https://s3.ap-south-1.amazonaws.com/crictecdev/Players/Headshot/cdc-5483-k-rabada-headshot.png",
        "overs": "48",
        "maiden": null,
        "runs": "6",
        "wickets": "0",
        "playerId": "cdc-5483-k-rabada"
      },
      {
        "name": "D Pretorius",
        "avatar": "https://s3.ap-south-1.amazonaws.com/crictecdev/Players/Headshot/default-player-avtar.png",
        "overs": "49",
        "maiden": null,
        "runs": "5",
        "wickets": "0",
        "playerId": "cdc-7228-d-pretorius"
      },
      {
        "name": "AL Phehlukwayo",
        "avatar": "https://s3.ap-south-1.amazonaws.com/crictecdev/Players/Headshot/cdc-6499-al-phehlukwayo-headshot.png",
        "overs": "50",
        "maiden": null,
        "runs": "7",
        "wickets": "0",
        "playerId": "cdc-6499-al-phehlukwayo"
      }
    ],
    "matchId": "2019304-1-england-vs-south-africa",
    "inningNumber": 1,
    "batPosition": 2,
    "totalRuns": 295,
    "totalWickets": 6,
    "totalOvers": 50,
    "remainingOvers": 0,
    "currentBowler": "cdc-5483-k-rabada",
    "checked": false,
    status: 400,
    message: "error message"
  },
  "status": 200,
  "statusText": "",
}