import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the criclytics state domain
 */

const selectCriclyticsDomain = state => state.criclytics || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Criclytics
 */

const makeSelectCriclytics = () =>
  createSelector(selectCriclyticsDomain, substate => substate);

export default makeSelectCriclytics;
export { selectCriclyticsDomain };
