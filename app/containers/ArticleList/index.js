import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import InfiniteScroll from "react-infinite-scroll-component";

import FeatureArticleCard from "components-v2/Articles/FeatureArticleCard";
import ArticleCard from "components-v2/Articles/ArticleCard";
import HrLine from "components-v2/commonComponent/HrLine";
import CardGradientTitle from "components-v2/commonComponent/CardGradientTitle";
import { Helmet } from "react-helmet";
import {
  white,
  orange_10,
  grey_10,
  cardShadow,
  grey_4,
  black,
  red_Orange
} from "../../styleSheet/globalStyle/color";
import Loader from "components-v2/commonComponent/Loader";
import EmptyState from "components-v2/commonComponent/EmptyState";
import Header from "components-v2/commonComponent/Header";
import TabBar from "../../components-v2/commonComponent/TabBar";
import BottomBar from "../../components-v2/commonComponent/BottomBar";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";

import makeSelectArticles from "../Articles/selectors";
import articlesReducer from "../Articles/reducer";
import * as articlesActions from "../Articles/actions";
import articlesSaga from "../Articles/saga";
import ReactGA from "react-ga";

/* eslint-disable react/prefer-stateless-function */

let scrollTop = null;
const TAB_CONFIG = [
  "News",
  "Pre Match Analysis",
  "Post Match Analysis",
  // "Criclytics",
  "Opinion"
];
const TAB_CONFIG_OBJ = {
  news: "News",
  "pre-match-analysis": "Pre Match Analysis",
  "post-match-analysis": "Post Match Analysis",
  opinion: "Opinion"
};

export class ArticleList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      initial: true,
      activeButton: "news thread",
      title: "",
      isHeaderBackground: false,
      activeTab: "News"
    };
    this.loadMoreItems = this.loadMoreItems.bind(this);
  }

  componentDidMount = () => {
    console.log("dm: ", this.state);
    if (this.state.activeTab === "Post Match Analysis") {
      this.props.fetchFantasyAnalysis({
        limit: 10,
        skip: 0
      });
    } else if (!this.state.activeTab || this.state.activeTab === "News") {
      this.props.fetchNewsOpinion({
        limit: 10,
        skip: 0
      });
    } else if (this.state.activeTab === "Pre Match Analysis") {
      this.props.fetchPreAnalysis({
        limit: 10,
        skip: 0
      });
    } else if (this.state.activeTab === "Opinion") {
      this.props.fetchCriclyticsArticles({
        limit: 10,
        skip: 0,
        // types: ["criclytics_feature"]
        types: ["opinion"]
      });
    }
    // ReactGA.initialize("UA-136526387-1");
    // ReactGA.pageview(
    //   "https://www.cricket.com" +
    //     window.location.pathname +
    //     window.location.search
    // );
    // window.addEventListener("scroll", this.handleScroll);
    // if (this.props.match.params.type === "analysis") {
    //   this.props.fetchFantasyAnalysis({
    //     limit: 10,
    //     skip: 0
    //   });
    // } else if (this.props.match.params.type === "news") {
    // this.props.fetchNewsOpinion({
    //   limit: 10,
    //   skip: 0
    // });
    // } else if (this.props.match.params.type === "suggestion") {
    //   this.props.fetchSuggestion({
    //     limit: 10,
    //     skip: 0
    //   });
    // } else if (this.props.match.params.type === "pre-match-analysis") {
    //   this.props.fetchPreAnalysis({
    //     limit: 10,
    //     skip: 0
    //   });
    // }
  };

  componentWillUnmount() {
    // window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = event => {
    ({ scrollTop } = event.target.scrollingElement);
    // console.log("object: ", scrollTop);
    if (scrollTop > 26) {
      this.setState({
        title: this.props.match.params.type,
        isHeaderBackground: true
      });
    } else if (scrollTop < 27) {
      this.setState({
        title: "",
        isHeaderBackground: false
      });
    }
  };

  static getDerivedStateFromProps(props, state) {
    const search = props.location.search; // could be '?type=news'
    const params = new URLSearchParams(search);
    const paramValue = params.get("type"); // type
    if (state.initial)
      return {
        initial: false,
        activeTab: paramValue ? TAB_CONFIG_OBJ[paramValue] : "News"
      };
    return null;
  }

  cardOnClick = path => {
    this.props.history.push(path);
  };

  handleActiveButton = activeButton => {
    this.setState({
      activeButton
    });
  };

  handleActiveTab = activeTab => {
    this.setState(
      {
        activeTab
      },
      () => {
        let paramTab = activeTab.toLowerCase().replace(/ /g, "-");
        const search = this.props.location.search; // could be '?type=news'
        const params = new URLSearchParams(search);
        const paramValue = params.get("type"); // type
        if (paramValue) {
          this.props.history.replace(`/articles?type=${paramTab}`);
        }
        if (this.state.activeTab === "Post Match Analysis") {
          this.props.fetchFantasyAnalysis({
            limit: 10,
            skip: 0
          });
        } else if (this.state.activeTab === "News") {
          this.props.fetchNewsOpinion({
            limit: 10,
            skip: 0
          });
        } else if (this.state.activeTab === "Pre Match Analysis") {
          this.props.fetchPreAnalysis({
            limit: 10,
            skip: 0
          });
          // } else if (this.state.activeTab === "Criclytics") {
        } else if (this.state.activeTab === "Opinion") {
          this.props.fetchCriclyticsArticles({
            limit: 10,
            skip: 0,
            types: ["opinion"]
          });
        }
      }
    );
  };

  loadMoreItems = () => {
    // if (this.props.match.params.type === "analysis") {
    if (this.state.activeTab === "Post Match Analysis") {
      this.props.fetchFantasyAnalysis({
        limit: 10,
        skip: this.props.articles.fantasyAnalysis.length
      });
    } else if (this.state.activeTab === "News") {
      this.props.fetchNewsOpinion({
        limit: 10,
        skip:
          this.props.articles.newsOpinions.length +
          this.props.articles.newsOpinionsFeatured.length
      });
    } else if (this.state.activeTab === "suggestion") {
      this.props.fetchSuggestion({
        limit: 10,
        // skip: 0,
        skip:
          this.props.articles.suggestion.length +
          this.props.articles.suggestionFeatured.length
      });
    } else if (this.state.activeTab === "Pre Match Analysis") {
      this.props.fetchPreAnalysis({
        limit: 10,
        // skip: 0,
        skip: this.props.articles.prematchAnalysis.length
      });
    } else if (this.state.activeTab === "Opinion") {
      this.props.fetchCriclyticsArticles({
        limit: 10,
        skip: this.props.articles.criclyticsArticles.length,
        // types: ["criclytics_feature"]
        types: ["opinion"]
      });
    }
  };

  render() {
    const { articles, history } = this.props;
    return (
      <React.Fragment>
        <Helmet titleTemplate="%s | cricket.com">
          <title>
            {/* {this.props.match.params.type === "analysis" */}
            {this.state.activeTab === "Post Match Analysis"
              ? "Cricket Statistics & Analysis "
              : this.state.activeTab === "News"
                ? "Cricket News | Live Editorial Updates and Opinions"
                : "Cricket Match Updates | Match Analysis & Reports"}
          </title>
          <meta
            name="description"
            content={
              this.state.activeTab === "Post Match Analysis"
                ? "Get a line with the cricket articles on detailed match statistics and analysis "
                : this.state.activeTab === "News"
                  ? "Explore the cricket news with live editorial updates & opinions shared by our experts"
                  : "Get cricket match updates along with the match analysis and reports on (crictec.com)"
            }
          />
          {/* <meta
            name="keywords"
            content={(article.articleDetails && article.articleDetails.seoKeywords) || ''}
          /> */}
          <link
            rel="canonical"
            href={`www.cricket.com${this.props.history.location.pathname}`}
          />
        </Helmet>
        <div
          style={{
            // padding: "54px 16px 16px",
            padding: "16px 16px 16px",
            // minHeight: "120vh",
            marginBottom: 60
          }}
        >
          {false && (
            <Header
              // title={this.state.title}
              title={this.props.match.params.type.replace(/-/g, " ")}
              // isHeaderBackground={this.state.isHeaderBackground}
              isHeaderBackground={true}
              leftArrowBack
              textColor={black}
              backgroundColor={grey_4}
              history={history}
              leftIconOnClick={() => {
                history.goBack();
              }}
            />
          )}
          {/* Title */}
          <div
            style={{
              fontFamily: "Montserrat",
              fontWeight: 700,
              fontSize: 22,
              color: grey_10,
              marginBottom: 12,
              textTransform: "capitalize"
            }}
          >
            News & Articles
          </div>

          <TabBar
            items={TAB_CONFIG}
            activeItem={this.state.activeTab}
            onClick={this.handleActiveTab}
            increaseLastItemWidth
          />
          <HrLine />

          {/* Section 1 Latest News */}
          {/* {this.props.match.params.type === "news" && ( */}
          {this.state.activeTab === "News" && (
            <div
              style={{
                background: white,
                borderRadius: 3,
                boxShadow: cardShadow,
                marginBottom: 10
              }}
            >
              {false && (
                <div>
                  {/* <ButtonBar
                  items={BUTTON_CONFIG}
                  activeItem={this.state.activeButton}
                  onClick={this.handleActiveButton}
                  increaseLastItemWidth
                />
                <HrLine /> */}
                  <CardGradientTitle
                    title="Latest News"
                    cardType="news"
                    rootStyles={{
                      fontSize: 14
                    }}
                    subtitleStyles={{
                      color: red_Orange,
                      marginRight: 10
                    }}
                  />
                </div>
              )}
              {articles.newsOpinionLoading && this.state.initial ? (
                <div style={{ padding: 10 }}>
                  <Loader styles={{ height: "120px" }} noOfLoaders={2} />
                </div>
              ) : articles.newsOpinions.length > 0 ? (
                <InfiniteScroll
                  dataLength={articles.newsOpinions.length}
                  next={this.loadMoreItems}
                  loader={
                    articles.newsOpinions.length >= 10 && (
                      <Loader styles={{ height: "120px" }} noOfLoaders={3} />
                    )
                  }
                  hasMore={
                    articles.newsOpinionsOldLength !==
                    articles.newsOpinions.length
                  }
                >
                  {articles.newsOpinions
                    .filter(function(el) {
                      return el.type != "video";
                    })
                    .map((news, key) => (
                      <div>
                        {key == 0 && news.filters.featured ? (
                          <div>
                            <FeatureArticleCard
                              article={news}
                              articleHost={articles.articleHost}
                              cardType="news"
                              key={`${key + 1}`}
                              cardOnClick={this.cardOnClick}
                            />
                            <HrLine />
                          </div>
                        ) : (
                          <div>
                            <ArticleCard
                              article={news}
                              articleHost={articles.articleHost}
                              cardType="news"
                              key={`${key + 1}`}
                              cardOnClick={this.cardOnClick}
                            />
                            <HrLine />
                          </div>
                        )}
                      </div>
                    ))}
                </InfiniteScroll>
              ) : (
                <EmptyState msg="No news found" />
              )}
            </div>
          )}
          {/* Section 1 Latest News closing */}

          {/* Section 2 Pre-Match Analysis */}
          {/* {this.props.match.params.type === "analysis" && ( */}
          {this.state.activeTab === "Post Match Analysis" && (
            <div
              style={{
                background: white,
                borderRadius: 3,
                boxShadow: cardShadow,
                marginBottom: 10
              }}
            >
              {false && (
                <CardGradientTitle
                  title="Post Match Analysis"
                  cardType="analysis"
                  rootStyles={{
                    background: white
                  }}
                  subtitleStyles={{
                    color: orange_10,
                    paddingRight: 8
                  }}
                />
              )}

              {articles.fantasyAnalysisLoading && this.state.initial ? (
                <div style={{ padding: 10 }}>
                  <Loader styles={{ height: "120px" }} noOfLoaders={2} />
                </div>
              ) : articles.fantasyAnalysis.length > 0 ? (
                <InfiniteScroll
                  dataLength={articles.fantasyAnalysis.length}
                  next={this.loadMoreItems}
                  loader={
                    articles.fantasyAnalysis.length >= 10 && (
                      <Loader styles={{ height: "120px" }} noOfLoaders={3} />
                    )
                  }
                  hasMore={
                    articles.fantasyAnalysisOldLength !==
                    articles.fantasyAnalysis.length
                  }
                >
                  {articles.fantasyAnalysis
                    .filter(function(el) {
                      return el.type != "video";
                    })
                    .map((analysis, key) => (
                      <div>
                        {key == 0 && analysis.filters.featured ? (
                          <div>
                            <FeatureArticleCard
                              article={analysis}
                              articleHost={articles.articleHost}
                              cardType="analysis"
                              key={`${key + 1}`}
                              cardOnClick={this.cardOnClick}
                            />
                            <HrLine />
                          </div>
                        ) : (
                          <div>
                            <ArticleCard
                              article={analysis}
                              articleHost={articles.articleHost}
                              cardType="analysis"
                              key={`${key + 1}`}
                              cardOnClick={this.cardOnClick}
                            />
                            <HrLine />
                          </div>
                        )}
                      </div>
                    ))}
                </InfiniteScroll>
              ) : (
                <EmptyState msg="No analysis found" />
              )}
            </div>
          )}
          {/* Section 2 Pre-Match Analysis closing */}
          {/* Section 3 */}
          {this.props.match.params.type === "suggestion" && (
            <div
              style={{
                background: white,
                borderRadius: 3,
                boxShadow: cardShadow,
                marginBottom: 10
              }}
            >
              <CardGradientTitle
                title="Our Suggestions"
                cardType="suggestion"
                rootStyles={{
                  background: white
                }}
                subtitleStyles={{
                  color: orange_10,
                  paddingRight: 8
                }}
              />
              {articles.suggestionLoading && this.state.initial ? (
                <div style={{ padding: 10 }}>
                  <Loader styles={{ height: "120px" }} noOfLoaders={2} />
                </div>
              ) : articles.suggestion.length > 0 ? (
                <InfiniteScroll
                  dataLength={articles.suggestion.length}
                  next={this.loadMoreItems}
                  loader={
                    articles.suggestion.length >= 10 && (
                      <Loader styles={{ height: "120px" }} noOfLoaders={3} />
                    )
                  }
                  hasMore={
                    articles.suggestionOldLength !== articles.suggestion.length
                  }
                >
                  {articles.suggestion
                    .filter(function(el) {
                      return el.type != "video";
                    })
                    .map((opinion, key) => (
                      <div>
                        {key == 0 && opinion.filters.featured ? (
                          <div>
                            <FeatureArticleCard
                              article={opinion}
                              articleHost={articles.articleHost}
                              cardType="suggestion"
                              key={`${key + 1}`}
                              cardOnClick={this.cardOnClick}
                            />
                            <HrLine />
                          </div>
                        ) : (
                          <div>
                            <ArticleCard
                              article={opinion}
                              articleHost={articles.articleHost}
                              cardType="suggestion"
                              key={`${key + 1}`}
                              cardOnClick={this.cardOnClick}
                            />
                            <HrLine />
                          </div>
                        )}
                      </div>
                    ))}
                </InfiniteScroll>
              ) : (
                <EmptyState msg="No suggestion found" />
              )}
            </div>
          )}

          {/* {this.props.match.params.type === "pre-match-analysis" && ( */}
          {this.state.activeTab === "Pre Match Analysis" && (
            <div
              style={{
                background: white,
                borderRadius: 3,
                boxShadow: cardShadow,
                marginBottom: 10
              }}
            >
              {false && (
                <CardGradientTitle
                  title="Pre Match Analysis"
                  cardType="pre-match-analysis"
                  rootStyles={{
                    background: white
                  }}
                  subtitleStyles={{
                    color: orange_10,
                    paddingRight: 8
                  }}
                />
              )}
              {articles.preanalysisLoading && this.state.initial ? (
                <div style={{ padding: 10 }}>
                  <Loader styles={{ height: "120px" }} noOfLoaders={2} />
                </div>
              ) : articles.prematchAnalysis.length > 0 ? (
                <InfiniteScroll
                  dataLength={articles.prematchAnalysis.length}
                  next={this.loadMoreItems}
                  loader={
                    articles.prematchAnalysis.length >= 10 && (
                      <Loader styles={{ height: "120px" }} noOfLoaders={3} />
                    )
                  }
                  hasMore={
                    articles.prematchAnalysisOldLength !==
                    articles.prematchAnalysis.length
                  }
                >
                  {articles.prematchAnalysis
                    .filter(function(el) {
                      return el.type != "video";
                    })
                    .map((opinion, key) => (
                      <div>
                        {key == 0 && opinion.filters.featured ? (
                          <div>
                            <FeatureArticleCard
                              article={opinion}
                              articleHost={articles.articleHost}
                              cardType="pre-match-analysis"
                              key={`${key + 1}`}
                              cardOnClick={this.cardOnClick}
                            />
                            <HrLine />
                          </div>
                        ) : (
                          <div>
                            <ArticleCard
                              article={opinion}
                              articleHost={articles.articleHost}
                              cardType="pre-match-analysis"
                              key={`${key + 1}`}
                              cardOnClick={this.cardOnClick}
                            />
                            <HrLine />
                          </div>
                        )}
                      </div>
                    ))}
                </InfiniteScroll>
              ) : (
                <EmptyState msg="No suggestion found" />
              )}
            </div>
          )}
          {/* Section 3 closing */}

          {/* Opinion Featured Articles section */}
          {this.state.activeTab === "Opinion" && (
            <div
              style={{
                background: white,
                borderRadius: 3,
                boxShadow: cardShadow,
                marginBottom: 10
              }}
            >
              {false && (
                <CardGradientTitle
                  title="Pre Match Analysis"
                  cardType="pre-match-analysis"
                  rootStyles={{
                    background: white
                  }}
                  subtitleStyles={{
                    color: orange_10,
                    paddingRight: 8
                  }}
                />
              )}
              {articles.criclyticsArticlesLoading && this.state.initial ? (
                <div style={{ padding: 10 }}>
                  <Loader styles={{ height: "120px" }} noOfLoaders={2} />
                </div>
              ) : articles.criclyticsArticles.length > 0 ? (
                <InfiniteScroll
                  dataLength={articles.criclyticsArticles.length}
                  next={this.loadMoreItems}
                  loader={
                    articles.criclyticsArticles.length >= 10 && (
                      <Loader styles={{ height: "120px" }} noOfLoaders={3} />
                    )
                  }
                  hasMore={
                    articles.criclyticsArticlesOldLength !==
                    articles.criclyticsArticles.length
                  }
                >
                  {articles.criclyticsArticles
                    .filter(function(el) {
                      return el.type != "video";
                    })
                    .map((item, key) => (
                      <div>
                        {key == 0 && item.filters.featured ? (
                          <div>
                            <FeatureArticleCard
                              article={item}
                              articleHost={articles.articleHost}
                              cardType="opinion"
                              key={`${key + 1}`}
                              cardOnClick={this.cardOnClick}
                            />
                            <HrLine />
                          </div>
                        ) : (
                          <div>
                            <ArticleCard
                              article={item}
                              articleHost={articles.articleHost}
                              cardType="opinion"
                              key={`${key + 1}`}
                              cardOnClick={this.cardOnClick}
                            />
                            <HrLine />
                          </div>
                        )}
                      </div>
                    ))}
                </InfiniteScroll>
              ) : (
                <EmptyState msg="No suggestion found" />
              )}
            </div>
          )}
          {/* Criclytics Featured Articles section End */}
        </div>
        <BottomBar
          {...this.props.history}
          tabs={[
            { key: "home", route: "/" },
            { key: "schedule", route: "schedule" },
            { key: "Criclytics", route: "criclytics" },
            { key: "news", route: "articles" },
            { key: "more", route: "more" }
          ]}
        />
      </React.Fragment>
    );
  }
}

ArticleList.propTypes = {
  fetchFantasyAnalysis: PropTypes.func,
  fetchMatchReport: PropTypes.func,
  fetchNewsOpinion: PropTypes.func,
  fetchSuggestion: PropTypes.func,
  fetchPreAnalysis: PropTypes.func,
  articles: PropTypes.object,
  match: PropTypes.object,
  history: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  articles: makeSelectArticles()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchFantasyAnalysis: payload =>
      dispatch(articlesActions.fetchFantasyAnalysis(payload)),
    fetchNewsOpinion: payload =>
      dispatch(articlesActions.fetchNewsOpinion(payload)),
    fetchSuggestion: payload =>
      dispatch(articlesActions.fetchSuggestion(payload)),
    fetchPreAnalysis: payload =>
      dispatch(articlesActions.fetchPreAnalysis(payload)),
    fetchCriclyticsArticles: payload =>
      dispatch(articlesActions.fetchCriclyticsArticles(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withArticlesReducer = injectReducer({
  key: "articles",
  reducer: articlesReducer
});
const withArticlesSaga = injectSaga({ key: "articles", saga: articlesSaga });

export default compose(
  withArticlesReducer,
  withArticlesSaga,
  withConnect
)(ArticleList);
