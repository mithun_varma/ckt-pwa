import React from "react";
import { Switch, Route } from "react-router-dom";
import Modal from "react-responsive-modal";
import Home from "containers/Home";
import { isAndroid } from "react-device-detect";

import AppDownload from "../../components-v2/commonComponent/AppDownload";
// New V2 UI imports
import Articles from "containers/Articles";
import ArticleList from "containers/ArticleList";
import ArticleDetails from "containers/ArticleDetails";
import Grounds from "containers/Grounds";
import Ranking from "containers/Ranking";
// import Photos from "containers/Photos";
// import PhotosList from "containers/PhotosList";
import Videos from "containers/Videos";
import Record from "containers/Record";
import Schedule from "containers/Schedule";
import More from "containers/More";
import Criclytics from "containers/Criclytics";

import PlayerFullBio from "containers/PlayerFullBio";

import PlayerProfile from "./../../containers/Players/index";
import Settings from "../../components-v2/Settings/Settings";
import PhotoGallery from "./../../containers/Photos";
import ScoreCard from "./../../containers/ScoreDetails";
import PlayerDiscovery from "./../../containers/PlayersList";
import TeamDisccovery from "./../../containers/Teams";
import Teams from "./../../containers/TeamsSchedule";
import Series from "../Series";
import SeriesDetails from "../SeriesDetails";
import FantasyPlayers from "../../components-v2/Fantasy/FantasyPlayerPerformance"; // edited
import FantasyTeamCreation from "../../components-v2/Fantasy/FantasyTeamCreation";
import FantasyTeamCode from "../../components-v2/Fantasy/FantasyTeamCode";
import PlayoffsProjection from "../../components-v2/PlayoffsProjection";
import CriclyticsSlider from "../CriclyticsSlider";
import VideoDetails from "../VideoDetails";
import TermofUse from "./../../components-v2/screenComponent/TermOfUse";
import CookiesPolicy from "../../components-v2/screenComponent/CookiesPolicy";
import PrivacyPolicy from "../../components-v2/screenComponent/PrivacyPolicy";
import AboutCrictec from "../../components-v2/screenComponent/AboutCrictec";
import CriclyticsKeyStats from "../../components-v2/Criclytics/KeyStats";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalOpen: false
    };
  }
  componentDidMount = () => {
    const appDownloaded = localStorage.getItem("appDownloaded");
    const laterTime = localStorage.getItem("laterTime");
    // console.log(appDownloaded, laterTime);
    if (!appDownloaded) {
      if (!laterTime) {
        this.to = setTimeout(() => {
          this.setState({
            modalOpen: true
          });
        }, 30000);
      } else {
        let diff = new Date().getTime() - new Date(+laterTime).getTime();
        const hours = Math.floor(diff / 1000 / 60 / 60);
        diff -= hours * 1000 * 60 * 60;
        const minutes = Math.floor(diff / 1000 / 60);
        // console.log(minutes > 1 * 10);
        if (minutes > 72 * 60) {
          this.to = setTimeout(() => {
            this.setState({
              modalOpen: true
            });
          }, 30000);
        }
      }
    }
  };

  _handleCloseModal = () => {
    this.setState({
      modalOpen: false
    });
  };

  handleRedirect = () => {
    this.setState(
      {
        modalOpen: false
      },
      () => {
        localStorage.setItem("appDownloaded", "true");
        if (isAndroid) {
          window.open(
            "https://play.google.com/store/apps/details?id=com.crictec.cricket",
            "_blank"
          );
        } else {
          window.open(
            "https://itunes.apple.com/us/app/cricket-com/id1460360497",
            "_blank"
          );
        }
      }
    );
  };

  handleMaybeLater = () => {
    const nw = new Date().getTime();
    this.setState(
      {
        modalOpen: false
      },
      () => {
        localStorage.setItem("laterTime", nw);
      }
    );
  };

  handle = () => {
    const url =
      "http://qaapi.crictec.com/api/scorecards?filters[]=featured&cardType=Micro&limit=5";
    fetch(url);
    fetch(url)
      .then(response => response.json())
      .then(data =>
        this.setState({
          splashLoading: false
        })
      );

    setTimeout(() => {
      this.setState({
        splashLoading: false
      });
    }, 3000);
  };

  render() {
    return (
      <React.Fragment>
        <Modal
          open={this.state.modalOpen}
          closeIconSize={20}
          showCloseIcon={false}
          onClose={this._handleCloseModal}
          closeOnOverlayClick={false}
          styles={{
            overlay: {
              padding: 0,
              alignItems: "flex-end"
            },
            modal: {
              padding: 0,
              margin: 0,
              // minWidth: "95%"
              width: "100%",
              borderTopLeftRadius: 16,
              borderTopRightRadius: 16,
              backgroundImage: `url(${require("../../images/cricket_dnld_modal.png")})`,
              backgroundSize: "contain",
              backgroundRepeat: "no-repeat"
            },
            closeButton: {
              top: 11,
              right: 8
            },
            closeIcon: {
              fill: "#8b94ae"
            }
          }}
          // center
        >
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              fontFamily: "Montserrat",
              fontWeight: "600",
              alignItems: "center"
            }}
          >
            <div
              style={{
                // width: 50,
                position: "absolute",
                background: "#fff",
                padding: "12px 16px",
                top: -32,
                borderRadius: "50%"
              }}
            >
              <img
                style={{ width: 50 }}
                src={require("../../images/icon-512x512.png")}
              />
            </div>
            <h1
              style={{
                fontSize: 18,
                color: "#141b2f",
                padding: "0 40px",
                textAlign: "center",
                margin: 0,
                marginTop: 64
              }}
            >
              Have you tried the
            </h1>
            <h1
              style={{
                fontSize: 18,
                color: "#141b2f",
                padding: "0 40px",
                textAlign: "center",
                margin: 0,
                marginBottom: 8
              }}
            >
              <span style={{ color: "#cb462a" }}>cricket</span>
              <span style={{ color: "#ec601d" }}>.</span>
              <span style={{ color: "#f7a100" }}>com</span> App yet?
            </h1>
            <p
              style={{
                color: "#727682",
                fontWeight: 400,
                padding: "0 44px",
                textAlign: "center"
              }}
            >
              Download the latest version of the App to follow live scores,
              projections and much more.
            </p>
            <div
              style={{
                margin: "0 32px",
                marginTop: 32,
                padding: "14px 80px",
                color: "#fff",
                borderRadius: 4,
                textAlign: "center",
                background: "linear-gradient(to right, #d44030, #9b000d)"
              }}
              id="download-app"
              onClick={this.handleRedirect}
            >
              Download Now
            </div>
            {/* <div style={{ width: "80%", marginTop: 32 }}>
              {isAndroid ? (
                <AppDownload
                  url={
                    "https://play.google.com/store/apps/details?id=com.crictec.cricket"
                  }
                  rootStyles={{
                    background: "linear-gradient(to right, #d44030, #9b000d)"
                  }}
                  innerStyle={{
                    justifyContent: "center"
                  }}
                  isAndroid
                  noIcon
                />
              ) : (
                <AppDownload
                  url={
                    "https://itunes.apple.com/us/app/cricket-com/id1460360497"
                  }
                  rootStyles={{
                    background: "linear-gradient(to right, #d44030, #9b000d)"
                  }}
                  innerStyle={{
                    justifyContent: "center"
                  }}
                  noIcon
                />
              )}
            </div> */}
            <div
              style={{
                marginTop: 14,
                marginBottom: 14,
                textAlign: "center",
                color: "#d44030",
                textDecoration: "underline"
              }}
              onClick={this.handleMaybeLater}
            >
              <p>Maybe Later</p>
            </div>
          </div>
        </Modal>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/players" component={PlayerDiscovery} />
          <Route exact path="/players/:playerId" component={PlayerProfile} />
          <Route
            exact
            path="/playerbio/:playerId/:playerName"
            component={PlayerFullBio}
          />
          <Route exact path="/teams" component={TeamDisccovery} />
          <Route exact path="/teams/:teamId" component={Teams} />
          <Route
            exact
            path="/photos"
            component={PhotoGallery}
            history={this.props.history}
          />
          <Route exact path="/score/:matchId/:type?" component={ScoreCard} />
          <Route exact path="/grounds/:venueId?" component={Grounds} />

          {/* ======== */}
          <Route exact path="/more" component={More} />
          {/* <Route exact path="/articles" component={Articles} /> */}
          {/* <Route exact path="/articles/:type" component={ArticleList} /> */}
          <Route exact path="/articles" component={ArticleList} />
          {/* <Route
            exact
            path="/articles/:type/:title/:articleId"
            component={ArticleDetails}
          /> */}
          <Route
            exact
            path="/articles/:type/:title"
            component={ArticleDetails}
          />
          <Route
            exact
            path="/series/:seriesname/:seriesid/:type?"
            component={SeriesDetails}
          />
          <Route exact path="/videos" component={Videos} />
          <Route exact path="/videos/:id" component={VideoDetails} />
          <Route exact path="/rankings" component={Ranking} />
          <Route exact path="/schedule" component={Schedule} />
          <Route exact path="/records/:recordType?" component={Record} />
          {/* <Route exact path="/criclytics" component={Criclytics} /> */}
          <Route exact path="/settings" component={Settings} />
          <Route exact path="/series" component={Series} />

          <Route exact path="/fantasyplayers" component={FantasyPlayers} />
          <Route exact path="/criclytics" component={Criclytics} />
          <Route
            exact
            path="/criclytics-slider/:matchId?"
            component={CriclyticsSlider}
          />
          <Route
            exact
            path="/fantasyTeamCreation"
            component={FantasyTeamCreation}
          />
          <Route exact path="/fantasyTeamCode" component={FantasyTeamCode} />
          <Route
            exact
            path="/playoffsProjection"
            component={PlayoffsProjection}
            history={this.props.history}
          />
          <Route
            exact
            path="/about"
            component={AboutCrictec}
            history={this.props.history}
          />
          <Route
            exact
            path="/TermofUse"
            component={TermofUse}
            history={this.props.history}
          />
          <Route
            exact
            path="/privacyPolicy"
            component={PrivacyPolicy}
            history={this.props.history}
          />
          <Route
            exact
            path="/cookiesPolicy"
            component={CookiesPolicy}
            history={this.props.history}
          />
          <Route exact path="/test-keystats" component={CriclyticsKeyStats} />
        </Switch>
      </React.Fragment>
    );
  }
}
