/**
 *
 * More
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
// import Header from "components/Common/Header";
import ChevronRight from "@material-ui/icons/ChevronRight";
import map from "lodash/map";
// import BottomBar from 'components/Common/BottomBar';
import BottomBar from "../../components-v2/commonComponent/BottomBar";
import HrLine from "../../components-v2/commonComponent/HrLine";
import {
  grey_10,
  white,
  cardShadow,
  grey_8
} from "../../styleSheet/globalStyle/color";

import SeriesImg from "./../../styleSheet/svg/seriesIcon.svg";

// import rightArrowBlack from "./../../styleSheet/svg/rightArrowBlack.svg";
// //
// import HistoricImg from "./../../styleSheet/svg/historicMatch.svg";
// import NewsImg from "./../../styleSheet/svg/newsArticle.svg";
//
// import PlayerImg from "./../../../styleSheet/svg/playerIcon.svg";
import PlayerImg from "./../../styleSheet/svg/playerIcon.svg";
import TeamImg from "./../../styleSheet/svg/teamIcon.svg";
import StadiumImg from "./../../styleSheet/svg/stadiumIcon.svg";
//
import PhotosImg from "./../../styleSheet/svg/photoIcon.svg";
import VideosImg from "./../../styleSheet/svg/videosIcon.svg";
//
import RankingImg from "./../../styleSheet/svg/rankingsIcon.svg";
import RecordImg from "./../../styleSheet/svg/recordsIcon.svg";
//
// import AboutImg from "./../../styleSheet/svg/settingsIcon.svg";
import crictecIcon from "./../../styleSheet/svg/crictecIconNew.svg";
// import FeedbackImg from "./../../styleSheet/svg/feedbackIcon.svg";
import AppDownload from "../../components-v2/commonComponent/AppDownload";
import { isAndroid } from "react-device-detect";

const MORE_LINKS = {
  1: [
    { title: "Series", route: "/series", icon: SeriesImg }
    // { title: "News and Articles", route: "/articles", icon: NewsImg },
    // { title: "Historic Matches", route: "/historic-matches", icon: HistoricImg }
  ],
  2: [
    { title: "Players", route: "/players", icon: PlayerImg },
    { title: "Teams", route: "/teams", icon: TeamImg },
    { title: "Stadiums", route: "/grounds", icon: StadiumImg }
  ],
  3: [
    { title: "Photo Gallery", route: "/photos", icon: PhotosImg },
    { title: "Videos", route: "/videos", icon: VideosImg }
  ],
  4: [
    { title: "Rankings", route: "/rankings", icon: RankingImg },
    { title: "Records", route: "/records", icon: RecordImg }
  ],
  5: [
    // { title: "Settings", route: "/settings", icon: settingsIcon },
    { title: "About Crictec", route: "/about", icon: crictecIcon }
    // { title: "Feedback", route: "/feedback", icon: feedbackIcon }
  ]
};

export class More extends React.Component {
  render() {
    const { history } = this.props;
    return (
      <div>
        <div
          style={{
            padding: 16
          }}
        >
          {/* MOre Title */}
          <div
            style={{
              fontFamily: "Montserrat",
              fontWeight: 700,
              fontSize: 22,
              color: grey_10,
              marginBottom: 12
            }}
          >
            More
          </div>
          <div
            style={{
              paddingBottom: 52
              // marginTop: 52
            }}
          >
            {map(MORE_LINKS, (list, key) => (
              <div
                style={{
                  background: white,
                  boxShadow: cardShadow,
                  borderRadius: 3,
                  marginBottom: 12
                }}
              >
                {list.map((item, index) => (
                  <div>
                    <div
                      style={{
                        display: "flex",
                        flex: 1,
                        alignItems: "center",
                        padding: 12
                      }}
                      onClick={e => {
                        e.preventDefault();
                        this.props.history.push(`${item.route}`);
                      }}
                    >
                      <div
                        style={{
                          flex: 0.8,
                          display: "flex",
                          alignItems: "center"
                        }}
                      >
                        <img
                          src={item.icon}
                          alt=""
                          style={{
                            width: 16,
                            height: 16,
                            marginRight: 24
                          }}
                        />
                        <div
                          style={{
                            fontFamily: "Montserrat",
                            fontSize: 14,
                            color: grey_10,
                            fontWeight: 600
                          }}
                        >
                          {item.title}
                        </div>
                      </div>
                      <div
                        style={{
                          flex: 0.2,
                          display: "flex",
                          justifyContent: "flex-end"
                        }}
                      >
                        <ChevronRight style={{ fontSize: 24, color: grey_8 }} />
                      </div>
                    </div>
                    {list.length - 1 != index && <HrLine />}
                  </div>
                ))}
              </div>
            ))}
            {isAndroid ? (
              <AppDownload
                rootStyles={{
                  margin: "0 0 20px"
                }}
                url={
                  "https://play.google.com/store/apps/details?id=com.crictec.cricket"
                }
                isAndroid
              />
            ) : (
              <AppDownload
                rootStyles={{
                  margin: "0 0 20px"
                }}
                url={"https://itunes.apple.com/us/app/cricket-com/id1460360497"}
              />
            )}
          </div>
        </div>

        {/* bottom header */}
        <BottomBar
          {...history}
          tabs={[
            { key: "home", route: "/" },
            { key: "schedule", route: "schedule" },
            { key: "Criclytics", route: "criclytics" },
            { key: "news", route: "articles" },
            { key: "more", route: "more" }
          ]}
        />
      </div>
    );
  }
}

More.propTypes = {
  dispatch: PropTypes.func.isRequired,
  history: PropTypes.object
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps
);

export default compose(withConnect)(More);
