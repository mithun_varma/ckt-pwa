import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the Photos state domain
 */

const selectPhotosDomain = state => state.photos || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Photos
 */

const makeSelectPhotos = () => createSelector(selectPhotosDomain, substate => substate);

export default makeSelectPhotos;
export { selectPhotosDomain };
