import { call, put, takeLatest } from 'redux-saga/effects';

import { API_GET_GALLERY} from 'utils/requestUrls';
import request from 'utils/request';

import * as photosConstants from './constants';
import * as photosActions from './actions';


export function* getPhotoGallery(payload) {
  // ;
  const requestUrl = API_GET_GALLERY;
  const config = { ...requestUrl };
 
  try {
    const options = yield call(request, config);
  
    yield put(
      photosActions.fetchPhotoGallerySuccess({
      data: options.data
      }),
    );
  } 
  catch (err) {
    yield put(photosActions.fetchPhotoGalleryFailure(err));
  }
}

export default function* defaultSaga() {
  yield takeLatest(photosConstants.FETCH_GALLERY, getPhotoGallery);
}
