
import { showToast } from 'utils/misc';
import * as photosConstants from './constants';

const ACTIVE_TOAST = {};

/* SCHEDULE MATCHES actions */

export const fetchPhotoGallery = payload =>({
  type:photosConstants.FETCH_GALLERY,
  payload,
})

export const fetchPhotoGallerySuccess = payload =>({
  type:photosConstants.FETCH_GALLERY_SUCCESS,
  payload,
})

export const fetchPhotoGalleryFailure = payload =>({
  type:photosConstants.FETCH_GALLERY_FAILURE,
  payload,
})

