import React from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectPhotos from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import * as photosActions from "./actions";
import PhotosUI from "./../../components-v2/screenComponent/photos/Photos";
import ReactGA from "react-ga";
import { Helmet} from 'react-helmet';
/* eslint-disable react/prefer-stateless-function */
export class Photos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showPhoto: false,
      activeImage: null,
      galleryId: ""
    };
  }
  componentDidMount = () => {
    // ReactGA.initialize("UA-136526387-1");
    // ReactGA.pageview(
    //   "https://www.cricket.com" +
    //     window.location.pathname +
    //     window.location.search
    // );
    this.props.fetchPhotoGallery();
  };

  render() {
    const { photos, history } = this.props;
    const photoGallery = photos.photoGallery;
    if (photoGallery[0]) {
      const photoObject = photoGallery[0].data;
      //convert photoobjecxt into array;
      var photoArray = [];
      Object.keys(photoObject).map(function(key) {
        let arrayData = photoObject[key];
        photoArray.push(arrayData);
      });

      return (
        <div>
          <Helmet titleTemplate="%s | cricket.com">
            <title>Cricket Images | Cricket Pictures | Cricket Photos</title>
            <meta
              name="description"
              content="Check out the latest cricket pictures and images  of all matches and tournaments, grounds, players etc at cricket.com. "
            />
            <meta
              name="keywords"
              content="cricket images, cricket pictures, cricket photos, cricket photos india"
            />
            <link
              rel="canonical"
              href={`www.cricket.com${history &&
                history.location &&
                history.location.pathname}`}
            />
          </Helmet>
          <PhotosUI history={history} photos={photoArray} />
        </div>
      );
    } else {
      return null;
    }
  }
}

const mapStateToProps = createStructuredSelector({
  photos: makeSelectPhotos()
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    fetchPhotoGallery: payload =>
      dispatch(photosActions.fetchPhotoGallery(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "photos", reducer });
const withSaga = injectSaga({ key: "photos", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(Photos);
