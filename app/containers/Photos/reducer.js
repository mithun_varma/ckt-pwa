
import * as photoActions from './constants';

export const initialState = {
  photoGalleryloading: false,
  photoGallery: [],

};

function photosReducer(state = initialState, action) {
  switch (action.type) {
    case photoActions.FETCH_GALLERY:
      return{
        ...state,
        photoGalleryloading:true,

        
      }
    case photoActions.FETCH_GALLERY_SUCCESS:
      return{
        ...state,
        photoGalleryloading:false,
        photoGallery:[action.payload.data]

      }
    case photoActions.FETCH_GALLERY_FAILURE:
        return{
          ...state,
          photoGalleryloading:false,
          photoGallery:[]

        }
    default:
      return state;
  }
}

export default photosReducer;
