import { call, put, takeLatest } from "redux-saga/effects";
import {
  API_GET_FANTASY_ANALYSIS,
  API_GET_NEWS_OPINION,
  API_GET_MATCH_REPORT,
  API_GET_ARTICLE_DETAILS,
  API_GET_SUGGESTION_OPINION,
  API_GET_VIDEOS_ARTICLE,
  API_GET_PRE_ANALYSIS,
  API_SERIES_DETAILS_ARTICLES,
  API_GET_IMAGES
} from "utils/requestUrls";
import request from "utils/request";

import * as articlesConstants from "./constants";
import * as articleActions from "./actions";

export function* getFantasyAnalysis(payload) {
  const config = {
    ...API_GET_FANTASY_ANALYSIS,
    params: {
      ...API_GET_FANTASY_ANALYSIS.params,
      ...payload.payload
    }
  };
  try {
    const options = yield call(request, config);
    yield put(articleActions.fetchFantasyAnalysisSuccess(options));
  } catch (err) {
    yield put(articleActions.fetchFantasyAnalysisFailure(err));
  }
}

export function* getSuggestion(payload) {
  const config = {
    ...API_GET_SUGGESTION_OPINION,
    params: {
      ...API_GET_SUGGESTION_OPINION.params,
      ...payload.payload
    }
  };
  try {
    const options = yield call(request, config);
    yield put(articleActions.fetchSuggestionSuccess(options));
  } catch (err) {
    yield put(articleActions.fetchSuggestionFailure(err));
  }
}

export function* getNewsOpinion(payload) {
  const config = {
    ...API_GET_NEWS_OPINION,
    params: {
      ...API_GET_NEWS_OPINION.params,
      ...payload.payload
    }
  };
  try {
    const options = yield call(request, config);
    yield put(articleActions.fetchNewsOpinionSuccess(options));
  } catch (err) {
    yield put(articleActions.fetchNewsOpinionFailure(err));
  }
}

export function* getCriclyticsArticles(payload) {
  const config = {
    ...API_GET_NEWS_OPINION,
    params: {
      ...API_GET_NEWS_OPINION.params,
      ...payload.payload
    }
  };
  try {
    const options = yield call(request, config);
    yield put(articleActions.fetchCriclyticsArticlesSuccess(options));
  } catch (err) {
    yield put(articleActions.fetchCriclyticsArticlesFailure(err));
  }
}

export function* getFeaturedVideos(payload) {
  const config = {
    ...API_GET_VIDEOS_ARTICLE,
    params: {
      ...API_GET_VIDEOS_ARTICLE.params,
      ...payload.payload
    }
  };
  try {
    const options = yield call(request, config);
    yield put(articleActions.fetchFeaturedVideosSuccess(options.data));
  } catch (err) {
    yield put(articleActions.fetchFeaturedVideosFailure(err));
  }
}

export function* getMatchReport(payload) {
  const config = {
    ...API_GET_MATCH_REPORT,
    params: {
      ...API_GET_MATCH_REPORT.params,
      ...payload.payload
    }
  };
  try {
    const options = yield call(request, config);
    yield put(articleActions.fetchMatchReportSuccess(options));
  } catch (err) {
    yield put(articleActions.fetchMatchReportFailure(err));
  }
}

export function* getPreMatchAnalysis(payload) {
  const config = {
    ...API_GET_PRE_ANALYSIS,
    params: {
      ...API_GET_PRE_ANALYSIS.params,
      ...payload.payload
    }
  };
  try {
    const options = yield call(request, config);
    yield put(articleActions.fetchPreAnalysisSuccess(options));
  } catch (err) {
    yield put(articleActions.fetchPreAnalysisFailure(err));
  }
}

export function* getArticleDetails(payload) {
  try {
    const options = yield call(
      request,
      API_GET_ARTICLE_DETAILS(payload.payload.id)
    );
    yield put(articleActions.fetchArticleDetailsSuccess(options.data));
  } catch (err) {
    yield put(articleActions.fetchArticleDetailsFailure(err));
  }
}
export function* getSeriesNews(payload) {
  const url = API_SERIES_DETAILS_ARTICLES(payload.payload.seriesId);
  const config = {
    ...url,
    params: {
      ...payload.payload,
      types: ["news"]
    }
  };
  try {
    const options = yield call(request, config);
    yield put(articleActions.fetchseriesNewsSuccess(options.data.data));
  } catch (err) {
    yield put(articleActions.fetchSuggestionFailure(err));
  }
}

export function* getHomeImages(payload) {
  const config = {
    ...API_GET_IMAGES,
    params: {
      ...API_GET_IMAGES.params,
      ...payload.payload
    }
  };
  try {
    const options = yield call(request, config);
    yield put(articleActions.fetchHomeImagesSuccess(options.data.data));
  } catch (err) {
    yield put(articleActions.fetchHomeImagesFailure(err));
  }
}

export function* getHighlightVideo(payload) {
  const config = {
    ...API_GET_VIDEOS_ARTICLE,
    params: {
      ...API_GET_VIDEOS_ARTICLE.params,
      ...payload.payload
    }
  };
  try {
    const options = yield call(request, config);
    yield put(articleActions.fetchHighlightSuccess(options.data.data));
  } catch (err) {
    yield put(articleActions.fetchHighlightFailure(err));
  }
}

export default function* defaultSaga() {
  yield takeLatest(
    articlesConstants.FETCH_FANTASY_ANALYSIS,
    getFantasyAnalysis
  );
  yield takeLatest(articlesConstants.FETCH_NEWS_OPINION, getNewsOpinion);
  yield takeLatest(articlesConstants.FETCH_SERIES_NEWS, getSeriesNews);
  yield takeLatest(articlesConstants.FETCH_SUGGESTION, getSuggestion);
  yield takeLatest(articlesConstants.FETCH_PRE_ANALYSIS, getPreMatchAnalysis);
  yield takeLatest(articlesConstants.FETCH_MATCH_REPORT, getMatchReport);
  yield takeLatest(articlesConstants.FETCH_ARTICLE_DETAILS, getArticleDetails);
  yield takeLatest(articlesConstants.FETCH_FEATURED_VIDEOS, getFeaturedVideos);
  yield takeLatest(articlesConstants.FETCH_WC_HIGHLIGHT, getHighlightVideo);
  yield takeLatest(articlesConstants.FETCH_HOME_IMAGES, getHomeImages);
  yield takeLatest(
    articlesConstants.FETCH_CRICLYTICS_ARTICLES,
    getCriclyticsArticles
  );
}
