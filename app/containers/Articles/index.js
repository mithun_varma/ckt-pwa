import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import Loader from "components-v2/commonComponent/Loader";
import EmptyState from "components-v2/commonComponent/EmptyState";
import FeatureArticleCard from "components-v2/Articles/FeatureArticleCard";
import ArticleCard from "components-v2/Articles/ArticleCard";
import HrLine from "components-v2/commonComponent/HrLine";
import CardGradientTitle from "components-v2/commonComponent/CardGradientTitle";
import {
  white,
  grey_10,
  cardShadow,
  red_Orange
} from "../../styleSheet/globalStyle/color";
// ===

import makeSelectArticles from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import BottomBar from "../../components-v2/commonComponent/BottomBar";

import * as articlesActions from "./actions";
import ReactGA from "react-ga";
import { Helmet } from "react-helmet";

/* eslint-disable react/prefer-stateless-function */
/* eslint-disable react/no-unused-prop-types */

let scrollTop = null;
export class Articles extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      initial: true,
      activeButton: "news thread",
      title: "",
      isHeaderBackground: false
    };
    this.slideSettings = {
      dots: false,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      autoplay: false,
      centerMode: true
    };
    this.loadMoreItems = this.loadMoreItems.bind(this);
    this.cardOnClick = this.cardOnClick.bind(this);
  }

  componentDidMount = () => {
    // ReactGA.initialize("UA-136526387-1");
    // ReactGA.pageview(
    //   "https://www.cricket.com" +
    //     window.location.pathname +
    //     window.location.search
    // );
    window.addEventListener("scroll", this.handleScroll);
    this.props.fetchFantasyAnalysis({
      limit: 3,
      skip: 0
    });
    this.props.fetchPreAnalysis({
      limit: 3,
      skip: 0
    });
    this.props.fetchNewsOpinion({
      limit: 3,
      skip: 0
    });
    this.props.fetchSuggestion({
      limit: 3,
      skip: 0
    });
  };

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = event => {
    ({ scrollTop } = event.target.scrollingElement);
    // console.log("object: ", scrollTop);
    if (scrollTop > 26) {
      this.setState({
        title: "News & Articles",
        isHeaderBackground: true
      });
    } else if (scrollTop < 27) {
      this.setState({
        title: "",
        isHeaderBackground: false
      });
    }
  };

  static getDerivedStateFromProps(props, state) {
    if (state.initial)
      return {
        initial: false
      };
    return null;
  }

  cardOnClick = path => {
    this.props.history.push(path);
  };
  loadMoreItems = () => {
    if (this.props.match.params.type === "analysis") {
      this.props.fetchFantasyAnalysis({
        limit: 10,
        skip: this.props.articles.fantasyAnalysis.length
      });
    } else if (this.props.match.params.type === "news-opinion") {
      this.props.fetchNewsOpinion({
        limit: 10,
        skip:
          this.props.articles.newsOpinions.length +
          this.props.articles.newsOpinionsFeatured.length
      });
    } else if (this.props.match.params.type === "match-report") {
      this.props.fetchMatchReport({
        limit: 10,
        skip:
          this.props.articles.matchReports.length +
          this.props.articles.matchReportsFeatured.length
      });
    } else if (this.props.match.params.type === "pre_match_analysis") {
      this.props.fetchPreAnalysis({
        limit: 10,
        skip: this.props.articles.prematchAnalysis.length
      });
    }
  };

  handleActiveButton = activeButton => {
    this.setState({
      activeButton
    });
  };

  handleValidateArticlesLength = key => {
    if (this.props.articles[key].length > 0) {
      return true;
    }
    return false;
  };

  redirectTo = path => {
    this.props.history.push(path);
  };

  render() {
    const { articles, history } = this.props;
    let title = "";
    if (this.props.match.params.type === "analysis") {
      title = "Analysis";
    } else if (this.props.match.params.type === "news-opinion") {
      title = "News & Opinion";
    } else if (this.props.match.params.type === "match-report") {
      title = "Match Reports";
    } else if (this.props.match.params.type === "pre_match_analysis") {
      title = "Pre Match Analysis";
    }
    return (
      <React.Fragment>
        <Helmet titleTemplate="%s | cricket.com">
          <title>
            Cricket News | Cricket Update | Match Analysis And Reports
          </title>
          <meta
            name="description"
            content="Find latest cricket updates, articles and insights. Some interesting blog topics are Rajasthan Royals take on Andre Russell juggernaut at home,The yellow brigade are all set to conquer Jaipur etc."
          />
          <meta
            name="keywords"
            content="ipl news, india cricket news, ipl latest news, ipl news today, cricket news today match"
          />
          <link
            rel="canonical"
            href={`www.cricket.com${this.props.history.location.pathname}`}
          />
        </Helmet>
        <div
          style={{
            // padding: "54px 16px 16px",
            padding: "16px 16px 16px",
            minHeight: "120vh",
            marginBottom: 60
          }}
        >
          {/* <Header
            title={this.state.title}
            isHeaderBackground={this.state.isHeaderBackground}
            leftArrowBack
            textColor={black}
            backgroundColor={grey_4}
            history={history}
            leftIconOnClick={() => {
              history.goBack();
            }}
          /> */}
          <div
            style={{
              fontFamily: "Montserrat",
              fontWeight: 700,
              fontSize: 22,
              color: grey_10,
              marginBottom: 12
            }}
          >
            News & Articles
          </div>
          {/* Section 1 Latest News */}
          <div
            style={{
              background: white,
              borderRadius: 3,
              boxShadow: cardShadow,
              marginBottom: 10
            }}
          >
            <div>
              {/* <ButtonBar
                items={BUTTON_CONFIG}
                activeItem={this.state.activeButton}
                onClick={this.handleActiveButton}
                increaseLastItemWidth
              />
              <HrLine /> */}
              <CardGradientTitle
                title="Latest News"
                subtitle={
                  this.handleValidateArticlesLength("newsOpinions") &&
                  "View All"
                }
                cardType="news"
                isSubtitleClickable
                redirectTo={this.redirectTo}
                rootStyles={{
                  fontSize: 14,
                  borderRadius: "3px 3px"
                }}
                subtitleStyles={{
                  color: red_Orange,
                  marginRight: 10,
                  fontSize: 12
                }}
              />
            </div>
            {articles.newsOpinionLoading ? (
              <div style={{ padding: 10 }}>
                <Loader styles={{ height: "150px" }} noOfLoaders={1} />
              </div>
            ) : (
              articles.newsOpinions.length > 0 && (
                <div>
                  <FeatureArticleCard
                    article={articles.newsOpinions[0]}
                    articleHost={articles.articleHost}
                    cardType="news"
                    // key={`${key + 1}`}
                    cardOnClick={this.cardOnClick}
                  />
                  <HrLine />
                </div>
              )
            )}
            {articles.newsOpinionLoading ? (
              <div style={{ padding: 10 }}>
                <Loader styles={{ height: "120px" }} noOfLoaders={2} />
              </div>
            ) : (
              articles.newsOpinions.slice(1, 3).map((news, key) => (
                <div>
                  <ArticleCard
                    article={news}
                    articleHost={articles.articleHost}
                    cardType="news"
                    key={`${key + 1}`}
                    cardOnClick={this.cardOnClick}
                  />
                  <HrLine />
                </div>
              ))
            )}
            {articles.newsOpinions.length == 0 && (
              <EmptyState msg="No news found" />
            )}
          </div>
          {/* Section 1 Latest News closing */}

          {/* Section 2 Pre-Match Analysis */}
          <div
            style={{
              background: white,
              borderRadius: 3,
              boxShadow: cardShadow,
              marginBottom: 10
            }}
          >
            <CardGradientTitle
              title="Post Match Analysis"
              subtitle={
                this.handleValidateArticlesLength("fantasyAnalysis") &&
                "View All"
              }
              cardType="analysis"
              isSubtitleClickable
              redirectTo={this.redirectTo}
              rootStyles={{
                background: white,
                fontSize: 14,
                borderRadius: "3px 3px"
              }}
              subtitleStyles={{
                color: red_Orange,
                paddingRight: 8,
                fontSize: 12
              }}
            />

            <div>
              {articles.fantasyAnalysisLoading ? (
                <div style={{ padding: 10 }}>
                  <Loader styles={{ height: "150px" }} noOfLoaders={1} />
                </div>
              ) : (
                articles.fantasyAnalysis.length > 0 && (
                  <div>
                    <FeatureArticleCard
                      article={articles.fantasyAnalysis[0]}
                      articleHost={articles.articleHost}
                      cardType="analysis"
                      // key={`${key + 1}`}
                      cardOnClick={this.cardOnClick}
                    />
                    <HrLine />
                  </div>
                )
              )}
              {articles.fantasyAnalysisLoading ? (
                <div style={{ padding: 10 }}>
                  <Loader styles={{ height: "120px" }} noOfLoaders={2} />
                </div>
              ) : (
                articles.fantasyAnalysis.slice(1, 3).map((analysis, key) => (
                  <div>
                    <ArticleCard
                      article={analysis}
                      articleHost={articles.articleHost}
                      cardType="analysis"
                      key={`${key + 1}`}
                      cardOnClick={this.cardOnClick}
                    />
                    <HrLine />
                  </div>
                ))
              )}
            </div>
            {articles.fantasyAnalysis.length == 0 && (
              <EmptyState msg="No analysis found" />
            )}
          </div>

          <div
            style={{
              background: white,
              borderRadius: 3,
              boxShadow: cardShadow,
              marginBottom: 10
            }}
          >
            <CardGradientTitle
              title="Pre Match Analysis"
              subtitle={
                this.handleValidateArticlesLength("prematchAnalysis") &&
                "View All"
              }
              cardType="pre-match-analysis"
              isSubtitleClickable
              redirectTo={this.redirectTo}
              rootStyles={{
                background: white,
                fontSize: 14,
                borderRadius: "3px 3px"
              }}
              subtitleStyles={{
                color: red_Orange,
                paddingRight: 8,
                fontSize: 12
              }}
            />

            <div>
              {articles.preanalysisLoading ? (
                <div style={{ padding: 10 }}>
                  <Loader styles={{ height: "150px" }} noOfLoaders={1} />
                </div>
              ) : (
                articles.prematchAnalysis.length > 0 && (
                  <div>
                    <FeatureArticleCard
                      article={articles.prematchAnalysis[0]}
                      articleHost={articles.articleHost}
                      cardType="analysis"
                      // key={`${key + 1}`}
                      cardOnClick={this.cardOnClick}
                    />
                    <HrLine />
                  </div>
                )
              )}
              {articles.preanalysisLoading ? (
                <div style={{ padding: 10 }}>
                  <Loader styles={{ height: "120px" }} noOfLoaders={2} />
                </div>
              ) : (
                articles.prematchAnalysis.slice(1, 3).map((analysis, key) => (
                  <div>
                    <ArticleCard
                      article={analysis}
                      articleHost={articles.articleHost}
                      cardType="analysis"
                      key={`${key + 1}`}
                      cardOnClick={this.cardOnClick}
                    />
                    <HrLine />
                  </div>
                ))
              )}
            </div>
            {articles.prematchAnalysis.length == 0 && (
              <EmptyState msg="No analysis found" />
            )}
          </div>
          {/* Section 2 Pre-Match Analysis closing */}
          {/* Section 3 */}
          {/* <div
            style={{
              background: white,
              borderRadius: 3,
              boxShadow: cardShadow,
              marginBottom: 10
            }}
          >
            <CardGradientTitle
              title="Our Suggestions"
              subtitle={
                this.handleValidateArticlesLength("suggestion") && "View All"
              }
              cardType="suggestion"
              isSubtitleClickable
              redirectTo={this.redirectTo}
              rootStyles={{
                background: white,
                fontSize: 14,
                borderRadius: "3px 3px"
              }}
              subtitleStyles={{
                color: red_Orange,
                paddingRight: 8,
                fontSize: 12
              }}
            />

            <div>
              {articles.suggestionLoading ? (
                <div style={{ padding: 10 }}>
                  <Loader styles={{ height: "150px" }} noOfLoaders={1} />
                </div>
              ) : (
                articles.suggestion.length > 0 && (
                  <div>
                    <FeatureArticleCard
                      article={articles.suggestion[0]}
                      articleHost={articles.articleHost}
                      cardType="opinion"
                      // key={`${key + 1}`}
                      cardOnClick={this.cardOnClick}
                    />
                    <HrLine />
                  </div>
                )
              )}
              {articles.suggestionLoading ? (
                <div style={{ padding: 10 }}>
                  <Loader styles={{ height: "120px" }} noOfLoaders={2} />
                </div>
              ) : (
                articles.suggestion.slice(1, 3).map((opinion, key) => (
                  <div>
                    <ArticleCard
                      article={opinion}
                      articleHost={articles.articleHost}
                      cardType="opinion"
                      key={`${key + 1}`}
                      cardOnClick={this.cardOnClick}
                    />
                    <HrLine />
                  </div>
                ))
              )}
            </div>
            {articles.suggestion.length == 0 && (
              <EmptyState msg="No suggestion found" />
            )}
          </div> */}
          {/* Section 3 closing */}
        </div>
        <BottomBar
          {...this.props.history}
          tabs={[
            { key: "home", route: "/" },
            { key: "schedule", route: "schedule" },
            { key: "Criclytics", route: "criclytics" },
            { key: "news", route: "articles" },
            { key: "more", route: "more" }
          ]}
        />
      </React.Fragment>
    );
  }
}

Articles.propTypes = {
  fetchFantasyAnalysis: PropTypes.func,
  fetchMatchReport: PropTypes.func,
  fetchNewsOpinion: PropTypes.func,
  fetchSuggestion: PropTypes.func,
  articles: PropTypes.object,
  match: PropTypes.object,
  history: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  articles: makeSelectArticles()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchFantasyAnalysis: payload =>
      dispatch(articlesActions.fetchFantasyAnalysis(payload)),
    fetchMatchReport: payload =>
      dispatch(articlesActions.fetchMatchReport(payload)),
    fetchNewsOpinion: payload =>
      dispatch(articlesActions.fetchNewsOpinion(payload)),
    fetchSuggestion: payload =>
      dispatch(articlesActions.fetchSuggestion(payload)),
    fetchPreAnalysis: payload =>
      dispatch(articlesActions.fetchPreAnalysis(payload)),
    fetchFeaturedVideos: payload =>
      dispatch(articlesActions.fetchFeaturedVideos(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "articles", reducer });
const withSaga = injectSaga({ key: "articles", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(Articles);
