/*
 *
 * Articles reducer
 *
 */

import sortBy from "lodash/sortBy";
import partition from "lodash/partition";
import {
  FETCH_FANTASY_ANALYSIS,
  FETCH_FANTASY_ANALYSIS_SUCCESS,
  FETCH_FANTASY_ANALYSIS_FAILURE,
  FETCH_NEWS_OPINION,
  FETCH_NEWS_OPINION_FAILURE,
  FETCH_NEWS_OPINION_SUCCESS,
  FETCH_MATCH_REPORT,
  FETCH_MATCH_REPORT_FAILURE,
  FETCH_MATCH_REPORT_SUCCESS,
  FETCH_ARTICLE_DETAILS,
  FETCH_ARTICLE_DETAILS_SUCCESS,
  FETCH_ARTICLE_DETAILS_FAILURE,
  FETCH_SUGGESTION,
  FETCH_SUGGESTION_SUCCESS,
  FETCH_SUGGESTION_FAILURE,
  FETCH_FEATURED_VIDEOS,
  FETCH_FEATURED_VIDEOS_FAILURE,
  FETCH_FEATURED_VIDEOS_SUCCESS,
  FETCH_PRE_ANALYSIS,
  FETCH_PRE_ANALYSIS_FAILURE,
  FETCH_PRE_ANALYSIS_SUCCESS,
  FETCH_CRICLYTICS_ARTICLES,
  FETCH_CRICLYTICS_ARTICLES_SUCCESS,
  FETCH_CRICLYTICS_ARTICLES_FAILURE,
  FETCH_SERIES_NEWS,
  FETCH_SERIES_NEWS_SUCCESS,
  FETCH_SERIES_NEWS_FAILURE,
  FETCH_HOME_IMAGES,
  FETCH_HOME_IMAGES_FAILURE,
  FETCH_HOME_IMAGES_SUCCESS,
  FETCH_WC_HIGHLIGHT,
  FETCH_WC_HIGHLIGHT_FAILURE,
  FETCH_WC_HIGHLIGHT_SUCCESS
} from "./constants";

export const initialState = {
  loadCount: 0,
  articleHost: "",
  fantasyAnalysisLoading: false,
  fantasyAnalysis: [],
  fantasyAnalysisFeatured: [],
  fantasyAnalysisOldLength: 0,
  fantasyAnalysisInitiallyLoaded: false,

  newsOpinionLoading: false,
  newsOpinions: [],
  newsOpinionsOldLength: 0,
  newsOpinionsFeatured: [],
  newsOpinionsInitiallyLoaded: false,

  suggestionLoading: false,
  suggestion: [],
  suggestionOldLength: 0,
  suggestionFeatured: [],
  suggestionInitiallyLoaded: false,

  matchReportLoading: false,
  matchReports: [],
  matchReportsOldLength: 0,
  matchReportsFeatured: [],
  matchReportsInitiallyLoaded: false,
  articleDetailsLoading: false,
  articleDetails: {},

  featuredVideos: [],
  featuredVideosLoading: false,
  prematchAnalysis: [],
  prematchAnalysisOldLength: 0,
  prematchAnalysisInitiallyLoaded: false,
  preanalysisLoading: false,

  criclyticsArticlesLoading: false,
  criclyticsArticles: [],
  criclyticsArticlesOldLength: 0,
  criclyticsArticlesFeatured: [],
  criclyticsArticlesInitiallyLoaded: false,

  seriesNewsLoading: false,
  seriesNews: [],

  highlight: [],
  highlightLoading: false,

  homeImagesLoading: false,
  homeImages: []
};

/* eslint-disable no-else-return */

function articlesReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PRE_ANALYSIS:
      if (!state.prematchAnalysisInitiallyLoaded) {
        return { ...state, preanalysisLoading: true };
      } else {
        return state;
      }
    case FETCH_PRE_ANALYSIS_SUCCESS:
      if (!action.payload.config.params.skip) {
        return {
          ...state,
          preanalysisLoading: false,
          prematchAnalysis: sortBy(
            action.payload.data.data,
            d => d.filters.featured
          ),
          articleHost: action.payload.data.host,
          loadCount: state.loadCount + 1,
          prematchAnalysisOldLength: 0
        };
      } else {
        const prematchAnalysis = [...state.prematchAnalysis];
        return {
          ...state,
          articleHost: action.payload.data.host,
          preanalysisLoading: false,
          prematchAnalysis: prematchAnalysis.concat(action.payload.data.data),
          loadCount: state.loadCount + 1,
          prematchAnalysisOldLength: state.prematchAnalysis.length
        };
      }
    case FETCH_SERIES_NEWS:
      return {
        ...state,
        seriesNewsLoading: true
      };
    case FETCH_SERIES_NEWS_SUCCESS:
      return {
        ...state,
        seriesNewsLoading: false,
        seriesNews: action.payload
      };
    case FETCH_SERIES_NEWS_FAILURE:
      return {
        ...state,
        seriesNewsLoading: false
      };
    case FETCH_WC_HIGHLIGHT:
      return {
        ...state,
        highlightLoading: true
      };
    case FETCH_WC_HIGHLIGHT_SUCCESS:
      return {
        ...state,
        highlightLoading: false,
        highlight: action.payload
      };
    case FETCH_WC_HIGHLIGHT_FAILURE:
      return {
        ...state,
        highlightLoading: false
      };
    case FETCH_HOME_IMAGES:
      return {
        ...state,
        homeImagesLoading: true
      };
    case FETCH_HOME_IMAGES_SUCCESS:
      return {
        ...state,
        homeImagesLoading: false,
        homeImages: action.payload
      };
    case FETCH_HOME_IMAGES_FAILURE:
      return {
        ...state,
        homeImagesLoading: false
      };
    case FETCH_PRE_ANALYSIS_FAILURE:
      return {
        ...state,
        preanalysisLoading: false
      };
    // Criclytics Articles case
    case FETCH_CRICLYTICS_ARTICLES:
      if (!state.criclyticsArticlesInitiallyLoaded) {
        return { ...state, criclyticsArticlesLoading: true };
      } else {
        return state;
      }
    case FETCH_CRICLYTICS_ARTICLES_SUCCESS:
      if (!action.payload.config.params.skip) {
        return {
          ...state,
          criclyticsArticlesLoading: false,
          criclyticsArticles: sortBy(
            action.payload.data.data,
            d => d.filters.featured
          ),
          articleHost: action.payload.data.host,
          loadCount: state.loadCount + 1,
          criclyticsArticlesOldLength: 0
        };
      } else {
        const criclyticsArticles = [...state.criclyticsArticles];
        return {
          ...state,
          articleHost: action.payload.data.host,
          preanalysisLoading: false,
          criclyticsArticles: criclyticsArticles.concat(
            action.payload.data.data
          ),
          loadCount: state.loadCount + 1,
          criclyticsArticlesOldLength: state.criclyticsArticles.length
        };
      }
    case FETCH_CRICLYTICS_ARTICLES_FAILURE:
      return {
        ...state,
        criclyticsArticlesLoading: false
      };
    // Criclytics Articles case end
    case FETCH_FEATURED_VIDEOS:
      return {
        ...state,
        featuredVideosLoading: false
      };
    case FETCH_FEATURED_VIDEOS_SUCCESS:
      return {
        ...state,
        featuredVideosLoading: false,
        featuredVideos: action.payload.data
      };
    case FETCH_FEATURED_VIDEOS_FAILURE:
      return {
        ...state,
        featuredVideosLoading: false
      };

    // Analysis Article Case
    case FETCH_FANTASY_ANALYSIS:
      if (!state.fantasyAnalysisInitiallyLoaded) {
        return { ...state, fantasyAnalysisLoading: true };
      } else {
        return state;
      }
    case FETCH_FANTASY_ANALYSIS_SUCCESS: {
      if (!action.payload.config.params.skip) {
        return {
          ...state,
          fantasyAnalysisLoading: false,
          fantasyAnalysis: sortBy(
            action.payload.data.data,
            d => d.filters.featured
          ),
          articleHost: action.payload.data.host,
          loadCount: state.loadCount + 1,
          fantasyAnalysisOldLength: 0
        };
      } else {
        const fantasyAnalysis = [...state.fantasyAnalysis];
        return {
          ...state,
          articleHost: action.payload.data.host,
          fantasyAnalysisLoading: false,
          fantasyAnalysis: fantasyAnalysis.concat(action.payload.data.data),
          loadCount: state.loadCount + 1,
          fantasyAnalysisOldLength: state.fantasyAnalysis.length
        };
      }
    }
    case FETCH_FANTASY_ANALYSIS_FAILURE:
      return { ...state, fantasyAnalysisLoading: false };

    // News Article Case
    case FETCH_NEWS_OPINION:
      if (!state.newsOpinionsInitiallyLoaded) {
        return { ...state, newsOpinionLoading: true };
      } else {
        return state;
      }
    case FETCH_NEWS_OPINION_SUCCESS: {
      if (!action.payload.config.params.skip) {
        return {
          ...state,
          articleHost: action.payload.data.host,
          newsOpinionLoading: false,
          newsOpinions: sortBy(
            action.payload.data.data,
            d => d.filters.featured
          ),
          newsOpinionsOldLength: 0
        };
      } else {
        const newsOpinions = [...state.newsOpinions];
        return {
          ...state,
          articleHost: action.payload.data.host,
          newsOpinionLoading: false,
          newsOpinions: newsOpinions.concat(action.payload.data.data),
          newsOpinionsOldLength: state.newsOpinions.length
        };
      }
    }
    case FETCH_NEWS_OPINION_FAILURE:
      return { ...state, newsOpinionLoading: false };

    // Suggestion Article Case
    case FETCH_SUGGESTION:
      if (!state.suggestionInitiallyLoaded) {
        return { ...state, suggestionLoading: true };
      } else {
        return state;
      }
    case FETCH_SUGGESTION_SUCCESS: {
      if (!action.payload.config.params.skip) {
        return {
          ...state,
          articleHost: action.payload.data.host,
          suggestionLoading: false,
          suggestion: sortBy(action.payload.data.data, d => d.filters.featured),
          suggestionOldLength: 0
        };
      } else {
        const suggestion = [...state.suggestion];
        return {
          ...state,
          articleHost: action.payload.data.host,
          suggestionLoading: false,
          suggestion: suggestion.concat(action.payload.data.data),
          suggestionOldLength: state.suggestion.length
        };
      }
    }
    case FETCH_SUGGESTION_FAILURE:
      return { ...state, suggestionLoading: false };

    // Match Report Case
    case FETCH_MATCH_REPORT:
      return { ...state, matchReportLoading: true };
    case FETCH_MATCH_REPORT_SUCCESS: {
      if (!action.payload.config.params.skip) {
        const matchReportGrouped = partition(
          action.payload.data.data,
          d => d.filters.featured
        );
        const [matchReportsFeatured, matchReports] = matchReportGrouped;
        return {
          ...state,
          articleHost: action.payload.data.host,
          matchReportLoading: false,
          matchReports,
          matchReportsFeatured,
          matchReportsInitiallyLoaded: true,
          matchReportsOldLength: 0
        };
      } else {
        const matchReports = [...state.matchReports];
        return {
          ...state,
          articleHost: action.payload.data.host,
          matchReportsInitiallyLoaded: true,
          matchReportLoading: false,
          matchReports: matchReports.concat(action.payload.data.data),
          matchReportsOldLength: state.matchReports.length
        };
      }
    }
    case FETCH_MATCH_REPORT_FAILURE:
      return { ...state, matchReportLoading: false };
    case FETCH_ARTICLE_DETAILS:
      return { ...state, articleDetailsLoading: false };
    case FETCH_ARTICLE_DETAILS_SUCCESS: {
      return {
        ...state,
        articleDetailsLoading: false,
        articleHost: action.payload.host,
        articleDetails: action.payload.data
      };
    }
    case FETCH_ARTICLE_DETAILS_FAILURE:
      return {
        ...state,
        articleDetailsLoading: false
      };
    default:
      return state;
  }
}

export default articlesReducer;
