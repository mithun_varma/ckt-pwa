/*
 *
 * Articles actions
 *
 */

import { showToast } from "utils/misc";
import {
  FETCH_FANTASY_ANALYSIS,
  FETCH_FANTASY_ANALYSIS_SUCCESS,
  FETCH_FANTASY_ANALYSIS_FAILURE,
  FETCH_NEWS_OPINION,
  FETCH_NEWS_OPINION_SUCCESS,
  FETCH_NEWS_OPINION_FAILURE,
  FETCH_MATCH_REPORT,
  FETCH_MATCH_REPORT_SUCCESS,
  FETCH_MATCH_REPORT_FAILURE,
  FETCH_ARTICLE_DETAILS,
  FETCH_ARTICLE_DETAILS_SUCCESS,
  FETCH_ARTICLE_DETAILS_FAILURE,
  FETCH_SUGGESTION,
  FETCH_SUGGESTION_SUCCESS,
  FETCH_SUGGESTION_FAILURE,
  FETCH_FEATURED_VIDEOS,
  FETCH_FEATURED_VIDEOS_FAILURE,
  FETCH_FEATURED_VIDEOS_SUCCESS,
  FETCH_PRE_ANALYSIS,
  FETCH_PRE_ANALYSIS_SUCCESS,
  FETCH_PRE_ANALYSIS_FAILURE,
  FETCH_CRICLYTICS_ARTICLES,
  FETCH_CRICLYTICS_ARTICLES_SUCCESS,
  FETCH_CRICLYTICS_ARTICLES_FAILURE,
  FETCH_SERIES_NEWS,
  FETCH_SERIES_NEWS_FAILURE,
  FETCH_SERIES_NEWS_SUCCESS,
  FETCH_HOME_IMAGES,
  FETCH_HOME_IMAGES_SUCCESS,
  FETCH_HOME_IMAGES_FAILURE,
  FETCH_WC_HIGHLIGHT,
  FETCH_WC_HIGHLIGHT_FAILURE,
  FETCH_WC_HIGHLIGHT_SUCCESS
} from "./constants";

const ACTIVE_TOAST = {};

export const fetchFeaturedVideos = payload => ({
  type: FETCH_FEATURED_VIDEOS,
  payload
});

export const fetchFeaturedVideosSuccess = payload => ({
  type: FETCH_FEATURED_VIDEOS_SUCCESS,
  payload
});

export const fetchFeaturedVideosFailure = err => {
  return {
    type: FETCH_FEATURED_VIDEOS_FAILURE,
    payload: err
  };
};

export const fetchHighlight = payload => ({
  type: FETCH_WC_HIGHLIGHT,
  payload
});

export const fetchHighlightSuccess = payload => ({
  type: FETCH_WC_HIGHLIGHT_SUCCESS,
  payload
});

export const fetchHighlightFailure = err => {
  return {
    type: FETCH_WC_HIGHLIGHT_FAILURE,
    payload: err
  };
};

export const fetchHomeImages = payload => ({
  type: FETCH_HOME_IMAGES,
  payload
});

export const fetchHomeImagesSuccess = payload => ({
  type: FETCH_HOME_IMAGES_SUCCESS,
  payload
});

export const fetchHomeImagesFailure = err => {
  return {
    type: FETCH_HOME_IMAGES_FAILURE,
    payload: err
  };
};

/* Pre match analysis articles actions */
export const fetchPreAnalysis = payload => ({
  type: FETCH_PRE_ANALYSIS,
  payload
});

export const fetchPreAnalysisSuccess = payload => ({
  type: FETCH_PRE_ANALYSIS_SUCCESS,
  payload
});

export const fetchPreAnalysisFailure = err => {
  showToast(ACTIVE_TOAST, "fetchpreanalysis", "Unable to fetch analysis");
  return {
    type: FETCH_PRE_ANALYSIS_FAILURE,
    payload: err
  };
};

export const fetchseriesNews = payload => ({
  type: FETCH_SERIES_NEWS,
  payload
});

export const fetchseriesNewsSuccess = payload => ({
  type: FETCH_SERIES_NEWS_SUCCESS,
  payload
});

export const fetchseriesNewsFailure = err => {
  showToast(ACTIVE_TOAST, "fetchpreanalysis", "Unable to fetch analysis");
  return {
    type: FETCH_SERIES_NEWS_FAILURE,
    payload: err
  };
};

/* Fantasy analysis articles actions */
export const fetchFantasyAnalysis = payload => ({
  type: FETCH_FANTASY_ANALYSIS,
  payload
});

export const fetchFantasyAnalysisSuccess = payload => ({
  type: FETCH_FANTASY_ANALYSIS_SUCCESS,
  payload
});

export const fetchFantasyAnalysisFailure = err => {
  showToast(
    ACTIVE_TOAST,
    "fetchFantasyAnalysis",
    "Unable to fetch fantasy analyis articles"
  );
  return {
    type: FETCH_FANTASY_ANALYSIS_FAILURE,
    payload: err
  };
};

/* News opinion articles actions */
export const fetchNewsOpinion = payload => ({
  type: FETCH_NEWS_OPINION,
  payload
});

export const fetchNewsOpinionSuccess = payload => ({
  type: FETCH_NEWS_OPINION_SUCCESS,
  payload
});

export const fetchNewsOpinionFailure = err => {
  showToast(ACTIVE_TOAST, "fetchNewsOpinion", "Unable to fetch news opinions");
  return {
    type: FETCH_NEWS_OPINION_FAILURE,
    payload: err
  };
};

/* Match report articles actions */
export const fetchMatchReport = payload => ({
  type: FETCH_MATCH_REPORT,
  payload
});

export const fetchMatchReportSuccess = payload => ({
  type: FETCH_MATCH_REPORT_SUCCESS,
  payload
});

export const fetchMatchReportFailure = err => {
  showToast(ACTIVE_TOAST, "fetchMatchReport", "Unable to fetch match reports");
  return {
    type: FETCH_MATCH_REPORT_FAILURE,
    payload: err
  };
};

/* SUGGESTION articles actions */
export const fetchSuggestion = payload => ({
  type: FETCH_SUGGESTION,
  payload
});

export const fetchSuggestionSuccess = payload => ({
  type: FETCH_SUGGESTION_SUCCESS,
  payload
});

export const fetchSuggestionFailure = err => {
  showToast(ACTIVE_TOAST, "fetchSuggestion", "Unable to fetch suggestion");
  return {
    type: FETCH_SUGGESTION_FAILURE,
    payload: err
  };
};

/* Article-Details actions */
export const fetchArticleDetails = payload => ({
  type: FETCH_ARTICLE_DETAILS,
  payload
});

export const fetchArticleDetailsSuccess = payload => ({
  type: FETCH_ARTICLE_DETAILS_SUCCESS,
  payload
});

export const fetchArticleDetailsFailure = err => {
  showToast(
    ACTIVE_TOAST,
    "fetchArticleDetails",
    "Unable to fetch article details"
  );
  return {
    type: FETCH_ARTICLE_DETAILS_FAILURE,
    payload: err
  };
};

/* CRICLYTICS FEATURED ARTICLES actions */
export const fetchCriclyticsArticles = payload => ({
  type: FETCH_CRICLYTICS_ARTICLES,
  payload
});

export const fetchCriclyticsArticlesSuccess = payload => ({
  type: FETCH_CRICLYTICS_ARTICLES_SUCCESS,
  payload
});

export const fetchCriclyticsArticlesFailure = err => {
  showToast(
    ACTIVE_TOAST,
    "fetchCriclyticsArticles",
    "Unable to fetch criclytics articles"
  );
  return {
    type: FETCH_CRICLYTICS_ARTICLES_FAILURE,
    payload: err
  };
};
