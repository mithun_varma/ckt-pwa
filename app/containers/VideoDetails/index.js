/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/media-has-caption */
/* eslint-disable jsx-a11y/iframe-has-title */
/* eslint-disable react/no-danger */
/* eslint-disable indent */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable no-nested-ternary */
/**
 *
 * VideoDetails
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import isEmpty from "lodash/isEmpty";
import moment from "moment";
import { AUTHOR_DATE_FORMAT } from "utils/constants";

import Header from "../../components-v2/commonComponent/Header";
import Loader from "components/Common/Loader";
import EmptyState from "../../components-v2/commonComponent/EmptyState";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";

import makeSelectVideos from "../Videos/selectors";
import videosReducer from "../Videos/reducer";
import * as videosActions from "../Videos/actions";
import videosSaga from "../Videos/saga";
import ReactGA from "react-ga";
import {
  white,
  gradientRedOrange,
  grey_10,
  grey_6
} from "../../styleSheet/globalStyle/color";
import VideoCard from "../../components-v2/Articles/VideoCard";
import { Helmet } from "react-helmet";
/* eslint-disable react/prefer-stateless-function */
export class VideoDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      videoId: ""
    };
  }

  componentDidMount = () => {
    // ReactGA.initialize("UA-136526387-1");
    // ReactGA.pageview(
    //   "https://www.cricket.com" +
    //     window.location.pathname +
    //     window.location.search
    // );
    this.props.fetchVideoDetails({ id: this.props.match.params.id });
  };

  static getDerivedStateFromProps = (nextprops, state) => {
    if (nextprops.match.params.id !== state.videoId) {
      nextprops.fetchVideoDetails({ id: nextprops.match.params.id });
      return {
        videoId: nextprops.match.params.id
      };
    }
    return null;
  };

  getVideoDetails = id => {
    this.props.fetchVideoDetails({ id });
  };

  render() {
    const { videos, history } = this.props;
    // console.log("Video: ", this.props);
    return (
      <React.Fragment>
        <Helmet titleTemplate="%s | cricket.com">
          <title>{`${videos.videoDetails &&
            videos.videoDetails.title} | ${videos.videoDetails &&
            videos.videoDetails.type}`}</title>
          <meta
            name="description"
            content={videos.videoDetails && videos.videoDetails.description}
          />
          <meta
            name="keywords"
            content={
              (videos.videoDetails && videos.videoDetails.seoKeywords) ||
              "videos"
            }
          />
          <meta itemprop="width" content="595" />
          <meta itemprop="height" content="450" />
          <meta
            itemprop="url"
            content={
              videos.videoDetails &&
              videos.videoDetails.imageData &&
              videos.videoDetails.imageData.thumbnail
                ? videos.videoDetails.imageData.thumbnail
                : ""
            }
          />

          <meta
            property="og:video"
            content={
              (videos.videoDetails.videoData &&
                videos.videoDetails.videoData.url) ||
              ""
            }
          />
          <meta
            property="og:video:url"
            content={
              (videos.videoDetails.videoData &&
                videos.videoDetails.videoData.url) ||
              ""
            }
          />
          <meta
            property="og:video:secure_url"
            content={
              (videos.videoDetails.videoData &&
                videos.videoDetails.videoData.url) ||
              ""
            }
          />
          <meta property="og:video:type" content="video/mp4" />
          <meta
            property="og:video:type"
            content="application/x-shockwave-flash"
          />
          <meta property="og:video:width" content="595" />
          <meta property="og:video:height" content="450" />

          <meta
            property="og:image"
            itemprop="image"
            content={
              videos.videoDetails &&
              videos.videoDetails.imageData &&
              videos.videoDetails.imageData.thumbnail
                ? videos.videoDetails.imageData.thumbnail
                : ""
            }
          />
          <meta property="og:width" content="595" />
          <meta property="og:height" content="450" />
          <meta property="og:image:width" content="595" />
          <meta property="og:image:height" content="450" />
          <meta
            property="og:image:secure_url"
            content={
              videos.videoDetails &&
              videos.videoDetails.imageData &&
              videos.videoDetails.imageData.thumbnail
                ? videos.videoDetails.imageData.thumbnail
                : ""
            }
          />
          <meta
            property="og:title"
            content={
              (videos.videoDetails && videos.videoDetails.title) ||
              "cricket.com video"
            }
          />
          <meta
            property="og:description"
            content={
              (videos.videoDetails && videos.videoDetails.seoDescription) ||
              (videos.videoDetails && videos.videoDetails.description) ||
              "cricket.com video "
            }
          />
          <meta
            property="og:url"
            content={"https://www.cricket.com" + this.props.match.url}
          />
          <meta property="og:type" content="Article" />
          <meta property="fb:app_id" content="631889693979290" />
          <meta property="og:site_name" content="cricket.com" />
          <link
            rel="canonical"
            href={`https://www.cricket.com${
              this.props.history.location.pathname
            }`}
          />
          {/* <link rel="apple-touch-icon" href="http://mysite.com/img/apple-touch-icon-57x57.png" /> */}
        </Helmet>
        <Header
          title={""}
          isHeaderBackground={false}
          leftArrowBack
          textColor={white}
          backgroundColor={"rgba(0,0,0,0)"}
          history={history}
          leftIconOnClick={() => {
            history.goBack();
          }}
        />
        <section>
          <div>
            {videos.videoDetailLoading ? (
              <div
                style={{
                  // width: "90%",
                  // padding:'16px',
                  minHeight: 350,
                  background: "#fff"
                  // margin: "16px"
                }}
              >
                <img
                  src={require("../../images/CricketLoader.gif")}
                  style={{
                    width: "100px",
                    marginLeft: "35%",
                    marginTop: "10vh"
                    // height: 200,
                    // background: "#fff"
                  }}
                />
              </div>
            ) : !isEmpty(videos.videoDetails) ? (
              <div className="videoPlay__wrapper">
                <div className="img-block" style={{ maxHeight: 250 }}>
                  {/* <img src={videos.videoDetails.thumbnail} alt="" /> */}
                  {/* <div className="featured__title">
                    <PlayArrow />
                  </div> */}
                  {videos.videoDetails.videoData &&
                  videos.videoDetails.videoData.source === "custom" ? (
                    <video
                      width="100%"
                      height="100%"
                      controls
                      controlsList="nodownload"
                    >
                      <source
                        src={
                          videos.videoDetails.videoData &&
                          videos.videoDetails.videoData.url
                        }
                        type="video/mp4"
                      />
                      Your browser does not support the video tag.
                    </video>
                  ) : (
                    <iframe
                      width="100%"
                      height="100%"
                      src={
                        videos.videoDetails.videoData &&
                        videos.videoDetails.videoData.url
                      }
                      frameBorder="0"
                      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                      allowFullScreen
                    />
                  )}
                </div>
                <div style={{ padding: 16, background: white, marginTop: -8 }}>
                  <div
                    style={{
                      fontFamily: "mont500",
                      fontSize: 15,
                      color: grey_10,
                      marginBottom: 10
                    }}
                  >
                    {videos.videoDetails.title}
                    {videos.videoDetails.title && (
                      <div
                        style={{
                          display: "flex",
                          marginTop: 8
                        }}
                      >
                        <a
                          href={`https://www.facebook.com/sharer.php?u=https://www.cricket.com${encodeURIComponent(
                            this.props.history.location.pathname
                          )}&amp;text=${
                            videos.videoDetails.title
                          }&amp;via=cricket.com`}
                          target="_blank"
                        >
                          <img
                            src={require("../../images/fb.svg")}
                            alt=""
                            style={{
                              width: 20,
                              height: 20
                            }}
                          />
                        </a>
                        <a
                          href={`https://twitter.com/intent/tweet?url=https://www.cricket.com${encodeURIComponent(
                            this.props.history.location.pathname
                          )}&amp;text=${
                            videos.videoDetails.title
                          }&amp;via=cricket.com`}
                          target="_blank"
                        >
                          <img
                            src={require("../../images/twitter.svg")}
                            alt=""
                            style={{
                              width: 20,
                              height: 20,
                              margin: "0 10px"
                            }}
                          />
                        </a>
                        <a
                          href={`https://wa.me/?text=${
                            videos.videoDetails.title
                          } - https://www.cricket.com${encodeURIComponent(
                            this.props.history.location.pathname
                          )} (Sent via cricket.com)`}
                          target="_blank"
                        >
                          <img
                            src={require("../../images/whatsapp.svg")}
                            alt=""
                            style={{
                              width: 20,
                              height: 20
                            }}
                          />
                        </a>
                      </div>
                    )}
                  </div>
                  <div
                    style={{
                      fontFamily: "mont400",
                      fontSize: 12,
                      color: grey_6,
                      marginBottom: 10
                    }}
                  >
                    {videos.videoDetails.description
                      ? videos.videoDetails.description
                      : "--"}
                  </div>
                  <div
                    style={{
                      fontFamily: "mont500",
                      fontSize: 12,
                      color: grey_10,
                      marginBottom: 10
                    }}
                  >
                    {moment(videos.videoDetails.publishedAt).format(
                      "DD MMM YYYY"
                    )}
                  </div>
                  {/* <div className="videoPlay__desc">
                    <p
                      dangerouslySetInnerHTML={{
                        __html: videos.videoDetails.content
                      }}
                    />
                  </div> */}
                </div>
              </div>
            ) : (
              <EmptyState msg="No video details available" />
            )}
          </div>
          <div>
            {videos.videoDetails.relatedArticles &&
            videos.videoDetails.relatedArticles.length > 0 ? (
              <VideoCard
                loading={videos.videoDetailLoading}
                videos={videos.videoDetails.relatedArticles}
                title="Realted Videos"
                history={history}
              />
            ) : (
              <EmptyState
                rootStyles={{
                  fontFamily: "mont400",
                  fontSize: 12,
                  background: white,
                  margin: 16
                }}
                msg="no related videos found"
              />
            )}
          </div>

          {/* Others Video Block */}
          {/* // ========== */}
          {/* {videos.videoDetails.relatedArticles &&
            videos.videoDetails.relatedArticles.length > 0 && (
              <div className="videos videos--others">
                <div className="photos__title">Related Videos</div>
                <div className="videos__list">
                  {videos.videoDetails.relatedArticles.map((list, key) => (
                    <OtherVideos
                      key={`${key + 1}`}
                      videos={list}
                      history={history}
                      getVideoDetails={this.getVideoDetails}
                    />
                  ))}
                </div>
              </div>
            )} */}
        </section>
      </React.Fragment>
    );
  }
}

VideoDetails.propTypes = {
  fetchVideoDetails: PropTypes.func,
  history: PropTypes.object,
  match: PropTypes.object,
  videos: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  videos: makeSelectVideos()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchVideoDetails: payload =>
      dispatch(videosActions.fetchVideoDetails(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withVideoReducer = injectReducer({
  key: "videos",
  reducer: videosReducer
});

const withVideoSaga = injectSaga({ key: "videos", saga: videosSaga });

export default compose(
  withVideoReducer,
  withVideoSaga,
  withConnect
)(VideoDetails);
