import React, { PureComponent } from "react";
import { connect } from "react-redux";
import Slider from "react-slick";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import injectReducer from "utils/injectReducer";
import injectSaga from "utils/injectSaga";
import Header from "../../components-v2/commonComponent/Header";
import DeathOverSimulator from "../../components-v2/Criclytics/DeathOverSimulator";
import LivePlayerPrediction from "../../components-v2/Criclytics/LivePlayerProjection";
// import PlayerComparision from "../../components-v2/Criclytics/MatchUps";  // Old Matchups
import Matchups from "../../components-v2/Criclytics/MatchUps/Matchups";
import MomentumShift from "../../components-v2/Criclytics/MomentumShift";
import LiveScorePredictor from "../../components-v2/Criclytics/NewLiveScorePredictor";
import PlayerProjection from "../../components-v2/Criclytics/PlayerProjection";
import ScorePredictor from "../../components-v2/Criclytics/ScorePredictor";
import TimeMachine from "../../components-v2/Criclytics/TImeMachine ";
import { grey_4 } from "../../styleSheet/globalStyle/color";
import * as CriclyticsAction from "../Criclytics/actions";
import reducer from "../Criclytics/reducer";
import saga from "../Criclytics/saga";
import makeSelectCriclytics from "../Criclytics/selectors";
import { ErrorBoundary } from "../../components-v2/Error";
import NewDeathOverSimulator from "../../components-v2/Criclytics/NewDeathOverSimulator/NewDeathOverSimulator";
import KeyStats from "../../components-v2/Criclytics/KeyStats";
import TestLiveScore from "../../components-v2/Criclytics/TestLiveScore";

const stumps = require("../../images/groundImageWicket.png");
const loader = require("../../images/CricketLoader.gif");
const notFound = require("../../images/emptyState.svg");

// Seperate the components in two files ,
export const NotFound = props => (
  <div
    style={{
      height: props.imageTop ? "93vh" : "93vh",
      position: "relative",
      background: grey_4,
      margin: props.imageTop ? "0 16px" : "0 16px"
    }}
  >
    <div
      style={{
        position: !props.imageTop ? "absolute" : "", // use static instead of blank "" , helps the css parses calculate the computed declaration value quicker
        top: !props.imageTop ? "50%" : "0%",
        left: !props.imageTop ? "50%" : "0%",
        transform: !props.imageTop ? "translate(-50%, -50%)" : ""
      }}
    >
      {/* We can do one ternary instead of two binaries below , < better readability plus one line less to interpret for v8 */}
      {props.imageTop && (
        <div
          style={{
            paddingTop: "50px"
          }}
        >
          <div
            style={{
              fontFamily: "Montserrat",
              fontWeight: 700,
              fontSize: "20px",
              padding: "10px 25px",
              // textAlign: "center",
              listStyleType: "circle",
              color: "#000"
            }}
          >
            Death Overs Simulator
          </div>
          {/*  add a fallback */}
          {props.message}
          <center>
            <img
              style={{
                height: "210px",
                width: "204px",
                marginTop: "30px"
              }}
              src={stumps}
            />
          </center>
        </div>
      )}
      {!props.imageTop && (
        <div>
          {" "}
          <img
            style={{
              height: "250px",
              width: "250px"
            }}
            src={notFound}
          />
          <div
            style={{
              fontFamily: "Montserrat",
              fontSize: "18px",
              paddingTop: "15px",
              textAlign: "center"
            }}
          >
            {/*  add a fallback */}
            {props.message}
          </div>
        </div>
      )}
    </div>
  </div>
);
const DeathOverInstruction = props => {
  return (
    <React.Fragment>
      <NotFound
        imageTop
        message={
          <ul
            style={{
              listStyleType: "disc",
              lineHeight: "21px",
              opacity: 0.6,
              padding: "8px 40px"
            }}
          >
            <li
              style={{
                fontFamily: "Montserrat",
                fontWeight: 500,
                fontSize: "12px",
                listStyleType: "disc",
                color: "#141b2f"
              }}
            >
              This will only get activated only for the last five overs of
              innings
            </li>
            <li
              style={{
                fontFamily: "Montserrat",
                fontWeight: 500,
                fontSize: "12px",
                listStyleType: "disc",
                color: "#141b2f",
                marginTop: 14
              }}
            >
              Do not forget to come back and simulate your options of who could
              bowl the crucial death bowlers
            </li>
          </ul>
        }
      />
    </React.Fragment>
  );
};
export class CriclyticsSlider extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      initial: true,
      currentMatch: null,
      loader: true,
      fallback: false
    };
  }

  componentDidMount() {
    const matchId = this.props.match.params.matchId;
    this.props.fetchScoreCard(matchId);
    this.props.getKeyStats(matchId);
    // this.props.fetchFirstBattingProbables(matchId);
    this.props.fetchMomentumShift(matchId);
    this.props.fetchDeathBowlers(matchId);

    this.props.fetchMatchUps(matchId);

    this.props.fetchDeathOverForMomentum(matchId);
    this.props.fetchLiveBattingProbables(matchId);

    this.props.fetchLiveMatchProjections(matchId);
    this.props.fetchPreMatchStats(matchId);
    this.props.fetchLiveBowlers(matchId);
    const currentMatch =
      this.props.criclytics.matchMiniScoreCards &&
      this.props.criclytics.matchMiniScoreCards.fullScorecard &&
      this.props.criclytics.matchMiniScoreCards.fullScorecard
        .matchMiniScorecard;
    this.props.fetchTimeMachine({
      matchId: matchId,
      inningNo: currentMatch && currentMatch.inningOrder.length <= 1 ? 0 : 1,
      overNo: currentMatch && currentMatch.inningOrderLength <= 1 ? 0 : 1
    });
    this.props.fetchScreensForCriclytics(this.props.match.params.matchId);
    this.setState({
      fallback: false
    });
  }

  static getDerivedStateFromProps = (nextprops, state) => {
    // console.log(nextprops);
    if (
      state.initial &&
      nextprops.criclytics.matchMiniScoreCards &&
      nextprops.criclytics.matchMiniScoreCards.fullScorecard &&
      nextprops.criclytics.matchMiniScoreCards.fullScorecard
        .matchMiniScorecard &&
      nextprops.criclytics.matchMiniScoreCards.fullScorecard.matchMiniScorecard
        .matchId === nextprops.match.params.matchId
    ) {
      const currentMatch =
        nextprops.criclytics.matchMiniScoreCards.fullScorecard
          .matchMiniScorecard;
      nextprops.fetchTimeMachine({
        matchId: nextprops.match.params.matchId,
        inningNo: currentMatch && currentMatch.inningOrder.length <= 1 ? 0 : 1,
        overNo: currentMatch && currentMatch.inningOrderLength <= 1 ? 0 : 1
      });
      return {
        ...state,
        currentMatch:
          nextprops.criclytics.matchMiniScoreCards.fullScorecard
            .matchMiniScorecard,
        initial: false
      };
    }
  };

  componentWillUnmount() {
    this.props.clearStore();
  }

  callDeathBowlers = () => {
    // this.props.history.go(
    //   `/criclytics-slider/${this.props.match.params.matchId}`
    // );
    this.props.getDeathBowlersUpdate(this.props.match.params.matchId);
    this.props.fetchScoreCard(this.props.match.params.matchId);
    // this.props.resetdeathOverProjectedStore();
  };

  render() {
    const matchID = this.props.match.params.matchId;
    const { criclytics, history } = this.props;
    // const { currentMatch } = this.state;
    const currentMatch =
      (this.props.criclytics.matchMiniScoreCards &&
        this.props.criclytics.matchMiniScoreCards.fullScorecard &&
        this.props.criclytics.matchMiniScoreCards.fullScorecard
          .matchMiniScorecard) ||
      this.state.currentMatch;
    let settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      appendDots: dots => <ul>{dots}</ul>,
      arrows: false,
      draggable: true,
      // adaptiveHeight: true,
      dotsClass: "slick-dots slick-criclytics"
    };

    return (
      <React.Fragment>
        {currentMatch && (
          <div
            style={{
              background: "linear-gradient(176deg, #0b1533, #050b1b)",
              minHeight: "100vh"
            }}
          >
            <div style={{ padding: "16px 16px" }}>
              <Header
                // isShare
                backgroundColor="#0b1533"
                textColor="#fff"
                isHeaderBackground={true}
                title={currentMatch && currentMatch.matchShortName}
                leftArrowBack
                leftIconOnClick={() => {
                  history.goBack();
                }}
                history={history}
              />
            </div>

            <Slider {...settings}>
              {criclytics.criclyticsScreenLoading &&
                criclytics.criclyticsScreen !== null &&
                criclytics.criclyticsScreen.length <= 0 && (
                  <div>
                    <div
                      style={{
                        padding: "16px 16px"
                      }}
                    >
                      <div
                        style={{
                          background: grey_4
                        }}
                      >
                        <Loader message="Loading data for you" />
                      </div>
                    </div>
                  </div>
                )}
              {criclytics.criclyticsScreen.length === 0 && (
                <div>
                  <div
                    style={{
                      padding: "16px 16px"
                    }}
                  >
                    <div
                      style={{
                        background: grey_4
                      }}
                    >
                      <NotFound message="No Criclytics found for the match" />
                    </div>
                  </div>
                </div>
              )}

              {criclytics.criclyticsScreen === null && (
                <div>
                  <div
                    style={{
                      padding: "16px 16px"
                    }}
                  >
                    <div
                      style={{
                        background: grey_4
                      }}
                    >
                      <NotFound message="No Criclytics found for the match" />
                    </div>
                  </div>
                </div>
              )}
              {criclytics.criclyticsScreen &&
                criclytics.criclyticsScreen.length > 0 &&
                criclytics.criclyticsScreen.map((ele, key) => {
                  if (ele === "KeyStats")
                    return (
                      <div key={key}>
                        <div
                          style={{
                            padding: "16px 16px"
                          }}
                        >
                          <div
                            style={{
                              background: grey_4
                            }}
                          >
                            {criclytics.keyStats &&
                            (criclytics.keyStats.headToHead ||
                              criclytics.keyStats.topRunScorerData ||
                              criclytics.keyStats.topWicketTakerData) ? (
                              <ErrorBoundary>
                                <KeyStats
                                  data={criclytics.keyStats}
                                  format={
                                    currentMatch &&
                                    currentMatch.format == "TEST"
                                      ? "TEST"
                                      : "ODI"
                                  }
                                  // data={mockKeyStats}
                                  // keyStatsLoading={criclytics.keyStatsLoading}
                                  // currentMatch={currentMatch}
                                />
                              </ErrorBoundary>
                            ) : (
                              <NotFound message="No Key Stats Found" />
                            )}
                          </div>
                        </div>
                      </div>
                    );
                  if (ele === "Match ups")
                    return (
                      <div key={key}>
                        <div
                          style={{
                            padding: "16px 16px"
                          }}
                        >
                          <div
                            style={{
                              background: grey_4
                            }}
                          >
                            {criclytics.matchUps ? (
                              <ErrorBoundary>
                                {/* <PlayerComparision
                                currentMatch={currentMatch}
                                data={criclytics.matchUps}
                              /> */}
                                <Matchups
                                  currentMatch={currentMatch}
                                  data={criclytics.matchUps}
                                  getMatchUpsForPlayerOne={
                                    this.props.getMatchUpsForPlayerOne
                                  }
                                  getMatchUpsForSelectedPlayer={
                                    this.props.getMatchUpsForSelectedPlayer
                                  }
                                  matchUpsPlayerOneData={
                                    criclytics.matchUpsPlayerOneData
                                  }
                                  matchUpsForSelectedPlayer={
                                    criclytics.matchUpsForSelectedPlayer
                                  }
                                  matchId={matchID}
                                />
                              </ErrorBoundary>
                            ) : (
                              <NotFound message="No Player Comparision Found" />
                            )}
                          </div>
                        </div>
                      </div>
                    );
                  // if (ele === "First batting score projector")
                  //   return (
                  //     <div key={key}>
                  //       <div
                  //         style={{
                  //           padding: "16px 16px"
                  //         }}
                  //       >
                  //         <div
                  //           style={{
                  //             background: grey_4
                  //           }}
                  //         >
                  //           <ScorePredictor
                  //             currentMatch={currentMatch}
                  //             data={criclytics.firstBattingTeamProbability}
                  //           />
                  //         </div>
                  //       </div>
                  //     </div>
                  //   );
                  if (ele === "Time Machine")
                    return (
                      <div key={key}>
                        <div
                          style={{
                            padding: "16px 16px"
                          }}
                        >
                          <div
                            style={{
                              background: grey_4,
                              height: "100vh"
                            }}
                          >
                            {!criclytics.timeMachineLoading &&
                            criclytics.timeMachine ? (
                              <ErrorBoundary>
                                <TimeMachine
                                  history={history}
                                  currentMatch={currentMatch}
                                  data={criclytics.timeMachine}
                                  matchId={matchID}
                                />
                              </ErrorBoundary>
                            ) : (
                              <NotFound message="No Time Machine Found" />
                            )}
                          </div>
                        </div>
                      </div>
                    );
                  if (ele === "Death Over Instruction")
                    return <DeathOverInstruction />;
                  if (ele === "Death Over Simulator")
                    return (
                      <div key={key}>
                        <div
                          style={{
                            padding: "16px 16px"
                          }}
                        >
                          <div
                            style={{
                              background: grey_4
                            }}
                          >
                            {criclytics.deathBowlers !== undefined ? (
                              <NewDeathOverSimulator
                                postData={criclytics.deathOverProjected}
                                currentMatch={currentMatch}
                                playerStats={
                                  (this.props.criclytics.matchMiniScoreCards &&
                                    this.props.criclytics.matchMiniScoreCards
                                      .playerStatsViews) ||
                                  {}
                                }
                                postDeathBowlers={
                                  this.props.postDeathOversBowlers
                                }
                                data={criclytics.deathBowlers}
                                callDeathBowlers={this.callDeathBowlers}
                                getFullScoreDetails={this.props.fetchScoreCard}
                                matchId={matchID}
                              />
                            ) : (
                              // <DeathOverSimulator
                              //   postData={criclytics.deathOverProjected}
                              //   currentMatch={currentMatch}
                              //   postDeathBowlers={
                              //     this.props.postDeathOversBowlers
                              //   }
                              //   data={criclytics.deathBowlers}
                              // />
                              <NotFound message="No Death Over Prediction Found" />
                            )}
                          </div>
                        </div>
                      </div>
                    );
                  if (ele === "Player Projections")
                    return (
                      <div key={key}>
                        <div
                          style={{
                            padding: "16px 16px"
                          }}
                        >
                          <div
                            style={{
                              background: grey_4
                            }}
                          >
                            {criclytics.preMatchPlayerProjections ? (
                              <ErrorBoundary>
                                <PlayerProjection
                                  currentMatch={currentMatch}
                                  loading={
                                    criclytics.preMatchPlayerProjectionsLoading
                                  }
                                  data={criclytics.preMatchPlayerProjections}
                                />
                              </ErrorBoundary>
                            ) : (
                              <NotFound message="No Player Projection Found" />
                            )}
                          </div>
                        </div>
                      </div>
                    );
                  if (ele === "Momentum Shift")
                    return (
                      <div key={key}>
                        <div
                          style={{
                            padding: "16px 16px"
                          }}
                        >
                          <div
                            style={{
                              background: grey_4
                            }}
                          >
                            {criclytics.momentumShift ? (
                              <ErrorBoundary>
                                <MomentumShift
                                  ballByBallData={criclytics.ballByBall}
                                  ballByBall={this.props.fetchBallByBall}
                                  fetchOverSeperator={
                                    this.props.fetchDeathOverForMomentum
                                  }
                                  OverData={criclytics.overSeparatorMomentum}
                                  currentMatch={currentMatch}
                                  matchId={matchID}
                                  loading={criclytics.momentumShiftLoading}
                                  data={criclytics.momentumShift}
                                />
                              </ErrorBoundary>
                            ) : (
                              <NotFound message="No Momentum Shift Found" />
                            )}
                          </div>
                        </div>
                      </div>
                    );
                  if (ele === "Live Score Predictor")
                    return (
                      <div key={ele} style={{ background: grey_4 }}>
                        {criclytics.liveMatchPredictionLoading ? (
                          <div
                            style={{
                              padding: "16px 16px"
                            }}
                          >
                            <div
                              style={{
                                background: grey_4
                              }}
                            >
                              <Loader message="Loading data for you" />
                            </div>
                          </div>
                        ) : criclytics.liveMatchPrediction &&
                        criclytics.liveMatchPrediction.liveScores &&
                        criclytics.liveMatchPrediction.liveScores.length > 0 ? (
                          <ErrorBoundary>
                            {currentMatch && currentMatch.format === "TEST" ? (
                              <TestLiveScore
                                currentMatch={currentMatch}
                                data={
                                  criclytics.liveMatchPrediction &&
                                  criclytics.liveMatchPrediction
                                }
                              />
                            ) : (
                              <LiveScorePredictor
                                currentMatch={currentMatch}
                                data={
                                  criclytics.liveMatchPrediction &&
                                  criclytics.liveMatchPrediction
                                }
                              />
                            )}
                          </ErrorBoundary>
                        ) : (
                          <NotFound message="No Live Score Prediction Found" />
                        )}
                      </div>
                    );
                  if (ele === "Live player projections")
                    return (
                      <div key={key}>
                        <div
                          style={{
                            padding: "16px 16px"
                          }}
                        >
                          <div
                            style={{
                              background: grey_4
                            }}
                          >
                            {criclytics.livePlayerProjections ? (
                              <ErrorBoundary>
                                <LivePlayerPrediction
                                  currentMatch={currentMatch}
                                  data={criclytics.livePlayerProjections}
                                  bowlingData={criclytics.liveBowlerPrediction}
                                  loading={
                                    criclytics.livePlayerProjectionsLoading
                                  }
                                />
                              </ErrorBoundary>
                            ) : (
                              <NotFound message="No Live Player Projection Found" />
                            )}
                          </div>
                        </div>
                      </div>
                    );
                })}
            </Slider>
          </div>
        )}
      </React.Fragment>
    );
  }
}

const Loader = props => (
  <div
    style={{
      height: "100vh",
      position: "relative"
    }}
  >
    <div
      style={{
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)"
      }}
    >
      <img
        style={{
          height: "250px",
          width: "250px"
        }}
        src={loader}
      />
      <div
        style={{
          fontFamily: "Montserrat",
          fontSize: "18px",
          paddingTop: "15px",
          textAlign: "center"
        }}
      >
        {props.message}
      </div>
    </div>
  </div>
);

const mapStateToProps = createStructuredSelector({
  criclytics: makeSelectCriclytics()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchMatchUps: payload => dispatch(CriclyticsAction.getMatchUps(payload)),
    fetchMatches: () => dispatch(CriclyticsAction.getMatchesForCriclytics()),
    fetchMomentumShift: payload =>
      dispatch(CriclyticsAction.getMomentumShift(payload)),
    fetchLiveBattingProbables: payload =>
      dispatch(CriclyticsAction.getLivePlayerProjections(payload)),

    fetchPreMatchStats: payload =>
      dispatch(CriclyticsAction.getPreMatchProbability(payload)),
    fetchDeathBowlers: payload =>
      dispatch(CriclyticsAction.getDeathOverBowlers(payload)),
    postDeathOversBowlers: payload =>
      dispatch(CriclyticsAction.postDeathOversBowler(payload)),

    fetchTimeMachine: payload =>
      dispatch(CriclyticsAction.getTimeMachine(payload)),
    fetchLiveMatchProjections: payload =>
      dispatch(CriclyticsAction.getLiveMatchPrediction(payload)),
    fetchLiveBowlers: payload =>
      dispatch(CriclyticsAction.getLiveBowlerPrediction(payload)),
    fetchLivePlayers: payload =>
      dispatch(CriclyticsAction.getLiveMatchPlayers(payload)),
    fetchPreMatchPlayers: payload =>
      dispatch(CriclyticsAction.getPreMatchPlayers(payload)),
    fetchScreensForCriclytics: payload =>
      dispatch(CriclyticsAction.getScreensForCriclytics(payload)),

    fetchDeathOverForMomentum: payload =>
      dispatch(CriclyticsAction.getMomentumShiftOverSeparator(payload)),
    fetchBallByBall: payload =>
      dispatch(CriclyticsAction.getBallByBall(payload)),
    fetchScoreCard: payload =>
      dispatch(CriclyticsAction.getScoreCardForCriclytics(payload)),
    clearStore: payload => dispatch(CriclyticsAction.clearStore()),
    resetdeathOverProjectedStore: payload =>
      dispatch(CriclyticsAction.resetdeathOverProjectedStore()),
    fetchFirstBattingProbables: payload =>
      dispatch(CriclyticsAction.getFirstBattingProbability(payload)),

    getMatchUpsForPlayerOne: payload =>
      dispatch(CriclyticsAction.getMatchUpsForPlayerOne(payload)),
    getMatchUpsForSelectedPlayer: payload =>
      dispatch(CriclyticsAction.getMatchUpsForSelectedPlayer(payload)),

    getKeyStats: payload => dispatch(CriclyticsAction.getKeyStats(payload)),

    getDeathBowlersUpdate: payload =>
      dispatch(CriclyticsAction.getDeathBowlersUpdate(payload))
  };
}

const styles = {
  wrapper: {}
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "criclytics", reducer });
const withSaga = injectSaga({ key: "criclytics", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(CriclyticsSlider);

const mockKeyStats = {
  headToHead: {
    head2Head: {
      matches: "3",
      teamA: "2",
      teamB: "1",
      other: "0"
    },
    venueStats: {
      firstBattingWinPercent: null,
      highestScoreChased: null,
      avgFirstInningScore: null,
      paceWicketPercent: null,
      spinWicketPercent: null
    },
    teamA: {
      key: "cdc-15-england",
      teamName: "England",
      teamShortName: "ENG",
      lastFStatus: ["W", "NR", "W", "L", "W"]
    },
    teamB: {
      key: "cdc-42-south-africa",
      teamName: "South Africa",
      teamShortName: "SA",
      lastFStatus: ["W", "W", "W", "W", "W"]
    }
  },
  topRunScorerData: [
    {
      name: null,
      avatar: null,
      teamkey: null,
      runs: 0,
      average: null
    },
    {
      name: null,
      avatar: null,
      teamkey: null,
      runs: 0,
      average: null
    }
  ],
  topWicketTakerData: [
    {
      name: null,
      avatar: null,
      teamkey: null,
      average: null,
      wickets: 0
    },
    {
      name: null,
      avatar: null,
      teamkey: null,
      average: null,
      wickets: 0
    }
  ]
};
