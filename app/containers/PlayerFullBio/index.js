/**
 *
 * PlayerFullBio
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";

import makeSelectPlayers from "../Players/selectors";
import playersReducer from "../Players/reducer";
import * as playersActions from "../Players/actions";

import playersSaga from "../Players/saga";
import Header from "../../components-v2/commonComponent/Header";
import {
  black,
  white,
  gradientGrey,
  grey_8
} from "../../styleSheet/globalStyle/color";
import HrLine from "../../components-v2/commonComponent/HrLine";
import isEmpty from "lodash/isEmpty";
/* eslint-disable react/prefer-stateless-function */
export class PlayerFullBio extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      isHeaderBackground: false,
      titlePlaced: false
    };
    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
    if (isEmpty(this.props.players.playerDetails))
      this.props.fetchPlayerDetails({
        playerId: this.props.match.params.playerId
      });
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = event => {
    const scrollTop = event.target.scrollingElement.scrollTop;
    // console.log("object: ", scrollTop);
    if (scrollTop > 26 && !this.state.titlePlaced) {
      this.setState({
        // title: "Records",
        // title: this.props.player.name,
        title: this.props.players.playerDetails[this.props.match.params.playerId].name,
        titlePlaced: true,
        isHeaderBackground: true
      });
    } else if (scrollTop < 27) {
      this.setState({
        title: "",
        titlePlaced: false,
        isHeaderBackground: false
      });
    }
  };

  render() {
    const { players, history } = this.props;
    const playerDetails =
      players.playerDetails[this.props.match.params.playerId];
    if (playerDetails) {
      return (
        <div
          style={{
            padding: "54px 0px",
            minHeight: "120vh"
          }}
        >
          <Header
            title={this.state.title}
            isHeaderBackground={this.state.isHeaderBackground}
            leftArrowBack
            textColor={black}
            backgroundColor={white}
            history={history}
            leftIconOnClick={() => {
              history.goBack();
            }}
          />
          <div
            style={{
              fontFamily: "Montserrat",
              fontWeight: 700,
              fontSize: 22,
              color: black,
              // marginBottom: 12,
              // minHeight: "90px",
              paddingLeft: "16px",
              textTransform: "capitalize"
              // background: grey_8
            }}
          >
            {/* {title} */}
            player bio
          </div>
          <div
            style={{
              fontFamily: "Montserrat",
              fontSize: "12px",
              margin: "16px",
              background: white
            }}
          >
            <div
              style={{
                background: gradientGrey,
                padding: "14px",
                fontSize: "14px",
                fontWeight: "500"
              }}
            >
              {playerDetails.displayName}
            </div>
            <HrLine />
            <div style={{ padding: "12px 24px 12px 14px", color: grey_8 }}>
              {playerDetails.about ? playerDetails.about : "--"}
            </div>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}

PlayerFullBio.propTypes = {
  players: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  players: makeSelectPlayers()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchPlayerDetails: payload =>
      dispatch(playersActions.fetchPlayerDetails(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withPlayersReducer = injectReducer({
  key: "players",
  reducer: playersReducer
});
const withPlayersSaga = injectSaga({ key: "players", saga: playersSaga });

export default compose(
  withConnect,
  withPlayersReducer,
  withPlayersSaga
)(PlayerFullBio);
