/*
 *
 * Schedule actions
 *
 */
import { showToast } from "utils/misc";
import * as scheduleConstants from "./constants";

const ACTIVE_TOAST = {};

/* SCHEDULE MATCHES actions */
export const fetchScheduleMatches = payload => ({
  type: scheduleConstants.FETCH_SCHEDULE_MATCHES,
  payload
});

export const fetchScheduleMatchesSuccess = payload => ({
  type: scheduleConstants.FETCH_SCHEDULE_MATCHES_SUCCESS,
  payload
});

export const fetchScheduleMatchesFailure = err => {
  showToast(
    ACTIVE_TOAST,
    "fetchScheduleMatches",
    "Unable to fetch scheduled matches"
  );
  return {
    type: scheduleConstants.FETCH_SCHEDULE_MATCHES_FAILURE,
    payload: err
  };
};

export const fetchScheduleBySeries = payload => ({
  type: scheduleConstants.FETCH_SCHEDULE_BY_SERIES,
  payload
});

export const fetchScheduleBySeriesSuccess = payload => ({
  type: scheduleConstants.FETCH_SCHEDULE_BY_SERIES_SUCCESS,
  payload
});

export const fetchScheduleBySeriesFailure = err => {
  showToast(
    ACTIVE_TOAST,
    "fetchScheduleMatches",
    "Unable to fetch scheduled matches"
  );
  return {
    type: scheduleConstants.FETCH_SCHEDULE_BY_SERIES_FAILURE,
    payload: err
  };
};
