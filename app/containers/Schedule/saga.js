import { call, put, takeLatest } from "redux-saga/effects";

import {
  API_GET_SCHEDULE_MATCHES,
  API_GET_SERIES_MATCHES_BY_STATUS
} from "utils/requestUrls";
import request from "utils/request";

import * as scheduleConstants from "./constants";
import * as scheduleActions from "./actions";

export function* getScheduleMatches(payload) {
  const config = {
    ...API_GET_SCHEDULE_MATCHES,
    params: {
      ...API_GET_SCHEDULE_MATCHES.params,
      ...payload.payload
    }
  };
  //
  try {
    const options = yield call(request, config);
    yield put(
      scheduleActions.fetchScheduleMatchesSuccess({
        data: options.data.data,
        skip: payload.payload.skip
      })
    );
  } catch (err) {
    yield put(scheduleActions.fetchScheduleMatchesFailure(err));
  }
}

export function* getScheduleBySeries(payload) {
  const config = API_GET_SERIES_MATCHES_BY_STATUS(payload);
  //
  try {
    const options = yield call(request, config);
    yield put(scheduleActions.fetchScheduleBySeriesSuccess(options.data.data));
  } catch (err) {
    yield put(scheduleActions.fetchScheduleBySeriesFailure(err));
  }
}

export default function* defaultSaga() {
  yield takeLatest(
    scheduleConstants.FETCH_SCHEDULE_MATCHES,
    getScheduleMatches
  );
  yield takeLatest(
    scheduleConstants.FETCH_SCHEDULE_BY_SERIES,
    getScheduleBySeries
  );
}
