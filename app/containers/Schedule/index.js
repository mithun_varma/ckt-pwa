/* eslint-disable global-require */
/**
 *
 * Schedule
 *
 */
/* eslint-disable no-nested-ternary */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint-disable dot-notation */
/* eslint camelcase: 0 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { Helmet } from "react-helmet";
// import sortBy from 'lodash/sortBy';
// import Loader from "components/Common/Loader";
import Loader from "components-v2/commonComponent/Loader";
import EmptyState from "components/Common/EmptyState";

import ButtonBar from "components-v2/commonComponent/ButtonBar";
import BottomBar from "components-v2/commonComponent/BottomBar";
import TabBar from "components-v2/commonComponent/TabBar";
import HrLine from "components-v2/commonComponent/HrLine";
import InfiniteScroll from "react-infinite-scroll-component";
import { grey_10, white } from "../../styleSheet/globalStyle/color";
import CardGradientTitle from "components-v2/commonComponent/CardGradientTitle";
// import ScheduleCard from "components-v2/Schedule/ScheduleCard";
import ScheduleCard from "components-v2/Schedule/ScheduleCardNew";
// import Sticky from 'react-stickynode';

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectSchedule from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import * as scheduleActions from "./actions";

// const TAGS_CONFIG = [
//   {
//     label: "All",
//     key: "all",
//     filters: []
//   },
//   {
//     label: "International",
//     key: "international",
//     filters: ["international"]
//   },
//   {
//     label: "Domestic",
//     key: "domestic",
//     filters: ["domestic"]
//   },
//   {
//     label: "ODI",
//     key: "odi",
//     filters: ["odi"]
//   },
//   {
//     label: "T20",
//     key: "t20",
//     filters: ["t20"]
//   },
//   {
//     label: "Leagues",
//     key: "leagues",
//     filters: ["leagues"]
//   },
//   {
//     label: "Womens",
//     key: "womens",
//     filters: ["womens"]
//   }
// ];

const TABS_CONFIG = {
  all: "ALL",
  international: "INTERNATIONAL",
  domestic: "DOMESTIC"
  // odi: ["odi"],
  // t20: ["t20"],
  // leagues: ["leagues"],
  // women: ["women"]
};

const MATCH_FORMAT = [
  "all",
  "international",
  "domestic"
  // "odi",
  // "leagues",
  // "women",
  // "t20",
  // "test"
];
const MATCH_STATUS = ["UPCOMING", "RUNNING", "COMPLETED"];
let lastTab = MATCH_STATUS[0];
let lastScroll = 0;
/* eslint-disable react/prefer-stateless-function */
export class Schedule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      matchStatus: "UPCOMING",
      lastScrol: 0,
      matchType: "all",
      matchTypeFilter: [],
      initial: true,
      activeTab: "all",
      activeButton: "UPCOMING"
    };
  }

  // handleMatchSelect = (val, type, filter) => {
  //   const itemType = type || "matchStatus";
  //   if (this.state[itemType] !== val) {
  //     const config = {};
  //     if (itemType === "matchStatus") {
  //       config.matchStatus = val;
  //       config.matchType = this.state.matchType;
  //       if (this.state.matchTypeFilter.length > 0) {
  //         config.matchTypeFilter = this.state.matchTypeFilter;
  //       }
  //       // config.matchTypeFilter = this.state.matchTypeFilter;
  //       // config.matchTypeFilter = TABS_CONFIG[val];
  //     } else {
  //       config.matchType = val;
  //       if (filter && filter.length > 0) {
  //         config.matchTypeFilter = filter;
  //       } else {
  //         config.matchTypeFilter = [];
  //       }
  //     }
  //     this.setState(
  //       {
  //         ...config,
  //         initial: true
  //       },
  //       () => {
  //         if (this.state.matchTypeFilter.length > 0) {
  //           this.props.fetchScheduleMatches({
  //             status: this.state.matchStatus,
  //             tags: this.state.matchTypeFilter,
  //             skip: 0,
  //             limit: 10,
  //             isTabChanged: true
  //           });
  //         } else {
  //           this.props.fetchScheduleMatches({
  //             status: this.state.matchStatus,
  //             skip: 0,
  //             limit: 10,
  //             isTabChanged: true
  //           });
  //         }
  //       }
  //     );
  //   }
  // };

  componentDidMount = () => {
    if (lastTab !== "UPCOMING") {
      this.handleActiveButton(lastTab);
    } else {
      this.props.fetchScheduleMatches({
        status: this.state.matchStatus,
        tag: TABS_CONFIG[this.state.activeTab],
        skip: 0,
        limit: 10
      });
    }
    this.setState({
      lastScrol: lastScroll
    });
  };

  UNSAFE_componentWillReceiveProps = nextProps => {
    if (this.state.initial) {
      this.setState({
        initial: false
      });
    }
  };

  loadMoreItems = () => {
    // console.log("called");
    if (this.state.activeTab !== "all") {
      this.props.fetchScheduleMatches({
        status: this.state.activeButton,
        tag: TABS_CONFIG[this.state.activeTab],
        limit: 10,
        skip:
          this.props.schedule.scheduleMatchesOldLength === 0
            ? 10
            : this.props.schedule.scheduleMatchesOldLength
      });
    } else {
      this.props.fetchScheduleMatches({
        status: this.state.activeButton,
        tag: TABS_CONFIG[this.state.activeTab],
        limit: 10,
        skip:
          this.props.schedule.scheduleMatchesOldLength === 0
            ? 10
            : this.props.schedule.scheduleMatchesOldLength
      });
    }
  };

  // ====================
  handleActiveButton = activeButton => {
    this.setState(
      {
        activeButton
      },
      () => {
        lastTab = activeButton;
        this.props.fetchScheduleMatches({
          status: this.state.activeButton,
          tag: TABS_CONFIG[this.state.activeTab],
          skip: 0,
          limit: 10
        });
      }
    );
  };

  handleActiveTab = activeTab => {
    this.setState(
      {
        activeTab
      },
      () => {
        this.props.fetchScheduleMatches({
          status: this.state.activeButton,
          tag: TABS_CONFIG[this.state.activeTab],
          skip: 0,
          limit: 10
        });
      }
    );
  };

  handleCardClick = (score, Id) => {
    lastScroll = document.getElementById(Id).offsetTop - 200;
    this.props.history.push(`/score/${score.matchId}`);
  };

  handleSeriesClick = (id, name) => {
    // console.log(id,name)
    this.props.history.push(
      `/series/${name ? name.toLowerCase().replace(/[ /]/g, "-") : ""}/${id}`
    );
  };
  // ====================
  render() {
    const { schedule, history } = this.props;
    // const scheduleMatches = schedule.scheduleMatches;
    return (
      <div>
        <Helmet titleTemplate="%s | cricket.com">
          <title>IPL Schedule | IPL Fixtures | Cricket Schedule</title>
          <meta
            name="description"
            content="Get all the cricket fixtures for international and domestic matches. Check all the latest and upcoming cricket match schedule at cricket.com."
          />
          <meta
            name="keywords"
            content="ipl schedule, ipl fixtures, cricket schedule, cricket information, india cricket schedule"
          />
          <link
            rel="canonical"
            href={`www.cricket.com${history &&
              history.location &&
              history.location.pathname}`}
          />
        </Helmet>
        <div
          style={{
            padding: "16px",
            paddingBottom: 70
          }}
        >
          {/* Title */}
          <div
            style={{
              color: grey_10,
              fontFamily: "Montserrat",
              fontWeight: 700,
              fontSize: 22,
              marginBottom: 8
            }}
          >
            Schedule
          </div>

          {/* Schedule body Card */}
          <div
            style={{
              background: white,
              padding: "10px 0 0",
              borderRadius: 3
              // boxShadow: cardShadow
            }}
          >
            <ButtonBar
              items={MATCH_STATUS}
              activeItem={this.state.activeButton}
              onClick={this.handleActiveButton}
              // onClick={this.handleMatchSelect}
            />

            <div
              style={{
                marginTop: 4
              }}
            >
              <TabBar
                items={MATCH_FORMAT}
                activeItem={this.state.activeTab}
                onClick={this.handleActiveTab}
              />
            </div>
            {/* <HrLine /> */}
            {/* Schedule Card */}
            <div>
              {/* Match Title */}
              {schedule.scheduleMatchesLoading &&
              schedule.scheduleMatches.length === 0 ? (
                <div
                  style={{
                    padding: "2px 8px"
                  }}
                >
                  <Loader styles={{ height: "150px" }} noOfLoaders={4} />
                </div>
              ) : schedule.scheduleMatches &&
              schedule.scheduleMatches.length > 0 ? (
                <InfiniteScroll
                  dataLength={schedule.scheduleMatches.length}
                  next={this.loadMoreItems}
                  initialScrollY={this.state.lastScrol}
                  loader={
                    <img
                      src={require("../../images/CricketLoader.gif")}
                      style={{
                        width: "50px",
                        // height: "50px",
                        marginLeft: "45%"
                      }}
                    />
                  }
                  hasMore={schedule.scheduleMatches.length % 10 === 0}
                  height={600}
                >
                  {this.state.activeButton === "UPCOMING"
                    ? schedule.scheduleMatches
                        .filter(m => {
                          return !(m && m.status === "CANCELLED");
                        })
                        .map(
                          (match, index) =>
                            match && (
                              <div key={index}>
                                {(index === 0 ||
                                  (index > 0 &&
                                    schedule.scheduleMatches[index - 1] &&
                                    match.seriesName !==
                                      schedule.scheduleMatches[index - 1]
                                        .seriesName)) &&
                                  (match.seriesId && (
                                    <div>
                                      {index != 0 && (
                                        <div
                                          style={{
                                            height: 10,
                                            background: "#edeff4"
                                          }}
                                        />
                                      )}
                                      <CardGradientTitle
                                        // title="India’s Tour of Australia 2018"
                                        title={match.seriesName || ""}
                                        name={match.seriesName}
                                        id={match.seriesId}
                                        handleCardClick={() =>
                                          this.handleSeriesClick(
                                            match.seriesId,
                                            match.seriesName
                                          )
                                        }
                                        history={history}
                                        isRightIcon
                                      />
                                    </div>
                                  ))}
                                {/* Match Body */}
                                {match.matchId && (
                                  <ScheduleCard
                                    handleCardClick={this.handleCardClick}
                                    history={history}
                                    score={match}
                                  />
                                )}
                                <HrLine />
                              </div>
                            )
                        )
                    : schedule.scheduleMatches.map(
                        (match, index) =>
                          match && (
                            <div key={index}>
                              {(index === 0 ||
                                (index > 0 &&
                                  schedule.scheduleMatches[index - 1] &&
                                  match.seriesName !==
                                    schedule.scheduleMatches[index - 1]
                                      .seriesName)) &&
                                (match.seriesId && (
                                  <div>
                                    {index != 0 && (
                                      <div
                                        style={{
                                          height: 10,
                                          background: "#edeff4"
                                        }}
                                      />
                                    )}
                                    <CardGradientTitle
                                      // title="India’s Tour of Australia 2018"
                                      title={match.seriesName || ""}
                                      name={match.seriesName}
                                      id={match.seriesId}
                                      handleCardClick={() =>
                                        this.handleSeriesClick(
                                          match.seriesId,
                                          match.seriesName
                                        )
                                      }
                                      history={history}
                                      isRightIcon
                                    />
                                  </div>
                                ))}
                              {/* Match Body */}
                              {match.matchId && (
                                <div id={`match_${match.matchId}`}>
                                  <ScheduleCard
                                    handleCardClick={this.handleCardClick}
                                    history={history}
                                    score={match}
                                  />
                                </div>
                              )}
                              <HrLine />
                            </div>
                          )
                      )}
                </InfiniteScroll>
              ) : (
                <EmptyState msg={`No matches found`} />
              )}
            </div>
            {/* Schedule Card closing */}
          </div>
          {/* Schedule Score Card */}
        </div>
        <BottomBar
          {...this.props.history}
          tabs={[
            { key: "home", route: "/" },
            { key: "schedule", route: "schedule" },
            { key: "Criclytics", route: "criclytics" },
            { key: "news", route: "articles" },
            { key: "more", route: "more" }
          ]}
        />
      </div>
    );
  }
}

Schedule.propTypes = {
  fetchScheduleMatches: PropTypes.func,
  schedule: PropTypes.object,
  history: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  schedule: makeSelectSchedule()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchScheduleMatches: payload =>
      dispatch(scheduleActions.fetchScheduleMatches(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "schedule", reducer });
const withSaga = injectSaga({ key: "schedule", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(Schedule);
