/*
 *
 * Schedule reducer
 *
 */

import {
  FETCH_SCHEDULE_MATCHES,
  FETCH_SCHEDULE_MATCHES_SUCCESS,
  FETCH_SCHEDULE_MATCHES_FAILURE,
  FETCH_SCHEDULE_BY_SERIES,
  FETCH_SCHEDULE_BY_SERIES_FAILURE,
  FETCH_SCHEDULE_BY_SERIES_SUCCESS
} from "./constants";

export const initialState = {
  scheduleMatchesLoading: false,
  scheduleMatches: [],
  scheduleMatchesOldLength: 0,

  seriesMatches: [],
  seriesMatchesLoading: false
};

function scheduleReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_SCHEDULE_MATCHES:
      return Object.assign({}, state, {
        scheduleMatchesLoading: true,
        scheduleMatches:
          action.payload.skip !== 0 ? [...state.scheduleMatches] : []
      });
    case FETCH_SCHEDULE_MATCHES_SUCCESS: {
      // return Object.assign({}, state, {
      //   scheduleMatchesLoading: false,
      //   scheduleMatches: action.payload.data,
      // });
      const scheduleMatches = [...state.scheduleMatches];
      return {
        ...state,
        scheduleMatchesLoading: false,
        // scheduleMatches: action.payload.data,
        scheduleMatches:
          action.payload.skip !== 0
            ? scheduleMatches.concat(action.payload.data)
            : action.payload.data,
        scheduleMatchesOldLength:
          action.payload.skip !== 0
            ? action.payload.data.length + state.scheduleMatchesOldLength
            : 10
      };
    }
    case FETCH_SCHEDULE_MATCHES_FAILURE:
      return Object.assign({}, state, { scheduleMatchesLoading: false });
    case FETCH_SCHEDULE_BY_SERIES:
      return {
        ...state,
        seriesMatchesLoading: true
      };
    case FETCH_SCHEDULE_BY_SERIES_SUCCESS:
      return {
        ...state,
        seriesMatches: action.payload,
        seriesMatchesLoading: false
      };
    case FETCH_SCHEDULE_BY_SERIES_FAILURE:
      return {
        ...state,
        seriesMatchesLoading: false
      };
    default:
      return state;
  }
}

export default scheduleReducer;
