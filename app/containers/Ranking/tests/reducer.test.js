import { fromJS } from 'immutable';
import rankingReducer from '../reducer';

describe('rankingReducer', () => {
  it('returns the initial state', () => {
    expect(rankingReducer(undefined, {})).toEqual(fromJS({}));
  });
});
