/**
 *
 * Ranking
 *
 */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable no-nested-ternary */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import orderBy from "lodash/orderBy";

import Loader from "components-v2/commonComponent/Loader";
// import EmptyState from "components/Common/EmptyState";
import EmptyState from "components-v2/commonComponent/EmptyState";
import Header from "components-v2/commonComponent/Header";
import { Helmet } from "react-helmet";
import {
  grey_10,
  white,
  black,
  grey_4
} from "../../styleSheet/globalStyle/color";
import HrLine from "components-v2/commonComponent/HrLine";
import ScrollButtons from "../../components-v2/Rankings/ScrollButtons";
import RankingListItems from "components-v2/commonComponent/RankingListItems";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectRanking from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import * as rankingActions from "./actions";
import ReactGA from "react-ga";

/* eslint-disable react/prefer-stateless-function */

// const TABS_CONFIG = ["team", "batting", "bowling", "all-rounder"];
const TABS_CONFIG = ["team"];

const MATCH_TYPE = [
  { label: "ODI", key: "odi", filters: { format: "odi", gender: "male" } },
  { label: "T20", key: "t20", filters: { format: "t20", gender: "male" } },
  { label: "TEST", key: "test", filters: { format: "test", gender: "male" } }
  // {
  //   label: "women's odi",
  //   key: "women's odi",
  //   filters: { format: "odi", gender: "female" }
  // },
  // {
  //   label: "women's t20",
  //   key: "women's t20",
  //   filters: { format: "t20", gender: "female" }
  // },
  // {
  //   label: "women's test",
  //   key: "women's test",
  //   filters: { format: "test", gender: "female" }
  // }
];

// const MATCH_TYPE = [
//   "odi",
//   "t20",
//   "test",
//   "women's odi",
//   "women's t20",
//   "women's test"
// ];

const RANK_TYPE = ["team"];
let scrollTop = null;

let ClevertapReact = null;
if (typeof window !== "undefined") {
  ClevertapReact = require("clevertap-react");
  // ClevertapReact.initialize("TEST-Z88-4KR-845Z"); // Old Key
  ClevertapReact.initialize("W88-4KR-845Z");
  // initializeFirebase();
  // askForPermissioToReceiveNotifications();
}
export class Ranking extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "team",
      activeButton: "odi",
      activeTag: "ODI",
      filters: {},
      title: "",
      isHeaderBackground: false
    };
  }

  componentDidMount = () => {
    // ReactGA.initialize("UA-136526387-1");
    // ReactGA.pageview(
    //   "https://www.cricket.com" +
    //     window.location.pathname +
    //     window.location.search
    // );
    this.props.fetchRankingLatest();
    window.addEventListener("scroll", this.handleScroll);
  };
  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = event => {
    ({ scrollTop } = event.target.scrollingElement);
    // console.log("object: ", scrollTop);
    if (scrollTop > 26) {
      this.setState({
        title: "Team Rankings",
        isHeaderBackground: true
      });
    } else if (scrollTop < 27) {
      this.setState({
        title: "",
        isHeaderBackground: false
      });
    }
  };

  handelActiveTab = activeTab => {
    if (activeTab !== this.state.activeTab) {
      this.setState({ activeTab }, () => {
        this.props.fetchRankingLatest({
          ...this.state.filters,
          role: this.state.activeTab
        });
      });
    }
  };

  handelActiveTag = (activeTag, filters) => {
    if (activeTag !== this.state.activeTag) {
      this.setState({ activeTag, filters }, () => {
        this.props.fetchRankingLatest({
          ...filters,
          role: this.state.activeTab
        });
      });
    }
  };

  handleActiveTab = activeTab => {
    if (activeTab !== this.state.activeTab) {
      this.setState({ activeTab }, () => {
        this.props.fetchRankingLatest({
          ...this.state.filters,
          role: this.state.activeTab
        });
      });
    }
  };

  handleActiveButton = (activeButton, filters) => {
    if (activeButton !== this.state.activeButton) {
      this.setState({ activeButton, filters }, () => {
        this.props.fetchRankingLatest({
          ...filters,
          role: this.state.activeTab
        });
      });
    }
  };

  redirectTo = list => {
    
    ClevertapReact.event("Ranking", {
      source: "Ranking",
      teamId: list.teamId
    });
    this.props.history.push(
      `/${this.state.activeTab === "team" ? "teams" : "players"}/${list.teamId}`
    );
  };

  render() {
    const { ranking, history } = this.props;
    return (
      <div
        style={{
          padding: "54px 16px 16px"
        }}
      >
        <Helmet titleTemplate="%s | cricket.com">
          <title>Cricket Ranking | ODI Team Ranking | Team Rankings</title>
          <meta
            name="description"
            content="Get latest cricket team rankings and ratings of ODI, Twenty20 and Test Matches at cricket.com. "
          />
          <meta
            name="keywords"
            content="cricket ranking, cricket team rankings, odi team ranking, ipl ranking"
          />
          <link
            rel="canonical"
            href={`www.cricket.com${history &&
              history.location &&
              history.location.pathname}`}
          />
          {/* <link rel="apple-touch-icon" href="http://mysite.com/img/apple-touch-icon-57x57.png" /> */}
        </Helmet>
        {/* Ranking Title */}
        {false && (
          <div
            style={{
              fontFamily: "Montserrat",
              fontWeight: 700,
              fontSize: 22,
              color: grey_10,
              marginBottom: 12
            }}
          >
            Team Rankings
          </div>
        )}

        <Header
          // title={this.state.title}
          title="Team Rankings"
          // isHeaderBackground={this.state.isHeaderBackground}
          isHeaderBackground={true}
          leftArrowBack
          textColor={black}
          backgroundColor={grey_4}
          history={history}
          leftIconOnClick={() => {
            history.goBack();
          }}
        />
        {/* Rankiong Card Section */}
        <div
          style={{
            background: white,
            borderRadius: 3
          }}
        >
          {/* <TabBar
            items={RANK_TYPE}
            activeItem={this.state.activeTab}
            onClick={this.handleActiveTab}
          /> */}
          {/* <HrLine /> */}

          {/* Button Bar */}
          <ScrollButtons
            items={MATCH_TYPE}
            activeItem={this.state.activeButton}
            onClick={this.handleActiveButton}
            rootStyles={{
              justifyContent: "center"
            }}
          />
          <HrLine />
          {/* 
          <CardGradientTitle
            title="TEAMS"
            subtitle="RATING"
            titleStyles={{
              flex: 0.74
            }}
            subtitleStyles={{
              display: "flex",
              flex: 0.24,
              justifyContent: "flex-end",
              marginRight: 34
            }}
            rootStyles={{
              color: grey_8
            }}
          /> */}

          {/* Ranking List */}
          <RankingListItems
            rank={"RANK"}
            title={this.state.activeTab == "team" ? "TEAMS" : "PLAYERS"}
            type={this.state.activeTab == "team" ? "TEAMS" : "PLAYERS"}
            ranking={"RATING"}
            isHeader
            isRightIcon
            // redirectTo={this.redirectTo}   // Uncomment when needed
          />

          {ranking.rankingListLoading ? (
            <Loader styles={{ height: "60px" }} noOfLoaders={4} />
          ) : ranking.rankingList.length > 0 ? (
            orderBy(ranking.rankingList, "rating", "desc").map((list, key) => (
              <RankingListItems
                key={`${key + 1}`}
                rank={`${key < 9 ? "0" : ""}${key + 1}`}
                title={list.name}
                ranking={list.rating}
                subtitle={list.rating}
                avatar={list.image}
                teamName={list.shortName}
                type={this.state.activeTab == "team" ? "TEAMS" : "PLAYERS"}
                onClick={this.redirectTo} // Uncomment when needed
                param={list}
                isAvatar
                isRightIcon
              />
            ))
          ) : (
            <EmptyState msg="No rank list found" />
          )}
          {/* Ranking List Closing */}
        </div>
        {/* Ranking Card Section closing */}
      </div>
    );
  }
}

Ranking.propTypes = {
  ranking: PropTypes.object,
  fetchRankingLatest: PropTypes.func,
  history: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  ranking: makeSelectRanking()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchRankingLatest: payload =>
      dispatch(rankingActions.fetchRankingLatest(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "ranking", reducer });
const withSaga = injectSaga({ key: "ranking", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(Ranking);

const ListData = [
  {
    title: "India",
    subtitle: 998
  },
  {
    title: "Australia",
    subtitle: 99
  },
  {
    title: "India",
    subtitle: 998
  },
  {
    title: "India",
    subtitle: 998
  }
];
