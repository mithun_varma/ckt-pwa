/*
 *
 * Ranking actions
 *
 */
import { showToast } from 'utils/misc';
import {
  FETCH_RANKING_ALL,
  FETCH_RANKING_ALL_SUCCESS,
  FETCH_RANKING_ALL_FAILURE,
  FETCH_RANKING_LATEST,
  FETCH_RANKING_LATEST_SUCCESS,
  FETCH_RANKING_LATEST_FAILURE,
  FETCH_RANKING_DETAILS,
  FETCH_RANKING_DETAILS_SUCCESS,
  FETCH_RANKING_DETAILS_FAILURE,
} from './constants';

const ACTIVE_TOAST = {};

/* Get Ranking All actions */
export const fetchRankingAll = payload => ({
  type: FETCH_RANKING_ALL,
  payload,
});

export const fetchRankingAllSuccess = payload => ({
  type: FETCH_RANKING_ALL_SUCCESS,
  payload,
});

export const fetchRankingAllFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchRankingAll', 'Unable to fetch ranking all');
  return {
    type: FETCH_RANKING_ALL_FAILURE,
    payload: err,
  };
};

/* Get Ranking Latest actions */
export const fetchRankingLatest = payload => ({
  type: FETCH_RANKING_LATEST,
  payload,
});

export const fetchRankingLatestSuccess = payload => ({
  type: FETCH_RANKING_LATEST_SUCCESS,
  payload,
});

export const fetchRankingLatestFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchRankingLatest', 'Unable to fetch ranking latest');
  return {
    type: FETCH_RANKING_LATEST_FAILURE,
    payload: err,
  };
};

/* Get Ranking All actions */
export const fetchRankingDetails = payload => ({
  type: FETCH_RANKING_DETAILS,
  payload,
});

export const fetchRankingDetailsSuccess = payload => ({
  type: FETCH_RANKING_DETAILS_SUCCESS,
  payload,
});

export const fetchRankingDetailsFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchRankingDetails', 'Unable to fetch ranking details');
  return {
    type: FETCH_RANKING_DETAILS_FAILURE,
    payload: err,
  };
};
