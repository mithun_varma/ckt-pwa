import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the ranking state domain
 */

const selectRankingDomain = state => state.ranking || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Ranking
 */

const makeSelectRanking = () => createSelector(selectRankingDomain, substate => substate);

export default makeSelectRanking;
export { selectRankingDomain };
