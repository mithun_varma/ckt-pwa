import { call, put, takeLatest } from 'redux-saga/effects';
import { API_RANKING_LATEST } from 'utils/requestUrls';
import request from 'utils/request';
import * as rankingConstants from './constants';
import * as rankingActions from './actions';

export function* getRankingLatest(payload) {
  const config = {
    ...API_RANKING_LATEST,
    params: {
      ...API_RANKING_LATEST.params,
      ...payload.payload,
    },
  };
  try {
    const options = yield call(request, config);
    yield put(rankingActions.fetchRankingLatestSuccess(options.data));
  } catch (err) {
    yield put(rankingActions.fetchRankingLatestFailure(err));
  }
}

export default function* defaultSaga() {
  yield takeLatest(rankingConstants.FETCH_RANKING_LATEST, getRankingLatest);
}
