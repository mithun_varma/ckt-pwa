/*
 *
 * Ranking reducer
 *
 */

import {
  FETCH_RANKING_LATEST,
  FETCH_RANKING_LATEST_SUCCESS,
  FETCH_RANKING_LATEST_FAILURE
} from "./constants";

export const initialState = {
  rankingListLoading: false,
  rankingListInitiallyLoading: true,
  rankingList: []
};

function rankingReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_RANKING_LATEST:
      return {
        ...state,
        rankingListLoading: true,
        rankingListInitiallyLoading: true
      };
    case FETCH_RANKING_LATEST_SUCCESS:
      return {
        ...state,
        rankingListLoading: false,
        rankingListInitiallyLoading: false,
        rankingList:
          action.payload.data && action.payload.data.rankingData
            ? action.payload.data.rankingData
            : []
      };
    case FETCH_RANKING_LATEST_FAILURE:
      return {
        ...state,
        rankingListLoading: false,
        rankingListInitiallyLoading: false
      };
    default:
      return state;
  }
}

export default rankingReducer;
