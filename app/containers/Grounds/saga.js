import { takeLatest, call, put } from "redux-saga/effects";

import {
  API_GROUND_LIST,
  API_GROUND_LIST_NEW,
  API_GROUND_DETAILS,
  API_TRENDING_GROUND_LIST,
  API_GROUND_MAGIC_MOMENTS
} from "utils/requestUrls";
import request from "utils/request";

import * as groundConstants from "./constants";
import * as groundActions from "./actions";

// Individual exports for testing

export function* getGroundList() {
  try {
    const options = yield call(request, API_GROUND_LIST);
    yield put(groundActions.fetchGroundListSuccess(options.data.data));
  } catch (err) {
    yield put(groundActions.fetchGroundListFailure(err));
  }
}

// Gwet all ground lists (new)
export function* getGroundListNew() {
  try {
    const options = yield call(request, API_GROUND_LIST_NEW);
    yield put(groundActions.fetchGroundListNewSuccess(options.data.data));
  } catch (err) {
    yield put(groundActions.fetchGroundListNewFailure(err));
  }
}

export function* getTrendingGround() {
  try {
    const options = yield call(request, API_TRENDING_GROUND_LIST);
    yield put(groundActions.fetchTrendingGroundSuccess(options.data.data));
  } catch (err) {
    yield put(groundActions.fetchTrendingGroundFailure(err));
  }
}

export function* getGroundDetails(payload) {
  try {
    const options = yield call(
      request,
      API_GROUND_DETAILS(payload.payload.venueId)
    );
    yield put(groundActions.fetchGroundDetailsSuccess(options.data));
  } catch (err) {
    yield put(groundActions.fetchGroundDetailsFailure(err));
  }
}

export function* getGroundMagicMoments(payload) {
  try {
    const options = yield call(
      request,
      API_GROUND_MAGIC_MOMENTS(payload.payload.venueId)
    );
    yield put(groundActions.fetchGroundMagicMomentsSuccess(options.data.data));
  } catch (err) {
    yield put(groundActions.fetchGroundMagicMomentsFailure(err));
  }
}

export function* getPopularGrounds() {
  try {
    const options = yield c;
    yield put(groundActions.fetchPopularGroundSuccess(options.data.data));
  } catch (err) {
    yield put(groundActions.fetchPopularGroundFailure(err));
  }
}


export default function* defaultSaga() {
  yield takeLatest(groundConstants.FETCH_GROUND_LIST, getGroundList);
  yield takeLatest(groundConstants.FETCH_GROUND_LIST_NEW, getGroundListNew);
  yield takeLatest(groundConstants.FETCH_GROUND_DETAILS, getGroundDetails);
  yield takeLatest(groundConstants.FETCH_GROUND_MAGIC_MOMENTS, getGroundMagicMoments);
  yield takeLatest(groundConstants.FETCH_TRENDING_GROUND, getTrendingGround);
  yield takeLatest(groundConstants.FETCH_POPULAR_GROUND, getPopularGrounds);
}
