/**
 *
 * Grounds
 *
 */
/* eslint-disable no-nested-ternary */
/* eslint no-underscore-dangle: 0 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { compose } from "redux";
import { Helmet } from "react-helmet";
import BackgroundComponent from "../../components-v2/commonComponent/BackgroundComponent";
import ImageContainer from "../../components-v2/commonComponent/ImageContainer";

import GroundDetails from "components-v2/Grounds/GroundDetails";

import injectSaga from "utils/injectSaga";
import injectReducer from "utils/injectReducer";
import makeSelectGrounds from "./selectors";
import reducer from "./reducer";
import saga from "./saga";

import * as groundActions from "./actions";
import Header from "../../components-v2/commonComponent/Header";
import { white, gradientRedOrange } from "../../styleSheet/globalStyle/color";
import { screenHeight } from "../../styleSheet/screenSize/ScreenDetails";
import ReactGA from "react-ga";
/* eslint-disable react/prefer-stateless-function */

let scrollTop = null;
export class Grounds extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      venueName: "",
      title: "",
      isHeaderBackground: false,
      showHeader: false,
      showHeadBg: true
    };
  }

  componentDidMount = () => {
    // ReactGA.initialize("UA-136526387-1");
    // ReactGA.pageview(
    //   "https://www.cricket.com" +
    //     window.location.pathname +
    //     window.location.search
    // );
    const { grounds, history } = this.props;
    window.addEventListener("scroll", this.handleScroll);
    if (this.props.match.params.venueId) {
      this.props.fetchGroundDetails({
        venueId: this.props.match.params.venueId
      });
    } else {
      this.props.fetchGroundListNew();
    }
  };
  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = event => {
    ({ scrollTop } = event.target.scrollingElement);
    if (scrollTop > 29) {
      this.setState({
        // title: "Stadiums",
        title: this.getHeaderTitle(),
        isHeaderBackground: true
      });
    } else if (scrollTop < 33) {
      this.setState({
        title: "",
        isHeaderBackground: false
      });
    }
  };

  toggleHeaderBg() {
    this.setState({
      isHeaderBackground: false,
      showHeadBg: !this.state.showHeadBg
    });
  }

  UNSAFE_componentWillReceiveProps = nextProps => {
    if (nextProps.match.url != this.props.match.url) {
      this.props.fetchTrendingGround();
    }
  };

  getGroundDetails = ground => {
    this.setState(
      {
        venueName: ground.name
      },
      () => {
        this.props.history.push(`/grounds/${ground.crictecId}`);
        let details = this.props.fetchGroundDetails({
          venueId: ground.crictecId
        });
      }
    );
  };

  getHeaderTitle = () => {
    if (this.props.match.params.venueId) {
      return (
        this.state.venueName.substring(0, this.state.venueName.indexOf(",")) ||
        (this.props.grounds.groundDetails.name &&
          this.props.grounds.groundDetails.name.substring(
            0,
            this.props.grounds.groundDetails.name.indexOf(",")
          ))
      );
    }
    return "Stadiums";
  };

  getHeaderTitle = () => {
    if (this.props.match.params.venueId) {
      return (
        this.state.venueName ||
        (this.props.grounds.groundDetails.name &&
          this.props.grounds.groundDetails.name.substring(
            0,
            this.props.grounds.groundDetails.name.indexOf(",")
          ))
      );
    }
    return "Stadiums";
  };

  componentDidUpdate() {
    const { grounds, history } = this.props;
  }

  displayGrounds = groundData => {
    return groundData.map((ele, key) => {
      return ele.length > 0 ? (
        <ImageContainer
          rootStyles={{
            marginTop: 12
          }}
          data={ele.stadiums}
          loading={false}
          title={ele.type}
          backgroundStyles={{
            background:
              "linear-gradient(30deg, #141b2f, rgba(114, 118, 130, 0))"
          }}
          getGroundDetails={this.getGroundDetails}
        />
      ) : null;
    });
  };

  render() {
    const { grounds, history } = this.props;
    const headerTitle = this.getHeaderTitle();
    let bgHeight = 150;
    if (this.props.grounds.groundDetails.name) {
      bgHeight = this.props.grounds.groundDetails.name.length > 30 ? 150 : 150;
    }
    return (
      <div>
        <Helmet titleTemplate="%s | cricket.com">
          <title>
            Cricket Stadium in India and Abroad | Stadium Details and Past Match
            Statistics
          </title>
          <meta
            name="description"
            content="Get list of all the ipl venues and world cup 2019 venues at Cricket.com. Check out the stadium stats and details of test, odi and t20 matches."
          />
          <meta
            name="keywords"
            content="top cricket stadium in india, top cricket stadium abroad, today ipl match venue, ipl venues, world cup venues"
          />
          <link
            rel="canonical"
            href={`www.cricket.com${history &&
              history.location &&
              history.location.pathname}`}
          />
        </Helmet>
        <Header
          title={this.getHeaderTitle()}
          isHeaderBackground={this.state.isHeaderBackground}
          leftArrowBack
          textColor={white}
          backgroundColor={gradientRedOrange}
          history={history}
          showHeader={true}
          leftIconOnClick={() => {
            if(this.props.match.params.venueId) {
              this.props.fetchGroundListNew();
            }
            history.goBack();
          }}
          titleStyles={{ fontSize: this.props.match.params.venueId ? 18 : 18 }}
        />

        <BackgroundComponent
          title={null}
          rootStyles={{
            height: this.props.match.params.venueId ? bgHeight : 150
          }}
          show={this.state.showHeadBg}
          titleStyles={{
            fontSize: this.props.match.params.venueId ? 22 : 22
          }}
        />
        {this.props.match.params.venueId ? (
          <div style={{ marginTop: screenHeight * 0.05 }}>
            <GroundDetails
              grounds={grounds}
              show={this.state.showHeadBg}
              toggleHeader={() => this.toggleHeaderBg()}
              id={this.props.match.params.venueId}
              fetchGroundMagicMoments={this.props.fetchGroundMagicMoments}
            />
          </div>
        ) : (
          <div style={{ marginTop: -100 }}>
            {grounds.groundList &&
              grounds.groundList.map((data, key) => (
                <ImageContainer
                  rootStyles={{
                    marginTop: 12,
                    width: "90%"
                  }}
                  data={data.stadiums}
                  isAvatar={data.type === "popularGrounds" ? true : false}
                  loading={false}
                  title={data.type}
                  backgroundStyles={{
                    background:
                      "linear-gradient(30deg, #141b2f, rgba(114, 118, 130, 0))"
                  }}
                  key={data.type}
                  getGroundDetails={this.getGroundDetails}
                />
              ))}{" "}
          </div>
        )}
      </div>
    );
  }
}

Grounds.propTypes = {
  fetchGroundList: PropTypes.func.isRequired,
  fetchGroundListNew: PropTypes.func.isRequired,
  fetchGroundMagicMoments: PropTypes.func.isRequired,
  fetchGroundDetails: PropTypes.func.isRequired,
  grounds: PropTypes.object,
  history: PropTypes.object,
  moments: PropTypes.object,
  match: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  grounds: makeSelectGrounds()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchGroundList: payload =>
      dispatch(groundActions.fetchGroundList(payload)),
    fetchGroundListNew: payload =>
      dispatch(groundActions.fetchGroundListNew(payload)),
    fetchGroundMagicMoments: payload =>
      dispatch(groundActions.fetchGroundMagicMoments(payload)),
    fetchGroundDetails: payload =>
      dispatch(groundActions.fetchGroundDetails(payload)),
    fetchTrendingGround: payload =>
      dispatch(groundActions.fetchTrendingGround(payload))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: "grounds", reducer });
const withSaga = injectSaga({ key: "grounds", saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(Grounds);

const popular = [
  {
    name: "Eden Gardens, Kolkata",
    img: require("../../images/place_holder_1.png")
  },
  {
    name: "MCG, Sydney",
    img: require("../../images/place_holder_1.png")
  },
  {
    name: "Eden Gardens, Kolkata",
    img: require("../../images/place_holder_1.png")
  },
  {
    name: "MCG, Sydney",
    img: require("../../images/place_holder_1.png")
  },
  {
    name: "Eden Gardens, Kolkata",
    img: require("../../images/place_holder_1.png")
  }
];

const trending = [
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "MCG, Sydney",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "MCG, Sydney",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  }
];
const similarGrounds = [
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "MCG, Sydney",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "MCG, Sydney",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  }
];

const mostMatches = [
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "MCG, Sydney",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "MCG, Sydney",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  }
];

const highestBatting = [
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "MCG, Sydney",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "MCG, Sydney",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  }
];

const highestBowling = [
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "MCG, Sydney",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "MCG, Sydney",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  }
];

const favouriteStatium = [
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "MCG, Sydney",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "MCG, Sydney",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  },
  {
    name: "Eden Gardens, Kolkata",
    img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
  }
];
