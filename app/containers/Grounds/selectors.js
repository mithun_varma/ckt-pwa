import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the grounds state domain
 */

const selectGroundsDomain = state => state.grounds || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Grounds
 */

const makeSelectGrounds = () => createSelector(selectGroundsDomain, substate => substate);

export default makeSelectGrounds;
export { selectGroundsDomain };
