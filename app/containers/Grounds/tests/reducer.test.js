import { fromJS } from 'immutable';
import groundsReducer from '../reducer';

describe('groundsReducer', () => {
  it('returns the initial state', () => {
    expect(groundsReducer(undefined, {})).toEqual(fromJS({}));
  });
});
