/*
 *
 * Grounds actions
 *
 */
import { showToast } from 'utils/misc';
import {
  FETCH_GROUND_LIST,
  FETCH_GROUND_LIST_SUCCESS,
  FETCH_GROUND_LIST_FAILURE,
  FETCH_GROUND_DETAILS,
  FETCH_GROUND_DETAILS_SUCCESS,
  FETCH_GROUND_DETAILS_FAILURE,
  FETCH_TRENDING_GROUND,
  FETCH_TRENDING_GROUND_SUCCESS,
  FETCH_TRENDING_GROUND_FAILURE,
  FETCH_GROUND_LIST_NEW,
  FETCH_GROUND_LIST_NEW_SUCCESS,
  FETCH_GROUND_LIST_NEW_FAILURE,
  FETCH_GROUND_MAGIC_MOMENTS,
  FETCH_GROUND_MAGIC_MOMENTS_SUCCESS,
  FETCH_GROUND_MAGIC_MOMENTS_FAILURE

} from './constants';

const ACTIVE_TOAST = {};

// Ground List actions
export const fetchGroundList = payload => ({
  type: FETCH_GROUND_LIST,
  payload,
});

export const fetchGroundListSuccess = payload => ({
  type: FETCH_GROUND_LIST_SUCCESS,
  payload,
});

export const fetchGroundListFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchGroundList', 'Unable to fetch ground list');
  return {
    type: FETCH_GROUND_LIST_FAILURE,
    payload: err,
  };
};

// Ground List  new actions
export const fetchGroundListNew = payload => {
  return  ({
    type: FETCH_GROUND_LIST_NEW,
    payload,
  });
}

export const fetchGroundListNewSuccess = payload => ({
  type: FETCH_GROUND_LIST_NEW_SUCCESS,
  payload,
});

export const fetchGroundListNewFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchGroundList', 'Unable to fetch ground list');
  return {
    type: FETCH_GROUND_LIST_NEW_FAILURE,
    payload: err,
  };
};

// Ground Details actions
export const fetchGroundDetails = payload => ({
  type: FETCH_GROUND_DETAILS,
  payload,
});

export const fetchGroundDetailsSuccess = payload => ({
  type: FETCH_GROUND_DETAILS_SUCCESS,
  payload,
});

export const fetchGroundDetailsFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchGroundDetails', 'Unable to fetch ground details');
  return {
    type: FETCH_GROUND_DETAILS_FAILURE,
    payload: err,
  };
};

// Ground Magic Moments actions
export const fetchGroundMagicMoments = payload => {
  return ({
    type: FETCH_GROUND_MAGIC_MOMENTS,
    payload,
  }); 
}

export const fetchGroundMagicMomentsSuccess = payload => ({
  type: FETCH_GROUND_MAGIC_MOMENTS_SUCCESS,
  payload,
});

export const fetchGroundMagicMomentsFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchMagicMoments', 'Unable to fetch ground magic moments');
  return {
    type: FETCH_GROUND_MAGIC_MOMENTS_FAILURE,
    payload: err,
  };
};

// Trending Ground actions
export const fetchTrendingGround = payload => ({
  type: FETCH_TRENDING_GROUND,
  payload,
});

export const fetchTrendingGroundSuccess = payload => ({
  type: FETCH_TRENDING_GROUND_SUCCESS,
  payload,
});

export const fetchTrendingGroundFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchTrendingGround', 'Unable to fetch trending ground');
  return {
    type: FETCH_TRENDING_GROUND_FAILURE,
    payload: err,
  };
};

// Popular Ground actions
export const fetchPopularGround = payload => ({
  type: FETCH_POPULAR_GROUND,
  payload,
});

export const fetchPopularGroundSuccess = payload => ({
  type: FETCH_POPULAR_GROUND_SUCCESS,
  payload,
});

export const fetchPopularGroundFailure = err => {
  showToast(ACTIVE_TOAST, 'fetchPopularGround', 'Unable to fetch Popular ground');
  return {
    type: FETCH_POPULAR_GROUND_FAILURE,
    payload: err,
  };
};
