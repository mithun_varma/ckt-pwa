/*
 *
 * Grounds reducer
 *
 */
import groupBy from "lodash/groupBy";
import {
  FETCH_GROUND_LIST,
  FETCH_GROUND_LIST_SUCCESS,
  FETCH_GROUND_LIST_FAILURE,
  FETCH_GROUND_LIST_NEW,
  FETCH_GROUND_LIST_NEW_SUCCESS,
  FETCH_GROUND_LIST_NEW_FAILURE,
  FETCH_GROUND_DETAILS,
  FETCH_GROUND_DETAILS_SUCCESS,
  FETCH_GROUND_DETAILS_FAILURE,
  FETCH_TRENDING_GROUND,
  FETCH_TRENDING_GROUND_SUCCESS,
  FETCH_TRENDING_GROUND_FAILURE,
  FETCH_POPULAR_GROUND,
  FETCH_POPULAR_GROUND_SUCCESS,
  FETCH_POPULAR_GROUND_FAILURE,
  FETCH_GROUND_MAGIC_MOMENTS,
  FETCH_GROUND_MAGIC_MOMENTS_SUCCESS,
  FETCH_GROUND_MAGIC_MOMENTS_FAILURE
} from "./constants";

export const initialState = {
  groundListLoading: false,
  groundList: [],
  groundListNewLoading: false,
  groundListNew: [],
  groundCountryWise: {},
  groundDetailsLoading: false,
  groundDetails: {},
  momentsLoading: false,
  moments: {},
  trendingGround: [],
  trendingGroundLoading: false,
  popularGround: [],
  popularGroundLoading: false,
};

function groundsReducer(state = initialState, action) {
  switch (action.type) {
    // Ground List Case
    case FETCH_GROUND_LIST:
      return {
        ...state,
        groundListLoading: true
      };
    case FETCH_GROUND_LIST_SUCCESS:
      return {
        ...state,
        groundListLoading: false,
        groundCountryWise: groupBy(action.payload, "country"),
        groundList: action.payload
      };
    case FETCH_GROUND_LIST_FAILURE:
      return {
        ...state,
        groundListLoading: false
      };
    // Ground List New Case
    case FETCH_GROUND_LIST_NEW:
      return {
        ...state,
        groundListNewLoading: true
      };
    case FETCH_GROUND_LIST_NEW_SUCCESS:
      return {
        ...state,
        groundList: action.payload,
        groundListLoading : false
     
      };
    case FETCH_GROUND_LIST_NEW_FAILURE:
      return {
        ...state,
        groundListNewLoading: false
      };
    // Ground Details Case
    case FETCH_GROUND_DETAILS:
      return {
        ...state,
        groundDetailsLoading: true
      };
    case FETCH_GROUND_DETAILS_SUCCESS:{
      return {
        ...state,
        groundDetailsLoading: false,
        groundDetails: action.payload
      };
    }
    case FETCH_GROUND_DETAILS_FAILURE:
      return {
        ...state,
        groundDetailsLoading: false
      };
    // Magic Moments Case
    case FETCH_GROUND_MAGIC_MOMENTS:
      return {
        ...state,
        momentsLoading: true
      };
    case FETCH_GROUND_MAGIC_MOMENTS_SUCCESS:{
      return {
        ...state,
        momentsLoading: false,
        moments: action.payload
      };
    }
    case FETCH_GROUND_MAGIC_MOMENTS_FAILURE:
      return {
        ...state,
        momentsLoading: false
      };
    // Trending Ground Case
    case FETCH_TRENDING_GROUND:
      return {
        ...state,
        trendingGroundLoading: true
      };
    case FETCH_TRENDING_GROUND_SUCCESS:
      return {
        ...state,
        trendingGroundLoading: false,
        trendingGround: action.payload ? action.payload : []
      };
    case FETCH_TRENDING_GROUND_FAILURE:
      return {
        ...state,
        trendingGroundLoading: false
      };
    // Popular Ground Case
    case FETCH_POPULAR_GROUND:
      return {
        ...state,
        popularGroundLoading: true
      };
    case FETCH_POPULAR_GROUND_SUCCESS:
      return {
        ...state,
        popularGroundLoading: false,
        popularGround: action.payload ? action.payload : []
      };
    case FETCH_POPULAR_GROUND_FAILURE:
      return {
        ...state,
        popularGroundLoading: false
      };
    default:
      return state;
  }
}

export default groundsReducer;
