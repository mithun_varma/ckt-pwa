// should be used with backgroundImage in css
export const gradientOrange = "linear-gradient(to right, #fa9441, #ea6550)";
export const gradientOrangeLeft = "linear-gradient(to left, #fa9441, #ea6550)";
export const gradientOrangeToBottom =
  "linear-gradient(to bottom, #fa9441, #ea6550)";
export const gradientRedOrange = "linear-gradient(to left, #ba5222, #a70f14)";
export const gradientRedNew = "linear-gradient(to bottom, #d44030, #9b000d)";
export const gradientRedNewLeft = "linear-gradient(to left, #d44030, #9b000d)";
export const gradientRedOrangeDeg = "linear-gradient(257deg, #ef724d, #9b000d)";
export const gradientRedOrangeLeft =
  "linear-gradient(to left, #d74730, #9b000d)";
export const gradientGreenYellow = "linear-gradient(285deg, #ffef05, #085059)";

// export const orange_10 = "#fa9441, #ea6550)";
export const gradientGrey =
  "linear-gradient(to right, #ffffff, #f0f1f2, #f0f1f2, #ffffff)";
export const cardShadow = "rgba(19, 0, 0, 0.07) 0px 3px 6px 1px";
export const avatarShadow =
  "0 0px 2px 0 rgba(0, 0, 0, 0.2), 0 1px 3px 0 rgba(0, 0, 0, 0.3)";

export const gradientBlack = "linear-gradient(to right, #353e59, #1e2437)";

export const gradientBlackSecondary =
  "linear-gradient(to right, #141b2f, #727682)";

export const gradientIPL =
  "linear-gradient(38deg, rgba(235, 104, 79,1), rgba(114, 118, 130, 0.2) 60%)";
export const gradientNonIpl =
  "linear-gradient(38deg, #141b2f, rgba(114, 118, 130, 0.2) 60%)";

export const red_Orange = "#d44030";

export const red_10 = "#d64b4b";
export const red_8 = "#dd6e6e";
export const red_6 = "#e49191";
export const red_4 = "#ecb4b4";
export const red_2 = "#f3d7d7";
export const darkRed = "#a70e13";

export const blue_10 = "#4a90e2";
export const blue_8 = "#6da5e7";
export const blue_6 = "#90baec";
export const blue_4 = "#b3d0f0";
export const blue_2 = "#d7e5f5";
export const blue_grey = "#8b94ae";

export const green_10 = "#35a863";
export const green_8 = "#5cb981";
export const green_6 = "#84c99f";
export const green_4 = "#abd9be";
export const green_2 = "#d3eadc";
export const lightGreen = "#61a823";

export const orange_10 = "#fa9441";
export const orange_8 = "#fba967";
export const orange_6 = "#fcbf8d";
export const orange_4 = "#fdd4b3";
export const orange_2 = "#feead9";
export const orange_2_1 = "#ffe8d9";
export const orange_1 = "#f76b1c";
export const orange_1_1 = "#f5a623";

export const grey_10 = "#141b2f";
export const grey_9 = "#697186";
export const grey_8 = "#727682";
export const grey_7 = "#9b9b9b";
export const grey_7_1 = "#979797";
export const grey_6 = "#a1a4ac";
export const grey_5 = "#d7d7ea";
export const grey_5_1 = "#dcdee6";
export const grey_4 = "#edeff4";
export const grey_2 = "#dbdbdb";
export const grey_2_1 = "#eeeeee";
export const grey_1 = "#fafafa";

export const pink_10 = "#ea6550";
export const pink_8 = "#ee8473";
export const pink_6 = "#f2a396";
export const pink_4 = "#f7c1b9";
export const pink_3 = "#fecdc8";
export const pink_2 = "#fbe0dc";
export const pink_1 = "#ffe9e8";

export const white = "#FFFFFF";
export const black = "#000000";

export const yellow_10 = "#fbedc6";
export const yellow_8 = "#fffaeb";
