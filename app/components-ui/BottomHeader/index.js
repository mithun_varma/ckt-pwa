/**
 *
 * BottomHeader
 *
 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */

import React from 'react';
import Home from '@material-ui/icons/Home';
import CalendarToday from '@material-ui/icons/CalendarToday';
import Menu from '@material-ui/icons/Menu';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

// function BottomHeader(props) {
const BottomHeader = props => {
  const redirectTo = path => {
    props.push(`/${path === '/' ? '' : path}`);
  };

  return (
    <div className="bottomHeader">
      <ul>
        <li
          className={`${
            // props.location.pathname.indexOf(props.tabs[0].route) == 0 ? 'active-tab' : 'inactive-tab'
            props.location.pathname === '/' ? 'active-tab' : 'inactive-tab'
          }`}
        >
          <Home className="tab-icon" />
          <span
            onClick={() => {
              redirectTo(props.tabs[0].route);
            }}
          >
            {props.tabs[0].key}
          </span>
        </li>
        <li
          className={`${
            props.location.pathname.indexOf(props.tabs[1].route) > 0 ? 'active-tab' : 'inactive-tab'
          }`}
        >
          <CalendarToday className="tab-icon" />
          <span
            onClick={() => {
              redirectTo(props.tabs[1].route);
            }}
          >
            {props.tabs[1].key}
          </span>
        </li>
        <li
          className={`${
            props.location.pathname.indexOf(props.tabs[2].route) > 0 ? 'active-tab' : 'inactive-tab'
          }`}
        >
          <Menu className="tab-icon" />
          <span
            onClick={() => {
              redirectTo(props.tabs[2].route);
            }}
          >
            {props.tabs[2].key}
          </span>
        </li>
      </ul>
    </div>
  );
};

BottomHeader.propTypes = {
  location: PropTypes.object,
  tabs: PropTypes.array,
  push: PropTypes.func,
};

export default BottomHeader;
