/**
 *
 * FeaturedCard
 *
 */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import PlayArrow from '@material-ui/icons//PlayArrow';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function FeaturedCardSm() {
  return (
    <React.Fragment>
      <div className="featured featured--videos">
        <div className="featured__header">
          <div className="featured__title">
            <PlayArrow />
          </div>
          <div className="articlefull__date articlefull__date__icon ">
            {/* Crictec Editorial Team
            <span className="dot-middle">•</span> */}
            6/12/2018
          </div>
        </div>

        <div className="featured__footer">
          <span>Yet Another Bohemian Rhapsody, DK - The Misfit That Refused to Fit in</span>
        </div>
      </div>
    </React.Fragment>
  );
}

FeaturedCardSm.propTypes = {};

export default FeaturedCardSm;
