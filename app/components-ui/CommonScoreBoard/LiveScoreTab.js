/**
 *
 * CommonScoreBoard
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function LiveScoreTab() {
  return (
    <React.Fragment>
      <table className="table live-table">
        <thead>
          <tr>
            <th>BATSMEN</th>
            <th>R</th>
            <th>B</th>
            <th>4s</th>
            <th>6s</th>
            <th>SR</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Diana Baig* (rhb)</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td>100.00</td>
          </tr>
          <tr>
            <td>Sidra Nawaz (rhb)</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td>33.33</td>
          </tr>
        </tbody>
      </table>
      <table className="table live-table">
        <thead>
          <tr>
            <th>BOWLERS</th>
            <th>O</th>
            <th>M</th>
            <th>R</th>
            <th>W</th>
            <th>ECON</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Diana Baig* (rhb)</td>
            <td>1</td>
            <td>1</td>
            <td>1</td>
            <td>2</td>
            <td>100.00</td>
          </tr>
        </tbody>
      </table>
      <div className="liveScore">
        <div className="liveScore__card">
          <span>
            CRR: <strong>3.42</strong>
          </span>
          <span>
            RRR: <strong>3.42</strong>
          </span>
          <span>
            Min.Ov.Rem: <strong>5.1</strong>
          </span>
        </div>
        <div className="liveScore__card">
          <span>
            Ground Time: <strong>16:40</strong>
          </span>
          <span>
            Reviews left: <strong>AUS 2 | PAK 2</strong>
          </span>
        </div>
        <div className="liveScore__card">
          <span>Resect:</span>
          <ul>
            <li className="overNumber">.</li>
            <li className="overNumber fourball">4</li>
            <li className="overNumber">1</li>
            <li className="overNumber sixball">6</li>
            <li className="overNumber">1</li>
            <li className="overNumber wicketball">w</li>
          </ul>
        </div>
      </div>
    </React.Fragment>
  );
}

export default LiveScoreTab;
