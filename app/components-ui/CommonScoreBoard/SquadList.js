/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint no-unused-expressions: 0 */
import React from 'react';
// import PropTypes from 'prop-types';
import ChevronRight from '@material-ui/icons/ChevronRight';
import map from 'lodash/map';

// const teamList = [
//   {
//     teamItem: {
//       source:
//         'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Virat..Kohli.jpg/260px-Virat..Kohli.jpg',
//       playerName: 'Virat Kohli',
//       playerType: 'Top-order batsman',
//       battingDetails: 'Right-hand bat',
//       bowlingDetails: 'Right-arm medium',
//     },
//   },
//   {
//     teamItem: {
//       playerName: 'Virat Kohli',
//       arrowIcon: <ChevronRight style={{ color: 'red' }} />,
//     },
//   },
// ];

// function SquadList(data) {
const SquadList = data => (
  <React.Fragment>
    {map(data.teamList, team => (
      <div
        className="squardList"
        onClick={() => {
          team.teamItem.route && data.history.push(`${team.teamItem.route}`);
        }}
      >
        {team.teamItem.source && (
          <div className="squardList__image">
            <img src={team.teamItem.source} alt="dc" />
          </div>
        )}
        <div className="squardList__content">
          <div className="squardList__list">
            {team.teamItem.playerName && (
              <span>
                <strong>{team.teamItem.playerName}</strong>
              </span>
            )}
            {team.teamItem.playerType && <span>{team.teamItem.playerType}</span>}
          </div>
          <div className="squardList__list">
            {team.teamItem.battingDetails && (
              <span>
                <strong>Batting:</strong>
                {team.teamItem.battingDetails}
              </span>
            )}
            {team.teamItem.bowlingDetails && (
              <span>
                <strong>Bowling:</strong>
                {team.teamItem.bowlingDetails}
              </span>
            )}
            {team.teamItem.arrowIcon && (
              <span className="squardList__list__icon">
                {team.teamItem.arrowIcon && <ChevronRight style={{ color: 'red' }} />}
              </span>
            )}
          </div>
        </div>
      </div>
    ))}
  </React.Fragment>
);

SquadList.propTypes = {};

export default SquadList;
