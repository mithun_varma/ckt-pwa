/**
 *
 * CommonScoreBoard
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function MatchDetail() {
  return (
    <div className="matchDetails">
      <ul>
        <li>
          <span className="name">Venue</span>
          <span className="description">The Rose Bowl,Southampton </span>
        </li>
        <li>
          <span className="name">Series</span>
          <span className="description">
            <a href>Eng vs Ind, 4th Test, India tour of England, 2018</a>
          </span>
        </li>
        <li>
          <span className="name">Match Number</span>
          <span className="description"> ODI no. 4032 </span>
        </li>
        <li>
          <span className="name">Toss</span>
          <span className="description">England, elected to field first </span>
        </li>
        <li>
          <span className="name">Umpires</span>
          <span className="description"> Mark Hawthorn, Ian Gould</span>
        </li>
        <li>
          <span className="name">Reserve Umpires</span>
          <span className="description">Alan Neill </span>
        </li>
        <li>
          <span className="name">Match Referee</span>
          <span className="description">Ranjan Madugalle </span>
        </li>
      </ul>
    </div>
  );
}

MatchDetail.propTypes = {};

export default MatchDetail;
