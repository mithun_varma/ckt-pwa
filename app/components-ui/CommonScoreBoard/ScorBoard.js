/**
 *
 * CommonScorBoard
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import ChevronRight from '@material-ui/icons/ChevronRight';
// import styled from 'styled-components';

function ScorBoard({ accortitle }) {
  return (
    <React.Fragment>
      <div className="accorTabs">
        <div className="accorHeader">
          <div className="squardList">
            <div className="squardList__content">
              <div className="squardList__list">
                <span>
                  <strong>{accortitle}</strong>
                </span>
              </div>
              <div className="squardList__list">
                <span className="squardList__list__icon">
                  <ChevronRight style={{ color: 'red' }} />
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="accorCcntent">
          <div className="table-responsive">
            <table className="table scorecard-table">
              <thead>
                <tr>
                  <th>Batsmen</th>
                  <th>R</th>
                  <th>B</th>
                  <th>4</th>
                  <th>6</th>
                  <th>SR</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    AN cook
                    <small>Ashvin</small>
                  </td>
                  <td>13</td>
                  <td>28 </td>
                  <td>0</td>
                  <td>0</td>
                  <td>46.6</td>
                </tr>
                <tr>
                  <td>
                    KK jenning
                    <small> Mohammed Shami</small>
                  </td>
                  <td>42</td>
                  <td>98 </td>
                  <td>0</td>
                  <td>1</td>
                  <td>42.85</td>
                </tr>
                <tr>
                  <td>
                    JE Root(c)
                    <small> not out</small>
                  </td>
                  <td>56</td>
                  <td>43 </td>
                  <td>4</td>
                  <td>2</td>
                  <td>51.28</td>
                </tr>
                <tr>
                  <td>
                    OJ Pope
                    <small> Run out (Kohli)</small>
                  </td>
                  <td>3</td>
                  <td>2 </td>
                  <td>0</td>
                  <td>0</td>
                  <td>57.14</td>
                </tr>
                <tr>
                  <td>
                    JM Bairtow
                    <small> lbw (b) Mohammed Sham</small>
                  </td>
                  <td>5</td>
                  <td>7 </td>
                  <td>0</td>
                  <td>0</td>
                  <td>14.23</td>
                </tr>
                <tr>
                  <td>
                    JC buttler
                    <small> lbw (b) Mohammed Sham</small>
                  </td>
                  <td>10</td>
                  <td>4</td>
                  <td>1</td>
                  <td>0</td>
                  <td>32.32</td>
                </tr>
              </tbody>
            </table>
            <div className="wicketFall-Card">
              <ul>
                <li>
                  Yet to Bat:
                  <ul className="full-wicket">
                    <li> DM de Silva,</li>
                    <li>NLTC Perera, MD Shanaka,</li>
                    <li> A Dananjaya,</li>
                    <li> PVD Chameera,</li>
                    <li>CAK Rajitha,</li>
                    <li>PADLR Sandakan</li>
                    <li>NLTC Perera, MD Shanaka,</li>
                    <li> A Dananjaya,</li>
                    <li> PVD Chameera,</li>
                    <li>CAK Rajitha,</li>
                    <li>PADLR Sandakan</li>
                  </ul>
                </li>
                <li>
                  Fall of wickets:
                  <ul className="full-wicket">
                    <li>1-137 (S Samarawickrama, 19.1 ov),</li>
                    <li>2-168 (N Dickwella, 25.4 ov)</li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
          <div className="table-responsive">
            <table className="table">
              <thead>
                <tr>
                  <th>BOWLERS</th>
                  <th>O</th>
                  <th>M</th>
                  <th>R</th>
                  <th>W</th>
                  <th>NB</th>
                  <th>WD</th>
                  <th>Econ</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Ut Yadav</td>
                  <td>2</td>
                  <td>2 </td>
                  <td>45</td>
                  <td>2</td>
                  <td>0</td>
                  <td>0</td>
                  <td>3.29</td>
                </tr>
                <tr>
                  <td>I Sharma</td>
                  <td>2</td>
                  <td>2 </td>
                  <td>45</td>
                  <td>2</td>
                  <td>0</td>
                  <td>0</td>
                  <td>3.29</td>
                </tr>
                <tr>
                  <td>R Ashwin</td>
                  <td>2</td>
                  <td>2 </td>
                  <td>45</td>
                  <td>2</td>
                  <td>0</td>
                  <td>0</td>
                  <td>3.29</td>
                </tr>
                <tr>
                  <td>Ut Yadav</td>
                  <td>2</td>
                  <td>2 </td>
                  <td>45</td>
                  <td>2</td>
                  <td>0</td>
                  <td>0</td>
                  <td>3.29</td>
                </tr>
                <tr>
                  <td>M. Shami</td>
                  <td>2</td>
                  <td>2 </td>
                  <td>45</td>
                  <td>2</td>
                  <td>0</td>
                  <td>0</td>
                  <td>3.29</td>
                </tr>
                <tr>
                  <td>HH Pandya</td>
                  <td>2</td>
                  <td>2 </td>
                  <td>45</td>
                  <td>2</td>
                  <td>0</td>
                  <td>0</td>
                  <td>3.29</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div className="accorTabs">
        <div className="accorHeader">
          <div className="squardList">
            <div className="squardList__content">
              <div className="squardList__list">
                <span>
                  <strong>{accortitle}</strong>
                </span>
                <span>Yet to Bat</span>
              </div>
              <div className="squardList__list">
                <span className="squardList__list__icon">
                  <ChevronRight style={{ color: 'red' }} />
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="accorCcntent" />
      </div>
    </React.Fragment>
  );
}

ScorBoard.defaultProps = {
  accortitle: 'India Innings',
};

ScorBoard.propTypes = {
  accortitle: PropTypes.string,
};

export default ScorBoard;
