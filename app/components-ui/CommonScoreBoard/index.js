/**
 *
 * CommonScoreBoard
 *
 */

import React from 'react';
import ScorBoard from './ScorBoard';
import MatchDetail from './MatchDetail';
import LiveScoreTab from './LiveScoreTab';
import SquardList from './SquadList';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function CommonScoreBoard() {
  return (
    <React.Fragment>
      <ScorBoard />
      <MatchDetail />
      <LiveScoreTab />
      <SquardList />
    </React.Fragment>
  );
}

CommonScoreBoard.propTypes = {};

export default CommonScoreBoard;
