/**
 *
 * HighLights
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import map from 'lodash/map';
// import styled from 'styled-components';
const commentryList = [
  {
    commentryTitle:
      'Aftab Alam to Balbirnie , FOUR runs, shorter ball, Balbirnie has been out there for a while now and his eye is definitely in. Pulls the ball towards the long leg boundary and it keeps running away from the fielder',
  },
  {
    commentryTitle:
      'Aftab Alam to Balbirnie , FOUR runs, shorter ball, Balbirnie has been out there for a while now and his eye is definitely in. Pulls the ball towards the long leg boundary and it keeps away from the fielder',
  },
  {
    commentryTitle:
      'Aftab Alam to Balbirnie , FOUR runs, shorter ball, Balbirnie has been out there for a while now and his eye is definitely in. Pulls the ball towards the long leg boundary and it  running away from the fielder',
  },
];
function HighLights() {
  return (
    <React.Fragment>
      <div className="commentry">
        {map(commentryList, commentryItem => (
          <div className="commentryCard">
            <div className="commentryCard__runs">
              <h5 className="overNumber__big">4</h5>
              <div className="overs">10.3 Overs</div>
            </div>
            <div className="commentryCard__detail">
              <p>{commentryItem.commentryTitle}</p>
            </div>
          </div>
        ))}
      </div>
    </React.Fragment>
  );
}

HighLights.propTypes = {
  // ballType: PropTypes.string,
  // over: PropTypes.string,
  // comment: PropTypes.string,
};

export default HighLights;
