/**
 *
 * FeaturedCard
 *
 */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import PlayArrow from '@material-ui/icons//PlayArrow';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function FeaturedCard() {
  return (
    <React.Fragment>
      <div className="featured featured--latest">
        <div className="featured__header">
          <div className="articlefull__icon">
            <svg
              className="MuiSvgIcon-root-1"
              focusable="false"
              viewBox="0 0 24 24"
              aria-hidden="true"
              role="presentation"
            >
              <path d="M3.5 18.49l6-6.01 4 4L22 6.92l-1.41-1.41-7.09 7.97-4-4L2 16.99z" />
              <path fill="none" d="M0 0h24v24H0z" />
            </svg>
          </div>
          <div className="articlefull__date articlefull__date__icon ">
            Crictec Editorial Team
            <span className="dot-middle">•</span>
            6/12/2018
          </div>
        </div>
        <div className="featured__title">
          <PlayArrow />
        </div>
        <div className="featured__footer">
          Yet Another Bohemian Rhapsody, DK - The Misfit That Refused to Fit in
        </div>
      </div>
    </React.Fragment>
  );
}

FeaturedCard.propTypes = {};

export default FeaturedCard;
