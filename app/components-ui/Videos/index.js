/**
 *
 * Ranking
 *
 */

import React from 'react';
import Reveal from 'react-reveal/Reveal';
import EmptyState from 'components/Common/EmptyState';
import Header from '../Header';
import animate from '../../utils/animationConfig';
// import FeaturedCard from '../../components-ui/FeaturedCard';
// import FeaturedArticle from '../../components-ui/FeaturedArticle';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function Videos() {
  return (
    <React.Fragment>
      <Header title="Videos" />

      <Reveal effect={animate.emptyState.effect}>
        <EmptyState
          img="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAAXNSR0IArs4c6QAAAsxJREFUaAXtW89rE0EUfm9iRGgFL4pUWus/EPVsahGUpEXoXRRPevDivyJ4Uk8iiFdB2vRQS03rVQn0WkwDVWpvtcYmdp9vRKRGspu3u5Od7czAkuzOvDfve9+bHzs7gxAzERHW6q27BHQTCEoIMBZTlRkxhG1W3CDAxZmp8SeIGOiK2E55WnrfOtfpHrwggGty6QwkENeKqnjrRnlsU0mrZ2ZV52fwKjdgNUCiK92g+1LbLma4Vm8+DAJ6JHWUFeVRPYjBMMxaYXwMIxCoIgbM8XE5Rl22iJTEgIngtC3Wi+0gOCMGLK7EMgHnAB8zQUDh1IlRE3rDdBbaONrZb38JK6PzjACuXDy7F1Vx2vnL69sI+9Fa3QvphZXmN54Pjxz2DQLuzUyfH3pYHrbB1H/3GDbhSW5PQ4+Og91/o7QfLiOdVnvn+26/CrN+7lxIe8BZh5zp+j3Dpj2ctX4jvfTs9KR4JSWpI/RQOMjo4EM6qadtl/cM285QUvucY9hIL61fOZMyIZX/sdMeSMQI4N7364EsGVIh50LaAx5SZGVWjZE2zGtiqXRa3BekvnJiBDAvAJ5Mg8L5lU/8VTbd5Ntwuv60T5tn2D5O0rXIOYaN9NImete0eHaOYQ84KnR4R9tWVBlb83kWsyVnmOijrYAi7UJoyAErfB2p2NICPMd/IwZcLU88A8S3lmLqaxYvlC9Xp8afiwFzGyYFdDtPoNnm1ePFwh1te6xxuHp18jNv1LxeW928BwHNMfhLfG/V9mFE+MqbhT/w73ylPPGYwf7ePhwLsI4b7S3+efrn0o9ykcQhnQtUIUZ6wCHOORJZnuEjQWMICFx419zgIeVCbxm9G6/3mY330h2Delhq8PUfYJs/lyRxvNLnepIoyJus0oeYeBaxljfD49qr9JRLH2JyBfTf3Tb6ENNivXWf226VvVfi43W5OMwh/crxC8+irQnzEZA3AAAAAElFTkSuQmCC"
          msg="No Series News found"
        />
      </Reveal>
    </React.Fragment>
  );
}

Videos.propTypes = {};

export default Videos;
