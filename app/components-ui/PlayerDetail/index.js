/**
 *
 * PlayerDetail
 *
 */

import React from 'react';
import ItemList from '../ItemList';
import SingleImage from '../SingleImage';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function PlayerDetail() {
  return (
    <div className="playerInfo">
      <div className="playerInfo__header">
        <div className="playerInfo__card">
          <div className="playerInfo__image">
            <SingleImage imagesource="https://qph.fs.quoracdn.net/main-qimg-764629f081bf5f9936019be07fa561d7-c" />
          </div>
          <ul>
            <li>
              <ItemList title="Role" desc="Top-order Batsman" />
            </li>
            <li>
              <ItemList title="Role" desc="Top-order Batsman" />
            </li>
            <li>
              <ItemList title="Role" desc="Top-order Batsman" />
            </li>
            <li>
              <ItemList title="Role" desc="Top-order Batsman" />
            </li>
          </ul>
        </div>
        <ul>
          <li>
            <ItemList title="Role" desc="Top-order Batsman" />
          </li>
          <li>
            <ItemList
              title="Role"
              link="India, Delhi, India Red, India Under-19s, Royal Challengers Bangalore"
            />
          </li>
        </ul>
      </div>
    </div>
  );
}

PlayerDetail.propTypes = {};

export default PlayerDetail;
