/**
 *
 * BallHighlights
 *
 */
/* eslint jsx-a11y/no-noninteractive-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */
import React from 'react';
import Slider from 'react-slick';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

function BallHighlights(props) {
  const settings = {
    dots: false,
    infinite: false,
    speed: 1000,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: false,
  };
  return (
    <div className="scheduleHighlights">
      <Slider {...settings}>
        {props &&
          props.values &&
          props.values.map(val => (
            <div
              className="scheduleHighlights__wrapper"
              role="presentation"
              onClick={e => {
                e.preventDefault();
                props.handleMatchSelect(val, 'matchType');
              }}
            >
              <div className={`${val === props.matchType && 'active'} scheduleHighlights__card`}>
                <span>{val}</span>
              </div>
            </div>
          ))}
      </Slider>
    </div>
  );
}

BallHighlights.propTypes = {
  values: PropTypes.array,
  matchType: PropTypes.string,
};

export default BallHighlights;
