/**
 *
 * Tabs
 *
 */

/* eslint jsx-a11y/no-noninteractive-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */
import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

function Tabs(props) {
  return (
    <div className="tabsSection">
      <ul>
        {props &&
          props.values &&
          props.values.map(val => (
            <li
              className={val === props.matchStatus && 'active'}
              onClick={e => {
                e.preventDefault();
                props.handleMatchSelect(val, 'matchStatus');
              }}
            >
              <span>{val}</span>
            </li>
          ))}
      </ul>
    </div>
  );
}

Tabs.propTypes = {
  values: PropTypes.array,
  matchStatus: PropTypes.string,
};

export default Tabs;
