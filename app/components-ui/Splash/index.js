/**
 *
 * Splash
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import splash from '../../images/splash.png';
import crictec from '../../images/crictec.png';

function Splash() {
  return (
    <div className="splash-section">
      <img src={splash} alt="logo" />
      <img src={crictec} alt="crcitec" />
    </div>
  );
}

Splash.propTypes = {};

export default Splash;
