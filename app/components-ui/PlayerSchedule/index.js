/**
 *
 * PlayerSchedule
 *
 */

import React from 'react';
import Header from '../Header';
import SingleImage from '../SingleImage';
import ItemList from '../ItemList';
import Tabs from '../Tabs';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function PlayerSchedule() {
  return (
    <React.Fragment>
      <Header isBgWhite />
      <div className="teamDetail">
        <section className="teamDetail__header" />
        <section className="teamDetail__wrapper">
          <div className="sectionWrapper">
            <div className="innerWrapper">
              <div className="playerInfo">
                <div className="playerInfo__header">
                  <div className="playerInfo__card">
                    <div className="playerInfo__image">
                      <SingleImage imagesource="https://qph.fs.quoracdn.net/main-qimg-764629f081bf5f9936019be07fa561d7-c" />
                    </div>
                    <ul>
                      <li>
                        <ItemList title="Role" desc="Top-order Batsman" />
                      </li>
                      <li>
                        <ItemList title="Role" desc="Top-order Batsman" />
                      </li>
                      <li>
                        <ItemList title="Role" desc="Top-order Batsman" />
                      </li>
                      <li>
                        <ItemList title="Role" desc="Top-order Batsman" />
                      </li>
                    </ul>
                  </div>
                  <ul>
                    <li>
                      <ItemList title="Role" desc="Top-order Batsman" />
                    </li>
                    <li>
                      <ItemList
                        title="Role"
                        link="India, Delhi, India Red, India Under-19s, Royal Challengers Bangalore"
                      />
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <Tabs values={['ABOUT PLAYER', 'STATISTICS', 'CAREER', 'NEWS']} />
            <div className="tabContent-section">
              <div className="articleDetail">
                <p>
                  Passionate. No word describes Virat Kohli better. His passion for cricket has made
                  him one of the best batsmen in the world across formats, and has also helped him
                  grow into a ruthless captain. Its also passion that defines Kohlis emotional,
                  effervescent and at times firecracker character. Virat Kohli does not hold back
                  and that remains his strength. Kohli approaches 30 as the most relentless and
                  popular performer in the sport. He is already a World Cup winner (2011), the
                  Player of the tournament at a World T20 (2014) and has led India to a record eight
                  consecutive Test series wins. Purely as a batsman he is already close to the
                  summit: by his 29th birthday he had more ODI hundreds (32) than anyone apart from
                  Sachin Tendulkar (49), and averages 49 or better in all three formats. He is the
                  only batsman to have achieved this feat.
                </p>
              </div>
            </div>
          </div>
        </section>
      </div>
    </React.Fragment>
  );
}

PlayerSchedule.propTypes = {};

export default PlayerSchedule;
