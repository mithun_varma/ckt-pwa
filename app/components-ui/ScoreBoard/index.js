/**
 *
 * Scoreboard
 *
 */

/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

const ScoreBoard = ({ matchStatus, onClick }) => (
  <React.Fragment>
    {matchStatus === 'upcoming' ? (
      <div onClick={onClick} className="scoreBoard scoreBoard__upcoming">
        <div className="scoreBoard__matchDetail">
          <div className="scoreBoard__header">
            <span className="scoreBoard__location">Match 2, WC19, Trent Bridge, Nottingham </span>
          </div>
          <div className="scoreBoard__teams">
            <div className="scoreBoard__playerteam">
              <div className="team-name">
                <img
                  src="https://upload.wikimedia.org/wikipedia/en/thumb/4/41/Flag_of_India.svg/1200px-Flag_of_India.svg.png"
                  alt="team first"
                />
                <span>IND</span>
              </div>
              <div className="team-name">
                <img
                  src="https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/1280px-Flag_of_the_United_Kingdom.svg.png  "
                  alt="team first"
                />
                <span>ENG</span>
              </div>
            </div>
            <div className="scoreBoard__runNeeds">England won the match by 3 wickets.</div>
          </div>
        </div>
      </div>
    ) : (
      <div onClick={onClick} className="scoreBoard">
        <div className="scoreBoard__matchDetail">
          <div className="scoreBoard__header">
            {matchStatus === 'live' && <span className="scoreBoard__live">Live</span>}
            {matchStatus === 'completed' && <span className="scoreBoard__complete">complete</span>}
            <span className="scoreBoard__location">Match 2, WC19, Trent Bridge, Nottingham </span>
          </div>
          <div className="scoreBoard__teamState">
            <div className="scoreBoard__teams">
              {['IND', 'ENG'].map(data => (
                <div className="scoreBoard__playerteam">
                  <div className="team-name">
                    <img
                      src="https://upload.wikimedia.org/wikipedia/en/thumb/4/41/Flag_of_India.svg/1200px-Flag_of_India.svg.png"
                      alt="team first"
                    />
                    <span>{data}</span>
                  </div>
                  <div className="team-score">
                    <span className="run">223/5</span>
                    <span className="over">(20 ov)</span>
                  </div>
                </div>
              ))}
            </div>
            <div className="scoreBoard__runNeeds">England won the match by 3 wickets.</div>
          </div>
        </div>

        {matchStatus === 'live' && (
          <div className="scoreBoard__predictionElement">
            <div className="prediction__winnings">
              <div className="whos-winning">
                Who’s Winning?<sup className="trademark">tm</sup>
              </div>
              <div className="team-wining">India will win by 10 runs</div>
            </div>
            <div className="predictionSection">
              <div className="name">IND</div>
              <div className="percentage">72%</div>
              <div className="progress-wrapper">
                <div className="progress progress-wrapper__bar" />
                <span className="slide-line" />
              </div>
              <div className="name">ENG</div>
              <div className="percentage">28%</div>
            </div>
          </div>
        )}

        {matchStatus === 'completed' && (
          <div className="scoreBoard__menofmatch">
            <img src="https://dummyimage.com/35x25/ffb300/fff/0011ff" alt="team first" />
            <h5>MAN OF THE MATCH</h5>
            <span>Virat Kholi</span>
          </div>
        )}
      </div>
    )}
  </React.Fragment>
);

ScoreBoard.propTypes = {
  matchStatus: PropTypes.string,
  onClick: PropTypes.func,
};

export default ScoreBoard;
