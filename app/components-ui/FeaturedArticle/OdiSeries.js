/**
 *
 * OdiSeries
 *
 */

/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */

import React from 'react';
import PropTypes from 'prop-types';
import Whatshot from '@material-ui/icons/Whatshot';
import ShowChart from '@material-ui/icons/ShowChart';
// import styled from 'styled-components';

function OdiSeries({
  articlestatus = 'news',
  seriesTitle,
  seriesThumnail,
  seriesDate,
  seriesAuthor,
  seriesSource,
  onClick,
}) {
  return (
    <React.Fragment>
      {articlestatus === 'fantasy' && (
        <div onClick={onClick} className="articlefull">
          <div className="articlefull__Content">
            <div className="articlefull__icon">
              <Whatshot />
            </div>
            <div className="articlefull__date">{seriesDate}</div>
            <span className="articlefull__title">{seriesTitle}</span>
            <h4 className="articlefull__highlights">{seriesThumnail}</h4>
            <div className="articlefull__author">
              By <span>{seriesAuthor}</span>
            </div>
          </div>
        </div>
      )}
      {articlestatus === 'news' && (
        <div onClick={onClick} className="articlefull articlefull__withImage">
          <div className="articlefull__image">
            <img src={seriesSource} alt=" " />
          </div>
          <div className="articlefull__Content">
            <div className="articlefull__icon">
              <ShowChart />
            </div>
            <div className="articlefull__date">{seriesDate}</div>
            <span className="articlefull__title">{seriesTitle}</span>
            <h4 className="articlefull__highlights">{seriesThumnail}</h4>
            <div className="articlefull__author">
              By <span>{seriesAuthor}</span>
            </div>
          </div>
        </div>
      )}
      {articlestatus === 'match' && (
        <div onClick={onClick} className="articlefull articlefull__withImage newsArticle">
          <div className="articlefull__image">
            <img src={seriesSource} alt=" " />
          </div>
          <div className="articlefull__Content">
            <div className="articlefull__date">{seriesDate}</div>
            <span className="articlefull__title">{seriesTitle}</span>
          </div>
        </div>
      )}
    </React.Fragment>
  );
}

OdiSeries.defaultProps = {
  seriesAuthor: 'Writer McAuthor',
  seriesTitle: 'India vs West Indies ODI series a chance to fine tune',
  seriesThumnail:
    'With Rohit Sharma and Shikhar Dhawan start India vs West Indies ODI series a chance to fine tune',
  seriesDate: '24/Dec/2018',
  seriesSource:
    'https://timesofindia.indiatimes.com/thumb/msid-64991904,imgsize-724360,width-400,resizemode-4/64991904.jpg',
};

OdiSeries.propTypes = {
  articlestatus: PropTypes.string,
  seriesTitle: PropTypes.string,
  seriesAuthor: PropTypes.string,
  seriesThumnail: PropTypes.string,
  seriesSource: PropTypes.string,
  seriesDate: PropTypes.string,
  onClick: PropTypes.func,
};

export default OdiSeries;
