/**
 *
 * News
 *
 */

/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */

import React from 'react';
import PropTypes from 'prop-types';

// import styled from 'styled-components';

function News({
  newstitle = 'news',
  newssource,
  newsstatus,
  newscaption,
  newsthumnail,
  newsdate,
  onClick,
}) {
  return (
    <React.Fragment>
      {newsstatus === 'news' && (
        <div onClick={onClick} className="article__news">
          <div className="article__thumbImage thumbheight">
            <img src={newssource} alt=" " />
          </div>
          <div className="article__shortContent">
            <h4 className="articlefull__highlights">{newsthumnail}</h4>
            <div className="article__descWrap">
              <div className="article__flexWrap">
                <div className="article__date">{newsdate}</div>
                <div className="article__title">{newstitle}</div>
              </div>
              <div className="article__caption">{newscaption}</div>
            </div>
          </div>
        </div>
      )}
    </React.Fragment>
  );
}

News.defaultProps = {
  newstitle: 'Star Sports',
  newsthumnail: 'India vs West Indies ODI series a chance to fine tune show',
  newsdate: '24/Dec/2018',
  newscaption: 'opinion',
  newssource: 'http://ste.india.com/sites/default/files/2017/08/21/618301-kohli-dhawan-ians.jpg',
};

News.propTypes = {
  newsstatus: PropTypes.string,
  newstitle: PropTypes.string,
  newsthumnail: PropTypes.string,
  newsdate: PropTypes.string,
  newssource: PropTypes.string,
  newscaption: PropTypes.string,
  onClick: PropTypes.func,
};

export default News;
