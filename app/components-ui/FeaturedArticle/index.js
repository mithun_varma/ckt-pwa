/**
 *
 * FeaturedArticle
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import OdiSeries from './OdiSeries';
import News from './News';
// import styled from 'styled-components';
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */

const FeaturedArticle = ({ title, source, thumbnail, date, author, onClick }) => (
  <React.Fragment>
    <div onClick={onClick} className="article__item">
      <div className="article__thumbImage">
        <img src={source} alt=" " />
      </div>{' '}
      <div className="article__shortContent">
        <div className="article__title"> {title} </div>{' '}
        <div className="article__date"> {date} </div> <h4 className="highlights"> {thumbnail} </h4>{' '}
        <div className="article__author">
          By <span> {author} </span>{' '}
        </div>{' '}
      </div>{' '}
    </div>{' '}
    <OdiSeries />
    <News />
  </React.Fragment>
);

FeaturedArticle.defaultProps = {
  title: 'Star Sports',
  author: 'Writer McAuthor',
  thumbnail: 'India vs West Indies ODI series a chance to fine tune show',
  date: '24/Dec/2018',
  source:
    'https://timesofindia.indiatimes.com/thumb/msid-64991904,imgsize-724360,width-400,resizemode-4/64991904.jpg',
};

FeaturedArticle.propTypes = {
  title: PropTypes.string,
  author: PropTypes.string,
  thumbnail: PropTypes.string,
  date: PropTypes.string,
  source: PropTypes.string,
  onClick: PropTypes.func,
};

export default FeaturedArticle;
