/**
 *
 * Series
 *
 */

import React from 'react';
import Header from '../../Header';
import BallHighlights from '../../Tabs/BallHighlights';
import SquadList from '../../CommonScoreBoard/SquadList';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function Series() {
  return (
    <React.Fragment>
      <Header title="Series" leftIcon="arrowBack" />
      <main className="wrapperContainer">
        <section className="sectionWrapper">
          <BallHighlights values={['All', 'International', 'T20', 'Domestic', 'WOMENS']} />
          <div className="seriesDate">
            <h5>SEPTEMBER 2018</h5>
          </div>
          <div className="sectionWrapper__card sectionWhite">
            <SquadList
              teamList={[
                {
                  teamItem: {
                    playerName: 'Australia vs Pakistan in UAE, 2018',
                    playerType: 'Sept 9- Oct 28',
                    arrowIcon: true,
                  },
                },
              ]}
            />
            <SquadList
              teamList={[
                {
                  teamItem: {
                    playerName: 'Windies Tour of India, 2018',
                    playerType: 'Sept 29 - Nov 11',
                    arrowIcon: true,
                  },
                },
              ]}
            />
          </div>
        </section>
        <section className="sectionWrapper">
          <div className="seriesDate">
            <h5>OCTOBER 2018</h5>
          </div>
          <div className="sectionWrapper__card sectionWhite">
            <SquadList
              teamList={[
                {
                  teamItem: {
                    playerName: 'Australia vs Pakistan in UAE, 2018',
                    playerType: 'Sept 9- Oct 28',
                    arrowIcon: true,
                  },
                },
              ]}
            />
            <SquadList
              teamList={[
                {
                  teamItem: {
                    playerName: 'Windies Tour of India, 2018',
                    playerType: 'Sept 29 - Nov 11',
                    arrowIcon: true,
                  },
                },
              ]}
            />
            <SquadList
              teamList={[
                {
                  teamItem: {
                    playerName: 'Australia vs Pakistan in UAE, 2018',
                    playerType: 'Sept 9- Oct 28',
                    arrowIcon: true,
                  },
                },
              ]}
            />
            <SquadList
              teamList={[
                {
                  teamItem: {
                    playerName: 'Windies Tour of India, 2018',
                    playerType: 'Sept 29 - Nov 11',
                    arrowIcon: true,
                  },
                },
              ]}
            />
          </div>
        </section>
      </main>
    </React.Fragment>
  );
}

Series.propTypes = {};

export default Series;
