/**
 *
 * Home
 *
 */

import React from 'react';
import Slider from 'react-slick';
import Header from '../../Header';
import ScoreBoard from '../../ScoreBoard';
import OdiSeries from '../../FeaturedArticle/OdiSeries';
import FeaturedArticle from '../../FeaturedArticle';
// import BottomHeader from '../../BottomHeader';
import News from '../../FeaturedArticle/News';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function Home() {
  const settings = {
    dots: true,
    infinite: true,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 2000,
    arrows: false,
    centerMode: true,
    centerPadding: 18,
    appendDots: dots => (
      <div>
        <ul className="slider-dots-wrapper">{dots}</ul>
      </div>
    ),
  };
  return (
    <React.Fragment>
      <Header />
      {/* Scorecard */}
      {/* Match Stats Slider block  START */}
      <Slider {...settings}>
        <ScoreBoard matchStatus="live" />
        <ScoreBoard matchStatus="live" />
        <ScoreBoard matchStatus="live" />
        <ScoreBoard matchStatus="live" />
      </Slider>
      {/* Match Stats Slider block END */}
      <main className="wrapperContainer">
        {/* Article */}
        <section className="sectionWrapper">
          <div className="sectionWrapper__title">
            <h5>Analysis</h5>
          </div>
          <div className="sectionWrapper__card">
            <OdiSeries articlestatus="fantasy" />
          </div>
          <div className="sectionWrapper__card">
            <FeaturedArticle />
          </div>
          <div className="sectionWrapper__card">
            <FeaturedArticle source="https://130513-387449-raikfcquaxqncofqfm.stackpathdns.com/wp-content/uploads/2017/12/Jasprit-Bumrah.jpg" />
          </div>
          <div className="sectionWrapper__card">
            <FeaturedArticle source="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsR1MGtuLigvcu1N94xaLToNHdKXoQ3dBfvwsh-b00YLN3Ovx5" />
          </div>
          <div className="sectionWrapper__link">
            <a href>View All Analysis</a>
          </div>
        </section>
        {/* News Opinion */}
        <section className="sectionWrapper">
          <div className="sectionWrapper__title">
            <h5>News & Opinion</h5>
          </div>
          <div className="sectionWrapper__card">
            <News
              newsstatus="news"
              newssource="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSe6JHGQuWwFGcVwHviewfnR_eeBGag_rVszcujUH9t8S8wTjnP"
            />
            <News
              newsstatus="news"
              newssource="http://a.espncdn.com/combiner/i?img=/media/motion/cricinfo/2015/1231/cric_151231_MFC-SuzieBates-Dec31/cric_151231_MFC-SuzieBates-Dec31.jpg?w=960&h=540"
            />
            <News
              newsstatus="news"
              newssource="http://ste.india.com/sites/default/files/2017/08/21/618301-kohli-dhawan-ians.jpg"
            />
          </div>
          <div className="sectionWrapper__card">
            {/* News & Opinion Slider block START */}
            <Slider {...settings} dots={false}>
              <OdiSeries
                articlestatus="news"
                seriesSource="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLYjgfLNYcoDNUtZX1TzQjfUoUACflRdk7Oq3fOhsAoxyXSwx9"
              />
              <OdiSeries
                articlestatus="news"
                seriesSource="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLYjgfLNYcoDNUtZX1TzQjfUoUACflRdk7Oq3fOhsAoxyXSwx9"
              />
              <OdiSeries
                articlestatus="news"
                seriesSource="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLYjgfLNYcoDNUtZX1TzQjfUoUACflRdk7Oq3fOhsAoxyXSwx9"
              />
            </Slider>
            {/* News & Opinion Slider block END */}
          </div>
          <div className="sectionWrapper__link">
            <a href>View All News & Opinions</a>
          </div>
        </section>
        {/* Match Reports */}
        <section className="sectionWrapper">
          <div className="sectionWrapper__title">
            <h5>Match Reports</h5>
          </div>
          <div className="sectionWrapper__card">
            <div className="sectionWrapper__card">
              {/* Match Reports Slider block START */}
              <Slider {...settings} dots={false} centerPadding={70}>
                <OdiSeries
                  articlestatus="match"
                  seriesSource="https://media.gqindia.com/wp-content/uploads/2018/02/under-19-top-image-new-866x487.jpg"
                />
                <OdiSeries
                  articlestatus="match"
                  seriesSource="https://media.gqindia.com/wp-content/uploads/2018/02/under-19-top-image-new-866x487.jpg"
                />
                <OdiSeries
                  articlestatus="match"
                  seriesSource="https://media.gqindia.com/wp-content/uploads/2018/02/under-19-top-image-new-866x487.jpg"
                />
              </Slider>
              {/* Match Reports Slider block START */}
            </div>
          </div>
          <div className="sectionWrapper__card">
            <FeaturedArticle source="https://image.redbull.com/rbcom/010/2014-04-04/1331643720852_2/0100/0/1/the-third-generation-cricketer-shubham-ranjhane.jpg" />
          </div>
          <div className="sectionWrapper__card">
            <FeaturedArticle souce="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSiZ7qyXSFnIXvEWyItdSuAQofSGeBzeJho7UlJL4B_85CVj_h27Q" />
          </div>
          <div className="sectionWrapper__card">
            <FeaturedArticle source="https://akm-img-a-in.tosshub.com/indiatoday/images/story/201702/sweepfb-story_647_020817054745.jpg" />
          </div>
          <div className="sectionWrapper__link">
            <a href>View All Match Reports</a>
          </div>
        </section>
      </main>
      {/* bottom header */}
      {/* <BottomHeader /> */}
    </React.Fragment>
  );
}

Home.propTypes = {};

export default Home;
