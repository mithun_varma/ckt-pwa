/**
 *
 * ArticleDetail
 *
 */

/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */

import React from 'react';
import Header from '../../Header';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function ArticleDetail() {
  return (
    <React.Fragment>
      <Header />
      <div className="articleDetail">
        <div className="articleDetail__header">
          <img
            src="http://statelywallpaper.com/wp-content/uploads/2017/11/Leagend-Man-of-Cricket-Virat-Kohli-1600x900.jpg"
            alt="virat"
          />
        </div>
        <div className="articleDetail__body">
          <div className="articleDetail__card">
            <div className="articleDetail__wrap">
              <button type="button" className="articleDetail__tags">
                News
              </button>
              <span className="articleDetail__date">20/04/2018</span>
              <h5>India vs West Indies ODI series a chance to fine-tune World Cup plans</h5>
              <span className="articleDetail__author">
                By<a href="#top">Loren Grush@lorengrush.com</a>
              </span>
              <span>
                With Rohit Sharma and Shikhar Dhawan the openers, there are four candidates vying
                two spots: numbers four and six.
              </span>
            </div>
            <p>
              In July 2014, M Vijay made a priceless third innings 95 at Lords. It was a triumph of
              self-denial and the highest point of his best phase as a cricketer. From December 2013
              to January 2015, India played all their Test cricket away from home - in South Africa,
              New Zealand, England and Australia. In July 2014, M Vijay made a priceless
              third-innings 95 at Lords. It was a triumph of self-denial and the highest point of
              his best phase as a cricketer. From December 2013 to January 2015, India played all
              their Test cricket away from home - in South Africa, New Zealand, England and
              Australia.In July 2014, M Vijay made a priceless third innings 95 at Lords. It was a
              triumph of self-denial and the highest point of his best phase as a cricketer. From
              December 2013 to January 2015, India played all their Test cricket away from home - in
              South Africa, New Zealand, England and Australia.
            </p>
            <p>
              In July 2014, M Vijay made a priceless third-innings 95 at Lords. It was a triumph of
              self-denial and the highest point of his best phase as a cricketer. From December 2013
              to January 2015, India played all their Test cricket away from home - in South Africa,
              New Zealand, England and Australia.In July 2014, M Vijay made a priceless third
              innings innings innings 95 at Lords. It was a triumph of self-denial and the highest
              highest highest point of his best phase as a cricketer. From December 2013 to January
              their Test cricket away from home - in South Africa, New Zealand, England and
              Australia. In July 2014, M Vijay made a priceless third-innings 95 at Lords. It was a
              triumph of self-denial and the highest point of his best phase as a cricketer. From
              December 2013 to January 2015, India played all their Test cricket away from home - in
              South Africa, England and Australia.In July 2014, M Vijay made a priceless third
              innings 95 at Lords. It was a triumph of self-denial and the highest point of his best
              phase as a cricketer. From December 2013 to January 2015, India played all their Test
              cricket away from home - in South Africa, New Zealand, England and Australia. In July
              2014, M Vijay made a priceless third-innings 95 at Lords. It was a triumph of
              self-denial and the highest
            </p>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

ArticleDetail.propTypes = {};

export default ArticleDetail;
