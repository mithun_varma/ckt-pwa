/**
 *
 * More
 *
 */

import React from 'react';
import Header from '../../Header';
import SquadList from '../../CommonScoreBoard/SquadList';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

// import BottomHeader from 'components-ui/BottomHeader';

const More = () => (
  <React.Fragment>
    <Header />
    <main className="wrapperContainer">
      {/* Article */}
      <section className="teamMore-section">
        {[
          { title: 'Series', route: '/series' },
          { title: 'News Opinions', route: '/news-opinion' },
          { title: 'Match Reports', route: '/match-reports' },
          { title: 'Rankings', route: '/rankings' },
          { title: 'Records', route: '/records' },
          { title: 'Teams', route: '/teams' },
          { title: 'Players', route: '/players' },
          { title: 'Grounds', route: '/grounds' },
          { title: 'Photos', route: '/photos' },
          { title: 'Videos', route: '/videos' },
        ].map(obj => (
          <SquadList
            teamList={[
              {
                teamItem: {
                  playerName: obj.title,
                  arrowIcon: true,
                  route: obj.route,
                },
              },
            ]}
          />
        ))}
      </section>
    </main>
    {/* bottom header */}
    {/* <BottomHeader
      // {...this.props.history}
      tabs={[
        { key: 'home', route: '/' },
        { key: 'schedule', route: 'schedule' },
        { key: 'more', route: 'more' },
      ]}
    /> */}
  </React.Fragment>
);

More.propTypes = {};

export default More;
