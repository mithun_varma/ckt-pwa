/**
 *
 * SeriesDetail
 *
 */

import React from 'react';
import Header from '../../Header';
import Tabs from '../../Tabs';
import ScoreBoard from '../../ScoreBoard';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function SeriesDetail() {
  return (
    <React.Fragment>
      <Header title="Series Detail" />
      <main>
        <section className="sectionWrapper">
          <div className="sectionWrapper__card">
            <Tabs values={['Matches', 'Squads', 'News', 'VENUES', 'STATS']} />
          </div>
        </section>
        <div className="wrapperContainer">
          <section className="sectionWrapper">
            <div className="seriesDate">
              <h5>SEPTEMBER 2018</h5>
            </div>
            <div className="sectionWrapper__card">
              <ScoreBoard matchStatus="live" />
            </div>
          </section>
          <section className="sectionWrapper">
            <div className="seriesDate">
              <h5>24th SEPTEMBER, 2018</h5>
            </div>
            <div className="sectionWrapper__card">
              <ScoreBoard matchStatus="completed" />
            </div>
          </section>
        </div>
      </main>
    </React.Fragment>
  );
}

SeriesDetail.propTypes = {};

export default SeriesDetail;
