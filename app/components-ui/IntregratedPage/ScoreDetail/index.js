/**
 *
 * ScoreDetail
 *
 */

import React from 'react';
import Header from '../../Header';
import ScoreBoard from '../../ScoreBoard';
import Tabs from '../../Tabs';
import LiveScoreTab from '../../CommonScoreBoard/LiveScoreTab';
import HighLights from '../../CommonScoreBoard/HighLights';
import ScorBoard from '../../CommonScoreBoard/ScorBoard';
import Commentary from '../../Commentary';
import FantacyBoard from '../../FantacyBoard';
import SquadList from '../../CommonScoreBoard/SquadList';

// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function ScoreDetail() {
  return (
    <React.Fragment>
      <Header />
      {/* Scorecard */}
      <ScoreBoard matchStatus="live" />
      {/* tabs */}
      <Tabs
        values={[
          'LIVE',
          'HIGHLIGHTS',
          'SCOREBOARD',
          'COMMENTARY',
          'FEATURED ARTICLES',
          'PLAYING 11',
        ]}
      />

      <main className="wrapperContainer">
        {/* Live Tabs Content */}
        <section className="sectionWrapper">
          <div className="tabContent-section">
            <LiveScoreTab />
          </div>
        </section>

        {/* Highlights Tab COntent */}
        <section className="sectionWrapper">
          <div className="tabContent-section">
            <HighLights />
          </div>
        </section>

        {/* Highlights Tab COntent */}
        <section className="sectionWrapper">
          <div className="tabContent-section no_style">
            <ScorBoard />
          </div>
        </section>

        {/* Highlights Tab COntent */}
        <section className="sectionWrapper">
          <div className="tabContent-section">
            <Commentary />
          </div>
        </section>

        {/* Highlights Tab COntent */}
        <section className="sectionWrapper">
          <div className="tabContent-section">
            <FantacyBoard />
          </div>
        </section>

        {/* Highlights Tab COntent */}
        <section className="sectionWrapper">
          <div className="tabContent-section">
            <SquadList />
          </div>
        </section>
      </main>

      {/* BottomHeader */}
      {/* <BottomHeader /> */}
    </React.Fragment>
  );
}

ScoreDetail.propTypes = {};

export default ScoreDetail;
