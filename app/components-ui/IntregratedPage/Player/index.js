/**
 *
 * Player
 *
 */

import React from 'react';
import Header from '../../Header';
import PlayerDetail from '../../PlayerDetail';
import Tabs from '../../Tabs';
import FeaturedArticle from '../../FeaturedArticle';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function Player() {
  return (
    <React.Fragment>
      <Header />

      <main className="wrapperContainer bg_white">
        <section className="sectionWrapper">
          <div className="sectionWrapper__card">
            <div className="innerWrapper">
              <PlayerDetail />
            </div>
            <Tabs values={['ABOUT PLAYER', 'STATISTICS', 'CAREER', 'NEWS']} />
          </div>
          <div className="sectionWrapper__card">
            <FeaturedArticle source="https://130513-387449-raikfcquaxqncofqfm.stackpathdns.com/wp-content/uploads/2017/12/Jasprit-Bumrah.jpg" />
          </div>
          <div className="sectionWrapper__card">
            <FeaturedArticle source="https://130513-387449-raikfcquaxqncofqfm.stackpathdns.com/wp-content/uploads/2017/12/Jasprit-Bumrah.jpg" />
          </div>
          <div className="sectionWrapper__card">
            <FeaturedArticle source="https://130513-387449-raikfcquaxqncofqfm.stackpathdns.com/wp-content/uploads/2017/12/Jasprit-Bumrah.jpg" />
          </div>
          <div className="sectionWrapper__card">
            <FeaturedArticle source="https://130513-387449-raikfcquaxqncofqfm.stackpathdns.com/wp-content/uploads/2017/12/Jasprit-Bumrah.jpg" />
          </div>
        </section>
      </main>
    </React.Fragment>
  );
}

Player.propTypes = {};

export default Player;
