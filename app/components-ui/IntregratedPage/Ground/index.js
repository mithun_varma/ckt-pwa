/**
 *
 * Ground
 *
 */

import React from 'react';
import Header from '../../Header';
import SingleImage from '../../SingleImage';
import GroundState from '../../GroundState';
import Tabs from '../../Tabs';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function Ground() {
  return (
    <React.Fragment>
      <Header />
      <main className="wrapperContainer">
        <section className="sectionWrapper">
          <div className="sectionWrapper__card">
            <div className="innerWrapper">
              <SingleImage imagesource="http://sth.india.com/hindi/sites/default/files/2015/09/07/88001-virat-kohli.jpg" />
            </div>
            <Tabs values={['ABOUT VENUE', 'STATISTICS']} />
            <div className="innerWrapper">
              <div claaaName="groundState">
                <div className="d-flex">
                  <div className="w-49">
                    <GroundState title="Run" score="110" series="Test" />
                  </div>
                  <div className="w-49">
                    <GroundState title="AVG. SECOND INNINGS SCORE" score="110" series="Test" />
                  </div>
                </div>
                <div className="d-flex">
                  <GroundState title="Run" score="110" series="Test" />
                </div>
                <div className="d-flex">
                  <div className="w-49">
                    <GroundState title="MATCHES WON BATTING FIRST" score="110" series="Test" />
                  </div>
                  <div className="w-49">
                    <GroundState title="Run" score="110" series="Test" />
                  </div>
                </div>
                <div className="d-flex">
                  <GroundState title="Run" score="110" series="Test" />
                </div>
                <div className="d-flex">
                  <div className="w-49">
                    <GroundState title="Run" score="110" series="Test" />
                  </div>
                  <div className="w-49">
                    <GroundState title="Run" score="110" series="Test" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </React.Fragment>
  );
}

Ground.propTypes = {};

export default Ground;
