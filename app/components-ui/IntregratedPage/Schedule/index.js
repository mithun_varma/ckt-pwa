/**
 *
 * Schedule
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import BottomHeader from 'components-ui/BottomHeader';
import Header from '../../Header';
import Tabs from '../../Tabs';
import ScoreBoard from '../../ScoreBoard';
import BallHighlights from '../../Tabs/BallHighlights';
// import styled from 'styled-components';

export class Schedule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      matchStatus: 'ongoing',
      matchType: 'all',
    };
  }

  handleMatchSelect = (val, type) => {
    this.setState({
      [type]: val,
    });
  };

  render() {
    return (
      <React.Fragment>
        <Header />
        <div className="scoreboardWrapper">
          <div className="sectionWrapper__card">
            <Tabs
              values={['ongoing', 'upcoming', 'completed']}
              handleMatchSelect={this.handleMatchSelect}
              matchStatus={this.state.matchStatus}
            />
          </div>
        </div>
        <div className="scoreboardWrapper">
          <div className="sectionWrapper__card">
            <BallHighlights
              values={['all', 'international', 'domestic', 'women', 'men']}
              handleMatchSelect={this.handleMatchSelect}
              matchType={this.state.matchType}
            />
          </div>
        </div>
        <main className="wrapperContainer">
          <section className="sectionWrapper__card">
            <ScoreBoard matchStatus="live" />
          </section>
          <section className="sectionWrapper__card">
            <ScoreBoard matchStatus="upcoming" />
          </section>
          <section className="sectionWrapper__card">
            <ScoreBoard matchStatus="upcoming" />
          </section>
          <section className="sectionWrapper__card">
            <ScoreBoard matchStatus="upcoming" />
          </section>
          <section className="sectionWrapper__card">
            <ScoreBoard matchStatus="upcoming" />
          </section>
        </main>
        {/* bottom header */}
        <BottomHeader
          {...this.props.history}
          tabs={[
            { key: 'home', route: '/' },
            { key: 'schedule', route: 'schedule' },
            { key: 'more', route: 'more' },
          ]}
        />
      </React.Fragment>
    );
  }
}

Schedule.propTypes = {
  history: PropTypes.object,
};

export default Schedule;
