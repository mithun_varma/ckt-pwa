/**
 *
 * SingleImage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

function SingleImage({ imagesource }) {
  return (
    <div className="singleImage">
      <div className="singleImage__card">
        <img src={imagesource} alt=" " />
      </div>
    </div>
  );
}

SingleImage.propTypes = {
  imagesource: PropTypes.string,
};

export default SingleImage;
