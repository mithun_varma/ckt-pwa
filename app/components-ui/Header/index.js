/**
 *
 * Header
 *
 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */

import React from 'react';
import BlurCircular from '@material-ui/icons/BlurCircular';
import ArrowBack from '@material-ui/icons/ArrowBack';
import Search from '@material-ui/icons//Search';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

const Header = ({ leftIcon, leftIconOnClick, rightIcon, rightIconOnClick, title }) => (
  <div className="header">
    {leftIcon && (
      <div className="header__icons" onKeyPress={leftIconOnClick}>
        {leftIcon === 'arrowBack' ? <ArrowBack /> : <BlurCircular />}
      </div>
    )}
    {title && <div className="header__title">{title}</div>}
    {rightIcon && (
      <div className="header__icons" onKeyPress={rightIconOnClick}>
        <Search />
      </div>
    )}
  </div>
);
Header.defaultProps = {
  leftIcon: 'arrow-back',
  title: 'crictec',
  rightIcon: 'arrow-back',
};

Header.propTypes = {
  leftIcon: PropTypes.string,
  leftIconOnClick: PropTypes.func,
  title: PropTypes.string,
  rightIcon: PropTypes.string,
  rightIconOnClick: PropTypes.string,
};

export default Header;
