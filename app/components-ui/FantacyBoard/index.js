/**
 *
 * FantacyBoard
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function FantacyBoard() {
  return (
    <div className="table-responsive">
      <table className="table">
        <thead>
          <tr>
            <th>Rank</th>
            <th>Players</th>
            <th>Batting</th>
            <th>Bowling</th>
            <th>Fielding</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Jayden Wilkerson</td>
            <td>9 </td>
            <td>9</td>
            <td>9</td>
            <td>27</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Jacob McGuire</td>
            <td>9 </td>
            <td>9</td>
            <td>9</td>
            <td>27</td>
          </tr>
          <tr>
            <td>3</td>
            <td>Herbert Ramos</td>
            <td>9 </td>
            <td>9</td>
            <td>9</td>
            <td>27</td>
          </tr>
          <tr>
            <td>4</td>
            <td>Fred Hicks</td>
            <td>9</td>
            <td>9</td>
            <td>9</td>
            <td>27</td>
          </tr>
          <tr>
            <td>5</td>
            <td>Roger McDaniel</td>
            <td>9</td>
            <td>9</td>
            <td>9</td>
            <td>27</td>
          </tr>
          <tr>
            <td>6</td>
            <td>Trevor Graham</td>
            <td>9</td>
            <td>9</td>
            <td>9</td>
            <td>27</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

FantacyBoard.propTypes = {};

export default FantacyBoard;
