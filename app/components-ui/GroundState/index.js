/**
 *
 * GroundState
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

function GroundState({ title, score, series }) {
  return (
    <div className="groundState__card">
      <h5>{title}</h5>
      <h4>{score}</h4>
      <span>{series}</span>
    </div>
  );
}

GroundState.propTypes = {
  title: PropTypes.string,
  score: PropTypes.number,
  series: PropTypes.string,
};

export default GroundState;
