/**
 *
 * PlayersList
 *
 */

import React from 'react';
import Header from './../Header';
import TeamCard from './../TeamCard';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function PlayersList() {
  return (
    <React.Fragment>
      <Header title="players" />
      <main>
        <div className="wrapperContainer">
          <section className="sectionWrapper">
            <div className="playerTeam">
              <div className="d-flex">
                <TeamCard title="India" />
                <TeamCard title="India" />
                <TeamCard title="India" />
                <TeamCard title="India" />
                <TeamCard title="India" />
                <TeamCard title="India" />
              </div>
            </div>
          </section>
        </div>
      </main>
    </React.Fragment>
  );
}

PlayersList.propTypes = {};

export default PlayersList;
