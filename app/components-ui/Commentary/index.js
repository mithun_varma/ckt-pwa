/**
 *
 * Commentary
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import map from 'lodash/map';
// import styled from 'styled-components';
const commentryList = [
  {
    commentryTitle:
      'Aftab Alam to Balbirnie , FOUR runs, shorter ball, Balbirnie has been out there for a while now and his eye is definitely in. Pulls the ball towards the long leg boundary and it keeps running away from the fielder',
  },
  {
    commentryTitle:
      'Aftab Alam to Balbirnie , FOUR runs, shorter ball, Balbirnie has been out there for a while now and his eye is definitely in. Pulls the ball towards the long leg boundary and it keeps running away from the fielder',
  },
  {
    commentryTitle:
      'Aftab Alam to Balbirnie , FOUR runs, shorter ball, Balbirnie has been out there for a while now and his eye is definitely in. Pulls the ball towards the long leg boundary and it keeps running away from the fielder',
  },
];
function Commentary() {
  return (
    <React.Fragment>
      <div className="commentry">
        {map(commentryList, commentryItem => (
          <div className="commentryCard">
            <div className="commentryCard__runs">
              <h5 className="overNumber__big">4</h5>
              <div className="overs">10.3 Overs</div>
            </div>
            <div className="commentryCard__detail">
              <p>{commentryItem.commentryTitle}</p>
            </div>
          </div>
        ))}
      </div>
      <div className="overHighlights">
        <div className="over">
          <div className="over__run">12</div>
          <span className="over__text">over</span>
        </div>
        <div className="score">
          <div className="score__run">
            <span className="cric-name">Runs Scored</span>
            <ul>
              <li>
                <span>10 |</span>
              </li>
              <li>1</li>
              <li>6</li>
              <li>1</li>
              <li>2</li>
              <li>w</li>
              <li>0</li>
            </ul>
            <div className="score__detail">
              <span className="cric-name">M Shami</span>
              <span className="cric-run">4-0-20-2</span>
            </div>
          </div>
          <div className="score__team">
            <span className="cric-name">ENG:</span>
            <span className="cric-run">142/4</span>
            <div className="score__detail">
              <span className="cric-name">A Cook</span>
              <span className="cric-run">22(11)</span>
            </div>
            <div className="score__detail">
              <span className="cric-name">J Bairstow</span>
              <span className="cric-run">22(11)</span>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

Commentary.propTypes = {
  // ballType: PropTypes.string,
  // over: PropTypes.string,
  // comment: PropTypes.string,
};

export default Commentary;
