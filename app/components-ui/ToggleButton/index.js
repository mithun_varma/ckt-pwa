/**
 *
 * ToggleButton
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function ToggleButton() {
  return (
    <React.Fragment>
      <div className="toogleButton">
        <ul>
          <li className="active">Ind</li>
          <li>ENG</li>
        </ul>
      </div>
      <div className="wicketFall-Card">
        <ul>
          <li>
            Yet to Bat:
            <ul className="full-wicket">
              <li> DM de Silva,</li>
              <li>NLTC Perera, MD Shanaka,</li>
              <li> A Dananjaya,</li>
              <li> PVD Chameera,</li>
              <li>CAK Rajitha,</li>
              <li>PADLR Sandakan</li>
              <li>NLTC Perera, MD Shanaka,</li>
              <li> A Dananjaya,</li>
              <li> PVD Chameera,</li>
              <li>CAK Rajitha,</li>
              <li>PADLR Sandakan</li>
            </ul>
          </li>
          <li>
            Fall of wickets:
            <ul className="full-wicket">
              <li>1-137 (S Samarawickrama, 19.1 ov),</li>
              <li>2-168 (N Dickwella, 25.4 ov)</li>
            </ul>
          </li>
        </ul>
      </div>
    </React.Fragment>
  );
}

ToggleButton.propTypes = {};

export default ToggleButton;
