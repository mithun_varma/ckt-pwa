/* eslint-disable no-underscore-dangle */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/**
 *
 * FeaturedCard
 *
 */
/* eslint-disable jsx-a11y/anchor-is-valid */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PlayArrow from '@material-ui/icons//PlayArrow';
import moment from 'moment';
import { AUTHOR_DATE_FORMAT } from 'utils/constants';

export class FeaturedVideoCard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      videos: { featuredVideo },
      history,
    } = this.props;
    return (
      <div
        className="featured featured--latest"
        onClick={() => history.push(`/video/${featuredVideo[0].title}/${featuredVideo[0]._id}`)}
      >
        <div className="featured__header">
          <div className="articlefull__icon">
            <svg
              className="MuiSvgIcon-root-1"
              focusable="false"
              viewBox="0 0 24 24"
              aria-hidden="true"
              role="presentation"
            >
              <path d="M3.5 18.49l6-6.01 4 4L22 6.92l-1.41-1.41-7.09 7.97-4-4L2 16.99z" />
              <path fill="none" d="M0 0h24v24H0z" />
            </svg>
          </div>
          <div className="articlefull__date articlefull__date__icon ">
            {`${featuredVideo[0].relatedName || 'Featured Video'} ${
              featuredVideo[0].season ? featuredVideo[0].season.name : ''
            }`}
            <span className="dot-middle">•</span>
            {featuredVideo.length > 0 &&
              moment(featuredVideo[0].publishedAt).format(AUTHOR_DATE_FORMAT)}
          </div>
        </div>
        {/* <div className="featured__title">
            <PlayArrow />
          </div> */}
        <div className="articlefull__image">
          <div className="featured__title">
            <PlayArrow />
          </div>
        </div>

        <div className="featured__footer">{featuredVideo[0].title}</div>
        <img src={featuredVideo[0].thumbnail} alt="Featured Video thumbnail" />
      </div>
    );
  }
}

FeaturedVideoCard.propTypes = {
  videos: PropTypes.object,
  history: PropTypes.object,
};

export default FeaturedVideoCard;
