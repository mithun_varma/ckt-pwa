/* eslint-disable no-unused-expressions */
/* eslint-disable no-underscore-dangle */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-unused-vars */
/**
 *
 * FeaturedCard
 *
 */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import PlayArrow from '@material-ui/icons//PlayArrow';
import PropTypes from 'prop-types';
import moment from 'moment';
import { AUTHOR_DATE_FORMAT } from 'utils/constants';

export class OtherVideos extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { videos, history } = this.props;
    return (
      <div
        className="featured featured--videos"
        onClick={() => {
          this.props.getVideoDetails
            ? this.props.getVideoDetails(videos._id)
            : history.push(`/video/${videos.title}/${videos._id}`);
        }}
      >
        <div className="featured__header">
          <div className="featured__title">
            <PlayArrow />
          </div>

          <div className="articlefull__date articlefull__date__icon ">
            {moment(videos.publishedAt).format(AUTHOR_DATE_FORMAT)}
          </div>
        </div>

        <div className="featured__footer">
          <span>{videos.title}</span>
        </div>
        <img src={videos.thumbnail} alt="other Video thumbnail" />
      </div>
    );
  }
}

OtherVideos.propTypes = {
  videos: PropTypes.object,
  history: PropTypes.object,
  getVideoDetails: PropTypes.func,
};

export default OtherVideos;
