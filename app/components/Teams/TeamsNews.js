/* eslint-disable global-require */
/**
 *
 * Header
 *
 */
/* eslint-disable indent */
/* eslint camelcase: 0 */

import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import InfiniteScroll from 'react-infinite-scroll-component';

import Reveal from 'react-reveal/Reveal';
import EmptyState from 'components/Common/EmptyState';
import Loader from 'components/Common/Loader';
import ArticleCard from 'components/Articles/ArticleCard';
import FeatureArticleCard from 'components/Articles/FeatureArticleCard';
import animate from '../../utils/animationConfig';

class TeamsNews extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      initial: true,
    };
    this.newsSlideSettings = {
      dots: false,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      centerMode: true,
      // centerPadding: 18,
    };
    this.loadMoreItems = this.loadMoreItems.bind(this);
  }

  componentDidMount() {
    this.props.fetchTeamNews({ teamId: this.props.teamId, skip: 0, limit: 10 });
  }

  // static getDerivedStateFromProps(props, state) {
  //   if (state.initial)
  //     return {
  //       initial: false,
  //     };
  //   return null;
  // }

  UNSAFE_componentWillReceiveProps = () => {
    if (this.state.initial) {
      this.setState({
        initial: false,
      });
    }
  };

  loadMoreItems = () => {
    this.props.fetchTeamNews({
      seriesId: this.props.teamId,
      limit: 10,
      skip: this.props.teams.teamNews.length,
    });
  };

  cardOnClick = path => {
    this.props.history.push(path);
  };

  getCardType = type => {
    if (type === 'match-report') {
      return 'matchReport';
    } else if (type === 'analysis') {
      return 'fantasyAnalysis';
    }
    return 'newsOpinion';
  };

  render() {
    const { teams } = this.props;
    return (
      <div className="wrapperContainer">
        {teams.teamNewsLoading && teams.teamNews.length === 0 ? (
          <Loader styles={{ height: '75px' }} noOfLoaders={5} />
        ) : (
          <React.Fragment>
            {/* Slider section */}
            {/* No Need to Display Slider in Team News, Added  false to do not map */}
            {false &&
              teams.teamNewsFeatured.length > 0 && (
                <div className="sectionWrapper__card">
                  {teams.teamNewsFeatured.length === 1 ? (
                    <FeatureArticleCard
                      article={teams.teamNewsFeatured[0]}
                      cardType={this.getCardType(teams.teamNewsFeatured[0].type)}
                      articleHost={teams.articleHost}
                      onClick={this.cardOnClick}
                    />
                  ) : (
                    <Slider
                      {...this.newsSlideSettings}
                      autoplay={false}
                      centerPadding={teams.teamNewsFeatured.length > 1 ? 18 : 0}
                      className={teams.teamNewsFeatured.length > 1 && 'multiple-articles'}
                    >
                      {teams.teamNewsFeatured.map((article, key) => (
                        <FeatureArticleCard
                          article={article}
                          cardType={this.getCardType(article.type)}
                          articleHost={teams.articleHost}
                          key={`${key + 1}`}
                          onClick={this.cardOnClick}
                        />
                      ))}
                    </Slider>
                  )}
                </div>
              )}
            {/* Infinite croll Section */}
            {teams.teamNews.length > 0 && (
              <InfiniteScroll
                dataLength={teams.teamNews.length}
                next={this.loadMoreItems}
                loader={
                  teams.teamNews.length >= 10 && (
                    <Loader styles={{ height: '150px' }} noOfLoaders={5} />
                  )
                }
                hasMore={teams.teamNewsOldLength !== teams.teamNews.length}
              >
                {teams.teamNews.map((article, articleKey) => (
                  <div className="sectionWrapper__card" key={`${articleKey + 1}`}>
                    {/* {article.filters && article.filters.featured ? (
                      <FeatureArticleCard
                        article={article}
                        cardType={this.getCardType(article.type)}
                        articleHost={teams.articleHost}
                        onClick={this.cardOnClick}
                      />
                    ) : ( */}
                    <ArticleCard
                      article={article}
                      // cardType={this.getCardType(article.type)}
                      cardType="analysis"
                      articleHost={teams.articleHost}
                      onClick={this.cardOnClick}
                    />
                    {/* )} */}
                  </div>
                ))}
              </InfiniteScroll>
            )}
            {/* Empty State */}
            {teams.teamNewsFeatured.length === 0 &&
              teams.teamNews.length === 0 && (
                <Reveal effect={animate.emptyState.effect}>
                  <EmptyState
                    img={require('../../images/no_articles.png')}
                    msg="No Teams News found"
                  />
                </Reveal>
              )}
          </React.Fragment>
        )}
      </div>
    );
  }
}

TeamsNews.propTypes = {
  fetchTeamNews: PropTypes.func,
  teamId: PropTypes.string,
  teams: PropTypes.object,
  history: PropTypes.object,
};

export default TeamsNews;
