/* eslint-disable */

import React from 'react';
import PropTypes from 'prop-types';
import EmptyState from 'components/Common/EmptyState';
import Loader from 'components/Common/Loader';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import Reveal from 'react-reveal/Reveal';
import TeamCard from './TeamCard';

// import styled from 'styled-components';

// function TeamList({ img, title, onClick }) {
class TeamList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      arrowUp: true,
      cardActive: true,
    };
  }

  toggleCard = () => {
    this.setState({ arrowUp: !this.state.arrowUp, cardActive: !this.state.cardActive });
  };

  cardClick = (e, id) => {
    e.preventDefault();
    this.props.cardClick(e, id);
  };

  render() {
    return (
      <div className="scoreboardWrapper">
        <div className="wrapperContainer">
          <div className="tabContent-section no_style">
            <div role="tabpanel" className="tab-pane fade" id="scoreboard">
              <div className="accordion score-accordion">
                <div className="card">
                  <div
                    className="accorHeader"
                    onClick={e => {
                      e.preventDefault();
                      // this.toggleCard();
                    }}
                  >
                    <div className="squardList" style={{ borderBottom: 0 }}>
                      <div className="squardList__content">
                        <div className="squardList__list">
                          <span>
                            <strong>{this.props.title}</strong>
                          </span>
                        </div>
                        <div className="squardList__list">
                          {false && (
                            <span className="squardList__list__icon">
                              {this.state.arrowUp ? (
                                <ExpandMore style={{ color: 'red' }} />
                              ) : (
                                <ExpandLess style={{ color: 'red' }} />
                              )}
                            </span>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div className="accorCcntent">
                      {/* style={{ display: this.state.cardActive ? 'block' : 'none' }} */}
                      <div className="table-responsive">
                        <div className="d-flex teamcard-section">
                          {this.props.teamData.map((list, key) => (
                            <TeamCard
                              key={`${key + 1}`}
                              img={list.image || require('../../images/Player_Placeholder.png')}
                              title={list.displayName}
                              onClick={e => this.cardClick(e, list._id)}
                              iterationId={key}
                              barColor={list.color}
                            />
                          ))}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

TeamList.defaultProps = {
  source: 'http://ste.india.com/sites/default/files/2017/08/21/618301-kohli-dhawan-ians.jpg',
};

TeamList.propTypes = {
  img: PropTypes.string,
  title: PropTypes.string,
  cardClick: PropTypes.func,
  teamData: PropTypes.obj,
};

export default TeamList;
