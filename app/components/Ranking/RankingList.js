/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

class RankingList extends Component {
  render() {
    const { ranking, type } = this.props;
    return (
      <div className="playerDetail" onClick={() => this.props.redirectTo(ranking)}>
        <div className="playerDetail__left">
          <div className="playerDetail__img">
            <img src={ranking.image || ranking.avatar} alt="List Image" />
          </div>
          <div className="playerDetail__name">
            <span className="text-bb">{ranking.displayName}</span>
            {type !== 'team' && <span className="text-reg">{ranking.nationality || '-'}</span>}
          </div>
        </div>
        <div className="playerDetail__right">
          <div className="playerDetail__rating">
            <span className="text-reg">{ranking.rating}</span>
          </div>
        </div>
      </div>
    );
  }
}

RankingList.propTypes = {
  ranking: PropTypes.object,
  type: PropTypes.string,
  redirectTo: PropTypes.func,
};

export default RankingList;
