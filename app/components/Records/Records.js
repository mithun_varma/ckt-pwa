import React, { Component } from 'react';
import EmptyState from '../Common/EmptyState';
import Header from '../Common/Header';

export class Records extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <Header title="Records" showLogo />
        <EmptyState msg="No Career Found" />
      </div>
    );
  }
}

export default Records;
