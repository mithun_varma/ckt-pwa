/**
 *
 * GroundsList
 *
 */

import React from "react";
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import BackgroundComponent from "../Common/BackgroundComponent";
import ImageContainer from "../Common/ImageContainer";

/* eslint-disable react/prefer-stateless-function */
class GroundsList extends React.Component {
  render() {
    return (
      <div>
        <BackgroundComponent title={"Stadiums"} />
        <ImageContainer data={popular} isAvatar />
        <ImageContainer
          rootStyles={{
            marginTop: 12
          }}
          data={trending}
        />
        <ImageContainer
          rootStyles={{
            marginTop: 12
          }}
          data={similarGrounds}
          // poweredStadium
        />
        <ImageContainer
          rootStyles={{
            marginTop: 12
          }}
          data={mostMatches}
        />
        <ImageContainer
          rootStyles={{
            marginTop: 12
          }}
          data={highestBatting}
        />
        <ImageContainer
          rootStyles={{
            marginTop: 12
          }}
          data={highestBowling}
        />
        <ImageContainer
          rootStyles={{
            marginTop: 12
          }}
          data={favouriteStatium}
        />
      </div>
    );
  }
}

GroundsList.propTypes = {};

export default GroundsList;

const popular = {
  header: "Popular Stadiums",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: require("../../images/place_holder_1.png")
    },
    {
      name: "MCG, Sydney",
      img: require("../../images/place_holder_1.png")
    },
    {
      name: "Eden Gardens, Kolkata",
      img: require("../../images/place_holder_1.png")
    },
    {
      name: "MCG, Sydney",
      img: require("../../images/place_holder_1.png")
    },
    {
      name: "Eden Gardens, Kolkata",
      img: require("../../images/place_holder_1.png")
    }
  ]
};

const trending = {
  header: "Trending Stadiums",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    }
  ]
};
const similarGrounds = {
  header: "Stadiums Similar to Eden Gardens",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    }
  ]
};

const mostMatches = {
  header: "Stadium with Most Matches",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    }
  ]
};

const highestBatting = {
  header: "Stadiums with Highest Batting S/R",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    }
  ]
};

const highestBowling = {
  header: "Stadiums with Highest Bowling S/R",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    }
  ]
};

const favouriteStatium = {
  header: "Virat favourite Stadiums",
  content: [
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "MCG, Sydney",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    },
    {
      name: "Eden Gardens, Kolkata",
      img: "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
    }
  ]
};
