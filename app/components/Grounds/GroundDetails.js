import React, { Component } from "react";
import BackgroundComponent from "../Common/BackgroundComponent";

export class GroundDetails extends Component {
  render() {
    return (
      <div>
        <BackgroundComponent
          rootStyles={{
            height: 144
          }}
          title="Eden Garden, Kolkata"
        />
        <div
          style={{
            margin: "0 16px",
            marginTop: -84,
            borderRadius: 3,
            background: "lightgrey",
            height: 203
          }}
        >
          <img
            src={
              "https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
            }
            alt="Ground Image"
            style={{
              width: "100%",
              height: "100%"
            }}
          />
        </div>
        {/* Detail Card */}
        <div
          style={{
            background: "#FFF",
            margin: "10px 16px 0",
            borderRadius: 3,
            padding: "0 16px"
          }}
        >
          <div
            style={{
              padding: "12px 0px",
              background: "#FFF",
              fontFamily: "Montserrat",
              fontWeight: 500,
              fontSize: 12,
              color: "#141b2f"
            }}
          >
            Stadium Details
          </div>

          <div style={pageStyle.hrLine} />
          {[1, 2, 3, 4].map(item => (
            <div>
              <div
                style={{
                  display: "flex",
                  flex: 1,
                  padding: "12px 0",
                  fontSize: 12,
                  fontFamily: "Montserrat",
                  fontWeight: 400
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flex: 0.5,
                    color: "#f76b1c"
                  }}
                >
                  Lable
                </div>
                <div
                  style={{
                    display: "flex",
                    flex: 0.5,
                    paddingLeft: 10,
                    color: "#141b2f"
                  }}
                >
                  value
                </div>
              </div>
              <div style={pageStyle.hrLine} />
            </div>
          ))}
        </div>
        {/* closing */}

        {/* Match format card */}
        <div
          style={{
            margin: "12px 16px",
            borderRadius: 3,
            background: "#FFF"
          }}
        >
          <div className={"tabsSection"}>
            <ul style={{ borderRadius: 3 }}>
              {["International", "Domestic", "IPL", "Women"].map(val => (
                <li
                  role="presentation"
                  className={val == "International" ? "active" : "inactive"}
                  key={val}
                >
                  <span>{val}</span>
                </li>
              ))}
            </ul>
          </div>
          <div style={pageStyle.hrLine} />
          <div style={{ display: "flex", padding: "18px 0" }}>
            <div
              className="buttonTab"
              style={{
                display: "inline-block",
                margin: "0 auto",
                border: "1px solid #e3e4e6",
                // padding: "4px 0p",
                fontFamily: "Montserrat",
                fontWeight: 400,
                fontSize: 10,
                color: "#a1a4ac",
                background: "#fbfbfc",
                borderRadius: 2
              }}
            >
              <div
                style={{
                  display: "inline-block",
                  padding: "8px 24px",
                  borderRight: "1px solid #e3e4e6"
                }}
              >
                TEST
              </div>
              <div
                style={{
                  display: "inline-block",
                  padding: "8px 24px",
                  borderRight: "1px solid #e3e4e6"
                }}
              >
                ODI
              </div>
              <div style={{ display: "inline-block", padding: "8px 24px" }}>
                T20
              </div>
            </div>
          </div>
          <div style={pageStyle.hrLine} />
          <div
            style={{
              display: "flex",
              flex: 1,
              flexDirection: "column",
              padding: "0 16px"
            }}
          >
            <div
              style={{
                display: "flex",
                flex: 1
              }}
            >
              <div style={{ flex: 0.5 }}>label 1</div>
              <div style={{ flex: 0.5 }}>value 1</div>
            </div>
            <div
              style={{
                display: "flex",
                flex: 1
              }}
            >
              <div style={{ flex: 0.5 }}> label 2</div>
              <div style={{ flex: 0.5 }}>value 2</div>
            </div>
          </div>
        </div>
        {/* Closing */}
      </div>
    );
  }
}

export default GroundDetails;

const pageStyle = {
  hrLine: {
    height: 1,
    background: "linear-gradient(to right, #ffffff, #e3e4e6, #e3e4e6, #ffffff)"
  }
};
