/* eslint-disable indent */
/* eslint-disable no-nested-ternary */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint global-require: 0 */

import React from 'react';
import PropTypes from 'prop-types';
import Reveal from 'react-reveal/Reveal';
import { distanceInWordsToNow } from 'date-fns';
import moment from 'moment';
import teamFlagPlaceholder from '../../images/team-flag-placeholder.svg';
// import manOftheMatch from '../../images/badge.svg';
import animate from '../../utils/animationConfig';

const liveStyle = {
  width: '40px',
  color: '#141414',
  fontSize: '11px',
  marginRight: '5px',
  fontFamily: 'robotoBold',
  textTransform: 'uppercase',
  boxSizing: 'border-box',
  paddingLeft: '8px',
};

const inningTeam = score => {
  const inningTeamArray = {};
  const teamWiseInings = [];
  score.battingOrder.map(inning => {
    inningTeamArray[inning[0]] = inningTeamArray[inning[0]] || [];
    if (inning[0] !== 'superover') return inningTeamArray[inning[0]].push(inning);
    return null;
  });
  score.battingOrder.map(
    inning => (teamWiseInings.indexOf(inning[0]) === -1 ? teamWiseInings.push(inning[0]) : null),
  );
  return { inningTeamArray, teamWiseInings };
};
const scoreDisplay = status => {
  const Status = (live, venue, league, score, prediction, matchStatus, detailPage) => ({
    live,
    venue,
    league,
    score,
    prediction,
    matchStatus,
    detailPage,
  });
  switch (status) {
    case 'started':
      return Status(true, true, true, true, true, true, true);
    case 'notstarted':
      return Status(false, true, true, false, true, true, false);
    case 'completed':
      return Status(false, true, true, true, false, true, false);
    default:
      return Status(false, true, true, false, true, true, false);
  }
};
const teamOrder = (teams, status, score) => {
  switch (status) {
    case 'started':
      return {
        first: score.firstBatting,
        second: teams.indexOf(score.firstBatting) === 1 ? teams[0] : teams[1],
      };
    case 'notstarted':
      return {
        first: teams[0],
        second: teams[1],
      };
    case 'completed':
      return {
        first: score.firstBatting,
        second: teams.indexOf(score.firstBatting) === 1 ? teams[0] : teams[1],
      };
    default:
      return {
        first: teams[0],
        second: teams[1],
      };
  }
};

class ScoreCard extends React.Component {
  // shouldComponentUpdate = (nextProps, nextState) => {
  //   if(nextProps.score)
  // }

  getBallColor(ball) {
    switch (ball) {
      case '6':
        return 'six';
      case '4':
        return 'four';
      case 'w':
        return 'wicket';
      default:
        return 'default';
    }
  }
  componentDidMount = () => {
    // if (
    //   this.props.score &&
    //   this.props.pollScoreCardDetails &&
    //   this.props.score.status === 'started'
    // ) {
    //   setTimeout(() => {
    //     this.props.pollScoreCardDetails({ data: this.props.score, id: this.props.score.id });
    //   }, 1000);
    // }
  };
  // shouldComponentUpdate = nextProps => !this.props.score.equals(nextProps.score);
  render() {
    const { score, teamName, onClick, isScoreDetails, isSchedule } = this.props;
    if (!score || !score.teams) {
      return null;
    }

    const teams = Object.keys(score.teams);
    const teamInnings = inningTeam(score);
    const { status } = score;
    const scorecard = scoreDisplay(status);
    const teamorder = teamOrder(teams, status, score);
    const firstBatting = (key, index) =>
      score.innings[`${teamorder.first}_${teamInnings.inningTeamArray[teamorder.first][index][1]}`][
        key
      ];
    const liveBatting = (key, index) =>
      score.innings[
        `${score.now.battingTeam}_${teamInnings.inningTeamArray[score.now.battingTeam][index][1]}`
      ][key];
    const secondBatting = (key, index) =>
      score.innings[
        `${teamorder.second}_${teamInnings.inningTeamArray[teamorder.second][index][1]}`
      ][key];
    const runs = (Batting, order) => (
      <React.Fragment>
        {teamInnings.inningTeamArray[teamorder[order]] && (
          <React.Fragment>
            {teamInnings.inningTeamArray[teamorder[order]].length === 1 ? (
              <div className="team-score">
                <Reveal effect={animate.scoreCard.run.effect}>
                  <span className="run">
                    {Batting('runs', 0)}/{Batting('wickets', 0)}
                  </span>
                  <span className="over">({` ${Batting('overs', 0)}`} Ov)</span>
                </Reveal>
              </div>
            ) : (
              <div className="team-score">
                <span className="innings">
                  {Batting('runs', 0)}/{Batting('wickets', 0)}
                </span>
                <span className="run">
                  &nbsp;
                  {`& ${Batting('runs', 1)}/${Batting('wickets', 1)}`}
                </span>
                <span className="over">({` ${Batting('overs', 1)}`} Ov)</span>
              </div>
            )}
          </React.Fragment>
        )}
      </React.Fragment>
    );

    return (
      <div
        onClick={() => {
          // if (status === 'started' || (status === 'completed' && score.key)) {
          onClick(
            `/details/${teamorder.first}-vs-${teamorder.second}/${
              score.startDate.str
                ? score.startDate.str.replace(/ /gi, '-')
                : moment(score.startDate.iso).format('DD-MMM-YYYY-HH:MM Z')
            }/${score.key}`,
            {
              data: score,
              id: score.key,
            },
          );
          // }
        }}
        className={`${status === 'scheduled' ? 'scoreBoard scoreBoard__upcoming' : 'scoreBoard'} ${
          this.props.className
        }`}
      >
        <div className="scoreBoard__matchDetail">
          <div className="scoreBoard__header">
            {status === 'started' && (
              <React.Fragment>
                <span className="circle-ripple" />
                <span style={liveStyle}>Live</span>
              </React.Fragment>
            )}
            {/* {status === 'completed' && <span className="scoreBoard__complete">completed</span>} */}
            {/* {status === 'notstarted' && <span className="scoreBoard__complete">Upcoming</span>} */}

            {/* {scorecard.venue && <div className="scoreBoard__location">{score.venue}</div>} */}
            <div className="scoreBoard__location">
              {scorecard.live
                ? isSchedule
                  ? `${score.relatedName}`
                  : `${score.relatedName}, ${score.season && score.season.name}`
                : `${score.relatedName}, ${score.venue}`}
            </div>
          </div>
          <div
            className={`scoreBoard__teamState ${
              isSchedule && status === 'notstarted' ? 'scoreBoard__teamUpcoming' : ''
            }`}
          >
            <div className="scoreBoard__teams">
              {teamName === undefined || status === 'completed' ? (
                <div className="scoreBoard__playerteam">
                  <div className="team-name">
                    <Reveal effect={animate.scoreCard.run.effect}>
                      <img
                        src={
                          (score.teams[teamorder.first] && score.teams[teamorder.first].avatar) ||
                          teamFlagPlaceholder
                        }
                        alt={score.teams[teamorder.first] && score.teams[teamorder.first].name}
                      />
                      <span>
                        {teamName ? teamorder.first.toUpperCase() : teamorder.first.toUpperCase()}
                      </span>
                    </Reveal>
                  </div>
                  {runs(firstBatting, 'first')}
                </div>
              ) : (
                <div className="score-scoreBoard__teams">
                  <div className="team-name">
                    <Reveal effect={animate.scoreCard.image.effect}>
                      <img
                        src={
                          (score.teams[score.now.battingTeam] &&
                            score.teams[score.now.battingTeam].avatar) ||
                          teamFlagPlaceholder
                        }
                        alt={
                          score.teams[score.now.battingTeam] &&
                          score.teams[score.now.battingTeam].name
                        }
                      />
                      <span>
                        {teamName
                          ? score.now.battingTeam.toUpperCase()
                          : score.now.battingTeam.toUpperCase()}
                      </span>
                    </Reveal>
                  </div>
                  {runs(
                    liveBatting,
                    score.now.battingTeam === teamorder.first ? 'first' : 'second',
                  )}
                </div>
              )}

              {teamName === undefined || status === 'completed' ? (
                <React.Fragment>
                  <div className="scoreBoard__playerteam">
                    <div className="team-name">
                      <Reveal effect={animate.scoreCard.image.effect}>
                        <img
                          src={
                            (score.teams[teamorder.second] &&
                              score.teams[teamorder.second].avatar) ||
                            teamFlagPlaceholder
                          }
                          alt={score.teams[teamorder.second] && score.teams[teamorder.second].name}
                        />
                        <span>{teamorder.second.toUpperCase()}</span>
                      </Reveal>
                    </div>
                    {scorecard.score && runs(secondBatting, 'second')}
                  </div>
                </React.Fragment>
              ) : (
                <React.Fragment />
              )}
            </div>
            {scorecard.matchStatus &&
            score &&
            score.statusStr &&
            score.statusStr !== 'null' &&
            score.statusStr !== '' ? (
              <div className="scoreBoard__runNeeds">{score.statusStr}</div>
            ) : (
              // <div className="scoreBoard__runNeeds" />
              score &&
              score.startDate &&
              score.startDate.iso && (
                <div className="scoreBoard__runNeeds">{`${moment(score.startDate.iso).format(
                  'ddd, MMM DD, YYYY | H:mm',
                )} ${window.moment().tz(window.moment.tz.guess(true)).format('z')}`}</div>
              )
            )}
          </div>
        </div>
        {!isScoreDetails &&
          !score.manOfTheMatch &&
          scorecard.live !== 'live' &&
          score.prediction && (
            <div className="scoreboard-prediction">
              <div className="scoreBoard__predictionElement">
                <div className="prediction__winnings">
                  <div className="whos-winning">
                    {'Who’s Winning?'}
                    <sup className="trademark">tm</sup>
                  </div>
                  <div className="team-wining">{score.prediction.predictionMargin}</div>
                </div>
                {score.prediction.current && (
                  <div className="predictionSection">
                    <div className="name">
                      {score.prediction.team1_name
                        ? score.prediction.team1_name.slice(0, 3).toUpperCase()
                        : ''}
                    </div>
                    <div className="percentage">
                      {score.prediction.current.team1_notie_percent}%
                    </div>
                    <div className="progress-wrapper">
                      <div className="progress progress-wrapper__bar" />
                      <span className="slide-line" />
                    </div>
                    <div className="percentage">
                      {`${score.prediction.current.team2_notie_percent}%`}
                    </div>
                    <div className="name">
                      {score.prediction.team2_name
                        ? score.prediction.team2_name.slice(0, 3).toUpperCase()
                        : ''}
                    </div>
                  </div>
                )}
              </div>
            </div>
          )}

        {!isScoreDetails &&
          (score.manOfTheMatch ? (
            <div className="scoreBoard__menofmatch">
              <img
                src={
                  score.players[score.manOfTheMatch].avatar ||
                  require('../../images/Player_Profile_Round.png')
                }
                alt="team first"
              />
              <Reveal effect={animate.scoreCard.mom.effect}>
                <h5>MAN OF THE MATCH</h5>
                {score.manOfTheMatch && <span>{score.players[score.manOfTheMatch].fullname}</span>}
              </Reveal>
            </div>
          ) : status !== 'started' && status !== 'completed' && !isSchedule ? (
            <div className="scoreBoard__menofmatch">
              {/* <img src="https://dummyimage.com/35x25/ffb300/fff/0011ff" alt="team first" /> */}
              <img
                style={{ width: '24px', height: '24px' }}
                src={require('../../images/clock - Material Design Icons.png')}
                alt="team first"
              />
              <h5 style={{ marginRight: 5 }}>{`${
                score.startDate.iso >= new Date().toISOString()
                  ? distanceInWordsToNow(score.startDate.iso, {
                      includeSeconds: true,
                    })
                  : 'about to start'
              }`}</h5>
              <span>{`${score.startDate.iso >= new Date().toISOString() ? 'to go' : ''}`}</span>
            </div>
          ) : (
            <div />
          ))}
      </div>
    );
  }
}

ScoreCard.propTypes = {
  score: PropTypes.object,
  teamName: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
  isScoreDetails: PropTypes.bool,
  isSchedule: PropTypes.bool,
};
export default ScoreCard;
