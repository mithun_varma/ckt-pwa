/**
 *
 * Header
 *
 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */
/* eslint-disable react/no-danger */

import React from 'react';
import PropTypes from 'prop-types';

const BallSummary = ({ info }) =>
  info ? (
    <div className="overHighlights">
      <div className="over">
        <div className="over__run">{info.over}</div>
        <span className="over__text">over</span>
      </div>
      <div className="score">
        {info.team && (
          <div className="score__team">
            <span className="cric-name">{info.team.short_name}:</span>
            <span className="cric-run">
              {info.match.runs}/{info.match.wicket}
            </span>
            {info.batsman &&
              info.batsman.length > 0 &&
              info.batsman.map((batsman, key) => (
                <div className="score__detail" key={`key: ${key + 1}`}>
                  <span className="cric-name">{batsman.batsmanDetail.name}</span>
                  <span className="cric-run">{` ${batsman.batting.runs}(${
                    batsman.batting.balls
                  })`}</span>
                </div>
              ))}
          </div>
        )}
        <div className="score__run">
          <span className="cric-name">Runs Scored</span>
          <ul>
            <li>
              <span className="totalRunsOvr">{info.runs}</span>
            </li>
            {info.thisOver && info.thisOver.length > 0 && info.thisOver.map(run => <li>{run}</li>)}
          </ul>
          <div className="score__detail">
            <span className="cric-name">{info.bowler[0].bowlerDetail.name}</span>
            <span className="cric-run">{` ${Number(info.bowler[0].bowling.overs)}-${
              info.bowler[0].bowling.maiden_overs
            }-${info.bowler[0].bowling.runs}-${info.bowler[0].bowling.wickets}`}</span>
          </div>
        </div>
      </div>
    </div>
  ) : (
    <div />
  );

BallSummary.propTypes = {
  info: PropTypes.object,
};

export default BallSummary;
