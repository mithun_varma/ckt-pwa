/* eslint-disable global-require */
/**
 *
 * Header
 *
 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable dot-notation */
/* eslint-disable no-nested-ternary */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/prefer-stateless-function */
/* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */
/* eslint indent: 0 */

import React from 'react';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroll-component';
import Reveal from 'react-reveal/Reveal';

import * as scoreDetailsActions from 'containers/ScoreDetails/actions';
import Loader from '../Common/Loader';
import EmptyState from '../Common/EmptyState';
import CommentaryCard from './CommentaryCard';
import BallSummary from './BallSummary';
import ScrollTags from '../Common/ScrollTags';
import animate from '../../utils/animationConfig';

const TAGS_CONFIG = [
  { label: 'All', key: 'all', filters: ['wicket', 'four', 'six', 'milestone'] },
  { label: 'Wickets', key: 'wicket', filters: ['wicket'] },
  { label: "4's", key: 'four', filters: ['four'] },
  { label: "6's", key: 'six', filters: ['six'] },
  { label: 'Milestones', key: 'milestone', filters: ['milestone'] },
];
class Commentary extends React.Component {
  setTime = '';
  state = {
    lastData: [],
    loading: true,
    loader: false,
    loaders: true,
  };

  componentDidMount = () => {
    if (this.props.activeTab === 'HIGHLIGHTS') {
      this.props.getCommentaryData({
        params: { matchId: this.props.matchId, highlights: TAGS_CONFIG[0].filters },
      });
      this.setState({
        lastData: this.props.data.values,
      });
    }
  };
  static getDerivedStateFromProps(props, state) {
    if (props.data.lastFetched !== state.lastFetched) {
      return {
        lastData: props.data.values,
        lastFetched: props.data.lastFetched,
        loader: false,
        loaders: false,
      };
    }
    return null;
  }
  handelActive = (activeKey, highlights) => {
    this.props.changeActiveKey(activeKey);
    this.setState(
      {
        loading: true,
        loaders: true,
        lastData: [],
        highlightsFilter: activeKey,
      },
      () => {
        clearTimeout(this.settime);
        this.settime = setTimeout(() => {
          this.props.commentaryStopPollingReq();
          setTimeout(() => {
            this.props.getCommentaryData({ params: { matchId: this.props.matchId, highlights } });
          }, 50);
        }, 50);
      },
    );
  };
  componentWillUnmount = () => {
    setTimeout(() => {
      this.props.commentaryStopPollingReq();
    }, 100);
  };

  fetchMoreData = () => {
    this.setState({
      loading: true,
      loader: true,
    });
    if (this.props.activeTab === 'COMMENTARY') {
      this.props.dispatch(
        scoreDetailsActions.getPagingCommentaryData({
          params: {
            matchId: this.props.match.params.matchId,
            limit: 10,
            lastId: this.props.data.values[this.props.data.values.length - 1]._id,
          },
          isCommentry: true,
        }),
      );
    } else if (this.props.activeTab === 'HIGHLIGHTS') {
      this.props.dispatch(
        scoreDetailsActions.getPagingCommentaryData({
          params: {
            limit: 10,
            // skip: this.props.data.values.length,
            lastId: this.props.data.values[this.props.data.values.length - 1]._id,
            matchId: this.props.match.params.matchId,
            highlights: this.props.data.filters.highlights,
          },
        }),
      );
    }
  };

  render() {
    if (this.props.data && this.props.data.values) {
      const data = this.state.lastData;
      return (
        <React.Fragment>
          {this.props.activeTab === 'HIGHLIGHTS' && (
            <Reveal effect={animate.commentary.tags.effect}>
              <ScrollTags
                tags={TAGS_CONFIG}
                activeKey={this.props.activeKey}
                onTagClick={this.handelActive}
              />
            </Reveal>
          )}
          <div className="wrapperContainer">
            <Reveal
              effect={animate.commentary.card.effect}
              duration={animate.commentary.card.effect}
            >
              <div className="tabContent-section">
                <div className="commentry">
                  {/* Components: Over description */}
                  <div className="container__inner">
                    {data.length > 0 ? (
                      <InfiniteScroll
                        dataLength={data.length}
                        next={this.fetchMoreData}
                        hasMore={this.state.loading}
                        loader={
                          this.state.loader ? (
                            <Loader styles={{ height: '75px' }} noOfLoaders={5} />
                          ) : this.props.activeTab === 'COMMENTARY' ? (
                            <Reveal effect={animate.emptyState.effect}>
                              {/* <EmptyState msg="No more commentary found d" /> */}
                            </Reveal>
                          ) : (
                            <Reveal effect={animate.emptyState.effect}>
                              {/* <EmptyState
                                msg={`No more ${
                                  this.props.activeKey === 'all'
                                    ? 'highlights'
                                    : `${this.props.activeKey}`
                                } Found`}
                              /> */}
                            </Reveal>
                          )
                        }
                        height={500}
                      >
                        {data.map(item => {
                          const { wicket } = item.info;
                          if (item.highlights && item.highlights[0] === 'over-summary') {
                            return <BallSummary info={item.info} />;
                          }
                          return (
                            <CommentaryCard
                              key={item._id}
                              ball={wicket ? 'w' : item.info.runs}
                              comment={item.comments}
                              overs={item.info.over_str}
                            />
                          );
                        })}
                      </InfiniteScroll>
                    ) : this.state.loaders ? (
                      <Loader styles={{ height: '75px' }} noOfLoaders={4} />
                    ) : data &&
                    data.length === 0 &&
                    (this.props.activeTab === 'COMMENTARY' ||
                      this.props.activeTab === 'HIGHLIGHTS') ? (
                      this.state.highlightsFilter === 'milestone' ? (
                        <EmptyState
                          img={require('../../images/no_highlights.png')}
                          msg="No Commentary Found"
                        />
                      ) : (
                        <Loader styles={{ height: '75px' }} noOfLoaders={4} />
                      )
                    ) : this.props.activeTab === 'COMMENTARY' ? (
                      <Reveal effect={animate.emptyState.effect}>
                        <EmptyState
                          img={require('../../images/no_commentary.png')}
                          msg="No Commentary Found"
                        />
                      </Reveal>
                    ) : (
                      <Reveal effect={animate.emptyState.effect}>
                        <EmptyState
                          img={require('../../images/no_highlights.png')}
                          msg={`No ${
                            this.props.activeKey === 'all' ? 'highlights' : this.props.activeKey
                          } Found`}
                        />
                      </Reveal>
                    )}
                  </div>
                  {/* end */}
                </div>
              </div>
            </Reveal>
          </div>
        </React.Fragment>
      );
    }
    return <div />;
  }
}

Commentary.propTypes = {
  data: PropTypes.object.isRequired,
  getCommentaryData: PropTypes.func.isRequired,
  matchId: PropTypes.string.isRequired,
  dispatch: PropTypes.any.isRequired,
  commentaryStopPollingReq: PropTypes.func.isRequired,
  changeActiveKey: PropTypes.func.isRequired,
  activeTab: PropTypes.string.isRequired,
  match: PropTypes.any.isRequired,
  activeKey: PropTypes.any.isRequired,
};

export default Commentary;
