/**
 *
 * Header
 *
 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable dot-notation */

import React from 'react';
import PropTypes from 'prop-types';
import PillTabs from '../Common/PillTabs';
// import ChevronRight from '@material-ui/icons/ChevronRight';

class PlayingSquad extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 0,
    };
    this.onTabChange = this.onTabChange.bind(this);
  }
  componentDidMount() {
    // this.props.fetchPlayingSquad({ matchId: this.props.matchId });
  }
  onTabChange = activeTab => {
    this.setState({ activeTab });
  };
  render() {
    const { score } = this.props;

    if ((!score && !score.players) || !score.teams || !score.battingOrder) return <div />;
    const teamKeys = Object.keys(score.teams);
    const firstTeam = score.firstBatting;
    const secondTeam = teamKeys.indexOf(score.firstBatting) === 1 ? teamKeys[0] : teamKeys[1];
    const activeTeam = this.state.activeTab ? secondTeam : firstTeam;
    return (
      <React.Fragment>
        <PillTabs
          items={[firstTeam, secondTeam]}
          activeItem={this.state.activeTab}
          onClick={this.onTabChange}
        />

        {score.teams[activeTeam].playingXi &&
          score.teams[activeTeam].playingXi.map(playerId => {
            const player = score.players[playerId];
            return (
              <div
                className="squardList"
                onClick={() => {
                  // team.teamItem.route && redirectTo(team.teamItem.route);
                }}
              >
                <div className="squardList__content">
                  <div className="squardList__list">
                    {player.name && (
                      <span>
                        <strong>{player.name}</strong>
                      </span>
                    )}
                  </div>
                </div>
              </div>
            );
          })}
      </React.Fragment>
    );
  }
}

PlayingSquad.propTypes = {
  score: PropTypes.object,
};

export default PlayingSquad;
