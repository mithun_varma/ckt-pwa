/**
 *
 * Header
 *
 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable dot-notation */

import React from 'react';
import PropTypes from 'prop-types';

class LiveScore extends React.Component {
  componentDidMount() {}
  getBallColor(ball) {
    switch (ball) {
      case '6':
        return 'sixball';
      case '4':
        return 'fourball';
      case 'w':
        return 'wicketball';
      default:
        return '';
    }
  }
  /* eslint-disable-next-line consistent-return */
  strikeDetails = (score, playerType) => {
    if (playerType === 'bowler' && score.now.bowler) {
      const player = score.now['bowler'];
      const details = score.players[player];
      const { bowling } = details.innings[score.now.innings];
      return (
        <React.Fragment key={player}>
          <tr>
            <td>{details.name}</td>
            <td>{bowling.overs || 0}</td>
            <td>{bowling.balls || 0}</td>
            <td>{bowling.runs || 0}</td>
            <td>{bowling.wickets || 0}</td>
            <td>{bowling.strikeRate || 0}</td>
          </tr>
        </React.Fragment>
      );
    } else if (playerType === 'batsmen') {
      /* eslint-disable-next-line consistent-return */
      /* eslint-disable-next-line array-callback-return */
      const data = ['striker', 'nonStriker'].map((key, index) => {
        if (!score.now[key]) return null;
        const player = score.now[key];
        const details = score.players[player];
        const { batting } = details.innings[score.now.innings];
        return (
          <React.Fragment key={player}>
            <tr key={`${index + 1}`}>
              <td>{`${details.name} ${key === 'striker' ? '*' : ''}`}</td>
              <td>{batting.runs || 0}</td>
              <td>{batting.balls || 0}</td>
              <td>{batting.fours || 0}</td>
              <td>{batting.sixers || 0}</td>
              <td>{batting.strikeRate || 0}</td>
            </tr>
          </React.Fragment>
        );
      });
      return data;
    }
  };
  overs = (balls, index) => {
    if (balls) {
      let val = 0;
      const over = balls;
      const ballArray = balls
        .split(/(,|;)/)
        .filter(Ball => (Ball === ',' || Ball === ';' ? null : Ball));
      ballArray.map(ball => {
        if (ball[0] === 'e' || ball[0] === 'r' || ball[0] === 'b') val += Number(ball[1]);
        return null;
      });
      if (over.length <= 2) {
        return (
          <li
            key={index}
            className={`overNumber ${
              over[0] === 'r' || over[0] === 'b'
                ? this.getBallColor(over[1])
                : this.getBallColor(over)
            }`}
          >
            {over[0] === 'r' || over[0] === 'b' ? over[1] : over}
          </li>
        );
      }
      return (
        <li key={index} className={`overNumber}`}>
          {`${val === 0 || val === 1 ? '' : val}${
            ballArray[ballArray.length - 1] === 'by' ? 'b' : ballArray[ballArray.length - 1]
          }`}
        </li>
      );
    }
    return null;
  };
  render() {
    const { score } = this.props;
    const strikeBatsmen = this.strikeDetails(score, 'batsmen');
    const strikeBowler = this.strikeDetails(score, 'bowler');
    if (!(score && score.now)) {
      return <div />;
    }
    return (
      <React.Fragment>
        <table className="table live-table">
          <thead>
            <tr>
              <th>BATSMEN</th>
              <th>R</th>
              <th>B</th>
              <th>4s</th>
              <th>6s</th>
              <th>SR</th>
            </tr>
          </thead>
          <tbody>
            {strikeBatsmen}
            {score.now.bowler && (
              <React.Fragment>
                <tr>
                  <th>BOWLERS</th>
                  <th>O</th>
                  <th>M</th>
                  <th>R</th>
                  <th>W</th>
                  <th>ECON</th>
                </tr>
                {strikeBowler}
              </React.Fragment>
            )}
          </tbody>
        </table>
        <div className="liveScore">
          <div className="liveScore__card">
            {score.now.req && Object.keys(score.now.req).length > 0 ? (
              <React.Fragment>
                <span>
                  CRR: <strong>{score.now.runRate && score.now.runRate}</strong>
                </span>
                <span>
                  RRR: <strong>{score.now.req.requiredRunRate} </strong>
                </span>
              </React.Fragment>
            ) : (
              <span>
                CRR: <strong>{score.now.runRate}</strong>
              </span>
            )}

            {/* <span>
              Min.Ov.Rem: <strong>5.1</strong>
            </span> */}
          </div>
          {/* <div className="liveScore__card">
            <span>
              Ground Time: <strong>16:40</strong>
            </span>
            <span>
              Reviews left: <strong>AUS 2 | PAK 2</strong>
            </span>
          </div> */}
          <div className="liveScore__card">
            <span>Recent:</span>
            <ul>{score.now.thisOver.map((ball, index) => this.overs(ball, index))}</ul>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

LiveScore.propTypes = {
  score: PropTypes.object,
};

export default LiveScore;
