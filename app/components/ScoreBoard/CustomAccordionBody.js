import React from 'react';
import PropTypes from 'prop-types';
// import { AccordionItemBody } from 'react-accessible-accordion';

/* eslint-disable react/prefer-stateless-function */
/* eslint-disable prefer-destructuring */

const battingKeys = ['runs', 'balls', 'fours', 'sixers', 'strikeRate'];
const bowlingKeys = ['overs', 'maidens', 'runs', 'wickets', 'wide', 'no_ball', 'economyRate'];

class CustomAccordionBody extends React.Component {
  state = {
    // hideSquard: false,
  };
  handleToggleSquard = () =>
    this.setState(ps => ({
      hideSquard: !ps.hideSquard,
    }));

  // shouldComponentUpdate = (nextProps, nextState) =>
  //   !nextProps.score.equals(this.props.score) || nextState.hideSquard !== this.state.hideSquard;

  render() {
    const { inning, score, order } = this.props;
    if (!score || !score.teams) return <React.Fragment />;
    const { players } = score;
    // TODO: refine this code
    const bowlingTeam =
      Object.keys(score.teams).indexOf(order[0]) === 0
        ? score.innings[`${Object.keys(score.teams)[1]}_${order[1]}`]
        : score.innings[`${Object.keys(score.teams)[0]}_${order[1]}`];
    if (score.teams && Object.keys(score.teams).length > 0)
      return (
        <div className="accorCcntent">
          <div className="table-responsive">
            <table className="table scorecard-table">
              <thead>
                <tr>
                  <th>BATSMEN</th>
                  <th>R</th>
                  <th>B</th>
                  <th>4s</th>
                  <th>6s</th>
                  <th>SR</th>
                </tr>
              </thead>
              <tbody>
                {inning.battingOrder &&
                  inning.battingOrder.map(player => {
                    const batsMan = players[player];
                    const { batting } = batsMan.innings[order[1]];
                    let battingStatus = '';
                    if (batting.runs) {
                      if (batting.dismissed) {
                        battingStatus = batting.dismissedAt.outStr;
                      } else {
                        battingStatus = 'Not Out';
                      }
                    }
                    return (
                      <tr key={player} className={batting.runs ? '' : ''}>
                        <td>
                          {batsMan.name}
                          <small>{battingStatus}</small>
                        </td>
                        {battingKeys.map(battingKey => {
                          let rate = null;
                          if (battingKey === 'strikeRate')
                            rate = batting[battingKey] ? batting[battingKey].toFixed(2) : ' 0.00';
                          else rate = batting[battingKey] ? batting[battingKey] : 0;
                          return <td key={battingKey}>{rate}</td>;
                        })}
                      </tr>
                    );
                  })}
              </tbody>
            </table>
            <div className="wicketFall-Card">
              <ul>
                <li>
                  Extra
                  <span className="total-wickets-right">
                    {inning.extras} (b {inning.byes} , lb {inning.legByes} , nb {inning.noBalls} ,
                    wd {inning.wides})
                  </span>
                </li>
                <li>
                  Total
                  <span className="total-wickets-right">
                    {inning.runs}/{inning.wickets} ({inning.overs / 1} overs, RR : &nbsp;
                    {inning.runRate})
                  </span>
                </li>
                {inning.wickets < 9 && (
                  <li>
                    Yet to Bat:
                    <ul className="full-wicket">
                      {inning.battingOrder &&
                        score.teams[order[0]].playingXi &&
                        score.teams[order[0]].playingXi
                          .filter(value => inning.battingOrder.indexOf(value) === -1)
                          .map((player, key) => {
                            const batsMan = players[player];
                            const { batting } = batsMan.innings[order[1]];
                            let battingStatus = '';
                            if (typeof batting === 'undefined') {
                              return null;
                            }
                            if (batting.runs) {
                              if (batting.dismissed) {
                                battingStatus = batting.outStr;
                              } else {
                                battingStatus = 'Not Out';
                              }
                            }
                            return (
                              <li key={`${key + 1}`}>
                                {batsMan.name} {','}
                                <small>{battingStatus}</small>
                              </li>
                            );
                          })}
                    </ul>
                  </li>
                )}
                {inning.wickets !== 0 && (
                  <li>
                    Fall Of Wickets:
                    <ul className="full-wicket">
                      {inning.wicketOrder &&
                        inning.wicketOrder
                          .map(player => ({
                            player,
                            wicket: score.players[player].innings[order[1]].batting.dismissedAt,
                          }))
                          .map(({ wicket, player }, index) => (
                            <li key={player}>
                              {` ${wicket.wicketIndex}-${wicket.teamRuns} (${
                                score.players[player].name
                              }, ${wicket.overStr})${
                                inning.wicketOrder.length === index + 1 ? '' : ', '
                              }`}
                            </li>
                          ))}
                    </ul>
                  </li>
                )}
              </ul>
            </div>
          </div>
          <div className="table-responsive">
            {/* <ul>
              <li>
                Extra
                <span className="total-wickets-right">
                  {inning.extras} (b {inning.byes} , lb {inning.leg_byes} , nb {inning.no_balls} ,
                  wd {inning.wides})
                </span>
              </li>
              <li>
                Total
                <span className="total-wickets-right">
                  {inning.runs}/{inning.wickets} ({inning.overs} overs, RR : &nbsp;
                  {inning.runRate})
                </span>
              </li>
              <li>
                Fall Of Wickets:
                <ul className="full-wicket">
                  {inning.wicketOrder &&
                    inning.wicketOrder
                      .map(player => ({
                        player,
                        wicket: score.players[player].innings[order[1]].batting.dismissedAt,
                      }))
                      .map(({ wicket, player }, index) => (
                        <li key={player}>
                          {` ${wicket.wicketIndex}-${wicket.teamRuns} (${
                            score.players[player].name
                          }, ${wicket.overStr})${
                            inning.wicketOrder.length === index + 1 ? '' : ', '
                          }`}
                        </li>
                      ))}
                </ul>
              </li>
            </ul> */}
            <table className="table bowlerCard">
              <thead>
                <tr>
                  <th>BOWLERS</th>
                  <th>O</th>
                  <th>M</th>
                  <th>R</th>
                  <th>W</th>
                  <th>NB</th>
                  <th>WD</th>
                  <th>Econ</th>
                </tr>
              </thead>
              <tbody>
                {bowlingTeam.bowlingOrder &&
                  bowlingTeam.bowlingOrder.map(player => {
                    const bowler = players[player];
                    const bowling = bowler.innings[order[1]].bowling;
                    return (
                      <tr key={player}>
                        <td>{bowler.name}</td>
                        {bowlingKeys.map(key => {
                          if (key === 'economyRate')
                            return (
                              <td key={key}>{bowling[key] ? bowling[key].toFixed(2) : ' 0.00'}</td>
                            );
                          return (
                            <td key={key}>
                              {bowling[key] || bowling[key] === 0 ? bowling[key] : '-'}
                            </td>
                          );
                        })}
                      </tr>
                    );
                  })}
              </tbody>
            </table>
          </div>
        </div>
      );
    return <React.Fragment />;
  }
}

CustomAccordionBody.propTypes = {
  score: PropTypes.object.isRequired,
  inning: PropTypes.object.isRequired,
  order: PropTypes.array.isRequired,
};
export default CustomAccordionBody;
