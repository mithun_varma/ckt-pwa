/**
 *
 * Header
 *
 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable dot-notation */

import React from 'react';
import PropTypes from 'prop-types';
import orderBy from 'lodash/orderBy';
import ScrollTags from '../Common/ScrollTags';

const TAGS_CONFIG = [
  { label: 'HIGHEST SCORING', key: 'desc' },
  { label: 'LOWEST SCORING', key: 'asc' },
];

class FantasyBoard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeKey: 'desc',
    };
    this.onTagClick = this.onTagClick.bind(this);
  }
  componentDidMount() {
    // this.props.fetchFantasyBoard({ matchId: this.props.matchId });
  }
  onTagClick = activeKey => {
    this.setState({ activeKey });
  };

  render() {
    const { fantasyBoard } = this.props;
    return (
      <React.Fragment>
        <ScrollTags
          tags={TAGS_CONFIG}
          activeKey={this.state.activeKey}
          onTagClick={this.onTagClick}
        />
        <div className="wrapperContainer">
          <div className="tabContent-section">
            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr>
                    <th>Rank</th>
                    <th>Players</th>
                    <th>Batting</th>
                    <th>Bowling</th>
                    <th>Fielding</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                  {orderBy(fantasyBoard, d => d.total, this.state.activeKey).map((record, key) => (
                    <tr key={`${key + 1}`}>
                      <td>{key + 1}</td>
                      <td>{record.fullname}</td>
                      <td>{record.batting}</td>
                      <td>{record.bowling}</td>
                      <td>{record.fielding}</td>
                      <td>{record.total}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

FantasyBoard.propTypes = {
  // fetchFantasyBoard: PropTypes.func,
  // matchId: PropTypes.string,
  fantasyBoard: PropTypes.array,
};

export default FantasyBoard;
