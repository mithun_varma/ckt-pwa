/**
 *
 * Header
 *
 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */
/* eslint-disable react/no-danger */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
const BALL_CONFIG = {
  '4': 'fourball',
  w: 'wicketball',
  '6': 'sixball',
  wd: '',
  '50': 'fiftyruns',
  '100': 'hundredruns',
};

const CommentaryCard = ({ comment, overs, style, ball }) => (
  <div className="commentryCard" style={style}>
    <div className="commentryCard__runs">
      <h5 className={`overNumber__big ${BALL_CONFIG[ball]}`}>{ball}</h5>
      <div className="overs">{overs} Overs</div>
    </div>
    <div className="commentryCard__detail">
      <p dangerouslySetInnerHTML={{ __html: comment }} />
    </div>
  </div>
);

CommentaryCard.propTypes = {
  ball: PropTypes.any.isRequired,
  style: PropTypes.any,
  comment: PropTypes.string.isRequired,
  overs: PropTypes.string.isRequired,
};

export default CommentaryCard;
