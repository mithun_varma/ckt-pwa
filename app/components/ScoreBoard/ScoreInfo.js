/* eslint-disable indent */
/* eslint-disable no-nested-ternary */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/anchor-is-valid: 0 */
/* eslint global-require: 0 */

import React from 'react';
import PropTypes from 'prop-types';
import Reveal from 'react-reveal/Reveal';
import { distanceInWordsToNow } from 'date-fns';
// import moment from 'moment';
// import teamFlagPlaceholder from '../../images/team-flag-placeholder.svg';
import manOftheMatch from '../../images/Player_Profile_Round.png';
import animate from '../../utils/animationConfig';

const scoreDisplay = status => {
  const Status = (live, venue, league, score, prediction, matchStatus, detailPage) => ({
    live,
    venue,
    league,
    score,
    prediction,
    matchStatus,
    detailPage,
  });
  switch (status) {
    case 'started':
      return Status(true, true, true, true, true, true, true);
    case 'notstarted':
      return Status(false, true, true, false, true, true, false);
    case 'completed':
      return Status(false, true, true, true, false, true, false);
    default:
      return Status(false, true, true, false, true, true, false);
  }
};

class ScoreInfo extends React.Component {
  getBallColor(ball) {
    switch (ball) {
      case '6':
        return 'six';
      case '4':
        return 'four';
      case 'w':
        return 'wicket';
      default:
        return 'default';
    }
  }
  render() {
    const { score } = this.props;
    if (!score || !score.teams) {
      return null;
    }

    const { status } = score;
    const scorecard = scoreDisplay(status);
    return (
      <div>
        {!score.manOfTheMatch &&
          scorecard.live !== 'live' &&
          score.prediction && (
            <div className="scoreboard-prediction">
              <div className="scoreBoard__predictionElement">
                <div className="prediction__winnings">
                  <div className="whos-winning">
                    {'Who’s Winning?'}
                    <sup className="trademark">tm</sup>
                  </div>
                  <div className="team-wining">{score.prediction.predictionMargin}</div>
                </div>
                {score.prediction.current && (
                  <div className="predictionSection">
                    <div className="name">
                      {score.prediction.team1_name
                        ? score.prediction.team1_name.slice(0, 3).toUpperCase()
                        : ''}
                    </div>
                    <div className="percentage">
                      {score.prediction.current.team1_notie_percent}%
                    </div>
                    <div className="progress-wrapper">
                      <div className="progress progress-wrapper__bar" />
                      <span className="slide-line" />
                    </div>
                    <div className="percentage">
                      {`${score.prediction.current.team2_notie_percent}%`}
                    </div>
                    <div className="name">
                      {score.prediction.team2_name
                        ? score.prediction.team2_name.slice(0, 3).toUpperCase()
                        : ''}
                    </div>
                  </div>
                )}
              </div>
            </div>
          )}

        {score.manOfTheMatch ? (
          <div className="scoreBoard__menofmatch">
            <img src={manOftheMatch} alt="team first" />
            <Reveal effect={animate.scoreCard.mom.effect}>
              <h5>MAN OF THE MATCH</h5>
              {score.manOfTheMatch && <span>{score.players[score.manOfTheMatch].fullname}</span>}
            </Reveal>
          </div>
        ) : status !== 'started' && status !== 'completed' ? (
          <div className="scoreBoard__menofmatch">
            {/* <img src="https://dummyimage.com/35x25/ffb300/fff/0011ff" alt="team first" /> */}
            <img
              style={{ width: '24px', height: '24px' }}
              src={require('../../images/colockSvg.svg')}
              alt="team first"
            />
            <h5 style={{ marginRight: 5 }}>{`${
              score.startDate.iso >= new Date().toISOString()
                ? distanceInWordsToNow(score.startDate.iso, {
                    includeSeconds: true,
                  })
                : 'about to start'
            }`}</h5>
            <span>{`${score.startDate.iso >= new Date().toISOString() ? 'to go' : ''}`}</span>
          </div>
        ) : (
          <div />
        )}
      </div>
    );
  }
}

ScoreInfo.propTypes = {
  score: PropTypes.object,
  // teamName: PropTypes.string,
  // className: PropTypes.string,
  // onClick: PropTypes.func,
};
export default ScoreInfo;
