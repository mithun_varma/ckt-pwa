/**
 *
 * Scoreboard
 *
 */

/* eslint-disable indent */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */

import React from 'react';
import PropTypes from 'prop-types';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

// import styled from 'styled-components';

const CustomAccordionTitle = ({
  score,
  order,
  inningsConstant,
  inning,
  index,
  toggle,
  onClick,
}) => {
  const toggleOpen = !!toggle[index];

  return (
    <React.Fragment>
      <div
        className={toggleOpen ? 'accorHeader active' : 'accorHeader'}
        onClick={e => {
          e.preventDefault();
          onClick(index);
        }}
      >
        <div className="squardList">
          <div className="squardList__content">
            <div className="squardList__list">
              <span>
                <strong>{score.teams[order[0]].name}</strong>
                {score.format === 'test' ? `${inningsConstant[order[1]]}` : ''}
                {` ${inning.runs}/${inning.wickets}(${inning.overs} Ov)`}
              </span>
            </div>
            <div className="squardList__list">
              <span className="squardList__list__icon">
                {toggleOpen ? (
                  <ExpandLess style={{ color: 'red' }} />
                ) : (
                  <ExpandMore style={{ color: 'red' }} />
                )}
              </span>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

CustomAccordionTitle.propTypes = {
  score: PropTypes.object.isRequired,
  order: PropTypes.array.isRequired,
  inningsConstant: PropTypes.object.isRequired,
  inning: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  toggle: PropTypes.array.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default CustomAccordionTitle;
