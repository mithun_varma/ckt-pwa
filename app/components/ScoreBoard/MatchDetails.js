/**
 *
 * Scoreboard
 *
 */

/* eslint-disable indent */
/* eslint no-unused-vars: 0 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint-disable no-nested-ternary */
/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from 'react';
import PropTypes from 'prop-types';

class MatchDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { score } = this.props;
    if (!score || !score.battingOrder) return <div />;
    return (
      <React.Fragment>
        <div className="tab-pane fade" role="presentation">
          <div className="accorCcntent">
            {score && (
              <div className="table-responsive">
                <div className="matchDetails">
                  <ul>
                    <li style={{ marginBottom: 20 }}>
                      <span className="name">Match Details</span>
                    </li>
                    <li>
                      <span className="name">Venue</span>
                      <span className="description">{score.venue}</span>
                    </li>
                    <li>
                      <span className="name">Series</span>
                      <span className="description">
                        <a href="#">{score.season && score.season.name}</a>
                      </span>
                    </li>
                    <li>
                      <span className="name">Match</span>
                      <span className="description">{score.name}</span>
                    </li>
                    <li>
                      <span className="name">Toss</span>
                      <span className="description">
                        {score.toss
                          ? `${score.teams[score.toss.won].name}, elected to ${
                              score.toss.decision
                            } first`
                          : '-'}
                      </span>
                    </li>
                  </ul>
                </div>
              </div>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

MatchDetails.propTypes = {
  score: PropTypes.object.isRequired,
};

export default MatchDetails;
