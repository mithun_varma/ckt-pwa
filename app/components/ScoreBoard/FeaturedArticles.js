/* eslint-disable global-require */
/**
 *
 * Scoreboard
 *
 */

/* eslint-disable indent */
/* eslint no-unused-vars: 0 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint-disable no-nested-ternary */

import React from 'react';
import PropTypes from 'prop-types';
import Reveal from 'react-reveal/Reveal';
import ArticleCard from 'components/Articles/ArticleCard';
import Loader from 'components/Common/Loader';
import EmptyState from 'components/Common/EmptyState';
import animate from '../../utils/animationConfig';

class FeaturedArticles extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = () => {
    this.props.fetchFeaturedArticles({ matchId: this.props.matchId });
  };
  cardOnClick = path => {
    this.props.history.push(path);
  };

  render() {
    const { articles, loading, articleHost } = this.props;
    return (
      <div>
        {loading ? (
          <Loader styles={{ height: '150px' }} noOfLoaders={5} />
        ) : articles && articles.length > 0 ? (
          articles.map(article => (
            <Reveal effect={animate.featuredArticles.card.effect}>
              <div className="sectionWrapper__card">
                <ArticleCard
                  article={article}
                  articleHost={articleHost}
                  cardType="fantasyAnalysis"
                  onClick={this.cardOnClick}
                />
              </div>
            </Reveal>
          ))
        ) : (
          <Reveal effect={animate.emptyState.effect}>
            <EmptyState img={require('../../images/no_articles.png')} msg="No Articles found" />
          </Reveal>
        )}
      </div>
    );
  }
}

FeaturedArticles.propTypes = {
  fetchFeaturedArticles: PropTypes.func,
  articles: PropTypes.array,
  matchId: PropTypes.string,
  history: PropTypes.object,
  loading: PropTypes.bool,
  articleHost: PropTypes.string,
};

export default FeaturedArticles;
