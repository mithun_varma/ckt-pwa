/**
 *
 * SingleImage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

function SingleImage({ imagesource, hasJerseyNo }) {
  return (
    <div className="singleImage">
      <div className="singleImage__card">
        <img className={hasJerseyNo ? 'playerImg' : ''} src={imagesource} alt=" " />
      </div>
    </div>
  );
}

SingleImage.propTypes = {
  imagesource: PropTypes.string,
  hasJerseyNo: PropTypes.bool,
};

export default SingleImage;
