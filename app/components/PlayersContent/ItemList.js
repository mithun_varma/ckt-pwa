/**
 *
 * ItemList
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Reveal from 'react-reveal/Reveal';
import animate from '../../utils/animationConfig';

// import styled from 'styled-components';

function ItemList({ title, desc, link }) {
  return (
    <div className="commonList">
      <span>
        <strong>{title}</strong>
      </span>
      <Reveal effect={animate.playerContent.desc.effect}>
        <span>{desc}</span>
      </Reveal>
      <span>
        <a href>{link}</a>
      </span>
    </div>
  );
}

ItemList.propTypes = {
  title: PropTypes.string,
  desc: PropTypes.string,
  link: PropTypes.string,
};

export default ItemList;
