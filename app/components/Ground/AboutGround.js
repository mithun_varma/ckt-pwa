/**
 *
 * AboutGround
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Reveal from 'react-reveal/Reveal';
import animate from '../../utils/animationConfig';

// import styled from 'styled-components';

function AboutGround({ title, value }) {
  return (
    <div className="commonList">
      <span>
        <strong>{title}</strong>
      </span>
      <Reveal effect={animate.aboutGround.value.effect}>
        <span>{value}</span>
      </Reveal>
    </div>
  );
}

AboutGround.propTypes = {
  title: PropTypes.string,
  value: PropTypes.string,
};

export default AboutGround;
