/**
 *
 * GroundStats
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import Reveal from 'react-reveal/Reveal';
import animate from '../../utils/animationConfig';

function GroundStats({ title, score, series }) {
  return (
    <div className="groundState__card">
      <h5>{title}</h5>
      <Reveal effect={animate.groundStats.score.effect}>
        <h4>{score}</h4>
      </Reveal>
      <Reveal effect={animate.groundStats.series.effect}>
        <span>{series}</span>
      </Reveal>
    </div>
  );
}

GroundStats.propTypes = {
  title: PropTypes.string,
  score: PropTypes.number,
  series: PropTypes.string,
};

export default GroundStats;
