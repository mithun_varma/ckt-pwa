/* eslint global-require: 0 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import map from 'lodash/map';
import Reveal from 'react-reveal/Reveal';
import animate from '../../utils/animationConfig';
import ListItem from '../Common/ListItem';

class CountrysGround extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { grounds, country } = this.props;
    return (
      <Reveal effect={animate.countryGround.parent.effect}>
        <div className="sectionWrapper__card sectionWhite">
          {grounds.groundCountryWise[country] &&
            map(grounds.groundCountryWise[country], (list, key) => (
              <ListItem
                key={`${key + 1}`}
                title={list.displayName}
                caption={`${list.city}, ${list.country}`}
                onClick={() => this.props.getGroundDetails(list)}
                img={list.avatar || require('../../images/stadium-placeholder.png')}
              />
            ))}
        </div>
      </Reveal>
    );
  }
}

CountrysGround.propTypes = {
  grounds: PropTypes.object,
  country: PropTypes.string,
  getGroundDetails: PropTypes.func,
};
export default CountrysGround;
