/* eslint global-require: 0 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Reveal from 'react-reveal/Reveal';
import EmptyState from 'components/Common/EmptyState';
import Sticky from 'react-stickynode';
import animate from '../../utils/animationConfig';

import GroundStats from './GroundStats';
import ScrollTabs from '../Common/ScrollTabs';
import AboutGround from './AboutGround';

const VENUE_TABS = ['about venue', 'statistics'];
export class GroundDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 'about venue',
    };
  }

  handleActiveTab = activeTab => {
    this.setState({
      activeTab,
    });
  };

  getMatchDateFormat = matchInfo => {
    let dateStr = '-';
    if (matchInfo && matchInfo.datetime) {
      dateStr = `${matchInfo.datetime} ${matchInfo.teamA} vs ${matchInfo.teamB}`;
      return dateStr;
    }
    return dateStr;
  };

  render() {
    const {
      grounds: { groundDetails },
    } = this.props;
    return (
      <div className="sectionWrapper__card">
        <div className="innerWrapper">
          <Reveal effect={animate.groundDetails.image.effect}>
            <div className="singleImage">
              <div className="singleImage__card">
                <img src={require('../../images/cricketGround1.png')} alt="" />
              </div>
            </div>
          </Reveal>
        </div>
        <Sticky
          enabled
          top={49}
          // bottomBoundary={3000}
          innerZ={999}
          activeClass="bottomBoxSch"
        >
          <ScrollTabs
            items={VENUE_TABS}
            activeItem={this.state.activeTab}
            className="groundTab"
            onClick={activeTab => {
              this.handleActiveTab(activeTab);
            }}
          />
        </Sticky>
        {!this.state.activeTab === 'statistics' ? (
          <div className="innerWrapper">
            <div claaaName="groundState">
              <div className="d-flex">
                <GroundStats title="Run" score="110" series="Test" />
              </div>
              <div className="d-flex">
                <GroundStats title="Matches won batting first" score="110" series="Test" />
              </div>
              <div className="d-flex">
                <div className="w-49">
                  <GroundStats title="Highest Total Recorded" score="410/7" series="Test" />
                </div>
                <div className="w-49">
                  <GroundStats title="Lowest Total Recorded" score="50/9" series="Test" />
                </div>
              </div>
              <div className="d-flex">
                <GroundStats title="Matches won bowling first" score="180" series="Test" />
              </div>
              <div className="d-flex">
                <div className="w-49">
                  <GroundStats title="Avg. First Innings score" score="254" series="Test" />
                </div>
                <div className="w-49">
                  <GroundStats title="Avg. Second Innings score" score="250" series="Test" />
                </div>
              </div>
            </div>
          </div>
        ) : (
          this.state.activeTab === 'statistics' && (
            <Reveal effect={animate.emptyState.effect}>
              <EmptyState msg="No Stats Found" />
            </Reveal>
          )
        )}
        {this.state.activeTab === 'about venue' && (
          <div className="innerWrapper">
            <ul>
              <li>
                <div className="d-flex">
                  <div className="w-49">
                    <AboutGround title="City" value={groundDetails.city} />
                  </div>
                  <div className="w-49">
                    <AboutGround title="Capacity" value={groundDetails.capacity} />
                  </div>
                </div>
              </li>
              <li>
                <div className="d-flex">
                  <div className="w-49">
                    <AboutGround
                      title="Flood Lights"
                      value={groundDetails.floodLights ? 'Y' : 'N'}
                    />
                  </div>
                  <div className="w-49">
                    <AboutGround
                      title="Bowling Ends"
                      value={
                        groundDetails.bowlingEnds && groundDetails.bowlingEnds.length > 0
                          ? groundDetails.bowlingEnds.join(', ')
                          : '-'
                      }
                    />
                  </div>
                </div>
              </li>
              <li>
                <div className="d-flex">
                  <div className="w-49">
                    <AboutGround
                      title="1st International Test"
                      value={this.getMatchDateFormat(groundDetails.firstInternationalTest)}
                    />
                  </div>
                  <div className="w-49">
                    <AboutGround
                      title="1st International ODI"
                      value={this.getMatchDateFormat(groundDetails.firstInternationalOdi)}
                    />
                  </div>
                </div>
              </li>
              <li>
                <div className="d-flex">
                  <AboutGround
                    title="1st International T20"
                    value={this.getMatchDateFormat(groundDetails.firstInternationalT20)}
                  />
                </div>
              </li>
              <li>
                <div className="d-flex">
                  <AboutGround
                    title="Domestic Teams"
                    value={
                      groundDetails.domesticTeams && groundDetails.domesticTeams.length > 0
                        ? groundDetails.domesticTeams.join(', ')
                        : '-'
                    }
                  />
                </div>
              </li>
              <li>
                <div className="d-flex">
                  <div className="commonList">
                    <span>
                      <strong>ABOUT</strong>
                    </span>
                    <p>{groundDetails.aboutGround}</p>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        )}
      </div>
    );
  }
}

GroundDetails.propTypes = {
  grounds: PropTypes.object,
};

export default GroundDetails;
