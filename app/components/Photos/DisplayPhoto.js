import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Header from 'components/Common/Header';
import Slider from 'react-slick';

import MagicSliderDots from 'react-magic-slider-dots';
// import 'slick-carousel/slick/slick.css';
// import 'slick-carousel/slick/slick-theme.css';
// import 'react-magic-slider-dots/dist/magic-dots.css';

export class DisplayPhoto extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.fantasySlideSettings = {
      dots: true,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      centerMode: true,
      centerPadding: '0px',
      className: 'scoreboard-slider',
      cssEase: 'linear',
      appendDots: dots => (
        <div>
          {/* <ul className="slider-dots-wrapper">{dots}</ul> */}
          <MagicSliderDots dots={dots} numDotsToShow={4} dotWidth={20} />
        </div>
      ),
    };
    this.sliderRef = React.createRef();
  }

  componentDidMount = () => {
    this.sliderRef.current.slickGoTo(this.props.activeImage);
  };

  render() {
    const { photos } = this.props;
    return (
      <React.Fragment>
        <Header
          title=""
          leftIcon="arrowBack"
          leftIconOnClick={() => {
            this.props.togglePhoto(false);
          }}
          styles={{ backgroundColor: 'transparent', color: '#FFF' }}
        />
        <div className="photos__display">
          <Slider {...this.fantasySlideSettings} ref={this.sliderRef}>
            {photos.photoGallery.map((list, key) => (
              <div className="photosDisplay">
                <div className="photos__item" key={`${key + 1}`}>
                  <img src={photos.photoHost + list.imageData.image} alt="" />
                  <div className="photosDisplay__desc">{list.title}</div>
                </div>
              </div>
            ))}
          </Slider>
        </div>
      </React.Fragment>
    );
  }
}

DisplayPhoto.propTypes = {
  togglePhoto: PropTypes.func,
  photos: PropTypes.object,
  activeImage: PropTypes.number,
};

export default DisplayPhoto;
