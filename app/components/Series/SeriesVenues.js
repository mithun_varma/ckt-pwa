/**
 *
 * Header
 *
 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable dot-notation */
/* eslint-disable no-nested-ternary */
/* eslint-disable indent */
/* eslint-disable global-require */

import React from 'react';
import PropTypes from 'prop-types';
import EmptyState from 'components/Common/EmptyState';

import Reveal from 'react-reveal/Reveal';
import Loader from 'components/Common/Loader';
import ListItem from 'components/Common/ListItem';
import animate from '../../utils/animationConfig';

class SeriesVenues extends React.Component {
  componentDidMount() {
    this.props.fetchSeriesVenues({ seriesId: this.props.seriesId });
  }

  render() {
    const { series } = this.props;
    return (
      <Reveal effect={animate.emptyState.effect}>
        {series.seriesVenuesLoading ? (
          <Loader styles={{ height: '75px' }} noOfLoaders={5} />
        ) : series.seriesVenues.venues && series.seriesVenues.venues.length > 0 ? (
          <div className="wrapperContainer">
            <div className="sectionWrapper__card sectionWhite">
              {series.seriesVenues.venues.map((list, key) => (
                <ListItem
                  key={`${key + 1}`}
                  title={list.displayName}
                  caption={`${list.city}, ${list.country.replace(', ', '')}`}
                  onClick={() => this.props.history.push(`/grounds/${list.country}/${list['_id']}`)}
                  img={list.avatar || require('../../images/stadium-placeholder.png')}
                />
              ))}
            </div>
          </div>
        ) : (
          <Reveal effect={animate.emptyState.effect}>
            <EmptyState msg="No Grounds Found" />
          </Reveal>
        )}
      </Reveal>
    );
  }
}

SeriesVenues.propTypes = {
  fetchSeriesVenues: PropTypes.func,
  seriesId: PropTypes.string,
  series: PropTypes.object,
  history: PropTypes.object,
};

export default SeriesVenues;
