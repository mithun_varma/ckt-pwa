/**
 *
 * Header
 *
 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable dot-notation */
/* eslint-disable no-nested-ternary */
/* eslint-disable indent */

import React from 'react';
import PropTypes from 'prop-types';
import EmptyState from 'components/Common/EmptyState';
import Loader from 'components/Common/Loader';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import Reveal from 'react-reveal/Reveal';

import groupBy from 'lodash/groupBy';
import map from 'lodash/map';

import PillTabs from '../Common/PillTabs';
import animate from '../../utils/animationConfig';

let TEAM_TABS = [];
class SeriesSquad extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      test: true,
      odi: true,
      t20: true,
      activeTab: 0,
      toggleCard: {
        0: false,
      },
    };
  }

  componentDidMount() {
    this.props.fetchSeriesSquads({ seriesId: this.props.seriesId });
  }

  onTabChange = tab => {
    if (tab !== this.state.activeTab) {
      this.setState({
        activeTab: tab,
        toggleCard: {},
      });
    }
  };

  toggleCard = key => {
    this.setState({
      toggleCard: {
        ...this.state.toggleCard,
        [key]: !this.state.toggleCard[key],
      },
    });
  };

  render() {
    const { series } = this.props;
    const teamsSquads = groupBy(series.seriesSquads.squads, 'format');
    // const teamsSquads = groupBy(series.seriesSquads.squads, 'additionalInfo.team.shortName');
    TEAM_TABS = Object.keys(teamsSquads);
    return (
      <div className="wrapperContainer">
        {series.seriesSquadsLoading ? (
          <Loader styles={{ height: '75px' }} noOfLoaders={4} />
        ) : series.seriesSquads &&
        series.seriesSquads.squads &&
        series.seriesSquads.squads.length > 0 ? (
          <div>
            {TEAM_TABS.length > 1 && (
              <div>
                <PillTabs
                  items={TEAM_TABS}
                  // items={series.seriesSquads.shortName.split(' vs ')}
                  activeItem={this.state.activeTab}
                  onClick={this.onTabChange}
                />
              </div>
            )}

            <div className="tab-pane fade" role="presentation">
              {/* {series.seriesSquads.squads.map((data, squadKey) => ( */}
              {map(teamsSquads[TEAM_TABS[this.state.activeTab]], (data, squadKey) => (
                <div
                  key={`${squadKey + 1}`}
                  className="accordion score-accordion"
                  onClick={e => {
                    e.preventDefault();
                    // this.setState({ [data.format]: !this.state[data.format] });
                    this.toggleCard(squadKey);
                  }}
                >
                  <div className="card">
                    <div className={this.state[data.format] ? 'accorHeader active' : 'accorHeader'}>
                      <div className="squardList">
                        <div className="squardList__content">
                          <div className="squardList__list">
                            <span>
                              <strong>{data.name}</strong>
                            </span>
                          </div>
                          <div className="squardList__list">
                            <span className="squardList__list__icon">
                              {/* {this.state[data.format] ? ( */}
                              {!this.state.toggleCard[squadKey] ? (
                                // {teamsSquads[TEAM_TABS[this.state.activeTab]] ? (
                                <ExpandLess style={{ color: 'red' }} />
                              ) : (
                                <ExpandMore style={{ color: 'red' }} />
                              )}
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    className="accorCcntent"
                    style={{ display: !this.state.toggleCard[squadKey] ? 'none' : 'block' }}
                    // style={{ display: this.state[data.format] ? 'block' : 'none' }}
                  >
                    {/* Currently Squad format is not correct, we have to change later */}
                    {data.additionalInfo.players.map((player, playerKey) => (
                      <div className="squardList" key={`${playerKey + 1}`}>
                        <div className="squardList__content">
                          <div className="squardList__list">
                            <span>{player.displayName}</span>
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              ))}
            </div>
          </div>
        ) : (
          <Reveal effect={animate.emptyState.effect}>
            <EmptyState msg="No squads found" />
          </Reveal>
        )}
      </div>
    );
  }
}

SeriesSquad.propTypes = {
  series: PropTypes.object.isRequired,
  seriesId: PropTypes.string,
  fetchSeriesSquads: PropTypes.func.isRequired,
};

export default SeriesSquad;

// Squad Structure Required
// squads = [
//   ind: {
//     test: [],
//     odi: [],
//     t20: []
//   },
//   aus: {
//     test: [],
//     odi: [],
//     t20: []
//   },
// ]
