/**
 *
 * Header
 *
 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable dot-notation */
/* eslint-disable no-nested-ternary */
/* eslint-disable indent */

import React from 'react';
import PropTypes from 'prop-types';
import Reveal from 'react-reveal/Reveal';
import EmptyState from 'components/Common/EmptyState';
import Loader from 'components/Common/Loader';
import ScoreCard from 'components/ScoreBoard/ScoreCard';
import moment from 'moment';
import InfiniteScroll from 'react-infinite-scroll-component';
import animate from '../../utils/animationConfig';

class MatchList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // initial: true,
    };
  }

  componentDidMount() {
    this.props.fetchSeriesDetails({ seriesId: this.props.seriesId, skip: 0, limit: 6 });
  }

  static getDerivedStateFromProps(props, state) {
    if (state.initial)
      return {
        initial: false,
      };
    return null;
  }

  loadMoreItems = () => {
    this.props.fetchSeriesDetails({
      seriesId: this.props.seriesId,
      limit: 6,
      skip: this.props.series.seriesMatches.length,
    });
  };

  cardOnClick = path => {
    this.props.history.push(path);
  };

  render() {
    const { series } = this.props;
    return (
      <div className="wrapperContainer">
        {/* {series.seriesDetailsLoading && this.state.initial ? ( */}
        {series.seriesDetailsLoading ? (
          <Loader styles={{ height: '75px' }} noOfLoaders={4} />
        ) : series.seriesMatches.length > 0 ? (
          <InfiniteScroll
            style={{ overflow: 'unset' }}
            dataLength={series.seriesMatches.length}
            next={this.loadMoreItems}
            loader={
              series.seriesMatches.length >= 6 && (
                <Loader styles={{ height: '150px' }} noOfLoaders={2} />
              )
            }
            // Uncoment Below condition when skip & limit starts working in Series Details
            // hasMore={series.seriesMatchesOldLength !== series.seriesMatches.length}
            hasMore={false}
          >
            {series.seriesMatches.map(
              (match, matchKey) =>
                match.scorecard && (
                  <section className="sectionWrapper__matchList" key={`${matchKey + 1}`}>
                    <div className="seriesDate">
                      {match.startDate && (
                        <h5>{moment(match.startDate).format('Do MMMM, YYYY')}</h5>
                      )}
                    </div>
                    <div className="sectionWrapper__card sectionWhite">
                      <ScoreCard score={match.scorecard} onClick={this.cardOnClick} />
                    </div>
                  </section>
                ),
            )}
          </InfiniteScroll>
        ) : (
          <Reveal effect={animate.emptyState.effect}>
            <EmptyState msg="No matches found" />
          </Reveal>
        )}
      </div>
    );
  }
}

MatchList.propTypes = {
  series: PropTypes.object.isRequired,
  fetchSeriesDetails: PropTypes.func.isRequired,
  seriesId: PropTypes.string,
  history: PropTypes.object,
};

export default MatchList;
