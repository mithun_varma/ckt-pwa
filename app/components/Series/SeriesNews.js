/**
 *
 * Header
 *
 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable dot-notation */
/* eslint-disable no-nested-ternary */
/* eslint-disable indent */

import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import InfiniteScroll from 'react-infinite-scroll-component';
import Reveal from 'react-reveal/Reveal';

import EmptyState from 'components/Common/EmptyState';
import Loader from 'components/Common/Loader';
const ArticleCard=null;
const  FeatureArticleCard=null;
import animate from '../../utils/animationConfig';

class SeriesNews extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // initial: true,
    };
    this.newsSlideSettings = {
      dots: false,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      centerMode: true,
      // centerPadding: 18,
    };
    this.loadMoreItems = this.loadMoreItems.bind(this);
  }

  componentDidMount() {
    this.props.fetchSeriesNews({ seriesId: this.props.seriesId, skip: 0, limit: 10 });
  }

  static getDerivedStateFromProps(props, state) {
    if (state.initial)
      return {
        initial: false,
      };
    return null;
  }

  loadMoreItems = () => {
    this.props.fetchSeriesNews({
      seriesId: this.props.seriesId,
      limit: 10,
      skip: this.props.series.seriesNews.length,
    });
  };

  cardOnClick = path => {
    this.props.history.push(path);
  };

  getCardType = type => {
    if (type === 'match-report') {
      return 'matchReport';
    } else if (type === 'analysis') {
      return 'fantasyAnalysis';
    }
    return 'newsOpinion';
  };

  render() {
    const { series } = this.props;
    return (
      <div className="wrapperContainer">
        {series.seriesNewsLoading && series.seriesNews.length === 0 ? (
          <Loader styles={{ height: '75px' }} noOfLoaders={5} />
        ) : (
          <React.Fragment>
            {/* Slider section */}
            {/* No Need to Display Slider in Series News, Added  false to do not map */}
            {false &&
              series.seriesNewsFeatured.length > 0 && (
                <div className="sectionWrapper__card">
                  {series.seriesNewsFeatured.length === 1 ? (
                    <FeatureArticleCard
                      article={series.seriesNewsFeatured[0]}
                      cardType={this.getCardType(series.seriesNewsFeatured[0].type)}
                      articleHost={series.host}
                      onClick={this.cardOnClick}
                    />
                  ) : (
                    <Slider
                      {...this.newsSlideSettings}
                      autoplay={false}
                      centerPadding={series.seriesNewsFeatured.length > 1 ? 18 : 0}
                      className={series.seriesNewsFeatured.length > 1 && 'multiple-articles'}
                    >
                      {series.seriesNewsFeatured.map((article, key) => (
                        <FeatureArticleCard
                          article={article}
                          cardType={this.getCardType(article.type)}
                          articleHost={series.host}
                          key={`${key + 1}`}
                          onClick={this.cardOnClick}
                        />
                      ))}
                    </Slider>
                  )}
                </div>
              )}
            {/* Infinite croll Section */}
            {series.seriesNews.length > 0 && (
              <InfiniteScroll
                dataLength={series.seriesNews.length}
                next={this.loadMoreItems}
                loader={
                  series.seriesNews.length >= 10 && (
                    <Loader styles={{ height: '150px' }} noOfLoaders={5} />
                  )
                }
                // hasMore={(series.seriesNews.length + series.seriesNewsFeatured.length) % 10 === 0}
                hasMore={series.seriesNewsOldLength !== series.seriesNews.length}
              >
                {series.seriesNews.map((article, articleKey) => (
                  <div className="sectionWrapper__card" key={`${articleKey + 1}`}>
                    {/* {article.filters && article.filters.featured ? (
                      <FeatureArticleCard
                        article={article}
                        cardType={this.getCardType(article.type)}
                        articleHost={series.host}
                        onClick={this.cardOnClick}
                      />
                    ) : ( */}
                    <ArticleCard
                      article={article}
                      // cardType={this.getCardType(article.type)}
                      cardType="analysis"
                      articleHost={series.host}
                      onClick={this.cardOnClick}
                      iterationId={articleKey}
                    />
                    {/* )} */}
                  </div>
                ))}
              </InfiniteScroll>
            )}
            {/* Empty State */}
            {series.seriesNews.length === 0 && (
              <Reveal effect={animate.emptyState.effect}>
                <EmptyState msg="No Series News found" />
              </Reveal>
            )}
          </React.Fragment>
        )}
      </div>
    );
  }
}

SeriesNews.propTypes = {
  fetchSeriesNews: PropTypes.func.isRequired,
  series: PropTypes.object,
  seriesId: PropTypes.string,
  history: PropTypes.object,
};

export default SeriesNews;
