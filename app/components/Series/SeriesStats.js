/**
 *
 * Header
 *
 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable dot-notation */
/* eslint-disable no-nested-ternary */
/* eslint-disable indent */

import React from 'react';
// import PropTypes from 'prop-types';
import EmptyState from 'components/Common/EmptyState';
// import Loader from 'components/Common/Loader';
import Reveal from 'react-reveal/Reveal';
import animate from '../../utils/animationConfig';

class SeriesStats extends React.Component {
  componentDidMount() {}

  render() {
    return (
      <Reveal effect={animate.emptyState.effect}>
        <EmptyState msg="No stats found" />
      </Reveal>
    );
  }
}

SeriesStats.propTypes = {};

export default SeriesStats;
