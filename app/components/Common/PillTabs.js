/**
 *
 * Header
 *
 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */

import React from "react";
import PropTypes from "prop-types";
import {
  grey_6,
  orange_10,
  red_Orange
} from "../../styleSheet/globalStyle/color";
// import styled from 'styled-components';

const PillTabs = ({
  items,
  activeItem,
  onTabSelect,
  rootStyles,
  isDisabled
}) => (
  <div
    className="toogleButton"
    style={{
      // minHeight: 25,
      // width: "40%",
      overflow: "hidden",
      ...rootStyles
      // margin: "0 auto"
    }}
  >
    <ul
      style={{
        display: "flex",
        justifyContent: "center",
        // margin: "12px auto",
        borderRadius: "3px",
        maxHeight: 24
      }}
    >
      {items.map((val, key) => (
        <li
          onClick={() => {
            isDisabled ? "" : onTabSelect(val);
          }}
          style={{
            fontFamily: "Montserrat",
            color: grey_6,
            fontWeight: "300",
            fontSize: "12px",
            textAlign: "center",
            // textTransform: "uppercase",
            // lineHeight: "14px",
            flex: 1,
            padding: "2px 0px",
            // paddingBottom: 4,
            border: `1px solid ${grey_6}`
          }}
          className={activeItem === val ? `active_${key}` : `inactive_${key}`}
          key={`${key + 1} pills`}
          role="presentation"
        >
          {val}
        </li>
      ))}
    </ul>
  </div>
);

PillTabs.propTypes = {
  items: PropTypes.array,
  activeItem: PropTypes.string,
  onClick: PropTypes.func
};

export default PillTabs;
