/* eslint-disable global-require */
/**
 *
 * Splash
 *
 */

import React from 'react';
import Lottie from 'react-lottie';

// import PropTypes from 'prop-types';

function SplashScreen() {
  return (
    <div className="splash-section" style={{ padding: '10%' }}>
      {/* <img src={crictec} alt="crcitec" /> */}
      {/* // eslint-disable-next-line global-require */}
      <Lottie
        options={{
          autoplay: true,
          loop: false,
          animationData: require('../../assets/lottie/SplashScreen.json'),
        }}
      />
      {/* <img src={require('../../images/Group 45.png')} alt="crcitec" /> */}
    </div>
  );
}

SplashScreen.propTypes = {};

export default SplashScreen;
