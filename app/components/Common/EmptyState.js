/* eslint-disable */
import React from 'react';
import Reveal from 'react-reveal/Reveal';
import animate from '../../utils/animationConfig';

export default ({ img, msg }) => {
  const imgData =
    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAAXNSR0IArs4c6QAAAsxJREFUaAXtW89rE0EUfm9iRGgFL4pUWus/EPVsahGUpEXoXRRPevDivyJ4Uk8iiFdB2vRQS03rVQn0WkwDVWpvtcYmdp9vRKRGspu3u5Od7czAkuzOvDfve9+bHzs7gxAzERHW6q27BHQTCEoIMBZTlRkxhG1W3CDAxZmp8SeIGOiK2E55WnrfOtfpHrwggGty6QwkENeKqnjrRnlsU0mrZ2ZV52fwKjdgNUCiK92g+1LbLma4Vm8+DAJ6JHWUFeVRPYjBMMxaYXwMIxCoIgbM8XE5Rl22iJTEgIngtC3Wi+0gOCMGLK7EMgHnAB8zQUDh1IlRE3rDdBbaONrZb38JK6PzjACuXDy7F1Vx2vnL69sI+9Fa3QvphZXmN54Pjxz2DQLuzUyfH3pYHrbB1H/3GDbhSW5PQ4+Og91/o7QfLiOdVnvn+26/CrN+7lxIe8BZh5zp+j3Dpj2ctX4jvfTs9KR4JSWpI/RQOMjo4EM6qadtl/cM285QUvucY9hIL61fOZMyIZX/sdMeSMQI4N7364EsGVIh50LaAx5SZGVWjZE2zGtiqXRa3BekvnJiBDAvAJ5Mg8L5lU/8VTbd5Ntwuv60T5tn2D5O0rXIOYaN9NImete0eHaOYQ84KnR4R9tWVBlb83kWsyVnmOijrYAi7UJoyAErfB2p2NICPMd/IwZcLU88A8S3lmLqaxYvlC9Xp8afiwFzGyYFdDtPoNnm1ePFwh1te6xxuHp18jNv1LxeW928BwHNMfhLfG/V9mFE+MqbhT/w73ylPPGYwf7ePhwLsI4b7S3+efrn0o9ykcQhnQtUIUZ6wCHOORJZnuEjQWMICFx419zgIeVCbxm9G6/3mY330h2Delhq8PUfYJs/lyRxvNLnepIoyJus0oeYeBaxljfD49qr9JRLH2JyBfTf3Tb6ENNivXWf226VvVfi43W5OMwh/crxC8+irQnzEZA3AAAAAElFTkSuQmCC';
  const PStyleImg = {
    margin: '20px 40px',
    marginTop: '-24px',
    color: '#828796',
    fontSize: 13,
  };
  const PStyle = {
    margin: '20px 40px',
    color: '#828796',
    fontSize: 13,
  };
  return (
    <div
      style={{
        display: 'flex',
        textAlign: 'center',
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        height: 300,
        flex: 1,
      }}
    >
      {img ? (
        <Reveal effect={animate.emptyState.img.effect} duration={animate.emptyState.img.duration}>
          <img src={img} style={{ width: '200px', height: '200px' }} alt="es" />
        </Reveal>
      ) : (
        <Reveal effect={animate.emptyState.img.effect} duration={animate.emptyState.img.duration}>
          <img src={imgData} alt="es" />
        </Reveal>
      )}
      <Reveal effect={animate.emptyState.text.effect} duration={animate.emptyState.text.duration}>
        <p style={img ? PStyleImg : PStyle}>{msg}</p>
      </Reveal>
    </div>
  );
};
