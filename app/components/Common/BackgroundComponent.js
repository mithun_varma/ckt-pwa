import React, { Component } from "react";

export class BackgroundComponent extends Component {
  render() {
    return (
      <div style={{ ...pageStyle.backgroundStyle, ...this.props.rootStyles }}>
        <div style={pageStyle.titleStyle}>{this.props.title}</div>
      </div>
    );
  }
}

export default BackgroundComponent;

const pageStyle = {
  backgroundStyle: {
    height: 258,
    background: "linear-gradient(to bottom, #ea6550, #fa9441)",
    padding: "16px 16px"
  },
  titleStyle: {
    fontFamily: "Montserrat",
    fontWeight: 700,
    fontSize: 22,
    color: "#FFFFFF"
  }
};
