/**
 *
 * Header
 *
 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

const ScrollTabs = ({ items, activeItem, onClick, className }) => (
  <div className={className ? `tabsSection ${className}` : 'tabsSection'}>
    <ul>
      {items.map(val => (
        <li
          role="presentation"
          key={val}
          onKeyPress={() => {}}
          className={val === activeItem ? 'active' : 'inactive'}
          onClick={e => {
            e.preventDefault();
            if (val !== activeItem) {
              onClick(val);
            }
          }}
        >
          <span>{val}</span>
        </li>
      ))}
    </ul>
  </div>
);

ScrollTabs.propTypes = {
  items: PropTypes.array,
  activeItem: PropTypes.string,
  onClick: PropTypes.func,
  className: PropTypes.string,
};

export default ScrollTabs;
