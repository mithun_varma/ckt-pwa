import React, { Component } from "react";

export class ImageContainer extends Component {
  render() {
    return (
      <div style={{ ...pageStyle.container, ...this.props.rootStyles }}>
        <div style={pageStyle.headerWrapper}>
          <div style={pageStyle.header}>{this.props.data.header}</div>
          {this.props.poweredStadium && (
            <div style={pageStyle.featuredHeader}>
              <div>Powered By</div>
              <div>
                Criclytics
                <span> &trade;</span>
              </div>
            </div>
          )}
        </div>
        <div style={pageStyle.hrLine} />

        {this.props.isAvatar ? (
          <div style={pageStyle.avatarWrapper}>
            {this.props.data.content.map(item => (
              <div style={pageStyle.avatarContainer}>
                <div>
                  <img
                    src={require("../../images/place_holder_1.png")}
                    alt="ground img"
                    width="86"
                    height="110"
                  />
                </div>
                <div style={pageStyle.avatarName}>{item.name}</div>
              </div>
            ))}
          </div>
        ) : (
          <div style={pageStyle.imageWrapper}>
            {this.props.data.content.map(item => (
              <div style={pageStyle.imageContainer}>
                <div
                  style={{
                    width: 150,
                    height: 150,
                    position: "relative"
                  }}
                >
                  <img
                    src="https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png"
                    alt="Snow"
                    style={pageStyle.imageStyle}
                  />
                  <div style={{
                    display: "absolute",
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                    background: "lightgrey"
                  }} />
                </div>
                <div style={pageStyle.imageName}>{item.name}</div>
              </div>
            ))}
          </div>
        )}
      </div>
    );
  }
}

// https://futhead.cursecdn.com/static/img/18/items/stadiums/28.png
export default ImageContainer;

const pageStyle = {
  container: {
    margin: "0 0 0 16px",
    marginTop: -194,
    borderRadius: 3,
    background: "#FFFFFF"
  },
  headerWrapper: {
    // padding: "12px 16px",
    fontFamily: "Montserrat",
    fontWeight: 500,
    fontSize: 12,
    display: "flex",
    flex: 1,
    justifyContent: "space-between"
  },
  header: {
    margin: "12px 0px 12px 16px"
  },
  featuredHeader: {
    background: "#edeff4",
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "center",
    padding: "0px 22px"
  },
  hrLine: {
    height: 1,
    background: "linear-gradient(to right, #ffffff, #e3e4e6, #e3e4e6, #ffffff)"
  },
  avatarWrapper: {
    overflowX: "scroll",
    whiteSpace: "nowrap",
    display: "flex",
    background: "#FFF",
    borderRadius: 3
  },
  avatarContainer: {
    display: "inline-block",
    padding: "20px 18px 18px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatarName: {
    fontFamily: "Montserrat",
    fontWeight: 400,
    fontSize: 12,
    color: "#141b2f",
    textAlign: "center",
    whiteSpace: "initial"
  },
  imageWrapper: {
    overflowX: "scroll",
    whiteSpace: "nowrap",
    display: "flex"
  },
  imageContainer: {
    position: "relative",
    padding: "12px 0px 12px 16px"
  },
  imageStyle: {
    width: 150,
    height: 150
  },
  imageName: {
    position: "absolute",
    bottom: 22,
    left: 22,
    right: 22,
    color: "#FFFFFF",
    fontSize: 12,
    fontFamily: "Montserrat",
    fontWeight: 600,
    whiteSpace: "initial"
  }
};
