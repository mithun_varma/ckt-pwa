import React from 'react';
import PropTypes from 'prop-types';

/* eslint-disable react/no-array-index-key */
function Loader({ styles, noOfLoaders }) {
  return [...Array(noOfLoaders)].map((v, key) => (
    <div className="animated-background" style={styles} key={key} />
  ));
}

Loader.propTypes = {
  styles: PropTypes.object,
  noOfLoaders: PropTypes.number,
};

Loader.defaultProps = {
  styles: {},
  noOfLoaders: 1,
};

export default Loader;
