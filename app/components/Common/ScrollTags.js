/**
 *
 * Header
 *
 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-noninteractive-element-interact: 0 */

import React from 'react';
import PropTypes from 'prop-types';

// import styled from 'styled-components';
const ScrollTags = ({ tags, activeKey, onTagClick, noMarginTop }) => (
  <div className="scheduleHighlights" style={noMarginTop ? { marginTop: 0 } : {}}>
    {/* <ul> */}
    {tags.map(tag => (
      <li
        key={tag.key}
        role="presentation"
        onClick={() => {
          if (activeKey !== tag.key) {
            onTagClick(tag.key, tag.filters);
          }
        }}
        className="scheduleHighlights__wrapper"
      >
        <div
          className={
            tag.key === activeKey ? 'scheduleHighlights__card active' : 'scheduleHighlights__card'
          }
        >
          <span
            style={{
              textTransform: (tag.label === "4's" || tag.label === "6's") && 'lowercase',
            }}
          >
            {tag.label}
          </span>
        </div>
      </li>
    ))}

    {/* </ul> */}
  </div>
);

ScrollTags.propTypes = {
  tags: PropTypes.array.isRequired,
  activeKey: PropTypes.string.isRequired,
  onTagClick: PropTypes.func.isRequired,
  noMarginTop: PropTypes.bool,
};

export default ScrollTags;
