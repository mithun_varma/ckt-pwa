/**
 *
 * ListItem
 *
 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */

import React from 'react';
import PropTypes from 'prop-types';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Reveal from 'react-reveal/Reveal';
import animate from '../../utils/animationConfig';

const ListItem = ({ img, title, caption, onClick, icon, iterationId, className }) => (
  <div
    className={`squardList ${className || ''}`}
    onClick={e => {
      e.preventDefault();
      onClick();
    }}
  >
    {img && (
      <Reveal effect={animate.listItem.content.effect} duration={(iterationId + 1) * 200}>
        <div className="squardList__image">
          <img src={img} alt="dc" />
        </div>
      </Reveal>
    )}
    {/* <Reveal effect={animate.listItem.content.effect} duration={(iterationId + 1) * 1000}>
      {icon && <i className={`${icon}`} />}
    </Reveal> */}

    <div className="squardList__content">
      <Reveal effect={animate.listItem.content.effect} duration={(iterationId + 1) * 200}>
        <div className="squardList__list">
          {title && (
            <span className={icon ? `${icon}` : ''}>
              <strong>{title}</strong>
            </span>
          )}
          {caption && <span>{caption}</span>}
        </div>
      </Reveal>
      <div className="squardList__list">
        <span className="squardList__list__icon">
          <Reveal effect={animate.listItem.content.effect} duration={iterationId * 210}>
            <ChevronRight style={{ color: 'red' }} />
          </Reveal>
        </span>
      </div>
    </div>
  </div>
);

ListItem.propTypes = {
  img: PropTypes.string,
  title: PropTypes.string,
  caption: PropTypes.string,
  onClick: PropTypes.func,
  icon: PropTypes.string,
  iterationId: PropTypes.number,
  className: PropTypes.string,
};

export default ListItem;
