/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable global-require */
/**
 *
 * BottomHeader
 *
 */
/* eslint jsx-a11y/click-events-have-key-events: 0 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */
/* eslint jsx-a11y/no-non-interactive-element-interact: 0 */

import React from 'react';
import Home from '@material-ui/icons/Home';
import Menu from '@material-ui/icons/Menu';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

// function BottomHeader(props) {
const BottomBar = props => {
  const redirectTo = path => {
    props.push(`/${path === '/' ? '' : path}`);
  };

  return (
    <div className="bottomHeader">
      <ul>
        <li>
          <div
            className="li_content_wrapper"
            onClick={() => {
              redirectTo(props.tabs[0].route);
            }}
            role="presentation"
          >
            <div
              className={`${
                // props.location.pathname.indexOf(props.tabs[0].route) == 0 ? 'active-tab' : 'inactive-tab'
                props.location.pathname === '/' ? 'active-tab' : 'inactive-tab'
              }`}
            >
              <Home className="tab-icon" />
              <span>{props.tabs[0].key}</span>
            </div>
          </div>
          <div
            className="li_content_wrapper"
            onClick={() => {
              redirectTo(props.tabs[1].route);
            }}
            role="presentation"
          >
            <div
              className={`${
                // props.location.pathname.indexOf(props.tabs[0].route) == 0 ? 'active-tab' : 'inactive-tab'
                props.location.pathname.indexOf(props.tabs[1].route) > 0
                  ? 'active-tab'
                  : 'inactive-tab'
              }`}
            >
              {/* <CalendarToday className="tab-icon" /> */}
              <img src={require('../../images/calendar.svg')} />
              <span>{props.tabs[1].key}</span>
            </div>
          </div>
          <div
            className="li_content_wrapper"
            onClick={() => {
              redirectTo(props.tabs[2].route);
            }}
            role="presentation"
          >
            <div
              className={`${
                props.location.pathname.indexOf(props.tabs[2].route) > 0
                  ? 'active-tab'
                  : 'inactive-tab'
              }`}
            >
              <Menu className="tab-icon" />
              <span>{props.tabs[2].key}</span>
            </div>
          </div>
        </li>
      </ul>
    </div>
  );
};

BottomBar.propTypes = {
  location: PropTypes.object,
  tabs: PropTypes.array,
  push: PropTypes.func,
};

export default BottomBar;
