/* eslint-disable global-require */
/* eslint-disable jsx-a11y/alt-text */
/**
 *
 * Header
 *
 */
/* eslint jsx-a11y/no-static-element-interactions: 0 */

import React from 'react';
// import BlurCircular from '@material-ui/icons/BlurCircular';
import Search from '@material-ui/icons/Search';
import ArrowBack from '@material-ui/icons/ArrowBack';
import Home from '@material-ui/icons/Home';

import PropTypes from 'prop-types';
// import styled from 'styled-components';

const Header = ({
  history,
  leftIconOnClick,
  rightIconOnClick,
  title,
  showLogo,
  showSearch,
  showHome,
  isBgWhite,
  bgColor,
  className,
  leftImage,
  styles,
}) => (
  <div
    className={`header ${isBgWhite === undefined || isBgWhite === true ? '' : 'scroll'} ${
      bgColor ? 'headerBackground' : ''
    } ${className}`}
    style={{ backgroundColor: bgColor, ...styles }}
  >
    <div
      className="header__icons"
      role="presentation"
      onClick={e => {
        e.preventDefault();
        if (showLogo) {
          history.push('/');
        } else {
          leftIconOnClick();
        }
      }}
    >
      {/* {showLogo ? <BlurCircular /> : <ArrowBack />} */}
      {showLogo ? (
        <img className="logo" src={require('../../images/header_logo.png')} />
      ) : (
        <ArrowBack />
      )}
    </div>
    {leftImage && <img src={leftImage} style={{ width: '36px', marginRight: '8px' }} />}
    {title !== 'crictec' &&
      (title !== 'Crictec' && (
        <div className="header__title" style={{ textTransform: 'inherit' }}>
          {title}
        </div>
      ))}
    {(showSearch || showHome) && (
      <div
        className="header__icons"
        role="presentation"
        onClick={e => {
          e.preventDefault();
          if (showHome) {
            history.push('/');
          } else {
            rightIconOnClick();
          }
        }}
      >
        {showSearch ? <Search /> : <Home />}
      </div>
    )}
  </div>
);
Header.defaultProps = {
  title: 'crictec',
};

Header.propTypes = {
  leftIconOnClick: PropTypes.func,
  title: PropTypes.string,
  rightIconOnClick: PropTypes.string,
  showLogo: PropTypes.bool,
  isBgWhite: PropTypes.bool,
  showHome: PropTypes.bool,
  showSearch: PropTypes.bool,
  history: PropTypes.object,
  bgColor: PropTypes.string,
  className: PropTypes.string,
  leftImage: PropTypes.string,
  styles: PropTypes.object,
};

export default Header;
