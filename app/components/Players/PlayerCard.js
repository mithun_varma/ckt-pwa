/* eslint-disable */

import React from 'react';
import PropTypes from 'prop-types';
import Reveal from 'react-reveal/Reveal';
import animate from '../../utils/animationConfig';
// import styled from 'styled-components';

function PlayerCard({ img, title, onClick, iterationId, barColor }) {
  return (

    <Reveal effect={animate.playerCard.card.effect} duration={(iterationId + 1) * 650}>
      <div onClick={onClick} className="teamcard">
        <img src={img} alt=" " />
        <span
          style={{
            borderTopColor: barColor,
          }}
        >
          {title}
        </span>
      </div>
    </Reveal>
  );
}

PlayerCard.defaultProps = {
  source: 'http://ste.india.com/sites/default/files/2017/08/21/618301-kohli-dhawan-ians.jpg',
};

PlayerCard.propTypes = {
  img: PropTypes.string,
  title: PropTypes.string,
  onClick: PropTypes.func,
  iterationId: PropTypes.number,
};

export default PlayerCard;
