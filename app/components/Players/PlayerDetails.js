/* eslint-disable react/no-unused-state */
/* eslint-disable prettier/prettier */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Header from '../Common/Header';
import imagePlayer from './../../assests_new/images/virat_kohli_nike_removebg.jpg';
import PlayerScore from './PlayerScore';
import PlayerNews from './PlayerNews';
import PlayerBiodata from './PlayerBiodata';
import PlayerSocial from './PlayerSocial';

const PLAYER_DETAILS_TABS = ['about player', 'statistics', 'career', 'news'];
const widthScreen = window.innerWidth;
const heightScreen = window.innerHeight;

export default class PlayerDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {
			activeTab: 'about player',
		};
	}
	componentDidMount = () => {
		this.props.fetchPlayerDetails({ playerId: this.props.playerId });
		window.scrollTo(0, 0);
	};

	handleActiveTab = activeTab => {
		this.setState({
			activeTab,
		});
	};

	handlePlayerAge = personalDetails => {
		if (personalDetails && personalDetails.dob) {
			const dob = new Date(personalDetails.dob);
			const ageDifMs = Date.now() - dob.getTime();
			const ageDate = new Date(ageDifMs); // miliseconds from epoch
			const result = `${Math.abs(ageDate.getUTCFullYear() - 1970)} Years`;
			return result;
		}
		return '-';
	};

	render() {
		const { playerDetails, history, fetchPlayerNews, players } = this.props;
		const PLAYER_DETAILS = playerDetails[this.props.playerId]
			? playerDetails[this.props.playerId]
			: {};
		return (
			<div style={{ display: 'flex', flexDirection: 'column' }}>
				<Header
					title={PLAYER_DETAILS.name}
					leftIcon="arrowBack"
					leftIconOnClick={() => history.goBack()}
					bgColor={PLAYER_DETAILS.color}
				/>
				<div style={pageStyle.container}>
					{this.playerPreview()}
				</div>
				<div style={pageStyle.playerScoreContainer}>
					<PlayerScore />
				</div>
				<div style={pageStyle.playerNewsContainer}>
					<PlayerNews />
				</div>
				<div style={pageStyle.playerBioContainer}>
					<PlayerBiodata />
				</div>
				<div style={pageStyle.playerSocialContainer}>
					<PlayerSocial />
				</div>

			</div>
		)
	}

	playerPreview() {
		return (
			<div style={pageStyle.playerPreviewContainer}>
				<div style={{ display: 'flex', width: widthScreen * 0.05 }}>
					{}
				</div>
				<div style={pageStyle.playerPreviewStyle}>
					<div style={{ display: 'flex', flexDirection: 'row', flex: 1, padding: '9px 9px 0px 0px' }}>
						<div style={{ display: 'flex', flex: 0.50 }}>
							<img src={imagePlayer} width={widthScreen * 0.45} />
						</div>
						<div style={{ display: 'flex', flex: 0.50, marginLeft: '5px' }}>
							{this.playerDetails()}
						</div>

					</div>
				</div>
				<div style={{ display: 'flex', width: widthScreen * 0.05 }}>
					{}
				</div>
			</div>
		);
	}
	playerDetails() {
		return (
			<div style={{ display: 'flex', flexDirection: 'column', flex: 1, }}>
				<div style={{ display: 'flex', flex: 0.30, flexDirection: 'column' }}>
					<div style={{ display: 'flex', flex: 0.40, fontSize: '24px', color: '#141b2f', fontWeight: '500' }}>
						Virat
					</div>
					<div style={{ display: 'flex', flex: 0.40, fontSize: '24px', color: '#141b2f', fontWeight: '700' }}>
						Kholi
					</div>
					<div style={{ display: 'flex', flex: 0.20, fontColor: '#141b2f', fontSize: '10px' }}>
						BATSMAN | INDIA
					</div>
				</div>

				<div style={{ display: 'flex', flex: 0.02 }}>
					{}
				</div>

				<div style={{ display: 'flex', flex: 0.15, flexDirection: 'column' }}>
					<div style={{ display: 'flex', flex: 0.40, fontSize: '12px', color: '#f76b1c', fontWeight: '700' }}>
						Born
					</div>
					<div style={{ display: 'flex', flex: 0.40, fontSize: '12px', color: '#141b2f' }}>
						20 November 1987
					</div>
					<div style={{ display: 'flex', flex: 0.20, fontColor: '#a1a4ac', fontSize: '10px' }}>
						(31 Years)
					</div>
				</div>

				<div style={{ display: 'flex', flex: 0.04 }}>
					{}
				</div>

				<div style={{ display: 'flex', flex: 0.15, flexDirection: 'column' }}>
					<div style={{ display: 'flex', flex: 0.40, fontSize: '12px', color: '#f76b1c', fontWeight: '700' }}>
						Batting Style
					</div>
					<div style={{ display: 'flex', flex: 0.40, fontSize: '12px', color: '#141b2f' }}>
						Right Handed Batsman
					</div>
				</div>

				<div style={{ display: 'flex', flex: 0.04 }}>
					{}
				</div>

				<div style={{ display: 'flex', flex: 0.15, flexDirection: 'column' }}>
					<div style={{ display: 'flex', flex: 0.40, fontSize: '12px', color: '#f76b1c', fontWeight: '700' }}>
						Bowling Style
					</div>
					<div style={{ display: 'flex', flex: 0.40, fontSize: '12px', color: '#141b2f' }}>
						Right Handed Bowler
					</div>
				</div>

			</div>
		);
	}

}
const pageStyle = {
	container: {
		display: 'flex',
		width: widthScreen,
		backgroundImage: 'linear-gradient(to left, #ea6550, #fa9441)',
		height: widthScreen * 0.71,
	},
	playerPreviewStyle: {
		display: 'flex',
		width: widthScreen * 0.90,
		height: heightScreen * 0.50,
		backgroundColor: '#fff',
		boxShadow: '0px 3px 6px 0px #13000000',
	},
	playerPreviewContainer: {
		display: 'flex',
		marginTop: '98px',
		flex: 1,
		flexDirection: 'row',
	},
	playerScoreContainer: {
		justifyContent: 'center',
		display: 'flex',
		marginTop: '190px',
		flex: 1,
	},
	playerNewsContainer: {
		width: widthScreen * 0.90,
		backgroundColor: '#fff',
		display: 'flex',
		marginTop: '19px',
		flex: 1,
		marginLeft: heightScreen * 0.03,
	},
	playerBioContainer: {
		width: widthScreen * 0.90,
		backgroundColor: '#fff',
		display: 'flex',
		marginTop: '19px',
		flex: 1,
		marginLeft: heightScreen * 0.03,


	},
	playerSocialContainer: {
		width: widthScreen * 0.90,
		backgroundColor: '#fff',
		display: 'flex',
		marginTop: '19px',
		flex: 1,
		marginLeft: heightScreen * 0.03,


	},
}

PlayerDetails.propTypes = {
	playerDetails: PropTypes.object,
	history: PropTypes.object,
	fetchPlayerDetails: PropTypes.func,
	playerId: PropTypes.string,
	fetchPlayerNews: PropTypes.func,
	players: PropTypes.object,
};