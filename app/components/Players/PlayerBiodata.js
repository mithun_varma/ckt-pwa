/* eslint-disable prettier/prettier */
import React, { Component } from 'react';


export default class PlayerBio extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };

    }
    componentDidMount() {

    }
    render() {
        return (
            <div style={pageStyle.container}>
                <div style={pageStyle.playerBioHeading}>
                    Player Bio
                </div>
                <div style={pageStyle.playerBioType}>
                    IND vs WI
                </div>
                <div style={pageStyle.playerBioPersonal}>
                    {"Born on November 20, 1987, Virat Kohli began his career as a under 19 captain for Delhi.."}
                </div>
                <div style={pageStyle.playerBioType}>
                    Teams
                </div>
                <div style={pageStyle.playerBioTeams}>
                    {"Indian Team, Delhi Boys Club, Bangalore City, Royal Challengers Bangalore, Ranjhy.."}

                </div>
            </div>
        );
    }

}

const pageStyle = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
        paddingBottom: '16px',
    },
    playerBioHeading: {
        display: 'flex',
        justifyContent: 'flex-start',
        padding: '12px 0 12px 12px',
        fontSize: '12px',
        fontWeight: '700',
        border: '1px solid #e3e4e6',
        fontFamily: 'Montserrate-Medium',
    },
    playerBioType: {
        display: 'flex',
        flex: 1,
        fontWeight: '700',
        fontSize: '12px',
        letterSpacing: '0.33',
        justifyContent: 'flex-start',
        color: '#f76b1c',
        marginTop: '10px',

    },
    playerBioPersonal: {
        display: 'flex',
        flex: 1,
        fontSize: '12px',
        color: '#141b2f',
        justifyContent: 'flex-start',

    },
    playerBioTeams: {
        display: 'flex',
        flex: 1,
        fontSize: '12px',
        color: '#141b2f',
        justifyContent: 'flex-start',

    }

}