/* eslint no-underscore-dangle: 0 */
/* eslint no-nested-ternary: 0 */
/* eslint react/jsx-boolean-value: 0 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Reveal from 'react-reveal/Reveal';

import Loader from 'components/Common/Loader';
import EmptyState from 'components/Common/EmptyState';
import PlayerCard from 'components/Players/PlayerCard';

import animate from '../../utils/animationConfig';
import Header from '../Common/Header';
class PlayersList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = () => {
    this.props.fetchPlayersList({ skip: 0, limit: 6 });
  };

  getPlayerDetails = playerData => {
    this.props.history.push(`/players/${playerData._id}`);
  };

  render() {
    const { players, history } = this.props;
    return (
      <div className="wrapperContainer">
        <Header
          title="Players"
          leftIconOnClick={() => {
            history.goBack();
          }}
        />

        <main>
          <div className="sectionWrapper__card">
            {players.playersListLoading ? (
              <Loader styles={{ height: '75px' }} noOfLoaders={5} />
            ) : players.playersList.length > 0 ? (
              <Reveal
                effect={animate.playerList.card.effect}
                duration={animate.playerList.card.effect}
              >
                <section className="sectionWrapper">
                  <div className="sectionWrapper__title">
                    <Reveal effect="" duration={animate.home.fantasyHeading.duration}>
                      <h5>Top Guns</h5>
                    </Reveal>
                  </div>
                  <div className="playerTeam">
                    <div className="d-flex">
                      {players.playersList.slice(0, 6).map((data, key) => (
                        <PlayerCard
                          key={`${key + 1}`}
                          // eslint-disable-next-line global-require
                          img={data.avatar || require('../../images/Player_Placeholder.png')}
                          title={data.displayName}
                          onClick={() => this.getPlayerDetails(data)}
                          iterationId={key}
                          barColor={data.color}
                        />
                      ))}
                    </div>
                    <div className="sectionWrapper__link">
                      <a
                        href="##"
                        onClick={e => {
                          e.preventDefault();
                          this.props.history.push('/players/list');
                        }}
                      >
                        View All
                      </a>
                    </div>
                  </div>
                </section>
              </Reveal>
            ) : (
              <Reveal effect={animate.emptyState.effect}>
                <EmptyState msg="No Series News found" />
              </Reveal>
            )}
          </div>
        </main>
      </div>
    );
  }
}

PlayersList.propTypes = {
  players: PropTypes.object,
  fetchPlayersList: PropTypes.func,
  history: PropTypes.object,
};

export default PlayersList;
