/* eslint-disable global-require */
/* eslint-disable indent */
/* eslint camelcase: 0 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import InfiniteScroll from 'react-infinite-scroll-component';

import EmptyState from 'components/Common/EmptyState';
import Loader from 'components/Common/Loader';
import ArticleCard from 'components/Articles/ArticleCard';
import FeatureArticleCard from 'components/Articles/FeatureArticleCard';

class PlayerNews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initial: true,
    };
    this.newsSlideSettings = {
      dots: false,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      centerMode: true,
      // centerPadding: 18,
    };
    this.loadMoreItems = this.loadMoreItems.bind(this);
  }

  componentDidMount = () => {
    this.props.fetchPlayerNews({ playerId: this.props.playerId, skip: 0, limit: 10 });
  };

  // static getDerivedStateFromProps(props, state) {
  //   if (state.initial)
  //     return {
  //       initial: false,
  //     };
  //   return null;
  // }

  UNSAFE_componentWillReceiveProps = () => {
    if (this.state.initial) {
      this.setState({
        initial: false,
      });
    }
  };

  loadMoreItems = () => {
    this.props.fetchPlayerNews({
      playerId: this.props.playerId,
      skip: 0,
      limit: this.props.players.playerNews.length,
    });
  };

  cardOnClick = path => {
    this.props.history.push(path);
  };

  getCardType = type => {
    if (type === 'match-report') {
      return 'matchReport';
    } else if (type === 'analysis') {
      return 'fantasyAnalysis';
    }
    return 'newsOpinion';
  };

  render() {
    const { players } = this.props;
    return (
      <div className="infinite_scroll_news">
        {players.playerNewsLoading && players.playerNews.length === 0 ? (
          <Loader styles={{ height: '75px' }} noOfLoaders={5} />
        ) : (
          <React.Fragment>
            {/* Slider section */}
            {false &&
              players.playerNewsFeatured.length > 0 && (
                <div className="sectionWrapper__card">
                  {players.playerNewsFeatured.length === 1 ? (
                    <FeatureArticleCard
                      article={players.playerNewsFeatured[0]}
                      cardType={this.getCardType(players.playerNewsFeatured[0].type)}
                      articleHost={players.articleHost}
                      onClick={this.cardOnClick}
                    />
                  ) : (
                    <Slider
                      {...this.newsSlideSettings}
                      autoplay={false}
                      centerPadding={players.playerNewsFeatured.length > 1 ? 18 : 0}
                      className={players.playerNewsFeatured.length > 1 && 'multiple-articles'}
                    >
                      {players.playerNewsFeatured.map((article, key) => (
                        <FeatureArticleCard
                          article={article}
                          cardType={this.getCardType(article.type)}
                          articleHost={players.articleHost}
                          key={`${key + 1}`}
                          onClick={this.cardOnClick}
                        />
                      ))}
                    </Slider>
                  )}
                </div>
              )}
            {/* Infinite croll Section */}
            {players.playerNews.length > 0 && (
              <InfiniteScroll
                dataLength={players.playerNews.length}
                next={this.loadMoreItems}
                loader={
                  players.playerNews.length >= 10 && (
                    <Loader styles={{ height: '150px' }} noOfLoaders={5} />
                  )
                }
                hasMore={players.playerNewsOldLength !== players.playerNews.length}
              >
                {players.playerNews.map((article, articleKey) => (
                  <div className="sectionWrapper__card" key={`${articleKey + 1}`}>
                    {/* {article.filters && article.filters.featured ? (
                      <FeatureArticleCard
                        article={article}
                        cardType={this.getCardType(article.type)}
                        articleHost={players.articleHost}
                        onClick={this.cardOnClick}
                      />
                    ) : ( */}
                    <ArticleCard
                      article={article}
                      // cardType={this.getCardType(article.type)}
                      cardType="analysis"
                      articleHost={players.articleHost}
                      onClick={this.cardOnClick}
                    />
                    {/* )} */}
                  </div>
                ))}
              </InfiniteScroll>
            )}
            {/* Empty State */}
            {players.playerNewsFeatured.length === 0 &&
              players.playerNews.length === 0 && (
                <EmptyState
                  img={require('../../images/no_articles.png')}
                  msg="No Teams News found"
                />
              )}
          </React.Fragment>
        )}
      </div>
    );
  }
}

PlayerNews.propTypes = {
  players: PropTypes.object,
  fetchPlayerNews: PropTypes.func,
  history: PropTypes.object,
  playerId: PropTypes.string,
};

export default PlayerNews;
