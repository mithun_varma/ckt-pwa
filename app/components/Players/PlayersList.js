/* eslint no-underscore-dangle: 0 */
/* eslint no-nested-ternary: 0 */
/* eslint react/jsx-boolean-value: 0 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroll-component';

import Reveal from 'react-reveal/Reveal';

import Loader from 'components/Common/Loader';
import EmptyState from 'components/Common/EmptyState';
import PlayerCard from 'components/Players/PlayerCard';

import animate from '../../utils/animationConfig';
import Header from '../Common/Header';
class PlayersList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initial: true,
    };
  }

  componentDidMount = () => {
    this.props.fetchPlayersList({ skip: 0, limit: 18 });
  };

  getPlayerDetails = playerData => {
    this.props.history.push(`/players/${playerData._id}`);
  };

  static getDerivedStateFromProps(props, state) {
    if (state.initial)
      return {
        initial: false,
      };
    return null;
  }

  loadMoreItems = () => {
    this.props.fetchPlayersList({
      limit: 18,
      skip: this.props.players.playersList.length,
    });
  };

  render() {
    const { players, history } = this.props;
    return (
      <div className="wrapperContainer">
        <Header
          title="Players"
          leftIconOnClick={() => {
            history.goBack();
          }}
        />

        <main>
          <div className="sectionWrapper__card">
            {players.playersListLoading && this.state.initial ? (
              <Loader styles={{ height: '75px' }} noOfLoaders={5} />
            ) : players.playersList.length > 0 ? (
              <InfiniteScroll
                dataLength={players.playersList.length}
                next={this.loadMoreItems}
                loader={
                  players.playersList.length >= 18 && (
                    <Loader styles={{ height: '150px' }} noOfLoaders={2} />
                  )
                }
                hasMore={players.oldListLength !== players.playersList.length}
              >
                <Reveal
                  effect={animate.playerList.card.effect}
                  duration={animate.playerList.card.effect}
                >
                  <section className="sectionWrapper">
                    <div className="playerTeam">
                      <div className="d-flex">
                        {players.playersList.map((data, key) => (
                          <PlayerCard
                            key={`${key + 1}`}
                            // eslint-disable-next-line global-require
                            img={data.avatar || require('../../images/Player_Placeholder.png')}
                            title={data.displayName} // caption={
                            //   data.personalDetails ? data.personalDetails.birthPlace : '-'
                            // }
                            onClick={() => this.getPlayerDetails(data)}
                            iterationId={key}
                            barColor={data.color}
                          />
                        ))}
                      </div>
                    </div>
                  </section>
                </Reveal>
              </InfiniteScroll>
            ) : (
              <Reveal effect={animate.emptyState.effect}>
                <EmptyState msg="No Series News found" />
              </Reveal>
            )}
          </div>
        </main>
      </div>
    );
  }
}

PlayersList.propTypes = {
  players: PropTypes.object,
  fetchPlayersList: PropTypes.func,
  history: PropTypes.object,
};

export default PlayersList;
