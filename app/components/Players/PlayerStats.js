/* eslint indent: 0 */
/* eslint eqeqeq: 0 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PillTabs from '../Common/PillTabs';

const BATTING_CONFIG = [
  { title: 'Matches', key: 'matchesPlayed' },
  { title: 'Innings', key: 'inningsPlayed' },
  { title: 'Runs', key: 'runs' },
  { title: 'Highest Score', key: 'highestScore' },
  { title: 'Average', key: 'avg' },
  { title: 'Strike Rate', key: 'strikeRate' },
  { title: 'Not Outs', key: 'notOuts' },
  { title: 'Fours', key: 'fours' },
  { title: 'Sixes', key: 'sixes' },
  { title: 'Ducks', key: 'ducks' },
  { title: '50s', key: 'fifties' },
  { title: '100s', key: 'centuries' },
  { title: '200s', key: 'doubleCenturies' },
];

const BOWLING_CONFIG = [
  { title: 'Matches', key: 'matchesPlayed' },
  { title: 'Innings', key: 'inningsPlayed' },
  { title: 'Runs', key: 'runs' },
  { title: 'Average', key: 'avg' },
  { title: 'Strike Rate', key: 'strikeRate' },
  { title: 'Balls', key: 'balls' },
  { title: 'Economy', key: 'economy' },
  { title: 'Five Wickets', key: 'fiveWickets' },
  { title: 'Ten Wickets', key: 'tenWickets' },
  { title: 'Wickets', key: 'wickets' },
  { title: 'Best Bowl Innings', key: 'bestBallInInnings' },
  { title: 'Best Bowl  Matches', key: 'bestBallInMatches' },
];

export class PlayerStats extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 0,
    };
  }

  onTabChange = tab => {
    if (tab !== this.state.activeTab) {
      this.setState({
        activeTab: tab,
      });
    }
  };

  render() {
    const { batFieldStats, bowlStats } = this.props.stats;
    return (
      <React.Fragment>
        {/* <div className="tabContent-section"> */}

        <PillTabs
          items={['Batting', 'Bowling']}
          activeItem={this.state.activeTab}
          onClick={this.onTabChange}
        />
        <div className="table-responsive">
          <table className="table">
            <thead>
              <tr>
                <th>{this.state.activeTab == 0 ? 'Batting' : 'Bowling'}</th>
                <th>Test</th>
                <th>ODI</th>
                <th>T20</th>
                <th>IPL</th>
              </tr>
            </thead>

            <tbody>
              {this.state.activeTab === 0
                ? BATTING_CONFIG.map((record, key) => (
                    <tr key={`${key + 1}`}>
                      <td>{record.title}</td>
                      <td>
                        {batFieldStats && batFieldStats.test ? batFieldStats.test[record.key] : '-'}
                      </td>
                      <td>
                        {batFieldStats && batFieldStats.odi ? batFieldStats.odi[record.key] : '-'}
                      </td>
                      <td>
                        {batFieldStats && batFieldStats.t20i ? batFieldStats.t20i[record.key] : '-'}
                      </td>
                      <td>
                        {batFieldStats && batFieldStats.ipl ? batFieldStats.ipl[record.key] : '-'}
                      </td>
                    </tr>
                  ))
                : BOWLING_CONFIG.map((record, key) => (
                    <tr key={`${key + 1}`}>
                      <td>{record.title}</td>
                      <td>{bowlStats && bowlStats.test ? bowlStats.test[record.key] : '-'}</td>
                      <td>{bowlStats && bowlStats.odi ? bowlStats.odi[record.key] : '-'}</td>
                      <td>{bowlStats && bowlStats.t20i ? bowlStats.t20i[record.key] : '-'}</td>
                      <td>{bowlStats && bowlStats.ipl ? bowlStats.ipl[record.key] : '-'}</td>
                    </tr>
                  ))}
            </tbody>
          </table>
        </div>
        {/* </div> */}
      </React.Fragment>
    );
  }
}

PlayerStats.propTypes = {
  stats: PropTypes.object,
  batFieldStats: PropTypes.object,
  bowlStats: PropTypes.object,
};

export default PlayerStats;
