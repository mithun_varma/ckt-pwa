/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import map from 'lodash/map';
import isEmpty from 'lodash/isEmpty';

// import * as stringConverter from 'utils/stringConverter';
import Sticky from 'react-stickynode';

import Reveal from 'react-reveal/Reveal';
import animate from '../../utils/animationConfig';
import Header from '../Common/Header';
import SingleImage from '../PlayersContent/SingleImage';
import ItemList from '../PlayersContent/ItemList';
import ScrollTabs from '../Common/ScrollTabs';
import PlayerStats from './PlayerStats';
import EmptyState from '../Common/EmptyState';
import PlayerNews from './PlayerNews';
import jersey from '../../images/tshirt.svg';
import cap from '../../images/baseball-cap.svg';

const PLAYER_DETAILS_TABS = ['about player', 'statistics', 'career', 'news'];
export default class PlayerDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 'about player',
    };
  }

  componentDidMount = () => {
    this.props.fetchPlayerDetails({ playerId: this.props.playerId });
    window.scrollTo(0, 0);
  };

  handleActiveTab = activeTab => {
    this.setState({
      activeTab,
    });
  };

  handlePlayerAge = personalDetails => {
    if (personalDetails && personalDetails.dob) {
      const dob = new Date(personalDetails.dob);
      const ageDifMs = Date.now() - dob.getTime();
      const ageDate = new Date(ageDifMs); // miliseconds from epoch
      const result = `${Math.abs(ageDate.getUTCFullYear() - 1970)} Years`;

      return result;
    }
    return '-';
  };

  render() {
    const { playerDetails, history, fetchPlayerNews, players } = this.props;
    const PLAYER_DETAILS = playerDetails[this.props.playerId]
      ? playerDetails[this.props.playerId]
      : {};
    return (
      <div>
        <Header
          title={PLAYER_DETAILS.name}
          leftIcon="arrowBack"
          leftIconOnClick={() => history.goBack()}
          bgColor={PLAYER_DETAILS.color}
        />
      </div>
    )
  }
}


//           <div className="teamDetail">
//             <section
//               className="teamDetail__header"
//               style={{ backgroundColor: PLAYER_DETAILS.color }}
//             />
//             <section className="teamDetail__wrapper">
//               <section className="sectionWrapper">
//                 <div className="sectionWrapper__card">
//                   <div className="innerWrapper topRadius">
//                     <div className="playerInfo">
//                       <div className="playerInfo__header">
//                         <div className="playerInfo__card">
//                           <div className="playerInfo__image">
//                             <Reveal effect={animate.playerDetails.image.effect}>
//                               <SingleImage
//                                 imagesource={
//                                   PLAYER_DETAILS.avatar || // eslint-disable-next-line global-require
//                                   require('../../images/Player_Placeholder.png')
//                                 }
//                                 hasJerseyNo
//                               />
//                               <div className="singleImage__card__jersey">
//                                 <div>
//                                   <p>Jersey</p>
//                                   <p>
//                                     <span>
//                                       <img src={jersey} alt="Jersey" />
//                                     </span>
//                                     {PLAYER_DETAILS.personalDetails &&
//                                       PLAYER_DETAILS.personalDetails.jerseyNumber
//                                       ? PLAYER_DETAILS.personalDetails.jerseyNumber
//                                       : '-'}
//                                   </p>
//                                 </div>
//                                 <div>
//                                   <p>Test Cap</p>
//                                   <p>
//                                     <span>
//                                       <img src={cap} alt="Cap" />
//                                     </span>
//                                     {PLAYER_DETAILS.personalDetails &&
//                                       PLAYER_DETAILS.personalDetails.testCap
//                                       ? PLAYER_DETAILS.personalDetails.testCap
//                                       : '-'}
//                                   </p>
//                                 </div>
//                               </div>
//                             </Reveal>
//                           </div>
//                           <ul>
//                             <li>
//                               <ItemList
//                                 title="Born"
//                                 desc={
//                                   PLAYER_DETAILS.personalDetails && PLAYER_DETAILS.personalDetails.dob
//                                     ? PLAYER_DETAILS.personalDetails.dob
//                                     : '-'
//                                 }
//                               />
//                             </li>
//                             <li>
//                               <ItemList
//                                 title="Current Age"
//                                 desc={`${this.handlePlayerAge(PLAYER_DETAILS.personalDetails)}`}
//                               />
//                             </li>
//                             <li>
//                               <ItemList
//                                 title="Batting Style"
//                                 desc={
//                                   PLAYER_DETAILS.personalDetails &&
//                                     PLAYER_DETAILS.personalDetails.battingStyle
//                                     ? PLAYER_DETAILS.personalDetails.battingStyle
//                                     : '-'
//                                 }
//                               />
//                             </li>
//                             <li>
//                               <ItemList
//                                 title="Bowling Style"
//                                 desc={
//                                   PLAYER_DETAILS.personalDetails &&
//                                     PLAYER_DETAILS.personalDetails.bowlingStyle
//                                     ? PLAYER_DETAILS.personalDetails.bowlingStyle
//                                     : '-'
//                                 }
//                               />
//                             </li>
//                             <li>
//                               <ItemList
//                                 title="Role"
//                                 desc={
//                                   PLAYER_DETAILS.personalDetails &&
//                                     PLAYER_DETAILS.personalDetails.role
//                                     ? PLAYER_DETAILS.personalDetails.role
//                                     : '-'
//                                 }
//                               />
//                             </li>
//                           </ul>
//                         </div>
//                         <ul>
//                           <li>
//                             <ItemList
//                               title="Teams"
//                               link={
//                                 PLAYER_DETAILS.teams && PLAYER_DETAILS.teams.length > 1
//                                   ? PLAYER_DETAILS.teams.join(',')
//                                   : '-'
//                               }
//                             />
//                           </li>
//                         </ul>
//                       </div>
//                     </div>
//                   </div>
//                   <Sticky
//                     enabled
//                     top={49}
//                     // bottomBoundary={3000}
//                     innerZ={999}
//                     activeClass="bottomBoxSch"
//                   >
//                     <ScrollTabs
//                       items={PLAYER_DETAILS_TABS}
//                       activeItem={this.state.activeTab}
//                       onClick={activeTab => {
//                         this.handleActiveTab(activeTab);
//                       }}
//                     />
//                   </Sticky>
//                   {this.state.activeTab === 'about player' && (
//                     <div className="tabContent-section">
//                       {!isEmpty(PLAYER_DETAILS.profile) ? (
//                         <div className="articleDetail">
//                           <p>{PLAYER_DETAILS.profile}</p>
//                         </div>
//                       ) : (
//                           <EmptyState
//                             // eslint-disable-next-line global-require
//                             img={require('../../images/no_players.png')}
//                             msg="No Career Found"
//                           />
//                         )}
//                     </div>
//                   )}
//                   {this.state.activeTab === 'statistics' && (
//                     <div className="tabContent-section">
//                       <div className="articleDetail">
//                         <Reveal
//                           effect={animate.playerDetails.stats.effect}
//                           duration={animate.playerDetails.stats.effect}
//                         >
//                           <PlayerStats stats={PLAYER_DETAILS} />
//                         </Reveal>
//                       </div>
//                     </div>
//                   )}
//                   {this.state.activeTab === 'career' && (
//                     <div className="tabContent-section">
//                       <Reveal effect={animate.emptyState.effect}>
//                         <div className="playerInfo__card__padLeft">
//                           { {!isEmpty(PLAYER_DETAILS.careerInfo) ? (
//                           <ul>
//                             {map(PLAYER_DETAILS.careerInfo, (item, key) => (
//                               <li key={`${key + 1}`}>
//                                 <ItemList title={stringConverter.toUpperCase(key)} desc={item} />
//                               </li>
//                             ))}
//                           </ul>
//                         ) : (
//                           <EmptyState msg="No Career Found" />
//                         )} }
//         {/*
//                         <div className="careerContent d-flex">
//                             <ul className="timeLine">
//                               <li />
//                               <li />
//                               <li />
//                               <li />
//                               <li />
//                               <li />
//                               <li />
//                               <li />
//                             </ul>
//                             <div className="timelineInfo">
//                               <div className="career__item">
//                                 <div className="career__date">October 27, 2018</div>
//                                 <div className="career__title">Last ODI</div>
//                                 <div className="career__text">
//                                   vs Windies at Maharashtra Cricket Association Stadium
//                               </div>
//                               </div>
//                               <div className="career__item">
//                                 <div className="career__date">October 27, 2018</div>
//                                 <div className="career__title">Last Test</div>
//                                 <div className="career__text">
//                                   vs Windies at Rajiv Gandhi International Stadium
//                               </div>
//                               </div>
//                               <div className="career__item">
//                                 <div className="career__date">October 27, 2018</div>
//                                 <div className="career__title">ODI Debut</div>
//                                 <div className="career__text">
//                                   vs Sri Lanka at Rangiri Dambulla International Stadium
//                               </div>
//                               </div>
//                               <div className="career__item">
//                                 <div className="career__date">October 27, 2018</div>
//                                 <div className="career__title"> Last T20</div>
//                                 <div className="career__text">
//                                   vs England at County Ground, Jul 08, 2018
//                               </div>
//                               </div>
//                               <div className="career__item">
//                                 <div className="career__date">October 27, 2018</div>
//                                 <div className="career__title">Last IPL</div>
//                                 <div className="career__text">
//                                   vs Rajasthan Royals at Sawai Mansingh Stadium, May 19, 2018
//                               </div>
//                               </div>
//                               <div className="career__item">
//                                 <div className="career__date">October 27, 2018</div>
//                                 <div className="career__title">ODI Debut</div>
//                                 <div className="career__text">
//                                   vs Windies at Sabina Park, Jun 20, 2011
//                               </div>
//                               </div>
//                               <div className="career__item">
//                                 <div className="career__date">October 27, 2018</div>
//                                 <div className="career__title">T20 Debut</div>
//                                 <div className="career__text">
//                                   vs Zimbabwe at Harare Sports Club, Jun 12, 2010
//                               </div>
//                               </div>
//                               <div className="career__item">
//                                 <div className="career__date">October 27, 2018</div>
//                                 <div className="career__title"> IPL debut</div>
//                                 <div className="career__text">
//                                   vs Kolkata Knight Riders at M.Chinnaswamy Stadium, Apr 18, 2008
//                               </div>
//                               </div>
//                             </div>
//                           </div>
//                         </div>

//                       </Reveal>
//                     </div >
//         */}
//         )
// }
//                   {
//           this.state.activeTab === 'news' && (
//             <Reveal effect={animate.playerDetails.playerNews.effect}>
//               <div className="tabContent-section">
//                 <PlayerNews
//                   playerId={this.props.playerId}
//                   history={history}
//                   players={players}
//                   fetchPlayerNews={fetchPlayerNews}
//                 />
//               </div>
//             </Reveal>
//           )
//         }
//       </div >
//               </section >
//             </section >
//           </div >
//         }
//       </div >
//     );
//   }
// }




PlayerDetails.propTypes = {
  playerDetails: PropTypes.object,
  history: PropTypes.object,
  fetchPlayerDetails: PropTypes.func,
  playerId: PropTypes.string,
  fetchPlayerNews: PropTypes.func,
  players: PropTypes.object,
};

