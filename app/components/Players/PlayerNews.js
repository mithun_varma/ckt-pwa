/* eslint-disable react/prefer-stateless-function */
/* eslint-disable prettier/prettier */
/* eslint-disable global-require */
/* eslint-disable indent */
/* eslint camelcase: 0 */

import React, { Component } from 'react';
import NewsImage from './../../assests_new/images/news.jpg';

const widthScreen = window.innerWidth;
const heightScreen = window.innerHeight;


export default class PlayerNews extends Component {
    render() {
        return (
            <div style={pageStyle.playerNewsContainer}>
                <div style={pageStyle.playerNewsHead}>
                    {"Virat in News"}
                </div>
                <div style={pageStyle.playerNewsView}>
                    <div style={pageStyle.playerNewsViewImage}>
                        <img src={NewsImage} alt="player news" />
                    </div>
                    <div style={pageStyle.playerNewsViewTime}>
                        4 mins read
                    </div>
                    <div style={pageStyle.playerNewsViewHeading}>
                        Virat comes to age as captain.
                    </div>
                    <div style={pageStyle.playerNewsViewDescription}>
                        Delhi boy showed the world that he is best cricket captain in the world considering what he has achived.
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row', flex: 1, padding: '10px 5px 10px 5px' }}>
                        <div style={pageStyle.playerNewsViewAuthor}>
                            Crictech Team
                        </div>
                        <div style={pageStyle.playerNewsViewDate}>
                            Today date
                        </div>
                    </div>
                </div>
                <div style={pageStyle.playerNewsMoreNews}>
                    More News on Virat
                </div>

            </div>
        );
    }
}

const pageStyle = {
    playerNewsContainer: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column',

    },
    playerNewsHead: {
        display: 'flex',
        justifyContent: 'flex-start',
        color: '#141b2f',
        fontSize: '12px',
        marginLeft: '16px',
        marginTop: '12px',
        borderBottomWidth: '1px',
        borderBottomStyle: 'solid',
        borderBottomColor: '#e3e4e6',
        fontWeight: '700',
        fontFamily: 'Montserrat-Medium',
    },
    playerNewsView: {
        paddingTop: '10px',
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: '#f1f1f1',
        boxShadow: '0 3px 6px 0 #13000000',
    },
    playerNewsViewImage: {
        display: 'flex',
        width: widthScreen * 0.60,


    },
    playerNewsViewTime: {
        display: 'flex',
        width: widthScreen * 0.40,
        flex: 0.4,
        padding: '5px',
        color: '#ffffff',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundImage: 'linear-gradient(to right, #ea6550, #fa9441)',
        marginTop: '-20px',

    },
    playerNewsViewHeading: {
        display: 'flex',
        padding: '5px',
        color: '#141b2f',
        fontSize: '14px',
        fontWeight: '700',
        fontFamily: 'Montserrat-Medium',


    },
    playerNewsViewDescription: {
        display: 'flex',
        padding: '5px',
        color: '#141b2f',
        fontSize: '14px',
        fontFamily: 'Montserrat-Medium',

    },
    playerNewsViewAuthor: {
        display: 'flex',
        padding: '5px',
        flex: 0.50,
        justifyContent: 'flex-start',
        color: '#141b2f',
        fontSize: '14px',
        fontFamily: 'Montserrat-Medium',

    },
    playerNewsViewDate: {
        display: 'flex',
        padding: '5px',
        flex: 0.50,
        justifyContent: 'flex-end',
        color: '#141b2f',
        fontSize: '14px',
        fontFamily: 'Montserrat-Medium',

    },
    playerNewsMoreNews: {
        display: 'flex',
        flex: 1,
        padding: '10px 5px 10px 0',
        color: '#f76b1c',
        fontSize: '14px',
        fontFamily: 'Montserrat-Medium',
        justifyContent: 'flex-end',
    },
}
