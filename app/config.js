export const SCORE_DETAILS_POLLING_DELAY = 8000;
export const SCORE_DETAILS_FETCH_ERROR__POLLING_DELAY = 5000;
export const FEATURED_SCORES_POLLING_DELAY = 8000;
export const COMMENTARY_DATA_DELAY = 8000;
export const HIGHLIGHTS_DATA_DELAY = 800;
export const FEATURED_SCORES_FETCH_ERROR_POLLING_DELAY = 5000;
